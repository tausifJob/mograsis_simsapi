
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');
    simsController.controller('AdmissionDashboardCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.itemsPerPage = '15';
            $scope.currentPage = 0;
            $scope.itemsPerPage1 = '5';
            $scope.currentPage1 = 0;
            $scope.itemsPerPage2 = '5';
            $scope.currentPage2 = 0;
            $scope.currentPage3 = 0;
            $scope.itemsPerPage3 = '5';
            var str, cnt;
            $scope.dash = [];
            $scope.view = [];
            $scope.screen_datails = [];
            $scope.Upload_doc_datails = [];
            var admdetails = [];
            var del = [];
            var t = false;
            var main, section = "", fee_category = "";
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');

            var param = $stateParams.Class;

            if (param != undefined) {
                $scope.edt =
                    {
                        curr_code: param.curr_code,
                        academic_year: param.academic_year,
                        grade_code: param.grade_code,
                        sims_appl_parameter_reg: param.sims_appl_parameter_reg
                    };
            }



            /*start_paging_gridview*/
            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;
            }

            $scope.size1 = function (pagesize) {
                $scope.itemsPerPage1 = pagesize;
            }

            $scope.size2 = function (pagesize) {
                $scope.itemsPerPage2 = pagesize;
            }

            $scope.size3 = function (pagesize) {
                $scope.itemsPerPage3 = pagesize;
            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }
                return ret;
            };

            $scope.range1 = function () {
                var rangeSize1 = 5;
                var ret1 = [];
                var start1;

                start1 = $scope.currentPage1;
                if (start1 > $scope.pageCount1() - rangeSize1) {
                    start1 = $scope.pageCount1() - rangeSize1 + 1;
                }

                for (var i = start1; i < start1 + rangeSize1; i++) {
                    if (i >= 0)
                        ret1.push(i);
                }

                return ret1;
            }

            $scope.range2 = function () {
                var rangeSize2 = 5;
                var ret2 = [];
                var start2;

                start2 = $scope.currentPage2;
                if (start2 > $scope.pageCount2() - rangeSize2) {
                    start2 = $scope.pageCount2() - rangeSize2 + 1;
                }

                for (var i = start2; i < start2 + rangeSize2; i++) {
                    if (i >= 0)
                        ret2.push(i);
                }
                return ret2;
            }

            $scope.range3 = function () {
                var rangeSize3 = 5;
                var ret3 = [];
                var start3;

                start3 = $scope.currentPage3;
                if (start3 > $scope.pageCount3() - rangeSize3) {
                    start3 = $scope.pageCount3() - rangeSize3 + 1;
                }

                for (var i = start3; i < start3 + rangeSize3; i++) {
                    if (i >= 0)
                        ret3.push(i);
                }
                return ret3;
            }

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPage1 = function () {
                if ($scope.currentPage1 > 0) {
                    $scope.currentPage1--;
                }
            }

            $scope.prevPage2 = function () {
                if ($scope.currentPage2 > 0) {
                    $scope.currentPage2--;
                }
            }

            $scope.prevPage3 = function () {
                if ($scope.currentPage3 > 0) {
                    $scope.currentPage3--;
                }
            }

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";

            };

            $scope.prevPageDisabled1 = function () {
                return $scope.currentPage1 === 0 ? "disabled" : "";
            };

            $scope.prevPageDisabled2 = function () {
                return $scope.currentPage2 === 0 ? "disabled" : "";
            };

            $scope.prevPageDisabled3 = function () {
                return $scope.currentPage3 === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {
                return Math.ceil($scope.dash.length / $scope.itemsPerPage) - 1;
            };

            $scope.pageCount1 = function () {
                return Math.ceil($scope.view.length / $scope.itemsPerPage1) - 1;
            };

            $scope.pageCount2 = function () {
                return Math.ceil($scope.screen_datails.length / $scope.itemsPerPage2) - 1;
            };

            $scope.pageCount3 = function () {
                return Math.ceil($scope.Upload_doc_datails.length / $scope.itemsPerPage3) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPage1 = function () {
                if ($scope.currentPage1 < $scope.pageCount1()) {
                    $scope.currentPage1++;
                }
            };

            $scope.nextPage2 = function () {
                if ($scope.currentPage2 < $scope.pageCount2()) {
                    $scope.currentPage2++;
                }
            };

            $scope.nextPage3 = function () {
                if ($scope.currentPage3 < $scope.pageCount3()) {
                    $scope.currentPage3++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.nextPageDisabled1 = function () {
                return $scope.currentPage1 === $scope.pageCount1() ? "disabled" : "";

            };

            $scope.nextPageDisabled2 = function () {
                return $scope.currentPage2 === $scope.pageCount2() ? "disabled" : "";
            };

            $scope.nextPageDisabled3 = function () {
                return $scope.currentPage3 === $scope.pageCount3() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.setPage1 = function (n) {
                $scope.currentPage1 = n;
            };

            $scope.setPage2 = function (n) {
                $scope.currentPage2 = n;
            };

            $scope.setPage3 = function (n) {
                $scope.currentPage3 = n;
            };

            /*End Region*/

            $scope.Cancel = function () {
                $scope.view = "";
            }

            $scope.link = function (str) {
                console.log($scope.edt);
                $state.go("main.Sim010", { admission_num: str, Class: $scope.edt });
            }

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                console.log($scope.obj2);
            });

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (res) {
                $scope.obj1 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/getRegistration").then(function (res) {
                $scope.reg = res.data;
            });

            $scope.GetGrade = function (cur, acad_yr) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&ac_year=" + acad_yr).then(function (res) {
                    $scope.obj3 = res.data;
                    console.log($scope.obj3);
                });
            }

            $scope.GetGrade(param.curr_code, param.academic_year);

            //Get Dashboard Details

            $scope.GetInfo = function (cur_code, AcadmicYear, gradeCode, reg) {
                // $scope.url = $rootScope.globals.currentSchool.lic_website_url;
                if (reg == '1') {

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.dashTiles = res.data;
                        console.log($scope.dashTiles);

                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetStudents?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=W").then(function (res) {
                        $scope.dash = res.data;
                        console.log($scope.dash);
                    });
                }
                if (reg == '2') {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.dashTiles = res.data;
                        console.log($scope.dashTiles);


                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetStudents?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=R").then(function (res) {
                        $scope.dash = res.data;
                        console.log($scope.dash);

                    });
                }
            }

            $scope.GetInfo(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.check1 = function (dash) {
                var v = document.getElementById(dash.admission_number);
                if (v.checked == true) {
                    if (dash.curr_code == '' || dash.academic_year == '' || dash.grade_code == '' || dash.section_code == '' || dash.fee_category == '') {
                        $rootScope.strMessage = "Please Check Whether Section and Fee Category are Selected for this Students";
                        $('#message').modal({ backdrop: 'static', keyboard: false });
                        v.checked = false;
                        var index = del.indexOf(dash.admission_number);
                        console.log(index);

                        if (index > -1) {
                            del.splice(index, 3);
                        }
                        console.log(del);
                    }
                    else {
                        del.push(dash.admission_number, dash.section_code, dash.fee_category);
                        console.log(del);
                        $scope.row1 = '';
                    }
                }
                else {
                    console.log(dash.admission_number);
                    v.checked = false;
                    var index = del.indexOf(dash.admission_number);
                    console.log(index);

                    if (index > -1) {
                        del.splice(index, 3);
                    }
                    console.log(del);
                }
            }

            $scope.check = function () {
                console.log($scope.dash);
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.dash.length; i++) {
                        var v = document.getElementById($scope.dash[i].admission_number);
                        if ($scope.dash[i].curr_code == '' || $scope.dash[i].academic_year == '' || $scope.dash[i].grade_code == '' || $scope.dash[i].section_code == '' || $scope.dash[i].fee_category == '') {
                            v.checked = false;
                            var index = del.indexOf($scope.dash[i].admission_number);
                            console.log(index);

                            if (index > -1) {
                                del.splice(index, 3);
                            }
                            console.log(del);
                        }
                        else {
                            v.checked = true;
                            del.push($scope.dash[i].admission_number, $scope.dash[i].section_code, $scope.dash[i].fee_category);
                            console.log(del);
                            $scope.row1 = '';
                        }
                        //var t = $scope.dash[i].admission_number;
                        //var v = document.getElementById(t);
                        //v.checked = true;
                        //del.push(t, $scope.dash[i].section_code, $scope.dash[i].fee_category);
                        //$scope.row1 = 'row_selected';
                    }
                    console.log(del);
                }
                else {
                    for (var i = 0; i < $scope.dash.length; i++) {
                        var t = $scope.dash[i].admission_number;
                        var v = document.getElementById(t, $scope.dash[i].section_code, $scope.dash[i].fee_category);
                        v.checked = false;
                        del.pop(t);
                        $scope.row1 = '';
                    }
                    console.log(del);
                }
            }

            //Promote the Admissions
            $scope.getPromote = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash.length; i++) {
                    var t = $scope.dash[i].admission_number;
                    //  var sec = $scope.dash[i].section_code;
                    //  var fee_cat = $scope.dash[i].fee_category;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        admdetails = admdetails + $scope.dash[i].admission_number + ',';
                    }
                }
                console.log(admdetails);
                if (admdetails.length == 0) {
                    $rootScope.strMessage = "Nothing Selected";
                    $('#message').modal({ backdrop: 'static', keyboard: false });

                }
                else {
                    console.log($scope.dash);
                    //console.log(sec);
                    //console.log(fee_cat);
                    // if ($scope.dash.fee_category !=undefined && $scope.dash.section_code !=undefined)
                    // {
                    var data = $scope.edt;
                    data.admission_number = admdetails;
                    console.log(data);
                    if (admdetails.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/Admission/ApproveMultiple", data).then(function (res) {
                            $scope.promote = res.data;
                            console.log($scope.promote);
                            $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                            main.checked = false;
                        });
                    }
                    //}
                    //else
                    //{
                    //    $rootScope.strMessage = "Please Check Whether Section and Fee of Selected Students";
                    //    $('#message').modal({ backdrop: 'static', keyboard: false });
                    //}
                }

            }

            //Reject the Admissions
            $scope.getReject = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash.length; i++) {
                    var t = $scope.dash[i].admission_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        admdetails = admdetails + $scope.dash[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    $rootScope.strMessage = "Please select atleast 1 applicant";
                    $('#message').modal({ backdrop: 'static', keyboard: false });
                }
                else {
                    $rootScope.strMessage = "The selected applications will be rejected";
                    $('#dashModal').modal({ backdrop: 'static', keyboard: false });
                }
            }

            $scope.OkRejectadm = function () {
                var data = $scope.edt;
                data.admission_number = admdetails;
                data.status = 'R';

                $http.post(ENV.apiUrl + "api/common/Admission/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {
                        $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                    }
                });
            }

            //Show_Tile_Details
            $scope.getViewDetails = function (datails, cur_code, acad_yr, grade) {
                //var data = $scope.edt;
                //console.log(str);
                if (datails == "Applicants") {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesViewDetails?curr_code=" + cur_code + "&AcadmicYear=" + acad_yr + "&gradeCode=" + grade + "&admission_status=null").then(function (res) {
                        $scope.view = res.data;
                        console.log($scope.view);

                        $scope.grid1 = true;
                        $scope.t = true;
                        $('#viewModal').modal({ backdrop: 'static', keyboard: false });
                    });
                }
                if (datails == "Approved") {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesViewDetails?curr_code=" + cur_code + "&AcadmicYear=" + acad_yr + "&gradeCode=" + grade + "&admission_status=C").then(function (res) {
                        $scope.view = res.data;
                        console.log($scope.view);
                        $scope.grid1 = true;
                        $scope.t = true;

                        $('#viewModal').modal({ backdrop: 'static', keyboard: false });
                    });
                }
                if (datails == "Rejected") {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesViewDetails?curr_code=" + cur_code + "&AcadmicYear=" + acad_yr + "&gradeCode=" + grade + "&admission_status=R").then(function (res) {
                        $scope.view = res.data;
                        console.log($scope.view);
                        $scope.grid1 = true;
                        $scope.t = true;
                        //  $scope.str = 'view_data';
                        // str = $scope.str;

                        $('#viewModal').modal({ backdrop: 'static', keyboard: false });
                    });
                }
                if (datails == 'Waiting') {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesViewDetails?curr_code=" + cur_code + "&AcadmicYear=" + acad_yr + "&gradeCode=" + grade + "&admission_status=W").then(function (res) {
                        $scope.view = res.data;
                        console.log($scope.view);
                        $scope.grid1 = true;
                        $scope.t = true;
                        // $scope.str = 'view_data';
                        //  str = $scope.str;

                        $('#viewModal').modal({ backdrop: 'static', keyboard: false });
                    });
                }
            }

            //Upload_document
            $scope.UploadDocument = function (adm_no) {
                //  $scope.admission_no = adm_no;
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetCriteriaName?admission_num=" + adm_no).then(function (res) {
                    // $scope.Upload_doc_datails = res.data;
                    for (var i = 0; i < res.data.length; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }

                    console.log($scope.Upload_doc_datails);
                    $scope.Upload_doc = true;
                    $scope.edt.sims_admission_doc_path1 = "NO";
                    $('#UploadDocModal').modal({ backdrop: 'static', keyboard: false });
                });

            }

            //screeningMarks
            $scope.screeningMarks = function (adm_no) {
                //$scope.admission_no = adm_no;

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetcriteriaMarks?admission_num=" + adm_no).then(function (res) {
                    $scope.screen_datails = res.data;
                    console.log($scope.screen_datails);
                    $scope.Screening = true;
                    $scope.t = true;
                    $('#screeningModal').modal({ backdrop: 'static', keyboard: false });
                });
            }

            //Update_screening_details
            $scope.screeningModal_Cancel = function () {
                var data = $scope.screen_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_UpdateAdmissionMarks", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);
                    if ($scope.msg1.messageType > 0) {
                        swal({ title: "Admission Marks", text: "'" + $scope.msg1.strMessage + "'", imageUrl: "assets/img/notification-success.png", },
                        function () {
                            $('#screeningModal').modal('hide');
                            $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                        });
                    }


                    // $scope.screeningMarks($scope.screen_datails.admis_num);
                    //// $rootScope.strMessage = $scope.msg1.strMessage;
                    //// console.log($scope.msg1);
                    //// $('#message').modal('show');
                });


            }

            /*start Upload*/

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[i].size > 200000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 200Kb.", imageUrl: "assets/img/notification-alert.png", });
                    }
                    if ($scope.edt1.count > 2) {
                        // swal({ title: "Alert", text: "File Size Should Not Exceed 2.", imageUrl: "assets/img/notification-alert.png", });
                    }
                });

            };

            $scope.file_changed = function (element, str) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                if (element.files[0].size < 200000) {
                    if ($scope.edt1.count < 2) {
                        console.log('xxxx');
                        if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                            $scope.edt1.count = $scope.edt1.count;
                        }
                        else {
                            $scope.edt1.count = ($scope.edt1.count) + 1;
                        }

                        // if ($scope.edt1.count < 2)
                        {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + 'api/file1/uploadDocument?filename=' + $scope.edt1.admis_num + '_' + $scope.edt1.sims_criteria_code + '_' + $scope.edt1.count + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/Student",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                //alert(d);

                                var data = {
                                    admis_num: $scope.edt1.admis_num,
                                    sims_criteria_code: $scope.edt1.sims_criteria_code,
                                    sims_admission_doc_path: d,
                                    sims_admission_doc_path_old: $scope.edt1.sims_admission_doc_path
                                }


                                if (data.sims_admission_doc_path != null || data.sims_admission_doc_path != "") {
                                    if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                                        console.log($scope.edt1.count);
                                        data.sims_admission_doc_status = 'false';
                                        data.sims_admission_doc_verify = 'false';
                                        data.opr = 'N';

                                        console.log(data);

                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Update_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ title: "Alert", text: "Document Updated Successfully.", imageUrl: "assets/img/notification-alert.png", });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                    else {
                                        console.log($scope.edt1.count);
                                        console.log('sss');
                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Insert_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ title: "Alert", text: "Document Uploaded Successfully.", imageUrl: "assets/img/notification-alert.png", });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                }

                            });
                        }
                        //else
                        //{
                        //    swal({ title: "Alert", text: "File Size Should Not Exceed 2", imageUrl: "assets/img/notification-alert.png", });
                        //}
                    }
                    else {
                        swal({ title: "Alert", text: "You Can Upload Max 2 File Documents For Each Criteria", imageUrl: "assets/img/notification-alert.png", });

                    }

                }
            };



            $scope.add_upload_doc = function () {
                var data = $scope.Upload_doc_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Update_Admission_DocList", data).then(function (res) {
                    $scope.ins = res.data;
                    if (res.data) {
                        //  swal({ title: "Alert", text: "Document Updated Successfully.", imageUrl: "assets/img/notification-alert.png", });
                        //$scope.UploadDocument($scope.admission_no);
                    }
                    //  console.log('kkkk');
                    $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });
            }


            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.edt.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.doc_delete = function (doc_path, crit_code, adm_no) {
                console.log(doc_path + '-' + crit_code + '-' + adm_no);


                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Delete_Admission_Doc?adm_no=" + adm_no + "&criteria_code=" + crit_code + "&doc_path=" + doc_path).then(function (res) {
                    $scope.del = res.data;
                    if (res.data) {
                        swal({ title: "Alert", text: "Document Deleted Successfully.", imageUrl: "assets/img/notification-alert.png", });
                        //$scope.UploadDocument($scope.admission_no);
                    }
                    $scope.UploadDocument(adm_no);
                    // $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });
            }


            /*End Upload*/

        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();