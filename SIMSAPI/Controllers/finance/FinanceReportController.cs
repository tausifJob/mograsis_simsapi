﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.FinnCommonReport;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.finance
{
     [RoutePrefix("api/FinanceReportRoute")]
    public class FinanceReportController : ApiController
    {
        // GET: FinanceReport
         [Route("GetFamilyAnalysisDetails")]
         public HttpResponseMessage GetFamilyAnalysisDetails(string acad_year, string from,string rpt_status,string with_pdc)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_family_analysis_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@rpt_status", rpt_status),
                            new SqlParameter("@with_pdc", with_pdc)
                            
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.sltr_tran_amt_dr = float.Parse(dr["sltr_tran_amt_dr"].ToString());
                             simsobj.sltr_tran_amt_cr = float.Parse(dr["sltr_tran_amt_cr"].ToString());
                             simsobj.pdc_amount = float.Parse(dr["pdc_amount"].ToString());
                             simsobj.father_name = dr["father_name"].ToString();
                             simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                             simsobj.net_amt = float.Parse(dr["net_amt"].ToString());
                             simsobj.perc_amt = float.Parse(dr["perc_amt"].ToString());
                             simsobj.parent_contact_number = dr["parent_contact_number"].ToString();
                             simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             simsobj.student_name = dr["student_name"].ToString();
                             simsobj.class1 = dr["class"].ToString();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();      
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetFamilyAnalysisDetails_New")]
         public HttpResponseMessage GetFamilyAnalysisDetails_New(string acad_year, string from, string rpt_status, string with_pdc)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_family_analysis_new_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@rpt_status", rpt_status),
                            new SqlParameter("@with_pdc", with_pdc)
                            
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.sltr_tran_amt_dr = float.Parse(dr["sltr_tran_amt_dr"].ToString());
                             simsobj.sltr_tran_amt_cr = float.Parse(dr["sltr_tran_amt_cr"].ToString());
                             simsobj.pdc_amount = float.Parse(dr["pdc_amount"].ToString());
                             simsobj.father_name = dr["father_name"].ToString();
                             simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                             simsobj.net_amt = float.Parse(dr["net_amt"].ToString());
                             simsobj.perc_amt = float.Parse(dr["perc_amt"].ToString());
                             simsobj.parent_contact_number = dr["parent_contact_number"].ToString();
                             simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             simsobj.student_name = dr["student_name"].ToString();
                             simsobj.class1 = dr["class"].ToString();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                             try {
                                 simsobj.sims_remark = dr["sims_remark"].ToString();
                                 simsobj.fee_status_desc = dr["fee_status_desc"].ToString();
                             }
                             catch(Exception e){}
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }


         [Route("GetFamilyAnalysisDetails_New_studentwise")]
         public HttpResponseMessage GetFamilyAnalysisDetails_New(string acad_year, string from, string rpt_status, string with_pdc, string sims_parent_number)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_family_analysis_new_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'G'),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@rpt_status", rpt_status),
                            new SqlParameter("@with_pdc", with_pdc),
                            new SqlParameter("@sims_parent_number", sims_parent_number),
                            
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.sltr_tran_amt_dr = float.Parse(dr["sltr_tran_amt_dr"].ToString());
                             simsobj.sltr_tran_amt_cr = float.Parse(dr["sltr_tran_amt_cr"].ToString());
                             simsobj.pdc_amount = float.Parse(dr["pdc_amount"].ToString());
                             simsobj.father_name = dr["father_name"].ToString();
                             simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                             simsobj.net_amt = float.Parse(dr["net_amt"].ToString());
                             simsobj.perc_amt = float.Parse(dr["perc_amt"].ToString());
                             simsobj.parent_contact_number = dr["parent_contact_number"].ToString();
                             simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             simsobj.student_name = dr["student_name"].ToString();
                             simsobj.class1 = dr["class"].ToString();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                             try
                             {
                                 simsobj.sims_remark = dr["sims_remark"].ToString();
                                 simsobj.fee_status_desc = dr["fee_status_desc"].ToString();
                             }
                             catch (Exception e) { }
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("postgettransportfeedetails")]
         public HttpResponseMessage postgettransportfeedetails(FIN01 itsr)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
             List<FIN01> goaltarget_list = new List<FIN01>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();

                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_transport_fee_payment_details_new_Fee_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@view", itsr.view),
                                new SqlParameter("@report_type", itsr.report_type),
                                new SqlParameter("@period_no", itsr.period_no),
                                new SqlParameter("@cur_code", itsr.cur_code),
                                new SqlParameter("@acad_year", itsr.acad_year),
                                new SqlParameter("@grade_code", itsr.grade_code),
                                new SqlParameter("@section_code", itsr.section_code),
                                new SqlParameter("@vehicle_code", itsr.vehicle_code),
                                new SqlParameter("@transport_status", itsr.transport_status),
                                new SqlParameter("@search", itsr.search),
                             });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 itsrobj = new FIN01();
                             itsrobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             itsrobj.student_name = dr["student_name"].ToString();
                             itsrobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                             itsrobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             itsrobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                             itsrobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             itsrobj.sims_transport_effective_from = dr["sims_transport_effective_from"].ToString();
                             itsrobj.sims_transport_effective_upto = dr["sims_transport_effective_upto"].ToString();
                             itsrobj.sims_fee_expected = Convert.ToDouble(dr["sims_fee_expected"].ToString());
                             itsrobj.sims_fee_concession = Convert.ToDouble(dr["sims_fee_concession"].ToString());
                             itsrobj.sims_fee_paid_amount = Convert.ToDouble(dr["sims_fee_paid_amount"].ToString());
                             itsrobj.sims_fee_concession_paid = Convert.ToDouble(dr["sims_fee_concession_paid"].ToString());
                         }
                     }
                 }

             }
             catch (Exception x)
             {
               //  Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("GetFeeFrequency")]
         public HttpResponseMessage GetFeeFrequency(string opr)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_transport_fee_payment_details_new_Fee_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", opr),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetPeriod")]
         public HttpResponseMessage GetPeriod(string view,string cur_code,string acad_year)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_transport_fee_payment_details_new_Fee_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@view", view),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetGrade")]
         public HttpResponseMessage GetGrade(string cur_code, string acad_year, string report_type)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_transport_fee_payment_details_new_Fee_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@report_type", report_type),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetSection")]
         public HttpResponseMessage GetSection(string cur_code, string acad_year,string grade,string report_type)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_transport_fee_payment_details_new_Fee_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@report_type", report_type),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             simsobj.sims_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetNextYearStud")]
         public HttpResponseMessage GetNextYearStud(string cur_code, string acad_year)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<FIN01> doc_list = new List<FIN01>();
           
             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_student_not_register_for_next_year",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FIN01 simsobj = new FIN01();
                             string str1 = "";
                             str1 = dr["sims_parent_number"].ToString();
                             var v = from p in doc_list where p.sims_parent_number == str1 select p;

                             simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                             simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
                             simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                             simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();
                             simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                             simsobj.number_of_sibling = dr["number_of_sibling"].ToString();
                             simsobj.stud_list = new List<studlist>();

                             studlist h = new studlist();

                             h.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             h.student_name = dr["student_name"].ToString();
                             h.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             h.sims_section_name_en = dr["sims_section_name_en"].ToString();

                             if (v.Count() == 0)
                             {
                                 simsobj.stud_list.Add(h);
                                 doc_list.Add(simsobj);
                             }
                             else
                             {
                                 v.ElementAt(0).stud_list.Add(h);
                             }
                             

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("getAllVoucherForPrint")]
         public HttpResponseMessage getAllVoucherForPrint(string comp_code, string from_date, string to_date, string search)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";
             List<FINR15> com_list = new List<FINR15>();
             Message message = new Message();
             try
             {
                 if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                 {
                     from_date = null;
                 }
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_print_vouchers_with_doc_date_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FINR15 simsobj = new FINR15();

                             simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                             simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                             simsobj.gltd_post_date = (dr["gltd_post_date"].ToString());
                             simsobj.gltd_doc_date = (dr["gltd_doc_date"].ToString());
                             simsobj.pty_name = dr["pty_name"].ToString();
                             simsobj.voucher_amt = dr["voucher_amt"].ToString();

                             simsobj.gltd_verify_user = dr["gltd_verify_user"].ToString();
                             simsobj.gltd_verify_date = dr["gltd_verify_date"].ToString();
                             simsobj.gltd_authorize_user = dr["gltd_authorize_user"].ToString();
                             simsobj.gltd_authorize_date = dr["gltd_authorize_date"].ToString();
                             simsobj.gltd_prepare_date = dr["gltd_authorize_date"].ToString();
                             simsobj.gltd_prepare_user = dr["gltd_prepare_user"].ToString();
                             simsobj.voucher_amt = dr["voucher_amt"].ToString();

                             com_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, com_list);
                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, com_list);
                 }
             }
             catch (Exception x)
             {
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }
         }
    }
}