﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/GlCostCentre")]
    public class GlCostCentreMappingController : ApiController
    {
        [Route("getGlCostCentreDetail")]
        public HttpResponseMessage getGlCostCentreDetail(string comp_code, string year)
        {
            List<Fin220> com_list = new List<Fin220>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_cost_center]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@glco_comp_code", comp_code),
                            new SqlParameter("@glco_year",year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin220 obj = new Fin220();
                            obj.glco_comp_code = dr["glco_comp_code"].ToString();
                            obj.glco_year = dr["glco_year"].ToString();

                            obj.codp_dept_no = dr["glco_dept_no"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();

                            obj.glac_acct_code = dr["glco_acct_code"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();

                            obj.glco_op_bal_amt = dr["glco_op_bal_amt"].ToString();

                            obj.glco_cost_centre_code = dr["glco_cost_centre_code"].ToString();
                            obj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            if (dr["glco_status"].ToString() == "A")
                                obj.glco_status = true;
                            else
                                obj.glco_status = false;
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getDepartmentDetail")]
        public HttpResponseMessage getDepartmentDetail(string year, string comp_code)
        {
            List<Fin220> com_list = new List<Fin220>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_cost_center]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new  SqlParameter("@glco_year", year),
                             new SqlParameter("@glco_comp_code", comp_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin220 obj = new Fin220();
                            obj.codp_comp_code = dr["codp_comp_code"].ToString();
                            obj.codp_dept_no = dr["codp_dept_no"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();
                            obj.codp_dept_type = dr["codp_dept_type"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getAccountCodeDetail")]
        public HttpResponseMessage getAccountCodeDetail(string dep_code, string comp_code, string year)
        {
            List<Fin220> com_list = new List<Fin220>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_cost_center]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                            new SqlParameter("@glco_dept_no",dep_code),
                             new SqlParameter("@glco_comp_code", comp_code),
                             new  SqlParameter("@glco_year", year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin220 obj = new Fin220();
                            obj.glma_acct_code = dr["glma_acct_code"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            obj.glma_dept_no = dr["glma_dept_no"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getCentreCodeDetail")]
        public HttpResponseMessage getCentreCodeDetail(string comp_code, string year)
        {
            List<Fin220> com_list = new List<Fin220>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_cost_center]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "X"),
                             new SqlParameter("@glco_comp_code", comp_code),
                             new  SqlParameter("@glco_year", year),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin220 obj = new Fin220();
                            obj.coce_cost_centre_code = dr["coce_cost_centre_code"].ToString();
                            obj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("GlCostCentreDetailCUD")]
        public HttpResponseMessage GlCostCentreDetailCUD(List<Fin220> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin220 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_gl_cost_center",
                            new List<SqlParameter>()

                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@glco_comp_code", simsobj.comp_code),
                                 new SqlParameter("@glco_year", simsobj.finance_year),
                                 new SqlParameter("@glco_dept_no", simsobj.codp_dept_no),
                                 new SqlParameter("@glco_acct_code", simsobj.glac_acct_code),
                                 new SqlParameter("@glco_cost_centre_code", simsobj.coce_cost_centre_code),
                                 new SqlParameter("@codp_dept_no_old", simsobj.codp_dept_no_old1),
                                 new SqlParameter("@glac_acct_code_old", simsobj.glac_acct_code_old1),
                                 new SqlParameter("@glco_op_bal_amt", simsobj.money),
                                 new SqlParameter("@glco_status", simsobj.glco_status==true?"A":"I")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}