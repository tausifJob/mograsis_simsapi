﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
     [RoutePrefix("api/common/AssetLiteral")]

    public class AssetLiteralController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("Get_Asset_Literals")]
         public HttpResponseMessage Get_Asset_Literals(string comp_code,string finance_year)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Asset_Literals(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "STUDENT", "Get_Asset_Literals"));

             List<Finn006> fins_list = new List<Finn006>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_literals_proc]",
                         new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@gal_comp_code", comp_code),
                            new SqlParameter("@financialyear", finance_year)
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Finn006 finnobj = new Finn006();
                             finnobj.gal_comp_code = dr["gal_comp_code"].ToString();
                             finnobj.gal_comp_code_name = dr["gal_comp_code_name"].ToString();
                             finnobj.gal_asst_type = dr["gal_asst_type"].ToString();
                             //finnobj.gal_asst_type_name = dr["gal_asset_type_name"].ToString();
                             finnobj.gal_rec_type = dr["gal_rec_type"].ToString();
                             finnobj.gal_rec_type_name = dr["gal_rec_type_name"].ToString();
                             finnobj.gal_type_desc = dr["gal_type_desc"].ToString();
                             finnobj.gal_asset_acno = dr["gal_asset_acno"].ToString();
                             finnobj.gal_asset_acno_name =dr["gal_asset_acno_name"].ToString();
                             finnobj.gal_deprn_acno = dr["gal_deprn_acno"].ToString();
                             finnobj.gal_deprn_acno_name =dr["gal_deprn_acno_name"].ToString();
                             finnobj.gal_resrv_acno = dr["gal_resrv_acno"].ToString();
                             finnobj.gal_resrv_acno_name =dr["gal_resrv_acno_name"].ToString();
                             finnobj.gal_deprn_percent = dr["gal_deprn_percent"].ToString();

                            fins_list.Add(finnobj);
                         }
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, fins_list);
             }
             catch (Exception e)
             {
                 Log.Error(e);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_list);
             }
         }

         [Route("GetAllAssetLiteralsReceiptType")]
         public HttpResponseMessage GetAllAssetLiteralsReceiptType()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllAssetLiteralsReceiptType(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "STUDENT", "GetAllAssetLiteralsReceiptType"));

             List<Finn006> fins_list = new List<Finn006>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_literals_proc]",
                         new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "T"),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Finn006 finnobj = new Finn006();
                             finnobj.gal_rec_type = dr["fins_appl_parameter"].ToString();
                             finnobj.gal_rec_type_name = dr["fins_appl_form_field_value1"].ToString();
                             fins_list.Add(finnobj);
                         }
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, fins_list);
             }
             catch (Exception e)
             {
                 Log.Error(e);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_list);
             }
         }

         [Route("GetAllAccountCodes")]
         public HttpResponseMessage GetAllAccountCodes(string comp_code)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllAccountCodes(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "STUDENT", "GetAllAccountCodes"));

             List<Finn006> fins_list = new List<Finn006>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_literals_proc]",
                         new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@gal_comp_code", comp_code)
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Finn006 finnobj = new Finn006();
                             finnobj.gal_asset_acno = dr["glac_acct_code"].ToString();
                             finnobj.gal_asset_acno_name = dr["glac_acct_code"].ToString() + " - " + dr["glac_name"].ToString();
                             finnobj.gal_deprn_acno = dr["glac_acct_code"].ToString();
                             finnobj.gal_deprn_acno_name = dr["glac_acct_code"].ToString() + " - " + dr["glac_name"].ToString();
                             finnobj.gal_resrv_acno = dr["glac_acct_code"].ToString();
                             finnobj.gal_resrv_acno_name = dr["glac_acct_code"].ToString() + " - " + dr["glac_name"].ToString();
                             fins_list.Add(finnobj);
                         }
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, fins_list);
             }
             catch (Exception e)
             {
                 Log.Error(e);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_list);
             }
         }

         [Route("CUDInsert_Asset_Literals")]
         public HttpResponseMessage CUDInsert_Asset_Literals(List<Finn006> data)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsert_Asset_Literals(),PARAMETERS :: No";
             Log.Debug(string.Format(debug, "STUDENT", "CUDInsert_Asset_Literals"));

             Message message = new Message();
             bool inserted = false;
             try
             {
                 if (data != null)
                 {
                     using (DBConnection db = new DBConnection())
                     {
                         db.Open();
                         foreach (Finn006 finnobj in data)
                         {
                             SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_literals_proc]",
                                 new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@gal_comp_code", finnobj.gal_comp_code),
                                new SqlParameter("@gal_asst_type", finnobj.gal_asst_type),
                                new SqlParameter("@gal_rec_type", finnobj.gal_rec_type),
                                new SqlParameter("@gal_type_desc", finnobj.gal_type_desc),
                                new SqlParameter("@gal_asset_acno", finnobj.gal_asset_acno),
                                new SqlParameter("@gal_deprn_acno", finnobj.gal_deprn_acno),
                                new SqlParameter("@gal_resrv_acno", finnobj.gal_resrv_acno),
                                new SqlParameter("@gal_deprn_percent", finnobj.gal_deprn_percent),
                             });
                                 while (dr.Read())
                                 {
                                     string cnt = dr["param_cnt"].ToString();
                                     if (cnt == "1")
                                     {
                                         inserted = true;
                                     }
                                     else
                                     {
                                         inserted = false;
                                     }
                                 }
                            
                         }

                     }
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, inserted);
                 }
             }
             catch (Exception x)
             {
                 return Request.CreateResponse(HttpStatusCode.OK, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }

         [Route("CUDUpdate_Asset_Literals")]
         public HttpResponseMessage CUDUpdate_Asset_Literals(List<Finn006> data)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdate_Asset_Literals(),PARAMETERS :: No";
             Log.Debug(string.Format(debug, "STUDENT", "CUDUpdate_Asset_Literals"));

             Message message = new Message();
             bool inserted = false;
             try
             {
                 if (data != null)
                 {
                    if(data[0].gal_deprn_percent=="" || data[0].gal_deprn_percent == "undefined")
                    {
                        data[0].gal_deprn_percent = null;
                    }
                     using (DBConnection db = new DBConnection())
                     {
                         db.Open();
                         foreach (Finn006 finnobj in data)
                         {
                             int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_asset_literals_proc]",
                                 new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@gal_comp_code", finnobj.gal_comp_code),
                                new SqlParameter("@gal_asst_type", finnobj.gal_asst_type),
                                new SqlParameter("@gal_rec_type", finnobj.gal_rec_type),
                                new SqlParameter("@gal_type_desc", finnobj.gal_type_desc),
                                new SqlParameter("@gal_asset_acno", finnobj.gal_asset_acno),
                                new SqlParameter("@gal_deprn_acno", finnobj.gal_deprn_acno),
                                new SqlParameter("@gal_resrv_acno", finnobj.gal_resrv_acno),
                                new SqlParameter("@gal_deprn_percent", finnobj.gal_deprn_percent),
                             });
                             if (ins > 0)
                             {
                                 inserted = true;
                             }
                             else
                             {
                                 inserted = false;
                             }
                         }

                     }
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, inserted);
                 }
             }
             catch (Exception x)
             {
                 return Request.CreateResponse(HttpStatusCode.OK, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }
      
    }
}