﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/LedgerCont")]
    public class LedgerCntrlController:ApiController
    {

        [Route("getLedgerCntrl")]
        public HttpResponseMessage getLedgerCntrl(string sllc_comp_code, string finance_year)
        {
            List<fins069> ldgrcont_list = new List<fins069>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_ledger_control_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sllc_comp_code", sllc_comp_code),
                            new SqlParameter("@sllc_year", finance_year)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins069 obj = new fins069();
                            obj.sllc_comp_code = dr["sllc_comp_code"].ToString();
                            obj.company_name1 = dr["company_name"].ToString();
                            obj.sllc_year = dr["sllc_year"].ToString();

                            obj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            obj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            obj.ldgr_name = dr["ldgr_name"].ToString();
                            obj.sllc_ldgr_type = dr["sllc_ldgr_type"].ToString();
                            obj.sllc_earlst_pstng_date = dr["sllc_earlst_pstng_date"].ToString();
                            obj.sllc_match_type = dr["sllc_match_type"].ToString();
                            obj.sllc_match_type_desc = dr["sllc_match_type_desc"].ToString();
                            ldgrcont_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ldgrcont_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ldgrcont_list);
        }

        [Route("LedgerControlCUD")]
        public HttpResponseMessage LedgerControlCUD(List<fins069> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LedgerControlCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins069 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_ledger_control_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sllc_comp_code",simsobj.sllc_comp_code),
                          new SqlParameter("@sllc_year",simsobj.sllc_year),

                          new SqlParameter("@sllc_ldgr_code", simsobj.sllc_ldgr_code),
                          new SqlParameter("@sllc_ldgr_type", simsobj.sllc_ldgr_type),
                          new SqlParameter("@sllc_ldgr_name", simsobj.sllc_ldgr_name),
                          new SqlParameter("@sllc_earlst_pstng_date", simsobj.sllc_earlst_pstng_date),
                          new SqlParameter("@sllc_match_type", simsobj.sllc_match_type),

                          new SqlParameter("@sllc_prd_no", simsobj.sllc_prd_no),
                          new SqlParameter("@sllc_password", simsobj.sllc_password),


                          new SqlParameter("@sllc_curcy_flag",simsobj.sllc_curcy_flag==true?"Y":"N"),
                          new SqlParameter("@sllc_qty_flag",simsobj.sllc_qty_flag==true?"Y":"N"),
                          new SqlParameter("@sllc_yob_flag",simsobj.sllc_yob_flag==true?"Y":"N")



                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAccountCode")]
        public HttpResponseMessage getAccountCode(string comp_code, string finance_year)
        {
            List<fins069> Account_list = new List<fins069>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_ledger_acc_control_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'N'),
                            new SqlParameter("@slac_comp_code", comp_code),
                            new SqlParameter("@slac_year", finance_year)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins069 obj = new fins069();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            obj.slac_ctrl_acno = dr["glma_acct_code"].ToString();

                            Account_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Account_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Account_list);
        }


        [Route("getLdgrAccountControl")]
        public HttpResponseMessage getLdgrAccountControl(string ledger_code, string slac_comp_code, string finance_year)
        {
            List<fins069> ldgrAccountcont_list = new List<fins069>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_ledger_acc_control_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@slac_ldgr_code", ledger_code),
                            new SqlParameter("@slac_comp_code", slac_comp_code),
                            new SqlParameter("@slac_year", finance_year)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins069 obj = new fins069();
                            obj.slac_comp_code = dr["slac_comp_code"].ToString();
                            obj.company_name = dr["company_name"].ToString();
                            obj.slac_ldgr_code = dr["slac_ldgr_code"].ToString();
                            obj.slac_year = dr["slac_year"].ToString();
                            obj.slac_cntrl_class = dr["slac_cntrl_class"].ToString();
                            obj.slac_cntrl_class_desc = dr["slac_cntrl_class_desc"].ToString();
                            obj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            obj.slac_sales_clr_acno = dr["slac_sales_clr_acno"].ToString();

                            ldgrAccountcont_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ldgrAccountcont_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ldgrAccountcont_list);
        }

        [Route("LdgrAccountCntrlCUD")]
        public HttpResponseMessage LdgrAccountCntrlCUD(List<fins069> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LedgerControlCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins069 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_ledger_acc_control_proc]",
                            new List<SqlParameter>()
                            {
                               new SqlParameter("@opr",simsobj.opr),

                               new SqlParameter("@slac_comp_code", simsobj.slac_comp_code),
                               new SqlParameter("@slac_year", simsobj.slac_year),
                               new SqlParameter("@slac_ldgr_code", simsobj.slac_ldgr_code),
                               new SqlParameter("@slac_cntrl_class", simsobj.slac_cntrl_class),
                               new SqlParameter("@slac_cntrl_class_desc", simsobj.slac_cntrl_class_desc),
                               new SqlParameter("@old_slac_cntrl_class", simsobj.old_slac_cntrl_class),
                               new SqlParameter("@slac_ctrl_acno", simsobj.slac_ctrl_acno),
                               new SqlParameter("@slac_sales_clr_acno", simsobj.slac_sales_clr_acno),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}