﻿using System;
using System.Collections.Generic;
using System.Net;
using log4net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/AssetAllocation")]
    public class AssetAllocationController : ApiController
    {
        [Route("Get_Asset_Type_Finn218")]
        public HttpResponseMessage Get_Asset_Type_Finn218(string comp_code)
        {
            List<Finn218> com_list = new List<Finn218>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_asset_allocation]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@opr_upd", "AL"),
                              new SqlParameter("@gal_comp_code", comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn218 FinnObj = new Finn218();
                            FinnObj.gal_comp_code = dr["gal_comp_code"].ToString();
                            FinnObj.gal_asst_type = dr["gal_asst_type"].ToString();
                            FinnObj.gal_type_desc = dr["gal_type_desc"].ToString();
                            com_list.Add(FinnObj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_Asset_Location_Finn218")]
        public HttpResponseMessage Get_Asset_Location_Finn218(string comp_code)
        {
            List<Finn218> com_list = new List<Finn218>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_asset_allocation]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@opr_upd", "AML"),
                              new SqlParameter("@gal_comp_code", comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn218 FinnObj = new Finn218();
                            FinnObj.gam_location = dr["gam_location"].ToString();
                            com_list.Add(FinnObj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_Asset_Department_Finn218")]
        public HttpResponseMessage Get_Asset_Department_Finn218(string financialyear, string comp_code)
        {
            List<Finn218> com_list = new List<Finn218>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_asset_allocation]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@opr_upd", "DEP"),
                              new SqlParameter("@Year", financialyear),
                              new SqlParameter("@gal_comp_code", comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn218 FinnObj = new Finn218();
                            FinnObj.codp_dept_no = dr["codp_dept_no"].ToString();
                            FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();
                            FinnObj.deptno_fullname = dr["deptno_fullname"].ToString();
                            com_list.Add(FinnObj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_AllAsset_Item_Finn218")]
        public HttpResponseMessage Get_AllAsset_Item_Finn218(string asset_type, string location, string dept_no,string comp_code)
        {
            List<Finn218> com_list = new List<Finn218>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_allocation_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@opr_upd", "AM"),
                              new SqlParameter("@gam_dept_code", dept_no),
                              new SqlParameter("@gal_asst_type", asset_type),
                              new SqlParameter("@gam_location", location),
                              new SqlParameter("@gal_comp_code", comp_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn218 FinnObj = new Finn218();
                            FinnObj.gam_item_no = dr["gam_item_no"].ToString();
                            FinnObj.gam_desc_1 = dr["gam_desc_1"].ToString();
                            if (!string.IsNullOrEmpty(DateTime.Now.Date.ToString()))
                                FinnObj.fal_from_date = db.UIDDMMYYYYformat(Convert.ToDateTime(DateTime.Now.Date.ToString()).ToShortDateString());
                            if (!string.IsNullOrEmpty(DateTime.Now.Date.ToString()))
                                FinnObj.fal_upto_date = db.UIDDMMYYYYformat(Convert.ToDateTime(DateTime.Now.Date.ToString()).ToShortDateString());
                            //FinnObj.fal_from_date = DateTime.Now.Date.ToString();
                            //FinnObj.fal_upto_date = DateTime.Now.Date.ToString();
                            string chk = dr["gam_status"].ToString();
                            if (chk == "A" || chk == "1")
                            {
                                FinnObj.gam_status = true;
                            }
                            else
                            {
                                FinnObj.gam_status = false;
                            }
                            com_list.Add(FinnObj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Insert_Asset_Location")]
        public HttpResponseMessage Insert_Asset_Location(List<Finn218> data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn218 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_asset_allocation_proc]",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@opr_upd", "I"),
                        new SqlParameter("@fal_item_no", finnobj.gam_item_no),
                        new SqlParameter("@fal_user_code", finnobj.emp_code),
                        new SqlParameter("@fal_from_date", db.DBYYYYMMDDformat(finnobj.fal_from_date)),
                        new SqlParameter("@fal_upto_date", db.DBYYYYMMDDformat(finnobj.fal_upto_date)),
                        //new SqlParameter("@fal_status", finnobj.fal_status==true?"A":"I"),
                       
                    });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("searchEmployeeList")]
        public HttpResponseMessage searchEmployeeList()
        {
            List<Finn218> empDetail = new List<Finn218>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn218 obj = new Finn218();

                            obj.em_login_code = dr["em_login_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.fullname = dr["fullname"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();
                            obj.em_email = dr["em_email"].ToString();
                            obj.em_status = dr["em_status"].ToString();

                            empDetail.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDetail);
        }
    }
}
