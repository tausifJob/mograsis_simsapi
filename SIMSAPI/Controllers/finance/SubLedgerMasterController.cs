﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/common/SubLedgerMaster")]

    public class SubLedgerMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
         
        string compCode = string.Empty;
        decimal financialyear = 0;

       
        [Route("GetCurrentFinancialYear")]
        public HttpResponseMessage GetCurrentFinancialYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCurrentFinancialYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetCurrentFinancialYear"));

            List<Finn016> mod_list = new List<Finn016>();
            Finn016 finsobj = new Finn016();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //finsobj.financialyear = dr["fins_appl_form_field_value1"].ToString();
                            financialyear = Convert.ToDecimal(dr["Year"].ToString());
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, financialyear);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, financialyear);
            }
        }

        [Route("GetAllLedgerCodes")]
        public HttpResponseMessage GetAllLedgerCodes(string comp_code,string fin_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLedgerCodes(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllLedgerCodes"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sl_period_ledgers_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@slpr_comp_code",comp_code),
                                new SqlParameter("@slpr_year",fin_yr)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_ldgrctl_code = dr["sllc_ldgr_code"].ToString();
                            finnobj.slma_ldgrctl_name = dr["sllc_ldgr_code"].ToString() + "-" + dr["sllc_ldgr_type"].ToString();
                            finnobj.slma_xref_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            finnobj.slma_xref_ldgr_name = dr["sllc_ldgr_code"].ToString() + "-" + dr["sllc_ldgr_type"].ToString();
                            try
                            {
                                finnobj.sllc_ldgr_name = dr["sllc_ldgr_code"].ToString() + "-" + dr["sllc_ldgr_name"].ToString();
                            }
                            catch(Exception e) {}
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetForeignCurrency")]
        public HttpResponseMessage GetForeignCurrency(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetForeignCurrency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetForeignCurrency"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sl_period_ledgers_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B"),
                                 new SqlParameter("@slpr_comp_code",comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_curcy_code = dr["excg_curcy_code"].ToString();
                            finnobj.slma_curcy_name = dr["excg_curcy_desc"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllPartyType")]
        public HttpResponseMessage GetAllPartyType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllPartyType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllPartyType"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_pty_type = dr["fins_appl_parameter"].ToString();
                            finnobj.slma_pty_type_name = dr["fins_appl_form_field_value1"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllAddressType")]
        public HttpResponseMessage GetAllAddressType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllAddressType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllAddressType"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_addr_type = dr["fins_appl_parameter"].ToString();
                            finnobj.slma_addr_type_name = dr["fins_appl_form_field_value1"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllPartyAddressId")]
        public HttpResponseMessage GetAllPartyAddressId()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllPartyAddressId(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllPartyAddressId"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Q")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.coad_xref_addr_id = dr["coad_addr_id"].ToString();
                            finnobj.coad_xref_addr_name = dr["coad_pty_short_name"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllBanks")]
        public HttpResponseMessage GetAllBanks()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllBanks"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "R")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_pty_bank_id = dr["pb_bank_code"].ToString();
                            finnobj.slma_pty_bank_name = dr["pb_bank_code"].ToString() + "-" + dr["pb_bank_name"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllRegion")]
        public HttpResponseMessage GetAllRegion()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllRegion(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllRegion"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "V")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.coad_region_code = dr["sims_region_code"].ToString();
                            finnobj.coad_region_name = dr["sims_region_name_en"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllCountry")]
        public HttpResponseMessage GetAllCountry(string region)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllCountry(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllCountry"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (region != null)
                    {
                        region = "";
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "W"),
                                 new SqlParameter("@coad_region_code",region),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_ctry_code = dr["sims_country_code"].ToString();
                            finnobj.slma_ctry_name = dr["sims_country_name_en"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllPartyControlClass")]
        public HttpResponseMessage GetAllPartyControlClass(string comp_code,string fin_yr,string ledger_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllPartyControlClass(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllPartyControlClass"));

            List<Finn016> mod_list = new List<Finn016>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (ledger_code == null)
                    {
                        ledger_code = "";
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@slma_comp_code", comp_code),
                                 new SqlParameter("@slma_ldgrctl_code",ledger_code),
                                new SqlParameter("@slma_ldgrctl_year",fin_yr)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_cntrl_class = dr["slac_cntrl_class"].ToString();
                            finnobj.slma_acno_dept_no = dr["slac_ctrl_acno"].ToString().Substring(1, 2);
                            finnobj.slma_cntrl_class_name = dr["slac_cntrl_class_desc"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllSLAccountNos")]
        public HttpResponseMessage GetAllSLAccountNos(string comp_code, string financial_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSLAccountNos(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSLAccountNos"));

            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "N"),
                                new SqlParameter("@slma_comp_code",comp_code),
                                new SqlParameter("@slma_ldgrctl_year",financial_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_xref_acno = dr["slma_acno"].ToString();
                            finnobj.coad_pty_full_name = dr["coad_pty_full_name"].ToString() + "-" + finnobj.slma_xref_acno;
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllSLAccountNos")]
        public HttpResponseMessage GetAllSLAccountNos(string comp_code, string financial_year, string ctl_code, string acc_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSLAccountNos(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSLAccountNos"));

            List<Finn016> mod_list = new List<Finn016>();
            bool flag = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@slma_comp_code",comp_code),
                                new SqlParameter("@slma_ldgrctl_year",financial_year),
                                new SqlParameter("@slma_ldgrctl_code",ctl_code),
                                new SqlParameter("@slma_acno",acc_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            flag = true;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getCheck_SL_acno")]
        public HttpResponseMessage getCheck_SL_acno(string comp_code, string financial_year, string ctl_code, string acc_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCheck_SL_acno(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getCheck_SL_acno"));
            bool flag = false;
            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y"),
                                new SqlParameter("@slma_comp_code",comp_code),
                                new SqlParameter("@slma_ldgrctl_year",financial_year),
                                new SqlParameter("@slma_ldgrctl_code",ctl_code),
                                new SqlParameter("@slma_acno",acc_no),
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            flag = true;
                        }
                       
                    }
                    else
                    {
                        flag = false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, flag);
            }
        }

        [Route("Get_Subledger_Master")]
        public HttpResponseMessage Get_Subledger_Master(string lgr_cd, string search_text, string comp_code, string financial_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Subledger_Master(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "Get_Subledger_Master"));
          
            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //if (search_text == "undefined")
                    //{
                    //    search_text = null;
                    //}

                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@opr_ins", ""),
                                new SqlParameter("@slma_comp_code", comp_code),
                                new SqlParameter("@slma_ldgrctl_year", financial_year),
                                new SqlParameter("@slma_ldgrctl_code", lgr_cd),
                                new SqlParameter("@fins_appl_code1", CommonStaticClass.Finn016_fins_appl_code1),
                                new SqlParameter("@fins_appl_code2", CommonStaticClass.Finn016_fins_appl_code2),
                                new SqlParameter("@fins_appl_form_field_addr_type", CommonStaticClass.Finn016_fins_appl_form_field_addr_type),
                                new SqlParameter("@fins_appl_form_field_party_class", CommonStaticClass.Finn016_fins_appl_form_field_party_class),
                                new SqlParameter("@fins_appl_form_field_party_type", CommonStaticClass.Finn016_fins_appl_form_field_party_type),
                                new SqlParameter("@search_text", search_text),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_comp_name = dr["slma_comp_name"].ToString();
                            finnobj.slma_comp_code = dr["slma_comp_code"].ToString();
                            finnobj.slma_ldgrctl_year = dr["slma_ldgrctl_year"].ToString();
                            finnobj.slma_ldgrctl_code = dr["slma_ldgrctl_code"].ToString();
                            finnobj.slma_ldgrctl_name = dr["slma_ldgrctl_code"].ToString() + "-" + dr["slma_ldgrctl_name"].ToString();
                            finnobj.slma_acno = dr["slma_acno"].ToString();
                            finnobj.slma_addr_id = dr["slma_addr_id"].ToString();
                            finnobj.slma_addr_type = dr["slma_addr_type"].ToString();
                            finnobj.slma_addr_type_name = dr["slma_addr_type_name"].ToString();
                            if (dr["slma_status"].ToString().Equals("A"))
                                finnobj.slma_status = true;
                            else
                                finnobj.slma_status = false;
                            finnobj.slma_cntrl_class = dr["slma_cntrl_class"].ToString();
                            finnobj.slma_cntrl_class_name = dr["slma_cntrl_class_name"].ToString();
                            finnobj.slma_pty_type = dr["slma_pty_type"].ToString();
                            finnobj.slma_pty_type_name = dr["slma_pty_type_name"].ToString();
                            finnobj.slma_ctry_code = dr["slma_ctry_code"].ToString();
                            finnobj.slma_ctry_name = dr["slma_ctry_name"].ToString();
                            finnobj.slma_xref_ldgr_code = dr["slma_xref_ldgr_code"].ToString();
                            finnobj.slma_xref_ldgr_name = dr["slma_xref_ldgr_code"].ToString() + "-" + dr["slma_xref_ldgr_name"].ToString();
                            finnobj.slma_xref_acno = dr["slma_xref_acno_name"].ToString() + "-" + dr["slma_xref_acno"].ToString();
                            if (dr["slma_fc_flag"].ToString().Equals("Y"))
                                finnobj.slma_fc_flag = true;
                            else
                                finnobj.slma_fc_flag = false;
                            if (dr["slma_qty_flag"].ToString().Equals("Y"))
                                finnobj.slma_qty_flag = true;
                            else
                                finnobj.slma_qty_flag = false;
                            if (dr["slma_remove_tran"].ToString().Equals("Y"))
                                finnobj.slma_remove_tran = true;
                            else
                                finnobj.slma_remove_tran = false;
                            if (string.IsNullOrEmpty(dr["slma_yob_amt"].ToString()) == false)
                                finnobj.slma_yob_amt = Decimal.Parse(dr["slma_yob_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_outstg_dr_amt"].ToString()) == false)
                                finnobj.slma_outstg_dr_amt = Decimal.Parse(dr["slma_outstg_dr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_outstg_cr_amt"].ToString()) == false)
                                finnobj.slma_outstg_cr_amt = Decimal.Parse(dr["slma_outstg_cr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_for_curcy_bal_amt"].ToString()) == false)
                                finnobj.slma_for_curcy_bal_amt = Decimal.Parse(dr["slma_for_curcy_bal_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_tdy_dr_amt"].ToString()) == false)
                                finnobj.slma_tdy_dr_amt = Decimal.Parse(dr["slma_tdy_dr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_tdy_cr_amt"].ToString()) == false)
                                finnobj.slma_tdy_cr_amt = Decimal.Parse(dr["slma_tdy_cr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_cr_limit"].ToString()) == false)
                                finnobj.slma_cr_limit = Decimal.Parse(dr["slma_cr_limit"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_loan_inst_amt"].ToString()) == false)
                                finnobj.slma_loan_inst_amt = Decimal.Parse(dr["slma_loan_inst_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_orig_loan_amt"].ToString()) == false)
                                finnobj.slma_orig_loan_amt = Decimal.Parse(dr["slma_orig_loan_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_ovrid_amt"].ToString()) == false)
                                finnobj.slma_ovrid_amt = Decimal.Parse(dr["slma_ovrid_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_bank_clos_bal_amt"].ToString()) == false)
                                finnobj.slma_bank_clos_bal_amt = Decimal.Parse(dr["slma_bank_clos_bal_amt"].ToString());

                            finnobj.slma_curcy_code = dr["slma_curcy_code"].ToString();
                            finnobj.slma_curcy_name = dr["slma_curcy_name"].ToString();
                            // finnobj.slma_cash_crd_ind = Decimal.Parse(dr["slma_cash_crd_ind"].ToString());
                            if (dr["slma_cash_crd_ind"].ToString().Equals("1"))
                            {
                                finnobj.slma_cash_crd_ind = true;
                            }
                            else
                            {
                                finnobj.slma_cash_crd_ind = false;
                            }
                            if (dr["slma_stop_cr_ind"].ToString().Equals("1"))
                            {
                                finnobj.slma_stop_cr_ind = true;
                            }
                            else
                            {
                                finnobj.slma_stop_cr_ind = false;
                            }
                            finnobj.slma_pty_bank_acno = dr["slma_pty_bank_acno"].ToString();
                            if (!string.IsNullOrEmpty(dr["slma_bank_stmt_date"].ToString()))
                                finnobj.slma_bank_stmt_date = DateTime.Parse(dr["slma_bank_stmt_date"].ToString()).ToShortDateString();
                            else
                                finnobj.slma_bank_stmt_date = "";
                            if (!string.IsNullOrEmpty(dr["slma_pmt_start_date"].ToString()))
                                finnobj.slma_pmt_start_date = dr["slma_pmt_start_date"].ToString();
                            else
                                finnobj.slma_pmt_start_date = "";
                            if (!string.IsNullOrEmpty(dr["slma_doc_date"].ToString()))
                                finnobj.slma_doc_date = dr["slma_doc_date"].ToString();
                            else
                                finnobj.slma_doc_date = "";
                            finnobj.slma_pty_bank_id = dr["slma_pty_bank_id"].ToString();
                            finnobj.slma_pty_bank_name = dr["slma_pty_bank_id"].ToString() + "-" + dr["slma_pty_bank_name"].ToString();
                            finnobj.slma_loan_inst_type = dr["slma_loan_inst_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_yob_amt"].ToString()))
                                finnobj.slma_credit_prd = int.Parse(dr["slma_credit_prd"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_ovrid_prd"].ToString()))
                                finnobj.slma_ovrid_prd = int.Parse(dr["slma_ovrid_prd"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_stmt_prd"].ToString()))
                                finnobj.slma_stmt_prd = int.Parse(dr["slma_stmt_prd"].ToString());
                            finnobj.slma_sched_code = dr["slma_sched_code"].ToString();
                            finnobj.slma_sched_name = dr["slma_sched_name"].ToString();
                            finnobj.slma_qty_code = dr["slma_qty_code"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_last_pmt_date"].ToString()))
                                finnobj.slma_last_pmt_date = dr["slma_last_pmt_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_last_pmt_amt"].ToString()))
                                finnobj.slma_last_pmt_amt = Decimal.Parse(dr["slma_last_pmt_amt"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_int_rate"].ToString()))
                                finnobj.slma_int_rate = Decimal.Parse(dr["slma_int_rate"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_int_prd"].ToString()))
                                finnobj.slma_int_prd = int.Parse(dr["slma_int_prd"].ToString());
                            finnobj.slma_term_name = dr["slma_term_name"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_outstg_dr_amt_orig"].ToString()))
                                finnobj.slma_outstg_dr_amt_orig = Decimal.Parse(dr["slma_outstg_dr_amt_orig"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_outstg_cr_amt_orig"].ToString()))
                                finnobj.slma_outstg_cr_amt_orig = Decimal.Parse(dr["slma_outstg_cr_amt_orig"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_yob_amt_orig"].ToString()))
                                finnobj.slma_yob_amt_orig = Decimal.Parse(dr["slma_yob_amt_orig"].ToString());
                            if (!string.IsNullOrEmpty(dr["slma_reval_date"].ToString()))
                                finnobj.slma_reval_date = dr["slma_reval_date"].ToString();
                            else
                                finnobj.slma_reval_date = "";
                            finnobj.slma_cntrl_class_org = dr["slma_cntrl_class_org"].ToString();
                            finnobj.slma_credit_remark = dr["slma_credit_remark"].ToString();

                            finnobj.coad_dept_no = dr["coad_dept_no"].ToString();
                            finnobj.coad_xref_addr_id = dr["coad_xref_addr_id"].ToString();
                            finnobj.coad_xref_addr_name = dr["coad_xref_addr_name"].ToString();
                            finnobj.coad_pty_full_name = dr["coad_pty_full_name"].ToString();
                            finnobj.coad_pty_short_name = dr["coad_pty_short_name"].ToString();
                            finnobj.coad_alt_key = dr["coad_alt_key"].ToString();
                            finnobj.coad_pty_arab_name = dr["coad_pty_arab_name"].ToString();
                            finnobj.coad_line_1 = dr["coad_line_1"].ToString();
                            finnobj.coad_line_2 = dr["coad_line_2"].ToString();
                            finnobj.coad_po_box = dr["coad_po_box"].ToString();
                            finnobj.coad_tel_no = dr["coad_tel_no"].ToString();
                            finnobj.coad_fax = dr["coad_fax"].ToString();
                            finnobj.coad_telex = dr["coad_telex"].ToString();
                            finnobj.coad_region_code = dr["coad_region_code"].ToString();
                            finnobj.coad_region_name = dr["coad_region_name"].ToString();
                            finnobj.coad_class_level1 = dr["coad_class_level1"].ToString();
                            finnobj.coad_class_level2 = dr["coad_class_level2"].ToString();
                            finnobj.coad_class_level3 = dr["coad_class_level3"].ToString();
                            finnobj.coad_class_level4 = dr["coad_class_level4"].ToString();
                            finnobj.coad_class_level5 = dr["coad_class_level5"].ToString();
                            finnobj.coad_sms_no = dr["coad_sms_no"].ToString();
                            finnobj.coad_email = dr["coad_email"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDInsertSubLedgerMaster")]
        public HttpResponseMessage CUDInsertSubLedgerMaster(List<Finn016> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubLedgerMaster(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "SubLedgerMaster"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn016 finnobj in data)
                        {
                            if (finnobj.slma_pmt_start_date == "")
                                finnobj.slma_pmt_start_date = null;
                            SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_sblgr_masters_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@opr_ins",finnobj.opr_ins),
                                new SqlParameter("@slma_comp_code",finnobj.slma_comp_code),
                                new SqlParameter("@slma_ldgrctl_year", finnobj.financialyear),
                                new SqlParameter("@slma_ldgrctl_code", finnobj.slma_ldgrctl_code),
                                new SqlParameter("@slma_acno", finnobj.slma_acno),
                                new SqlParameter("@slma_addr_id", finnobj.slma_addr_id),
                                new SqlParameter("@slma_addr_type", finnobj.slma_addr_type),
                                new SqlParameter("@slma_status", finnobj.slma_status==true?'A':'I'),
                                new SqlParameter("@slma_cntrl_class", finnobj.slma_cntrl_class),
                                new SqlParameter("@slma_pty_type", finnobj.slma_pty_type),
                                new SqlParameter("@slma_ctry_code", finnobj.slma_ctry_code),
                                new SqlParameter("@slma_xref_ldgr_code ", finnobj.slma_xref_ldgr_code),
                                new SqlParameter("@slma_xref_acno", finnobj.slma_xref_acno),
                                new SqlParameter("@slma_fc_flag", finnobj.slma_fc_flag==true?'Y':'N'),
                                new SqlParameter("@slma_qty_flag", finnobj.slma_qty_flag==true?'Y':'N'),
                                new SqlParameter("@slma_remove_tran", finnobj.slma_remove_tran==true?'Y':'N'),
                                new SqlParameter("@slma_cr_limit", finnobj.slma_cr_limit),
                                new SqlParameter("@slma_curcy_code", finnobj.slma_curcy_code),
                                new SqlParameter("@slma_pty_bank_acno", finnobj.slma_pty_bank_acno),
                                new SqlParameter("@slma_pty_bank_id", finnobj.slma_pty_bank_id),
                                new SqlParameter("@slma_pmt_start_date", db.DBYYYYMMDDformat(finnobj.slma_pmt_start_date)),
                                new SqlParameter("@slma_credit_prd", finnobj.slma_credit_prd),
                                new SqlParameter("@slma_sched_code", finnobj.slma_sched_code),
                                new SqlParameter("@coad_dept_no", finnobj.coad_dept_no),
                                new SqlParameter("@coad_xref_addr_id", finnobj.coad_xref_addr_id),
                                new SqlParameter("@coad_pty_full_name", finnobj.coad_pty_full_name),
                                new SqlParameter("@coad_pty_short_name", finnobj.coad_pty_short_name),
                                new SqlParameter("@coad_alt_key", finnobj.coad_alt_key),
                                new SqlParameter("@coad_pty_arab_name", finnobj.coad_pty_arab_name),
                                new SqlParameter("@coad_line_1", finnobj.coad_line_1),
                                new SqlParameter("@coad_line_2", finnobj.coad_line_2),
                                new SqlParameter("@coad_po_box", finnobj.coad_po_box),
                                new SqlParameter("@coad_tel_no", finnobj.coad_tel_no),
                                new SqlParameter("@coad_fax", finnobj.coad_fax),
                                new SqlParameter("@coad_telex", finnobj.coad_telex),
                                new SqlParameter("@coad_region_code", finnobj.coad_region_code),
                                new SqlParameter("@coad_class_level1", finnobj.coad_class_level1),
                                new SqlParameter("@coad_class_level2", finnobj.coad_class_level2),
                                new SqlParameter("@coad_class_level3", finnobj.coad_class_level3),
                                new SqlParameter("@coad_class_level4", finnobj.coad_class_level4),
                                new SqlParameter("@coad_class_level5", finnobj.coad_class_level5),
                                new SqlParameter("@coad_sms_no", finnobj.coad_sms_no),
                                new SqlParameter("@coad_email", finnobj.coad_email),
                             });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDUpdateSubLedgerMaster")]
        public HttpResponseMessage CUDUpdateSubLedgerMaster(List<Finn016> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubLedgerMaster(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "SubLedgerMaster"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn016 finnobj in data)
                        {
                            if (finnobj.slma_pmt_start_date == "")
                                finnobj.slma_pmt_start_date = null;
                            int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_sblgr_masters_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@opr_ins",finnobj.opr_ins),
                                new SqlParameter("@slma_comp_code",finnobj.slma_comp_code),
                                new SqlParameter("@slma_ldgrctl_year", finnobj.financialyear),
                                new SqlParameter("@slma_ldgrctl_code", finnobj.slma_ldgrctl_code),
                                new SqlParameter("@slma_acno", finnobj.slma_acno),
                                new SqlParameter("@slma_addr_id", finnobj.slma_addr_id),
                                new SqlParameter("@slma_addr_type", finnobj.slma_addr_type),
                                new SqlParameter("@slma_status", finnobj.slma_status==true?'A':'I'),
                                new SqlParameter("@slma_cntrl_class", finnobj.slma_cntrl_class),
                                new SqlParameter("@slma_pty_type", finnobj.slma_pty_type),
                                new SqlParameter("@slma_ctry_code", finnobj.slma_ctry_code),
                                new SqlParameter("@slma_xref_ldgr_code ", finnobj.slma_xref_ldgr_code),
                                new SqlParameter("@slma_xref_acno", finnobj.slma_xref_acno),
                                new SqlParameter("@slma_fc_flag", finnobj.slma_fc_flag==true?'Y':'N'),
                                new SqlParameter("@slma_qty_flag", finnobj.slma_qty_flag==true?'Y':'N'),
                                new SqlParameter("@slma_remove_tran", finnobj.slma_remove_tran==true?'Y':'N'),
                                new SqlParameter("@slma_cr_limit", finnobj.slma_cr_limit),
                                new SqlParameter("@slma_curcy_code", finnobj.slma_curcy_code),
                                new SqlParameter("@slma_pty_bank_acno", finnobj.slma_pty_bank_acno),
                                new SqlParameter("@slma_pty_bank_id", finnobj.slma_pty_bank_id),
                                new SqlParameter("@slma_pmt_start_date", finnobj.slma_pmt_start_date),
                                new SqlParameter("@slma_credit_prd", finnobj.slma_credit_prd),
                                new SqlParameter("@slma_sched_code", finnobj.slma_sched_code),
                                new SqlParameter("@coad_dept_no", finnobj.coad_dept_no),
                                new SqlParameter("@coad_xref_addr_id", finnobj.coad_xref_addr_id),
                                new SqlParameter("@coad_pty_full_name", finnobj.coad_pty_full_name),
                                new SqlParameter("@coad_pty_short_name", finnobj.coad_pty_short_name),
                                new SqlParameter("@coad_alt_key", finnobj.coad_alt_key),
                                new SqlParameter("@coad_pty_arab_name", finnobj.coad_pty_arab_name),
                                new SqlParameter("@coad_line_1", finnobj.coad_line_1),
                                new SqlParameter("@coad_line_2", finnobj.coad_line_2),
                                new SqlParameter("@coad_po_box", finnobj.coad_po_box),
                                new SqlParameter("@coad_tel_no", finnobj.coad_tel_no),
                                new SqlParameter("@coad_fax", finnobj.coad_fax),
                                new SqlParameter("@coad_telex", finnobj.coad_telex),
                                new SqlParameter("@coad_region_code", finnobj.coad_region_code),
                                new SqlParameter("@coad_class_level1", finnobj.coad_class_level1),
                                new SqlParameter("@coad_class_level2", finnobj.coad_class_level2),
                                new SqlParameter("@coad_class_level3", finnobj.coad_class_level3),
                                new SqlParameter("@coad_class_level4", finnobj.coad_class_level4),
                                new SqlParameter("@coad_class_level5", finnobj.coad_class_level5),
                                new SqlParameter("@coad_sms_no", finnobj.coad_sms_no),
                                new SqlParameter("@coad_email", finnobj.coad_email),
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        private string getCompanyCode()
        {
            try
            {
                compCode = System.Web.HttpContext.Current.Session["CompanyCode"].ToString();
            }
            catch (Exception x)
            {
            }
            return compCode;
        }

    }
}