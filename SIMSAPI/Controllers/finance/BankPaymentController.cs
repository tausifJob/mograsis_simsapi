﻿using System;
using System.Collections.Generic;
using System.Net;
using log4net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/BankPayment")]
    public class BankPaymentController : ApiController
    {
        [Route("getDocCode")]
        public HttpResponseMessage getDocCode(string comp_code, string finance_year)
        {
            List<Fin141> DC = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "W"),
                             new SqlParameter("@gldd_comp_code",comp_code)
                             //new SqlParameter("@fins_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            DC.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC);
        }

        [Route("getLedgerCode")]
        public HttpResponseMessage getLedgerCode()
        {
            List<Fin141> LC = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Z")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            obj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            obj.Full_name = dr["fullname"].ToString();

                            LC.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, LC);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, LC);
        }

        [Route("getBankName")]
        public HttpResponseMessage getBankName(string User_Code)
        {
            List<Fin141> BN = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_sblgr_masters_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "M"),

                            new SqlParameter("@glac_comp_code","1"),
                            new SqlParameter("@glac_year","2016"),
                           // new SqlParameter("@glac_acct_code","BP"),
                            new SqlParameter("@glac_type",User_Code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.glma_acno = dr["glma_acno"].ToString();
                            obj.gdua_dept_no = dr["gdua_dept_no"].ToString();
                            obj.glac_name = dr["glac_name"].ToString();
                            obj.glacnameacno = dr["glma_acno"].ToString() + "-" + dr["glac_name"].ToString();
                            BN.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, BN);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, BN);
        }

        [Route("getEmpName")]
        public HttpResponseMessage getEmpName()
        {
            List<Fin141> EmpName = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "H")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.em_number = dr["em_number"].ToString();
                            obj.Employee_Full_Name = dr["Employee_Full_Name"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.em_dept_code = dr["em_dept_code"].ToString();
                            obj.Empname = dr["Empname"].ToString();
                            EmpName.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, EmpName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, EmpName);
        }

        [Route("Insert_Fins_temp_docs")]
        public HttpResponseMessage Insert_Fins_temp_docs(List<Finn141> data)
        {
            string status = string.Empty;
            string prov_no = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {
                        if (finnobj.gltd_cur_status.Equals("Save"))
                        {
                            status = "Sav";
                        }
                        else if (finnobj.gltd_cur_status.Equals("Verify"))
                        {
                            status = "Vry";
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_docs]",
                       new List<SqlParameter>()
                   {

                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@opr_upd", "AP"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_final_doc_no", finnobj.gltd_final_doc_no),
                                new SqlParameter("@gltd_doc_date",db.DBYYYYMMDDformat(finnobj.gltd_doc_date)),
                                new SqlParameter("@gltd_post_date", db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                                new SqlParameter("@gltd_doc_narr", finnobj.gltd_doc_narr),
                                new SqlParameter("@gltd_remarks", finnobj.gltd_remarks),
                                new SqlParameter("@gltd_doc_flag", ""),
                                new SqlParameter("@gltd_cur_status",status),
                                new SqlParameter("@gltd_prepare_user", finnobj.gltd_prepare_user),
                                new SqlParameter("@gltd_prepare_date", db.DBYYYYMMDDformat(finnobj.gltd_prepare_date)),
                                new SqlParameter("@gltd_verify_user", finnobj.gltd_verify_user),
                                new SqlParameter("@gltd_verify_date", db.DBYYYYMMDDformat(finnobj.gltd_verify_date)),
                                new SqlParameter("@gltd_authorize_user", finnobj.gltd_authorize_user),
                                new SqlParameter("@gltd_authorize_date",db.DBYYYYMMDDformat(finnobj.gltd_authorize_date)),
                                new SqlParameter("@gltd_paid_to", finnobj.gltd_paid_to),
                                new SqlParameter("@gltd_cheque_no", finnobj.gltd_cheque_no),
                                new SqlParameter("@gltd_batch_source", finnobj.gltd_batch_source),
                                new SqlParameter("@gltd_reference_doc_code", finnobj.gltd_reference_doc_code),
                                new SqlParameter("@gltd_reference_final_doc_no", finnobj.gltd_reference_final_doc_no),
                                new SqlParameter("@gltd_payment_date", db.DBYYYYMMDDformat(finnobj.gltd_payment_date)),

                   });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["gltd_prov_doc_no"].ToString()))
                                    prov_no = dr["gltd_prov_doc_no"].ToString();
                            }
                        }

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, prov_no);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, prov_no);
        }


        [Route("Insert_Fins_temp_doc_details")]
        public HttpResponseMessage Insert_Fins_temp_doc_details(List<Finn141> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@gldd_comp_code", finnobj.gldd_comp_code),
                        new SqlParameter("@gldd_doc_code", finnobj.gldd_doc_code),
                        new SqlParameter("@gldd_prov_doc_no", finnobj.gldd_prov_doc_no),
                        new SqlParameter("@gldd_final_doc_no", finnobj.gldd_final_doc_no),
                        new SqlParameter("@gldd_line_no", finnobj.gldd_line_no),
                        new SqlParameter("@gldd_doc_narr", finnobj.gldd_doc_narr),
                        new SqlParameter("@gldd_ledger_code", finnobj.gldd_ledger_code),
                        new SqlParameter("@gldd_acct_code", finnobj.gldd_acct_code),
                        new SqlParameter("@gldd_doc_amount", finnobj.gldd_doc_amount),
                        new SqlParameter("@gldd_fc_code", finnobj.gldd_fc_code),
                        new SqlParameter("@gldd_fc_rate", finnobj.gldd_fc_rate),
                        new SqlParameter("@gldd_fc_amount", finnobj.gldd_fc_amount_debit),
                        new SqlParameter("@gldd_dept_code", finnobj.gldd_dept_code),
                        new SqlParameter("@gldd_party_ref_no", finnobj.gldd_party_ref_no),
                        new SqlParameter("@gldd_party_ref_date",db.DBYYYYMMDDformat((finnobj.gldd_party_ref_date))),
                        new SqlParameter("@pc_bank_from", finnobj.pc_bank_code),
                        new SqlParameter("@gldd_bank_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),               
                        new SqlParameter("@posting_date", db.DBYYYYMMDDformat((finnobj.gltd_post_date))),
                        new SqlParameter("@gldd_cost_center_code", finnobj.gldd_cost_center_code),

                      });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("Insert_JV_Fins_temp_doc_details")]
        public HttpResponseMessage Insert_JV_Fins_temp_doc_details(List<Finn141> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@gldd_comp_code", finnobj.gldd_comp_code),
                        new SqlParameter("@gldd_doc_code", finnobj.gldd_doc_code),
                        new SqlParameter("@gldd_prov_doc_no", finnobj.gldd_prov_doc_no),
                        new SqlParameter("@gldd_final_doc_no", finnobj.gldd_final_doc_no),
                        new SqlParameter("@gldd_line_no", finnobj.gldd_line_no),
                        new SqlParameter("@gldd_doc_narr", finnobj.gldd_doc_narr),
                        new SqlParameter("@gldd_ledger_code", finnobj.gldd_ledger_code),
                        new SqlParameter("@gldd_acct_code", finnobj.gldd_acct_code),
                        new SqlParameter("@gldd_doc_amount",(finnobj.gldd_doc_amount_debit>0)? finnobj.gldd_doc_amount_debit:(0-finnobj.gldd_doc_amount_credit)),
                        new SqlParameter("@gldd_fc_code", finnobj.gldd_fc_code),
                        new SqlParameter("@gldd_fc_rate", finnobj.gldd_fc_rate),
                        new SqlParameter("@gldd_fc_amount", finnobj.gldd_fc_amount_debit),
                        new SqlParameter("@gldd_dept_code", finnobj.gldd_dept_code),
                        new SqlParameter("@gldd_party_ref_no", finnobj.gldd_party_ref_no),
                        new SqlParameter("@gldd_party_ref_date",db.DBYYYYMMDDformat(finnobj.gldd_party_ref_date)),
                        new SqlParameter("@pc_bank_from", finnobj.pc_bank_code),
                        new SqlParameter("@gldd_bank_date", null),
                        new SqlParameter("@posting_date", db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                        new SqlParameter("@gldd_cost_center_code", finnobj.gldd_cost_center_code)


                      });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("GetVerifyAuthorizeUsers")]
        public HttpResponseMessage GetVerifyAuthorizeUsers(string username, string comp_code, string fyear)
        {

            List<string> com_list = new List<string>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_doc_users]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "G"),
                            new SqlParameter("@gltd_authorize_user", username),
                            new SqlParameter("@gldu_comp_code", comp_code),
                            new SqlParameter("@gldu_year", fyear)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            com_list.Add(dr["gldu_verify_user"].ToString());
                            com_list.Add(dr["gldu_authorize_user"].ToString());

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetVerifyAuthorizeUsers_PC")]
        public HttpResponseMessage GetVerifyAuthorizeUsers_PC(string username, string comp_code, string fyear, string doc_code)
        {

            List<string> com_list = new List<string>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_doc_users]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "S"),
                            new SqlParameter("@gltd_authorize_user", username),
                            new SqlParameter("@gldu_doc_code", doc_code),
                            new SqlParameter("@gldu_comp_code", comp_code),
                            new SqlParameter("@gldu_year", fyear)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            com_list.Add(dr["gldu_verify_user"].ToString());
                            com_list.Add(dr["gldu_authorize_user"].ToString());

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("Authorize_Posting")]
        public HttpResponseMessage Authorize_Posting(string comp_code, string doc_code, string prv_no, string auth_user, string auth_date)
        {
            string final_doc_no = string.Empty;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[payment_posting]",
                    new List<SqlParameter>()
                 {

                         new SqlParameter("@gldd_comp_code", comp_code),
                         new SqlParameter("@gldd_doc_code", doc_code),
                         new SqlParameter("@gldd_prov_doc_no", prv_no),
                         new SqlParameter("@gltd_cur_status", "Pos"),
                         new SqlParameter("@gltd_authorize_user", auth_user),
                         new SqlParameter("@gltd_authorize_date", db.DBYYYYMMDDformat(auth_date)),

                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["gldd_final_doc_no"].ToString()))
                                final_doc_no = dr["gldd_final_doc_no"].ToString();
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, final_doc_no);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, final_doc_no);
        }

        //GetLedgerNumber
        [Route("GetLedgerNumber")]
        public HttpResponseMessage GetLedgerNumber(string financialyear, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLedgerNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),
                            new SqlParameter("@Year", financialyear),
                              new SqlParameter("@pb_comp_code", comp_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            simsobj.sllc_ldgr_name = dr["sllc_ldgr_full_name"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }



        //GetGLAccountNumber
        [Route("GetSLAccNumber")]
        public HttpResponseMessage GetSLAccNumber(string pbslcode, string cmp_cd,string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSLAccNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@pb_comp_code",cmp_cd),
                            new SqlParameter("@pb_sl_code",pbslcode),
                            new SqlParameter("@Year",fyear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.gldd_acct_code = dr["slma_acno"].ToString();
                            simsobj.gldd_acct_name = simsobj.gldd_acct_code + "-" + dr["coad_pty_short_name"].ToString();
                            simsobj.gldd_party_ref_no = dr["coad_xref_addr_id"].ToString();
                            simsobj.gldd_dept_code = dr["coad_dept_no"].ToString();
                            simsobj.gldd_dept_name = dr["codp_dept_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetSLAccCodeDetail")]

        public HttpResponseMessage GetSLAccCodeDetail(string ledger_code, string slmano, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSLAccCodeDetail()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@pb_comp_code",comp_code),
                            new SqlParameter("@pb_sl_code",ledger_code),
                            new SqlParameter("@pb_sl_acno",slmano)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.slma_acno = dr["slma_acno"].ToString();
                            simsobj.coad_dept_no = dr["coad_dept_no"].ToString();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.coad_pty_short_name = dr["coad_pty_short_name"].ToString();
                            simsobj.coad_pty_full_name = dr["slma_acno"].ToString() + "-" + dr["coad_pty_short_name"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
        [Route("getBankPayment")]

        public HttpResponseMessage getBankPayment()
        {
            List<Fin141> GetBankPaymentDetail = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                            obj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            obj.gldd_prov_doc_no = dr["gldd_prov_doc_no"].ToString();
                            obj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            obj.gldd_line_no = dr["gldd_line_no"].ToString();
                            obj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                            obj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            obj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            obj.gldd_doc_amount = dr["gldd_doc_amount"].ToString();
                            obj.gldd_fc_code = dr["gldd_fc_code"].ToString();
                            obj.gldd_fc_rate = dr["gldd_fc_rate"].ToString();
                            obj.gldd_fc_amount = dr["gldd_fc_amount"].ToString();
                            obj.gldd_dept_code = dr["[gldd_dept_code"].ToString();
                            obj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();

                            GetBankPaymentDetail.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, GetBankPaymentDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, GetBankPaymentDetail);
        }

        [Route("GetNextfins_doc_prvno")]

        public HttpResponseMessage GetNextfins_doc_prvno(string doc_cd, string financialyear, string cmp_cd)
        {
            List<Fin141> GetBankPaymentDetail = new List<Fin141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Q"),
                            new SqlParameter("@gltd_doc_code",doc_cd),
                            new SqlParameter("@gldc_year",financialyear),
                            new SqlParameter("@gldc_comp_code",cmp_cd)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 obj = new Fin141();
                            obj.next_prov_no = dr["gldc_next_prv_no"].ToString();
                            GetBankPaymentDetail.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, GetBankPaymentDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, GetBankPaymentDetail);
        }

        [Route("GetComp_Name")]
        public HttpResponseMessage GetComp_Name(string username)
        {
            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@tbl_cond", username),
                new SqlParameter("@opr", "A"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 fin006Obj = new Finn102();
                            fin006Obj.comp_name = dr["comp_name"].ToString();
                            fin006Obj.comp_code = dr["comp_code"].ToString();

                            house.Add(fin006Obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("GetFinancial_year")]
        public HttpResponseMessage GetFinancial_year(string comp_code)
        {


            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@opr_mem", comp_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 simsobj = new Finn102();
                            if (!string.IsNullOrEmpty(dr["sims_financial_year"].ToString()))
                                simsobj.financial_year = decimal.Parse(dr["sims_financial_year"].ToString());

                            house.Add(simsobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("SetCompany")]
        public HttpResponseMessage SetCompany(string comp_code, string year)
        {
            System.Web.HttpContext.Current.Session.Add("CompanyCode", comp_code);
            System.Web.HttpContext.Current.Session.Add("Year", year);
            System.Web.HttpContext.Current.Session.Timeout = 20;
            return Request.CreateResponse(HttpStatusCode.OK, true);

        }

        [Route("getCompanyCode")]
        public HttpResponseMessage getCompanyCode()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, System.Web.HttpContext.Current.Session["CompanyCode"].ToString());

            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);

            }
        }

        [Route("UpdateFinancial_year")]
        public HttpResponseMessage UpdateFinancial_year(string comp_cd, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateFinancial_year(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "UpdateAttednace", comp_cd));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("fins_get_data_sp",
                            new List<SqlParameter>()
                            {

                 new SqlParameter("@opr", "U"),
                 new SqlParameter("@opr_mem", comp_cd),
                 new SqlParameter("@tbl_cond", year)

                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllCashBankGLMasterAccountNo")]
        public HttpResponseMessage GetAllCashBankGLMasterAccountNo(string cmp_cd, string financialyear, string doc_cd, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllCashBankGLMasterAccountNo()PARAMETERS ::NA";

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "M"),
                            new SqlParameter("@glac_comp_code", cmp_cd),
                            new SqlParameter("@glac_year", financialyear),
                            new SqlParameter ("@glac_acct_code", doc_cd),
                            new SqlParameter("@glac_type", username)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.master_acno = dr["glma_acno"].ToString();
                            simsobj.master_acno_name = dr["glac_name"].ToString();
                            simsobj.master_ac_cdname = dr["glma_acno"].ToString() + " " + dr["glac_name"].ToString();
                            simsobj.gldd_dept_code = dr["gdua_dept_no"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetAllPetty_CashAccount")]
        public HttpResponseMessage GetAllPetty_CashAccount(string cmp_cd, string fyear, string doc_cd, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllPetty_CashAccount()PARAMETERS ::NA";

            List<Fin227> doc_list = new List<Fin227>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_petty_cash_masters",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@glpc_comp_code", cmp_cd),
                            new SqlParameter("@glpc_year", fyear),
                            new SqlParameter("@glpc_dept_no", doc_cd),
                            new SqlParameter("@login_user", username)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 obj = new Fin227();
                            obj.glpc_number = dr["glpc_number"].ToString();
                            obj.glpc_dept_no = dr["glpc_dept_no"].ToString();
                            obj.glpc_acct_code = dr["glpc_acct_code"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            obj.glpc_max_limit = dr["glpc_max_limit"].ToString();
                            obj.glpc_min_limit = dr["glpc_min_limit"].ToString();
                            obj.glpc_max_limit_per_transaction = dr["glpc_max_limit_per_transaction"].ToString();
                            obj.glpc_curr_balance = dr["glpc_curr_balance"].ToString();
                            if (dr["glpc_cost_center_flag"].ToString() == "A")
                                obj.glpc_cost_center_flag = true;
                            else
                                obj.glpc_cost_center_flag = false;

                            doc_list.Add(obj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetAllGLAccountNos")]
        public HttpResponseMessage GetAllGLAccountNos(string glma_accountcode, string cmpnycode,string fyear,string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllGLAccountNos()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin141> doc_list = new List<Fin141>();
            if (glma_accountcode == "undefined")
                glma_accountcode = null;


            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_gl_masters]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@glma_comp_code",cmpnycode),
                            new SqlParameter("@glma_acct_code",glma_accountcode),
                            new SqlParameter("@glma_year",fyear),
                            new SqlParameter("@fins_appl_code",user)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 FinnObj = new Fin141();
                            FinnObj.gldd_acct_code = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString();//
                            FinnObj.gldd_acct_name = FinnObj.gldd_acct_code + "-" + dr["glma_acct_name"].ToString();
                            FinnObj.gldd_dept_code = dr["glma_dept_no"].ToString();
                            FinnObj.gldd_dept_name = dr["codp_dept_name"].ToString();
                            FinnObj.glma_acct_code = dr["glma_acct_code"].ToString();
                            FinnObj.sltr_yob_amt = decimal.Parse(dr["glma_op_bal_amt"].ToString());
                            doc_list.Add(FinnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getInsertMesage")]
        public HttpResponseMessage getInsertMesage()
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_bills1",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "T"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            message.strMessage = dr["msg"].ToString();

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getCostCenter")]
        public HttpResponseMessage getCostCenter(string aacno, string comp_cd, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLedgerNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Fin152> doc_list = new List<Fin152>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_cost_center]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@glco_comp_code",comp_cd),
                            new SqlParameter("@glco_year",fyear),
                            new SqlParameter("@glco_acct_code",aacno)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin152 simsobj = new Fin152();
                            simsobj.glco_cost_centre_code = dr["glco_cost_centre_code"].ToString();
                            simsobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }




    }
}