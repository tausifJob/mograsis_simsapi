﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;





namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/CumSubSumReport")]

    public class CumulativeSubledgerSummaryReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCompCode")]
        public HttpResponseMessage getCompCode()
        {
            List<CSSR01> lstCuriculum = new List<CSSR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_CumulativeSubledgerSummaryReport]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CSSR01 sequence = new CSSR01();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAllLedgerCode")]
        public HttpResponseMessage getAllLedgerCode(string comp_code )

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<CSSR01> grade_list = new List<CSSR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_CumulativeSubledgerSummaryReport]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@comp_code", comp_code),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CSSR01 simsobj = new CSSR01();
                            simsobj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            simsobj.sllc_ldgr_type = dr["sllc_ldgr_type"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }
               
        [Route("getCummulativeSummaryReport")]
        public HttpResponseMessage getCummulativeSummaryReport(string comp_code, string view_report, string ldgr_code,  string mom_start_date, string mom_end_date)
        {
            List<CSSR01> lstCuriculum = new List<CSSR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_CumulativeSubledgerSummaryReport]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'X'),
                             new SqlParameter("@comp_code", comp_code),
                             new SqlParameter("@view_report", view_report),
                             new SqlParameter("@ldgr_code", ldgr_code),
                             new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(mom_end_date)),
                            // new SqlParameter("@user_number", lecture_count),
                             //new SqlParameter("@value", String.IsNullOrEmpty(lecture_count)?null:lecture_count)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CSSR01 sequence = new CSSR01();
                            sequence.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            sequence.slma_acno = dr["slma_acno"].ToString();
                            sequence.slma_name = dr["slma_name"].ToString();
                            sequence.slma_op_bal_amt = dr["slma_op_bal_amt"].ToString();
                            sequence.slma_tran_amt_dr = dr["slma_tran_amt_dr"].ToString();
                            sequence.slma_tran_amt_cr = dr["slma_tran_amt_cr"].ToString();
                            sequence.slma_close_bal_amt = dr["slma_close_bal_amt"].ToString();
                           
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getViwReport")]
        public HttpResponseMessage getViwReport()

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<CSSR01> grade_list = new List<CSSR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_CumulativeSubledgerSummaryReport]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "R"),
                               

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CSSR01 simsobj = new CSSR01();
                            simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }






    }
}
