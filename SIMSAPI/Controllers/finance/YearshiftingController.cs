﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.FINNANCE;
using SIMSAPI.Models.ERP.messageClass;
using System.Linq;
using System.Data;
namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/yearshit")]
    public class YearshiftingController:ApiController
    {
        [Route("Get_Button_details")]
        public HttpResponseMessage Get_Button_details()
        {
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].Yearshifting_proc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')
                              
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finnyearshifting FinnObj = new Finnyearshifting();
                            FinnObj.comn_rollover_code = dr["comn_rollover_code"].ToString();
                            FinnObj.comn_rollover_desc_en = dr["comn_rollover_desc_en"].ToString(); 
                            FinnObj.comn_rollover_icon_color = dr["comn_rollover_icon_color"].ToString();
                            FinnObj.comn_rollover_proc_name_opr = dr["comn_rollover_proc_name_opr"].ToString();
                            com_list.Add(FinnObj);                      
                       };
                    }
                }

                
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_fee_data_details")]
        public HttpResponseMessage Get_fee_data_details(string cur_code,string academic_year,string Default)
        {
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[sims].Yearshifting_proc",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "SF"),
                               new SqlParameter("@sims_cur_code",cur_code),
                               new SqlParameter("@sims_academic_year", academic_year),
                               new SqlParameter("@Defaulter", Default)
                         });
                     if (dr.HasRows)
                    {
                        while(dr.Read())
                        {
                            decimal i = 0;
                            Finnyearshifting obj = new Finnyearshifting();
                            obj.fee_det = new List<fee_details>();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_cur_code= dr["sims_cur_code"].ToString();
                           string str=  dr["sims_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_enroll_number = str + "";
                            fee_details fd = new fee_details();
                            fd.sims_fee_code = dr["sims_fee_code"].ToString();
                            fd.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            fd.fee_amount = dr["fee_amount"].ToString();
                            i =decimal.Parse(dr["fee_amount"].ToString());
                            obj.total_amount = dr["total"].ToString();
                            fd.fee_total_ammount = dr["fee_total_ammount"].ToString();
                            fd.posting_fee_code = dr["posting_fee_code"].ToString();
                            if ( i> 0)
                            {
                                fd.text_colour = "#FF0000";

                            }
                            else
                            {
                                fd.text_colour = "#576475";

                            }
                            var v = (from p in com_list where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                obj.fee_det.Add(fd);
                                com_list.Add(obj);
                            }
                            else
                            {
                                v.ElementAt(0).fee_det.Add(fd);
                            }
                        }
                    }
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_details")]
        public HttpResponseMessage Get_details(string opr,string sp_name,string comn_rollover_code)
        {
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure(sp_name,
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", opr),
                              new SqlParameter("@comn_rollover_code", comn_rollover_code),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finnyearshifting FinnObj = new Finnyearshifting();
                            FinnObj.comn_rollover_code = dr["comn_rollover_code"].ToString();
                            FinnObj.comn_rollover_desc_en = dr["comn_rollover_desc_en"].ToString();
                            FinnObj.comn_rollover_icon_color = dr["comn_rollover_icon_color"].ToString();
                            FinnObj.comn_rollover_proc_name_opr = dr["comn_rollover_proc_name_opr"].ToString();
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Fee_posting_data")]
        public HttpResponseMessage Fee_posting_data( Finnyearshifting obj)
        {
            bool inserted = false;
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[rollOverFeeModule_createOpeningBalance]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@acadYear", obj.sims_academic_year),
                              new SqlParameter("@curCode", obj.sims_cur_code),
                              new SqlParameter("@rollOverRemark", obj.comn_rollover_process_remark),
                              new SqlParameter("@userCode", obj.user_name),
                              new SqlParameter("@fee_types", obj.sims_fee_code),

                         });
                        if (dr.RecordsAffected>0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    

                    if (inserted == true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[comn].[comn_Rollover_info_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@comn_rollover_code", obj.comn_rollover_code)

                         });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Finnyearshifting obj1 = new Finnyearshifting();
                                obj.comn_rollover_process_remark = dr1["comn_rollover_process_remark"].ToString();
                                obj.comn_rollover_process_remark_status = dr1["comn_rollover_process_remark_status"].ToString();
                                com_list.Add(obj);
                            }
                            dr1.Close();
                        }
                    }
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }
        
        [Route("Get_Academic_Year_Data_Shifting")]
        public HttpResponseMessage Get_Academic_Year_Data_Shifting( string sims_academic_year,string comn_rollover_code)
        {
            bool inserted = false;
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[RollOver_Academics]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@currentAcaYear", sims_academic_year),

                         });
                    if (dr.RecordsAffected>0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                    if (inserted == true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[comn].[comn_Rollover_info_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@comn_rollover_code", comn_rollover_code)

                         });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Finnyearshifting obj = new Finnyearshifting();
                                obj.comn_rollover_process_remark = dr1["comn_rollover_process_remark"].ToString();
                                obj.comn_rollover_process_remark_status =dr1["comn_rollover_process_remark_status"].ToString();
                                com_list.Add(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }
        
        [Route("Get_Academic_Year_Data_Shifting")]
        public HttpResponseMessage Get_inventory_Data_Shifting(string comp_code, string fin_year, string comn_rollover_code)
        {
            bool inserted = false;
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[RollOver_Academics]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@currentcur", comp_code),
                              new SqlParameter("@currentAcaYear", fin_year),

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                    if (inserted == true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[comn].[comn_Rollover_info_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@comn_rollover_code", comn_rollover_code)

                         });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Finnyearshifting obj = new Finnyearshifting();
                                obj.comn_rollover_process_remark = dr1["comn_rollover_process_remark"].ToString();
                                obj.comn_rollover_process_remark_status = dr1["comn_rollover_process_remark_status"].ToString();
                                com_list.Add(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }
        
        [Route("Get_Finance_Data_Shifting")]
        public HttpResponseMessage Get_Finance_Data_Shifting(string comp_code, string fin_year, string comn_rollover_code)
        {
            bool inserted = false;
            List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[RollOver_Finance]",
                        new List<SqlParameter>()
                         {

                              new SqlParameter("@comp_code", comp_code),
                              new SqlParameter("@currentFinYear", fin_year),

                         });
                    if (dr.RecordsAffected>0)
                    {
                        inserted = true;
                    }
                    dr.Close();

                    if(inserted==true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[comn].[comn_Rollover_info_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@comn_rollover_code", comn_rollover_code)

                         });
                        if(dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Finnyearshifting obj = new Finnyearshifting();
                                obj.comn_rollover_process_remark = dr1["comn_rollover_process_remark"].ToString();
                                obj.comn_rollover_process_remark_status = dr1["comn_rollover_process_remark_status"].ToString();
                                com_list.Add(obj);
                            }
                        }
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("Get_year_processing_sp_execution")]
        public HttpResponseMessage Get_year_processing_sp_execution(string opr, string sp_name, string comn_rollover_code)
        {
            string msg = string.Empty;
            Message message = new Message();

           // List<Finnyearshifting> com_list = new List<Finnyearshifting>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure(sp_name,
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", opr),
                              new SqlParameter("@comn_rollover_code", comn_rollover_code),


                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            msg = dr["result"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, msg);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, msg);
                }


            }
            catch (Exception x) {

                message.strMessage = "Error in Excution Process.";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            
        }


    }
}