﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/CreateEditAccount")]
    public class CreateEditAccountCodeController:ApiController
    {
        #region Fins_Account_Codes_Finn001...

        [Route("getScheduleCode")]
        public HttpResponseMessage getGetScheduleCode(string comp_code, string finance_year)
        {
            List<Finn001> schedule_list = new List<Finn001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_account_codes_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@fins_comp_code",comp_code),
                                new SqlParameter("@glac_year",finance_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn001 finnobj = new Finn001();
                            finnobj.glac_sched_desc = dr["glsc_sched_code"].ToString() + "-" + dr["glsc_sched_name"].ToString();
                            finnobj.glac_sched_code = dr["glsc_sched_code"].ToString();
                            schedule_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, schedule_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, schedule_list);
            }
        }

        [Route("getAllAccountTypes")]
        public HttpResponseMessage getAllAccountTypes(string comp_code, string finance_year)
        {
            List<Finn001> type_list = new List<Finn001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_account_codes_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Q"),
                                new SqlParameter("@fins_comp_code",comp_code),
                                new SqlParameter("@glac_year",finance_year)
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn001 finnobj = new Finn001();
                            finnobj.glac_type = dr["fins_appl_form_field_value1"].ToString();
                            finnobj.glac_type_code = dr["fins_appl_parameter"].ToString();
                            type_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }

        [Route("getAllParentAccounts")]
        public HttpResponseMessage getAllParentAccounts(string comp_code, string finance_year)
        {
            List<Finn001> acc_list = new List<Finn001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_account_codes_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@fins_comp_code",comp_code),
                                new SqlParameter("@glac_year",finance_year)
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn001 finnobj = new Finn001();
                            finnobj.glac_parent_acct = dr["glac_acct_code"].ToString() + "-" + dr["glac_name"].ToString();
                            finnobj.glac_parent_acct_code = dr["glac_acct_code"].ToString();
                            finnobj.glac_name = dr["glac_name"].ToString();
                            acc_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acc_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, acc_list);
            }
        }

        [Route("CheckAccountCodes")]
        public HttpResponseMessage CheckAccountCodes(string accountcodes)
        {

            List<Finn001> acc_list = new List<Finn001>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "K"),
                                new SqlParameter("@glac_acct_code", accountcodes),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn001 finnobj = new Finn001();
                            finnobj.glac_parent_acct = dr["glac_acct_code"].ToString() + "-" + dr["glac_name"].ToString();
                            acc_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acc_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, acc_list);
            }
        }

        [Route("getAllFinsAccountCodes")]
        public HttpResponseMessage getAllFinsAccountCodes(string comp_code, string finance_year)
        {
            List<Finn001> code_list = new List<Finn001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_account_codes_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@glac_comp_code", comp_code),
                            new SqlParameter("@fins_comp_code",comp_code),
                            new SqlParameter("@glac_year",finance_year),
                            new SqlParameter("@fins_appl_code", "Fin001"),
                            new SqlParameter("@fins_appl_form_field", "Account Type")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn001 finn = new Finn001();
                            finn.comp_code = dr["glac_comp_code"].ToString();
                            finn.comp_name = dr["comapy_name"].ToString();
                            finn.glac_acct_code = dr["glac_acct_code"].ToString();
                            finn.glac_type = dr["Account_Type"].ToString();
                            finn.glac_name = dr["glac_name"].ToString();
                            finn.glac_sched_code = dr["glac_sched_code1"].ToString();
                            finn.glac_status = dr["glac_status"].ToString().Equals("A") ? true : false;
                            finn.glac_post_allow = dr["glac_post_allow"].ToString().Equals("Y") ? true : false;
                            finn.glac_parent_acct = dr["glac_parent_acct1"].ToString();
                            finn.glac_type1 = dr["glac_type"].ToString();
                            finn.glac_parent_acct1 = dr["glac_parent_acct"].ToString();
                            finn.glac_sched_code1 = dr["glac_sched_code"].ToString();
                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

        [Route("CUDAccountCodeDetails")]
        public HttpResponseMessage CUDAccountCodeDetails(List<Finn001> data)
        {

            //Finn001 finobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Finn001>(data);

            bool message = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn001 finobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_account_codes_proc]",
                                new List<SqlParameter>() 
                             { 
                                new SqlParameter("@opr",finobj.opr),
                                new SqlParameter("@glac_comp_code",finobj.comp_code),
                                new SqlParameter("@fins_comp_code",finobj.comp_code),
                                new SqlParameter("@glac_year",finobj.glac_year),
                                new SqlParameter("@glac_acct_code", finobj.glac_acct_code),
                                new SqlParameter("@glac_type", finobj.glac_type_code),
                                new SqlParameter("@glac_name", finobj.glac_name),
                                new SqlParameter("@glac_sched_code", finobj.glac_sched_code),
                                new SqlParameter("@glac_status", finobj.glac_status == true?"A":"I"),
                                new SqlParameter("@glac_post_allow", finobj.glac_post_allow == true?"Y":"N"),
                                new SqlParameter("@glac_parent_acct", finobj.glac_parent_acct_code),
                                new SqlParameter("@fins_appl_code", "Fin001"),
                                new SqlParameter("@fins_appl_form_field", "Account Type")

                         });
                            if (ins > 0)
                            {
                                message = true;
                            }
                        }
                    }
                }
                else
                {
                    message = false;
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
        #endregion
    }
}