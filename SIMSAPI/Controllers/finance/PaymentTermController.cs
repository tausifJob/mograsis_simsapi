﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/PaymentTermDetails")]
    public class PaymentTermController:ApiController
    {
        [Route("getPaymentTerm")]
        public HttpResponseMessage getPaymentTerm()
        {
            List<fins108> payterm_list = new List<fins108>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pty_payment_terms",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                           // new SqlParameter("@comp_code", "1")
                          
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins108 obj = new fins108();
                            obj.PT_COMP_Code = dr["PT_COMP_Code"].ToString();
                            obj.company_name = dr["company_name"].ToString();
                            obj.PT_TERM_NAME = dr["PT_TERM_NAME"].ToString();

                            obj.PT_NO_OF_DAYS = dr["PT_NO_OF_DAYS"].ToString();
                         
                            obj.PT_ENABLED = dr["PT_ENABLED"].Equals("A") ? true : false;
                          
                            payterm_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, payterm_list);
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, payterm_list);
        }


        [Route("PaymentTermCUD")]
        public HttpResponseMessage PaymentTermCUD(List<fins108> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LedgerControlCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins108 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pty_payment_terms",
                            new List<SqlParameter>()
                            {
                               new SqlParameter("@opr",simsobj.opr),
                               new SqlParameter("@comp_code", simsobj.PT_COMP_Code),
                               new SqlParameter("@pt_term_name", simsobj.PT_TERM_NAME),
                               new SqlParameter("@pt_no_of_days", simsobj.PT_NO_OF_DAYS),
                               new SqlParameter("@pt_enabled",simsobj.PT_ENABLED==true?"A":"I"),
                               new SqlParameter("@org_pt_term_name", simsobj.org_PT_TERM_NAME),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}