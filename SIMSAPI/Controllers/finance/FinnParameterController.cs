﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.FinnParameterController
{
    [RoutePrefix("api/common/FinnParameter")]
    [BasicAuthentication]
    public class FinnParameterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllFinnParameter")]
        public HttpResponseMessage getAllFinnParameter()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFinnParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllFinnParameter"));

            List<Fin149> desg_list = new List<Fin149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin149 simsobj = new Fin149();
                            simsobj.fins_comp_code = dr["fins_comp_code"].ToString();
                            simsobj.fins_comp_name = dr["fins_comp_name"].ToString();
                            simsobj.fins_appl_code = dr["fins_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.fins_appl_form_field = dr["fins_appl_form_field"].ToString();
                            simsobj.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            simsobj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();
                            simsobj.fins_appl_form_field_value2 = dr["fins_appl_form_field_value2"].ToString();
                            simsobj.fins_appl_form_field_value3 = dr["fins_appl_form_field_value3"].ToString();
                            simsobj.fins_appl_form_field_value4 = dr["fins_appl_form_field_value4"].ToString();
                            //simsobj.desg_communication_status = dr["dg_communication_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanyName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCompanyName"));

            List<Fin149> mod_list = new List<Fin149>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_designation_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin149 simsobj = new Fin149();
                            simsobj.fins_comp_code = dr["comp_code"].ToString();
                            simsobj.fins_comp_name = dr["comp_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getApplicationName")]
        public HttpResponseMessage getApplicationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getApplicationName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getApplicationName"));

            List<Fin149> fin_list = new List<Fin149>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin149 simsobj = new Fin149();
                            simsobj.fins_application_code = dr["comn_appl_code"].ToString();
                            simsobj.fins_application_name = dr["comn_appl_name_en"].ToString();
                            fin_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, fin_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("CUDFinnParameter")]
        public HttpResponseMessage CUDFinnParameter(List<Fin149> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFinnParameter(),PARAMETERS";
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin149 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_parameter_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@fins_comp_code", simsobj.fins_comp_code),
                                new SqlParameter("@fins_appl_code", simsobj.fins_appl_code),
                                new SqlParameter("@fins_appl_form_field", simsobj.fins_appl_form_field),
                                new SqlParameter("@fins_appl_parameter", simsobj.fins_appl_parameter),
                                new SqlParameter("@fins_appl_form_field_value1", simsobj.fins_appl_form_field_value1),
                                new SqlParameter("@fins_appl_form_field_value2", simsobj.fins_appl_form_field_value2),
                                new SqlParameter("@fins_appl_form_field_value3", simsobj.fins_appl_form_field_value3),
                                new SqlParameter("@fins_appl_form_field_value4", simsobj.fins_appl_form_field_value4),
                                new SqlParameter("@fins_appl_form_field_old",simsobj.fins_appl_form_field_old),	    
                                new SqlParameter("@fins_appl_parameter_old"	,simsobj.fins_appl_parameter_old),    
                                new SqlParameter("@fins_appl_form_field_value1_old",simsobj.fins_appl_form_field_value1_old),
                                new SqlParameter("@fins_appl_form_field_value2_old",simsobj.fins_appl_form_field_value2_old),
                                new SqlParameter("@fins_appl_form_field_value3_old",simsobj.fins_appl_form_field_value3_old),
                                new SqlParameter("@fins_appl_form_field_value4_old",simsobj.fins_appl_form_field_value4_old)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
