﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.FinancialDocumentController
{
    [RoutePrefix("api/FinancialDoc")]
    [BasicAuthentication]
    public class FinancialDocumentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllFinancialDocument")]
        public HttpResponseMessage getAllFinancialDocument(string comp_cd, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFinancialDocument(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllFinancialDocument"));

            List<Fin060> goaltarget_list = new List<Fin060>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_financial_documents_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@gldc_comp_code", comp_cd),
                            new SqlParameter("@gldc_year", fyear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin060 simsobj = new Fin060();
                            simsobj.gldc_comp_code = dr["gldc_comp_code"].ToString();
                            simsobj.gldc_comp_name = dr["gldc_comp_name"].ToString();
                            simsobj.gldc_year = dr["gldc_year"].ToString();
                            simsobj.gldc_doc_code = dr["gldc_doc_code"].ToString();
                            simsobj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            simsobj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            simsobj.gldc_doc_type_name = dr["gldc_doc_type_name"].ToString();
                            simsobj.gldc_next_srl_no = dr["gldc_next_srl_no"].ToString();
                            simsobj.gldc_next_prv_no = dr["gldc_next_prv_no"].ToString();
                            simsobj.gldc_doc_type_name = dr["gldc_doc_type_name"].ToString();// gldc_doc_type
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gldc_seq_prefix = dr["gldc_seq_prefix"].ToString();
                            //simsobj.sims_sip_goal_target_max_point = dr["sims_sip_goal_target_max_point"].ToString();
                            //simsobj.sims_sip_goal_target_status = dr["sims_sip_goal_target_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAllFinancialDocument_DPS")]
        public HttpResponseMessage getAllFinancialDocument_DPS(string comp_cd, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFinancialDocument(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllFinancialDocument"));

            List<Fin060> goaltarget_list = new List<Fin060>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_financial_documents_DPS_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@gldc_comp_code", comp_cd),
                            new SqlParameter("@gldc_year",fyear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin060 simsobj = new Fin060();
                            simsobj.gldc_comp_code = dr["gldc_comp_code"].ToString();
                            simsobj.gldc_comp_name = dr["gldc_comp_name"].ToString();
                            simsobj.gldc_year = dr["gldc_year"].ToString();
                            simsobj.gldc_doc_code = dr["gldc_doc_code"].ToString();
                            simsobj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            simsobj.gldc_doc_type_name = dr["gldc_doc_type_name"].ToString();
                            simsobj.gldc_next_srl_no = dr["gldc_next_srl_no"].ToString();
                            simsobj.gldc_next_prv_no = dr["gldc_next_prv_no"].ToString();
                            simsobj.gldc_doc_type_name = dr["gldc_doc_type_name"].ToString();// gldc_doc_type
                            //simsobj.sims_sip_goal_target_end_date = dr["sims_sip_goal_target_end_date"].ToString();
                            //simsobj.sims_sip_goal_target_min_point = dr["sims_sip_goal_target_min_point"].ToString();
                            //simsobj.sims_sip_goal_target_max_point = dr["sims_sip_goal_target_max_point"].ToString();
                            //simsobj.sims_sip_goal_target_status = dr["sims_sip_goal_target_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getDocumentType")]
        public HttpResponseMessage getDocumentType(string comp_cd,string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocumentType()PARAMETERS ::NA";
          //  Log.Debug(string.Format(debug, "ERP/Inventory/", "COMBOBOX1getDocumentType"));

            List<Fin060> doc_list = new List<Fin060>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_financial_documents_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@gldc_comp_code",comp_cd),
                            new SqlParameter("@gldc_year", fyear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin060 simsobj = new Fin060();
                            simsobj.gldc_doc_type = dr["fins_appl_parameter"].ToString();
                            simsobj.gldc_doc_type_name = dr["fins_appl_form_field_value1"].ToString(); 
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

       

        [Route("CUDFinantialDocument")]
        public HttpResponseMessage CUDFinantialDocument(List<Fin060> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFinantialDocument(),PARAMETERS";

            //Log.Debug(string.Format(debug, "MODULE", "CUDDocumentDetails", simsobj));

          
            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin060 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_financial_documents_proc]",
                            new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                               // new SqlParameter("@sub_opr", simsobj.opr),
                                new SqlParameter("@gldc_year", simsobj.gldc_year),
                                new SqlParameter("@gldc_comp_code",simsobj.gldc_comp_code),
                               new SqlParameter("@gldc_doc_code", simsobj.gldc_doc_code),
                                new SqlParameter("@gldc_doc_name", simsobj.gldc_doc_name),
                                new SqlParameter("@gldc_doc_type", simsobj.gldc_doc_type),
                                new SqlParameter("@gldc_next_srl_no",simsobj.gldc_next_srl_no),
                                new SqlParameter("@gldc_next_prv_no",simsobj.gldc_next_prv_no),
                                  new SqlParameter("@gldc_doc_short_name", simsobj.gldc_doc_short_name),
                                new SqlParameter("@gldc_seq_prefix",simsobj.gldc_seq_prefix),
                               
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);

                }
            }

            catch (Exception x)
            {

               // message.strMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDFinantialDocument_DPS")]
        public HttpResponseMessage CUDFinantialDocument_DPS(List<Fin060> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFinantialDocument(),PARAMETERS";

            //Log.Debug(string.Format(debug, "MODULE", "CUDDocumentDetails", simsobj));


            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin060 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_financial_documents_DPS_proc]",
                            new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                               // new SqlParameter("@sub_opr", simsobj.opr),
                                new SqlParameter("@gldc_year", simsobj.gldc_year),
                                new SqlParameter("@gldc_comp_code",simsobj.gldc_comp_code),
                               new SqlParameter("@gldc_doc_code", simsobj.gldc_doc_code),
                                new SqlParameter("@gldc_doc_name", simsobj.gldc_doc_name),
                                new SqlParameter("@gldc_doc_short_name", simsobj.gldc_doc_short_name),
                                new SqlParameter("@gldc_doc_type", simsobj.gldc_doc_type),
                                new SqlParameter("@gldc_next_srl_no",simsobj.gldc_next_srl_no),
                                new SqlParameter("@gldc_next_prv_no",simsobj.gldc_next_prv_no),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);

                }
            }

            catch (Exception x)
            {

                message.strMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }

}










