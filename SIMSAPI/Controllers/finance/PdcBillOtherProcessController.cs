﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/PDCBillOtherProcess")]
    [BasicAuthentication]
    public class PdcBillOtherProcessController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    
        [Route("getPDCBillsFinn213")]
        public HttpResponseMessage getPDCBillsFinn213(string compcode, string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string bank_cd, string filter_text, string filter_range, string sub_ref_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPDCBillsFinn213()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            DateTime? duedate = null;
            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","S"),
                             new SqlParameter("@opr_upd", "PB"),
                             new SqlParameter("@pb_comp_code",compcode),
                             new SqlParameter("@pb_sl_ldgr_code", ldgr_code),
                             new SqlParameter("@pb_sl_acno", slma_acno),
                             new SqlParameter("@pb_dept_no", dept_no),
                             new SqlParameter("@pb_bank_from", bank_cd),
                             new SqlParameter("@filter_text", filter_text),
                             new SqlParameter("@filter_range", filter_range),
                             new SqlParameter("@sdate",db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@edate", db.DBYYYYMMDDformat(to_date)),                           
                             new SqlParameter("@pb_resub_ref", sub_ref_no)
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 finnobj = new Finn212();
                            finnobj.pb_comp_code = dr["pb_comp_code"].ToString();
                            finnobj.pb_discd = dr["pb_discd"].ToString();
                            finnobj.pb_sl_ldgr_code = dr["pb_sl_ldgr_code"].ToString();
                            finnobj.pb_ldgr_code_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.pb_doc_type = dr["pb_doc_type"].ToString();
                            finnobj.pb_sl_acno = dr["pb_sl_acno"].ToString();
                            finnobj.pb_sl_acno_name = dr["pb_sl_acno_name"].ToString();
                            finnobj.pb_dept_no = dr["pb_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_cur_date"].ToString()) == false)
                                finnobj.pb_cur_date = db.UIDDMMYYYYformat(dr["pb_cur_date"].ToString());
                            finnobj.pb_bank_from = dr["pb_bank_from"].ToString();
                            finnobj.pb_bank_code_name = dr["pb_bank_from_name"].ToString();

                            finnobj.pb_cheque_no = dr["pb_cheque_no"].ToString();

                            if (string.IsNullOrEmpty(dr["pb_due_date"].ToString()) == false)
                                //duedate = Convert.ToDateTime(dr["pb_due_date"].ToString());
                                finnobj.pb_due_date = db.UIDDMMYYYYformat(dr["pb_due_date"].ToString());
                                //finnobj.pb_due_date = duedate.Value.ToShortDateString().ToString();

                            if (string.IsNullOrEmpty(dr["pb_amount"].ToString()) == false)
                                finnobj.pb_amount = Decimal.Parse(dr["pb_amount"].ToString());
                            finnobj.pb_our_doc_no = dr["pb_our_doc_no"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pb_ref_no = dr["pb_ref_no"].ToString();

                            mod_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getPDC_Bills_Pool_data")]
        public HttpResponseMessage getPDC_Bills_Pool_data(string compcode, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPDC_Bills_Pool_data()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            string ref_no = string.Empty;

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[fins_pdc_cheques]",
                        new List<SqlParameter>()
                         {
                           
                             
                              new SqlParameter("@opr", 'P'),
                              new SqlParameter("@opr_upd", "PB"),
                              new SqlParameter("@pc_comp_code",compcode),
                              new SqlParameter("@pc_due_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@pc_submission_date", db.DBYYYYMMDDformat(to_date)),

                         });
                    if (ins > 0)
                    {
                        ref_no = ins + " Records Pulled Successfully";
                    }
                    else
                    {
                        ref_no = "0 Record Pulled";
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ref_no);

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Submit_Pdc_Bilss_Finn213")]
        public HttpResponseMessage Submit_Pdc_Bilss_Finn213(List<Finn212> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Submit_Pdc_Bilss_Finn213()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            int record_cnt = 1;
            string status = string.Empty;
            string ref_no = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                     {
                       
                         new SqlParameter("@opr", 'U'),
                         new SqlParameter("@opr_upd", "O"),
                         new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                         new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                         new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                         new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                         new SqlParameter("@pb_amount", finnobj.pb_amount),
                         new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                         
                         new SqlParameter("@sdate", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         new SqlParameter("@user_name", finnobj.pb_ldgr_code_name),
                           new SqlParameter("@count", record_cnt),    
                     });
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["pb_ref_no"].ToString()))
                                    ref_no = dr["pb_ref_no"].ToString();
                            }
                        }
                        dr.Close();
                        record_cnt = record_cnt + 1;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ref_no);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                /*message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;*/
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }


        }

        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<Finn212> doc_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@tbl_cond", comp_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 simsobj = new Finn212();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.codp_dept_no = dr["codp_dept_no"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getPDCReSubmissionBillsFinn214")]
        public HttpResponseMessage getPDCReSubmissionBillsFinn214(string compcode, string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string bank_cd, string filter_text, string filter_range, string sub_ref_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPDCReSubmissionBillsFinn214()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            DateTime? duedate = null;
            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","S"),
                             new SqlParameter("@opr_upd", "PRS"),
                             new SqlParameter("@pb_comp_code",compcode),
                             new SqlParameter("@pb_sl_ldgr_code", ldgr_code),
                             new SqlParameter("@pb_sl_acno", slma_acno),
                             new SqlParameter("@pb_dept_no", dept_no),
                             new SqlParameter("@pb_bank_from", bank_cd),
                             new SqlParameter("@filter_text", filter_text),
                             new SqlParameter("@filter_range", filter_range),
                             new SqlParameter("@sdate", db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@edate", db.DBYYYYMMDDformat(to_date)),                           
                             new SqlParameter("@pb_resub_ref", sub_ref_no)
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 finnobj = new Finn212();
                            finnobj.pb_comp_code = dr["pb_comp_code"].ToString();
                            finnobj.pb_discd = dr["pb_discd"].ToString();
                            finnobj.pb_sl_ldgr_code = dr["pb_sl_ldgr_code"].ToString();
                            finnobj.pb_ldgr_code_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.pb_doc_type = dr["pb_doc_type"].ToString();
                            finnobj.pb_sl_acno = dr["pb_sl_acno"].ToString();
                            finnobj.pb_sl_acno_name = dr["pb_sl_acno_name"].ToString();
                            finnobj.pb_dept_no = dr["pb_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_cur_date"].ToString()) == false)
                                finnobj.pb_cur_date = db.UIDDMMYYYYformat(dr["pb_cur_date"].ToString());
                            finnobj.pb_bank_from = dr["pb_bank_from"].ToString();
                            finnobj.pb_bank_code_name = dr["pb_bank_from_name"].ToString();

                            finnobj.pb_cheque_no = dr["pb_cheque_no"].ToString();

                            if (string.IsNullOrEmpty(dr["pb_due_date"].ToString()) == false)
                                //duedate = Convert.ToDateTime(dr["pb_due_date"].ToString());
                                finnobj.pb_due_date = db.UIDDMMYYYYformat(dr["pb_due_date"].ToString());
                            //finnobj.pb_due_date = duedate.Value.ToShortDateString().ToString();

                            if (string.IsNullOrEmpty(dr["pb_amount"].ToString()) == false)
                                finnobj.pb_amount = Decimal.Parse(dr["pb_amount"].ToString());
                            finnobj.pb_our_doc_no = dr["pb_our_doc_no"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pb_ref_no = dr["pb_ref_no"].ToString();

                            mod_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("ReSubmit_Pdc_Bilss_Finn214")]
        public HttpResponseMessage ReSubmit_Pdc_Bilss_Finn214(List<Finn212> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ReSubmit_Pdc_Bilss_Finn214()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            int record_cnt = 1;
            string status = string.Empty;
            string ref_no = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                     {
                       
                         new SqlParameter("@opr", 'U'),
                         new SqlParameter("@opr_upd", "RSU"),
                         new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                         new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                         new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                         new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                         new SqlParameter("@pb_amount", finnobj.pb_amount),
                         new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                         new SqlParameter("@user_name", finnobj.pb_ldgr_code_name),                         
                         new SqlParameter("@sdate", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                           new SqlParameter("@count", record_cnt),    
                     });
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["pb_resub_ref"].ToString()))
                                    ref_no = dr["pb_resub_ref"].ToString();
                            }
                        }
                        dr.Close();
                        record_cnt = record_cnt + 1;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ref_no);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }


        }

        public string Get_Max_resub_ref()
        {

            string ref_no = string.Empty;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","R"),
                             
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ref_no = dr["pb_resub_ref"].ToString();
                        }
                        return ref_no;

                    }
                    else
                        return ref_no;
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return ref_no;
            }


        }

        public string Get_Max_RefNo()
        {

            string ref_no = string.Empty;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","Q"),
                             
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ref_no = dr["pb_ref_no"].ToString();
                        }
                        return ref_no;

                    }
                    else
                        return ref_no;
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return ref_no;
            }


        }

        [Route("getPDCReturn_BillsFinn215")]
        public HttpResponseMessage getPDCReturn_BillsFinn215(string compcode, string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string bank_cd, string filter_text, string filter_range, string sub_ref_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPDCReSubmissionBillsFinn214()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            DateTime? duedate = null;
            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","S"),
                             new SqlParameter("@opr_upd", "RS"),
                             new SqlParameter("@pb_comp_code",compcode),
                             new SqlParameter("@pb_sl_ldgr_code", ldgr_code),
                             new SqlParameter("@pb_sl_acno", slma_acno),
                             new SqlParameter("@pb_dept_no", dept_no),
                             new SqlParameter("@pb_bank_from", bank_cd),
                             new SqlParameter("@filter_text", filter_text),
                             new SqlParameter("@filter_range", filter_range),
                             new SqlParameter("@sdate", db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@edate", db.DBYYYYMMDDformat(to_date)),                           
                             new SqlParameter("@pb_ref_no", sub_ref_no)
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 finnobj = new Finn212();
                            finnobj.pb_comp_code = dr["pb_comp_code"].ToString();
                            finnobj.pb_discd = dr["pb_discd"].ToString();
                            finnobj.pb_sl_ldgr_code = dr["pb_sl_ldgr_code"].ToString();
                            finnobj.pb_ldgr_code_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.pb_doc_type = dr["pb_doc_type"].ToString();
                            finnobj.pb_sl_acno = dr["pb_sl_acno"].ToString();
                            finnobj.pb_sl_acno_name = dr["pb_sl_acno_name"].ToString();
                            finnobj.pb_dept_no = dr["pb_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_cur_date"].ToString()) == false)
                                finnobj.pb_cur_date = db.UIDDMMYYYYformat(dr["pb_cur_date"].ToString());
                            finnobj.pb_bank_from = dr["pb_bank_from"].ToString();
                            finnobj.pb_bank_code_name = dr["pb_bank_from_name"].ToString();

                            finnobj.pb_cheque_no = dr["pb_cheque_no"].ToString();

                            if (string.IsNullOrEmpty(dr["pb_due_date"].ToString()) == false)
                                //duedate = Convert.ToDateTime(dr["pb_due_date"].ToString());
                                finnobj.pb_due_date = db.UIDDMMYYYYformat(dr["pb_due_date"].ToString());
                            //finnobj.pb_due_date = duedate.Value.ToShortDateString().ToString();

                            if (string.IsNullOrEmpty(dr["pb_amount"].ToString()) == false)
                                finnobj.pb_amount = Decimal.Parse(dr["pb_amount"].ToString());
                            finnobj.pb_our_doc_no = dr["pb_our_doc_no"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pb_ref_no = dr["pb_ref_no"].ToString();

                            mod_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Return_Pdc_Bilss_Finn215")]
        public HttpResponseMessage Return_Pdc_Bilss_Finn215(List<Finn212> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Return_Pdc_Bilss_Finn215()PARAMETERS ::NA";



            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_bills1",
                        new List<SqlParameter>()
                     {
                       
                         new SqlParameter("@opr", 'U'),
                         new SqlParameter("@opr_upd", "RU"),
                         new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                         new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                         new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                         new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                         new SqlParameter("@pb_amount", finnobj.pb_amount),
                         new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                         new SqlParameter("@user_name", finnobj.pb_ldgr_code_name),
                         new SqlParameter("@sdate", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                /*message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;*/
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }


        }


        [Route("getPDCCancel_BillsFinn216")]
        public HttpResponseMessage getPDCCancel_BillsFinn216(string compcode, string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string bank_cd, string filter_text, string filter_range, string sub_ref_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPDCCancel_BillsFinn216()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));
            DateTime? duedate = null;
            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_bills1]",
                        new List<SqlParameter>()
                         {
                           
                             new SqlParameter("@opr","S"),
                             new SqlParameter("@opr_upd", "PCB"),
                             new SqlParameter("@pb_comp_code",compcode),
                             new SqlParameter("@pb_sl_ldgr_code", ldgr_code),
                             new SqlParameter("@pb_sl_acno", slma_acno),
                             new SqlParameter("@pb_dept_no", dept_no),
                             new SqlParameter("@pb_bank_from", bank_cd),
                             new SqlParameter("@filter_text", filter_text),
                             new SqlParameter("@filter_range", filter_range),
                             new SqlParameter("@sdate", db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@edate", db.DBYYYYMMDDformat(to_date)),                           
                             new SqlParameter("@pb_resub_ref", sub_ref_no)
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 finnobj = new Finn212();
                            finnobj.pb_comp_code = dr["pb_comp_code"].ToString();
                            finnobj.pb_discd = dr["pb_discd"].ToString();
                            finnobj.pb_sl_ldgr_code = dr["pb_sl_ldgr_code"].ToString();
                            finnobj.pb_ldgr_code_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.pb_doc_type = dr["pb_doc_type"].ToString();
                            finnobj.pb_sl_acno = dr["pb_sl_acno"].ToString();
                            finnobj.pb_sl_acno_name = dr["pb_sl_acno_name"].ToString();
                            finnobj.pb_dept_no = dr["pb_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_cur_date"].ToString()) == false)
                                finnobj.pb_cur_date = db.UIDDMMYYYYformat(dr["pb_cur_date"].ToString());
                            finnobj.pb_bank_from = dr["pb_bank_from"].ToString();
                            finnobj.pb_bank_code_name = dr["pb_bank_from_name"].ToString();

                            finnobj.pb_cheque_no = dr["pb_cheque_no"].ToString();

                            if (string.IsNullOrEmpty(dr["pb_due_date"].ToString()) == false)
                                //duedate = Convert.ToDateTime(dr["pb_due_date"].ToString());
                            finnobj.pb_due_date =db.UIDDMMYYYYformat(dr["pb_due_date"].ToString());
                            //finnobj.pb_due_date = duedate.Value.ToShortDateString().ToString();

                            if (string.IsNullOrEmpty(dr["pb_amount"].ToString()) == false)
                                finnobj.pb_amount = Decimal.Parse(dr["pb_amount"].ToString());
                            finnobj.pb_our_doc_no = dr["pb_our_doc_no"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pb_ref_no = dr["pb_ref_no"].ToString();

                            mod_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("Cancel_Pdc_Bilss_Finn216")]
        public HttpResponseMessage Cancel_Pdc_Bilss_Finn216(List<Finn212> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Cancel_Pdc_Bilss_Finn216()PARAMETERS ::NA";



            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_bills1",
                        new List<SqlParameter>()
                     {
                       
                         new SqlParameter("@opr", 'U'),
                         new SqlParameter("@opr_upd", "C"),
                         new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                         new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                         new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                         new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                         new SqlParameter("@pb_amount", finnobj.pb_amount),
                         new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                         new SqlParameter("@user_name", finnobj.pb_ldgr_code_name),
                         new SqlParameter("@pb_narrative", finnobj.pb_narrative),

                         new SqlParameter("@sdate", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }


        }


        [Route("PDC_Bills_Realize")]
        public HttpResponseMessage PDC_Bills_Realize(List<Finn212> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : PDC_Bills_Realize()PARAMETERS ::NA";



            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        if (string.IsNullOrEmpty(finnobj.ps_approv_date))
                            finnobj.ps_approv_date = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_bills_realize",
                        new List<SqlParameter>()
                     {
                       
                         
                         new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                         new SqlParameter("@pb_dept_no", finnobj.pb_dept_no),
                         new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                         new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                         new SqlParameter("@pb_ref_no", finnobj.pb_ref_no),
                         new SqlParameter("@gltd_doc_code", "PP"),
                         new SqlParameter("@pb_our_doc_no", finnobj.pb_our_doc_no),
                         new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                         new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                         new SqlParameter("@pb_amount", finnobj.pb_amount),
                         new SqlParameter("@pb_discd", "S"),
                         new SqlParameter("@user_name", finnobj.pb_ldgr_code_name),
                         new SqlParameter("@gltd_doc_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         new SqlParameter("@gltd_doc_flag", ""),
                         new SqlParameter("@gltd_cur_status", "Pos"),
                         new SqlParameter("@gltd_prepare_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         new SqlParameter("@gltd_verify_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         new SqlParameter("@gltd_authorize_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         //new SqlParameter("@gltd_post_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                         new SqlParameter("@gltd_post_date", db.DBYYYYMMDDformat(finnobj.ps_approv_date)),
                         new SqlParameter("@gltd_paid_to", ""),
                         new SqlParameter("@gltd_cheque_no", ""),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }


        }



    }
}