﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/CreditEditDivision")]
    [BasicAuthentication]
    public class CreditEditDivisionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllCreditEditDivision")]
        public HttpResponseMessage getAllCreditEditDivision()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCreditEditDivision(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllCreditEditDivision"));

            List<Fin011> goaltarget_list = new List<Fin011>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_divisions_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                          new SqlParameter("@gd_comp_code", '1'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin011 simsobj = new Fin011();
                            simsobj.gd_comp_code = dr["gd_comp_code"].ToString();
                            simsobj.gd_division_code = dr["gd_division_code"].ToString();
                            simsobj.gd_division_desc = dr["gd_division_desc"].ToString();
                           
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



       
        [Route("CUDCreditEditDivision")]
        public HttpResponseMessage CUDCreditEditDivision(List<Fin011> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin011 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_divisions_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@gd_comp_code", "1"),
                                new SqlParameter("@gd_division_code", simsobj.gd_division_code),
                                new SqlParameter("@gd_division_desc", simsobj.gd_division_desc),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                           insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }

  
}










