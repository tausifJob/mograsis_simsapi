﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/BAPL_Report")]
    public class BudgetVsActualReportForProfitAndLossController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCompCode1")]
        public HttpResponseMessage getCompCode1()
        {
            List<BAPL01> lstCuriculum = new List<BAPL01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[BudgetVsActualReportForProfitAndLoss]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BAPL01 sequence = new BAPL01();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAllGroupCode")]
        public HttpResponseMessage getAllGroupCode(string comp_code)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<BAPL01> grade_list = new List<BAPL01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[BudgetVsActualReportForProfitAndLoss]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@comp_code", comp_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BAPL01 simsobj = new BAPL01();
                            simsobj.group_code = dr["group_code"].ToString();
                            simsobj.group_name = dr["group_name"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllSubGroupCode")]
        public HttpResponseMessage getAllSubGroupCode(string comp_code,String mom_start_date, string group_code)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<BAPL01> grade_list = new List<BAPL01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[BudgetVsActualReportForProfitAndLoss]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "U"),
                               new SqlParameter("@comp_code", comp_code),
                               new SqlParameter("@group_code", group_code),
                               new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date))

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BAPL01 simsobj = new BAPL01();
                            simsobj.subgroup_code = dr["subgroup_code"].ToString();
                            simsobj.subgroup_name = dr["subgroup_name"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }
        [Route("getBAPLSummaryReport")]
        public HttpResponseMessage getBAPLSummaryReport(string comp_code, string mom_start_date, string mom_end_date, string report_status, string group_code,string sub_group_code,string view)
        {
            List<BAPL01> lstCuriculum = new List<BAPL01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[BudgetVsActualReportForProfitAndLoss]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'X'),
                             new SqlParameter("@comp_code", comp_code),
                             new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(mom_end_date)),
                             new SqlParameter("@report_status", report_status),
                             new SqlParameter("@group_code", group_code),
                             new SqlParameter("@sub_group_code", sub_group_code),
                             new SqlParameter("@view", view),
                             

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BAPL01 sequence = new BAPL01();
                            sequence.group_code = dr["group_code"].ToString();
                            sequence.group_name = dr["group_name"].ToString();
                            sequence.subgroup_code = dr["subgroup_code"].ToString();
                            sequence.subgroup_name = dr["subgroup_name"].ToString();
                            sequence.acct_name = dr["acct_name"].ToString();
                            sequence.glma_acct_code = dr["glma_acct_code"].ToString();
                            sequence.glma_dept_no = dr["glma_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.actual_amount = dr["actual_amount"].ToString();
                            sequence.glpr_bud_amt = dr["glpr_bud_amt"].ToString();
                            sequence.glfp_prd_name = dr["glfp_prd_name"].ToString();
                            sequence.glfp_prd_no = dr["glfp_prd_no"].ToString();
                            sequence.glfp_prd_st_date = dr["glfp_prd_st_date"].ToString();
                            sequence.glma_year = dr["glma_year"].ToString();
                            sequence.variance = dr["variance"].ToString();
                            sequence.variance_per = dr["variance_per"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        






    }
}