﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/ScheduleGroup")]
    public class ScheduleGroupController : ApiController
    {
        [Route("getScheduleGroup")]
        public HttpResponseMessage getScheduleGroup(string comp_code)
        {
            List<Fin117> SGD = new List<Fin117>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedule_groups_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@glsg_comp_code", comp_code),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin117 obj = new Fin117();
                            obj.glsg_comp_code = dr["glsg_comp_code"].ToString();
                            obj.glsg_group_code = dr["glsg_group_code"].ToString();
                            obj.glsg_group_description = dr["glsg_group_description"].ToString();
                            obj.glsg_group_type = dr["glsg_group_type"].ToString();
                            obj.glsg_group_ind = dr["glsg_group_ind"].ToString();
                            obj.glsg_parent_group = dr["glsg_parent_group"].ToString();

                            SGD.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SGD);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SGD);
        }

        [Route("ScheduleGroupCUD")]
        public HttpResponseMessage ScheduleGroupCUD(List<Fin117> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin117 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_schedule_groups_proc]",
                            new List<SqlParameter>()
                                    {
                       
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@glsg_comp_code", simsobj.glsg_comp_code),
                                 new SqlParameter("@glsg_group_code", simsobj.glsg_group_code),
                                 new SqlParameter("@glsg_group_description", simsobj.glsg_group_description),
                                 new SqlParameter("@glsg_group_type", simsobj.glsg_group_type),
                                 new SqlParameter("@glsg_group_ind", simsobj.glsg_group_ind),
                                 new SqlParameter("@glsg_parent_group", simsobj.glsg_parent_group)

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getTypeGroup")]
        public HttpResponseMessage getTypeGroup()
        {
            List<Fin117> GT = new List<Fin117>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedule_groups_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin117 obj = new Fin117();
                            obj.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            obj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();


                            GT.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, GT);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, GT);
        }

    }
}