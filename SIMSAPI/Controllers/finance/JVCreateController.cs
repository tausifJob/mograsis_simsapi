﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/JVCreation")]
    public class JVCreateController : ApiController
    {

     
        [Route("getDocCodeJV")]
        public HttpResponseMessage getDocCodeJV(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@gldd_comp_code",comp_code)
                             //new SqlParameter("@fins_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            obj.gldc_doc_code_type = dr["gldc_doc_code_type"].ToString();
                            obj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            obj.gldc_doc_name   = obj.gltd_doc_code + "-" + dr["gldc_doc_name"].ToString();
                            //obj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            obj.gldc_seq_prefix = dr["gldc_seq_prefix"].ToString();
                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }

        [Route("getDocCodeJV_DPS")]
        public HttpResponseMessage getDocCodeJV_DPS(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_doc_details_DPS]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@gldd_comp_code",comp_code)
                             //new SqlParameter("@fins_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            obj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();

                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }

        [Route("getDocCodePCRI")]
        public HttpResponseMessage getDocCodePCRI(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_petty_cash_masters",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "J"),
                             new SqlParameter("@glpc_comp_code",comp_code),
                             new SqlParameter("@glpc_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            obj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();

                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }

        [Route("getDocCodePCDI")]
        public HttpResponseMessage getDocCodePCDI(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_petty_cash_masters",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "K"),
                             new SqlParameter("@glpc_comp_code",comp_code),
                             new SqlParameter("@glpc_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            obj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();

                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }


        //GetLedgerNumber
        [Route("GetLedgerNumber")]
        public HttpResponseMessage GetLedgerNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLedgerNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Fin150> ledgercode = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            simsobj.sllc_ldgr_name = dr["sllc_ldgr_full_name"].ToString();

                            ledgercode.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ledgercode);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, ledgercode);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetGLAccountNumber
        [Route("GetSLAccNumber")]
        public HttpResponseMessage GetSLAccNumber(string ldgr_code, string cmp_cd, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSLAccNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_sblgr_masters",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "G"),
                            new SqlParameter ("@slma_comp_code", cmp_cd),
                            new SqlParameter ("@slma_ldgrctl_year", year),
                            new SqlParameter ("@slma_ldgrctl_code", ldgr_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 finnObj = new Fin141();
                            finnObj.gldd_acct_code = dr["slma_acno"].ToString();
                            finnObj.gldd_acct_name = finnObj.gldd_acct_code + "-" + dr["coad_pty_short_name"].ToString();
                            finnObj.gldd_party_ref_no = dr["coad_xref_addr_id"].ToString();
                            finnObj.gldd_dept_code = dr["coad_dept_no"].ToString();
                            finnObj.gldd_dept_name = dr["codp_dept_name"].ToString();
                            doc_list.Add(finnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Get_provno")]

        public HttpResponseMessage Get_provno(string comp_code,string fyear, string doccode,string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_provno()PARAMETERS ::NA";
            List<Fin150> com_list = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@gltd_comp_code",comp_code),
                            new SqlParameter("@fyear",fyear),
                            new SqlParameter("@gltd_doc_code",doccode),
                            new SqlParameter("@gltd_prepare_user",user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            //simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAllRecords")]
        public HttpResponseMessage getAllRecords(string doccode, string prnumber, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";
            List<Fin150> com_list = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@opr_upd", 'S'),
                            new SqlParameter("@gldd_comp_code",comp_code),
                            new SqlParameter("@gldd_doc_code",doccode),
                            new SqlParameter("@gldd_prov_doc_no",prnumber)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                            simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            simsobj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_doc_narr_old = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            simsobj.gldd_acct_name = dr["gldd_acct_name"].ToString();

                            if (string.IsNullOrEmpty("gldd_doc_amount") == false)
                            {
                                if (decimal.Parse(dr["gldd_doc_amount"].ToString()) > 0)
                                    simsobj.gldd_doc_amount_debit = Convert.ToDecimal(dr["gldd_doc_amount"].ToString());
                                else
                                    simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gldd_doc_amount"].ToString()));
                            }
                            //simsobj.gldd_doc_amount = dr["gldd_doc_amount"].ToString();
                            simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                            simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                            simsobj.gldd_line_no = dr["gldd_line_no"].ToString();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            simsobj.gltd_doc_date =db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            simsobj.gltd_paid_to = dr["gltd_paid_to"].ToString();
                            simsobj.gltd_cheque_no = dr["gltd_cheque_no"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            simsobj.gldu_verify = dr["gltd_verify_user"].ToString();
                            simsobj.gldu_authorize = dr["gltd_authorize_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                simsobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_authorize_date"].ToString()))
                                simsobj.gltd_authorize_date = db.UIDDMMYYYYformat(dr["gltd_authorize_date"].ToString());
                            simsobj.gldd_cost_center_code = dr["gldd_cost_center_code"].ToString();
                            simsobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getAllRecords_DPS")]
        public HttpResponseMessage getAllRecords_DPS(string doccode, string prnumber, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";
            List<Fin150> com_list = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details_DPS]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@opr_upd", 'S'),
                            new SqlParameter("@gldd_comp_code",comp_code),
                            new SqlParameter("@gldd_doc_code",doccode),
                            new SqlParameter("@gldd_prov_doc_no",prnumber)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                            simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            simsobj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_doc_narr_old = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            simsobj.gldd_acct_name = dr["gldd_acct_name"].ToString();

                            if (string.IsNullOrEmpty("gldd_doc_amount") == false)
                            {
                                if (decimal.Parse(dr["gldd_doc_amount"].ToString()) > 0)
                                    simsobj.gldd_doc_amount_debit = Convert.ToDecimal(dr["gldd_doc_amount"].ToString());
                                else
                                    simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gldd_doc_amount"].ToString()));
                            }
                            //simsobj.gldd_doc_amount = dr["gldd_doc_amount"].ToString();
                            simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                            simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            simsobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            simsobj.gltd_paid_to = dr["gltd_paid_to"].ToString();
                            simsobj.gltd_cheque_no = dr["gltd_cheque_no"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            simsobj.gldu_verify = dr["gltd_verify_user"].ToString();
                            simsobj.gldu_authorize = dr["gltd_authorize_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                simsobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_authorize_date"].ToString()))
                                simsobj.gltd_authorize_date = db.UIDDMMYYYYformat(dr["gltd_authorize_date"].ToString());
                            simsobj.gldd_cost_center_code = dr["gldd_cost_center_code"].ToString();
                            simsobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        //API DOCument DETAILS

        [Route("Getdocument_type")]
        public HttpResponseMessage Getdocument_type()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getdocument_type()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Fin150> ledgercode = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_doc_document_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gltd_doc_type = dr["fins_appl_parameter"].ToString();
                            simsobj.gltd_doc_type_name = dr["fins_appl_form_field_value1"].ToString();

                            ledgercode.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ledgercode);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetDocument_Details_DPS")]
        public HttpResponseMessage GetDocument_Details_DPS(string doccode, string prnumber, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";
            List<Fin150> com_list = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_document_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", 'S'),
                            new SqlParameter("@gltd_comp_code",comp_code),
                            new SqlParameter("@gltd_doc_code",doccode),
                            new SqlParameter("@gltd_prov_doc_no",prnumber)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            simsobj.gldd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            simsobj.gltd_doc_type = dr["gltd_doc_type"].ToString();
                            simsobj.gltd_doc_type_name = dr["gltd_doc_type_name"].ToString();
                            simsobj.gltd_doc_no = dr["gltd_doc_no"].ToString();
                            simsobj.gltd_doc_line_no = dr["gltd_doc_line_no"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();

                            if (string.IsNullOrEmpty("gltd_doc_amount") == false)
                            {
                                if (decimal.Parse(dr["gltd_doc_amount"].ToString()) > 0)
                                    simsobj.gldd_doc_amount_debit = Convert.ToDecimal(dr["gltd_doc_amount"].ToString());
                                else
                                    simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gltd_doc_amount"].ToString()));
                            }
                            //simsobj.gldd_doc_amount = dr["gldd_doc_amount"].ToString();


                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("Insert_JV_document_details")]
        public HttpResponseMessage Insert_JV_document_details(List<Fin150> data)
        {
            string status = string.Empty;
            bool insert = false;
            int cnt = 1;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin150 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_temp_doc_document_details",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                        new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                        new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                        new SqlParameter("@gltd_final_doc_no", finnobj.gldd_final_doc_no),
                        new SqlParameter("@gltd_doc_line_no", finnobj.gltd_doc_line_no),
                        new SqlParameter("@gltd_doc_type", finnobj.gltd_doc_type),
                        new SqlParameter("@gltd_doc_no", finnobj.gltd_doc_no),
                        new SqlParameter("@gltd_remarks", finnobj.gltd_remarks),
                        new SqlParameter("@gltd_doc_amount",(finnobj.gldd_doc_amount_debit>0)? finnobj.gldd_doc_amount_debit:(0-finnobj.gldd_doc_amount_credit)),
                        new SqlParameter("@cnt", cnt),

                      });
                        if (ins > 0)
                        {
                            insert = true;
                            cnt = cnt + 1;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        
        [Route("Checkstattus_doc_users")]
        public HttpResponseMessage Checkstattus_doc_users(string comp_code, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Checkstattus_doc_users()PARAMETERS ::NA";
            List<Fin150> getAllRecords = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@slma_comp_code",comp_code),
                            new SqlParameter("@gldu_user_name",user)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gldu_prepare = dr["gldu_prepare"].ToString();
                            simsobj.gldu_verify = dr["gldu_verify"].ToString();
                            simsobj.gldu_authorize = dr["gldu_authorize"].ToString();
                            getAllRecords.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, getAllRecords);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("Check_status_for_doc_users_In_comn_user_application_fins")]
        public HttpResponseMessage Check_status_for_doc_users_In_comn_user_application_fins(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Check_status_for_doc_users_In_comn_user_application_fins()PARAMETERS ::NA";
            List<Fin150> getAllRecords = new List<Fin150>();
            Message message = new Message();
            bool Flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@slma_comp_code",comp_code)
                              //new SqlParameter("@gldd_comp_code",user)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Flag = true;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, Flag);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, Flag);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("GetAllCashBankGLMasterAccountNo")]
        public HttpResponseMessage GetAllCashBankGLMasterAccountNo(string cmp_cd, string financialyear, string doc_cd, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllCashBankGLMasterAccountNo()PARAMETERS ::NA";

            List<Fin141> doc_list = new List<Fin141>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "M"),
                            new SqlParameter("@glac_comp_code", cmp_cd),
                            new SqlParameter("@glac_year", financialyear),
                            new SqlParameter ("@glac_acct_code", doc_cd),
                            new SqlParameter("@glac_type", username)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.master_acno = dr["glma_acno"].ToString();
                            simsobj.master_acno_name = dr["glac_name"].ToString();
                            simsobj.master_ac_cdname = dr["glma_acno"].ToString() + "-" + dr["glac_name"].ToString();
                            simsobj.gldd_dept_code = dr["gdua_dept_no"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getRevertedData")]
        public HttpResponseMessage getRevertedData(string comp_code, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRevertedData()PARAMETERS ::NA";

            List<Fin150> doc_list = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "R"),
                            new SqlParameter("@gltd_comp_code", comp_code),
                            new SqlParameter("@gltd_prepare_user", username)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            //simsobj.revert_doc_no = simsobj.revert_doc_no + dr["gltd_doc_code"].ToString() + dr["gltd_prov_doc_no"].ToString() + ",";
                            simsobj.revert_doc_no = dr["revert_docs"].ToString();
                            //if (!string.IsNullOrEmpty(simsobj.revert_doc_no))
                            //{
                            //    simsobj.revert_doc_no = simsobj.revert_doc_no.Substring(0, simsobj.revert_doc_no.Length - 1);
                            //}
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

       /* [Route("getrevertedDelete")]
        public HttpResponseMessage getrevertedDelete(string comp_code, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getrevertedDelete()PARAMETERS ::NA";

            List<Fin150> doc_list = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "Z"),
                            
                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            
                            //simsobj.revert_doc_no = dr["revert_docs"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
               // message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }*/
        /*[Route("deleteTempDocs")]
        public HttpResponseMessage deleteTempDocs(List<Fin150> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin150 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_temp_docs",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'T'),
                        new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                        new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                        new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                        
                      });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }*/
        [Route("revertedDelete")]
        public HttpResponseMessage revertedDelete(List<Fin150> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin150 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_temp_docs",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'Z'),
                        new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                        new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                        new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                        
                      });
                        if (ins > 0)
                        {
                            insert = true;
                           
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
        [Route("getLoggedUser")]
        public HttpResponseMessage getLoggedUser()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLoggedUser()PARAMETERS ::NA";

            List<Fin150> doc_list = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "L"),
                           
                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.fins_appl_code = dr["fins_appl_code"].ToString();
                            simsobj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();
                            simsobj.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                // message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRevertedData_DPS")]
        public HttpResponseMessage getRevertedData_DPS(string comp_code, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRevertedData()PARAMETERS ::NA";

            List<Fin150> doc_list = new List<Fin150>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs_DPS]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "R"),
                            new SqlParameter("@gltd_comp_code", comp_code),
                            new SqlParameter("@gltd_prepare_user", username)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            //simsobj.revert_doc_no = simsobj.revert_doc_no + dr["gltd_doc_code"].ToString() + dr["gltd_prov_doc_no"].ToString() + ",";
                            simsobj.revert_doc_no = dr["revert_docs"].ToString();
                            //if (!string.IsNullOrEmpty(simsobj.revert_doc_no))
                            //{
                            //    simsobj.revert_doc_no = simsobj.revert_doc_no.Substring(0, simsobj.revert_doc_no.Length - 1);
                            //}
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("GetAllGLAccountNos")]
        public HttpResponseMessage GetAllGLAccountNos(string glma_accountcode, string cmpnycode,string fyear,string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllGLAccountNos()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin141> doc_list = new List<Fin141>();
            if (glma_accountcode == "undefined")
                glma_accountcode = null;


            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_gl_masters]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@glma_comp_code",cmpnycode),
                            new SqlParameter("@glma_acct_code",glma_accountcode),
                            new SqlParameter("@glma_year",fyear),
                            new SqlParameter("@fins_appl_code",user)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 FinnObj = new Fin141();
                            FinnObj.gldd_acct_code = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString();//
                            FinnObj.gldd_acct_name = FinnObj.gldd_acct_code + "-" + dr["glma_acct_name"].ToString();
                            FinnObj.gldd_dept_code = dr["glma_dept_no"].ToString();
                            FinnObj.gldd_dept_name = dr["codp_dept_name"].ToString();
                            FinnObj.glma_acct_code = dr["glma_acct_code"].ToString();
                            FinnObj.sltr_yob_amt = decimal.Parse(dr["glma_op_bal_amt"].ToString());
                            doc_list.Add(FinnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("UpdateFins_temp_docs")]
        public HttpResponseMessage UpdateFins_temp_docs(Finn141 finnobj)
        {
            string status = string.Empty;
            bool Updated = false;
            try
            {
                finnobj.gltd_final_doc_no = "0";
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (finnobj.gltd_cur_status.Equals("Save"))
                    {
                        status = "Sav";
                    }
                    else if (finnobj.gltd_cur_status.Equals("Verify"))
                    {
                        status = "Vry";
                    }

                    int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_docs]",
                           new List<SqlParameter>()
                   {
                       new SqlParameter("@opr", 'U'),
                                new SqlParameter("@opr_upd", "NA"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_final_doc_no", finnobj.gltd_final_doc_no),
                                new SqlParameter("@gltd_doc_date",db.DBYYYYMMDDformat(finnobj.gltd_doc_date)),
                                new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                                new SqlParameter("@gltd_doc_narr", finnobj.gltd_doc_narr),
                                new SqlParameter("@gltd_remarks", finnobj.gltd_remarks),
                                new SqlParameter("@gltd_doc_flag", ""),
                                new SqlParameter("@gltd_cur_status",status),
                                new SqlParameter("@gltd_prepare_user", finnobj.gltd_prepare_user),
                                new SqlParameter("@gltd_prepare_date", db.DBYYYYMMDDformat(finnobj.gltd_prepare_date)),
                                new SqlParameter("@gltd_verify_user", finnobj.gltd_verify_user),
                                new SqlParameter("@gltd_verify_date", db.DBYYYYMMDDformat(finnobj.gltd_verify_date)),
                                new SqlParameter("@gltd_authorize_user", finnobj.gltd_authorize_user),
                                new SqlParameter("@gltd_authorize_date",db.DBYYYYMMDDformat(finnobj.gltd_authorize_date)),
                                new SqlParameter("@gltd_paid_to", finnobj.gltd_paid_to),
                                new SqlParameter("@gltd_cheque_no", finnobj.gltd_cheque_no),


                   });

                    if (upd > 0)
                    {
                        Updated = true;
                    }


                    else
                    {
                        Updated = false;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Updated);


                }
            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }


        [Route("UpdateFins_temp_docs_current_status_sendtoverify")]
        public HttpResponseMessage UpdateFins_temp_docs_current_status_sendtoverify(Finn141 finnobj)
        {
            bool Updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_docs]",
                           new List<SqlParameter>()
                   {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@opr_upd", "NV"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_cur_status","Vry"),
                                new SqlParameter("@gltd_prepare_user", finnobj.gltd_prepare_user),
                                new SqlParameter("@gltd_verify_user", finnobj.gltd_verify_user),
                                new SqlParameter("@gltd_verify_date", db.DBYYYYMMDDformat(finnobj.gltd_verify_date))
                                


                   });

                    if (upd > 0)
                    {
                        Updated = true;
                    }
                    else
                    {
                        Updated = false;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Updated);


                }
            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }

        //Verify Update
        [Route("UpdateFinn140_temp_docs")]
        public HttpResponseMessage UpdateFinn140_temp_docs(List<Finn141> data)
        {
            bool Updated = false;
            string status = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {

                        if (finnobj.gltd_verify == true)
                            status = "Aut";
                        else if (finnobj.gltd_revert == true)
                            status = "Rev";
                        else
                            status = "Vry";

                        int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_docs]",
                               new List<SqlParameter>()
                   {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@opr_upd", "NC"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_cur_status",status),
                                new SqlParameter("@gltd_verify_user", finnobj.gltd_verify_user),
                                new SqlParameter("@gltd_verify_date", db.DBYYYYMMDDformat(finnobj.gltd_verify_date))

                   });

                        if (upd > 0)
                        {
                            Updated = true;
                        }
                        else
                        {
                            Updated = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Updated);
                }
            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }

        //Authorize Update
        [Route("UpdateFinn142_temp_docs")]
        public HttpResponseMessage UpdateFinn142_temp_docs(List<Finn141> data)
        {
            bool Updated = false;
            string status = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {

                        if (finnobj.gltd_authorize == true)
                            status = "Pos";
                        else if (finnobj.gltd_verify == true)
                            status = "Vry";
                        else
                            status = "Aut";



                        int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_docs]",
                               new List<SqlParameter>()
                   {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@opr_upd", "ND"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_cur_status",status),
                                new SqlParameter("@gltd_authorize_user", finnobj.gltd_authorize_user),
                                new SqlParameter("@gltd_authorize_date", db.DBYYYYMMDDformat(finnobj.gltd_authorize_date))
                   });

                        if (upd > 0)
                        {
                            Updated = true;
                        }
                        else
                        {
                            Updated = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Updated);
                }
            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }


        //Verify search
        [Route("getFinn140_SearchFins_temp_docs")]
        public HttpResponseMessage getFinn140_SearchFins_temp_docs(string compcode, string username)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";

            Message message = new Message();

            List<Finn141> com_list = new List<Finn141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "NG"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Vry"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();

                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date =db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_prepare_user = dr["gltd_prepare_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_prepare_date"].ToString()))
                                finnobj.gltd_prepare_date = db.UIDDMMYYYYformat(dr["gltd_prepare_date"].ToString());
                            finnobj.gltd_verify = false;
                            finnobj.gltd_revert = false;
                            com_list.Add(finnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getFinn140_SearchFins_temp_docs_DPS")]
        public HttpResponseMessage getFinn140_SearchFins_temp_docs_DPS(string compcode, string username)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";

            Message message = new Message();

            List<Finn141> com_list = new List<Finn141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs_DPS",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "NG"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Vry"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();

                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_verify = false;
                            finnobj.gltd_revert = false;
                            com_list.Add(finnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }



        //Authorize search
        [Route("getFinn142_SearchFins_temp_docs")]
        public HttpResponseMessage getFinn142_SearchFins_temp_docs(string compcode, string username)
        {
            List<Finn141> com_list = new List<Finn141>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "NP"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Aut"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();
                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date =db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                finnobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_prepare_user = dr["gltd_prepare_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_prepare_date"].ToString()))
                                finnobj.gltd_prepare_date = db.UIDDMMYYYYformat(dr["gltd_prepare_date"].ToString());
                            finnobj.gltd_verify_user = dr["gltd_verify_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                finnobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            finnobj.gltd_authorize = false;
                            finnobj.gltd_verify = false;
                            com_list.Add(finnobj);
                            
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getFinn142_SearchFins_temp_docs_DPS")]
        public HttpResponseMessage getFinn142_SearchFins_temp_docs_DPS(string compcode, string username)
        {
            List<Finn141> com_list = new List<Finn141>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs_DPS",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "NP"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Aut"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();
                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                finnobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_authorize = false;
                            finnobj.gltd_verify = false;
                            com_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("Getdoc_cd_prov_no")]
        public HttpResponseMessage Getdoc_cd_prov_no(string doc_prov_no, string comp_code)
        {
            string doc_cd_final_no = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@gltd_comp_code", comp_code),
                            new SqlParameter("@prv_no", doc_prov_no),
                            
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            doc_cd_final_no = dr["gltd_final_doc_no"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_cd_final_no);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_cd_final_no);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("RollBack_Fins_temp_doc_details")]
        public HttpResponseMessage RollBack_Fins_temp_doc_details(string comp_cd, string doccd, string prv_no)
        {
            bool Updated = false;
            string status = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                           new List<SqlParameter>()
                   {
                                new SqlParameter("@opr", 'D'),
                                new SqlParameter("@gldd_comp_code", comp_cd),
                                new SqlParameter("@gldd_doc_code", doccd),
                                new SqlParameter("@gldd_prov_doc_no",prv_no)
                                
                   });

                    if (upd > 0)
                    {
                        Updated = true;
                    }
                    else
                    {
                        Updated = false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Updated);

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }

        [Route("Recurring_Fins_temp_doc_details")]
        public HttpResponseMessage Recurring_Fins_temp_doc_details(string comp_cd, string doccd, string prv_no, string recur_period, string recur_days)
        {
            bool Updated = false;
            string status = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int upd = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                           new List<SqlParameter>()
                   {
                                new SqlParameter("@opr", 'B'),
                                new SqlParameter("@gldd_comp_code", comp_cd),
                                new SqlParameter("@gldd_doc_code", doccd),
                                new SqlParameter("@gldd_prov_doc_no",prv_no),
                                new SqlParameter("@pc_bank_from",recur_days),
                                new SqlParameter("@gldd_cost_center_code",recur_period)

                                
                   });

                    if (upd > 0)
                    {
                        Updated = true;
                    }
                    else
                    {
                        Updated = false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Updated);

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }


        [Route("GetReportUrl")]
        public HttpResponseMessage GetReportUrl(string opr, string sub_opr)
        {
            string voucher_url = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("get_fins_report_url",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", opr),
                            new SqlParameter("@sub_opr", sub_opr),
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            voucher_url = dr["fins_appl_form_field_value1"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, voucher_url);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, voucher_url);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error To Get URL";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("Get_Check_Postdate")]
        public HttpResponseMessage Get_Check_Postdate(string post_date, string comp_cd, string fyear)
        {
            string return_message = string.Empty;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@opr_mem", comp_cd),
                            new SqlParameter("@tbl_cond",db.DBYYYYMMDDformat(post_date)),

                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["return_message"].ToString()))
                                return_message = dr["return_message"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, return_message);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, return_message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error To Check post date";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, return_message);
            }
        }

        //Update Voucher Application 

        [Route("UpdateVoucherCUD")]
        public HttpResponseMessage UpdateVoucherCUD(List<Fin201> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateVoucherCUD(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin201 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                            new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@opr_upd",simsobj.opr_upd),
                                new SqlParameter("@gldd_comp_code", simsobj.gldd_comp_code),
                                new SqlParameter("@gldd_doc_code", simsobj.gldd_doc_code),
                                new SqlParameter("@gldd_prov_doc_no", simsobj.gltd_prov_doc_no),
                                new SqlParameter("@gldd_final_doc_no", simsobj.gltd_final_doc_no),
                                new SqlParameter("@gldd_doc_narr", simsobj.gltd_doc_narr),
                                new SqlParameter("@gltd_remarks", simsobj.gltd_remarks),
                                new SqlParameter("@gldd_line_no", simsobj.gldd_line_no),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    
    }
}