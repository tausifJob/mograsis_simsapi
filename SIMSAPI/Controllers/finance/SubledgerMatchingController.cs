﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/SubledgerMatchingDetails")]
    public class SubledgerMatchingController:ApiController
    {

        //Get Sub Ledger Code
        [Route("GetLedgercode")]
        public HttpResponseMessage GetLedgercode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLedgercode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Fin143> ledger_list = new List<Fin143>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin143 simsobj = new Fin143();
                            simsobj.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            simsobj.sllc_ldgr_name = dr["sllc_ldgr_full_name"].ToString();
                            ledger_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ledger_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, ledger_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
              
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ledger_list);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetSLAccNumber")]
        public HttpResponseMessage GetSLAccNumber(string sltr_comp_code,string sltr_ldgr_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSLAccNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin143> accountno_list = new List<Fin143>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sltr_comp_code",sltr_comp_code),
                            new SqlParameter("@sltr_ldgr_code",sltr_ldgr_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin143 FinnObj = new Fin143();
                            FinnObj.sltr_slmast_acno = dr["slma_acno"].ToString();
                            FinnObj.sltr_slmast_acname = FinnObj.sltr_slmast_acno + "/" + dr["coad_pty_short_name"].ToString();
                            FinnObj.gldd_party_ref_no = dr["coad_xref_addr_id"].ToString();
                            FinnObj.gldd_dept_code = dr["coad_dept_no"].ToString();
                            FinnObj.gldd_dept_name = dr["codp_dept_name"].ToString();
                            accountno_list.Add(FinnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, accountno_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, accountno_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, accountno_list);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDocCode")]
        public HttpResponseMessage getDocCode(string sltr_comp_code)
        {
            List<Fin143> doc_list = new List<Fin143>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),
                             new SqlParameter("@sltr_comp_code",sltr_comp_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin143 obj = new Fin143();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.sltr_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            doc_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, doc_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, doc_list);
        }


        [Route("gettransactiondetails")]
        public HttpResponseMessage gettransactiondetails(string sltr_comp_code, string sltr_ldgr_code, string sltr_slmast_acno, string sltr_doc_code, string sltr_pstng_date,string sltr_doc_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : gettransactiondetails()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin143> SubledgerMatching_list = new List<Fin143>();
            decimal credit_tran_amt;

            if (sltr_pstng_date == "undefined" || sltr_pstng_date == "")
            {
                sltr_pstng_date = null;
            }
            if (sltr_doc_date == "undefined" || sltr_doc_date == "")
            {
                sltr_doc_date = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'L'),
                            new SqlParameter("@sltr_comp_code",sltr_comp_code),
                            new SqlParameter("@sltr_ldgr_code",sltr_ldgr_code),
                            new SqlParameter("@sltr_slmast_acno",sltr_slmast_acno),
                            new SqlParameter("@sltr_doc_code",sltr_doc_code),
                            new SqlParameter("@sltr_pstng_date",(sltr_pstng_date)),
                            new SqlParameter("@sltr_doc_date",(sltr_doc_date))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin143 FinnObj = new Fin143();
                            
                            FinnObj.sltr_year = dr["sltr_year"].ToString();
                            FinnObj.sltr_comp_code = dr["sltr_comp_code"].ToString();

                            FinnObj.sltr_dept_no = dr["sltr_dept_no"].ToString();
                            FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();

                            FinnObj.sltr_ldgr_code = dr["sltr_ldgr_code"].ToString();
                            FinnObj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();

                            FinnObj.sltr_slmast_acno = dr["sltr_slmast_acno"].ToString();
                            FinnObj.coad_pty_short_name = dr["coad_pty_short_name"].ToString();

                            FinnObj.sltr_pstng_date = (dr["sltr_pstng_date"].ToString());

                            FinnObj.sltr_doc_code = dr["sltr_doc_code"].ToString();
                            FinnObj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            credit_tran_amt = decimal.Parse(dr["sltr_tran_amt"].ToString());
                            if (credit_tran_amt< 0)//decimal.Parse(dr["sltr_tran_amt"].ToString()
                            {
                                
                                FinnObj.sltr_tran_amt_debite = (-(credit_tran_amt)).ToString();
                                FinnObj.sltr_tran_amt_credit = "-";
                            }
                            else
                            {
                                FinnObj.sltr_tran_amt_credit = ((credit_tran_amt)).ToString();
                                FinnObj.sltr_tran_amt_debite = "-";
                            }
                            FinnObj.sltr_our_doc_no = dr["sltr_our_doc_no"].ToString();
                            FinnObj.sltr_pstng_no = dr["sltr_pstng_no"].ToString();
                            FinnObj.sltr_pstng_seq_no = dr["sltr_pstng_seq_no"].ToString();
                            FinnObj.sltr_match_ind = dr["sltr_match_ind"].ToString().Equals("Y") ? true : false;

                            SubledgerMatching_list.Add(FinnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, SubledgerMatching_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, SubledgerMatching_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, SubledgerMatching_list);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDtransactiondetails")]
        public HttpResponseMessage CUDtransactiondetails(List<Fin143> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Fin143 routeobj in data)
                    {
                       
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_sl_transactions]",
                            new List<SqlParameter>()
                            {
                               
                                new SqlParameter("@opr",'U'),
                                new SqlParameter("@sltr_comp_code", routeobj.sltr_comp_code),
                                new SqlParameter("@sltr_ldgr_code", routeobj.sltr_ldgr_code),
                                new SqlParameter("@sltr_slmast_acno", routeobj.sltr_slmast_acno),
                                new SqlParameter("@sltr_pstng_no", routeobj.sltr_pstng_no),
                                new SqlParameter("@sltr_pstng_seq_no", routeobj.sltr_pstng_seq_no),
                                new SqlParameter("@sltr_year", routeobj.sltr_year),

                                new SqlParameter("@sltr_match_ind",routeobj.sltr_match_ind==true?"Y":"N")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}