﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;
using SIMSAPI.Models.FINNANCE;
using log4net;


namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/PettyCash")]
    public class PettyCashController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
 
        //Verify search

       
        [Route("getFinn140_SearchFins_temp_docs_DPS")]
        public HttpResponseMessage getFinn140_SearchFins_temp_docs_DPS(string compcode, string username)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";

            Message message = new Message();

            List<Finn141> com_list = new List<Finn141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs_DPS",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "PV"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Vry"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();

                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_verify = false;
                            finnobj.gltd_revert = false;
                            com_list.Add(finnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getFinn142_SearchFins_temp_docs_DPS")]
        public HttpResponseMessage getFinn142_SearchFins_temp_docs_DPS(string compcode, string username)
        {
            List<Finn141> com_list = new List<Finn141>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_temp_docs_DPS",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_upd", "PA"),
                            new SqlParameter("@gltd_comp_code", compcode),
                            new SqlParameter("@gltd_cur_status","Aut"),
                            new SqlParameter("@gltd_verify_user", username),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 finnobj = new Finn141();
                            finnobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            finnobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finnobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            finnobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                finnobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            finnobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            finnobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                finnobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                finnobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                finnobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());
                            finnobj.gltd_authorize = false;
                            finnobj.gltd_verify = false;
                            com_list.Add(finnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("GetVerifyAuthorizeUsers_PC")]
        public HttpResponseMessage GetVerifyAuthorizeUsers_PC(string username, string comp_code, string fyear, string doc_code)
        {

            List<string> com_list = new List<string>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_doc_users]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "P"),
                            new SqlParameter("@gltd_authorize_user", username),
                            new SqlParameter("@gldu_doc_code", doc_code),
                            new SqlParameter("@gldu_comp_code", comp_code),
                            new SqlParameter("@gldu_year", fyear)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            com_list.Add(dr["gldu_verify_user"].ToString());
                            com_list.Add(dr["gldu_authorize_user"].ToString());
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //PCRI Finance

        [Route("getAccount_details_Reim")]
        public HttpResponseMessage getAccount_details_Reim(string comp, string year, string final_doc, string amount, string c_date, string a_date, string narr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getapiNewButtonData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getapiNewButtonData"));

            List<Finn141> goaltarget_list = new List<Finn141>();
            if (final_doc == "undefined")
            {
                final_doc = null;
            }

            if (amount == "undefined")
            {
                amount = null;
            }

            if (c_date == "undefined")
            {
                c_date = null;
            }

            if (a_date == "undefined")
            {
                a_date = null;
            }
            if (narr == "undefined")
            {
                narr = null;
            }
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'G'),
                            new SqlParameter("@glpc_comp_code", comp),
                            new SqlParameter("@glpc_year", year),
                            new SqlParameter("@gltd_final_doc_no", final_doc),
                            new SqlParameter("@glpc_max_limit", amount),
                            new SqlParameter("@glpc_creation_date", db.DBYYYYMMDDformat(c_date)),
                            new SqlParameter("@glpc_authorize_date", db.DBYYYYMMDDformat(a_date)),
                            new SqlParameter("@search", narr),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 simsobj = new Finn141();

                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.glpc_expense_acno = dr["glpc_expense_acno"].ToString();
                            simsobj.glpc_petty_cash_acno = dr["glpc_petty_cash_acno"].ToString();
                            simsobj.glpc_expense_acno_name = dr["glpc_expense_acno_name"].ToString();
                            simsobj.glpc_petty_cash_acno_name = dr["glpc_petty_cash_acno_name"].ToString();
                            simsobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["glpc_amount"].ToString()))
                                simsobj.gldd_doc_amount_credit = decimal.Parse(dr["glpc_amount"].ToString());
                            simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAccount_details_fill_table")]
        public HttpResponseMessage getAccount_details_fill_table(string final_doc_no, string doc_cd, string cmp_cd, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccount_details_fill_table(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccount_details_fill_table"));

            List<Finn141> mod_list = new List<Finn141>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@glpc_comp_code", cmp_cd),
                            new SqlParameter("@glpc_year", fyear),
                            new SqlParameter("@glpc_dept_no", doc_cd),
                            new SqlParameter("@gltd_final_doc_no", final_doc_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 simsobj = new Finn141();

                            simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            simsobj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            simsobj.gldd_acct_name = dr["gldd_acct_name"].ToString();

                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                                simsobj.gldd_doc_amount = decimal.Parse(dr["gldd_doc_amount"].ToString());

                            simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                            simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                            simsobj.gldd_cost_center_code = dr["gldd_cost_center_code"].ToString();
                            simsobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();

                            mod_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        //PCDI 

        [Route("Insert_Fins_temp_docs_PCDI")]
        public HttpResponseMessage Insert_Fins_temp_docs_PCDI(List<Finn141> data)
        {
            string status = string.Empty;
            string prov_no = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {
                        if (finnobj.gltd_cur_status.Equals("Save"))
                        {
                            status = "Sav";
                        }
                        else if (finnobj.gltd_cur_status.Equals("Verify"))
                        {
                            status = "Vry";
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_docs_DPS]",
                       new List<SqlParameter>()
                   {

                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@opr_upd", "AP"),
                                new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                                new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                                new SqlParameter("@gltd_prov_doc_no", finnobj.gltd_prov_doc_no),
                                new SqlParameter("@gltd_final_doc_no", finnobj.gltd_final_doc_no),
                                new SqlParameter("@gltd_doc_date",db.DBYYYYMMDDformat(finnobj.gltd_doc_date)),
                                new SqlParameter("@gltd_post_date", db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                                new SqlParameter("@gltd_doc_narr", finnobj.gltd_doc_narr),
                                new SqlParameter("@gltd_remarks", finnobj.gltd_remarks),
                                new SqlParameter("@gltd_doc_flag", ""),
                                new SqlParameter("@gltd_cur_status",status),
                                new SqlParameter("@gltd_prepare_user", finnobj.gltd_prepare_user),
                                new SqlParameter("@gltd_prepare_date", db.DBYYYYMMDDformat(finnobj.gltd_prepare_date)),
                                new SqlParameter("@gltd_verify_user", finnobj.gltd_verify_user),
                                new SqlParameter("@gltd_verify_date", db.DBYYYYMMDDformat(finnobj.gltd_verify_date)),
                                new SqlParameter("@gltd_authorize_user", finnobj.gltd_authorize_user),
                                new SqlParameter("@gltd_authorize_date",db.DBYYYYMMDDformat(finnobj.gltd_authorize_date)),
                                new SqlParameter("@gltd_paid_to", finnobj.gltd_paid_to),
                                new SqlParameter("@gltd_cheque_no", finnobj.gltd_cheque_no),
                                new SqlParameter("@gltd_batch_source", finnobj.gltd_batch_source),
                                new SqlParameter("@glpc_amount", finnobj.glpc_amount),
                                new SqlParameter("@glpc_petty_cash_acno", finnobj.glpc_petty_cash_acno),
                                new SqlParameter("@glpc_expense_acno", finnobj.glpc_expense_acno),


                   });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["gltd_prov_doc_no"].ToString()))
                                    prov_no = dr["gltd_prov_doc_no"].ToString();
                            }
                        }

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, prov_no);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, prov_no);
        }


        [Route("Insert_Fins_temp_doc_details_PCRI")]
        public HttpResponseMessage Insert_Fins_temp_doc_details_PCRI(List<Finn141> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn141 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@gldd_comp_code", finnobj.gldd_comp_code),
                        new SqlParameter("@gldd_doc_code", finnobj.gldd_doc_code),
                        new SqlParameter("@gldd_prov_doc_no", finnobj.gldd_prov_doc_no),
                        new SqlParameter("@gldd_final_doc_no", finnobj.gldd_final_doc_no),
                        new SqlParameter("@gldd_line_no", finnobj.gldd_line_no),
                        new SqlParameter("@gldd_doc_narr", finnobj.gldd_doc_narr),
                        new SqlParameter("@gldd_ledger_code", finnobj.gldd_ledger_code),
                        new SqlParameter("@gldd_acct_code", finnobj.gldd_acct_code),
                        new SqlParameter("@gldd_doc_amount", finnobj.gldd_doc_amount),
                        new SqlParameter("@gldd_fc_code", finnobj.gldd_fc_code),
                        new SqlParameter("@gldd_fc_rate", finnobj.gldd_fc_rate),
                        new SqlParameter("@gldd_fc_amount", finnobj.gldd_fc_amount_debit),
                        new SqlParameter("@gldd_dept_code", finnobj.gldd_dept_code),
                        new SqlParameter("@gldd_party_ref_no", finnobj.gldd_party_ref_no),
                        new SqlParameter("@gldd_party_ref_date",db.DBYYYYMMDDformat(finnobj.gldd_party_ref_date)),
                        new SqlParameter("@pc_bank_from", finnobj.pc_bank_code),
                        new SqlParameter("@gldd_bank_date", null),
                        new SqlParameter("@posting_date", db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                        new SqlParameter("@gldd_cost_center_code", finnobj.gldd_cost_center_code),

                      });
                        if (ins > 0)
                        {
                            insert = true;
                            int i = 1;
                            foreach (Finn158 obj in finnobj.inner_data)
                            {
                                int inr_ins = db.ExecuteStoreProcedureforInsert("[fins_temp_doc_details_DPS]",
                                new List<SqlParameter>()
                      {
                        new SqlParameter("@opr", 'M'),
                        new SqlParameter("@gldd_comp_code", finnobj.gldd_comp_code),
                        new SqlParameter("@gldd_doc_code", finnobj.gldd_doc_code),
                        new SqlParameter("@gldd_prov_doc_no", finnobj.gldd_prov_doc_no),
                        new SqlParameter("@gldd_final_doc_no", finnobj.gldd_final_doc_no),
                        new SqlParameter("@gldd_fc_code", finnobj.gldd_line_no),
                        new SqlParameter("@gldd_line_no", i),
                        new SqlParameter("@gldd_party_ref_no", obj.glpcd_receipt_no),
                        new SqlParameter("@gldd_party_ref_date", db.DBYYYYMMDDformat(obj.glpcd_receipt_date)),
                        new SqlParameter("@gldd_doc_narr", obj.glpcd_receipt_narr),
                        new SqlParameter("@gldd_doc_amount", obj.glpcd_amount)
                      });
                                i = i + 1;

                            }
                        }



                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}