﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/BankRecancellation")]
    [BasicAuthentication]
    public class BankReconciliationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllBankRecancellation")]
        public HttpResponseMessage getAllBankRecancellation(string start_date,string end_date,string acct_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankRecancellation(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankRecancellation"));

            List<BankReCon> goaltarget_list = new List<BankReCon>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                //    DateTime sdate = DateTime.Parse(start_date);
                  //  DateTime edate = DateTime.Parse(end_date);
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bank_racancellation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@start_date",db.DBYYYYMMDDformat(start_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@acct_code", acct_code)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BankReCon simsobj = new BankReCon();
                          //simsobj.gltr_pstng_date = ((DateTime)dr["gltr_pstng_date"]).ToShortDateString();
                           simsobj.gltr_pstng_date = dr["gltr_pstng_date"].ToString();

                            simsobj.gltr_our_doc_no = dr["gltr_our_doc_no"].ToString();

                            simsobj.gltr_acct_code = dr["gltr_acct_code"].ToString();

                            simsobj.acct_name = dr["acct_name"].ToString();

                            simsobj.gltr_doc_narr = dr["gltr_doc_narr"].ToString();

                            simsobj.gltr_tran_amt = dr["gltr_tran_amt"].ToString(); 
                                                                                     
                            simsobj.glbr_doc_remark = dr["glbr_doc_remark"].ToString();


                          //  simsobj.glbr_bank_date = ((DateTime)dr["glbr_bank_date"]).ToShortDateString();
                            simsobj.glbr_bank_date = dr["glbr_bank_date"].ToString();

                            simsobj.glbr_creation_user = dr["glbr_creation_user"].ToString();

                            simsobj.glbr_creation_date = dr["glbr_creation_date"].ToString();

                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.acct_code = dr["acct_code"].ToString();


                            simsobj.glbr_doc_code = dr["glbr_doc_code"].ToString();
                            simsobj.glbr_comp_code = dr["glbr_comp_code"].ToString();
                            simsobj.glbr_cheque_no = dr["glbr_cheque_no"].ToString();
                            
                            
                            
                            simsobj.glbr_cheque_due_date = dr["glbr_cheque_due_date"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        //GetGLAccountNumber
        [Route("GetAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<BankReCon> doc_list = new List<BankReCon>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bank_racancellation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BankReCon simsobj = new BankReCon();
                            simsobj.gdua_acct_code = dr["gdua_acct_code"].ToString();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDBankRacancellation")]
        public HttpResponseMessage CUDBankRacancellation(List<BankReCon> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    

                    db.Open();
                    foreach (BankReCon simsobj in data)
                    {
                        if (simsobj.acct_code == "" || simsobj.acct_code=="undefined")
                        {
                            simsobj.acct_code = null;
                        }
                        if (simsobj.doc_no == "" || simsobj.doc_no == "undefined")
                        {
                            simsobj.doc_no = null;
                        }
                        if (simsobj.glbr_doc_code == "" || simsobj.glbr_doc_code == "undefined")
                        {
                            simsobj.glbr_doc_code = null;
                        }
                        if (simsobj.glbr_comp_code == "" || simsobj.glbr_comp_code == "undefined")
                        {
                            simsobj.glbr_comp_code = null;
                        }
                        if (simsobj.glbr_cheque_no == "" || simsobj.glbr_cheque_no == "undefined")
                        {
                            simsobj.glbr_cheque_no = null;
                        }
                        if (simsobj.glbr_cheque_due_date == "" || simsobj.glbr_cheque_due_date == "undefined")
                        {
                            simsobj.glbr_cheque_due_date = null;
                        }



                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bank_racancellation_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                new SqlParameter("@glbr_doc_remark", simsobj.glbr_doc_remark),
                                new SqlParameter("@glbr_bank_date", db.DBYYYYMMDDformat(simsobj.glbr_bank_date)),
                                new SqlParameter("@glbr_creation_date", db.DBYYYYMMDDformat(simsobj.glbr_creation_date)),
                                new SqlParameter("@glbr_creation_user", simsobj.glbr_creation_user),



                                new SqlParameter("@glbr_acct_code", simsobj.acct_code),
                                new SqlParameter("@glbr_our_doc_no", simsobj.doc_no),
                                new SqlParameter("@glbr_doc_code", simsobj.glbr_doc_code),
                                new SqlParameter("@glbr_comp_code", simsobj.glbr_comp_code),
                                new SqlParameter("@glbr_cheque_no", simsobj.glbr_cheque_no),
                              //  new SqlParameter("@glbr_cheque_due_date", db.DBYYYYMMDDformat(simsobj.glbr_cheque_due_date)),

                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }

}










