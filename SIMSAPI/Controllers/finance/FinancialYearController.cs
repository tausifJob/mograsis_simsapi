﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/Financialyear")]
    [BasicAuthentication]
    public class FinancialYearController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllFinancialYear")]
        public HttpResponseMessage getAllFinancialYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFinancialYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllFinancialYear"));

            List<Sim515> goaltarget_list = new List<Sim515>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_financial_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim515 simsobj = new Sim515();
                            simsobj.sims_financial_year = dr["sims_financial_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_financial_year_desc"].ToString();

                            simsobj.sims_financial_year_start_date = db.UIDDMMYYYYformat(dr["sims_financial_year_start_date"].ToString());
                            simsobj.sims_financial_year_end_date = db.UIDDMMYYYYformat(dr["sims_financial_year_end_date"].ToString());
                           
                            simsobj.sims_financial_year_status = dr["sims_financial_year_status"].ToString();
                            simsobj.sims_financial_year_status1 = dr["sims_financial_year_status1"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


      


        [Route("getDocumentStatus")]
        public HttpResponseMessage getDocumentStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocumentStatus()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "COMBOBOX1getDocumentType"));

            List<Sim515> doc_list = new List<Sim515>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_financial_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim515 simsobj = new Sim515();
                            simsobj.sims_financial_year_status = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_financial_year_status1 = dr["sims_appl_form_field_value1"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getDocumentDDLYear")]
        public HttpResponseMessage getDocumentDDLYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocumentDDLYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "COMBOBOX1getDocumentType"));

            List<Sim515> doc_list = new List<Sim515>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_financial_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim515 simsobj = new Sim515();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_financial_year_start_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            simsobj.sims_financial_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDFinantialYear")]
        public HttpResponseMessage CUDFinantialYear(List<Sim515> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim515 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_financial_year_proc]",
                        new List<SqlParameter>()
                         {

                                new SqlParameter("@opr", simsobj.opr),
                                
                                new SqlParameter("@sims_financial_year", simsobj.sims_financial_year),
                                new SqlParameter("@sims_financial_year_start_date", db.DBYYYYMMDDformat(simsobj.sims_financial_year_start_date)),
                                new SqlParameter("@sims_financial_year_end_date", db.DBYYYYMMDDformat(simsobj.sims_financial_year_end_date)),
                                new SqlParameter("@sims_financial_year_status", simsobj.sims_financial_year_status),

                          });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, insert);


            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }

}










