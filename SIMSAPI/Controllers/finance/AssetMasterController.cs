﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.finance
{
     [RoutePrefix("api/common/AssetMaster")]

    public class AssetMasterController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetFins_AssetMaster")]
        public HttpResponseMessage GetFins_AssetMaster(string type,string gam_items,string comp_cd, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFins_AssetMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetFins_AssetMaster"));

            if (type == "undefined" || type == "")
            {
                type = null;
            }
            if (gam_items == "undefined" || gam_items == "")
            {
                gam_items = null;
            }


            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "S"),
                                 new SqlParameter("@gam_comp_code", comp_cd),
                                 new SqlParameter("@fin_year", fyear),
                                 //new SqlParameter("@gam_asst_typename", ""),
                                 //new SqlParameter("@gam_item_no", 0),
                                 //new SqlParameter("@gam_location_name", ""),
                                 //new SqlParameter("@gam_desc_1", ""),
                                 //new SqlParameter("@gam_desc_2", ""),
                                 //new SqlParameter("@gam_supl_name", ""),
                                 //new SqlParameter("@gam_acct_code", ""),
                                 //new SqlParameter("@gam_order_no", ""),
                                 //new SqlParameter("@gam_invoice_no", ""),
                                 //new SqlParameter("@gam_new_sec", ""),
                                 //new SqlParameter("@gam_page_no", 0),
                                 //new SqlParameter("@gam_line_no", 0),
                                 //new SqlParameter("@gam_quantity", 0),
                                 //new SqlParameter("@gam_invoice_amount", 0),
                                 //new SqlParameter("@gam_val_add_cum", 0),
                                 //new SqlParameter("@gam_val_add_mtd", 0),
                                 //new SqlParameter("@gam_val_add_ytd", 0),
                                 //new SqlParameter("@gam_book_value", 0),
                                 //new SqlParameter("@gam_cum_deprn", 0),
                                 //new SqlParameter("@gam_ytd_deprn", 0),
                                 //new SqlParameter("@gam_mth_deprn", 0),
                                 //new SqlParameter("@gam_sale_value", 0),
                                 //new SqlParameter("@gam_repl_cost", 0),
                                 //new SqlParameter("@gam_receipt_date", ""),
                                 //new SqlParameter("@gam_sale_date", ""),
                                 //new SqlParameter("@gam_entry_date", ""),
                                 //new SqlParameter("@gam_amend_date", ""),
                                 //new SqlParameter("@gam_deprn_percent", 0),
                                 //new SqlParameter("@gam_used_on_item", 0),
                                 //new SqlParameter("@gam_status_name", ""),
                                 new SqlParameter("@type", type),
                                 new SqlParameter("@gam_item_no", gam_items),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_comp_code = dr["gam_comp_code"].ToString();
                            finnobj.gam_dept_code = dr["gam_dept_code"].ToString();
                            finnobj.gam_dept_code_name = dr["gam_dept_code"].ToString() + '-' + dr["gam_dept_code_name"].ToString();
                            finnobj.gam_asst_type = dr["assettype_code"].ToString();
                            finnobj.gam_asst_type_name = dr["gam_asst_type"].ToString();
                            finnobj.gam_item_no = dr["gam_item_no"].ToString();
                            finnobj.gam_item_no1 = dr["gam_item_no"].ToString();
                            finnobj.gam_location = dr["loc_code"].ToString();
                            finnobj.gam_location_name = dr["gam_location"].ToString();
                            finnobj.gam_desc_1 = dr["gam_desc_1"].ToString();
                            finnobj.gam_desc_2 = dr["gam_desc_2"].ToString();
                            finnobj.gam_supl_name = dr["sup_name"].ToString();
                            finnobj.gam_supl_code=dr["gam_supl_name"].ToString();
                            finnobj.gam_acct_code = dr["gam_acct_code"].ToString();
                            //finnobj.gam_acct_code_name = dr["gam_acct_name"].ToString();
                            finnobj.gam_order_no = dr["gam_order_no"].ToString();
                            finnobj.gam_invoice_no = dr["gam_invoice_no"].ToString();
                            finnobj.gam_new_sec = dr["gam_new_sec"].ToString();
                            if (string.IsNullOrEmpty(dr["gam_page_no"].ToString()) == false)
                                finnobj.gam_page_no = Decimal.Parse(dr["gam_page_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_line_no"].ToString()) == false)
                                finnobj.gam_line_no = Decimal.Parse(dr["gam_line_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_quantity"].ToString()) == false)
                                finnobj.gam_quantity = Decimal.Parse(dr["gam_quantity"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_invoice_amount"].ToString()) == false)
                                finnobj.gam_invoice_amount = Decimal.Parse(dr["gam_invoice_amount"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_cum"].ToString()) == false)
                                finnobj.gam_val_add_cum = Decimal.Parse(dr["gam_val_add_cum"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_mtd"].ToString()) == false)
                                finnobj.gam_val_add_mtd = Decimal.Parse(dr["gam_val_add_mtd"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_ytd"].ToString()) == false)
                                finnobj.gam_val_add_ytd = Decimal.Parse(dr["gam_val_add_ytd"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_book_value"].ToString()) == false)
                                finnobj.gam_book_value = Decimal.Parse(dr["gam_book_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_cum_deprn"].ToString()) == false)
                                finnobj.gam_cum_deprn = Decimal.Parse(dr["gam_cum_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_ytd_deprn"].ToString()) == false)
                                finnobj.gam_ytd_deprn = Decimal.Parse(dr["gam_ytd_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_mth_deprn"].ToString()) == false)
                                finnobj.gam_mth_deprn = Decimal.Parse(dr["gam_mth_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_sale_value"].ToString()) == false)
                                finnobj.gam_sale_value = Decimal.Parse(dr["gam_sale_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_repl_cost"].ToString()) == false)
                                finnobj.gam_repl_cost = Decimal.Parse(dr["gam_repl_cost"].ToString());
                            if (!string.IsNullOrEmpty(dr["gam_receipt_date"].ToString()))
                                finnobj.gam_receipt_date = db.UIDDMMYYYYformat(dr["gam_receipt_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gam_sale_date"].ToString()))
                                finnobj.gam_sale_date = db.UIDDMMYYYYformat(dr["gam_sale_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gam_entry_date"].ToString()))
                                finnobj.gam_entry_date = db.UIDDMMYYYYformat(dr["gam_entry_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gam_amend_date"].ToString()))
                                finnobj.gam_amend_date = db.UIDDMMYYYYformat(dr["gam_amend_date"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_deprn_percent"].ToString()) == false)
                                finnobj.gam_deprn_percent = Decimal.Parse(dr["gam_deprn_percent"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_used_on_item"].ToString()) == false)
                                finnobj.gam_used_on_item = Decimal.Parse(dr["gam_used_on_item"].ToString());
                            finnobj.gam_status_name = dr["gam_statusValue"].ToString();
                            finnobj.gam_status_code = dr["gam_status"].ToString();
                            finnobj.empcode = dr["ass_empid"].ToString();
                            //finnobj.empname = dr[""].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetFins_AssetMaster_view")]
        public HttpResponseMessage GetFins_AssetMaster_view(string department, string asset_tyep, string desc, string comp_cd, string fyear)
        {
            List<Finn021> trans_list = new List<Finn021>();

            if (department == "undefined")
            {
                department = null;
            }
            if (asset_tyep == "undefined")
            {
                asset_tyep = null;
            }
            if (desc == "undefined")
            {
                desc = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                
                new SqlParameter("@opr", 'V'),

                 new SqlParameter("@gam_dept_code",department),
                 new SqlParameter("@gam_asst_type", asset_tyep),
                 new SqlParameter("@gam_desc_1", desc),
                 new SqlParameter("@gam_comp_code", comp_cd),
                 new SqlParameter("@fin_year", fyear)
                //new SqlParameter("@gam_asst_typename", ""),
                //new SqlParameter("@gam_item_no", 0),
                //new SqlParameter("@gam_location_name", ""),

                //new SqlParameter("@gam_desc_1", ""),
                //new SqlParameter("@gam_desc_2", ""),
                //new SqlParameter("@gam_supl_name", ""),
                //new SqlParameter("@gam_acct_code", ""),
                //new SqlParameter("@gam_order_no", ""),

                //new SqlParameter("@gam_invoice_no", ""),
                //new SqlParameter("@gam_new_sec", ""),
                //new SqlParameter("@gam_page_no", 0),
                //new SqlParameter("@gam_line_no", 0),
                //new SqlParameter("@gam_quantity", 0),

                //new SqlParameter("@gam_invoice_amount", 0),
                //new SqlParameter("@gam_val_add_cum", 0),
                //new SqlParameter("@gam_val_add_mtd", 0),
                //new SqlParameter("@gam_val_add_ytd", 0),
                //new SqlParameter("@gam_book_value", 0),

                //new SqlParameter("@gam_cum_deprn", 0),
                //new SqlParameter("@gam_ytd_deprn", 0),
                //new SqlParameter("@gam_mth_deprn", 0),
                //new SqlParameter("@gam_sale_value", 0),
                //new SqlParameter("@gam_repl_cost", 0),

                //new SqlParameter("@gam_receipt_date", ""),
                //new SqlParameter("@gam_sale_date", ""),
                //new SqlParameter("@gam_entry_date", ""),
                //new SqlParameter("@gam_amend_date", ""),
                //new SqlParameter("@gam_deprn_percent", 0),

                //new SqlParameter("@gam_used_on_item", 0),
                //new SqlParameter("@gam_status_name", "")

                         });
                    while (dr.Read())
                    {
                        Finn021 simsobj = new Finn021();
                        simsobj.gam_comp_name = dr["comp_name"].ToString();
                        simsobj.gam_comp_code = dr["gam_comp_code"].ToString();
                        simsobj.gam_dept_code = dr["gam_dept_code"].ToString();
                        simsobj.gam_dept_code_name = dr["gam_dept_code"].ToString() + '-' + dr["gam_dept_code_name"].ToString();
                        simsobj.gam_asst_type_name = dr["gam_asst_type"].ToString();
                        simsobj.gam_item_no = dr["gam_item_no"].ToString();
                        simsobj.gam_item_no1 = dr["gam_item_no"].ToString();
                        simsobj.gam_location_name = dr["gam_location"].ToString();
                        simsobj.gam_desc_1 = dr["gam_desc_1"].ToString();
                        simsobj.gam_desc_2 = dr["gam_desc_2"].ToString();
                        simsobj.gam_supl_name = dr["gam_supl_name"].ToString();
                        simsobj.gam_acct_code = dr["gam_acct_code"].ToString();
                        simsobj.gam_acct_code_name = dr["gam_acct_name"].ToString();
                        simsobj.gam_order_no = dr["gam_order_no"].ToString();

                        simsobj.gam_invoice_no = dr["gam_invoice_no"].ToString();
                        simsobj.gam_new_sec = dr["gam_new_sec"].ToString();
                        if (string.IsNullOrEmpty(dr["gam_page_no"].ToString()) == false)
                            simsobj.gam_page_no = Decimal.Parse(dr["gam_page_no"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_line_no"].ToString()) == false)
                            simsobj.gam_line_no = Decimal.Parse(dr["gam_line_no"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_quantity"].ToString()) == false)
                            simsobj.gam_quantity = Decimal.Parse(dr["gam_quantity"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_invoice_amount"].ToString()) == false)
                            simsobj.gam_invoice_amount = Decimal.Parse(dr["gam_invoice_amount"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_val_add_cum"].ToString()) == false)
                            simsobj.gam_val_add_cum = Decimal.Parse(dr["gam_val_add_cum"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_val_add_mtd"].ToString()) == false)
                            simsobj.gam_val_add_mtd = Decimal.Parse(dr["gam_val_add_mtd"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_val_add_ytd"].ToString()) == false)
                            simsobj.gam_val_add_ytd = Decimal.Parse(dr["gam_val_add_ytd"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_book_value"].ToString()) == false)
                            simsobj.gam_book_value = Decimal.Parse(dr["gam_book_value"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_cum_deprn"].ToString()) == false)
                            simsobj.gam_cum_deprn = Decimal.Parse(dr["gam_cum_deprn"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_ytd_deprn"].ToString()) == false)
                            simsobj.gam_ytd_deprn = Decimal.Parse(dr["gam_ytd_deprn"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_mth_deprn"].ToString()) == false)
                            simsobj.gam_mth_deprn = Decimal.Parse(dr["gam_mth_deprn"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_sale_value"].ToString()) == false)
                            simsobj.gam_sale_value = Decimal.Parse(dr["gam_sale_value"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_repl_cost"].ToString()) == false)
                            simsobj.gam_repl_cost = Decimal.Parse(dr["gam_repl_cost"].ToString());
                        if (!string.IsNullOrEmpty(dr["gam_receipt_date"].ToString()))
                            simsobj.gam_receipt_date = db.UIDDMMYYYYformat(dr["gam_receipt_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["gam_sale_date"].ToString()))
                            simsobj.gam_sale_date = db.UIDDMMYYYYformat(dr["gam_sale_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["gam_entry_date"].ToString()))
                            simsobj.gam_entry_date = db.UIDDMMYYYYformat(dr["gam_entry_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["gam_amend_date"].ToString()))
                            simsobj.gam_amend_date = db.UIDDMMYYYYformat(dr["gam_amend_date"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_deprn_percent"].ToString()) == false)
                            simsobj.gam_deprn_percent = Decimal.Parse(dr["gam_deprn_percent"].ToString());
                        if (string.IsNullOrEmpty(dr["gam_used_on_item"].ToString()) == false)
                            simsobj.gam_used_on_item = Decimal.Parse(dr["gam_used_on_item"].ToString());
                        simsobj.gam_status_name = dr["gam_statusValue"].ToString();
                        simsobj.gam_status = dr["gam_status"].ToString();
                        trans_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }

        [Route("GetAutoGenerate_AssetItemNumber")]
        public HttpResponseMessage GetAutoGenerate_AssetItemNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAutoGenerate_AssetItemNumber(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAutoGenerate_AssetItemNumber"));
            Finn021 obj = new Finn021();
            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            if (dr["max"].ToString() != "")
                                obj.gam_item_no = dr["max"].ToString();
                            else
                                obj.gam_item_no = "1";
                            mod_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj.gam_item_no);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, obj.gam_item_no);
            }
        }

        [Route("GetAllDepartmentName")]
        public HttpResponseMessage GetAllDepartmentName(string comp_code,string fins_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllDepartmentName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllDepartmentName"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@gam_comp_code",comp_code),
                             new SqlParameter("@fin_year",fins_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 simsObj = new Finn021();
                            simsObj.gam_dept_code = dr["codp_dept_no"].ToString();
                            simsObj.gam_dept_code_name = dr["codp_dept_no"].ToString() + "-" + dr["codp_dept_name"].ToString();
                            mod_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAssetType")]
        public HttpResponseMessage GetAssetType(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAssetType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAssetType"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@gam_comp_code", comp_code)
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 simsobj = new Finn021();
                            simsobj.gam_asst_type_name = dr["gal_type_desc"].ToString();
                            simsobj.gam_asst_type = dr["gal_asst_type"].ToString();
                            simsobj.gam_acct_code = dr["gal_asset_acno"].ToString();
                            if (!string.IsNullOrEmpty(dr["gal_deprn_percent"].ToString()))
                                simsobj.gam_deprn_percent = decimal.Parse(dr["gal_deprn_percent"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllLocationName")]
        public HttpResponseMessage GetAllLocationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLocationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllLocationName"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "E")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 simsObj = new Finn021();
                            simsObj.gam_location = dr["sims_location_code"].ToString();
                            simsObj.gam_location_name = dr["sims_location_desc"].ToString();
                            mod_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAccountNo")]
        public HttpResponseMessage GetAccountNo(string asset_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAccountNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAccountNo"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@gam_asst_typename",asset_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_acct_code = dr["gal_asset_acno"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllSupplierNames")]
        public HttpResponseMessage GetAllSupplierNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSupplierNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSupplierNames"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "G")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_supl_code = dr["sup_code"].ToString();
                            finnobj.gam_supl_name = dr["sup_name"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAllOrderNo")]
        public HttpResponseMessage GetAllOrderNo()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllOrderNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllOrderNo"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "H")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_order_no = dr["ord_no"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("GetAssetStatus")]
        public HttpResponseMessage GetAssetStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAssetStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetAssetStatus"));

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "J")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_status_code = dr["fins_appl_parameter"].ToString();
                            finnobj.gam_status_name = dr["fins_appl_form_field_value1"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDInsertFins_AssetMaster")]
        public HttpResponseMessage CUDInsertFins_AssetMaster(List<Finn021> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubLedgerMaster(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "SubLedgerMaster"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn021 finnobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@gam_comp_code", finnobj.gam_comp_code),
                                new SqlParameter("@gam_dept_name", finnobj.gam_dept_code),
                                new SqlParameter("@gam_asst_typename", finnobj.gam_asst_type),
                                new SqlParameter("@gam_item_no", finnobj.gam_item_no),
                                new SqlParameter("@gam_location_name", finnobj.gam_location),
                                new SqlParameter("@gam_desc_1", finnobj.gam_desc_1),
                                new SqlParameter("@gam_desc_2", finnobj.gam_desc_2),
                                new SqlParameter("@gam_supl_name", finnobj.gam_supl_code),
                                new SqlParameter("@gam_acct_code", finnobj.gam_acct_code),
                                new SqlParameter("@gam_order_no", finnobj.gam_order_no),
                                new SqlParameter("@gam_invoice_no", finnobj.gam_invoice_no),
                                new SqlParameter("@gam_new_sec", finnobj.gam_new_sec),
                                new SqlParameter("@gam_page_no", finnobj.gam_page_no),
                                new SqlParameter("@gam_line_no", finnobj.gam_line_no),
                                new SqlParameter("@gam_quantity", finnobj.gam_quantity),
                                new SqlParameter("@gam_invoice_amount",finnobj.gam_invoice_amount),
                                new SqlParameter("@gam_val_add_cum",finnobj.gam_val_add_cum),
                                new SqlParameter("@gam_val_add_mtd",finnobj.gam_val_add_mtd),
                                new SqlParameter("@gam_val_add_ytd",finnobj.gam_val_add_ytd),
                                new SqlParameter("@gam_book_value",finnobj.gam_book_value),
                                new SqlParameter("@gam_cum_deprn",finnobj.gam_cum_deprn),
                                new SqlParameter("@gam_ytd_deprn",finnobj.gam_ytd_deprn),
                                new SqlParameter("@gam_mth_deprn",finnobj.gam_mth_deprn),
                                new SqlParameter("@gam_sale_value",finnobj.gam_sale_value),
                                new SqlParameter("@gam_repl_cost",finnobj.gam_repl_cost),
                                new SqlParameter("@gam_receipt_date", db.DBYYYYMMDDformat(finnobj.gam_receipt_date)),
                                new SqlParameter("@gam_sale_date", db.DBYYYYMMDDformat(finnobj.gam_sale_date)),
                                new SqlParameter("@gam_entry_date", db.DBYYYYMMDDformat(finnobj.gam_entry_date)),
                                new SqlParameter("@gam_amend_date", db.DBYYYYMMDDformat(finnobj.gam_amend_date)),
                                new SqlParameter("@gam_deprn_percent",finnobj.gam_deprn_percent),
                                new SqlParameter("@gam_used_on_item",finnobj.gam_used_on_item),
                                new SqlParameter("@gam_status_name", finnobj.gam_status_code),
                                new SqlParameter("@empcode", finnobj.empcode),

                             });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDUpdateFins_AssetMaster")]
        public HttpResponseMessage CUDUpdateFins_AssetMaster(List<Finn021> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubLedgerMaster(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "SubLedgerMaster"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn021 finnobj in data)
                        {
                             int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_asset_master_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@gam_comp_code", finnobj.gam_comp_code),
                                new SqlParameter("@gam_dept_name", finnobj.gam_dept_code),
                                new SqlParameter("@gam_asst_typename", finnobj.gam_asst_type),
                                new SqlParameter("@gam_item_no", finnobj.gam_item_no),
                                new SqlParameter("@gam_location_name", finnobj.gam_location),
                                new SqlParameter("@gam_desc_1", finnobj.gam_desc_1),
                                new SqlParameter("@gam_desc_2", finnobj.gam_desc_2),
                                new SqlParameter("@gam_supl_name", finnobj.gam_supl_code),
                                new SqlParameter("@gam_acct_code", finnobj.gam_acct_code),
                                new SqlParameter("@gam_order_no", finnobj.gam_order_no),
                                new SqlParameter("@gam_invoice_no", finnobj.gam_invoice_no),
                                new SqlParameter("@gam_new_sec", finnobj.gam_new_sec),
                                new SqlParameter("@gam_page_no", finnobj.gam_page_no),
                                new SqlParameter("@gam_line_no", finnobj.gam_line_no),
                                new SqlParameter("@gam_quantity", finnobj.gam_quantity),
                                new SqlParameter("@gam_invoice_amount",finnobj.gam_invoice_amount),
                                new SqlParameter("@gam_val_add_cum",finnobj.gam_val_add_cum),
                                new SqlParameter("@gam_val_add_mtd",finnobj.gam_val_add_mtd),
                                new SqlParameter("@gam_val_add_ytd",finnobj.gam_val_add_ytd),
                                new SqlParameter("@gam_book_value",finnobj.gam_book_value),
                                new SqlParameter("@gam_cum_deprn",finnobj.gam_cum_deprn),
                                new SqlParameter("@gam_ytd_deprn",finnobj.gam_ytd_deprn),
                                new SqlParameter("@gam_mth_deprn",finnobj.gam_mth_deprn),
                                new SqlParameter("@gam_sale_value",finnobj.gam_sale_value),
                                new SqlParameter("@gam_repl_cost",finnobj.gam_repl_cost),
                                new SqlParameter("@gam_receipt_date", db.DBYYYYMMDDformat(finnobj.gam_receipt_date)),
                                new SqlParameter("@gam_sale_date", db.DBYYYYMMDDformat(finnobj.gam_sale_date)),
                                new SqlParameter("@gam_entry_date", db.DBYYYYMMDDformat(finnobj.gam_entry_date)),
                                new SqlParameter("@gam_amend_date", db.DBYYYYMMDDformat(finnobj.gam_amend_date)),
                                new SqlParameter("@gam_deprn_percent",finnobj.gam_deprn_percent),
                                new SqlParameter("@gam_used_on_item",finnobj.gam_used_on_item),
                                new SqlParameter("@gam_status_name", finnobj.gam_status_code),
                                new SqlParameter("@empcode", finnobj.empcode),

                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

         //Custodian Change

        [Route("GetCustodianEmployees")]
        public HttpResponseMessage GetCustodianEmployees()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCustodianEmployees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetCustodianEmployees"));
                       
            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "L"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.empcode = dr["em_number"].ToString();
                            finnobj.empname = dr["empname"].ToString();
                            
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }
    }
}