﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/schedulegroupdetails")]
    public class ScheduleGroupDetailsController : ApiController
    {
        [Route("getGroupName")]
        public HttpResponseMessage getGroupName()
        {
            List<Fin116> SGNameDetails = new List<Fin116>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedule_group_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin116 obj = new Fin116();
                            obj.glsg_group_code = dr["glsg_group_code"].ToString();
                            obj.glsg_group_description = dr["glsg_group_description"].ToString();
                            obj.codegroupdes = dr["codegroupdes"].ToString();


                            SGNameDetails.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SGNameDetails);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SGNameDetails);
        }

        [Route("getScheduleCode")]
        public HttpResponseMessage getScheduleCode()
        {
            List<Fin116> SGCodeDetails = new List<Fin116>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedule_group_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin116 obj = new Fin116();
                            obj.glsc_sched_code = dr["glsc_sched_code"].ToString();
                            obj.glsc_sched_name = dr["glsc_sched_name"].ToString();
                            obj.glsccodename = dr["glsccodename"].ToString();


                            SGCodeDetails.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SGCodeDetails);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SGCodeDetails);
        }

        [Route("getScheduleGroupDetail")]
        public HttpResponseMessage getScheduleGroupDetail(string comp_code,string finance_year)
        {
            List<Fin116> SGDetails = new List<Fin116>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedule_group_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@gsgd_comp_code", comp_code),
                            new SqlParameter("@year", finance_year),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin116 obj = new Fin116();
                            obj.gsgd_comp_code = dr["gsgd_comp_code"].ToString();
                            obj.gsgd_group_code = dr["gsgd_group_code"].ToString();
                            obj.codegroupdes = dr["codegroupdes"].ToString();
                            obj.glsccodename = dr["glsccodename"].ToString();
                            obj.gsgd_schedule_code = dr["gsgd_schedule_code"].ToString();
                            if (dr["gsgd_sign"].ToString() == "Y")
                                obj.gsgd_sign = true;
                            else
                                obj.gsgd_sign = false;

                            SGDetails.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SGDetails);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SGDetails);
        }

        [Route("ScheduleGroupDetailCUD")]
        public HttpResponseMessage ScheduleGroupDetailCUD(List<Fin116> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin116 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_schedule_group_details_proc]",
                            new List<SqlParameter>()

                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@gsgd_comp_code", "1"),
                                 new SqlParameter("@gsgd_group_code", simsobj.gsgd_group_code),
                                 new SqlParameter("@gsgd_schedule_code", simsobj.gsgd_schedule_code),
                                 new SqlParameter("@gsgd_sign", simsobj.gsgd_sign==true?"Y":"N")
                                  

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}