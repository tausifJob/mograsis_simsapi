﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/FeePosting")]
    [BasicAuthentication]
    public class FeePostingPaymentModeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getFeePosting")]
        public HttpResponseMessage getFeePosting(string comp_code, string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFeePosting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getFeePosting"));

            List<Fin207> goaltarget_list = new List<Fin207>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_fee_posting_payment_mode",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@fins_fee_cur_code", comp_code),
                            new SqlParameter("@fins_fee_academic_year", finance_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin207 simsobj = new Fin207();
                            simsobj.fins_fee_cur_code = dr["fins_fee_cur_code"].ToString();
                            simsobj.fins_fee_cur_name = dr["fins_fee_cur_name"].ToString();
                            simsobj.fins_fee_academic_year = dr["fins_fee_academic_year"].ToString();
                            simsobj.fins_fee_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.fins_fee_payment_type = dr["fins_fee_payment_type"].ToString();
                            simsobj.fins_fee_payment_type_desc = dr["fins_fee_payment_type_desc"].ToString();
                            simsobj.fins_fee_acno_name = dr["payment_acct_name"].ToString();
                            simsobj.fins_fee_acno = dr["fins_fee_acno"].ToString();
                            simsobj.fins_fee_advance_acno = dr["fins_fee_advance_acno"].ToString();
                            simsobj.fins_fee_advance_acno_name = dr["advance_acct_name"].ToString();
                            simsobj.fins_fee_refund_acno = dr["fins_fee_refund_acno"].ToString();
                            simsobj.fins_fee_refund_acno_name = dr["refund_acct_name"].ToString();
                            simsobj.fins_fee_posting_status = dr["fins_fee_posting_status"].Equals("A")? true :false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("GetPaymentMode")]
        public HttpResponseMessage GetPaymentMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetPaymentMode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetPaymentMode"));

            List<Fin207> doc_list = new List<Fin207>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_fee_posting_payment_mode",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin207 simsobj = new Fin207();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetGLAccountNumber
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber(string comp_code, string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin138> doc_list = new List<Fin138>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_gl_masters",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@glma_comp_code", comp_code),
                            new SqlParameter("@glma_year", finance_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin138 simsobj = new Fin138();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["glma_acct_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDFeePostingPaymentMode")]
        public HttpResponseMessage CUDFeePostingPaymentMode(List<Fin207> data)
        {
            //  bool insert = false;
            Message message = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin207 simsobj in data)
                    {
                        // int ins = db.ExecuteStoreProcedureforInsert("fins_fee_posting_payment_mode",
                        SqlDataReader dr = db.ExecuteStoreProcedure("fins_fee_posting_payment_mode",
                         new List<SqlParameter>()
                      {

                                new SqlParameter("@opr", simsobj.opr),

                                new SqlParameter("@fins_fee_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@fins_fee_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@fins_fee_payment_type", simsobj.sims_appl_parameter),
                                new SqlParameter("@fins_fee_acno", simsobj.acno),
                                new SqlParameter("@fins_fee_advance_acno", simsobj.acno_code),
                                new SqlParameter("@fins_fee_acno_old",simsobj.fins_fee_acno_old),
                                new SqlParameter("@fins_fee_refund_acno", simsobj.fins_fee_refund_acno),
                                new SqlParameter("@fins_fee_posting_status", simsobj.fins_fee_posting_status==true?"A":"I")
                        });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            //  inserted = false;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            // inserted = true;
                        }
                        if (inserted == true)
                        {
                            // message.strMessage = "Inserterd Successfully";
                            //  inserted = true;
                            st = dr[0].ToString();
                            message.strMessage = st;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
               
            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, message);
        }
          
    }

}










