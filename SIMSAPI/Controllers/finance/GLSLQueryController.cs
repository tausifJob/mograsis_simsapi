﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/GLSLQuery")]
    public class GLSLQueryController : ApiController
    {



        [Route("list_All_GL_Query")]
        public HttpResponseMessage list_All_GL_Query(Finn064 obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : list_All_GL_Query(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAllPDCSubmission"));

            List<Finn064> gl_querylist = new List<Finn064>();

            //int total = 0, skip = 0;


            decimal gltr_total_dr_amount = 0;
            decimal gltr_total_cr_amount = 0;
            decimal gltr_running_total_amt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_gl_transactions",
                        new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@sub_opr", "GQH"),
                                    new SqlParameter("@gltr_comp_code",obj.gltr_comp_code),
                                    new SqlParameter("@gltr_acct_code", obj.gltr_acct_code),
                                    new SqlParameter("@gltr_doc_code", obj.gltr_doc_code),
                                    new SqlParameter("@gltr_pstng_date",db.DBYYYYMMDDformat(obj.gltr_pstng_date)),
                                    new SqlParameter("@gltr_doc_date", db.DBYYYYMMDDformat(obj.gltr_doc_date)),
                                    new SqlParameter("@glco_cost_centre_code", obj.glco_cost_centre_code),
                                    //new SqlParameter("@fetch_record_min", min);
                                    //new SqlParameter("@fetch_record_max", max);
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Finn064 finnobj = new Finn064();
                            finnobj.gltr_comp_code = dr["gltr_comp_code"].ToString();
                            finnobj.gltr_dept_no = dr["gltr_dept_no"].ToString();
                            finnobj.gltr_acct_code = dr["gltr_acct_code"].ToString();
                            finnobj.gltr_acct_chk_dgt = dr["gltr_acct_chk_dgt"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_pstng_date"].ToString()) == false)
                                finnobj.gltr_pstng_date = db.UIDDMMYYYYformat(dr["gltr_pstng_date"].ToString());
                            else
                                finnobj.gltr_pstng_date = "";
                            finnobj.gltr_pstng_type = dr["gltr_pstng_type"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_pstng_no"].ToString()) == false)
                                finnobj.gltr_pstng_no = int.Parse(dr["gltr_pstng_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gltr_pstng_seq_no"].ToString()) == false)
                                finnobj.gltr_pstng_seq_no = int.Parse(dr["gltr_pstng_seq_no"].ToString());
                            finnobj.gltr_doc_code = dr["gltr_doc_code"].ToString();
                            finnobj.gltr_our_doc_no = dr["gltr_our_doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_doc_date"].ToString()) == false)
                                finnobj.gltr_doc_date = db.UIDDMMYYYYformat(dr["gltr_doc_date"].ToString());
                            else
                                finnobj.gltr_doc_date = "";
                            finnobj.gltr_doc_narr = dr["gltr_doc_narr"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_tran_amt"].ToString()) == false)
                                finnobj.gltr_tran_amt = decimal.Parse(dr["gltr_tran_amt"].ToString());
                            if (finnobj.gltr_tran_amt > 0)
                            {
                                finnobj.gltr_dr_amount = finnobj.gltr_tran_amt;

                                gltr_total_dr_amount = gltr_total_dr_amount + finnobj.gltr_dr_amount;
                            }
                            else
                            {
                                finnobj.gltr_cr_amount = Math.Abs(finnobj.gltr_tran_amt);

                                gltr_total_cr_amount = gltr_total_cr_amount + finnobj.gltr_cr_amount;
                            }
                            finnobj.gltr_total_dr_amount = gltr_total_dr_amount;
                            finnobj.gltr_total_cr_amount = gltr_total_cr_amount;

                            if (string.IsNullOrEmpty(dr["running_total"].ToString()) == false)
                            {
                                finnobj.gltr_tran_amt = decimal.Parse(dr["running_total"].ToString());

                                gltr_running_total_amt = gltr_running_total_amt + finnobj.gltr_tran_amt;

                            }
                            finnobj.gltr_running_total_amt = gltr_running_total_amt;

                            if (string.IsNullOrEmpty(dr["gltr_fc_amt"].ToString()) == false)
                                finnobj.gltr_fc_amt = decimal.Parse(dr["gltr_fc_amt"].ToString());
                            if (finnobj.gltr_fc_amt > 0)
                                finnobj.gltr_fc_dr_amount = finnobj.gltr_fc_amt;
                            else
                                finnobj.gltr_fc_cr_amount = Math.Abs(finnobj.gltr_fc_amt);
                            if (string.IsNullOrEmpty(dr["gltr_tran_qty"].ToString()) == false)
                                finnobj.gltr_tran_qty = decimal.Parse(dr["gltr_tran_qty"].ToString());
                            finnobj.gltr_user_id = dr["gltr_user_id"].ToString();
                            finnobj.gltr_dept_no = dr["gltr_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_cur_date"].ToString()) == false)
                                finnobj.gltr_cur_date = db.UIDDMMYYYYformat(dr["gltr_cur_date"].ToString());
                            else
                                finnobj.gltr_cur_date = "";
                            finnobj.gltr_fc_code = dr["gltr_fc_code"].ToString();
                            if (string.IsNullOrEmpty(dr["gltr_year"].ToString()) == false)
                                finnobj.gltr_year = int.Parse(dr["gltr_year"].ToString());
                            if (string.IsNullOrEmpty(dr["gltr_prd_no"].ToString()) == false)
                                finnobj.gltr_prd_no = int.Parse(dr["gltr_prd_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gltr_conv_rate"].ToString()) == false)
                                finnobj.gltr_conv_rate = decimal.Parse(dr["gltr_conv_rate"].ToString());

                            finnobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            finnobj.glco_cost_centre_code = dr["glco_cost_centre_code"].ToString();



                            gl_querylist.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, gl_querylist);

            }
            return Request.CreateResponse(HttpStatusCode.OK, gl_querylist);
        }

        [Route("getDocCode_GL_Query")]
        public HttpResponseMessage getDocCode_GL_Query(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@gltr_comp_code",comp_code),
                             new SqlParameter("@gltr_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_name = obj.gltd_doc_code + "-" + dr["gldc_doc_name"].ToString();
                            obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }


        [Route("getdataAccountChange")]
        public HttpResponseMessage getdataAccountChange(string compcode, string financial_year, string ledger_code, string slma_acno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
           // Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin123> goaltarget_list = new List<Fin123>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@slma_comp_code", compcode),
                            new SqlParameter("@slma_ldgrctl_year", financial_year),
                            new SqlParameter("@slma_ldgrctl_code",ledger_code),
                            new SqlParameter("@payable_Acc",slma_acno),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin123 simsobj = new Fin123();


                            simsobj.slma_acno = dr["slma_acno"].ToString();
                            simsobj.slma_ldgrctl_code = dr["slma_ldgrctl_code"].ToString();

                            simsobj.coad_pty_short_name = dr["coad_pty_short_name"].ToString();
                            simsobj.coad_xref_addr_id = dr["coad_xref_addr_id"].ToString();

                            simsobj.coad_dept_no = dr["coad_dept_no"].ToString();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.sltr_attr1 = dr["coad_line_1"].ToString();
                            simsobj.sltr_attr2 = dr["coad_line_2"].ToString();
                            simsobj.sltr_attr3 = dr["coad_po_box"].ToString();
                            simsobj.sltr_attr4 = dr["coad_tel_no"].ToString();

                            simsobj.slma_pty_bank_acno = dr["slma_pty_bank_acno"].ToString();
                            simsobj.slma_yob_amt = dr["slma_yob_amt"].ToString();

                            simsobj.slma_cr_limit = dr["slma_cr_limit"].ToString();
                            simsobj.slma_credit_prd = dr["slma_credit_prd"].ToString();
                            simsobj.slma_stop_cr_ind = dr["slma_stop_cr_ind"].ToString();



                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getdataAllShow")]
        public HttpResponseMessage getdataAllShow(string compcode, string ledger_code, string slma_acno, string doc_code, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
           // Log.Debug(string.Format(debug, "Common", "getdataAllShow"));

            List<Fin123> goaltarget_list = new List<Fin123>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (doc_code == "undefined")
                    {
                        doc_code = null;
                    }
                    if (from_date == "undefined")
                    {
                        from_date = null;
                    }
                    if (to_date == "undefined")
                    {
                        to_date = null;
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                             new SqlParameter("@opr_upd", "ASQ"),
                             new SqlParameter("@sltr_comp_code", compcode),
                             new SqlParameter("@sltr_ldgr_code",ledger_code),
                             new SqlParameter("@sltr_slmast_acno",slma_acno),


                             new SqlParameter("@sltr_doc_code",doc_code),
                             new SqlParameter("@sltr_pstng_date",db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@sltr_doc_date",db.DBYYYYMMDDformat(to_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin123 finnobj = new Fin123();
                            finnobj.sltr_comp_code = dr["sltr_comp_code"].ToString();
                            finnobj.sltr_ldgr_code = dr["sltr_ldgr_code"].ToString();
                            finnobj.sltr_ldgr_name = dr["sltr_ldgr_name"].ToString();
                            finnobj.sltr_slmast_acno = dr["sltr_slmast_acno"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_pstng_date"].ToString()) == false)
                                finnobj.sltr_pstng_date =db.UIDDMMYYYYformat(dr["sltr_pstng_date"].ToString());
                            else
                                finnobj.sltr_pstng_date = "";
                            finnobj.sltr_pstng_type = dr["sltr_pstng_type"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_pstng_no"].ToString()) == false)
                                finnobj.sltr_pstng_no = dr["sltr_pstng_no"].ToString();

                            if (string.IsNullOrEmpty(dr["sltr_pstng_seq_no"].ToString()) == false)
                                finnobj.sltr_pstng_seq_no = dr["sltr_pstng_seq_no"].ToString();
                            finnobj.sltr_doc_code = dr["sltr_doc_code"].ToString();
                            finnobj.sltr_our_doc_no = dr["sltr_our_doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_doc_date"].ToString()) == false)
                                finnobj.sltr_doc_date = db.UIDDMMYYYYformat(dr["sltr_doc_date"].ToString());
                            else
                                finnobj.sltr_doc_date = "";
                            finnobj.sltr_pty_ref_no = dr["sltr_pty_ref_no"].ToString();
                            finnobj.sltr_doc_narr = dr["sltr_doc_narr"].ToString();

                            if (string.IsNullOrEmpty(dr["sltr_tran_amt"].ToString()) == false)
                                finnobj.sltr_tran_amt = dr["sltr_tran_amt"].ToString();
                            if (decimal.Parse(finnobj.sltr_tran_amt) > 0)
                                finnobj.sltr_dr_amount = finnobj.sltr_tran_amt;
                            else
                            {
                                finnobj.sltr_cr_amount = Math.Abs(decimal.Parse(finnobj.sltr_tran_amt));
                                finnobj.sltr_dr_amount = 0.ToString();
                            }
                            if (string.IsNullOrEmpty(dr["running_total"].ToString()) == false)

                                finnobj.pb_amount = dr["running_total"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_fc_amt"].ToString()) == false)
                                finnobj.sltr_fc_amt = dr["sltr_fc_amt"].ToString();
                            if (decimal.Parse(finnobj.sltr_fc_amt) > 0)
                                finnobj.sltr_fc_dr_amount = finnobj.sltr_fc_amt;
                            else
                                finnobj.sltr_fc_cr_amount = Math.Abs(decimal.Parse(finnobj.sltr_fc_amt));

                            if (string.IsNullOrEmpty(dr["sltr_tran_qty"].ToString()) == false)
                                finnobj.sltr_tran_qty = dr["sltr_tran_qty"].ToString();
                            finnobj.sltr_user_id = dr["sltr_user_id"].ToString();
                            finnobj.sltr_dept_no = dr["sltr_dept_no"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_cur_date"].ToString()) == false)
                                finnobj.sltr_cur_date = db.UIDDMMYYYYformat(dr["sltr_cur_date"].ToString());
                            else
                                finnobj.sltr_cur_date = "";
                            finnobj.sltr_match_ind = dr["sltr_match_ind"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_match_ref_no"].ToString()) == false)
                                finnobj.sltr_match_ref_no = dr["sltr_match_ref_no"].ToString();
                            else
                                finnobj.sltr_match_ref_no = 0.ToString();
                            finnobj.sltr_fc_code = dr["sltr_fc_code"].ToString();
                            finnobj.sltr_pty_bank_id = dr["sltr_pty_bank_id"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_due_date"].ToString()) == false)
                                finnobj.sltr_due_date = db.UIDDMMYYYYformat(dr["sltr_due_date"].ToString());
                            else
                                finnobj.sltr_due_date = "";

                            if (string.IsNullOrEmpty(dr["sltr_year"].ToString()) == false)
                                finnobj.sltr_year = dr["sltr_year"].ToString();

                            if (string.IsNullOrEmpty(dr["sltr_prd_no"].ToString()) == false)
                                finnobj.sltr_prd_no = dr["sltr_prd_no"].ToString();
                            if (string.IsNullOrEmpty(dr["sltr_conv_rate"].ToString()) == false)
                                finnobj.sltr_conv_rate = dr["sltr_conv_rate"].ToString();

                           

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getChequeDetails")]
        public HttpResponseMessage getChequeDetails(string compcode, string ledger_code, string slma_acno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
          //  Log.Debug(string.Format(debug, "Common", "getdataAllShow"));

            List<Fin123> goaltarget_list = new List<Fin123>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'T'),
                             new SqlParameter("@sltr_comp_code", compcode),
                             new SqlParameter("@sltr_ldgr_code",ledger_code),
                             new SqlParameter("@sltr_slmast_acno",slma_acno),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin123 finnobj = new Fin123();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.pc_doc_type = dr["pc_doc_type"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                finnobj.pc_due_date = db.UIDDMMYYYYformat(dr["pc_due_date"].ToString());
                            else
                                finnobj.pc_due_date = "";
                            finnobj.pc_bank_from = dr["pc_bank_from"].ToString();
                            finnobj.pc_bank_slno = dr["pc_bank_slno"].ToString();
                            finnobj.pc_cheque_no = dr["pc_cheque_no"].ToString();
                            finnobj.pc_discd = dr["pc_discd"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.pc_amount = decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();



                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
              //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getBillDetails")]
        public HttpResponseMessage getBillDetails(string compcode, string ledger_code, string slma_acno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
         //   Log.Debug(string.Format(debug, "Common", "getdataAllShow"));

            List<Fin123> goaltarget_list = new List<Fin123>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'V'),
                             new SqlParameter("@sltr_comp_code", compcode),
                             new SqlParameter("@sltr_ldgr_code",ledger_code),
                             new SqlParameter("@sltr_slmast_acno",slma_acno),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin123 finnobj = new Fin123();
                            finnobj.pb_dept_no = dr["pb_dept_no"].ToString();
                            finnobj.pb_doc_type = dr["pb_doc_type"].ToString();
                            finnobj.pb_our_doc_no = dr["pb_our_doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_due_date"].ToString()) == false)
                                finnobj.pb_due_date = db.UIDDMMYYYYformat(dr["pb_due_date"].ToString());
                            else
                                finnobj.pb_due_date = "";
                            finnobj.pb_bank_from = dr["pb_bank_from"].ToString();
                            finnobj.pb_bank_slno = dr["pb_bank_slno"].ToString();
                            finnobj.pb_cheque_no = dr["pb_cheque_no"].ToString();
                            finnobj.pb_discd = dr["pb_discd"].ToString();
                            if (string.IsNullOrEmpty(dr["pb_amount"].ToString()) == false)
                                finnobj.pb_amount1 = decimal.Parse(dr["pb_amount"].ToString());
                            finnobj.pb_ref_no = dr["pb_ref_no"].ToString();



                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getDate_SL_Query")]
        public HttpResponseMessage getDate_SL_Query(string finance_year)
        {
            List<Fin150> DC_date = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sl_transactions]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@sltr_year",finance_year)
                            

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();

                            obj.sims_financial_year_start_date = dr["sims_financial_year_start_date"].ToString();
                            obj.sims_financial_year_end_date = dr["sims_financial_year_end_date"].ToString();
                           
                            DC_date.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_date);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_date);
        }

        [Route("Get_GL_opening_bal")]
        public HttpResponseMessage Get_GL_opening_bal(string comp_code, string fyear,string acct_code,string from_date)
        {
            string acct_opening_bal = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_gl_transactions",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'V'),
                            new SqlParameter("@gltr_comp_code", comp_code),
                            new SqlParameter("@gltr_year", fyear),
                            new SqlParameter("@gltr_acct_code", acct_code),
                            new SqlParameter("@gltr_pstng_date", db.DBYYYYMMDDformat(from_date)),

                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            acct_opening_bal = dr["gltr_ob_bal"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, acct_opening_bal);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, acct_opening_bal);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error To Get URL";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


    }
}