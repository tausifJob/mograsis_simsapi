﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;


namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/ScheduleDetails")]
    public class ScheduleController:ApiController
    {

        [Route("getScheduleCode")]
        public HttpResponseMessage getScheduleCode(string glsc_comp_code, string fyear)
        {
            List<fins118> schedule_list = new List<fins118>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_schedules_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@glsc_comp_code", glsc_comp_code),
                            new SqlParameter("@glsc_year", fyear)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins118 obj = new fins118();
                            obj.glsc_comp_code = dr["glsc_comp_code"].ToString();
                            obj.company_name = dr["company_name"].ToString();
                            //obj.company_name = dr["company_name"].ToString();
                            obj.glsc_year = dr["glsc_year"].ToString();
                            obj.glsc_sched_code = dr["glsc_sched_code"].ToString();
                            obj.glsc_sched_name = dr["glsc_sched_name"].ToString();

                            schedule_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, schedule_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, schedule_list);
        }

        [Route("ScheduleCUD")]
        public HttpResponseMessage ScheduleCUD(List<fins118> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "ScheduleCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins118 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_schedules_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@glsc_comp_code",simsobj.glsc_comp_code),
                          new SqlParameter("@glsc_year",simsobj.glsc_year),
                          new SqlParameter("@glsc_sched_code", simsobj.glsc_sched_code),
                          new SqlParameter("@glsc_sched_name", simsobj.glsc_sched_name)
                       
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}