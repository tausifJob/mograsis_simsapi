﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/UserControl")]
    [BasicAuthentication]
    public class UserAccountController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllUserControl")]
        public HttpResponseMessage getAllUserControl()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllUserControl(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllUserControl"));

            List<Fin205> goaltarget_list = new List<Fin205>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_doc_user_account_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.gdua_comp_code = dr["gdua_comp_code"].ToString();
                            simsobj.gdua_year = dr["gdua_year"].ToString();

                            simsobj.gdua_dept_no = dr["gdua_dept_no"].ToString();
                            simsobj.gdua_doc_code = dr["gdua_doc_code"].ToString();

                            simsobj.gdua_acct_code = dr["gdua_acct_code"].ToString();
                            simsobj.gdua_doc_user_code = dr["gdua_doc_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.gdua_status = dr["gdua_status"].Equals("A") ? true : false;
                           
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@tbl_cond", comp_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.codp_dept_no = dr["codp_dept_no"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetDocument")]
        public HttpResponseMessage GetDocument()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDocument()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDocument"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_financial_documents_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),//This block comes in Financil Document
                            new SqlParameter("@gldc_comp_code", '1'),
                            new SqlParameter("@gldc_year", "2016")
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            simsobj.gdua_doc_code = dr["gldc_doc_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetAccountCode")]
        public HttpResponseMessage GetAccountCode(string comp_code,string year,string dept_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAccountCode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetAccountCode"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_doc_user_account_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),//This block comes frm [fins_doc_user_account]
                            new SqlParameter("@gdua_comp_code", comp_code),
                            new SqlParameter("@gdua_year", year),
                            new SqlParameter("@gdua_dept_no",dept_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.gdua_acct_code = dr["glma_acct_code"].ToString();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetUSerCode")]
        public HttpResponseMessage GetUSerCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUSerCode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetUSerCode"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_doc_user_account_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),//This block comes frm [fins_doc_user_account]
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.user_name = dr["user_name"].ToString();
                            simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            simsobj.gdua_doc_user_code = dr["user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDUserAccount")]
        public HttpResponseMessage CUDUserAccount(List<Fin205> data)
        {
            bool insert=false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin205 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_doc_user_account_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                new SqlParameter("@gdua_comp_code", "1"),
                                new SqlParameter("@gdua_year", "2016"),
                                new SqlParameter("@gdua_dept_no", simsobj.gdua_dept_no),
                                new SqlParameter("@gdua_doc_code", simsobj.gdua_doc_code),
                                new SqlParameter("@gdua_acct_code", simsobj.gdua_acct_code),
                                new SqlParameter("@gdua_doc_user_code", simsobj.gdua_doc_user_code),

                                new SqlParameter("@gdua_status",simsobj.gdua_status==true?"A":"I")
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }

}










