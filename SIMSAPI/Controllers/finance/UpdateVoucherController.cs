﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/UpdateVoucher")]
    public class UpdateVoucherController:ApiController
    {
        [Route("getDocCodeJV")]
        public HttpResponseMessage getDocCodeJV(string comp_code, string finance_year)
        {
            List<Fin150> DC_JV = new List<Fin150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_financial_documents",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                             new SqlParameter("@gldc_comp_code",comp_code),
                             new SqlParameter("@gldc_year",finance_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 obj = new Fin150();
                            //obj.gltd_comp_code = dr["gldc_comp_code"].ToString();
                            obj.gltd_doc_code = dr["gldc_doc_code"].ToString();
                            //obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            DC_JV.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DC_JV);
        }

        [Route("UpdateVoucherCUD")]
        public HttpResponseMessage UpdateVoucherCUD(List<Fin201> data)
        {
          
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateVoucherCUD(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
              
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin201 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_temp_doc_details_proc]",
                            new List<SqlParameter>()
                            {
                                //new SqlParameter("@opr", simsobj.opr),
                                //new SqlParameter("@opr_upd",simsobj.opr_upd),
                                //new SqlParameter("@gldd_comp_code", simsobj.gldd_comp_code),
                                //new SqlParameter("@gldd_doc_code", simsobj.gldd_doc_code),
                                //new SqlParameter("@gldd_prov_doc_no", simsobj.gltd_prov_doc_no),
                                //// new SqlParameter("@gldd_prov_doc_no", simsobj.gldd_prov_doc_no),
                                //new SqlParameter("@gldd_final_doc_no", simsobj.gltd_final_doc_no),
                                //new SqlParameter("@gldd_doc_narr", simsobj.gldd_doc_narr),
                                // new SqlParameter("@gltd_doc_narr", simsobj.gltd_doc_narr),
                                //new SqlParameter("@gltd_remarks", simsobj.gltd_remarks),
                                //new SqlParameter("@gldd_line_no", simsobj.gldd_line_no),
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@opr_upd",simsobj.opr_upd),
                                new SqlParameter("@gldd_comp_code", simsobj.gldd_comp_code),
                                new SqlParameter("@gldd_doc_code", simsobj.gldd_doc_code),
                                new SqlParameter("@gldd_prov_doc_no", simsobj.gltd_prov_doc_no),
                                new SqlParameter("@gldd_final_doc_no", simsobj.gltd_final_doc_no),
                                new SqlParameter("@gldd_doc_narr", simsobj.gltd_doc_narr),
                                new SqlParameter("@gltd_remarks", simsobj.gltd_remarks),
                                new SqlParameter("@gldd_line_no", simsobj.gldd_line_no),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getDocCodeUpdateVoucher")]
        public HttpResponseMessage getDocCodeUpdateVoucher()
    {
        List<Fin201> UpdateVoucher = new List<Fin201>();
        try
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_financial_documents]",
                    new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", "W")

                     }
                     );
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                            Fin201 obj = new Fin201();
                        obj.gldc_doc_code = dr["gldc_doc_code"].ToString();
                        obj.gldc_doc_type = dr["gldc_doc_type"].ToString();
                            UpdateVoucher.Add(obj);

                    }
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, UpdateVoucher);
        }
        catch (Exception x) { }
        return Request.CreateResponse(HttpStatusCode.OK, UpdateVoucher);
    }

        [Route("getProvisionNo")]
        public HttpResponseMessage getProvisionNo(string doccode,string comp_code)
    {
        string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProvisionNo()PARAMETERS ::NA";
        List<Fin201> getAllPrNo = new List<Fin201>();
        Message message = new Message();
        try
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                    new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", 'Q'),
                              new SqlParameter("@gltd_comp_code",comp_code),
                            new SqlParameter("@gltd_doc_code",doccode)

                     });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Fin201 simsobj = new Fin201();
                            simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            getAllPrNo.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, getAllPrNo);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, getAllPrNo);
            }
        }
        catch (Exception x)
        {
            //Log.Error(x);
            message.strMessage = "No Records Found";
            message.messageType = MessageType.Error;
            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        }

        //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
    }

        [Route("getAllRecords")]
        public HttpResponseMessage getAllRecords(string doccode, string prnumber,string comp_code)
    {
        string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";
        List<Fin201> getRecords = new List<Fin201>();
        Message message = new Message();
        try
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_temp_doc_details_proc]",
                    new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@opr_upd", 'S'),
                            new SqlParameter("@gldd_comp_code",comp_code),
                            new SqlParameter("@gldd_doc_code",doccode),
                            new SqlParameter("@gldd_prov_doc_no",prnumber)
                     });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Fin201 simsobj = new Fin201();
                        simsobj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                        simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                        simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                        simsobj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                        simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                        simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                        if (string.IsNullOrEmpty("gldd_doc_amount") == false)
                        {
                            if (decimal.Parse(dr["gldd_doc_amount"].ToString()) > 0)
                                simsobj.gldd_doc_amount_debit = Convert.ToDecimal(dr["gldd_doc_amount"].ToString());
                            else
                                simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gldd_doc_amount"].ToString()));
                        }
                        simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                        simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                        simsobj.gldd_line_no = dr["gldd_line_no"].ToString();
                        simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                        simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                        simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                        simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                        simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                    if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                        simsobj.gltd_doc_date = Convert.ToDateTime(dr["gltd_doc_date"].ToString()).ToShortDateString();
                    if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                        simsobj.gltd_post_date = Convert.ToDateTime(dr["gltd_post_date"].ToString()).ToShortDateString();
                        simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                        getRecords.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, getRecords);
            }
        }
        catch (Exception x)
        {
            message.strMessage = "No Records Found";
            message.messageType = MessageType.Error;
            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        }
    }


        [Route("insertVoucherData")]  // added by shahaji
        public HttpResponseMessage insertVoucherData(List<FinVoucher> data)    //
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        {
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[fins].[fins_voucher_data_import_proc]",
                            new List<SqlParameter>(){
                            new SqlParameter("@opr", 'J'),
                         });
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();                               
                                foreach (FinVoucher finnobj in data)
                                {
                                    int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_voucher_data_import_proc]",
                                        new List<SqlParameter>() 
                                        {                             
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@doc_code", finnobj.doc_code),
                                        new SqlParameter("@final_doc_no", finnobj.final_doc_no),
                                        new SqlParameter("@doc_date", finnobj.doc_date),
                                        new SqlParameter("@ledger_doc_narr", finnobj.ledger_doc_narr),
                                        new SqlParameter("@acct_code", finnobj.acct_code),
                                        new SqlParameter("@acct_name", finnobj.acct_name),
                                        new SqlParameter("@doc_amount", finnobj.doc_amount),
                                        new SqlParameter("@created_user", finnobj.username),
                                        });
                                    if (ins > 0)
                                    {
                                        inserted = true;
                                    }
                                    else
                                    {
                                        inserted = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}