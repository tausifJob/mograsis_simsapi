﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/CostCentre")]
    public class CostCentreController : ApiController
    {
        [Route("getCostCentreDetail")]
        public HttpResponseMessage getCostCentreDetail()
        {
            List<Fin221> com_list = new List<Fin221>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_cost_centre]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin221 obj = new Fin221();
                            obj.coce_comp_code = dr["coce_comp_code"].ToString();
                            obj.coce_year = dr["coce_year"].ToString();
                            obj.coce_cost_centre_code = dr["coce_cost_centre_code"].ToString();
                            obj.coce_cost_centre_type = dr["coce_cost_centre_type"].ToString();
                            obj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            obj.coce_cost_centre_person_responsible = dr["coce_cost_centre_person_responsible"].ToString();
                            obj.coce_cost_centre_currency = dr["coce_cost_centre_currency"].ToString();
                            obj.currency_desc = dr["currency_desc"].ToString();
                            obj.coce_cost_centre_address = dr["coce_cost_centre_address"].ToString();
                            if (dr["coce_status"].ToString() == "A")
                                obj.coce_status = true;
                            else
                                obj.coce_status = false;

                            com_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getCurrencyDetail")]
        public HttpResponseMessage getCurrencyDetail()
        {
            List<Fin221> com_list = new List<Fin221>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_cost_centre]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin221 obj = new Fin221();
                            obj.excg_comp_code = dr["excg_comp_code"].ToString();
                            obj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            obj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();
                            com_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("CostCentreDetailCUD")]
        public HttpResponseMessage CostCentreDetailCUD(List<Fin221> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin221 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_cost_centre]",
                            new List<SqlParameter>()

                        {
                                new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@coce_comp_code", simsobj.comp_code),
                                 new SqlParameter("@coce_year", simsobj.finance_year),
                                 new SqlParameter("@coce_cost_centre_code", simsobj.coce_cost_centre_code),
                                 new SqlParameter("@coce_cost_centre_type", simsobj.coce_cost_centre_type),
                                 new SqlParameter("@coce_cost_centre_name", simsobj.coce_cost_centre_name),
                                 new SqlParameter("@coce_cost_centre_person_responsible", simsobj.coce_cost_centre_person_responsible),
                                 new SqlParameter("@coce_cost_centre_currency", simsobj.coce_cost_centre_currency),
                                 new SqlParameter("@coce_cost_centre_address", simsobj.coce_cost_centre_address),
                                 new SqlParameter("@coce_status", simsobj.coce_status==true?"A":"I")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}