﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
     [RoutePrefix("api/common/ViewEditPeriodLedger")]

    public class ViewEditPeriodLedgerController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("GetAll_Gl_period_ledgers_deptno")]
         public HttpResponseMessage GetAll_Gl_period_ledgers_deptno(string comp_code, string fin_yr)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLedgerCodes(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "STUDENT", "GetAllLedgerCodes"));

             List<Finn013> com_list = new List<Finn013>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_period_ledgers]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@glpr_comp_code",comp_code),
                                new SqlParameter("@glpr_year",fin_yr)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Finn013 FinnObj = new Finn013();
                             FinnObj.glpr_dept_no = dr["glpr_dept_no"].ToString();
                             FinnObj.deptname = dr["codp_dept_name"].ToString();
                             FinnObj.glpr_dept_desc = dr["glpr_dept_no"].ToString() + " - " + dr["codp_dept_name"].ToString();
                             com_list.Add(FinnObj);
                         }
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, com_list);
             }
             catch (Exception e)
             {
                 Log.Error(e);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, com_list);
             }
         }

         [Route("Get_account_code")]
         public HttpResponseMessage Get_account_code(string comp_code, string fin_yr,string deptno)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_account_code(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "STUDENT", "Get_account_code"));

             List<Finn013> com_list = new List<Finn013>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_period_ledgers]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B"),
                                new SqlParameter("@glpr_comp_code",comp_code),
                                new SqlParameter("@glpr_year",fin_yr),
                                 new SqlParameter("@glpr_dept_no",deptno)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Finn013 FinnObj = new Finn013();
                             FinnObj.account_name = dr["glma_acct_name"].ToString();
                             FinnObj.account_code = dr["glpr_acct_code"].ToString();
                             FinnObj.glpr_acct_code = dr["glpr_acct_code"].ToString() + " - " + dr["glma_acct_name"].ToString();
                             com_list.Add(FinnObj);
                         }
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, com_list);
             }
             catch (Exception e)
             {
                 Log.Error(e);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, com_list);
             }
         }

        [Route("GetFins_gl_period_ledgers")]
        public HttpResponseMessage GetFins_gl_period_ledgers(string comp_code, string fin_yr, string deptno,string accnt_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFins_gl_period_ledgers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetFins_gl_period_ledgers"));

            List<Finn013> fins_list = new List<Finn013>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_period_ledgers]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@glpr_comp_code", comp_code),
                            new SqlParameter("@glpr_year", fin_yr),
                            new SqlParameter("@glpr_dept_no", deptno),
                            new SqlParameter("@glpr_acct_code", accnt_no),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn013 Finnobj = new Finn013();
                            Finnobj.glpr_prd_no = decimal.Parse(dr["glpr_prd_no"].ToString());
                            Finnobj.glpr_bud_amt = decimal.Parse(dr["glpr_bud_amt"].ToString());
                            Finnobj.glpr_bud_qty = decimal.Parse(dr["glpr_bud_qty"].ToString());
                            Finnobj.glpr_rev_bud_amt = decimal.Parse(dr["glpr_rev_bud_amt"].ToString());
                            Finnobj.glpr_rev_bud_qty = decimal.Parse(dr["glpr_rev_bud_qty"].ToString());
                            Finnobj.glpr_bud_debit = decimal.Parse(dr["debit_Amount"].ToString());
                            try
                            {
                                Finnobj.glpr_dept_desc = dr["glpr_dept_desc"].ToString();
                            }
                            catch (Exception e)
                            {
                            }
                            Finnobj.glpr_bud_credit = decimal.Parse(dr["credit_Amount"].ToString());
                            
                            fins_list.Add(Finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fins_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_list);
            }
        }

        [Route("CUDUpdateFins_gl_period_ledgers")]
        public HttpResponseMessage CUDUpdateFins_gl_period_ledgers(List<Finn013> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateFins_gl_period_ledgers(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "FINANCE", "gl_period_ledgers"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn013 finnobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_gl_period_ledgers]",
                                new List<SqlParameter>() 
                            {                             
                            new SqlParameter("@opr", finnobj.opr),
                            new SqlParameter("@sub_opr", "NUA"),
                            new SqlParameter("@glpr_comp_code", finnobj.glpr_comp_code),
                            new SqlParameter("@glpr_year", finnobj.glpr_year),
                            new SqlParameter("@glpr_dept_no", finnobj.glpr_dept_no),
                            new SqlParameter("@glpr_acct_code", finnobj.glpr_acct_code),
                            new SqlParameter("@glpr_prd_no", finnobj.glpr_prd_no),
                            new SqlParameter("@glpr_bud_amt", finnobj.glpr_rev_bud_amt),
                            new SqlParameter("@glpr_bud_qty", finnobj.glpr_rev_bud_qty),
                            new SqlParameter("@glpr_rev_bud_amt", finnobj.glpr_rev_bud_amt),
                            new SqlParameter("@glpr_rev_bud_qty", finnobj.glpr_rev_bud_qty),
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}