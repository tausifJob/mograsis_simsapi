﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/checqsubrpt")]
    public class chequeSubmissionReportController : ApiController
    {
        [Route("getchequeSubmissionReport")]
        public HttpResponseMessage getchequeSubmissionReport(string mom_start_date, string mom_end_date, string doc_search)
        {
            List<CSR001> cheqdetail = new List<CSR001>();
            if (doc_search == "undefined"|| doc_search == "")
            {
                doc_search = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_finn_cheque_Submission_Report",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),
                                     new SqlParameter("@end_date", db.DBYYYYMMDDformat(mom_end_date)),
                                     new SqlParameter("@doc_no",doc_search),                                   



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CSR001 sequence = new CSR001();
                            sequence.pc_ref_no = dr["pc_ref_no"].ToString();
                            sequence.pc_submission_date = dr["pc_submission_date"].ToString();
                            sequence.sub_cheque_amt = dr["sub_cheque_amt"].ToString();
                            sequence.no_sub_cheque = dr["no_sub_cheque"].ToString();
                            sequence.re_sub_cheque_amt = dr["re_sub_cheque_amt"].ToString();
                            sequence.no_re_sub_cheque = dr["no_re_sub_cheque"].ToString();
                            sequence.out_cheque_amt = dr["out_cheque_amt"].ToString();
                            sequence.no_out_cheque = dr["no_out_cheque"].ToString();
                            sequence.ret_cheque_amt = dr["ret_cheque_amt"].ToString();
                            sequence.no_ret_cheque = dr["no_ret_cheque"].ToString();
                            sequence.cancle_cheque_amt = dr["cancle_cheque_amt"].ToString();
                            sequence.no_cancle_cheque = dr["no_cancle_cheque"].ToString();
                            sequence.realise_cheque_amt = dr["realise_cheque_amt"].ToString();
                            sequence.no_realise_cheque = dr["no_realise_cheque"].ToString();
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }

    }
}