﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Controllers.finance;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/FinancialPeriod")]

    public class Fin012Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getGetCompany")]
        public HttpResponseMessage getGetCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGetCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "DEPARTMENTCOMPANY"));

            List<Finn010> company_list = new List<Finn010>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn010 finnobj = new Finn010();
                            finnobj.dept_comp_name = dr["comp_name"].ToString();
                            finnobj.dept_comp_code = dr["comp_code"].ToString();
                            //finnobj.accd_comp_name = dr["comp_name"].ToString();
                            //finnobj.fp_comp_name = dr["comp_name"].ToString();
                            //finnobj.excg_comp_code_name = dr["comp_name"].ToString();
                            company_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, company_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, company_list);
            }
        }


        [Route("getGetFinancialPeriod")]
        public HttpResponseMessage getGetFinancialPeriod()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGetFinancialPeriod(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FINANCE", "FINANCIALPERIOD"));

            List<Finn012> finperiod_list = new List<Finn012>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                                                     
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn012 finnobj = new Finn012();
                            finnobj.dept_comp_code = dr["glfp_comp_code"].ToString();
                            finnobj.fp_comp_name = dr["comp_name"].ToString();
                            finnobj.glfp_year_desc = dr["glfp_year_desc"].ToString();


                            if (string.IsNullOrEmpty(dr["glfp_year"].ToString()) == false)
                                finnobj.prd_year = int.Parse(dr["glfp_year"].ToString());
                            if (string.IsNullOrEmpty(dr["glfp_prd_no"].ToString()) == false)
                                finnobj.prd_no = int.Parse(dr["glfp_prd_no"].ToString());
                            if (string.IsNullOrEmpty(dr["glfp_prd_st_date"].ToString()) == false)
                                finnobj.prd_st_dt = db.UIDDMMYYYYformat(dr["glfp_prd_st_date"].ToString());
                                //finnobj.prd_st_dt = DateTime.Parse(dr["glfp_prd_st_date"].ToString()).ToShortDateString();
                            if (string.IsNullOrEmpty(dr["glfp_prd_end_date"].ToString()) == false)
                                finnobj.prd_end_dt = db.UIDDMMYYYYformat(dr["glfp_prd_end_date"].ToString());
                                //finnobj.prd_end_dt = DateTime.Parse(dr["glfp_prd_end_date"].ToString()).ToShortDateString();
                            if (string.IsNullOrEmpty(dr["glfp_prd_days"].ToString()) == false)
                                finnobj.prd_days = int.Parse(dr["glfp_prd_days"].ToString());
                            finnobj.prd_jgen_dt = db.UIDDMMYYYYformat(dr["glfp_lst_jgen_date"].ToString());
                            if (dr["glfp_status"].ToString().Equals("Y"))
                                finnobj.status = true;
                            else
                                finnobj.status = false;
                            finperiod_list.Add(finnobj);
                          
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, finperiod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, finperiod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, finperiod_list);

            }
            // return Request.CreateResponse(HttpStatusCode.OK, finperiod_list);
        }


        [Route("getCheckFinancialPeriod")]
        public HttpResponseMessage getCheckFinancialPeriod(string comp_code, decimal year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCheckFinancialPeriod(),PARAMETERS :: YEAR{2}";
            Log.Debug(string.Format(debug, "FINANCE", "CHECKFINANCIALPERIOD", year));

            List<Finn010> deptcode_list = new List<Finn010>();
            int count = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'X'),     
                                new SqlParameter("@comp_code",comp_code),
                                new SqlParameter("@prd_year",year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            count = int.Parse(dr["cnt"].ToString());
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, count);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, count);
            }
        }

        [Route("CUDFinancialPeriod")]
        public HttpResponseMessage CUDFinancialPeriod(List<Finn012> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFinancialPeriod(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "FINANCE", "FINANCIALPERIOD"));
            bool inserted = false;
          
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn012 finobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_financial_periods_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", finobj.opr),
                            new SqlParameter("@comp_code", finobj.dept_comp_code),
                            new SqlParameter("@prd_year", finobj.prd_year),
                            new SqlParameter("@prd_st_dt", db.DBYYYYMMDDformat(finobj.prd_st_dt)),
                            new SqlParameter("@prd_end_dt", db.DBYYYYMMDDformat(finobj.prd_end_dt)),
                            new SqlParameter("@status", finobj.status == true?"Y":"N")

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}