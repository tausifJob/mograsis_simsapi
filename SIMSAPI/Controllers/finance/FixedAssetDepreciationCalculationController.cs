﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.FixedAssetDepreciationCalculationController
{
    [RoutePrefix("api/AssetDepreciation")]
    //[BasicAuthentication]
    public class FixedAssetDepreciationCalculationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAssetsData")]
        public HttpResponseMessage getAssetsData(string compnycode, string year, string month, string gal_asst_type, string gam_item_no)
        {
            List<Fin017> desg_list = new List<Fin017>();

            if (gal_asst_type == "undefined")
            {
                gal_asst_type = null;
            }
            else if (gam_item_no == "undefined")
            {
                gam_item_no = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[show_calculated_depreciation_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@year",year),
                            new SqlParameter("@month",month),
                            new SqlParameter("@gam_asst_type",gal_asst_type),
                            new SqlParameter("@gam_item_no",gam_item_no),
                            new SqlParameter("@company_code",compnycode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                            //simsobj.gam_item_no = int.Parse(dr["gam_item_no"].ToString());
                            simsobj.gam_item_no = dr["gam_item_no"].ToString();
                            simsobj.gam_desc_1 = dr["gam_desc_1"].ToString();
                            simsobj.gal_asst_type = dr["gal_asst_type"].ToString();
                            simsobj.gal_type_desc = dr["gal_type_desc"].ToString();
                            simsobj.gam_desc_2 = dr["gam_desc_2"].ToString();
                            simsobj.gam_supl_name = dr["gam_supl_name"].ToString();
                            simsobj.gam_acct_code = dr["gam_acct_name"].ToString();
                            simsobj.gam_invoice_no = dr["gam_invoice_no"].ToString();
                            if (string.IsNullOrEmpty(dr["gam_quantity"].ToString()) == false)
                                simsobj.gam_quantity = Decimal.Parse(dr["gam_quantity"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_invoice_amount"].ToString()) == false)
                                simsobj.gam_invoice_amount = Decimal.Parse(dr["gam_invoice_amount"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_book_value"].ToString()) == false)
                                simsobj.gam_book_value = Decimal.Parse(dr["gam_book_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_cum_deprn"].ToString()) == false)
                                simsobj.gam_cum_deprn = Decimal.Parse(dr["gam_cum_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_ytd_deprn"].ToString()) == false)
                                simsobj.gam_ytd_deprn = Decimal.Parse(dr["gam_ytd_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_mth_deprn"].ToString()) == false)
                                simsobj.gam_mth_deprn = Decimal.Parse(dr["gam_mth_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_sale_value"].ToString()) == false)
                                simsobj.gam_sale_value = Decimal.Parse(dr["gam_sale_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_repl_cost"].ToString()) == false)
                                simsobj.gam_repl_cost = Decimal.Parse(dr["gam_repl_cost"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_deprn_percent"].ToString()) == false)
                                simsobj.gam_deprn_percent = Decimal.Parse(dr["gam_deprn_percent"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_used_on_item"].ToString()) == false)
                                simsobj.gam_used_on_item = Decimal.Parse(dr["gam_used_on_item"].ToString());
                            simsobj.gam_status = dr["gam_status"].ToString();
                            simsobj.gam_status_name = dr["gam_statusValue"].ToString();
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getPeriods")]
        public HttpResponseMessage getPeriods(string fins_year,string fins_comp)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'K'),
                            new SqlParameter("@fins_year",fins_year),
                            new SqlParameter("@fins_comp_code",fins_comp),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                            simsobj.fins_financial_period_no = dr["glfp_prd_no"].ToString();
                            simsobj.fins_financial_period_name = dr["glfp_prd_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("get_calculateAssetDepreciation")]
        public HttpResponseMessage get_calculateAssetDepreciation(string year1, string period_no, string companyCode, string gal_asst_type, string gam_item_no)
        {
            string return_message = "";
            Message message = new Message();

            if (gal_asst_type == "undefined")
            {
                gal_asst_type = null;
            }
            else if (gam_item_no == "undefined")
            {
                gam_item_no = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[calculate_depreciation_proc]",
                        //  int i = db.ExecuteStoreProcedureforInsert("[fins].[calculate_depreciation_proc]",
                       new List<SqlParameter>()
                       {
                              new SqlParameter("@company_code",companyCode),
                              new SqlParameter("@year",year1),
                              new SqlParameter("@month",period_no),
                              new SqlParameter("@gam_asst_type",gal_asst_type),
                              new SqlParameter("@gam_item_no",gam_item_no),

                        });

                    if (dr.HasRows)
                    {
                        return_message = "Fixed Asset Depreciation Calculated Successfully" + "-" + "1";
                    }
                }
            }
            catch (Exception x)
            {
                return_message = x.Message + "-" + "0";
            }
            return Request.CreateResponse(HttpStatusCode.OK, return_message);
        }


        [Route("getCurrentYear")]
        public HttpResponseMessage getCurrentYear()
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'G'),
                           
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                            simsobj.year1 = dr["fins_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getDepreciationFixedJVPosting")]
        public HttpResponseMessage getDepreciationJVPosting(string year, string fins_financial_period_no, string username, string company_code)//
        {
            string return_message = "";
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[depreciation_jv_post_proc]",
                       new List<SqlParameter>()
                       {
                              new SqlParameter("@company_code", company_code),
                              new SqlParameter("@year", year),
                              new SqlParameter("@month", fins_financial_period_no),
                              new SqlParameter("@user_name", username),
                              new SqlParameter("@verify_status", "Vry")
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["gltd_doc_code"].ToString()))
                                return_message = "Depreciation Posted Successfully" + "-" + dr["gltd_doc_code"].ToString();
                        }
                    }

                }
            }
            catch (Exception x)
            {
                return_message = x.Message;

            }
            return Request.CreateResponse(HttpStatusCode.OK, return_message);
        }


        [Route("getAssetType")]
        public HttpResponseMessage getAssetType()
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[show_calculated_depreciation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'T'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();

                            simsobj.gal_asst_type = dr["gal_asst_type"].ToString();
                            simsobj.gal_type_desc = dr["gal_type_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getItemName")]
        public HttpResponseMessage getItemName(string item_name)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            if (item_name == "undefined")
            {
                item_name = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[show_calculated_depreciation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'M'),
                            new SqlParameter("@gam_asst_type",item_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();

                            simsobj.gam_item_no = dr["gam_item_no"].ToString();
                            simsobj.gam_desc_1 = dr["gam_desc_1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getDepreciationAcademicyear")]
        public HttpResponseMessage getDepreciationAcademicyear(string comp_year)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[show_calculated_depreciation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@company_code",comp_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();

                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


    }
}
