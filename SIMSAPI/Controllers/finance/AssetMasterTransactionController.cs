﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.finance
{
     [RoutePrefix("api/AssetMasterTransaction")]

    public class AssetMasterTransactionController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        [Route("CUDInsertFinsAssetMasterTransaction")]
         public HttpResponseMessage CUDInsertFinsAssetMasterTransaction(List<FinnAsttrans> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertFinsAssetMasterTransaction(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "FINANCE", "CUDInsertFinsAssetMasterTransaction"));

            bool result = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    foreach (FinnAsttrans finnobj in data)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_transaction_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", finnobj.opr),
                                new SqlParameter("@gam_item_no", finnobj.gam_item_no),
                                new SqlParameter("@gam_transaction_code", finnobj.gam_transaction_code),
                                new SqlParameter("@gam_quantity", finnobj.gam_quantity),
                                new SqlParameter("@gam_sell_to", finnobj.gam_sell_to),
                                new SqlParameter("@gam_cust_code", finnobj.gam_cust_code),
                                new SqlParameter("@gam_transaction_amount", finnobj.gam_transaction_amount),
                                new SqlParameter("@gam_revenue_acno", finnobj.gam_revenue_acno),
                                new SqlParameter("@gam_asset_disposal_acno", finnobj.gam_asset_disposal_acno),
                                new SqlParameter("@gam_created_by", finnobj.gam_created_by),
                                new SqlParameter("@gam_creater_remarks", finnobj.gam_creater_remarks),
                                new SqlParameter("@gam_approved_by", finnobj.gam_approved_by),
                                new SqlParameter("@gam_approved_date", finnobj.gam_approved_date),
                                new SqlParameter("@gam_approver_remarks", finnobj.gam_approver_remarks),
                                new SqlParameter("@gam_transaction_status", finnobj.gam_transaction_status),
                             });

                            if (dr.RecordsAffected > 0)
                            {
                                result = true;
                            }
                        }                       
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Inserting Asset Master Transaction Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("getaccounts")]
        public HttpResponseMessage getaccounts(string assettype, string compcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getaccounts(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FINANCE", "GetAccounts"));

            List<FinnAsttrans> acc_list = new List<FinnAsttrans>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "A"),
                                 new SqlParameter("@gal_asst_type", assettype),
                                 new SqlParameter("@gam_comp_code", compcode),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnAsttrans finnobj = new FinnAsttrans();
                            finnobj.ass_account = dr["asset_account"].ToString();
                            finnobj.ass_accountname = dr["asset_accname"].ToString();

                            acc_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acc_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acc_list);
            }
        }

        [Route("GetEmployees")]
        public HttpResponseMessage GetEmployees()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FINANCE", "GetEmployees"));

            List<FinnAsttrans> emp_list = new List<FinnAsttrans>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "B"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnAsttrans finnobj = new FinnAsttrans();
                            finnobj.employee_name = dr["em_name"].ToString();
                            finnobj.em_number = dr["em_number"].ToString();

                            emp_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_list);
            }
        }

        [Route("GetFins_AssetMaster")]
        public HttpResponseMessage GetFins_AssetMaster(string type, string gam_items)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFins_AssetMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GetFins_AssetMaster"));

            if (type == "undefined" || type == "")
            {
                type = null;
            }
            if (gam_items == "undefined" || gam_items == "")
            {
                gam_items = null;
            }

            List<Finn021> mod_list = new List<Finn021>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_asset_master_dpsd_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "N"),                                 
                                 new SqlParameter("@type", type),
                                 new SqlParameter("@gam_item_no", gam_items),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn021 finnobj = new Finn021();
                            finnobj.gam_comp_code = dr["gam_comp_code"].ToString();
                            finnobj.gam_dept_code = dr["gam_dept_code"].ToString();
                            finnobj.gam_dept_code_name = dr["gam_dept_code"].ToString() + '-' + dr["gam_dept_code_name"].ToString();
                            finnobj.gam_asst_type = dr["assettype_code"].ToString();
                            finnobj.gam_asst_type_name = dr["gam_asst_type"].ToString();
                            finnobj.gam_item_no = dr["gam_item_no"].ToString();
                            finnobj.gam_item_no1 = dr["gam_item_no"].ToString();
                            finnobj.gam_location = dr["loc_code"].ToString();
                            finnobj.gam_location_name = dr["gam_location"].ToString();
                            finnobj.gam_desc_1 = dr["gam_desc_1"].ToString();
                            finnobj.gam_desc_2 = dr["gam_desc_2"].ToString();
                            finnobj.gam_supl_name = dr["sup_name"].ToString();
                            finnobj.gam_supl_code = dr["gam_supl_name"].ToString();
                            finnobj.gam_acct_code = dr["gam_acct_code"].ToString();
                            finnobj.gam_order_no = dr["gam_order_no"].ToString();
                            finnobj.gam_invoice_no = dr["gam_invoice_no"].ToString();
                            finnobj.gam_new_sec = dr["gam_new_sec"].ToString();
                            if (string.IsNullOrEmpty(dr["gam_page_no"].ToString()) == false)
                                finnobj.gam_page_no = Decimal.Parse(dr["gam_page_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_line_no"].ToString()) == false)
                                finnobj.gam_line_no = Decimal.Parse(dr["gam_line_no"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_quantity"].ToString()) == false)
                                finnobj.gam_quantity = Decimal.Parse(dr["gam_quantity"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_invoice_amount"].ToString()) == false)
                                finnobj.gam_invoice_amount = Decimal.Parse(dr["gam_invoice_amount"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_cum"].ToString()) == false)
                                finnobj.gam_val_add_cum = Decimal.Parse(dr["gam_val_add_cum"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_mtd"].ToString()) == false)
                                finnobj.gam_val_add_mtd = Decimal.Parse(dr["gam_val_add_mtd"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_val_add_ytd"].ToString()) == false)
                                finnobj.gam_val_add_ytd = Decimal.Parse(dr["gam_val_add_ytd"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_book_value"].ToString()) == false)
                                finnobj.gam_book_value = Decimal.Parse(dr["gam_book_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_cum_deprn"].ToString()) == false)
                                finnobj.gam_cum_deprn = Decimal.Parse(dr["gam_cum_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_ytd_deprn"].ToString()) == false)
                                finnobj.gam_ytd_deprn = Decimal.Parse(dr["gam_ytd_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_mth_deprn"].ToString()) == false)
                                finnobj.gam_mth_deprn = Decimal.Parse(dr["gam_mth_deprn"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_sale_value"].ToString()) == false)
                                finnobj.gam_sale_value = Decimal.Parse(dr["gam_sale_value"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_repl_cost"].ToString()) == false)
                                finnobj.gam_repl_cost = Decimal.Parse(dr["gam_repl_cost"].ToString());
                            if (!string.IsNullOrEmpty(dr["gam_receipt_date"].ToString()))
                                finnobj.gam_receipt_date = Convert.ToDateTime(dr["gam_receipt_date"].ToString()).ToShortDateString();
                            if (!string.IsNullOrEmpty(dr["gam_sale_date"].ToString()))
                                finnobj.gam_sale_date = Convert.ToDateTime(dr["gam_sale_date"].ToString()).ToShortDateString();
                            if (!string.IsNullOrEmpty(dr["gam_entry_date"].ToString()))
                                finnobj.gam_entry_date = Convert.ToDateTime(dr["gam_entry_date"].ToString()).ToShortDateString();
                            if (!string.IsNullOrEmpty(dr["gam_amend_date"].ToString()))
                                finnobj.gam_amend_date = Convert.ToDateTime(dr["gam_amend_date"].ToString()).ToShortDateString();
                            if (string.IsNullOrEmpty(dr["gam_deprn_percent"].ToString()) == false)
                                finnobj.gam_deprn_percent = Decimal.Parse(dr["gam_deprn_percent"].ToString());
                            if (string.IsNullOrEmpty(dr["gam_used_on_item"].ToString()) == false)
                                finnobj.gam_used_on_item = Decimal.Parse(dr["gam_used_on_item"].ToString());
                            finnobj.gam_status_name = dr["gam_statusValue"].ToString();
                            finnobj.gam_status_code = dr["gam_status"].ToString();
                            finnobj.empcode = dr["ass_empid"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }
    }
}

//CREATE TABLE [fins].[fins_asset_master_transaction](
//    [gam_comp_code] [nvarchar](1) NOT NULL,
//    [gam_item_no] [nvarchar](6) NOT NULL,
//    [gam_transaction_code] [nvarchar](1) NOT NULL, --1: sale, 2: dispose, 
//    [gam_quantity] [numeric](14, 2) NULL,
//    [gam_sell_to] [nvarchar](50) NULL,
//    [gam_cust_code] [nvarchar](10) NULL,
//    [gam_transaction_amount] [numeric](17, 3) NULL, -- sale amount
//    [gam_revenue_acno] [nvarchar](10) NULL,
//    [gam_asset_disposal_acno] [nvarchar](10) NULL,
//    [gam_created_by] [nvarchar](10) NULL,
//    [gam_creater_remarks] [nvarchar](500) NULL,
//    [gam_approved_by] [nvarchar](10) NULL,
//    [gam_approved_date] [datetime] NULL,
//    [gam_approver_remarks] [nvarchar](500) NULL,
//    [gam_transaction_status] [nvarchar](1) NULL --1: open, 2: Close
	
//) ON [PRIMARY]