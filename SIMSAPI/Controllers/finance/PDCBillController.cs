﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Linq;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.PDCBillController
{
    [RoutePrefix("api/PDCBill")]
    [BasicAuthentication]
    public class PDCBillController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("Get_doc_narration")]
        public HttpResponseMessage Get_doc_narration(string cmpnycode, string doccode)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_doc_narration(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_PDC_PAYABLES_ACCOUNT"));

            List<Finn139> goaltarget_list = new List<Finn139>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_bills1",
                        new List<SqlParameter>()
                         {
                 new SqlParameter("@opr",'N'),               
                 new SqlParameter("@pb_comp_code",cmpnycode),
                 new SqlParameter("@pb_doc_type",doccode)
                   
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn139 FinnObj = new Finn139();
                            FinnObj.company_name = dr["gdn_doc_narration"].ToString();
                            goaltarget_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_PDC_PAYABLES_ACCOUNT")]
        public HttpResponseMessage Get_PDC_PAYABLES_ACCOUNT(string comp_code,string fyear)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_PDC_PAYABLES_ACCOUNT(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_PDC_PAYABLES_ACCOUNT"));

            List<Finn139> goaltarget_list = new List<Finn139>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_bills1",
                        new List<SqlParameter>()
                         {
                 new SqlParameter("@opr",'S'),
                 new SqlParameter("@opr_upd","PC"),
                  new SqlParameter("@pb_comp_code",comp_code),
                 new SqlParameter("@fins_appl_form_field","PDC PAYABLES ACCOUNT")
                   
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn139 FinnObj = new Finn139();
                            FinnObj.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            FinnObj.fins_appl_form_field_value1 = FinnObj.fins_appl_parameter + "-" + dr["fins_appl_form_field_value1"].ToString();
                            FinnObj.acc_code = dr["acc_code"].ToString();
                            FinnObj.dept_code = dr["dept_code"].ToString();
                            goaltarget_list.Add(FinnObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        
        [Route("Insert_PdcBill")]
        public HttpResponseMessage Insert_PdcBill(List<Finn212> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn212 finnobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_bills1",
                        new List<SqlParameter>()
                     {
                       
                    new SqlParameter("@opr", 'I'),
                    new SqlParameter("@pb_comp_code", finnobj.pb_comp_code),
                    new SqlParameter("@pb_sl_ldgr_code", finnobj.pb_sl_ldgr_code),
                    new SqlParameter("@pb_sl_acno", finnobj.pb_sl_acno),
                    new SqlParameter("@pb_dept_no", finnobj.pb_dept_no),
                    new SqlParameter("@pb_doc_type", finnobj.pb_doc_type),
                    new SqlParameter("@pb_our_doc_no", finnobj.pb_our_doc_no),
                    new SqlParameter("@pb_rec_serial", finnobj.pb_rec_serial),
                    new SqlParameter("@pb_bank_from", finnobj.pb_bank_from),
                    new SqlParameter("@pb_bank_slno", finnobj.pb_bank_slno),
                    new SqlParameter("@pb_cheque_no", finnobj.pb_cheque_no),
                    new SqlParameter("@pb_calendar", finnobj.pb_calendar),                 
                    new SqlParameter("@pb_due_date", (finnobj.pb_due_date)),
                    new SqlParameter("@pb_amount", finnobj.pb_amount),
                    new SqlParameter("@pb_pstng_no", finnobj.pb_pstng_no),
                    new SqlParameter("@pb_narrative", finnobj.pb_narrative),
                    new SqlParameter("@pb_bank_to", finnobj.pb_bank_to),
                    new SqlParameter("@pb_discd", finnobj.pb_discd),
                    new SqlParameter("@pb_ret_times", finnobj.pb_ret_times),
                    new SqlParameter("@pb_ref_no", finnobj.pb_ref_no),
                    new SqlParameter("@pb_resub_ref", finnobj.pb_resub_ref),                   
                    new SqlParameter("@pb_cur_date", (finnobj.pb_cur_date)),
                    new SqlParameter("@pb_amd_flag", finnobj.pb_amd_flag),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

       

        [Route("GetComp_Name")]
        public HttpResponseMessage GetComp_Name(string username)
        {
            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@tbl_cond", username),
                new SqlParameter("@opr", "A"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 fin006Obj = new Finn102();
                            fin006Obj.comp_name = dr["comp_name"].ToString();
                            fin006Obj.comp_code = dr["comp_code"].ToString();

                            house.Add(fin006Obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


        [Route("GetFinancial_year")]
        public HttpResponseMessage GetFinancial_year(string comp_code)
        {


            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@opr_mem", comp_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 simsobj = new Finn102();
                            if (!string.IsNullOrEmpty(dr["sims_financial_year"].ToString()))
                                simsobj.financial_year = decimal.Parse(dr["sims_financial_year"].ToString());

                            house.Add(simsobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


        [Route("getBankDeatails")]
        public HttpResponseMessage getBankDeatails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBankDeatails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getBankDeatails"));

            List<Finn141> goaltarget_list = new List<Finn141>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 objNew = new Finn141();
                            objNew.slma_pty_bank_id = dr["pb_bank_code"].ToString();
                            objNew.slma_pty_bank_name = dr["pb_bank_code"].ToString() + "-" + dr["pb_bank_name"].ToString();
                            objNew.pb_gl_acno = dr["pb_gl_acno"].ToString();
                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getPdcReceivableAcc")]
        public HttpResponseMessage getPdcReceivableAcc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetPdcReceivableAcc(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetPdcReceivableAcc"));

            List<Finn141> goaltarget_list = new List<Finn141>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           new SqlParameter("@comp_code", 1),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 objNew = new Finn141();
                            objNew.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            objNew.fins_appl_form_field_value2 = dr["fins_appl_form_field_value2"].ToString();
                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Checkstattus_doc_users")]
        public HttpResponseMessage Checkstattus_doc_users(string comp_code, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Checkstattus_doc_users()PARAMETERS ::NA";
            List<string> com_list = new List<string>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_sblgr_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@slma_comp_code",comp_code),
                            new SqlParameter("@gldu_user_name",user)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Fin150 simsobj = new Fin150();
                            com_list.Add(dr["gldu_prepare"].ToString());
                            com_list.Add(dr["gldu_verify"].ToString());
                            com_list.Add(dr["gldu_authorize"].ToString());
                            //getAllRecords.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


    }

}










