﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.financeClass;
using System.Collections;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.finance
{
      [RoutePrefix("api/documents")]
    public class DocumentsController:ApiController
    {
        #region Finn055

        //Fins055  Document .....................................................................................

        [Route("Fins055_Get_Fins_Document")]
          public HttpResponseMessage Fins055_Get_Fins_Document()
        {
            List<Finn055> obj_fins055 = new List<Finn055>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.fins_document_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'S')
               });
                    while (dr.Read())
                    {
                        Finn055 obj_list = new Finn055();
                        obj_list.codo_comp_code_name = dr["comp_name"].ToString();
                        obj_list.codo_doc_code = dr["codo_doc_code"].ToString();
                        obj_list.codo_doc_name = dr["codo_doc_name"].ToString();
                        obj_list.codo_doc_type = dr["sims_appl_form_field_value1"].ToString();
                        obj_list.codo_st_doc_no = Convert.ToInt32(dr["codo_st_doc_no"]);
                        obj_list.codo_end_doc_no = Convert.ToInt32(dr["codo_end_doc_no"]);
                        if (dr["codo_dr_cr_flag"].ToString().Equals("Y"))
                        {
                            obj_list.codo_dr_cr_flag = true;
                        }
                        else
                        {
                            obj_list.codo_dr_cr_flag = false;
                        }

                        if (dr["codo_sp_doc_flag"].ToString().Equals("Y"))
                        {
                            obj_list.codo_sp_doc_flag = true;
                        }
                        else
                        {
                            obj_list.codo_sp_doc_flag = false;
                        }
                        obj_fins055.Add(obj_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, obj_fins055);
        }

        [Route("Fins055_Insert_fins_Document")]
        public HttpResponseMessage Fins055_Insert_fins_Document(Finn055 fins_doc_insert)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.fins_document_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "I"),
                new SqlParameter("@company_name", fins_doc_insert.codo_comp_code_name),
                new SqlParameter("@codo_doc_code", fins_doc_insert.codo_doc_code),
                new SqlParameter("@codo_doc_name", fins_doc_insert.codo_doc_name),
                new SqlParameter("@codo_doc_type_name", fins_doc_insert.codo_doc_type),
                new SqlParameter("@codo_st_doc_no", fins_doc_insert.codo_st_doc_no),
                new SqlParameter("@codo_end_doc_no", fins_doc_insert.codo_end_doc_no),
                new SqlParameter("@codo_dr_cr_flag", fins_doc_insert.codo_dr_cr_flag == true?"Y":"N"),
                new SqlParameter("@codo_sp_doc_flag", fins_doc_insert.codo_sp_doc_flag == true?"Y":"N")
                  });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("Fins055_get_company")]
        public HttpResponseMessage Fins055_get_company()
        {
            List<Finn055> company_list = new List<Finn055>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S")
               });
                    while (dr.Read())
                    {
                        Finn055 fins_list = new Finn055();
                        fins_list.codo_comp_code_name = dr["comp_name"].ToString();
                        company_list.Add(fins_list);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, company_list);

        }

        [Route("fins055_Autoincrement")]
        public HttpResponseMessage fins055_Autoincrement()
        {
            Finn055 obj = new Finn055();
            try
            {
               using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.fins_document_proc",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "A")
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (dr["div_code"].ToString() != "")
                                obj.codo_doc_code = dr["div_code"].ToString();
                            else
                                obj.codo_doc_code = "1";
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj.codo_doc_code);

        }

        [Route("GetAllBellCode")]
        public HttpResponseMessage GetAllBellCode()
        {
            List<Finn055> com_list = new List<Finn055>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.fins_document_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn055 finsobj = new Finn055();
                            finsobj.codo_doc_type = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(finsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        #endregion
        
    }
}