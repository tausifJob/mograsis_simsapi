﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using System.Collections;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
using System.Data;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/GLAccountsCreateView")]
    public class GLAccountsCreateViewController : ApiController
    {

        [Route("GetAll_Accountcode")]
        public HttpResponseMessage GetAll_Accountcode(string co_code, string year)
        {
            // GetCurrentFinancialYear();
            List<Finn014> com_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                      new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C"),
                new SqlParameter("@glma_comp_code",co_code),
                new SqlParameter("@glma_year", year),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn014 FinnObj = new Finn014();
                            FinnObj.glma_acct_name = dr["glac_comp_code"].ToString()+"-"+dr["glac_acct_code"].ToString() + " - " + dr["glac_name"].ToString() + " (" + dr["glac_type"].ToString() + ")";
                            FinnObj.glma_acct_code = dr["glac_acct_code"].ToString();
                            FinnObj.glac_comp_code = dr["glac_comp_code"].ToString();
                          
                            
                            com_list.Add(FinnObj);
                        }
                    }
                }
            }

            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("GetAll_Accountcode_openingBalance")]
        public HttpResponseMessage GetAll_Accountcode_openingBalance(string co_code, string year)
        {
            // GetCurrentFinancialYear();
            List<Finn014> com_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_gl_masters",
                      new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                new SqlParameter("@glma_comp_code",co_code),
                new SqlParameter("@glma_year", year),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn014 FinnObj = new Finn014();
                         //   FinnObj.glma_acct_name = dr["glac_comp_code"].ToString() + "-" + dr["glac_acct_code"].ToString() + " - " + dr["glac_name"].ToString() + " (" + dr["glac_type"].ToString() + ")";
                            FinnObj.glma_acct_code = dr["glma_acct_code"].ToString();
                            FinnObj.glma_acct_name = dr["glma_acct_name"].ToString();


                            com_list.Add(FinnObj);
                        }
                    }
                }
            }

            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("GetAll_ledgercode")]
        public HttpResponseMessage GetAll_ledgercode(string co_code, string year)
        {
            List<Finn014> com_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", "Q"),
                  
                    new SqlParameter("@glma_comp_code",co_code),
                    new SqlParameter("@glma_year", year) 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn014 FinnObj = new Finn014();
                            FinnObj.glma_ldgr_name = dr["sllc_ldgr_code"].ToString() + " - " + dr["sllc_ldgr_type"].ToString();
                            FinnObj.glma_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            com_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("GetAll_glmaster_Department_Code")]
        public HttpResponseMessage GetAll_glmaster_Department_Code(string co_code,string year)
        {
            List<Finn014> com_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_departments_proc]",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "P"),
                new SqlParameter("@codp_comp_code",co_code),
                new SqlParameter("@codp_year",year),
                       });
                    while (dr.Read())
                    {
                        Finn014 FinnObj = new Finn014();
                        FinnObj.department_name = dr["codp_dept_no"].ToString() + " - " + dr["codp_dept_name"].ToString();
                        FinnObj.glma_dept_no = dr["codp_dept_no"].ToString();
                        com_list.Add(FinnObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("GetAll_partyclass_Code")]
        public HttpResponseMessage GetAll_partyclass_Code(string co_code, string year)
        {
            string lc = "00";
            List<Finn014> com_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@glma_comp_code", co_code),
                new SqlParameter("@lc",  lc),
                new SqlParameter("@glma_year", year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn014 FinnObj = new Finn014();
                            FinnObj.slac_cntrl_class_desc = dr["slac_cntrl_class"].ToString() + " - " + dr["slac_cntrl_class_desc"].ToString();
                            FinnObj.glma_pty_class = dr["slac_cntrl_class"].ToString();
                            com_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("GetFinn_gl_master_details_Finn014")]
        public HttpResponseMessage GetFinn_gl_master_details_Finn014(string co_code,string year)
        {
            List<Finn014> fins_list = new List<Finn014>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'S'),
                new SqlParameter("@glma_comp_code", co_code),
                new SqlParameter("@glma_year", year),
              //  new SqlParameter("@fins_comp_code","1"),
              //  new SqlParameter("@fins_appl_code", "Fin014"),
              //  new SqlParameter("@fins_appl_form_field", "GLPartyClass")
                });
                    while (dr.Read())
                    {
                        Finn014 Finnobj = new Finn014();
                        Finnobj.glma_comp_name = dr["glma_comp_name"].ToString();
                        Finnobj.glma_comp_code = dr["glma_comp_code"].ToString();
                        Finnobj.glma_year = decimal.Parse(dr["glma_year"].ToString());
                        Finnobj.glma_dept_no = dr["glma_dept_no"].ToString();
                        Finnobj.codp_dept_name = dr["codp_dept_name"].ToString();
                        Finnobj.glma_acct_code = dr["glma_acct_code"].ToString();

                        if (dr["glma_check_digit"].ToString().Equals("A"))
                        {
                            Finnobj.glma_check_digit = true;
                        }
                        else
                        {
                            Finnobj.glma_check_digit = false;
                        }

                        Finnobj.glma_acct_name = dr["glma_acct_code1"].ToString();
                        Finnobj.glma_op_bal_amt = decimal.Parse(dr["glma_op_bal_amt"].ToString());
                        Finnobj.glma_ldgr_code = dr["glma_ldgr_code"].ToString();
                        Finnobj.glma_ldgr_name = dr["glma_ldgr_code1"].ToString();
                        Finnobj.slac_cntrl_class_desc = dr["glma_pty_class"].ToString();
                        Finnobj.glma_pty_class = dr["glma_pty_class1"].ToString();
                        Finnobj.glma_status = dr["glma_status"].Equals("A") ? true : false;
                        Finnobj.glma_stmt_reqd = dr["glma_stmt_reqd"].Equals("Y") ? true : false;
                        Finnobj.glma_fc_flag = dr["glma_fc_flag"].Equals("Y") ? true : false;
                        Finnobj.glma_qty_flag = dr["glma_qty_flag"].Equals("Y") ? true : false; 

                        //if (dr["glma_status"].ToString().Equals("A"))
                        //{
                        //    Finnobj.glma_status = true;
                        //}
                        //else
                        //{
                        //    Finnobj.glma_status = false;
                        //}

                        //if (dr["glma_stmt_reqd"].ToString().Equals("Y"))
                        //{
                        //    Finnobj.glma_stmt_reqd = true;
                        //}
                        //else
                        //{
                        //    Finnobj.glma_stmt_reqd = false;
                        //}
                        //if (dr["glma_fc_flag"].ToString().Equals("Y"))
                        //{
                        //    Finnobj.glma_fc_flag = true;
                        //}
                        //else
                        //{
                        //    Finnobj.glma_fc_flag = false;
                        //}
                        //if (dr["glma_qty_flag"].ToString().Equals("Y"))
                        //{
                        //    Finnobj.glma_qty_flag = true;
                        //}
                        //else
                        //{
                        //    Finnobj.glma_qty_flag = false;
                        //}
                        fins_list.Add(Finnobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, fins_list);

        }

        [Route("InsertFinn_gl_master_Finn014")]
        public HttpResponseMessage InsertFinn_gl_master_Finn014(Finn014 finnobj)
        {
            string Inserted = string.Empty;
            

            try
            {

                finnobj.glma_tdy_dr = 0;
                finnobj.glma_tdy_cr = 0;
                finnobj.glma_net_dr = 0;
                finnobj.glma_net_cr = 0;
                finnobj.glma_yr_end_tran_amt = 0;
                finnobj.glma_net_dr_orig = 0;
                finnobj.glma_net_cr_orig = 0;
                finnobj.glma_op_bal_amt_orig = 0;
                finnobj.glma_reval_date = System.DateTime.Today;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'I'),
                new SqlParameter("@sub_opr", 'N'),
               new SqlParameter("@glma_comp_code",finnobj.comp_code),
               new SqlParameter("@glma_year",finnobj.finance_year),
               new SqlParameter("@glma_dept_no",finnobj.glma_dept_no),//.Substring(0, 2),
               new SqlParameter("@glma_acct_code", finnobj.glma_acct_code),
               new SqlParameter("@glma_check_digit", finnobj.glma_check_digit.Equals(true)?"A":"I"),
               new SqlParameter("@glma_acct_name", finnobj.glma_acct_name),
               new SqlParameter("@glma_tdy_dr", finnobj.glma_tdy_dr),
               new SqlParameter("@glma_tdy_cr", finnobj.glma_tdy_cr),// finnobj.glma_tdy_cr,
               new SqlParameter("@glma_net_dr", finnobj.glma_net_dr),//finnobj.glma_net_dr,
               new SqlParameter("@glma_net_cr", finnobj.glma_net_cr),// finnobj.glma_net_cr,
               new SqlParameter("@glma_op_bal_amt", finnobj.glma_op_bal_amt),
               new SqlParameter("@glma_yr_end_tran_amt",finnobj.glma_yr_end_tran_amt),// finnobj.glma_yr_end_tran_amt,
               new SqlParameter("@glma_status",finnobj.glma_status.Equals(true)?"A":"I"),
               new SqlParameter("@glma_stmt_reqd", finnobj.glma_stmt_reqd == true?"Y":"N"),
               new SqlParameter("@glma_ldgr_code",finnobj.glma_ldgr_code),
               new SqlParameter("@glma_pty_class", finnobj.glma_pty_class),
               new SqlParameter("@glma_fc_flag", finnobj.glma_fc_flag == true?"Y":"N"),
               new SqlParameter("@glma_qty_flag", finnobj.glma_qty_flag == true?"Y":"N"),
               new SqlParameter("@glma_net_dr_orig",finnobj.glma_net_dr_orig),// finnobj.glma_net_dr_orig,
               new SqlParameter("@glma_net_cr_orig",finnobj.glma_net_cr_orig),// finnobj.glma_net_cr_orig,
               new SqlParameter("@glma_op_bal_amt_orig",finnobj.glma_op_bal_amt),//glma_op_bal_amt_orig,// finnobj.glma_op_bal_amt_orig,
               new SqlParameter("@glma_reval_date", finnobj.glma_reval_date),
               new SqlParameter("@glpr_bud_amt", finnobj.glpr_bud_amt),//glpr_bud_amt,
               new SqlParameter("@glpr_bud_qty", finnobj.account_budget_quntity),// glpr_bud_qty,
 });
                    if (dr.Read())
                    {
                        Inserted = dr[0].ToString();
                    }
                }
            }
            catch (Exception c)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        //[Route("UpdateFinn_gl_master_Finn014")]
        //public HttpResponseMessage UpdateFinn_gl_master_Finn014(Finn014 finnobj)
        //{
        //    bool Update = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
        //               new List<SqlParameter>() 
        //                 { 
        //       new SqlParameter("@opr", 'U'),
        //       new SqlParameter("@sub_opr", "GLU"),
        //       new SqlParameter("@glma_comp_code",finnobj.glma_comp_code),
        //       new SqlParameter("@glma_year",financialyear1()),
        //       new SqlParameter("@glma_dept_no", finnobj.glma_dept_no),
        //       new SqlParameter("@glma_acct_code",finnobj.glma_acct_code),
        //       new SqlParameter("@glma_check_digit", finnobj.glma_check_digit == true?"A":"I"),
        //       new SqlParameter("@glma_status",finnobj.glma_status == true?"A":"I"),
        //       new SqlParameter("@glma_stmt_reqd", finnobj.glma_stmt_reqd == true?"Y":"N"),
        //       new SqlParameter("@glma_pty_class",finnobj.glma_pty_class),
        //       new SqlParameter("@glma_fc_flag", finnobj.glma_fc_flag == true?"Y":"N"),
        //       new SqlParameter("@glma_qty_flag", finnobj.glma_qty_flag == true?"Y":"N"),
        //       new SqlParameter("@glpr_bud_amt",finnobj.glpr_bud_amt),
        //       new SqlParameter("@glpr_bud_qty",finnobj.glpr_bud_qty)

        //                 });
        //            if (dr.RecordsAffected > 0)
        //            {
        //                Update = true;
        //            }
        //        }
        //    }

        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, Update);

        //}


        [Route("UpdateFinn_gl_master_Finn014")]
        public HttpResponseMessage UpdateFinn_gl_master_Finn014(Finn014 finnobj)
        {
            bool Update = false;

            if (finnobj.glpr_bud_amt == null)
                finnobj.glpr_bud_amt = "0";
            if (finnobj.glpr_bud_qty == null)
                finnobj.glpr_bud_qty = "0";
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
               new SqlParameter("@opr", 'U'),
               new SqlParameter("@sub_opr", "GLU"),
               new SqlParameter("@glma_comp_code",finnobj.comp_code),
               new SqlParameter("@glma_year",finnobj.finance_year),
               new SqlParameter("@glma_dept_no", finnobj.glma_dept_no),
               new SqlParameter("@glma_acct_code",finnobj.glma_acct_code),
               new SqlParameter("@glma_check_digit", finnobj.glma_check_digit == true?"A":"I"),
               new SqlParameter("@glma_status",finnobj.glma_status == true?"A":"I"),
               new SqlParameter("@glma_stmt_reqd", finnobj.glma_stmt_reqd == true?"Y":"N"),
               new SqlParameter("@glma_pty_class",finnobj.glma_pty_class),
               new SqlParameter("@glma_fc_flag", finnobj.glma_fc_flag == true?"Y":"N"),
               new SqlParameter("@glma_qty_flag", finnobj.glma_qty_flag == true?"Y":"N"),
               new SqlParameter("@glma_op_bal_amt_orig",finnobj.glma_op_bal_amt),


               new SqlParameter("@glpr_bud_amt",finnobj.glpr_bud_amt),

               new SqlParameter("@glpr_bud_qty",finnobj.glpr_bud_qty)

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        Update = true;
                    }

                    else
                    {
                        Update = false;
                    }
                }
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Update);

        }


#region  GL Opening Balanace APIS

        [Route("getGLOpenidgBalance")]
        public HttpResponseMessage getGLOpenidgBalance(string co_code, string year,string dept_no,string account_code)
        {

            List<Finn014> fins_list = new List<Finn014>();
            if (dept_no == "undefined")
            {
                dept_no = null;
            }
            if (account_code == "undefined")
            {
                account_code = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_gl_masters_proc]",
                       new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'K'),
                              new SqlParameter("@glma_comp_code", co_code),
                              new SqlParameter("@glma_year", year),
                              new SqlParameter("@glma_dept_no", dept_no),
                              new SqlParameter("@glma_acct_code", account_code),
                         });
                    while (dr.Read())
                    {
                        Finn014 Finnobj = new Finn014();
                        Finnobj.glma_comp_name = dr["glma_comp_name"].ToString();
                        Finnobj.glma_comp_code = dr["glma_comp_code"].ToString();
                        Finnobj.glma_year = decimal.Parse(dr["glma_year"].ToString());
                        Finnobj.glma_dept_no = dr["glma_dept_no"].ToString();
                        Finnobj.codp_dept_name = dr["codp_dept_name"].ToString();
                        Finnobj.glma_acct_code = dr["glma_acct_code"].ToString();
                        Finnobj.glma_acct_name = dr["glma_acct_name"].ToString();
                        Finnobj.glma_op_bal_amt = decimal.Parse(dr["glma_op_bal_amt"].ToString());
                        Finnobj.glma_op_bal_amt_old_val = decimal.Parse(dr["glma_op_bal_amt"].ToString());
                        Finnobj.glma_ldgr_code = dr["glma_ldgr_code"].ToString();

                        
                        fins_list.Add(Finnobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, fins_list);

        }


        [Route("CUDGLOpeningBAl")]
        public HttpResponseMessage CUDGLOpeningBAl(List<Finn014> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Finn014 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_gl_masters_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@glma_comp_code",simsobj.glma_comp_code),
                                new SqlParameter("@glma_year", simsobj.glma_year),
                                new SqlParameter("@glma_dept_no", simsobj.glma_dept_no),
                                new SqlParameter("@glma_acct_code", simsobj.glma_acct_code),
                                new SqlParameter("@glma_op_bal_amt", simsobj.glma_op_bal_amt),
                                new SqlParameter("@glma_op_bal_amt_old_val", simsobj.glma_op_bal_amt_old_val),
                                new SqlParameter("@login_user", simsobj.login_user),
                                new SqlParameter("@current_date", simsobj.current_date),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


#endregion

        #region  Student Fee Ledger APIS

       

        [Route("getstudentFeeLedger")]
        public HttpResponseMessage getstudentFeeLedger(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string user_number)
        {
            List<Finn014> fins_list = new List<Finn014>();

            if (user_number == "undefined")
            {
                user_number = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_student_fee_ledger_proc]",
                       new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@sims_cur_code", sims_cur_code),
                              new SqlParameter("@sims_academic_year", sims_academic_year),
                              new SqlParameter("@sims_grade_code", sims_grade_code),
                              new SqlParameter("@sims_section_code", sims_section_code),
                               new SqlParameter("@user_number", user_number)
                         });
                    while (dr.Read())
                    {
                        Finn014 Finnobj = new Finn014();
                        Finnobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                        Finnobj.student_name = dr["student_name"].ToString();
                        Finnobj.grade_name = dr["grade_name"].ToString();
                        Finnobj.section_name = dr["section_name"].ToString();
                        Finnobj.father_name = dr["father_name"].ToString();
                        Finnobj.mother_name = dr["mother_name"].ToString();
                        Finnobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();


                        fins_list.Add(Finnobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, fins_list);

        }



        #endregion

    }
}