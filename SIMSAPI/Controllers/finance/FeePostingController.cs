﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.FINNANCE;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/common/FeePosting")]

    public class FeePostingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllCurName")]
        public HttpResponseMessage GetAllCurName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ATTENDANCE", "ATTENDANCECRITERIA"));

            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_cur]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetAllAcademicYear")]
        public HttpResponseMessage GetAllAcademicYear(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_academic_year]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'G'),
                            new SqlParameter("@sims_cur_code",cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_academic_year_start_date = dr["sims_academic_year_start_date"].ToString();
                            objNew.sims_academic_year_end_date = dr["sims_academic_year_end_date"].ToString();
                            objNew.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetAllGrades")]
        public HttpResponseMessage GetAllGrades(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "GetAllGrades", "AcademicYear"));

            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_grade]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year",ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetAllSections")]
        public HttpResponseMessage GetAllSections(string cur_code, string ac_year, string g_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSections(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "GetAllSections", "GetAllSections"));

            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_section]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'G'),
                            new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year",ac_year),
                              new SqlParameter("@sims_g_code",g_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetAllAccount_Name")]
        public HttpResponseMessage GetAllAccount_Name(string comp_code, string financialyr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllAccount_Name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Account_Name", "Account_Name"));

            List<Finn155> fin_list = new List<Finn155>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_gl_masters]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'G'),
                             new SqlParameter("@glma_comp_code", comp_code),
                             new SqlParameter("@glma_year", financialyr),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn155 FinnObj = new Finn155();
                            FinnObj.fins_revenue_acno = dr["glma_acct_code"].ToString();
                            FinnObj.fins_revenue_acname = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_jan = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_jan = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_feb = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_feb = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_mar = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_mar = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_apr = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_apr = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_may = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_may = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_jun = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_jun = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_jul = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_jul = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_aug = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_aug = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_sep = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_sep = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_oct = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_oct = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_nov = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_nov = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_received_acno_dec = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_received_acname_dec = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_receivable_acno = dr["glma_acct_code"].ToString();
                            FinnObj.fins_receivable_acname = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_discount_acno = dr["glma_acct_code"].ToString();
                            FinnObj.fins_discount_acname = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();

                            FinnObj.fins_advance_academic_year_acno = dr["glma_acct_code"].ToString();
                            FinnObj.fins_advance_academic_year_acname = dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();
                            fin_list.Add(FinnObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fin_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, fin_list);

            }
        }

        //[Route("GetAllfins_fees_posting_accrual")]
        //public HttpResponseMessage GetAllfins_fees_posting_accrual(string cur_code, string ac_year, string g_code, string sec_code)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllfins_fees_posting_accrual(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "GetAllfins_fees_posting_accrual", "GetAllfins_fees_posting_accrual"));

        //    List<Finn155> finn_list = new List<Finn155>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_fees_posting_accrual_monthly_proc]",
        //                new List<SqlParameter>() 
        //                { 
        //                   new SqlParameter("@opr", 'S'),
        //                   new SqlParameter("@sims_cur_code", cur_code),
        //                   new SqlParameter("@sims_academic_year", ac_year),
        //                   new SqlParameter("@sims_grade_code", g_code),
        //                   new SqlParameter("@sims_section_code", sec_code),
        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Finn155 finn = new Finn155();
        //                    finn.receipt_based = dr["receipt_based"].ToString();
        //                    finn.fins_fee_cur_code = dr["fins_fee_cur_code"].ToString();
        //                    finn.fins_fee_academic_year = dr["fins_fee_academic_year"].ToString();
        //                    finn.fins_fee_number = dr["fins_fee_number"].ToString();
        //                    finn.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
        //                    finn.fins_revenue_acno = dr["fins_revenue_acno"].ToString();
        //                    finn.fins_advance_received_acno_jan = dr["fins_advance_received_acno_jan"].ToString();
        //                    finn.fins_advance_received_acname_jan = dr["fins_advance_received_acname_jan"].ToString();
        //                    if (string.IsNullOrEmpty(finn.receipt_based))
        //                    {
        //                        finn.fins_advance_received_acno_feb = dr["fins_advance_received_acno_feb"].ToString();
        //                        finn.fins_advance_received_acname_feb = dr["fins_advance_received_acname_feb"].ToString();
        //                        finn.fins_advance_received_acno_mar = dr["fins_advance_received_acno_mar"].ToString();
        //                        finn.fins_advance_received_acname_mar = dr["fins_advance_received_acname_mar"].ToString();
        //                        finn.fins_advance_received_acno_apr = dr["fins_advance_received_acno_apr"].ToString();
        //                        finn.fins_advance_received_acname_apr = dr["fins_advance_received_acname_apr"].ToString();
        //                        finn.fins_advance_received_acno_may = dr["fins_advance_received_acno_may"].ToString();
        //                        finn.fins_advance_received_acname_may = dr["fins_advance_received_acname_may"].ToString();
        //                        finn.fins_advance_received_acno_jun = dr["fins_advance_received_acno_jun"].ToString();
        //                        finn.fins_advance_received_acname_jun = dr["fins_advance_received_acname_jun"].ToString();
        //                        finn.fins_advance_received_acno_jul = dr["fins_advance_received_acno_jul"].ToString();
        //                        finn.fins_advance_received_acname_jul = dr["fins_advance_received_acname_jul"].ToString();
        //                        finn.fins_advance_received_acno_aug = dr["fins_advance_received_acno_aug"].ToString();
        //                        finn.fins_advance_received_acname_aug = dr["fins_advance_received_acname_aug"].ToString();
        //                        finn.fins_advance_received_acno_sep = dr["fins_advance_received_acno_sep"].ToString();
        //                        finn.fins_advance_received_acname_sep = dr["fins_advance_received_acname_sep"].ToString();
        //                        finn.fins_advance_received_acno_oct = dr["fins_advance_received_acno_oct"].ToString();
        //                        finn.fins_advance_received_acname_oct = dr["fins_advance_received_acname_oct"].ToString();
        //                        finn.fins_advance_received_acno_nov = dr["fins_advance_received_acno_nov"].ToString();
        //                        finn.fins_advance_received_acname_nov = dr["fins_advance_received_acname_nov"].ToString();
        //                        finn.fins_advance_received_acno_dec = dr["fins_advance_received_acno_dec"].ToString();
        //                        finn.fins_advance_received_acname_dec = dr["fins_advance_received_acname_dec"].ToString();
        //                    }
        //                    finn.fins_receivable_acno = dr["fins_receivable_acno"].ToString();

        //                    if (dr["fins_fees_posting_status"].ToString().Equals("A"))
        //                        finn.fins_fees_posting_status = true;
        //                    else
        //                        finn.fins_fees_posting_status = false;

        //                    finn.fins_discount_acno = dr["fins_discount_acno"].ToString();
        //                    finn.fins_advance_academic_year_acno = dr["fins_advance_academic_year_acno"].ToString();

        //                    //finn.AccCollection = new List<Finn155>();
        //                    //finn.AccCollection = finn_list1;
        //                    finn_list.Add(finn);
        //                }
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, finn_list);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e);
        //        return Request.CreateResponse(HttpStatusCode.OK, finn_list);

        //    }
        //}

        [Route("GetAllfins_fees_posting_accrual")]
        public HttpResponseMessage GetAllfins_fees_posting_accrual(string cur_code, string ac_year, string g_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllfins_fees_posting_accrual(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "GetAllfins_fees_posting_accrual", "GetAllfins_fees_posting_accrual"));

            List<Finn155> finn_list = new List<Finn155>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_fees_posting_accrual_monthly_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@IGNORED_GRADE_LIST", g_code),
                            new SqlParameter("@IGNORED_SECTION_LIST", sec_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn155 finn = new Finn155();
                            finn.receipt_based = dr["receipt_based"].ToString();
                            finn.fins_fee_cur_code = dr["fins_fee_cur_code"].ToString();
                            finn.fins_fee_academic_year = dr["fins_fee_academic_year"].ToString();
                            finn.fins_fee_number = dr["fins_fee_number"].ToString();
                            finn.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            finn.fins_revenue_acno = dr["fins_revenue_acno"].ToString();
                            finn.fins_advance_received_acno_jan = dr["fins_advance_received_acno_jan"].ToString();
                            finn.fins_advance_received_acname_jan = dr["fins_advance_received_acname_jan"].ToString();
                            if (string.IsNullOrEmpty(finn.receipt_based))
                            {
                                finn.fins_advance_received_acno_feb = dr["fins_advance_received_acno_feb"].ToString();
                                finn.fins_advance_received_acname_feb = dr["fins_advance_received_acname_feb"].ToString();
                                finn.fins_advance_received_acno_mar = dr["fins_advance_received_acno_mar"].ToString();
                                finn.fins_advance_received_acname_mar = dr["fins_advance_received_acname_mar"].ToString();
                                finn.fins_advance_received_acno_apr = dr["fins_advance_received_acno_apr"].ToString();
                                finn.fins_advance_received_acname_apr = dr["fins_advance_received_acname_apr"].ToString();
                                finn.fins_advance_received_acno_may = dr["fins_advance_received_acno_may"].ToString();
                                finn.fins_advance_received_acname_may = dr["fins_advance_received_acname_may"].ToString();
                                finn.fins_advance_received_acno_jun = dr["fins_advance_received_acno_jun"].ToString();
                                finn.fins_advance_received_acname_jun = dr["fins_advance_received_acname_jun"].ToString();
                                finn.fins_advance_received_acno_jul = dr["fins_advance_received_acno_jul"].ToString();
                                finn.fins_advance_received_acname_jul = dr["fins_advance_received_acname_jul"].ToString();
                                finn.fins_advance_received_acno_aug = dr["fins_advance_received_acno_aug"].ToString();
                                finn.fins_advance_received_acname_aug = dr["fins_advance_received_acname_aug"].ToString();
                                finn.fins_advance_received_acno_sep = dr["fins_advance_received_acno_sep"].ToString();
                                finn.fins_advance_received_acname_sep = dr["fins_advance_received_acname_sep"].ToString();
                                finn.fins_advance_received_acno_oct = dr["fins_advance_received_acno_oct"].ToString();
                                finn.fins_advance_received_acname_oct = dr["fins_advance_received_acname_oct"].ToString();
                                finn.fins_advance_received_acno_nov = dr["fins_advance_received_acno_nov"].ToString();
                                finn.fins_advance_received_acname_nov = dr["fins_advance_received_acname_nov"].ToString();
                                finn.fins_advance_received_acno_dec = dr["fins_advance_received_acno_dec"].ToString();
                                finn.fins_advance_received_acname_dec = dr["fins_advance_received_acname_dec"].ToString();
                            }
                            finn.fins_receivable_acno = dr["fins_receivable_acno"].ToString();

                            if (dr["fins_fees_posting_status"].ToString().Equals("A"))
                                finn.fins_fees_posting_status = true;
                            else
                                finn.fins_fees_posting_status = false;

                            finn.fins_discount_acno = dr["fins_discount_acno"].ToString();
                            finn.fins_advance_academic_year_acno = dr["fins_advance_academic_year_acno"].ToString();

                            //finn.AccCollection = new List<Finn155>();
                            //finn.AccCollection = finn_list1;
                            finn_list.Add(finn);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, finn_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, finn_list);

            }
        }

        [Route("Getfins_All_fees_posting")]
        public HttpResponseMessage Getfins_All_fees_posting(string cur_code, string ac_year, string g_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getfins_All_fees_posting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Getfins_All_fees_posting", "Getfins_All_fees_posting"));

            List<Finn155> finn_list = new List<Finn155>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_fees_posting_accrual_monthly_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@sims_grade_code", g_code),
                            new SqlParameter("@sims_section_code", sec_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn155 finn = new Finn155();
                            finn.receipt_based = dr["receipt_based"].ToString();
                            finn.fins_fee_number = dr["sims_fee_code"].ToString();
                            finn.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            if (dr["fins_fees_posting_status"].ToString().Equals("A"))
                                finn.fins_fees_posting_status = true;
                            else
                                finn.fins_fees_posting_status = false;

                            finn_list.Add(finn);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, finn_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, finn_list);

            }
        }

        [Route("CUDUpdate_fins_fees_posting")]
        public HttpResponseMessage CUDUpdate_fins_fees_posting(List<Finn155> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdate_fins_fees_posting(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "fees_posting"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Finn155 finnobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[fins].[fins_fees_posting_accrual_monthly_proc]",
                                new List<SqlParameter>() 
                             {                             
                                 new SqlParameter("@opr", finnobj.opr),
                                 new SqlParameter("@sims_cur_code", finnobj.fins_fee_cur_code),
                                 new SqlParameter("@sims_academic_year", finnobj.fins_fee_academic_year),
                                 new SqlParameter("@sims_grade_code", finnobj.fins_fee_grade_code),
                                 new SqlParameter("@sims_section_code", ""),
                                 new SqlParameter("@fins_fee_number", finnobj.fins_fee_number),
                                 new SqlParameter("@fins_revenue_acno", finnobj.fins_revenue_acno),
                                 new SqlParameter("@fins_advance_received_acno_jan", finnobj.fins_advance_received_acno_jan),
                                 new SqlParameter("@fins_advance_received_acno_feb", finnobj.fins_advance_received_acno_feb),
                                 new SqlParameter("@fins_advance_received_acno_mar", finnobj.fins_advance_received_acno_mar),
                                 new SqlParameter("@fins_advance_received_acno_apr", finnobj.fins_advance_received_acno_apr),
                                 new SqlParameter("@fins_advance_received_acno_may", finnobj.fins_advance_received_acno_may),
                                 new SqlParameter("@fins_advance_received_acno_jun", finnobj.fins_advance_received_acno_jun),
                                 new SqlParameter("@fins_advance_received_acno_jul", finnobj.fins_advance_received_acno_jul),
                                 new SqlParameter("@fins_advance_received_acno_aug", finnobj.fins_advance_received_acno_aug),
                                 new SqlParameter("@fins_advance_received_acno_sep", finnobj.fins_advance_received_acno_sep),
                                 new SqlParameter("@fins_advance_received_acno_oct", finnobj.fins_advance_received_acno_oct),
                                 new SqlParameter("@fins_advance_received_acno_nov", finnobj.fins_advance_received_acno_nov),
                                 new SqlParameter("@fins_advance_received_acno_dec", finnobj.fins_advance_received_acno_dec),
                                 new SqlParameter("@fins_receivable_acno", finnobj.fins_receivable_acno),
                                 new SqlParameter("@fins_fees_posting_status",finnobj.fins_fees_posting_status == true?"A":"I"),
                                 new SqlParameter("@fins_discount_acno", finnobj.fins_discount_acno),
                                 new SqlParameter("@fins_advance_academic_year_acno", finnobj.fins_advance_academic_year_acno),
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}