﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/CreateEditDepartment")]
    public class CreateEditDepartmentController : ApiController
    {
        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {
            List<Fin010> departtype = new List<Fin010>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_departments_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Q")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin010 obj = new Fin010();

                            obj.codp_comp_code = dr["codp_comp_code"].ToString();
                            obj.codp_comp_name = dr["codp_comp_name"].ToString();
                            departtype.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, departtype);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, departtype);
        }

        [Route("getDepartType")]
        public HttpResponseMessage getDepartType()
        {
            List<Fin010> departtype = new List<Fin010>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_departments_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin010 obj = new Fin010();
                            obj.fins_appl_parameter = dr["fins_appl_parameter"].ToString();
                            obj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();
                            departtype.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, departtype);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, departtype);
        }

        [Route("getDepartmentDetail")]
        public HttpResponseMessage getDepartmentDetail(string comp_code,string finance_year)
        {
            List<Fin010> DepartDetails = new List<Fin010>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_departments_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@codp_comp_code",comp_code),
                            new SqlParameter("@codp_year",finance_year),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin010 obj = new Fin010();
                            obj.codp_comp_code = dr["codp_comp_code"].ToString();
                            obj.codp_dept_no = dr["codp_dept_no"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();
                            obj.codp_short_name = dr["codp_short_name"].ToString();
                            obj.codp_dept_type = dr["codp_dept_type"].ToString();
                            obj.codp_dep_desc = dr["codp_dep_desc"].ToString();
                            obj.codp_year = dr["codp_year"].ToString();

                            if (dr["codp_status"].ToString() == "A")
                                obj.codp_status = true;
                            else
                                obj.codp_status = false;

                            DepartDetails.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DepartDetails);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, DepartDetails);
        }

        [Route("DepartmentDetailCUD")]
        public HttpResponseMessage DepartmentDetailCUD(List<Fin010> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin010 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_departments_proc]",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@codp_comp_code", simsobj.codp_comp_code),
                                 new SqlParameter("@codp_dept_no", simsobj.codp_dept_no),
                                 new SqlParameter("@codp_year", simsobj.codp_year),
                                 new SqlParameter("@codp_dept_name", simsobj.codp_dept_name),
                                 new SqlParameter("@codp_short_name", simsobj.codp_short_name),
                                 new SqlParameter("@codp_dept_type", simsobj.codp_dept_type),
                                 new SqlParameter("@codp_status", simsobj.codp_status==true?"A":"I")


                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}