﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/BankMaster")]
    [BasicAuthentication]
    public class BankMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        [Route("getAllBankMaster")]
        public HttpResponseMessage getAllFinancialYear(string comp_code, string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin138> goaltarget_list = new List<Fin138>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@pb_comp_code",comp_code),
                            new SqlParameter("@Year", finance_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Fin138 simsobj = new Fin138();
                            simsobj.pb_bank_code = dr["pb_bank_code"].ToString();
                            simsobj.pb_bank_name = dr["pb_bank_name"].ToString();
                            simsobj.pb_comp_code = dr["pb_comp_code"].ToString();
                            simsobj.pb_gl_acno = dr["pb_gl_acno"].ToString();
                            simsobj.pb_gl_name = dr["pb_gl_name"].ToString();
                            simsobj.pb_sl_code = dr["pb_sl_code"].ToString();
                            simsobj.pb_sl_name = dr["pb_sl_name"].ToString();
                            simsobj.pb_sl_acno = dr["pb_sl_acno"].ToString();
                            simsobj.pb_sl_ac_name = dr["pb_sl_ac_name"].ToString();
                            simsobj.pb_srl_no = dr["pb_srl_no"].ToString();
                            simsobj.comp_name = dr["comp_name"].ToString();
                            simsobj.pb_posting_flag = dr["pb_posting_flag"].Equals("Y") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDBankMaster")]
        public HttpResponseMessage CUDBankMaster(List<Fin138> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin138 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                new SqlParameter("@pb_bank_code", simsobj.pb_bank_code),
                                new SqlParameter("@pb_bank_name", simsobj.pb_bank_name),
                                new SqlParameter("@pb_gl_acno", simsobj.acno),
                                new SqlParameter("@pb_sl_code", simsobj.sllc_ldgr_code),
                                new SqlParameter("@pb_sl_acno", simsobj.slma_acno),
                                new SqlParameter("@pb_srl_no", simsobj.pb_srl_no),
                                new SqlParameter("@pb_comp_code", simsobj.comp_code),
                                new SqlParameter("@pb_posting_flag",simsobj.pb_posting_flag.Equals(true)?"Y":"N")

                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



        //Petty Cash Document
        [Route("CUDPettyCashDocument")]
        public HttpResponseMessage CUDPettyCashDocument(List<Fin226> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin226 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_petty_cash_documents]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@glpdc_comp_code", simsobj.glpdc_comp_code),
                                new SqlParameter("@glpdc_year", simsobj.glpdc_year),
                                new SqlParameter("@glpdc_doc_code", simsobj.glpdc_doc_code),
                                new SqlParameter("@glpdc_type", simsobj.glpdc_type),
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                */
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


        //Seelct
        [Route("getAllPettyCashDocs")]
        public HttpResponseMessage getAllPettyCashDocs()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin226> goaltarget_list = new List<Fin226>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_documents]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin226 simsobj = new Fin226();

                            simsobj.glpdc_comp_code = dr["glpdc_comp_code"].ToString();
                            simsobj.glpdc_year = dr["glpdc_year"].ToString();
                            simsobj.glpdc_doc_code = dr["glpdc_doc_code"].ToString();
                            simsobj.glpdc_type = dr["glpdc_type"].ToString();
                            simsobj.glpdc_type_desc = dr["glpdc_type_desc"].ToString();


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //Petty Cash Masters Api....
        [Route("getAllDepartments")]
        public HttpResponseMessage getAllDepartments(string tbl_cond)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDepartments(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin227> goaltarget_list = new List<Fin227>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@tbl_cond", tbl_cond),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 simsobj = new Fin227();

                            simsobj.codp_dept_no = dr["codp_dept_no"].ToString();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDPettyCashMasters")]
        public HttpResponseMessage CUDPettyCashMasters(List<Fin227> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin227 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@glpc_comp_code", simsobj.glpc_comp_code),
                                 new SqlParameter("@glpc_year", simsobj.glpc_year),
                                new SqlParameter("@glpc_dept_no", simsobj.glpc_dept_no),
                                new SqlParameter("@glpc_acct_code", simsobj.glpc_acct_code),
                                new SqlParameter("@glpc_max_limit", simsobj.glpc_max_limit),

                                 new SqlParameter("@glpc_min_limit", simsobj.glpc_min_limit),
                                new SqlParameter("@glpc_max_limit_per_transaction", simsobj.glpc_max_limit_per_transaction),
                                new SqlParameter("@glpc_curr_balance", simsobj.glpc_curr_balance),
                                new SqlParameter("@glpc_creation_date",db.DBYYYYMMDDformat(simsobj.glpc_creation_date)),
                                 new SqlParameter("@glpc_creation_user", simsobj.glpc_creation_user),
                                
                                 new SqlParameter("@glpc_authorize_date", db.DBYYYYMMDDformat(simsobj.glpc_authorize_date)),
                                new SqlParameter("@glpc_cur_date", db.DBYYYYMMDDformat(simsobj.glpc_cur_date)),

                                new SqlParameter("@glpc_cost_center_flag", simsobj.glpc_cost_center_flag==true?'A':'I'),
                                new SqlParameter("@glpc_status", simsobj.glpc_status==true?'P':'A'),
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getPettyCashDeatailsData")]
        public HttpResponseMessage getPettyCashDeatailsData()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDepartments(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin227> goaltarget_list = new List<Fin227>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 simsobj = new Fin227();
                            simsobj.glpc_number = dr["glpc_number"].ToString();
                            simsobj.glpc_comp_code = dr["glpc_comp_code"].ToString();
                            simsobj.glpc_year = dr["glpc_year"].ToString();

                            simsobj.glpc_dept_no = dr["glpc_dept_no"].ToString();
                            simsobj.glpc_acct_code = dr["glpc_acct_code"].ToString();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.glpc_max_limit = dr["glpc_max_limit"].ToString();
                            simsobj.glpc_min_limit = dr["glpc_min_limit"].ToString();

                            simsobj.glpc_max_limit_per_transaction = dr["glpc_max_limit_per_transaction"].ToString();
                            simsobj.glpc_curr_balance = dr["glpc_curr_balance"].ToString();

                            simsobj.glpc_creation_date =db.UIDDMMYYYYformat(dr["glpc_creation_date"].ToString());
                            simsobj.glpc_creation_user = dr["glpc_creation_user"].ToString();

                            simsobj.glpc_authorize_date = db.UIDDMMYYYYformat(dr["glpc_authorize_date"].ToString());
                            simsobj.glpc_authorize_user = dr["glpc_authorize_user"].ToString();

                            simsobj.glpc_cur_date = db.UIDDMMYYYYformat(dr["glpc_cur_date"].ToString());
                            simsobj.glpc_cost_center_flag = dr["glpc_cost_center_flag"].Equals("A") ? true : false;

                            simsobj.glpc_status = dr["glpc_status"].Equals("P") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getPettyCashApproveDeatailsData")]
        public HttpResponseMessage getPettyCashApproveDeatailsData(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDepartments(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Fin227> goaltarget_list = new List<Fin227>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),
                             new SqlParameter("@login_user", login),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 simsobj = new Fin227();
                            simsobj.glpc_number = dr["glpc_number"].ToString();
                            simsobj.glpc_comp_code = dr["glpc_comp_code"].ToString();
                            simsobj.glpc_year = dr["glpc_year"].ToString();

                            simsobj.glpc_dept_no = dr["glpc_dept_no"].ToString();
                            simsobj.glpc_acct_code = dr["glpc_acct_code"].ToString();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.glpc_max_limit = dr["glpc_max_limit"].ToString();
                            simsobj.glpc_min_limit = dr["glpc_min_limit"].ToString();

                            simsobj.glpc_max_limit_per_transaction = dr["glpc_max_limit_per_transaction"].ToString();
                            simsobj.glpc_curr_balance = dr["glpc_curr_balance"].ToString();

                            simsobj.glpc_creation_date = db.UIDDMMYYYYformat(dr["glpc_creation_date"].ToString());
                            simsobj.glpc_creation_user = dr["glpc_creation_user"].ToString();

                            simsobj.glpc_authorize_date = db.UIDDMMYYYYformat(dr["glpc_authorize_date"].ToString());
                            simsobj.glpc_authorize_user = dr["glpc_authorize_user"].ToString();

                            simsobj.glpc_cur_date = db.UIDDMMYYYYformat(dr["glpc_cur_date"].ToString());
                            simsobj.glpc_cost_center_flag = dr["glpc_cost_center_flag"].Equals("A") ? true : false;

                            simsobj.glpc_status = dr["glpc_status"].Equals("P") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getapiNewButtonData")]
        public HttpResponseMessage getapiNewButtonData(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getapiNewButtonData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getapiNewButtonData"));

            List<Fin227> goaltarget_list = new List<Fin227>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'F'),
                             new SqlParameter("@login_user", login),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 simsobj = new Fin227();
                            simsobj.glpca_prepare_user = dr["glpca_prepare_user"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAllAccNumber")]
        public HttpResponseMessage getAllAccNumber(string code, string year, string dept)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAccNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin227> doc_list = new List<Fin227>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@glpc_comp_code",code),
                            new SqlParameter("@glpc_year",year),
                            new SqlParameter("@glpc_dept_no",dept)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin227 simsobj = new Fin227();
                            simsobj.glma_acct_full_name = dr["glma_acct_full_name"].ToString();
                            simsobj.glma_acct_code = dr["glma_acct_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("UpdateAproveMasters")]
        public HttpResponseMessage UpdateAproveMasters(List<Fin227> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin227 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_petty_cash_masters]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@glpc_comp_code", simsobj.glpc_comp_code),
                                new SqlParameter("@glpc_year", simsobj.glpc_year),
                                new SqlParameter("@glpc_dept_no", simsobj.glpc_dept_no),
                                new SqlParameter("@glpc_acct_code", simsobj.glpc_acct_code),
                                new SqlParameter("@glpc_cost_center_flag", simsobj.glpc_cost_center_flag),
                               
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message); 

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        #region API for Fee Collection Report


            [Route("getAcademicYearsnew")]
        public HttpResponseMessage getAcademicYearsnew(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
          
            [Route("getfeecollectionData")]
            public HttpResponseMessage getfeecollectionData(string curCode, string acad_year, string from_date, string to_date)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {

                if (curCode == "undefined"){ curCode = null; }
                if (acad_year == "undefined") {  acad_year = null; }
                if (from_date == "undefined") { from_date = null; }
                if (from_date == "undefined")  { from_date = null; }
                if (to_date == "undefined") { to_date = null; }


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",curCode),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@from_date",from_date),
                            new SqlParameter("@to_date",to_date)
                         });



                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.doc_no = dr["doc_no"].ToString();
                          //  sequence.dd_fee_number = dr["dd_fee_number"].ToString();
                          //  sequence.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                           // sequence.fee_paid = dr["fee_paid"].ToString();
                            sequence.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            sequence.enroll_number = dr["enroll_number"].ToString();
                            sequence.doc_date = dr["doc_date"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.doc_reference_no = dr["doc_reference_no"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.fee_status_desc = dr["fee_status_desc"].ToString();
                            sequence.bank_name = dr["bank_name"].ToString();
                            sequence.cheque_number = dr["cheque_number"].ToString();
                            sequence.dd_fee_cheque_date = dr["dd_fee_cheque_date"].ToString();


                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




            [Route("getfeecollectionDataNew")]
            public HttpResponseMessage getfeecollectionDataNew(string curCode, string acad_year, string from_date, string to_date)
            {
                if (curCode == "undefined") { curCode = null; }
                if (acad_year == "undefined") { acad_year = null; }
                if (from_date == "undefined") { from_date = null; }
                if (from_date == "undefined") { from_date = null; }
                if (to_date == "undefined") { to_date = null; }


                DataSet ds = new DataSet();
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        ds = db.ExecuteStoreProcedureDS("[sims].[sims_fee_collection_rpt_proc]",
                           new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",curCode),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@from_date",from_date),
                            new SqlParameter("@to_date",to_date)
                         });
                         

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
                }
                catch (Exception x)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "");
                }

            }

       #endregion


       

    }

}










