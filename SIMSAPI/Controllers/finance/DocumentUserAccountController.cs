﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
namespace SIMSAPI.Controllers.Finance
{
    [RoutePrefix("api/finance")]
    public class FinanceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Document User Account
        [Route("getDocUserAccount")]
        public HttpResponseMessage getDocUserAccount(string comp_code,string finance_year1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocUserAccount(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getDocUserAccount"));
            List<Fin206> list = new List<Fin206>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@gdua_comp_code", comp_code),
                              new SqlParameter("@gdua_year_code", finance_year1)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_comp_code = dr["gdua_comp_code"].ToString();
                            obj.gdua_year = dr["gdua_year"].ToString();
                            obj.gdua_dept_no = dr["gdua_dept_no"].ToString();
                            obj.gdua_doc_code = dr["gdua_doc_code"].ToString();
                            obj.gdua_acct_code = dr["gdua_acct_code"].ToString();
                            obj.gdua_doc_user_code = dr["gdua_doc_user_code"].ToString();
                            obj.comn_user_name = dr["comn_user_name"].ToString();
                            obj.gdua_status = dr["gdua_status"].ToString().Equals("A") ? true : false;
                            obj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            obj.comp_name = dr["comp_name"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();
                            obj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            obj.user_name = dr["user_name"].ToString();
                            obj.gdua_year_code = dr["gdua_year_code"].ToString();
                            
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getComCode")]
        public HttpResponseMessage getComCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getComCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getComCode"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "C")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.comp_name = dr["comp_name"].ToString();
                            obj.gdua_comp_code = dr["comp_code"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getYear")]
        public HttpResponseMessage getYear(string comp)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getYear"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "F"),
                              new SqlParameter("@gdua_comp_code", comp)

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_year = dr["fins_appl_form_field_value1"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getDepCode")]
        public HttpResponseMessage getDepCode(string comp, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDepCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getDepCode"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@gdua_comp_code", comp),
                              new SqlParameter("@gdua_year", year)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_dept_no = dr["codp_dept_no"].ToString();
                            obj.gdua_dept_name = dr["codp_dept_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getDocCode")]
        public HttpResponseMessage getDocCode(string comp, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getDocCode"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "H"),
                              new SqlParameter("@gdua_comp_code", comp),
                              new SqlParameter("@gdua_year", year)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_doc_code = dr["gldc_doc_code"].ToString();
                            obj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAcctCode")]
        public HttpResponseMessage getAcctCode(string comp, string year, string deptno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcctCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAcctCode"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "J"),
                              new SqlParameter("@gdua_comp_code", comp),
                              new SqlParameter("@gdua_year", year),
                              new SqlParameter("@gdua_dept_no", deptno)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_acct_code = dr["glma_acct_code"].ToString();
                            obj.glma_acct_name = dr["glma_acct_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getDocUserCode")]
        public HttpResponseMessage getDocUserCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDocUserCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getDocUserCode"));

            List<Fin206> list = new List<Fin206>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "P")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin206 obj = new Fin206();
                            obj.gdua_doc_user_code = dr["user_code"].ToString();
                            obj.comn_user_name = dr["comn_user_name"].ToString();
                            obj.user_name = dr["user_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("CUDDocUserAccountDetails")]
        public HttpResponseMessage CUDDocUserAccountDetails(Fin206 obj)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@gdua_comp_code", obj.gdua_comp_code),
                                new SqlParameter("@gdua_dept_no", obj.gdua_dept_no),
                                new SqlParameter("@gdua_doc_code", obj.gdua_doc_code),
                                new SqlParameter("@gdua_acct_code", obj.gdua_acct_code),
                                new SqlParameter("@gdua_doc_user_code", obj.gdua_doc_user_code),
                                //if (!string.IsNullOrEmpty(Finnobj.gdua_doc_user_code))
                                //    cmd.Parameters.AddWithValue("@gdua_doc_user_code", Finnobj.gdua_doc_user_code);
                                //else
                                //    cmd.Parameters.AddWithValue("@gdua_doc_user_code", "NULL");

                                new SqlParameter("@gdua_year", obj.gdua_year),
                                new SqlParameter("@gdua_year_code", obj.gdua_year_code),
                                
                                new SqlParameter("@gdua_status", obj.gdua_status == true?"A":"I"),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();
                        

                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("DeUserAccountDetails")]
        public HttpResponseMessage DeUserAccountDetails(List<Fin206> data)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (var obj in data)
                        {
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("fins_doc_user_account_proc",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@gdua_comp_code", obj.gdua_comp_code),
                                new SqlParameter("@gdua_dept_no", obj.gdua_dept_no),
                                new SqlParameter("@gdua_doc_code", obj.gdua_doc_code),
                                new SqlParameter("@gdua_acct_code", obj.gdua_acct_code),
                                new SqlParameter("@gdua_doc_user_code", obj.gdua_doc_user_code),
                                //if (!string.IsNullOrEmpty(Finnobj.gdua_doc_user_code))
                                //    cmd.Parameters.AddWithValue("@gdua_doc_user_code", Finnobj.gdua_doc_user_code);
                                //else
                                //    cmd.Parameters.AddWithValue("@gdua_doc_user_code", "NULL");

                                new SqlParameter("@gdua_year", obj.gdua_year),
                                new SqlParameter("@gdua_year_code", obj.gdua_year_code),

                                new SqlParameter("@gdua_status", obj.gdua_status == true?"A":"I"),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion

        #region currency master
        [Route("getCurrencyMaster")]
        public HttpResponseMessage getCurrencyMaster()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrencyMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getCurrencyMaster"));

            List<Fin043> list = new List<Fin043>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_currency_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin043 obj = new Fin043();
                            obj.comp_name = dr["comp_name"].ToString();
                            obj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            obj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();
                            if (!string.IsNullOrEmpty(dr["excg_min_rate"].ToString()))
                                obj.excg_min_rate = Decimal.Parse(dr["excg_min_rate"].ToString());
                            if (!string.IsNullOrEmpty(dr["excg_max_rate"].ToString()))
                                obj.excg_max_rate = Decimal.Parse(dr["excg_max_rate"].ToString());
                            obj.excg_deci_desc = dr["excg_deci_desc"].ToString();
                            if (!string.IsNullOrEmpty(dr["excg_curcy_decimal"].ToString()))
                                obj.excg_curcy_decimal = Int32.Parse(dr["excg_curcy_decimal"].ToString());
                            obj.excg_enabled_flag = dr["excg_enabled_flag"].ToString().Equals("A") ? true : false;
                            if (!string.IsNullOrEmpty(dr["excg_reval_rate"].ToString()))
                                obj.excg_reval_rate = Decimal.Parse(dr["excg_reval_rate"].ToString());
                            if (!string.IsNullOrEmpty(dr["excg_effective_date"].ToString()))
                             //   obj.excg_effective_date = DateTime.Parse(dr["excg_effective_date"].ToString()).ToShortDateString();
                                obj.excg_effective_date = db.UIDDMMYYYYformat(dr["excg_effective_date"].ToString());
                                obj.excg_comp_code = dr["excg_comp_code"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("CUDCurrencyMaster")]
        public HttpResponseMessage CUDCurrencyMaster(List<Fin043> data)
        {
            string res = "";
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Fin043 obj in data)
                        {
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("fins_currency_master_proc",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@excg_comp_name", obj.excg_comp_code),
                                new SqlParameter("@excg_curcy_code", obj.excg_curcy_code),
                                new SqlParameter("@excg_curcy_desc", obj.excg_curcy_desc),
                                new SqlParameter("@excg_min_rate",obj.excg_min_rate),
                                new SqlParameter("@excg_max_rate", obj.excg_max_rate),
                                new SqlParameter("@excg_deci_desc", obj.excg_deci_desc),
                                new SqlParameter("@excg_curcy_decimal", obj.excg_curcy_decimal),
                                new SqlParameter("@excg_enabled_flag", obj.excg_enabled_flag == true?"A":"I"),    
                                new SqlParameter("@excg_reval_rate", obj.excg_reval_rate),
                                new SqlParameter("@excg_effective_date",db.DBYYYYMMDDformat(obj.excg_effective_date)),
                                
                           });
                            if (dr.Read())
                            {
                                res = dr[0].ToString();
                            }
                            dr.Close();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion


        #region Company Master

        [Route("getAllCountryName")]
        public HttpResponseMessage getAllCountryName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCountryName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAllCountryName"));

            List<Fin003> list = new List<Fin003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "C")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin003 obj = new Fin003();
                            obj.comp_cntry_name = dr["sims_country_name_en"].ToString();
                            obj.comp_cntry_code = dr["sims_country_code"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }
        [Route("getCurrencydesc")]
        public HttpResponseMessage getCurrencydesc(string contrycode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrencydesc(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getCurrencydesc"));

            List<Fin003> list = new List<Fin003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@country_code",contrycode),

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin003 obj = new Fin003();
                            obj.comp_curcy_name = dr["excg_curcy_desc"].ToString();
                            obj.comp_curcy_code = dr["excg_curcy_code"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAllCurrencyName")]
        public HttpResponseMessage getAllCurrencyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCurrencyName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAllCurrencyName"));

            List<Fin003> list = new List<Fin003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "H")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin003 obj = new Fin003();
                            obj.comp_curcy_name = dr["excg_curcy_desc"].ToString();
                            obj.comp_curcy_code = dr["excg_curcy_code"].ToString();
                            obj.comp_curcy_decimal_desc = dr["excg_deci_desc"].ToString();
                            obj.comp_curcy_dec = int.Parse(dr["excg_curcy_decimal"].ToString());
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAllCompanyMaster")]
        public HttpResponseMessage getAllCompanyMaster()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCompanyMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAllCompanyMaster"));

            List<Fin003> list = new List<Fin003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin003 obj = new Fin003();

                            obj.comp_cntry_code = dr["comp_cntry_code"].ToString();
                            obj.comp_curcy_code = dr["comp_curcy_code"].ToString();
                            obj.comp_code = dr["comp_code"].ToString();
                            obj.comp_name = dr["comp_name"].ToString();
                            obj.comp_short_name = dr["comp_short_name"].ToString();
                            obj.comp_cntry_name = dr["comp_cntry_name"].ToString();
                            obj.comp_curcy_name = dr["comp_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["comp_curcy_dec"].ToString()))
                                obj.comp_curcy_dec = int.Parse(dr["comp_curcy_dec"].ToString());
                            obj.comp_max_dept = dr["comp_max_dept_no"].ToString();
                            obj.comp_min_dept = dr["comp_min_dept_no"].ToString();
                            obj.comp_enable_chk_dgt = dr["comp_enable_chk_dgt"].ToString().Equals("Y") ? true : false;
                            obj.comp_allow_multi_curcy = dr["comp_allow_multi_cur"].ToString().Equals("Y") ? true : false;
                            obj.comp_allow_job_ledger = dr["comp_allow_job_ledger"].ToString().Equals("Y") ? true : false;
                            obj.comp_final_pl_acct = dr["comp_final_pl_acct"].ToString();
                            obj.comp_exchg_gain_loss = dr["comp_exchg_gain_loss"].ToString();
                            obj.comp_inter_comp_acct = dr["comp_inter_comp_acct"].ToString();
                            obj.comp_suspect_acct = dr["comp_susp_acct"].ToString();
                            obj.comp_final_pl_acct_name = dr["comp_final_pl_acct_name"].ToString();
                            obj.comp_exchg_gain_loss_name = dr["comp_exchg_gain_loss_name"].ToString();
                            obj.comp_inter_comp_acct_name = dr["comp_inter_comp_acct_name"].ToString();
                            obj.comp_suspect_acct_name = dr["comp_susp_acct_name"].ToString();
                            obj.comp_allow_inter_comp_jv = dr["comp_allow_inter_comp_jv"].ToString().Equals("Y") ? true : false;
                            obj.comp_setup_status = dr["comp_setup_status"].ToString().Equals("A") ? true : false;
                            obj.comp_addr_line1 = dr["comp_addr_line1"].ToString();
                            obj.comp_addr_line2 = dr["comp_addr_line2"].ToString();
                            obj.comp_addr_line3 = dr["comp_addr_line3"].ToString();
                            obj.comp_tel_no = dr["comp_tel_no"].ToString();
                            obj.comp_fax_no = dr["comp_fax_no"].ToString();
                            obj.comp_email = dr["comp_e_mail"].ToString();
                            obj.comp_web_site = dr["comp_web_site"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }



        [Route("getAllCompanyMaster_new")]
        public HttpResponseMessage getAllCompanyMaster_new()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCompanyMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAllCompanyMaster"));

            List<Fin003> list = new List<Fin003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin003 obj = new Fin003();

                            obj.comp_cntry_code = dr["comp_cntry_code"].ToString();
                            obj.comp_curcy_code = dr["comp_curcy_code"].ToString();
                            obj.comp_code = dr["comp_code"].ToString();
                            obj.comp_name = dr["comp_name"].ToString();
                            obj.comp_short_name = dr["comp_short_name"].ToString();
                            obj.comp_cntry_name = dr["comp_cntry_name"].ToString();
                            obj.comp_curcy_name = dr["comp_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["comp_curcy_dec"].ToString()))
                                obj.comp_curcy_dec = int.Parse(dr["comp_curcy_dec"].ToString());
                            obj.comp_max_dept = dr["comp_max_dept_no"].ToString();
                            obj.comp_min_dept = dr["comp_min_dept_no"].ToString();
                            obj.comp_enable_chk_dgt = dr["comp_enable_chk_dgt"].ToString().Equals("Y") ? true : false;
                            obj.comp_allow_multi_curcy = dr["comp_allow_multi_cur"].ToString().Equals("Y") ? true : false;
                            obj.comp_allow_job_ledger = dr["comp_allow_job_ledger"].ToString().Equals("Y") ? true : false;
                            obj.comp_final_pl_acct = dr["comp_final_pl_acct"].ToString();
                            obj.comp_exchg_gain_loss = dr["comp_exchg_gain_loss"].ToString();
                            obj.comp_inter_comp_acct = dr["comp_inter_comp_acct"].ToString();
                            obj.comp_suspect_acct = dr["comp_susp_acct"].ToString();
                            obj.comp_final_pl_acct_name = dr["comp_final_pl_acct_name"].ToString();
                            obj.comp_exchg_gain_loss_name = dr["comp_exchg_gain_loss_name"].ToString();
                            obj.comp_inter_comp_acct_name = dr["comp_inter_comp_acct_name"].ToString();
                            obj.comp_suspect_acct_name = dr["comp_susp_acct_name"].ToString();
                            obj.comp_allow_inter_comp_jv = dr["comp_allow_inter_comp_jv"].ToString().Equals("Y") ? true : false;
                            obj.comp_setup_status = dr["comp_setup_status"].ToString().Equals("A") ? true : false;
                            obj.comp_addr_line1 = dr["comp_addr_line1"].ToString();
                            obj.comp_addr_line2 = dr["comp_addr_line2"].ToString();
                            obj.comp_addr_line3 = dr["comp_addr_line3"].ToString();
                            obj.comp_tel_no = dr["comp_tel_no"].ToString();
                            obj.comp_fax_no = dr["comp_fax_no"].ToString();
                            obj.comp_email = dr["comp_e_mail"].ToString();
                            obj.comp_web_site = dr["comp_web_site"].ToString();

                            obj.comp_incorporation_number= dr["comp_incorporation_number"].ToString();
	                        obj.comp_incorporation_reg_date= dr["comp_incorporation_reg_date"].ToString();
	                        obj.comp_incorporation_renewal_date= dr["comp_incorporation_renewal_date"].ToString();
	                        obj.comp_incorporation_renewal_remind_days= dr["comp_incorporation_renewal_remind_days"].ToString(); 
	                        obj.comp_pf_number= dr["comp_pf_number"].ToString();
	                        obj.comp_pf_reg_date= dr["comp_pf_reg_date"].ToString();
	                        obj.comp_pf_posting_account_code= dr["comp_pf_posting_account_code"].ToString();
	                        obj.comp_sales_tax_number= dr["comp_sales_tax_number"].ToString();
	                        obj.comp_sales_tax_reg_date= dr["comp_sales_tax_reg_date"].ToString();
	                        obj.comp_sales_tax_posting_account_code= dr["comp_sales_tax_posting_account_code"].ToString();
	                        obj.comp_vat_number= dr["comp_vat_number"].ToString();
	                        obj.comp_vat_reg_date= dr["comp_vat_reg_date"].ToString();
	                        obj.comp_vat_posting_account_code= dr["comp_vat_posting_account_code"].ToString();
                            obj.comp_input_vat_posting_account_code = dr["comp_input_vat_posting_account_code"].ToString();
                            obj.include_vat_enable_status = dr["include_vat_enable_status"].ToString().Equals("Y") ? true : false;
                            obj.comp_vat_roundup_calculation = dr["comp_vat_roundup_calculation"].ToString().Equals("Y") ? true : false; 
                            obj.comp_vat_roundup_posting_account_code = dr["comp_vat_roundup_posting_account_code"].ToString();



                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }


        [Route("CUDCompanyMaster")]
        public HttpResponseMessage CUDCompanyMaster(List<Fin003> data)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Fin003 obj in data)
                        {
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@comp_code", obj.comp_code),
                                new SqlParameter("@comp_name", obj.comp_name),
                                new SqlParameter("@comp_short_name", obj.comp_short_name),
                                new SqlParameter("@comp_cntry_code", obj.comp_cntry_code),
                                new SqlParameter("@comp_curcy_code", obj.comp_curcy_code),
                                new SqlParameter("@comp_curcy_dec", obj.comp_curcy_dec),
                                new SqlParameter("@comp_max_dept_no", obj.comp_max_dept),
                                new SqlParameter("@comp_min_dept_no", obj.comp_min_dept),
                                new SqlParameter("@comp_enable_chk_dgt", obj.comp_enable_chk_dgt == true?"Y":"N"), 
                                new SqlParameter("@comp_allow_multi_cur", obj.comp_allow_multi_curcy == true?"Y":"N"),                                 
                                new SqlParameter("@comp_final_pl_acct", obj.comp_final_pl_acct),
                                new SqlParameter("@comp_exchg_gain_loss", obj.comp_exchg_gain_loss),
                                new SqlParameter("@comp_inter_comp_acct", obj.comp_inter_comp_acct),
                                new SqlParameter("@comp_susp_acct", obj.comp_suspect_acct),
                                new SqlParameter("@comp_setup_status", obj.comp_setup_status == true?"A":"I"),                                   
                                new SqlParameter("@comp_addr_line1", obj.comp_addr_line1),
                                new SqlParameter("@comp_addr_line2", obj.comp_addr_line2),
                                new SqlParameter("@comp_addr_line3", obj.comp_addr_line3),
                                new SqlParameter("@comp_tel_no",obj.comp_tel_no),
                                new SqlParameter("@comp_fax_no",obj.comp_fax_no),
                                new SqlParameter("@comp_e_mail",obj.comp_email),
                                new SqlParameter("@comp_web_site",obj.comp_web_site),

                                                             
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                //res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("CUDCompanyMaster_new")]
        public HttpResponseMessage CUDCompanyMaster_new(List<Fin003> data)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Fin003 obj in data)
                        {
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("fins_company_master_proc",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@comp_code", obj.comp_code),
                                new SqlParameter("@comp_name", obj.comp_name),
                                new SqlParameter("@comp_short_name", obj.comp_short_name),
                                new SqlParameter("@comp_cntry_code", obj.comp_cntry_code),
                                new SqlParameter("@comp_curcy_code", obj.comp_curcy_code),
                                new SqlParameter("@comp_curcy_dec", obj.comp_curcy_dec),
                                new SqlParameter("@comp_max_dept_no", obj.comp_max_dept),
                                new SqlParameter("@comp_min_dept_no", obj.comp_min_dept),
                                new SqlParameter("@comp_enable_chk_dgt", obj.comp_enable_chk_dgt == true?"Y":"N"), 
                                new SqlParameter("@comp_allow_multi_cur", obj.comp_allow_multi_curcy == true?"Y":"N"),                                 
                                new SqlParameter("@comp_final_pl_acct", obj.comp_final_pl_acct),
                                new SqlParameter("@comp_exchg_gain_loss", obj.comp_exchg_gain_loss),
                                new SqlParameter("@comp_inter_comp_acct", obj.comp_inter_comp_acct),
                                new SqlParameter("@comp_susp_acct", obj.comp_suspect_acct),
                                new SqlParameter("@comp_setup_status", obj.comp_setup_status == true?"A":"I"),                                   
                                new SqlParameter("@comp_addr_line1", obj.comp_addr_line1),
                                new SqlParameter("@comp_addr_line2", obj.comp_addr_line2),
                                new SqlParameter("@comp_addr_line3", obj.comp_addr_line3),
                                new SqlParameter("@comp_tel_no",obj.comp_tel_no),
                                new SqlParameter("@comp_fax_no",obj.comp_fax_no),
                                new SqlParameter("@comp_e_mail",obj.comp_email),
                                new SqlParameter("@comp_web_site",obj.comp_web_site),

                                new SqlParameter("@comp_incorporation_number",obj.comp_incorporation_number), 
	                            new SqlParameter("@comp_incorporation_reg_date",db.DBYYYYMMDDformat(obj.comp_incorporation_reg_date)),
	                            new SqlParameter("@comp_incorporation_renewal_date",db.DBYYYYMMDDformat(obj.comp_incorporation_renewal_date)),
	                            new SqlParameter("@comp_incorporation_renewal_remind_days",obj.comp_incorporation_renewal_remind_days),
	                            new SqlParameter("@comp_pf_number",obj.comp_pf_number),
	                            new SqlParameter("@comp_pf_reg_date",db.DBYYYYMMDDformat(obj.comp_pf_reg_date)),
	                            new SqlParameter("@comp_pf_posting_account_code",obj.comp_pf_posting_account_code),
	                            new SqlParameter("@comp_sales_tax_number",obj.comp_sales_tax_number),
	                            new SqlParameter("@comp_sales_tax_reg_date",db.DBYYYYMMDDformat(obj.comp_sales_tax_reg_date)),
	                            new SqlParameter("@comp_sales_tax_posting_account_code",obj.comp_sales_tax_posting_account_code),
	                            new SqlParameter("@comp_vat_number",obj.comp_vat_number),
	                            new SqlParameter("@comp_vat_reg_date",db.DBYYYYMMDDformat(obj.comp_vat_reg_date)),
	                            new SqlParameter("@comp_vat_posting_account_code",obj.comp_vat_posting_account_code),
                                 new SqlParameter("@comp_input_vat_posting_account_code",obj.comp_input_vat_posting_account_code),
                                new SqlParameter("@include_vat_enable_status",obj.include_vat_enable_status.Equals(true)?"Y":"N"),
                                new SqlParameter("@comp_vat_roundup_calculation",obj.comp_vat_roundup_calculation.Equals(true)?"Y":"N"),
                                new SqlParameter("@comp_vat_roundup_posting_account_code",obj.comp_vat_roundup_posting_account_code)



                                                             
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                //res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion
    }
}