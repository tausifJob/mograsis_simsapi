﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/DocumentUser")]
    [BasicAuthentication]
    public class DocumentUserController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //[Route("getAllDocumentUser")]
        //public HttpResponseMessage getAllDocumentUser()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDocumentUser(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "Common", "getAllDocumentUser"));

        //    List<fins054> goaltarget_list = new List<fins054>();
        //    //int total = 0, skip = 0;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_doc_user]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", 'S'),
        //                 });

        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    fins054 simsobj = new fins054();
        //                    simsobj.user_name = dr["user_name"].ToString();
        //                    simsobj.gldu_prepare = dr["gldu_prepare"].Equals("Y") ? true : false;

        //                    simsobj.gldu_verify = dr["gldu_verify"].Equals("Y") ? true : false;
        //                    simsobj.gldu_authorize = dr["gldu_authorize"].Equals("Y") ? true : false;

        //                    simsobj.gldu_verify_user = dr["gldu_verify_user"].ToString();
        //                    simsobj.gldu_authorize_user = dr["gldu_authorize_user"].ToString();


        //                    simsobj.gldu_dept_no = dr["gldu_dept_no"].ToString();
        //                    simsobj.gldu_comp_code = dr["gldu_comp_code"].ToString();

        //                    goaltarget_list.Add(simsobj);

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        //}

        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment(string comp_code, string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            List<fins054> doc_list = new List<fins054>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins.[fins_doc_user_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@gldu_comp_code", comp_code),
                            new SqlParameter("@year", finance_year),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins054 simsobj = new fins054();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.codp_dept_no = dr["codp_dept_no"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //[Route("CUDDocumentUser")]
        //public HttpResponseMessage CUDDocumentUser(List<fins054> data)
        //{
        //    Message message = new Message();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (fins054 simsobj in data)
        //            {
        //                int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_doc_user]",
        //                new List<SqlParameter>()
        //             {

        //                        new SqlParameter("@opr", simsobj.opr),

        //                        new SqlParameter("@gldu_user_name",simsobj.gldu_user_name),
        //                        new SqlParameter("@gldu_prepare",simsobj.gldu_prepare==true?"Y":"N"),
        //                        new SqlParameter("@gldu_verify", simsobj.gldu_verify==true?"Y":"D"),
        //                        new SqlParameter("@gldu_authorize", simsobj.gldu_authorize==true?"Y":"D"),
        //                        new SqlParameter("@gldu_verify_user", simsobj.gldu_verify_user),
        //                        new SqlParameter("@gldu_authorize_user", simsobj.gldu_authorize_user),

        //                        new SqlParameter("@gldu_comp_code", simsobj.gldu_comp_code),
        //                        new SqlParameter("@gldu_dept_no", simsobj.gldu_dept_no),

        //                      //  new SqlParameter("@gdua_status",simsobj.gdua_status==true?"A":"I")
        //                });
        //                if (ins > 0)
        //                {
        //                    if (simsobj.opr.Equals("U"))
        //                        message.strMessage = "Record Updated Sucessfully!!";
        //                    else if (simsobj.opr.Equals("I"))
        //                        message.strMessage = "Record  Added Sucessfully!!";
        //                    else if (simsobj.opr.Equals("D"))
        //                        message.strMessage = "Record  Deleted Sucessfully!!";

        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }


        //                else
        //                {
        //                    // message.strMessage = "";
        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                    // message.messageType = MessageType.Error;
        //                    // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //                }
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, message);
        //        }

        //    }


        //    catch (Exception x)
        //    {

        //        message.strMessage = x.Message;
        //        message.systemMessage = string.Empty;
        //        message.messageType = MessageType.Success;

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, message);
        //}

        [Route("getDocumentUserDetails")]
        public HttpResponseMessage getDocumentUserDetails(string comp_code, string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDocumentUser(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllDocumentUser"));

            List<fins054> group_list = new List<fins054>();
            fins054 finssobj = null;
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins.[fins_doc_user_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                             new SqlParameter("@gldu_comp_code", comp_code),
                            new SqlParameter("@year", finance_year),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (group_list.Count > 0)
                            {
                                if (group_list.ElementAt(group_list.Count - 1).gldu_user_name.Equals(dr["user_name"].ToString()))
                                {
                                    if (dr["gldu_verify_user"].ToString().Equals(dr["gldu_authorize_user"].ToString()))
                                    {
                                        fins054 ColVar1 = new fins054();
                                        ColVar1.gldu_user_name = dr["verify_user"].ToString();
                                        ColVar1.gldu_user_name_data = dr["gldu_verify_user"].ToString();
                                        ColVar1.gldu_verify_user = dr["gldu_verify_user"].ToString();
                                        ColVar1.gldu_authorize_user = dr["gldu_authorize_user"].ToString();
                                        ColVar1.gldu_verify = true;
                                        ColVar1.gldu_authorize = true;
                                        finssobj.ListColl.Add(ColVar1);
                                    }
                                    else
                                    {
                                        #region Assign Verify Authorize User
                                        if (!string.IsNullOrEmpty(dr["gldu_verify_user"].ToString()))
                                        {
                                            fins054 ColVar1 = new fins054();
                                            ColVar1.gldu_user_name = dr["verify_user"].ToString();
                                            ColVar1.gldu_user_name_data = dr["gldu_verify_user"].ToString();
                                            ColVar1.gldu_authorize_user = dr["gldu_authorize_user"].ToString();
                                            ColVar1.gldu_verify_user = dr["gldu_verify_user"].ToString();
                                            ColVar1.gldu_verify = true;
                                            ColVar1.gldu_authorize = false;
                                            finssobj.ListColl.Add(ColVar1);
                                        }
                                        if (!string.IsNullOrEmpty(dr["gldu_authorize_user"].ToString()))
                                        {
                                            fins054 ColVar1 = new fins054();
                                            ColVar1.gldu_user_name = dr["authorize_user"].ToString();
                                            ColVar1.gldu_user_name_data = dr["gldu_authorize_user"].ToString();
                                            ColVar1.gldu_authorize_user = dr["gldu_authorize_user"].ToString();
                                            ColVar1.gldu_verify_user = dr["gldu_verify_user"].ToString();
                                            ColVar1.gldu_verify = false;
                                            ColVar1.gldu_authorize = true;
                                            finssobj.ListColl.Add(ColVar1);
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    finssobj = new fins054();
                                    finssobj.gldu_user_name = dr["user_name"].ToString();
                                    finssobj.gldu_user_name_data = dr["gldu_user_name"].ToString();
                                    finssobj.gldu_prepare = dr["gldu_prepare"].ToString().Equals("Y") ? true : false;
                                    finssobj.gldu_dept_no = dr["gldu_dept_no"].ToString();
                                    finssobj.gldu_dept_no_name = dr["dept_name"].ToString();
                                    fins054 ColVar = new fins054();
                                    ColVar.gldu_user_name = dr["user_name"].ToString();
                                    ColVar.gldu_user_name_data = dr["gldu_user_name"].ToString();
                                    ColVar.gldu_prepare = dr["gldu_prepare"].ToString().Equals("Y") ? true : false;
                                    ColVar.gldu_authorize = dr["gldu_authorize"].ToString().Equals("Y") ? true : false;
                                    ColVar.gldu_authorize_user = dr["gldu_verify_user"].ToString();
                                    ColVar.gldu_verify_user = dr["gldu_authorize_user"].ToString();
                                    ColVar.gldu_verify = dr["gldu_verify"].ToString().Equals("Y") ? true : false;
                                    finssobj.ListColl = new List<fins054>();
                                    finssobj.ListColl.Add(ColVar);
                                    group_list.Add(finssobj);
                                }

                            }
                            else
                            {

                                finssobj = new fins054();
                                finssobj.gldu_user_name = dr["user_name"].ToString();
                                finssobj.gldu_user_name_data = dr["gldu_user_name"].ToString();
                                finssobj.gldu_prepare = dr["gldu_prepare"].ToString().Equals("Y") ? true : false;
                                finssobj.gldu_dept_no = dr["gldu_dept_no"].ToString();
                                finssobj.gldu_dept_no_name = dr["dept_name"].ToString();
                                fins054 ColVar = new fins054();
                                ColVar.gldu_user_name = dr["user_name"].ToString();
                                ColVar.gldu_user_name_data = dr["gldu_user_name"].ToString();
                                ColVar.gldu_prepare = dr["gldu_prepare"].ToString().Equals("Y") ? true : false;
                                ColVar.gldu_authorize = dr["gldu_authorize"].ToString().Equals("Y") ? true : false;
                                ColVar.gldu_authorize_user = dr["gldu_verify_user"].ToString();
                                ColVar.gldu_verify_user = dr["gldu_authorize_user"].ToString();
                                ColVar.gldu_verify = dr["gldu_verify"].ToString().Equals("Y") ? true : false;
                                finssobj.ListColl = new List<fins054>();
                                finssobj.ListColl.Add(ColVar);
                                group_list.Add(finssobj);
                            }


                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("SearchEmployee")]
        public HttpResponseMessage SearchEmployee(string data)
        {

            fins054 obj = new fins054();

            List<fins054> doc_list = new List<fins054>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<fins054>(data);
            }

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Search]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'F'),
                            new SqlParameter("@user_name",obj.comn_user_name),
                            new SqlParameter("@emp_name",obj.EmpName),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins054 simsobj = new fins054();
                            // simsobj.opr_mem_code = "F";
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("InsertDocumentUser")]
        public HttpResponseMessage InsertDocumentUser(List<fins054> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins054 finsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("fins.[fins_doc_user_proc]",
                    new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", "B"),

                                 new SqlParameter("@gldu_user_name", finsobj.gldu_user_name_data),
                                 new SqlParameter("@gldu_verify", finsobj.gldu_verify ? "Y" : "D"),
                                 new SqlParameter("@gldu_authorize", finsobj.gldu_authorize ? "Y" : "D"),
                                 new SqlParameter("@gldu_prepare", finsobj.gldu_prepare ? "Y" : "D"),
                                 new SqlParameter("@gldu_dept_no_name", finsobj.gldu_dept_no),
                                 new SqlParameter("@gldu_comp_code", finsobj.gldu_comp_code),
                                 new SqlParameter("@gldu_authorize_user", finsobj.gldu_authorize_user),
                                 new SqlParameter("@gldu_verify_user", finsobj.gldu_verify_user),



                        });
                        if (ins > 0)
                        {
                            message.strMessage = "Record  Added Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }


                        else
                        {
                            // message.strMessage = "";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UpdateDocumentUser")]
        public HttpResponseMessage UpdateDocumentUser(List<fins054> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins054 finsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins.[fins_doc_user_proc]",
                    new List<SqlParameter>()
                 {
                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@gldu_user_name_data", finsobj.gldu_user_name_data),
                        new SqlParameter("@gldu_dept_no_name", finsobj.gldu_dept_no),
                         });
                    }
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins054 finsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_doc_user]",
                    new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "U"),
                                 new SqlParameter("@gldu_user_name", finsobj.gldu_user_name_data),
                                 new SqlParameter("@gldu_verify", finsobj.gldu_verify ? "Y" : "D"),
                                 new SqlParameter("@gldu_authorize", finsobj.gldu_authorize ? "Y" : "D"),
                                 new SqlParameter("@gldu_prepare", finsobj.gldu_prepare ? "Y" : "D"),
                                 new SqlParameter("@gldu_dept_no_name", finsobj.gldu_dept_no),
                                 new SqlParameter("@gldu_comp_code", finsobj.gldu_comp_code),
                                 new SqlParameter("@gldu_authorize_user", finsobj.gldu_authorize_user),
                                 new SqlParameter("@gldu_verify_user", finsobj.gldu_verify_user),
                        });
                        if (ins > 0)
                        {
                            message.strMessage = "Record  Updated Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {
                            // message.strMessage = "";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
        [Route("delDocumentUser")]
        public HttpResponseMessage delDocumentUser(List<fins054> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (fins054 finsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("fins.[fins_doc_user_proc]",
                    new List<SqlParameter>()
                 {


                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@gldu_user_name_data", finsobj.gldu_user_name_data),
                        new SqlParameter("@gldu_dept_no_name", finsobj.gldu_dept_no),



                         });
                        if (ins > 0)
                        {
                            message.strMessage = "Record  Deleted Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }


                        else
                        {
                            // message.strMessage = "";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);

                }
            }



            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }




    }

}










