﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.finance
{

    [RoutePrefix("api/DepreciationJVPost")]
    public class DepreciationJVPostController:ApiController
    {


        [Route("getPeriodsData")]
        public HttpResponseMessage getPeriods(string fins_year, string comp_code)
        {
            List<Fin17A> mod_list = new List<Fin17A>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'K'),
                            new SqlParameter("@fins_year",fins_year),
                            new SqlParameter("@fins_comp_code",comp_code)


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin17A simsobj = new Fin17A();
                            simsobj.fins_financial_period_no = dr["glfp_prd_no"].ToString();
                            simsobj.fins_financial_period_name = dr["glfp_prd_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        //[Route("getAssetsData")]
        //public HttpResponseMessage getAssetsData()//string year,string month
        //{
        //    List<Fin17A> desg_list = new List<Fin17A>();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[depreciation_jv_post_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", 'S'),
        //                    new SqlParameter("@company_code",'1'),
        //                  //  new SqlParameter("@year",year),
        //                    //new SqlParameter("@month",month)
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Fin017 simsobj = new Fin017();
        //                    //simsobj.gam_item_no = int.Parse(dr["gam_item_no"].ToString());
        //                    simsobj.gam_item_no = dr["gam_item_no"].ToString();
        //                    simsobj.gam_desc_1 = dr["gam_desc_1"].ToString();
        //                    simsobj.gam_desc_2 = dr["gam_desc_2"].ToString();
        //                    simsobj.gam_supl_name = dr["gam_supl_name"].ToString();
        //                    simsobj.gam_acct_code = dr["gam_acct_name"].ToString();
        //                    simsobj.gam_invoice_no = dr["gam_invoice_no"].ToString();
        //                    if (string.IsNullOrEmpty(dr["gam_quantity"].ToString()) == false)
        //                        simsobj.gam_quantity = Decimal.Parse(dr["gam_quantity"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_invoice_amount"].ToString()) == false)
        //                        simsobj.gam_invoice_amount = Decimal.Parse(dr["gam_invoice_amount"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_book_value"].ToString()) == false)
        //                        simsobj.gam_book_value = Decimal.Parse(dr["gam_book_value"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_cum_deprn"].ToString()) == false)
        //                        simsobj.gam_cum_deprn = Decimal.Parse(dr["gam_cum_deprn"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_ytd_deprn"].ToString()) == false)
        //                        simsobj.gam_ytd_deprn = Decimal.Parse(dr["gam_ytd_deprn"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_mth_deprn"].ToString()) == false)
        //                        simsobj.gam_mth_deprn = Decimal.Parse(dr["gam_mth_deprn"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_sale_value"].ToString()) == false)
        //                        simsobj.gam_sale_value = Decimal.Parse(dr["gam_sale_value"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_repl_cost"].ToString()) == false)
        //                        simsobj.gam_repl_cost = Decimal.Parse(dr["gam_repl_cost"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_deprn_percent"].ToString()) == false)
        //                        simsobj.gam_deprn_percent = Decimal.Parse(dr["gam_deprn_percent"].ToString());
        //                    if (string.IsNullOrEmpty(dr["gam_used_on_item"].ToString()) == false)
        //                        simsobj.gam_used_on_item = Decimal.Parse(dr["gam_used_on_item"].ToString());
        //                    simsobj.gam_status = dr["gam_status"].ToString();
        //                    simsobj.gam_status_name = dr["gam_statusValue"].ToString();
        //                    desg_list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, desg_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        //}



        [Route("getDepreciationJVPosting")]
        public HttpResponseMessage getDepreciationJVPosting(string company_code, string year, string fins_financial_period_no,string username)//
        {
            string return_message = "";
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[depreciation_jv_post_proc]",
                       new List<SqlParameter>()
                       {
                              new SqlParameter("@company_code", company_code),
                              new SqlParameter("@year", year),
                              new SqlParameter("@month", fins_financial_period_no),
                              new SqlParameter("@user_name", username),
                              new SqlParameter("@verify_status", "Vry")
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["gltd_doc_code"].ToString()))
                                return_message = dr["gltd_doc_code"].ToString();
                        }
                    }

                }
            }
            catch (Exception x)
            {
                return_message = x.Message;
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, return_message);
        }

        //[Route("getReoprtsJvPost")]
        //public HttpResponseMessage getReoprtsJvPost(List<Fin17A> data)
        //{
        //    bool insert = false;
        //    Message message = new Message();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (Fin17A simsobj in data)
        //            {
        //                int ins = db.ExecuteStoreProcedureforInsert("[sims].[fins_bank_proc]",
        //                new List<SqlParameter>()
        //             {

        //                        new SqlParameter("@opr", simsobj.opr),

        //                        new SqlParameter("@pb_bank_code", simsobj.pb_bank_code),
        //                        new SqlParameter("@pb_bank_name", simsobj.pb_bank_name),
        //                        new SqlParameter("@pb_gl_acno", simsobj.acno),
        //                        new SqlParameter("@pb_sl_code", simsobj.sllc_ldgr_code),
        //                        new SqlParameter("@pb_sl_acno", simsobj.slma_acno),
        //                        new SqlParameter("@pb_srl_no", simsobj.pb_srl_no),
        //                        new SqlParameter("@pb_comp_code", "1"),//simsobj.pb_comp_code),

        //                });
        //                if (ins > 0)
        //                {
        //                    insert = true;
        //                }


        //                else
        //                {
        //                    insert = false;
        //                }
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, insert);
        //        }

        //    }


        //    catch (Exception x)
        //    {

        //        message.strMessage = x.Message;
        //        message.systemMessage = string.Empty;
        //        message.messageType = MessageType.Success;

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, insert);
        //}


        [Route("getCurrentYear")]
        public HttpResponseMessage getCurrentYear()
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'G'),
                           
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                            simsobj.year1 = dr["fins_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getMonths")]
        public HttpResponseMessage getMonths()
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_stud_advanced_fee_posting",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'P'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                            simsobj.month_name = dr["month_name"].ToString();
                            simsobj.month_no = dr["month_no"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getMonthsallData")]
        public HttpResponseMessage getMonthsallData(string months, string comp_code, string finance_year, string user)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_student_invoice_posting]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@month_no",months),
                            new SqlParameter("@sims_cur_code",comp_code),
                            new SqlParameter("@sims_acad_year",finance_year),
                            new SqlParameter("@user_name",user),

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();
                           
                            simsobj.sims_enroll_number= dr["sims_enroll_number"].ToString();
                            simsobj.sims_fee_cur_code= dr["sims_fee_cur_code"].ToString();
                            simsobj.sims_fee_academic_year= dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_grade_code= dr["sims_fee_grade_code"].ToString();
                            simsobj.sims_grade_name_en= dr["sims_grade_name_en"].ToString();
                            simsobj.sims_fee_section_code= dr["sims_fee_section_code"].ToString();
                            simsobj.sims_section_name_en= dr["sims_section_name_en"].ToString();
                            simsobj.sims_fee_code= dr["sims_fee_code"].ToString();
                            simsobj.sims_Fee_code_description= dr["sims_Fee_code_description"].ToString();
                            simsobj.period_no= dr["period_no"].ToString();
                            simsobj.expected_amount= dr["expected_amount"].ToString();
                            simsobj.paid_amount= dr["paid_amount"].ToString();
                            simsobj.posting_amount = dr["posting_amount"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }



        [Route("getstudentreceivableposting")]
        public HttpResponseMessage getstudentreceivableposting(string months, string comp_code, string finance_year, string user)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_student_invoice_posting]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@month_no",months),
                            new SqlParameter("@sims_cur_code",comp_code),
                            new SqlParameter("@sims_acad_year",finance_year),
                            new SqlParameter("@user_name",user),

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();

                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_Fee_code_description = dr["sims_Fee_code_description"].ToString();
                            simsobj.period_no = dr["period_no"].ToString();
                            simsobj.expected_amount = dr["expected_amount"].ToString();
                            simsobj.paid_amount = dr["paid_amount"].ToString();
                            simsobj.posting_amount = dr["posting_amount"].ToString();
                            simsobj.posted_amount = dr["posted_amount"].ToString();

                            mod_list.Add(simsobj);

                        }
                        //return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    //else
                    //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



           // return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getMonthsallUpdateData")]
        public HttpResponseMessage getMonthsallUpdateData(string months, string comp_code, string finance_year, string user,string date)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_student_invoice_posting]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'U'),
                            new SqlParameter("@month_no",months),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@financial_year",finance_year),
                            new SqlParameter("@user_name",user),
                            new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();


                            simsobj.final_doc_number = dr["final_doc_number"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAdvancedMonthsallData")]
        public HttpResponseMessage getAdvancedMonthsallData(string months, string comp_code, string finance_year, string user)
        {
            List<Fin017> mod_list = new List<Fin017>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_stud_advanced_fee_posting]",
                        new List<SqlParameter>() 
                         { 




                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@month",months),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@financial_year",finance_year),
                            new SqlParameter("@gltd_authorize_user",user),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin017 simsobj = new Fin017();

                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_period_code = dr["sims_fee_period_code"].ToString();
                            simsobj.fee_amount = dr["fee_amount"].ToString();
                            simsobj.discounted_amount = dr["discounted_amount"].ToString();
                            simsobj.fee_amount_final = dr["fee_amount_final"].ToString();
                            simsobj.sims_receivable_acno = dr["sims_receivable_acno"].ToString();
                            simsobj.sims_revenue_acno = dr["sims_revenue_acno"].ToString();
                            simsobj.sims_discount_acno = dr["sims_discount_acno"].ToString();
                            simsobj.sims_adjustment_pstng_date = dr["sims_adjustment_pstng_date"].ToString();
                         

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getAsvancedallUpdateData")]
        public HttpResponseMessage getAsvancedallUpdateData(string months, string comp_code, string finance_year, string user, string date)
        {
            List<Fin017> mod_list = new List<Fin017>();

            bool insert = false;

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_stud_advanced_fee_posting]",
                        new List<SqlParameter>() 
                         { 

                            new SqlParameter("@opr",'I'),
                            new SqlParameter("@month",months),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@financial_year",finance_year),
                            new SqlParameter("@gltd_authorize_user",user),
                            new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(date)),

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        insert = true;
                    }
                    else
                    {
                        insert = false;
                    }
                }
            }
            catch (Exception x)
            {
                insert = false;
                return Request.CreateResponse(HttpStatusCode.OK, insert);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);


            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


    }
}