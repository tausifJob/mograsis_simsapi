﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Linq;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.PDCSubmissionController
{
    [RoutePrefix("api/PDCSubmission")]
    [BasicAuthentication]
    public class PDCSubmissionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        [Route("GetComp_Name")]
        public HttpResponseMessage GetComp_Name(string username)
        {
            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@tbl_cond", username),
                new SqlParameter("@opr", "A"),
              
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 fin006Obj = new Finn102();
                            fin006Obj.comp_name = dr["comp_name"].ToString();
                            fin006Obj.comp_code = dr["comp_code"].ToString();
                            fin006Obj.comp_short_name = dr["comp_short_name"].ToString();
                            fin006Obj.comp_input_vat_posting_account_code = dr["comp_input_vat_posting_account_code"].ToString();
                            fin006Obj.include_vat_enable_status = dr["include_vat_enable_status"].ToString();
                            fin006Obj.comp_vat_per = dr["comp_vat_per"].ToString();
                            house.Add(fin006Obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("GetFinancial_year")]
        public HttpResponseMessage GetFinancial_year(string comp_code)
        {


            List<Finn102> house = new List<Finn102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_get_data_sp",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@opr_mem", comp_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn102 simsobj = new Finn102();
                            if (!string.IsNullOrEmpty(dr["sims_financial_year"].ToString()))
                                simsobj.financial_year = decimal.Parse(dr["sims_financial_year"].ToString());
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            house.Add(simsobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("SetCompany")]
        public HttpResponseMessage SetCompany(string comp_code, string year)
        {
            System.Web.HttpContext.Current.Session.Add("CompanyCode", comp_code);
            System.Web.HttpContext.Current.Session.Add("Year", year);
            System.Web.HttpContext.Current.Session.Timeout = 20;
            return Request.CreateResponse(HttpStatusCode.OK, true);

        }

        [Route("getCompanyCode")]
        public HttpResponseMessage getCompanyCode()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, System.Web.HttpContext.Current.Session["CompanyCode"].ToString());

            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);

            }
        }


        [Route("UpdateFinancial_year")]
        public HttpResponseMessage UpdateFinancial_year(string comp_cd, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateFinancial_year(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", comp_cd));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("fins_get_data_sp",
                            new List<SqlParameter>()
                            {
                       
                 new SqlParameter("@opr", "U"),
                 new SqlParameter("@opr_mem", comp_cd),
                 new SqlParameter("@tbl_cond", year)

                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getAllPDCSubmissionALLSubmission_new")]
        public HttpResponseMessage getAllPDCSubmissionALLSubmission_new(string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string pc_discd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCSubmission(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPDCSubmission"));

            List<Fin102> goaltarget_list = new List<Fin102>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'S'));
                    para.Add(new SqlParameter("@opr_upd", "ACN"));
                    para.Add(new SqlParameter("@pc_comp_code", "1"));
                    para.Add(new SqlParameter("@pc_sl_ldgr_code", ldgr_code));
                    para.Add(new SqlParameter("@pc_sl_acno", slma_acno));
                    para.Add(new SqlParameter("@pc_dept_no", dept_no));
                    // para.Add(new SqlParameter("@pc_bank_from", bank_cd));
                    // para.Add(new SqlParameter("@pc_cheque_no", filter_range));
                    para.Add(new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(from_date)));
                    para.Add(new SqlParameter("@pc_submission_date", db.DBYYYYMMDDformat(to_date)));
                    // para.Add(new SqlParameter("@pc_narrative", filter_text));
                    // para.Add(new SqlParameter("@pc_ref_no", sub_ref_no));
                    para.Add(new SqlParameter("@pc_payment_mode", "Ch"));

                    if (pc_discd.Length == 1)
                        para.Add(new SqlParameter("@pc_discd", pc_discd));
                    else
                    {
                        para.Add(new SqlParameter("@pc_discd", pc_discd.Substring(0, 1)));
                        para.Add(new SqlParameter("@pc_calendar", pc_discd.Substring(1, 1)));
                    }


                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_cheques", para);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 finnobj = new Fin102();
                            finnobj.ps_comp_code = dr["pc_comp_code"].ToString();
                            finnobj.ps_discd = dr["Number"].ToString();//varibale used for Row Number
                            finnobj.ps_bank_code = dr["pc_bank_from"].ToString();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.ps_ldgr_code = dr["pc_sl_ldgr_code"].ToString();
                            finnobj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.ps_sl_acno = dr["pc_sl_acno"].ToString();
                            finnobj.ps_sl_acno_name = dr["pc_sl_acno_name"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            finnobj.ps_cheque_no = dr["pc_cheque_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                finnobj.ps_due_date = db.UIDDMMYYYYformat(dr["pc_due_date"].ToString());
                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.ps_amount = Decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.ps_bank_code_name = dr["pc_bank_from_name"].ToString();

                            finnobj.ps_rec_type = dr["pc_doc_type"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_cur_date"].ToString()) == false)
                                finnobj.pc_cur_date = db.UIDDMMYYYYformat(dr["pc_cur_date"].ToString());
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();
                            finnobj.ps_select = false;

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getAllPDCSubmissionALL")]
        public HttpResponseMessage getAllPDCSubmissionALL(string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date, string pc_discd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCSubmission(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPDCSubmission"));

            List<Fin102> goaltarget_list = new List<Fin102>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'S'));
                    para.Add(new SqlParameter("@opr_upd", "ACP"));
                    para.Add(new SqlParameter("@pc_comp_code", "1"));
                    para.Add(new SqlParameter("@pc_sl_ldgr_code", ldgr_code));
                    para.Add(new SqlParameter("@pc_sl_acno", slma_acno));
                    para.Add(new SqlParameter("@pc_dept_no", dept_no));
                    // para.Add(new SqlParameter("@pc_bank_from", bank_cd));
                    // para.Add(new SqlParameter("@pc_cheque_no", filter_range));
                    para.Add(new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(from_date)));
                    para.Add(new SqlParameter("@pc_submission_date", db.DBYYYYMMDDformat(to_date)));
                    // para.Add(new SqlParameter("@pc_narrative", filter_text));
                    // para.Add(new SqlParameter("@pc_ref_no", sub_ref_no));
                    para.Add(new SqlParameter("@pc_payment_mode", "Ch"));


                    if (pc_discd.Length == 1)
                        para.Add(new SqlParameter("@pc_discd", pc_discd));
                    else
                    {
                        para.Add(new SqlParameter("@pc_discd", pc_discd.Substring(0, 1)));
                        para.Add(new SqlParameter("@pc_calendar", pc_discd.Substring(1, 1)));
                    }

                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_cheques", para);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 finnobj = new Fin102();
                            finnobj.ps_comp_code = dr["pc_comp_code"].ToString();
                            finnobj.ps_discd = dr["Number"].ToString();//varibale used for Row Number
                            finnobj.ps_bank_code = dr["pc_bank_from"].ToString();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.ps_ldgr_code = dr["pc_sl_ldgr_code"].ToString();
                            finnobj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.ps_sl_acno = dr["pc_sl_acno"].ToString();
                            finnobj.ps_sl_acno_name = dr["pc_sl_acno_name"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            finnobj.ps_cheque_no = dr["pc_cheque_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                finnobj.ps_due_date = db.UIDDMMYYYYformat(dr["pc_due_date"].ToString());
                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.ps_amount = Decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.ps_bank_code_name = dr["pc_bank_from_name"].ToString();

                            finnobj.ps_rec_type = dr["pc_doc_type"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_cur_date"].ToString()) == false)
                                finnobj.pc_cur_date = db.UIDDMMYYYYformat(dr["pc_cur_date"].ToString());
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();
                            finnobj.ps_select = false;

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("AllPDCSubmission_new_data")]
        public HttpResponseMessage AllPDCSubmission_new_data(Fin102 obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCSubmission(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAllPDCSubmission"));

            List<Fin102> goaltarget_list = new List<Fin102>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]",
                        new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@opr_upd", "ACN"),
                                    new SqlParameter("@pc_comp_code", obj.ps_comp_code),
                                    new SqlParameter("@pc_sl_ldgr_code", obj.ps_ldgr_code),
                                    new SqlParameter("@pc_sl_acno", obj.ps_sl_acno),
                                    new SqlParameter("@pc_dept_no", obj.ps_dept_no),
                                    new SqlParameter("@pc_bank_from", obj.ps_bank_code),
                                    new SqlParameter("@pc_cheque_no", obj.filterRange),
                                    new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(obj.ps_due_dateStr)),
                                    new SqlParameter("@pc_submission_date",db.DBYYYYMMDDformat(obj.subMissionDate)),
                                    new SqlParameter("@pc_narrative", obj.filterText),
                                    new SqlParameter("@pc_gradeSec", obj.GradeSec),
                                    new SqlParameter("@pc_createdBy", obj.createdBY),
                                    new SqlParameter("@pc_ref_no", obj.pc_ref_no),
                                    new SqlParameter("@pc_rec_toDt",db.DBYYYYMMDDformat(obj.recToDate)),
                                    new SqlParameter("@pc_rec_fromDt",db.DBYYYYMMDDformat(obj.recFromDate)),
                                    new SqlParameter("@pc_payment_mode", obj.pc_payment_mode),
                                    new SqlParameter("@pc_discd", obj.ps_discd),
                                    new SqlParameter("@pc_calendar",obj.pc_calendar),
                                    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 finnobj = new Fin102();
                            finnobj.ps_comp_code = dr["pc_comp_code"].ToString();
                            finnobj.ps_discd = dr["Number"].ToString();//varibale used for Row Number
                            finnobj.ps_bank_code = dr["pc_bank_from"].ToString();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.ps_ldgr_code = dr["pc_sl_ldgr_code"].ToString();
                            finnobj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.ps_sl_acno = dr["pc_sl_acno"].ToString();
                            finnobj.ps_sl_acno_name = dr["pc_sl_acno_name"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            finnobj.ps_cheque_no = dr["pc_cheque_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                //finnobj.ps_due_date = DateTime.Parse(dr["pc_due_date"].ToString()).ToShortDateString();
                                finnobj.ps_due_date = db.UIDDMMYYYYformat(dr["pc_due_date"].ToString());

                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                //finnobj.ps_due_date = DateTime.Parse(dr["pc_due_date"].ToString()).ToShortDateString();
                                finnobj.pdc_clearance_date = db.UIDDMMYYYYformat(dr["pc_due_date"].ToString());

                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.ps_amount = Decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.ps_bank_code_name = dr["pc_bank_from_name"].ToString();

                            finnobj.ps_rec_type = dr["pc_doc_type"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_cur_date"].ToString()) == false)
                                finnobj.pc_cur_date = db.UIDDMMYYYYformat(dr["pc_cur_date"].ToString());
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();
                            finnobj.pc_parent_number = dr["sims_parent_number"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_father_full_name"].ToString()) == false)
                                finnobj.pc_father_full_name = dr["sims_father_full_name"].ToString();
                            else if (string.IsNullOrEmpty(dr["sims_mother_full_name"].ToString()) == false)
                                finnobj.pc_father_full_name = dr["sims_mother_full_name"].ToString();
                            else
                                finnobj.pc_father_full_name="";
                            finnobj.ps_select = false;

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        [Route("getAllPDCSubmission")]
        public HttpResponseMessage getAllPDCSubmission(string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCSubmission(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPDCSubmission"));

            List<Fin102> goaltarget_list = new List<Fin102>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]",
                        new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@opr_upd", "ACP"),
                                    new SqlParameter("@pc_comp_code", "1"),
                                    new SqlParameter("@pc_dept_no",dept_no),
                                    new SqlParameter("@pc_sl_ldgr_code",ldgr_code),
                                    new SqlParameter("@pc_sl_acno",slma_acno),
                                    new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(from_date)),
                                    new SqlParameter("@pc_submission_date",db.DBYYYYMMDDformat(to_date)),
                                    new SqlParameter("@pc_discd", 'O'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 finnobj = new Fin102();
                            finnobj.ps_comp_code = dr["pc_comp_code"].ToString();
                            finnobj.ps_discd = dr["Number"].ToString();//varibale used for Row Number
                            finnobj.ps_bank_code = dr["pc_bank_from"].ToString();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.ps_ldgr_code = dr["pc_sl_ldgr_code"].ToString();
                            finnobj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.ps_sl_acno = dr["pc_sl_acno"].ToString();
                            finnobj.ps_sl_acno_name = dr["pc_sl_acno_name"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            finnobj.ps_cheque_no = dr["pc_cheque_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                finnobj.ps_due_date = dr["pc_due_date"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.ps_amount = Decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.ps_bank_code_name = dr["pc_bank_from_name"].ToString();

                            finnobj.ps_rec_type = dr["pc_doc_type"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_cur_date"].ToString()) == false)
                                finnobj.pc_cur_date = dr["pc_cur_date"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();

                            finnobj.ps_select = false;

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        [Route("getAllPDCSubmissions")]
        public HttpResponseMessage getAllPDCSubmissions(string dept_no, string ldgr_code, string slma_acno, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCSubmissions(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPDCSubmissions"));

            List<Fin102> goaltarget_list = new List<Fin102>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]",
                        new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@opr_upd", "ACP"),
                                    new SqlParameter("@pc_comp_code", "1"),
                                    new SqlParameter("@pc_dept_no",dept_no),
                                    new SqlParameter("@pc_sl_ldgr_code",ldgr_code),
                                    new SqlParameter("@pc_sl_acno",slma_acno),
                                    new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(from_date)),
                                    new SqlParameter("@pc_submission_date",db.DBYYYYMMDDformat(to_date)),
                                    new SqlParameter("@pc_discd", "IR"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 finnobj = new Fin102();
                            finnobj.ps_comp_code = dr["pc_comp_code"].ToString();
                            finnobj.ps_discd = dr["Number"].ToString();//varibale used for Row Number
                            finnobj.ps_bank_code = dr["pc_bank_from"].ToString();
                            finnobj.pc_dept_no = dr["pc_dept_no"].ToString();
                            finnobj.ps_ldgr_code = dr["pc_sl_ldgr_code"].ToString();
                            finnobj.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            finnobj.ps_sl_acno = dr["pc_sl_acno"].ToString();
                            finnobj.ps_sl_acno_name = dr["pc_sl_acno_name"].ToString();
                            finnobj.pc_our_doc_no = dr["pc_our_doc_no"].ToString();
                            finnobj.ps_cheque_no = dr["pc_cheque_no"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_due_date"].ToString()) == false)
                                finnobj.ps_due_date = dr["pc_due_date"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_amount"].ToString()) == false)
                                finnobj.ps_amount = Decimal.Parse(dr["pc_amount"].ToString());
                            finnobj.ps_bank_code_name = dr["pc_bank_from_name"].ToString();

                            finnobj.ps_rec_type = dr["pc_doc_type"].ToString();
                            if (string.IsNullOrEmpty(dr["pc_cur_date"].ToString()) == false)
                                finnobj.pc_cur_date = dr["pc_cur_date"].ToString();
                            finnobj.slac_ctrl_acno = dr["slac_ctrl_acno"].ToString();
                            finnobj.pc_ref_no = dr["pc_ref_no"].ToString();
                            finnobj.ps_select = false;

                            goaltarget_list.Add(finnobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //  GetGLAccountNumber
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin138> doc_list = new List<Fin138>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin138 simsobj = new Fin138();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        
        [Route("getBankName")]
        public HttpResponseMessage getBankName(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBankName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getBankName"));

            List<Fin102> mod_list = new List<Fin102>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_sub]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@pb_comp_code", comp_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 simsobj = new Fin102();
                            simsobj.bank_code = dr["pb_bank_code"].ToString();
                            simsobj.bank_name = dr["pb_bank_name"].ToString();

                            try { 
                            simsobj.sims_default_bank = dr["sims_default_bank"].ToString();
                            }
                            catch(Exception e){}
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("GetCreatedBy_user")]
        public HttpResponseMessage GetCreatedBy_user()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCreatedBy_userPARAMETERS ::NA";


            List<Fin102> mod_list = new List<Fin102>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_cheques",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 simsobj = new Fin102();
                            simsobj.createdby_usr = dr["creation_user"].ToString();
                            simsobj.createdBY = dr["user_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }


        [Route("getDepartment")]
        public HttpResponseMessage getDepartment(string finance_year, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDepartment()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDepartment"));

            List<Fin102> mod_list = new List<Fin102>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_sub]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@fins_year", finance_year),
                            new SqlParameter("@pb_comp_code", comp_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 simsobj = new Fin102();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getLedgerName")]
        public HttpResponseMessage getLedgerName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLedgerName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getLedgerName"));

            List<Fin102> mod_list = new List<Fin102>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_sub]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin102 simsobj = new Fin102();
                            simsobj.ledger_code = dr["sllc_ldgr_code"].ToString();
                            simsobj.ledger_name = dr["sllc_ldgr_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("CUDBankMaster")]
        public HttpResponseMessage CUDBankMaster(List<Fin138> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fin138 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_bank]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pb_bank_code", simsobj.pb_bank_code),
                                new SqlParameter("@pb_bank_name", simsobj.pb_bank_name),
                                new SqlParameter("@pb_gl_acno", simsobj.acno),
                                new SqlParameter("@pb_sl_code", simsobj.sllc_ldgr_code),
                                new SqlParameter("@pb_sl_acno", simsobj.slma_acno),
                                new SqlParameter("@pb_srl_no", simsobj.pb_srl_no),
                                new SqlParameter("@pb_comp_code", "1"),//simsobj.pb_comp_code),

                        });
                        if (ins > 0)
                        {
                            if (simsobj.opr.Equals("U"))
                                message.strMessage = "Record Updated Sucessfully!!";
                            else if (simsobj.opr.Equals("I"))
                                message.strMessage = "Record  Added Sucessfully!!";
                            else if (simsobj.opr.Equals("D"))
                                message.strMessage = "Record  Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {

                            message.strMessage = "Bank code already present!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
           // return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("PDC_Submission_or_ReSubmission")]
        public HttpResponseMessage PDC_Submission_or_ReSubmission(List<Fin102> fobj)
        {
            Message message = new Message();
            string ref_no = string.Empty;
            bool insert = false;
            int flag = 0;
            int record_cnt = 1;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'U'));
                        if (finnobj.cnt == 1)//Check Submission
                        {
                            para.Add(new SqlParameter("@opr_upd", "CS"));
                            para.Add(new SqlParameter("@pc_discd", 'I'));
                        }
                        else if (finnobj.cnt == 2)
                        {
                            para.Add(new SqlParameter("@opr_upd", "CR"));
                            para.Add(new SqlParameter("@pc_discd", 'R'));
                        }
                        else if (finnobj.cnt == 3)
                        {
                            para.Add(new SqlParameter("@opr_upd", "CL"));
                            para.Add(new SqlParameter("@pc_discd", 'O'));
                        }
                        para.Add(new SqlParameter("@pc_comp_code", finnobj.ps_comp_code));
                        para.Add(new SqlParameter("@pc_doc_type", finnobj.ps_rec_type));
                        para.Add(new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code));
                        para.Add(new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno));
                        para.Add(new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no));
                        para.Add(new SqlParameter("@pc_bank_from", finnobj.ps_bank_code));
                        para.Add(new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no));
                        para.Add(new SqlParameter("@pc_due_date", db.DBYYYYMMDDformat(finnobj.ps_due_date)));
                        para.Add(new SqlParameter("@pc_amount", finnobj.ps_amount));
                        para.Add(new SqlParameter("@pc_submission_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day));
                        para.Add(new SqlParameter("@pc_ref_no", finnobj.pc_ref_no));
                        para.Add(new SqlParameter("@pc_payment_mode", finnobj.pc_payment_mode));
                        para.Add(new SqlParameter("@user_name", finnobj.ps_ldgr_code_name));
                        para.Add(new SqlParameter("@count", record_cnt));
                        para.Add(new SqlParameter("@pc_narrative", finnobj.pc_narrative));

                        if (finnobj.cnt == 1 || finnobj.cnt == 3)
                        {

                            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]", para);
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    if (!string.IsNullOrEmpty(dr["pc_ref_no"].ToString()))
                                    {
                                        ref_no = dr["pc_ref_no"].ToString();
                                        flag = 1;

                                    }

                                }
                            }
                        }
                        else if (finnobj.cnt == 2)
                        {

                            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]", para);
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    if (!string.IsNullOrEmpty(dr["pc_ref_no"].ToString()))
                                    {
                                        ref_no = dr["pc_ref_no"].ToString();
                                        flag = 1;

                                    }

                                }
                            }
                        }
                        record_cnt = record_cnt + 1;

                    }

                    if (flag > 0)
                    {
                        message.strMessage = "Cheque Transfered Successfully!!";
                        message.systemMessage = ref_no;
                        message.messageType = MessageType.Success;

                    }
                    else
                    {
                        message.strMessage = "Cheque not Transfered !!";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("PDC_Pool_Submission_data")]
        public HttpResponseMessage PDC_Pool_Submission_data(Fin102 obj)
        {
            Message message = new Message();
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : PDC_Pool_Submission_data(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "PDC_Pool_Submission_data", from_date));

            //bool inserted = false;
            string inserted = string.Empty;
            int Flag = 0;
            try
            {
                int ins;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_pdc_cheques]",
                   new List<SqlParameter>()
                    {
                                     new SqlParameter("@opr", 'P'),
                                     new SqlParameter("@opr_upd", "PD"),
                                     new SqlParameter("@pc_comp_code", obj.ps_comp_code),
                                     new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(obj.recFromDate)),
                                     new SqlParameter("@pc_submission_date",db.DBYYYYMMDDformat(obj.recToDate)),
                                     new SqlParameter("@pc_payment_mode", obj.pc_payment_mode),
                                     new SqlParameter("@pc_sl_ldgr_code", obj.ps_ldgr_code),
                    });
                }
                if (ins > 0)
                {
                    message.strMessage = ins + " Records Pulled Successfully";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                else
                {
                    message.strMessage = " No Data Pulled...!";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }



        [Route("PDC_Submission_or_ReSubmission1")]
        public HttpResponseMessage PDC_Submission_or_ReSubmission1(List<Fin102> fobj)
        {
            Message message = new Message();
            string ref_no = string.Empty;

            int flag = 0;
            int record_cnt = 1;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'U'));
                        if (finnobj.cnt == 1)//Check Submission
                        {
                            para.Add(new SqlParameter("@opr_upd", "CS"));
                            para.Add(new SqlParameter("@pc_discd", 'I'));
                        }
                        else if (finnobj.cnt == 2)
                        {
                            para.Add(new SqlParameter("@opr_upd", "CR"));
                            para.Add(new SqlParameter("@pc_discd", 'R'));
                        }
                        else if (finnobj.cnt == 3)
                        {
                            para.Add(new SqlParameter("@opr_upd", "CL"));
                            para.Add(new SqlParameter("@pc_discd", 'O'));
                        }
                        para.Add(new SqlParameter("@pc_comp_code", finnobj.ps_comp_code));
                        para.Add(new SqlParameter("@pc_doc_type", finnobj.ps_rec_type));
                        para.Add(new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code));
                        para.Add(new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno));
                        para.Add(new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no));
                        para.Add(new SqlParameter("@pc_bank_from", finnobj.ps_bank_code));
                        para.Add(new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no));
                        para.Add(new SqlParameter("@pc_due_date", db.DBYYYYMMDDformat(finnobj.ps_due_date)));
                        para.Add(new SqlParameter("@pc_amount", finnobj.ps_amount));
                        para.Add(new SqlParameter("@pc_submission_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day));
                        para.Add(new SqlParameter("@pc_ref_no", finnobj.pc_ref_no));
                        para.Add(new SqlParameter("@user_name", finnobj.ps_ldgr_code_name));
                        para.Add(new SqlParameter("@count", record_cnt));

                        if (finnobj.cnt == 1 || finnobj.cnt == 3)
                        {

                            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_cheques]", para);
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    if (!string.IsNullOrEmpty(dr["pc_ref_no"].ToString()))
                                    {
                                        ref_no = dr["pc_ref_no"].ToString();
                                        flag = 1;

                                    }

                                }
                            }
                        }
                        else if (finnobj.cnt == 2)
                        {

                            int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_pdc_cheques]", para);
                            if (ins > 0)
                            {
                                ref_no = "Inserted Successfully";
                            }
                        }
                        record_cnt = record_cnt + 1;

                    }

                    if (flag > 0)
                    {
                        message.strMessage = "Cheque Transfered Successfully!!";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;

                    }
                    else
                    {
                        message.strMessage = "Cheque not Transfered !!";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("Finn102_PDC_Cancellation")]
        public HttpResponseMessage Finn102_PDC_Cancellation(List<Fin102> fobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", fobj));
            Message message = new Message();

            bool inserted = false;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {


                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_cheques",
                                new List<SqlParameter>()
                            {
                       
                 
                new SqlParameter("@opr", 'C'),
                new SqlParameter("@pc_comp_code", finnobj.ps_comp_code),
                new SqlParameter("@pc_doc_type", finnobj.ps_rec_type),
                new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code),
                new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno),
                new SqlParameter("@pc_dept_no", finnobj.ps_dept_no),
                new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no),
                new SqlParameter("@pc_bank_from", finnobj.ps_bank_code),
                new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no),
                new SqlParameter("@pc_amount", finnobj.ps_amount),
                new SqlParameter("@pc_discd", "C"),
                new SqlParameter("@pc_cur_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@pc_cancel_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                //new SqlParameter("@pc_narrative", finnobj.ps_dept_name),
                new SqlParameter("@pc_due_date",db.DBYYYYMMDDformat(finnobj.ps_due_date)),
            //    new SqlParameter("@pc_amount", finnobj.ps_amount),
                new SqlParameter("@user_name", finnobj.ps_ldgr_code_name),
                new SqlParameter("@gltd_cur_status",  "Pos"),
                new SqlParameter("@gltd_doc_code", "CC"),
                new SqlParameter("@pc_payment_mode", finnobj.pc_payment_mode),
                new SqlParameter("@pc_narrative", finnobj.pc_narrative),


                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

                if (inserted)
                {

                    message.strMessage = " Cheques Cancelled Successfully";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                else
                {
                    message.strMessage = "Cheques Not Cancelled...!";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("Finn102_PDC_Return")]
        public HttpResponseMessage Finn102_PDC_Return(List<Fin102> fobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", fobj));
            Message message = new Message();

            bool inserted = false;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();


                        if (finnobj.cnt == 1)
                        {
                            para.Add(new SqlParameter("@opr", 'R'));
                            para.Add(new SqlParameter("@opr_upd", "RC"));
                        }
                        else if (finnobj.cnt == 2)
                        {
                            para.Add(new SqlParameter("@opr", 'R'));
                            para.Add(new SqlParameter("@opr_upd", "GL"));
                        }
                        else if (finnobj.cnt == 3)
                        {
                            para.Add(new SqlParameter("@opr", 'U'));
                            para.Add(new SqlParameter("@opr_upd", "UPC"));

                        }

                        para.Add(new SqlParameter("@pc_comp_code", finnobj.ps_comp_code));
                        para.Add(new SqlParameter("@pc_doc_type", finnobj.ps_rec_type));
                        para.Add(new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code));
                        para.Add(new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno));
                        para.Add(new SqlParameter("@pc_dept_no", finnobj.ps_dept_no));
                        para.Add(new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no));
                        para.Add(new SqlParameter("@pc_bank_from", finnobj.ps_bank_code));
                        para.Add(new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no));
                        para.Add(new SqlParameter("@pc_due_date", db.DBYYYYMMDDformat(finnobj.ps_due_date)));
                        para.Add(new SqlParameter("@pc_amount", finnobj.ps_amount));
                        para.Add(new SqlParameter("@pc_discd", "B"));
                        para.Add(new SqlParameter("@pc_cur_date", db.DBYYYYMMDDformat(finnobj.ps_approv_date)));
                        para.Add(new SqlParameter("@user_name", finnobj.ps_ldgr_code_name));
                        para.Add(new SqlParameter("@gltd_cur_status", "Pos"));
                        para.Add(new SqlParameter("@gltd_doc_code", "RC"));
                        para.Add(new SqlParameter("@pc_prov_doc_no", finnobj.ps_bank_code_name));
                        para.Add(new SqlParameter("@pc_final_doc_no", finnobj.ps_discd));
                        para.Add(new SqlParameter("@pc_bank_slno", finnobj.ps_numeric));
                        para.Add(new SqlParameter("@pc_pstng_no", finnobj.ps_bank_slno));
                        para.Add(new SqlParameter("@pc_payment_mode", finnobj.pc_payment_mode));
                        para.Add(new SqlParameter("@pc_narrative", finnobj.pc_narrative));
                        if (finnobj.ps_select == true)
                            para.Add(new SqlParameter("@pc_allow_chq_bounc_chargs", "Y"));
                        else
                            para.Add(new SqlParameter("@pc_allow_chq_bounc_chargs", "N"));

                        int ins = db.ExecuteStoreProcedureforInsert("fins_pdc_cheques", para);

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

                if (inserted)
                {

                    message.strMessage = " Cheques Returned Successfully";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                else
                {
                    message.strMessage = "Cheques Not Returned...!";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("Finn102_PDC_Realise")]
        public HttpResponseMessage Finn102_PDC_Realise(List<Fin102> fobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", fobj));
            Message message = new Message();

            bool inserted = false;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        if (string.IsNullOrEmpty(finnobj.ps_approv_date))
                            finnobj.ps_approv_date = DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year;
                        int ins = db.ExecuteStoreProcedureforInsert("pdc_realize",
                                new List<SqlParameter>()
                            {
                       
                 
                 new SqlParameter("@pc_comp_code", finnobj.ps_comp_code),
                new SqlParameter("@pc_dept_no", finnobj.pc_dept_no),
                new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code),
                new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno),
                new SqlParameter("@pc_ref_no", finnobj.pc_ref_no),
                new SqlParameter("@gltd_doc_code", "CD"),                
                new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no),
                new SqlParameter("@pc_bank_from", finnobj.ps_bank_code),
                new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no),
                new SqlParameter("@pc_amount", finnobj.ps_amount),
                new SqlParameter("@pc_discd", "S"),
                new SqlParameter("@user_name", finnobj.ps_ldgr_code_name),
                new SqlParameter("@gltd_doc_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_doc_flag",null),
                new SqlParameter("@gltd_cur_status","Pos"),
                new SqlParameter("@gltd_prepare_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_verify_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_authorize_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_payment_date", db.DBYYYYMMDDformat(finnobj.ps_due_date)),
                new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(finnobj.ps_approv_date)),
                new SqlParameter("@gltd_paid_to", finnobj.ps_sl_acno),
                new SqlParameter("@gltd_cheque_no",finnobj.ps_cheque_no),
                new SqlParameter("@pc_payment_mode", finnobj.pc_payment_mode),
                new SqlParameter("@gltd_remarks", finnobj.pc_narrative),
                new SqlParameter("@pc_bank_to", finnobj.bank_code),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                if (inserted)
                {

                    message.strMessage = " Cheques Realized Successfully";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                else
                {
                    message.strMessage = "Cheques Not Realized...!";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("Finn102_PDC_Realise_ABQIS")]
        public HttpResponseMessage Finn102_PDC_Realise_ABQIS(List<Fin102> fobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", fobj));
            Message message = new Message();

            bool inserted = false;
            try
            {
                foreach (Fin102 finnobj in fobj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        if (string.IsNullOrEmpty(finnobj.ps_approv_date))
                            finnobj.ps_approv_date = DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year;
                        int ins = db.ExecuteStoreProcedureforInsert("pdc_realize_ABQIS",
                                new List<SqlParameter>()
                            {
                       
                 
                 new SqlParameter("@pc_comp_code", finnobj.ps_comp_code),
                new SqlParameter("@pc_dept_no", finnobj.pc_dept_no),
                new SqlParameter("@pc_sl_ldgr_code", finnobj.ps_ldgr_code),
                new SqlParameter("@pc_sl_acno", finnobj.ps_sl_acno),
                new SqlParameter("@pc_ref_no", finnobj.pc_ref_no),
                new SqlParameter("@gltd_doc_code", "CD"),                
                new SqlParameter("@pc_our_doc_no", finnobj.pc_our_doc_no),
                new SqlParameter("@pc_bank_from", finnobj.ps_bank_code),
                new SqlParameter("@pc_cheque_no", finnobj.ps_cheque_no),
                new SqlParameter("@pc_amount", finnobj.ps_amount),
                new SqlParameter("@pc_discd", "S"),
                new SqlParameter("@user_name", finnobj.ps_ldgr_code_name),
                new SqlParameter("@gltd_doc_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_doc_flag",null),
                new SqlParameter("@gltd_cur_status","Pos"),
                new SqlParameter("@gltd_prepare_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_verify_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_authorize_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                new SqlParameter("@gltd_payment_date", db.DBYYYYMMDDformat(finnobj.ps_due_date)),
                new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(finnobj.ps_approv_date)),
                new SqlParameter("@gltd_paid_to", finnobj.ps_sl_acno),
                new SqlParameter("@gltd_cheque_no",finnobj.ps_cheque_no),
                new SqlParameter("@pc_payment_mode", finnobj.pc_payment_mode),
                new SqlParameter("@gltd_remarks", finnobj.pc_narrative),
                new SqlParameter("@pc_bank_to", finnobj.bank_code),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                if (inserted)
                {

                    message.strMessage = " Cheques Realized Successfully";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                else
                {
                    message.strMessage = "Cheques Not Realized...!";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getDefaultBank")]
        public HttpResponseMessage getDefaultBank()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLedgerName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getLedgerName"));

            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_pdc_sub]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 simsobj = new Finn212();
                            simsobj.fins_appl_parameter = dr["sllc_ldgr_code"].ToString();
                            simsobj.fins_appl_form_field_value1 = dr["sllc_ldgr_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }
    }

}










