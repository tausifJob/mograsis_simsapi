﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/accountingqueryview")]
    public class AccountingViewQueryApplicationController : ApiController
    {
        #region Fins_Account_Codes_AVQA01...

        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {
            List<AVQA01> lstCuriculum = new List<AVQA01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 sequence = new AVQA01();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getfinYear")]
        public HttpResponseMessage getfinYear(string comp_code)
        {
            List<AVQA01> lstModules = new List<AVQA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comp_code",comp_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 sequence = new AVQA01();
                            sequence.sims_financial_year = dr["sims_financial_year"].ToString();
                            sequence.sims_financial_year_description = dr["sims_financial_year_description"].ToString();
                            sequence.sims_financial_year_status = dr["sims_financial_year_status"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getdepCode")]
        public HttpResponseMessage getdepCode(string comp_code, string financial_year)
        {
            List<AVQA01> schedule_list = new List<AVQA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@comp_code",comp_code),
                                new SqlParameter("@acad_year",financial_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 finnobj = new AVQA01();
                            finnobj.codp_dept_no = dr["codp_dept_no"].ToString();
                            finnobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            schedule_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, schedule_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, schedule_list);
            }
        }

        [Route("getScheduleCode")]
        public HttpResponseMessage getGetScheduleCode(string comp_code, string financial_year)
        {
            List<AVQA01> schedule_list = new List<AVQA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@comp_code",comp_code),
                                new SqlParameter("@acad_year",financial_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 finnobj = new AVQA01();
                            finnobj.glac_sched_desc = dr["glsc_sched_name"].ToString();
                            finnobj.glac_sched_code = dr["glsc_sched_code"].ToString();
                            schedule_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, schedule_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, schedule_list);
            }
        }

        [Route("getAllAccountTypes")]
        public HttpResponseMessage getAllAccountTypes(string comp_code)
        {
            List<AVQA01> type_list = new List<AVQA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@comp_code",comp_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 finnobj = new AVQA01();
                            finnobj.glac_type = dr["fins_appl_form_field_value1"].ToString();
                            finnobj.glac_type_code = dr["fins_appl_parameter"].ToString();
                            type_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }
             

        [Route("getAllFinsAccountCodes")]
        public HttpResponseMessage getAllFinsAccountCodes(string comp_code, string financial_year,string dep_code, string acount_type,string schedule_code)
        {
            List<AVQA01> code_list = new List<AVQA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_AccountingViewQueryApplication_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@comp_code", comp_code),                            
                            new SqlParameter("@acad_year",financial_year),
                            new SqlParameter("@dept_no",dep_code),
                            new SqlParameter("@acc_type", acount_type),
                            new SqlParameter("@sched_code", schedule_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AVQA01 finn = new AVQA01();
                            finn.gltr_dept_no = dr["gltr_dept_no"].ToString();
                            finn.codp_dept_name = dr["codp_dept_name"].ToString();
                            finn.gltr_acct_code = dr["gltr_acct_code"].ToString();
                            finn.glac_name = dr["glac_name"].ToString();
                            finn.account_Type = dr["account_Type"].ToString();
                            finn.glac_sched_code1 = dr["glac_sched_code1"].ToString();
                            finn.debit_amt = dr["debit_amt"].ToString();
                            finn.credit_amt = dr["credit_amt"].ToString();
                            finn.balance = dr["balance"].ToString();
                            finn.sims_financial_year_start_date= dr["sims_financial_year_start_date"].ToString();
                            finn.sims_financial_year_end_date = dr["sims_financial_year_end_date"].ToString();
                            finn.gltr_year = dr["gltr_year"].ToString(); 


                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

       
        #endregion
    }
}