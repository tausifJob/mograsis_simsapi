﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.financeClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.finance
{

    [RoutePrefix("api/VoucherReversal")]
    public class VoucherReversalController : ApiController
    {
        //string comp_code = "1";
        public static string Finn060_gltd_cur_state_save = "Sav";
        public static string Finn060_gltd_cur_state_verify = "Vry";
        public static string Finn060_gltd_cur_state_authories = "Aut";
        public static string Finn060_gltd_cur_state_posting_date = "Pos";
        public static string Finn060_gltd_cur_state_revort = "Rev";
        public static string Finn060_sltr_print_status = "I";


        private string getFinancialYear()
        {
            string financialyear = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_temp_doc_details_proc]",
                       new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            financialyear = dr["fins_appl_form_field_value1"].ToString();
                        }
                    }
                }
                return financialyear;
            }
            catch (Exception e)
            {
                return financialyear;
            }
        }

        //PDC unrealize ()
   
        
        #region PDC unrealize      
       
        [Route("getAllVouchers")]
        public HttpResponseMessage getAllVouchers(string comp_cd, string doc_cd, string final_doc_no, string from_date, string to_date, string cheque_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVouchers()PARAMETERS ::NA";
            List<Fin141> getRecords = new List<Fin141>();
            Message message = new Message();
            try
            {
                if (final_doc_no == "undefined" || final_doc_no == "")
                    final_doc_no = null;
                if (doc_cd == "undefined" || doc_cd == "")
                    doc_cd = null;
                if (from_date == "undefined" || from_date == "")
                    from_date = null;
                if (to_date == "undefined" || to_date == "")
                    to_date = null;
                if (cheque_no == "undefined" || cheque_no == "")
                    cheque_no = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@opr_upd", "CV"),
                            new SqlParameter("@gltd_comp_code", comp_cd),
                            new SqlParameter("@gltd_doc_code",doc_cd),
                            new SqlParameter("@gltd_final_doc_no",final_doc_no),
                            new SqlParameter("@gltd_authorize_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@gltd_cheque_no",cheque_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_prov_doc_no"].ToString()))
                                simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                simsobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                simsobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());

                            getRecords.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }



        [Route("getAllPDCvouchers")]
        public HttpResponseMessage getAllPDCvouchers(string comp_cd, string doc_cd, string final_doc_no, string from_date, string to_date, string cheque_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPDCvouchers()PARAMETERS ::NA";
            List<Fin141> getRecords = new List<Fin141>();
            Message message = new Message();
            try
            {
                if (final_doc_no == "undefined" || final_doc_no == "")
                    final_doc_no = null;
                if (doc_cd == "undefined" || doc_cd == "")
                    doc_cd = null;
                if (from_date == "undefined" || from_date == "")
                    from_date = null;
                if (to_date == "undefined" || to_date == "")
                    to_date = null;
                if (cheque_no == "undefined" || cheque_no == "")
                    cheque_no = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@opr_upd", "CR"),
                            new SqlParameter("@gltd_comp_code", comp_cd),
                            new SqlParameter("@gltd_doc_code",doc_cd),
                            new SqlParameter("@gltd_final_doc_no",final_doc_no),
                            new SqlParameter("@gltd_authorize_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@gltd_cheque_no",cheque_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin141 simsobj = new Fin141();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_prov_doc_no"].ToString()))
                                simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_doc_date"].ToString()))
                                simsobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_post_date"].ToString()))
                                simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                simsobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());

                            getRecords.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("Get_vouchers_data")]
        public HttpResponseMessage Get_vouchers_data(string doc_cd, string final_doc_no, string usrname, string comp_code, string fyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_vouchers_data()PARAMETERS ::NA";
            List<Finn141> getRecords = new List<Finn141>();
            Message message = new Message();
            decimal fc_amount = 0, amount = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr","R"),
                              new SqlParameter("@opr_upd", "RV"),
                              new SqlParameter("@gldd_comp_code", comp_code),
                              new SqlParameter("@gldd_doc_code", doc_cd),
                              new SqlParameter("@gldd_final_doc_no", final_doc_no),
                              new SqlParameter("@user_name", usrname),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn141 simsobj = new Finn141();
                            simsobj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                            simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            simsobj.gldd_prov_doc_no = dr["gldd_prov_doc_no"].ToString();
                            simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["gldd_line_no"].ToString()))
                                simsobj.gldd_line_no = decimal.Parse(dr["gldd_line_no"].ToString());
                            simsobj.gldd_doc_narr = "Reverse Entry - " + dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            simsobj.gldd_acct_name = dr["gldd_acct_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["gldd_doc_amount"].ToString()))
                            {
                                if (decimal.Parse(dr["gldd_doc_amount"].ToString()) > 0)
                                    simsobj.gldd_doc_amount_debit = decimal.Parse(dr["gldd_doc_amount"].ToString());
                                else
                                    simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gldd_doc_amount"].ToString()));
                            }

                            simsobj.gldd_fc_code = dr["gldd_fc_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["gldd_fc_rate"].ToString()))
                                simsobj.gldd_fc_rate = decimal.Parse(dr["gldd_fc_rate"].ToString());

                            if (!string.IsNullOrEmpty(dr["gldd_fc_amount"].ToString()))
                            {
                                decimal.TryParse(dr["gldd_fc_amount"].ToString(), out fc_amount);

                                //fc_amount = decimal.Parse(dr["gldd_fc_amount"].ToString());

                                if (fc_amount > 0)
                                    simsobj.gldd_fc_amount_debit = decimal.Parse(dr["gldd_fc_amount"].ToString());
                                else
                                    simsobj.gldd_fc_amount_credit = (decimal.Parse(dr["gldd_fc_amount"].ToString()));
                            }

                            simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                            simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                            simsobj.gldd_party_ref_date = db.UIDDMMYYYYformat(dr["gldd_party_ref_date"].ToString());
                            //simsobj.gldd_acct_code = dr["gldd_acct_name"].ToString();
                            getRecords.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, getRecords);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("Insert_ReverseVoucher")]
        public HttpResponseMessage Insert_ReverseVoucher(Finn141 finnobj)
        {
            bool inserted = false;
            string temp_doc_provno = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_temp_docs]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@opr_upd", "RV"),
                            new SqlParameter("@gltd_comp_code", finnobj.gltd_comp_code),
                            new SqlParameter("@gltd_doc_code", finnobj.gltd_doc_code),
                            new SqlParameter("@gltd_doc_date",db.DBYYYYMMDDformat(finnobj.gltd_doc_date)),
                            new SqlParameter("@gltd_post_date",db.DBYYYYMMDDformat(finnobj.gltd_post_date)),
                            new SqlParameter("@gltd_doc_narr", finnobj.gltd_doc_narr),
                            new SqlParameter("@gltd_remarks", finnobj.gltd_remarks),
                            new SqlParameter("@gltd_prepare_user", finnobj.gltd_prepare_user),
                            new SqlParameter("@gltd_prepare_date",db.DBYYYYMMDDformat(finnobj.gltd_doc_date)),                           
                            new SqlParameter("@gltd_reference_doc_code", finnobj.gltd_reference_doc_code),
                            new SqlParameter("@gltd_reference_final_doc_no", finnobj.gltd_reference_final_doc_no),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        if (dr.Read())
                        {
                            inserted = true;
                            temp_doc_provno = dr["gltd_prov_doc_no"].ToString();
                        }
                    }
                    if (dr.RecordsAffected < 0)
                    {
                        inserted = false;
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, temp_doc_provno);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, temp_doc_provno);
        }

        [Route("getAllRecords")]
        public HttpResponseMessage getAllRecords(string doccode, string prnumber, string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecords()PARAMETERS ::NA";
            List<Fin150> com_list = new List<Fin150>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_temp_doc_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@opr_upd", "OV"),
                            new SqlParameter("@gldd_comp_code",comp_code),
                            new SqlParameter("@gldd_doc_code",doccode),
                            new SqlParameter("@gldd_final_doc_no",prnumber)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin150 simsobj = new Fin150();
                            simsobj.gldd_comp_code = dr["gldd_comp_code"].ToString();
                            simsobj.gldd_doc_code = dr["gldd_doc_code"].ToString();
                            simsobj.gltd_prov_doc_no = dr["gltd_prov_doc_no"].ToString();
                            simsobj.gldd_final_doc_no = dr["gldd_final_doc_no"].ToString();
                            simsobj.gldd_doc_narr = dr["gldd_doc_narr"].ToString();
                            simsobj.gldd_ledger_code = dr["gldd_ledger_code"].ToString();
                            simsobj.gldd_acct_code = dr["gldd_acct_code"].ToString();
                            simsobj.gldd_acct_name = dr["gldd_acct_name"].ToString();

                            if (string.IsNullOrEmpty("gldd_doc_amount") == false)
                            {
                                if (decimal.Parse(dr["gldd_doc_amount"].ToString()) > 0)
                                    simsobj.gldd_doc_amount_debit = Convert.ToDecimal(dr["gldd_doc_amount"].ToString());
                                else
                                    simsobj.gldd_doc_amount_credit = Math.Abs(Convert.ToDecimal(dr["gldd_doc_amount"].ToString()));
                            }
                            //simsobj.gldd_doc_amount = dr["gldd_doc_amount"].ToString();
                            simsobj.gldd_dept_code = dr["gldd_dept_code"].ToString();
                            simsobj.gldd_party_ref_no = dr["gldd_party_ref_no"].ToString();
                            simsobj.gltd_comp_code = dr["gltd_comp_code"].ToString();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_doc_narr = dr["gltd_doc_narr"].ToString();
                            simsobj.gltd_doc_date = db.UIDDMMYYYYformat(dr["gltd_doc_date"].ToString());
                            simsobj.gltd_post_date = db.UIDDMMYYYYformat(dr["gltd_post_date"].ToString());
                            simsobj.gltd_paid_to = dr["gltd_paid_to"].ToString();
                            simsobj.gltd_cheque_no = dr["gltd_cheque_no"].ToString();
                            simsobj.gltd_remarks = dr["gltd_remarks"].ToString();
                            simsobj.gldu_verify = dr["gltd_verify_user"].ToString();
                            simsobj.gldu_authorize = dr["gltd_authorize_user"].ToString();
                            if (!string.IsNullOrEmpty(dr["gltd_verify_date"].ToString()))
                                simsobj.gltd_verify_date = db.UIDDMMYYYYformat(dr["gltd_verify_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["gltd_authorize_date"].ToString()))
                                simsobj.gltd_authorize_date = db.UIDDMMYYYYformat(dr["gltd_authorize_date"].ToString());
                            simsobj.gldd_cost_center_code = dr["gldd_cost_center_code"].ToString();
                            simsobj.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        #endregion


    }
}