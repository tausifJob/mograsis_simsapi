﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/PrintVoucher")]
    public class PrintVoucherController:ApiController
    {

        [Route("getAllVoucherForPrint")]
        public HttpResponseMessage getAllVoucherForPrint(string comp_code, string from_date, string to_date, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";
            List<FINR15> com_list = new List<FINR15>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_print_vouchers_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FINR15 simsobj = new FINR15();

                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            simsobj.gltd_post_date =(dr["gltd_post_date"].ToString());
                            simsobj.pty_name = dr["pty_name"].ToString();
                            simsobj.voucher_amt = dr["voucher_amt"].ToString();
                            simsobj.print_pv_status = dr["print_pv_status"].ToString();

                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getAllVoucherForPrint_DPS")]
        public HttpResponseMessage getAllVoucherForPrint_DPS(string comp_code, string from_date, string to_date, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";
            List<FINR15> com_list = new List<FINR15>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_print_vouchers_proc_DPS]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@from_date",(from_date)),
                            new SqlParameter("@to_date",(to_date)),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FINR15 simsobj = new FINR15();
                            simsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            simsobj.gldc_doc_short_name = dr["gldc_doc_short_name"].ToString();
                            simsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            simsobj.gltd_post_date =(dr["gltd_post_date"].ToString());
                            simsobj.pty_name = dr["pty_name"].ToString();
                            simsobj.voucher_amt = dr["voucher_amt"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getPurchasedeatils")]
        public HttpResponseMessage getPurchasedeatils()
        {
            List<FINR15> mod_list = new List<FINR15>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[get_fins_report_url]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","J"),
                            new SqlParameter("@sub_opr","Single"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FINR15 simsobj = new FINR15();
                            simsobj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }



        [Route("getprintvoucherForCheque")]
        public HttpResponseMessage getprintvoucherForCheque()
        {
            List<FINR15> mod_list = new List<FINR15>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[get_fins_report_url]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","J"),
                            new SqlParameter("@sub_opr","cheque"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FINR15 simsobj = new FINR15();
                            simsobj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }



        #region INvoice Receiveable(APIS)


        [Route("getAllInvoiceReceivablePrint")]
        public HttpResponseMessage getAllInvoiceReceivablePrint(string comp_code, string from_date, string to_date, string search, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";
            List<FINR155> com_list = new List<FINR155>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_invoice_receiveable_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@year",academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FINR155 simsobj = new FINR155();

                            simsobj.gltr_year = dr["gltr_year"].ToString();
                            simsobj.gltr_doc_code = dr["gltr_doc_code"].ToString();
                            simsobj.gltr_our_doc_no = dr["gltr_our_doc_no"].ToString();
                            simsobj.gltr_pstng_date = (dr["gltr_pstng_date"].ToString());
                            simsobj.gltr_doc_narr = dr["gltr_doc_narr"].ToString();
                            simsobj.gltr_tran_amt = dr["gltr_tran_amt"].ToString();
                           

                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }
        #endregion


        //Trial Balance API
        [Route("getTrialBalance")]
        public HttpResponseMessage getTrialBalance(string from_date, string to_date, string viewmode, string comp_code, string year, string group)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";

            Object[] ob = new Object[4];
            List<TrialBalance> com_list = new List<TrialBalance>();
            List<TrialBalance> com_list1 = new List<TrialBalance>();
            List<TrialBalance> com_list2 = new List<TrialBalance>();
            List<TrialBalance> com_list3 = new List<TrialBalance>();
           
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_trial_balance_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@view_mode",viewmode),
                            new SqlParameter("@year",year),
                             new SqlParameter("@group",group),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrialBalance simsobj = new TrialBalance();

                            simsobj.acct_name= dr["acct_name"].ToString();
                            simsobj.glma_acct_code= dr["glma_acct_code"].ToString();
                            simsobj.glma_dept_no= dr["glma_dept_no"].ToString();
                            simsobj.codp_dept_name= dr["codp_dept_name"].ToString();
                            simsobj.codp_short_name= dr["codp_short_name"].ToString();
                            simsobj.glma_op_bal_amt_dr= dr["glma_op_bal_amt_dr"].ToString();
                            simsobj.glma_op_bal_amt_cr= dr["glma_op_bal_amt_cr"].ToString();
                            simsobj.glac_sched_code= dr["glac_sched_code"].ToString();
                            simsobj.glsc_sched_name= dr["glsc_sched_name"].ToString();
                            simsobj.gsgd_group_code= dr["gsgd_group_code"].ToString();	
                            simsobj.glsg_group_description= dr["glsg_group_description"].ToString();	
                            simsobj.transaction_amt_cr= dr["transaction_amt_cr"].ToString();
                            simsobj.transaction_amt_dr= dr["transaction_amt_dr"].ToString();	
                            simsobj.closing_balance_dr= dr["closing_balance_dr"].ToString();	
                            simsobj.closing_balance_cr= dr["closing_balance_cr"].ToString();
                            simsobj.glpr_bud_amt = dr["glpr_bud_amt"].ToString();
                            simsobj.budget_variance = dr["budget_variance"].ToString();
                            simsobj.budget_percentage = dr["budget_percentage"].ToString();
                            simsobj.glac_display_acct_code = dr["glac_display_acct_code"].ToString();
                            com_list.Add(simsobj);
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {
                            TrialBalance simsobj = new TrialBalance();
                            simsobj.glsg_group_code = dr["glsg_group_code"].ToString();
                            simsobj.glsg_group_description = dr["glsg_group_description"].ToString();
                            com_list1.Add(simsobj);
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {
                            TrialBalance simsobj = new TrialBalance();
                            simsobj.glsc_sched_code = dr["glsc_sched_code"].ToString();
                            simsobj.glsc_sched_name = dr["glsc_sched_name"].ToString();
                            simsobj.gsgd_group_code = dr["gsgd_group_code"].ToString();
                            
                            com_list2.Add(simsobj);
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {
                            TrialBalance simsobj = new TrialBalance();
                            simsobj.acct_name = dr["acct_name"].ToString();
                            simsobj.glma_dept_no = dr["glma_dept_no"].ToString();
                            simsobj.glma_acct_code = dr["glma_acct_code"].ToString();

                            com_list3.Add(simsobj);
                        }

                        ob[0] = new object();
                        ob[0] = com_list;
                        ob[1] = new object();
                        ob[1] = com_list1;
                        ob[2] = new object();
                        ob[2] = com_list2;
                        ob[3] = new object();
                        ob[3] = com_list3;

                        return Request.CreateResponse(HttpStatusCode.OK, ob);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, ob);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getTrialBalanceGroup")]
        public HttpResponseMessage getTrialBalanceGroup()
        {
            List<TrialBalance> lstModules = new List<TrialBalance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_trial_balance_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H")
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrialBalance sequence = new TrialBalance();
                            sequence.glsg_comp_code = dr["glsg_comp_code"].ToString();
                            sequence.glsg_group_code = dr["glsg_group_code"].ToString();
                            sequence.glsg_group_description = dr["glsg_group_description"].ToString();
                            sequence.glsg_group_type = dr["glsg_group_type"].ToString();
                            sequence.glsg_group_ind = dr["glsg_group_ind"].ToString();
                            sequence.glsg_parent_group = dr["glsg_parent_group"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }






        [Route("getTrialBalanceAccountCode")]
        public HttpResponseMessage getTrialBalanceAccountCode(string comp_code, string year, string account_code, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllVoucherForPrint()PARAMETERS ::NA";
            List<TrialBalance> com_list = new List<TrialBalance>();
            Message message = new Message();
            try
            {
               
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_trial_balance_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@year",year),
                            new SqlParameter("@acct_code",account_code),
                             new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),

                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrialBalance simsobj = new TrialBalance();

                            simsobj.gltr_comp_code= dr["gltr_comp_code"].ToString();
                            simsobj.gltr_year= dr["gltr_year"].ToString();
                            simsobj.gltr_dept_no= dr["gltr_dept_no"].ToString();
                            simsobj.gltr_acct_code= dr["gltr_acct_code"].ToString();
                            simsobj.glma_acct_name= dr["glma_acct_name"].ToString();
                            simsobj.gltr_pstng_date= dr["gltr_pstng_date"].ToString();
                            simsobj.gltr_doc_code= dr["gltr_doc_code"].ToString();
                            simsobj.gltr_our_doc_no= dr["gltr_our_doc_no"].ToString();
                            simsobj.gltr_doc_narr= dr["gltr_doc_narr"].ToString();
                            simsobj.debit= dr["debit"].ToString();
                            simsobj.credit = dr["credit"].ToString();


                           
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }
      

        //FeePosting

        [Route("getfee_posting_acno_mapping_fee_type")]
        public HttpResponseMessage getfee_posting_acno_mapping_fee_type(string curcode,string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProductAccountDetail(),PARAMETERS :: NO";


            List<feeposfeetype> goaltarget_list = new List<feeposfeetype>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_fee_posting_acno_mapping_fee_type_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_fee_cur_code", curcode),
                           new SqlParameter("@sims_fee_academic_year", academic_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            feeposfeetype simsobj = new feeposfeetype();

                                simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();

                                simsobj.sims_fee_code= dr["sims_fee_code"].ToString();
                                simsobj.sims_fee_code_description= dr["sims_fee_code_description"].ToString();

                                simsobj.fins_revenue_acno= dr["fins_revenue_acno"].ToString();
                                simsobj.revenue_acno_name= dr["revenue_acno_name"].ToString();

                                simsobj.fins_receivable_acno= dr["fins_receivable_acno"].ToString();
                                simsobj.receivable_acno_name= dr["receivable_acno_name"].ToString();

                                simsobj.fins_discount_acno= dr["fins_discount_acno"].ToString();
                                simsobj.discount_acno_name= dr["discount_acno_name"].ToString();

                                simsobj.fins_curr_advance_received_acno_jan = dr["fins_advance_received_acno_jan"].ToString();
                                simsobj.fins_curr_advance_received_acno_feb = dr["fins_advance_received_acno_feb"].ToString();
                                simsobj.fins_curr_advance_received_acno_mar = dr["fins_advance_received_acno_mar"].ToString();
                                simsobj.fins_curr_advance_received_acno_apr = dr["fins_advance_received_acno_apr"].ToString();
                                simsobj.fins_curr_advance_received_acno_may = dr["fins_advance_received_acno_may"].ToString();
                                simsobj.fins_curr_advance_received_acno_jun = dr["fins_advance_received_acno_jun"].ToString();
                                simsobj.fins_curr_advance_received_acno_jul = dr["fins_advance_received_acno_jul"].ToString();
                                simsobj.fins_curr_advance_received_acno_aug = dr["fins_advance_received_acno_aug"].ToString();
                                simsobj.fins_curr_advance_received_acno_sep = dr["fins_advance_received_acno_sep"].ToString();
                                simsobj.fins_curr_advance_received_acno_oct = dr["fins_advance_received_acno_oct"].ToString();
                                simsobj.fins_curr_advance_received_acno_nov = dr["fins_advance_received_acno_nov"].ToString();
                                simsobj.fins_curr_advance_received_acno_dec = dr["fins_advance_received_acno_dec"].ToString();

                                simsobj.fins_receivable_acno_jan = dr["fins_receivable_acno_jan"].ToString();
                                simsobj.fins_receivable_acno_feb = dr["fins_receivable_acno_feb"].ToString();
                                simsobj.fins_receivable_acno_mar = dr["fins_receivable_acno_mar"].ToString();
                                simsobj.fins_receivable_acno_apr = dr["fins_receivable_acno_apr"].ToString();
                                simsobj.fins_receivable_acno_may = dr["fins_receivable_acno_may"].ToString();
                                simsobj.fins_receivable_acno_jun = dr["fins_receivable_acno_jun"].ToString();
                                simsobj.fins_receivable_acno_jul = dr["fins_receivable_acno_jul"].ToString();
                                simsobj.fins_receivable_acno_aug = dr["fins_receivable_acno_aug"].ToString();
                                simsobj.fins_receivable_acno_sep = dr["fins_receivable_acno_sep"].ToString();
                                simsobj.fins_receivable_acno_oct = dr["fins_receivable_acno_oct"].ToString();
                                simsobj.fins_receivable_acno_nov = dr["fins_receivable_acno_nov"].ToString();
                                simsobj.fins_receivable_acno_dec = dr["fins_receivable_acno_dec"].ToString();





                                simsobj.fins_advance_academic_year_acno = dr["fins_advance_academic_year_acno"].ToString();
                                simsobj.advance_academic_year_acno_name = dr["advance_academic_year_acno_name"].ToString();

                                simsobj.fins_fees_posting_status = dr["fins_fees_posting_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        [Route("updateFeePosting_new")]
        public HttpResponseMessage updateFeePosting_new(List<feeposfeetype> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (feeposfeetype simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_fee_posting_acno_mapping_fee_type_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                               
                                new SqlParameter("@sims_fee_cur_code", simsobj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                                new SqlParameter("@fins_revenue_acno", simsobj.fins_revenue_acno),
                                new SqlParameter("@fins_receivable_acno", simsobj.fins_receivable_acno),
                                new SqlParameter("@fins_discount_acno", simsobj.fins_discount_acno),
                                new SqlParameter("@fins_advance_academic_year_acno", simsobj.fins_advance_academic_year_acno),
                                new SqlParameter("@fins_fees_posting_status",simsobj.fins_fees_posting_status==true?"A":"I"),


                                new SqlParameter("@fins_advance_received_acno_jan", simsobj.fins_curr_advance_received_acno_jan),
                                new SqlParameter("@fins_advance_received_acno_feb", simsobj.fins_curr_advance_received_acno_feb),
                                new SqlParameter("@fins_advance_received_acno_mar", simsobj.fins_curr_advance_received_acno_mar),
                                new SqlParameter("@fins_advance_received_acno_apr", simsobj.fins_curr_advance_received_acno_apr),
                                new SqlParameter("@fins_advance_received_acno_may", simsobj.fins_curr_advance_received_acno_may),
                                new SqlParameter("@fins_advance_received_acno_jun", simsobj.fins_curr_advance_received_acno_jun),
                                new SqlParameter("@fins_advance_received_acno_jul", simsobj.fins_curr_advance_received_acno_jul),
                                new SqlParameter("@fins_advance_received_acno_aug", simsobj.fins_curr_advance_received_acno_aug),
                                new SqlParameter("@fins_advance_received_acno_sep", simsobj.fins_curr_advance_received_acno_sep),
                                new SqlParameter("@fins_advance_received_acno_oct", simsobj.fins_curr_advance_received_acno_oct),
                                new SqlParameter("@fins_advance_received_acno_nov", simsobj.fins_curr_advance_received_acno_nov),
                                new SqlParameter("@fins_advance_received_acno_dec", simsobj.fins_curr_advance_received_acno_dec),

                                new SqlParameter("@fins_receivable_acno_jan", simsobj.fins_receivable_acno_jan),
                                new SqlParameter("@fins_receivable_acno_feb", simsobj.fins_receivable_acno_feb),
                                new SqlParameter("@fins_receivable_acno_mar", simsobj.fins_receivable_acno_mar),
                                new SqlParameter("@fins_receivable_acno_apr", simsobj.fins_receivable_acno_apr),
                                new SqlParameter("@fins_receivable_acno_may", simsobj.fins_receivable_acno_may),
                                new SqlParameter("@fins_receivable_acno_jun", simsobj.fins_receivable_acno_jun),
                                new SqlParameter("@fins_receivable_acno_jul", simsobj.fins_receivable_acno_jul),
                                new SqlParameter("@fins_receivable_acno_aug", simsobj.fins_receivable_acno_aug),
                                new SqlParameter("@fins_receivable_acno_sep", simsobj.fins_receivable_acno_sep),
                                new SqlParameter("@fins_receivable_acno_oct", simsobj.fins_receivable_acno_oct),
                                new SqlParameter("@fins_receivable_acno_nov", simsobj.fins_receivable_acno_nov),
                                new SqlParameter("@fins_receivable_acno_dec", simsobj.fins_receivable_acno_dec)

                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

             
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
         
        }







        [Route("updateFeePosting_new_month")]
        public HttpResponseMessage updateFeePosting_new_month(List<feeposfeetype> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (feeposfeetype simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[fins_fee_posting_acno_mapping_fee_type_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                               
                                new SqlParameter("@sims_fee_cur_code", simsobj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                                new SqlParameter("@fins_revenue_acno", simsobj.fins_revenue_acno),
                                new SqlParameter("@fins_receivable_acno", simsobj.fins_receivable_acno),
                                new SqlParameter("@fins_discount_acno", simsobj.fins_discount_acno),
                                new SqlParameter("@fins_advance_academic_year_acno", simsobj.fins_advance_academic_year_acno),
                                new SqlParameter("@fins_fees_posting_status",simsobj.fins_fees_posting_status==true?"A":"I"),


                                new SqlParameter("@fins_advance_received_acno_jan", simsobj.fins_curr_advance_received_acno_jan),
                                new SqlParameter("@fins_advance_received_acno_feb", simsobj.fins_curr_advance_received_acno_feb),
                                new SqlParameter("@fins_advance_received_acno_mar", simsobj.fins_curr_advance_received_acno_mar),
                                new SqlParameter("@fins_advance_received_acno_apr", simsobj.fins_curr_advance_received_acno_apr),
                                new SqlParameter("@fins_advance_received_acno_may", simsobj.fins_curr_advance_received_acno_may),
                                new SqlParameter("@fins_advance_received_acno_jun", simsobj.fins_curr_advance_received_acno_jun),
                                new SqlParameter("@fins_advance_received_acno_jul", simsobj.fins_curr_advance_received_acno_jul),
                                new SqlParameter("@fins_advance_received_acno_aug", simsobj.fins_curr_advance_received_acno_aug),
                                new SqlParameter("@fins_advance_received_acno_sep", simsobj.fins_curr_advance_received_acno_sep),
                                new SqlParameter("@fins_advance_received_acno_oct", simsobj.fins_curr_advance_received_acno_oct),
                                new SqlParameter("@fins_advance_received_acno_nov", simsobj.fins_curr_advance_received_acno_nov),
                                new SqlParameter("@fins_advance_received_acno_dec", simsobj.fins_curr_advance_received_acno_dec),

                                new SqlParameter("@fins_receivable_acno_jan", simsobj.fins_receivable_acno_jan),
                                new SqlParameter("@fins_receivable_acno_feb", simsobj.fins_receivable_acno_feb),
                                new SqlParameter("@fins_receivable_acno_mar", simsobj.fins_receivable_acno_mar),
                                new SqlParameter("@fins_receivable_acno_apr", simsobj.fins_receivable_acno_apr),
                                new SqlParameter("@fins_receivable_acno_may", simsobj.fins_receivable_acno_may),
                                new SqlParameter("@fins_receivable_acno_jun", simsobj.fins_receivable_acno_jun),
                                new SqlParameter("@fins_receivable_acno_jul", simsobj.fins_receivable_acno_jul),
                                new SqlParameter("@fins_receivable_acno_aug", simsobj.fins_receivable_acno_aug),
                                new SqlParameter("@fins_receivable_acno_sep", simsobj.fins_receivable_acno_sep),
                                new SqlParameter("@fins_receivable_acno_oct", simsobj.fins_receivable_acno_oct),
                                new SqlParameter("@fins_receivable_acno_nov", simsobj.fins_receivable_acno_nov),
                                new SqlParameter("@fins_receivable_acno_dec", simsobj.fins_receivable_acno_dec)

                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {


                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

        }


    }
}