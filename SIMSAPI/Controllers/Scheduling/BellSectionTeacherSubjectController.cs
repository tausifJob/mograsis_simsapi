﻿ using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/bellsectionteacher")]
    public class BellSectionTeacherSubjectController : ApiController
    {

        #region TeacherSubject

        [Route("TeacherSubjectCommon")]
        public HttpResponseMessage TeacherSubjectCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_bell_section_teacher_subject_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        [Route("GetAllTeacherName")]
        public HttpResponseMessage GetAllTeacherName()
        {
            List<Sims060> term_list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_teacher_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_gb_teacher_code_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetAllslotgroup")]
        public HttpResponseMessage GetAllslotgroup()
        {
            List<Sims060> term_list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_teacher_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","G")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetAllslotroom")]
        public HttpResponseMessage GetAllslotroom()
        {
            List<Sims060> term_list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_teacher_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","R")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_bell_slot_room_code = dr["sims_bell_slot_room_code"].ToString();
                            simsobj.sims_bell_slot_room_desc = dr["sims_bell_slot_room_desc"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("CUDbellsectionteacher")]
        public HttpResponseMessage CUDbellsectionteacher(List<Sims060> obj)
        {
            object o = null;
            bool result = true;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                foreach (Sims060 data in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_section_teacher_subject_proc]",
                             new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",data.opr),
                          new SqlParameter("@subject_code1",data.sims_subject_code),
                          new SqlParameter("@teacher_code",data.teacher_code),
                        new SqlParameter("@academic_year",data.academic_year),
                          new SqlParameter("@grade_code",data.grade_code),
                           new SqlParameter("@section_code",data.section_code),
                          
                          new SqlParameter("@slot_group",data.sims_bell_slot_group),
                          new SqlParameter("@slot_room",data.sims_bell_slot_room_code),
                          new SqlParameter("@lect_count",data.sims_bell_lecture_per_week) 
                     
                        
                        
                        });
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                result = false;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, result);
        }
    }
}