﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/BellSlotGroup")]
    public class BellSlotGroupController : ApiController
    {
        [Route("getBellslot")]
        public HttpResponseMessage getBellslot()
        {
            List<Sim062> bellslot = new List<Sim062>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_slot_group_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim062 obj = new Sim062();

                            obj.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                            obj.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();

                            obj.sims_bell_slot_group_status = dr["sims_bell_slot_group_status"].Equals("A") ? true : false;
                            bellslot.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, bellslot);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, bellslot);
        }

        [Route("BellSlotCUD")]
        public HttpResponseMessage BellSlotCUD(List<Sim062> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "BellSlotCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim062 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_slot_group_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_bell_slot_group",simsobj.sims_bell_slot_group),
                          new SqlParameter("@sims_bell_slot_group_desc", simsobj.sims_bell_slot_group_desc),


                          new SqlParameter("@sims_bell_slot_group_status",simsobj.sims_bell_slot_group_status==true?"A":"I")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}