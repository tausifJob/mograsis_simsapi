﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/bell")]
    public class BellController : ApiController
    {

        [Route("getBell")]
        public HttpResponseMessage getBell()
        {
            List<Sim061> bell = new List<Sim061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim061 obj = new Sim061();

                            obj.sims_bell_academic_year = dr["sims_bell_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            obj.sims_bell_code = dr["sims_bell_code"].ToString();
                            obj.sims_bell_desc = dr["sims_bell_desc"].ToString();

                            obj.sims_bell_status = dr["sims_bell_status"].Equals("A") ? true : false;


                            bell.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, bell);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, bell);
        }

        [Route("BellCUD")]
        public HttpResponseMessage BellCUD(List<Sim061> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "BellCUD", simsobj));

            //int i = 0;

            //if (simsobj.opr == "I")
            //{
            //    i = Convert.ToInt32(simsobj.sims_bell_code);
            //    if (i < 10)
            //    {
            //        simsobj.sims_bell_code = "0" + i;
            //    }
            //}
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim061 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_bell_academic_year",simsobj.sims_bell_academic_year),
                          new SqlParameter("@sims_bell_code", simsobj.sims_bell_code),
                          new SqlParameter("@sims_bell_desc",simsobj.sims_bell_desc),
                       
                          new SqlParameter("@sims_bell_status",simsobj.sims_bell_status==true?"A":"I")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        #region Learntron Data


        [Route("insertupdate_ClassSubject")]
        public HttpResponseMessage insertupdate_ClassSubject()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_ClassSubject_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("insertupdate_student_section_subject")]
        public HttpResponseMessage insertupdate_student_section_subject()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_sims_student_section_subject_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("insertupdate_homeroom_batch_student")]
        public HttpResponseMessage insertupdate_homeroom_batch_student()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_homeroom_batch_student_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("insertupdate_TeacherMaster")]
        public HttpResponseMessage insertupdate_TeacherMaster()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_TeacherMaster_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("insertupdate_homeroom_teacher")]
        public HttpResponseMessage insertupdate_homeroom_teacher()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_sims_homeroom_teacher_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("insertupdate_section_subject_teacher")]
        public HttpResponseMessage insertupdate_section_subject_teacher()
        {

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[proc_sims_bell_section_subject_teacher_insertupdate]",
                            new List<SqlParameter>()
                            {
                            });

                    if (ins > 0)
                    {
                        inserted = ins + " Records Updated Successfully";
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Learntron_user_audit_data")]
        public HttpResponseMessage Insert_Learntron_user_audit_data(Lrnton obj)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                        int ins = db.ExecuteStoreProcedureforInsert("[sims_learntron_user_audit]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",'I'),

                          new SqlParameter("@comn_audit_user_name",obj.comn_audit_user_name),
                          new SqlParameter("@comn_audit_user_appl_code", obj.comn_audit_user_appl_code),
                          new SqlParameter("@comn_audit_ip",obj.comn_audit_ip),
                          new SqlParameter("@comn_audit_dns",obj.comn_audit_dns),
                          new SqlParameter("@comn_audit_remark",obj.comn_audit_remark),
                       
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        #endregion

    }
}