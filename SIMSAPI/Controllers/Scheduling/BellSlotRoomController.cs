﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;


namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/BellSlotRoom")]
    public class BellSlotRoomController : ApiController
    {
        [Route("getBellroom")]
        public HttpResponseMessage getBellroom()
        {
            List<Sim063> bellroom = new List<Sim063>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_slot_room_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim063 obj = new Sim063();

                            obj.sims_bell_slot_room_code = dr["sims_bell_slot_room_code"].ToString();
                            obj.sims_bell_slot_room_desc = dr["sims_bell_slot_room_desc"].ToString();

                            obj.sims_bell_slot_room_status = dr["sims_bell_slot_room_status"].Equals("A") ? true : false;
                            bellroom.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, bellroom);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, bellroom);
        }

        [Route("SlotroomCUD")]
        public HttpResponseMessage SlotroomCUD(List<Sim063> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "SlotroomCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim063 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_slot_room_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_bell_slot_room_code",simsobj.sims_bell_slot_room_code),
                          new SqlParameter("@sims_bell_slot_room_desc", simsobj.sims_bell_slot_room_desc),


                          new SqlParameter("@sims_bell_slot_room_status",simsobj.sims_bell_slot_room_status==true?"A":"I")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}