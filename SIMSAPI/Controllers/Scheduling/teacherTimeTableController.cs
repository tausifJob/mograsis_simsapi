﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

using System.IO;
using System.Web.Configuration;
using System.Web;

namespace SIMSAPI.Controllers.modules.teacherTimeTableController
{
    [RoutePrefix("api/common/teacherTimeTable")]
    [BasicAuthentication]    
    public class teacherTimeTableController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("GetAll_Sims_Timetable_file")]
        public HttpResponseMessage GetAll_Sims_Timetable_file()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims171> list = new List<Sims171>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_timetable_pdf_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                            //  new SqlParameter("@sims_album_code", album_code)                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims171 simsobj = new Sims171();

                            simsobj.sims_timetable_number = dr["sims_timetable_number"].ToString();
                            simsobj.sims_timetable_cur_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_timetable_cur = dr["sims_timetable_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_timetable_academic_year"].ToString();                            
                            simsobj.sims_timetable_employee_id = dr["sims_timetable_employee_id"].ToString();
                            simsobj.employee_name = dr["employee_name"].ToString();
                            simsobj.sims_timetable_filename = dr["sims_timetable_filename"].ToString();

                            if (dr["sims_timetable_status"].ToString().Equals("A"))
                                simsobj.sims_timetable_status = true;
                            else
                                simsobj.sims_timetable_status = false;

                            //   sims_timetable.Add(simsobj);
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetAll_Sims_Timetable_fileUser")]
        public HttpResponseMessage GetAll_Sims_Timetable_fileUser(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims171> list = new List<Sims171>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_timetable_pdf_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_timetable_employee_id", user)                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims171 simsobj = new Sims171();

                            simsobj.sims_timetable_number = dr["sims_timetable_number"].ToString();
                            simsobj.sims_timetable_cur_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_timetable_cur = dr["sims_timetable_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_timetable_academic_year"].ToString();
                            simsobj.sims_timetable_employee_id = dr["sims_timetable_employee_id"].ToString();
                            simsobj.employee_name = dr["employee_name"].ToString();
                            simsobj.sims_timetable_filename = dr["sims_timetable_filename"].ToString();

                            if (dr["sims_timetable_status"].ToString().Equals("A"))
                                simsobj.sims_timetable_status = true;
                            else
                                simsobj.sims_timetable_status = false;

                            //   sims_timetable.Add(simsobj);
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Insert_Sims_Timetable_File")]
        public HttpResponseMessage Insert_Sims_Timetable_File(List<Sims171> data)
        {    
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_timetable_pdf_proc]",                        
                            new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_timetable_number", simsobj.sims_timetable_number),
                             new SqlParameter("@sims_timetable_cur_code", simsobj.sims_timetable_cur),
                             new SqlParameter("@sims_timetable_academic_year", simsobj.sims_academic_year),                 
                             new SqlParameter("@sims_timetable_employee_id", simsobj.sims_timetable_employee_id),
                             new SqlParameter("@sims_timetable_filename", simsobj.sims_timetable_filename),
                             new SqlParameter("@sims_appl_code", "Sim171"),
                             new SqlParameter("@comn_sequence_code", "TTC"),
                             new SqlParameter("@sims_timetable_status", simsobj.sims_timetable_status ? "A" : "I")


                         });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("Delete_Sims_Timetable_File")]
        public HttpResponseMessage Delete_Sims_Timetable_File(List<Sims171> simsobj1)
        {
           
            bool inserted = false;
            try
            {
                foreach (Sims171 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_teacher_timetable_pdf_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_timetable_pdf_proc]",
                            new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_timetable_number", simsobj.sims_timetable_number),
                             new SqlParameter("@sims_timetable_cur_code", simsobj.sims_timetable_cur),
                             new SqlParameter("@sims_timetable_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_timetable_grade_code", simsobj.sims_grade),
                             new SqlParameter("@sims_timetable_section_code", simsobj.sims_section),
                             new SqlParameter("@sims_timetable_filename", simsobj.sims_timetable_filename),
                             new SqlParameter("@sims_appl_code", "Sim171"),
                             new SqlParameter("@comn_sequence_code", "TTC"),
                             new SqlParameter("@sims_timetable_status", simsobj.sims_timetable_status ? "A" : "I")
                             
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";
            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        // string type = Path.GetFileName(hpf.ContentType);
                        file = filename;
                        hpf.SaveAs((root + filename));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }



    }
}