﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/BellDay")]
    public class BellDayController : ApiController
    {
        [Route("getBellDay")]
        public HttpResponseMessage getBellDay()
        {
            List<Sim064> bellday = new List<Sim064>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_day_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim064 obj = new Sim064();

                            obj.sims_bell_academic_year = dr["sims_bell_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_bell_code = dr["sims_bell_code"].ToString();
                            obj.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            obj.sims_bell_day_code = dr["sims_bell_day_code"].ToString();
                            obj.sims_bell_day_desc = dr["sims_bell_day_desc"].ToString();
                            obj.sims_bell_day_short_desc = dr["sims_bell_day_short_desc"].ToString();
                            obj.sims_bell_day_order = dr["sims_bell_day_order"].ToString();

                            bellday.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, bellday);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, bellday);
        }

        [Route("BellDayCUD")]
        public HttpResponseMessage BellDayCUD(List<Sim064> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "BellCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim064 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_day_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_bell_academic_year",simsobj.sims_bell_academic_year),
                          new SqlParameter("@sims_bell_code", simsobj.sims_bell_code),
                          new SqlParameter("@sims_bell_day_code",simsobj.sims_bell_day_code),
                          new SqlParameter("@sims_bell_day_desc", simsobj.sims_bell_day_desc),
                          new SqlParameter("@sims_bell_day_short_desc",simsobj.sims_bell_day_short_desc),
                          new SqlParameter("@sims_bell_day_order", simsobj.sims_bell_day_order),

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getBellDescByAcy")]
        public HttpResponseMessage getBellDescByAcy(string Acayear)
        {
            List<Sim064> belldesc = new List<Sim064>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_day_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_bell_academic_year",Acayear)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim064 obj = new Sim064();


                            obj.sims_bell_code = dr["sims_bell_code"].ToString();
                            obj.sims_bell_desc = dr["sims_bell_desc"].ToString();

                            belldesc.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, belldesc);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, belldesc);
        }

    }
}