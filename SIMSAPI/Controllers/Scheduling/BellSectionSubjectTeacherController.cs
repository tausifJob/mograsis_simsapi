﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/bellsectionsubject")]
    public class BellSectionSubjectTeacherController : ApiController
    {

        #region Sims060 ( bell_section_subject_teacher)

        [Route("GetAllTeacherName")]
        public HttpResponseMessage GetAllTeacherName()
        {
            List<Sims060> term_list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_section_subject_teacher_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_gb_teacher_code_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetSectionSubjectTeacher")]
        public HttpResponseMessage GetSectionSubjectTeacher(string data)
        {

            Sims060 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims060>(data);

            List<Sims060> list = new List<Sims060>();
            try
            {
                int count = 0;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_section_subject_teacher_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@academic_year", obj.academic_year),
                new SqlParameter("@grade_code", obj.grade_code),
                new SqlParameter("@roleCode", obj.section_code),
                new SqlParameter("@cur_code", obj.sims_cur_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims060 simsobj = new Sims060();
                            simsobj.sims_grade_section = dr["sims_grade_name_en"].ToString() + "-" + dr["sims_section_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            simsobj.sims_bell_code = dr["sims_bell_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_gb_teacher_code_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();
                            simsobj.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.sims_bell_slot_room = dr["sims_bell_slot_room"].ToString();
                            simsobj.sims_bell_slot_room_desc = dr["sims_bell_slot_room_desc"].ToString();
                            simsobj.sims_bell_group_order = dr["sims_bell_group_order"].ToString();
                            simsobj.sims_lession_type1 = dr["sims_bell_lesson_type"].ToString();
                            simsobj.sims_old_teacher_code = dr["sims_teacher_code"].ToString();
                           
                            simsobj.sims_bell_section_block = dr["sims_bell_section_block"].ToString().Equals("1") ? true : false;
                            
                            simsobj.sims_bell_section_block_size = dr["sims_bell_section_block_size"].ToString();

                            if (dr["sims_bell_lecture_count"].ToString() == "") simsobj.sims_bell_lecutre_count = 0;
                            else simsobj.sims_bell_lecutre_count = int.Parse(dr["sims_bell_lecture_count"].ToString());

                            if (dr["sims_bell_lecture_per_week"].ToString() == "") simsobj.sims_bell_lecture_per_week = 0;
                            else simsobj.sims_bell_lecture_per_week = int.Parse(dr["sims_bell_lecture_per_week"].ToString());

                            if (dr["sims_bell_slot_per_lecture"].ToString() == "") simsobj.sims_bell_slot_per_lecture = 0;
                            else simsobj.sims_bell_slot_per_lecture = int.Parse(dr["sims_bell_slot_per_lecture"].ToString());

                            if (dr["sims_bell_lecture_practical_count"].ToString() == "") simsobj.sims_bell_lecture_practical_count = 0;
                            else simsobj.sims_bell_lecture_practical_count = int.Parse(dr["sims_bell_lecture_practical_count"].ToString());

                            if (dr["sims_bell_practical_per_week"].ToString() == "") simsobj.sims_bell_practical_per_week = 0;
                            else simsobj.sims_bell_practical_per_week = int.Parse(dr["sims_bell_practical_per_week"].ToString());

                            if (dr["sims_bell_slot_pr_practical"].ToString() == "") simsobj.sims_bell_slot_per_practical = 0;
                            else simsobj.sims_bell_slot_per_practical = int.Parse(dr["sims_bell_slot_pr_practical"].ToString());
                            if (dr["sims_bell_lesson_type"].ToString() == "1")
                                simsobj.sims_lesson_type = "Lesson Type1";
                            else if (dr["sims_bell_lesson_type"].ToString() == "2")
                                simsobj.sims_lesson_type = "Lesson Type2";
                            if (dr["sub_status"].ToString() == "A")
                                simsobj.sims_sub_status = true;
                            else
                                simsobj.sims_sub_status = false;

                            string str= dr["total_count"].ToString();
                            if (string.IsNullOrEmpty(str))
                            {
                                simsobj.total_count = 0;
                            }
                            if (count<= simsobj.total_count)
                            {
                                count = int.Parse(dr["total_count"].ToString());
                                simsobj.total_count = int.Parse(dr["total_count"].ToString());
                            }else
                            {
                                simsobj.total_count = count;
                            }
                            list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("GetSectionSubjectTeacher_DMC")]
        public HttpResponseMessage GetSectionSubjectTeacher_DMC(string data)
        {

            Sims060 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims060>(data);

            List<Sims060> list = new List<Sims060>();
            try
            {
                int count = 0;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_section_subject_teacher_dmc_proc",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@academic_year", obj.academic_year),
                            new SqlParameter("@grade_code", obj.grade_code),
                            new SqlParameter("@roleCode", obj.section_code),
                            new SqlParameter("@cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_section_term_code", obj.sims_section_subject_term_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims060 simsobj = new Sims060();
                            simsobj.sims_grade_section = dr["sims_grade_name_en"].ToString() + "-" + dr["sims_section_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            simsobj.sims_bell_code = dr["sims_bell_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_gb_teacher_code_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();
                            simsobj.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.sims_bell_slot_room = dr["sims_bell_slot_room"].ToString();
                            simsobj.sims_bell_slot_room_desc = dr["sims_bell_slot_room_desc"].ToString();
                            simsobj.sims_bell_group_order = dr["sims_bell_group_order"].ToString();
                            simsobj.sims_lession_type1 = dr["sims_bell_lesson_type"].ToString();
                            simsobj.sims_old_teacher_code = dr["sims_teacher_code"].ToString();

                            simsobj.sims_bell_section_block = dr["sims_bell_section_block"].ToString().Equals("1") ? true : false;

                            simsobj.sims_bell_section_block_size = dr["sims_bell_section_block_size"].ToString();

                            if (dr["sims_bell_lecture_count"].ToString() == "") simsobj.sims_bell_lecutre_count = 0;
                            else simsobj.sims_bell_lecutre_count = int.Parse(dr["sims_bell_lecture_count"].ToString());

                            if (dr["sims_bell_lecture_per_week"].ToString() == "") simsobj.sims_bell_lecture_per_week = 0;
                            else simsobj.sims_bell_lecture_per_week = int.Parse(dr["sims_bell_lecture_per_week"].ToString());

                            if (dr["sims_bell_slot_per_lecture"].ToString() == "") simsobj.sims_bell_slot_per_lecture = 0;
                            else simsobj.sims_bell_slot_per_lecture = int.Parse(dr["sims_bell_slot_per_lecture"].ToString());

                            if (dr["sims_bell_lecture_practical_count"].ToString() == "") simsobj.sims_bell_lecture_practical_count = 0;
                            else simsobj.sims_bell_lecture_practical_count = int.Parse(dr["sims_bell_lecture_practical_count"].ToString());

                            if (dr["sims_bell_practical_per_week"].ToString() == "") simsobj.sims_bell_practical_per_week = 0;
                            else simsobj.sims_bell_practical_per_week = int.Parse(dr["sims_bell_practical_per_week"].ToString());

                            if (dr["sims_bell_slot_pr_practical"].ToString() == "") simsobj.sims_bell_slot_per_practical = 0;
                            else simsobj.sims_bell_slot_per_practical = int.Parse(dr["sims_bell_slot_pr_practical"].ToString());
                            if (dr["sims_bell_lesson_type"].ToString() == "1")
                                simsobj.sims_lesson_type = "Lesson Type1";
                            else if (dr["sims_bell_lesson_type"].ToString() == "2")
                                simsobj.sims_lesson_type = "Lesson Type2";
                            if (dr["sub_status"].ToString() == "A")
                                simsobj.sims_sub_status = true;
                            else
                                simsobj.sims_sub_status = false;

                            string str = dr["total_count"].ToString();
                            if (string.IsNullOrEmpty(str))
                            {
                                simsobj.total_count = 0;
                            }
                            if (count <= simsobj.total_count)
                            {
                                count = int.Parse(dr["total_count"].ToString());
                                simsobj.total_count = int.Parse(dr["total_count"].ToString());
                            }
                            else
                            {
                                simsobj.total_count = count;
                            }
                            list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("InsertSectionSubjectTeacher")]
        public HttpResponseMessage InsertSectionSubjectTeacher(Sims060 simsobj)
        {

            bool inserted = false;
            List<Sims060> list = new List<Sims060>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_teacher_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@academic_year", simsobj.academic_year),
                                new SqlParameter("@bell_code", simsobj.sims_bell_desc),
                                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@section_code", simsobj.sims_section_code),
                                new SqlParameter("@subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@teacher_code", simsobj.sims_gb_teacher_code),
                                new SqlParameter("@lect_count", simsobj.sims_bell_lecutre_count),
                                new SqlParameter("@lect_pract_count", simsobj.sims_bell_lecture_practical_count),
                                new SqlParameter("@slot_group", simsobj.sims_bell_slot_group_code),
                                new SqlParameter("@slot_room", simsobj.sims_bell_slot_room),
                                new SqlParameter("@sub_status", ""),
                                new SqlParameter("@roleCode", ""),
                               // new SqlParameter("@slot_room", simsobj.sims_bell_slot_room),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();

                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_parameter_proc]",
                    new List<SqlParameter>() 
                {
                new SqlParameter("@opr", simsobj.opr),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@bell_code", simsobj.sims_bell_desc),
                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                new SqlParameter("@section_code", simsobj.sims_section_code),
                new SqlParameter("@subject_code", simsobj.sims_subject_code),
                new SqlParameter("@group_order", simsobj.sims_bell_group_order),
                new SqlParameter("@lect_per_week", simsobj.sims_bell_lecture_per_week),
                new SqlParameter("@slot_per_lect", simsobj.sims_bell_slot_per_lecture),
                new SqlParameter("@pract_per_week", simsobj.sims_bell_practical_per_week),
                new SqlParameter("@slot_per_pract", simsobj.sims_bell_slot_per_practical),
                new SqlParameter("@lesson_type",simsobj.sims_lesson_type)
                
                });
                    if (dr1.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("InsertSectionSubjectTeacher_DMC")]
        public HttpResponseMessage InsertSectionSubjectTeacher_DMC(Sims060 simsobj)
        {

            bool inserted = false;
            List<Sims060> list = new List<Sims060>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_teacher_dmc_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@academic_year", simsobj.academic_year),
                                new SqlParameter("@bell_code", simsobj.sims_bell_desc),
                                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@section_code", simsobj.sims_section_code),
                                new SqlParameter("@subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@teacher_code", simsobj.sims_gb_teacher_code),
                                new SqlParameter("@lect_count", simsobj.sims_bell_lecutre_count),
                                new SqlParameter("@lect_pract_count", simsobj.sims_bell_lecture_practical_count),
                                new SqlParameter("@slot_group", simsobj.sims_bell_slot_group_code),
                                new SqlParameter("@slot_room", simsobj.sims_bell_slot_room),
                                new SqlParameter("@sub_status", ""),
                                new SqlParameter("@roleCode", ""),                                
                                new SqlParameter("@sims_bell_section_term", simsobj.sims_bell_section_term),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();

                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_parameter_proc]",
                    new List<SqlParameter>()
                {
                new SqlParameter("@opr", simsobj.opr),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@bell_code", simsobj.sims_bell_desc),
                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                new SqlParameter("@section_code", simsobj.sims_section_code),
                new SqlParameter("@subject_code", simsobj.sims_subject_code),
                new SqlParameter("@group_order", simsobj.sims_bell_group_order),
                new SqlParameter("@lect_per_week", simsobj.sims_bell_lecture_per_week),
                new SqlParameter("@slot_per_lect", simsobj.sims_bell_slot_per_lecture),
                new SqlParameter("@pract_per_week", simsobj.sims_bell_practical_per_week),
                new SqlParameter("@slot_per_pract", simsobj.sims_bell_slot_per_practical),
                new SqlParameter("@lesson_type",simsobj.sims_lesson_type)

                });
                    if (dr1.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("UpdateSectionSubjectTeacher")]
        public HttpResponseMessage UpdateSectionSubjectTeacher(List<Sims060> simsobj)
        {

            bool inserted = false;
            List<Sims060> list = new List<Sims060>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    for (int i = 0; i < simsobj.Count; i++)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_teacher_proc]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "U"),
                new SqlParameter("@academic_year", simsobj[i].academic_year),
                new SqlParameter("@bell_code", simsobj[i].sims_bell_desc),
                new SqlParameter("@grade_code", simsobj[i].sims_grade_code),
                new SqlParameter("@section_code", simsobj[i].sims_section_code),
                new SqlParameter("@lect_count", simsobj[i].sims_bell_lecutre_count),
                new SqlParameter("@lect_pract_count", simsobj[i].sims_bell_lecture_practical_count),
                new SqlParameter("@slot_group", simsobj[i].sims_bell_slot_group_code),
                new SqlParameter("@slot_room", simsobj[i].sims_bell_slot_room),
                new SqlParameter("@subject_code", simsobj[i].sims_subject_code),
                new SqlParameter("@teacher_code", simsobj[i].sims_gb_teacher_code),
                new SqlParameter("@sims_old_teacher_code", simsobj[i].sims_old_teacher_code),
                 new SqlParameter("@sims_bell_section_block", simsobj[i].sims_bell_section_block==false?'N':'Y'),
                new SqlParameter("@sims_bell_section_block_size", simsobj[i].sims_bell_section_block_size),
               
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                    for (int i = 0; i < simsobj.Count; i++)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_parameter_proc]",
                        new List<SqlParameter>() 
                {
                new SqlParameter("@opr", simsobj[i].opr),
                new SqlParameter("@academic_year", simsobj[i].academic_year),
                new SqlParameter("@bell_code", simsobj[i].sims_bell_desc),
                new SqlParameter("@grade_code", simsobj[i].sims_grade_code),
                new SqlParameter("@section_code", simsobj[i].sims_section_code),
                new SqlParameter("@subject_code", simsobj[i].sims_subject_code),
                new SqlParameter("@group_order", simsobj[i].sims_bell_group_order),
                new SqlParameter("@lect_per_week", simsobj[i].sims_bell_lecture_per_week),
                new SqlParameter("@slot_per_lect", simsobj[i].sims_bell_slot_per_lecture),
                new SqlParameter("@pract_per_week", simsobj[i].sims_bell_practical_per_week),
                new SqlParameter("@slot_per_pract", simsobj[i].sims_bell_slot_per_practical),
                new SqlParameter("@lesson_type",simsobj[i].sims_lesson_type)
                
                });
                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr1.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("UpdateSectionSubjectTeacher_DMC")]
        public HttpResponseMessage UpdateSectionSubjectTeacher_DMC(List<Sims060> simsobj)
        {

            bool inserted = false;
            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    for (int i = 0; i < simsobj.Count; i++)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_teacher_dmc_proc]",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "U"),
                                 new SqlParameter("@academic_year", simsobj[i].academic_year),
                                 new SqlParameter("@bell_code", simsobj[i].sims_bell_desc),
                                 new SqlParameter("@grade_code", simsobj[i].sims_grade_code),
                                 new SqlParameter("@section_code", simsobj[i].sims_section_code),
                                 new SqlParameter("@lect_count", simsobj[i].sims_bell_lecutre_count),
                                 new SqlParameter("@lect_pract_count", simsobj[i].sims_bell_lecture_practical_count),
                                 new SqlParameter("@slot_group", simsobj[i].sims_bell_slot_group_code),
                                 new SqlParameter("@slot_room", simsobj[i].sims_bell_slot_room),
                                 new SqlParameter("@subject_code", simsobj[i].sims_subject_code),
                                 new SqlParameter("@teacher_code", simsobj[i].sims_gb_teacher_code),
                                 new SqlParameter("@sims_old_teacher_code", simsobj[i].sims_old_teacher_code),
                                 new SqlParameter("@sims_bell_section_block", simsobj[i].sims_bell_section_block==false?'N':'Y'),
                                 new SqlParameter("@sims_bell_section_block_size", simsobj[i].sims_bell_section_block_size),
                                 new SqlParameter("@sims_bell_section_term", simsobj[i].sims_bell_section_term),

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                    for (int i = 0; i < simsobj.Count; i++)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_parameter_proc]",
                        new List<SqlParameter>()
                        {
                              new SqlParameter("@opr", simsobj[i].opr),
                              new SqlParameter("@academic_year", simsobj[i].academic_year),
                              new SqlParameter("@bell_code", simsobj[i].sims_bell_desc),
                              new SqlParameter("@grade_code", simsobj[i].sims_grade_code),
                              new SqlParameter("@section_code", simsobj[i].sims_section_code),
                              new SqlParameter("@subject_code", simsobj[i].sims_subject_code),
                              new SqlParameter("@group_order", simsobj[i].sims_bell_group_order),
                              new SqlParameter("@lect_per_week", simsobj[i].sims_bell_lecture_per_week),
                              new SqlParameter("@slot_per_lect", simsobj[i].sims_bell_slot_per_lecture),
                              new SqlParameter("@pract_per_week", simsobj[i].sims_bell_practical_per_week),
                              new SqlParameter("@slot_per_pract", simsobj[i].sims_bell_slot_per_practical),
                              new SqlParameter("@lesson_type",simsobj[i].sims_lesson_type)

                        });
                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr1.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("SectionSubjectTeacherDelete")]
        public HttpResponseMessage SectionSubjectTeacherDelete(Sims060 simsobj)
        {

            bool inserted = false;
            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_parameter_proc]",
                    new List<SqlParameter>() 
                         { 

                new SqlParameter("@opr", "D"),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@bell_code",simsobj.sims_bell_desc),
                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                new SqlParameter("@section_code", simsobj.sims_section_code),
                new SqlParameter("@subject_code", simsobj.sims_subject_code),
                new SqlParameter("@group_order", simsobj.sims_bell_group_order),
                new SqlParameter("@lect_per_week", simsobj.sims_bell_lecture_per_week),
                new SqlParameter("@slot_per_lect", simsobj.sims_bell_slot_per_lecture),
                new SqlParameter("@pract_per_week", simsobj.sims_bell_practical_per_week),
                new SqlParameter("@slot_per_pract", simsobj.sims_bell_slot_per_practical),
                new SqlParameter("@lesson_type",simsobj.sims_lesson_type),
                 
                });
                    if (dr2.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr2.Close();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_section_subject_teacher_proc]",
                    new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "D"),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@grade_code", simsobj.sims_grade_code),
                new SqlParameter("@section_code", simsobj.sims_section_code),
                new SqlParameter("@subject_code1", simsobj.sims_subject_code),
                new SqlParameter("@teacher_code", simsobj.sims_gb_teacher_code),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("GetBellDesc")]
        public HttpResponseMessage GetBellDesc(string academic_year)
        {
            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[emp_teacher_mapping_proc]",
                        new List<SqlParameter>() 
                     {
                new SqlParameter("@opr","S"),
                 new SqlParameter("@sims_bell_academic_year",academic_year)
                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetBellDesc")]
        public HttpResponseMessage GetSlotGroup()
        {
            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_proc]",

                        new List<SqlParameter>() 
                                    {
                                        new SqlParameter("@opr", "G")
                                    });

                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();
                            simsobj.sims_bell_slot_group_code = dr["sims_bell_slot_group"].ToString();
                            list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);

                }

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetSlotRoom")]
        public HttpResponseMessage GetSlotRoom()
        {
            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr","R")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_bell_slot_room_desc = dr["sims_bell_slot_room_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }


            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetAllSectionSubject")]
        public HttpResponseMessage GetAllSectionSubject(string section_code, string academic_year, string grade_code, string cur_code)
        {

            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_proc]",
                        new List<SqlParameter>() 
                         {
                         
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),
                             new SqlParameter("@sims_grade_code", grade_code),
                             new SqlParameter("@sims_section_code",section_code) 
                         });

                    while (dr.Read())
                    {
                        Sims060 simsobj = new Sims060();
                        simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        list.Add(simsobj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }

            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("GetLessonType")]
        public HttpResponseMessage GetLessonType()
        {

            List<Sims060> list = new List<Sims060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr","A")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims060 simsobj = new Sims060();
                            simsobj.sims_lesson_type = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_lesson_type_code = dr["sims_appl_parameter"].ToString();
                            list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);

                }
            }


            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }
        #endregion
 


    }
}