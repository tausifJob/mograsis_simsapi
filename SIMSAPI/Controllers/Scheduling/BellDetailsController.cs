﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Scheduling
{
    [RoutePrefix("api/BellDetails")]
    public class BellDetailsController : ApiController
    {
        [Route("getBellDetails")]
        public HttpResponseMessage getBellDetails()
        {
            List<Sim065> belldetails = new List<Sim065>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim065 obj = new Sim065();

                            obj.sims_bell_academic_year = dr["sims_bell_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            obj.sims_bell_code = dr["sims_bell_code"].ToString();
                            obj.sims_bell_day_code = dr["sims_bell_day_code"].ToString();
                            obj.sims_bell_slot_code = dr["sims_bell_slot_code"].ToString();
                            obj.sims_bell_break = dr["sims_bell_break"].Equals("Y") ? true : false;
                            obj.sims_bell_start_time = dr["sims_bell_start_time"].ToString();
                            obj.sims_bell_end_time = dr["sims_bell_end_time"].ToString();

                            obj.sims_bell_slot_desc = dr["sims_bell_slot_desc"].ToString();
                            obj.sims_bell_status = dr["sims_bell_status"].Equals("A") ? true : false;

                            obj.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            obj.sims_bell_day_desc = dr["sims_bell_day_desc"].ToString();
                            belldetails.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, belldetails);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, belldetails);
        }

        [Route("BellDetailsCUD")]
        public HttpResponseMessage BellDetailsCUD(List<Sim065> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "BellDetailsCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim065 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bell_detail_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_bell_academic_year",simsobj.sims_bell_academic_year),
                          new SqlParameter("@sims_bell_code", simsobj.sims_bell_code),
                          new SqlParameter("@sims_bell_day_code",simsobj.sims_bell_day_code),
                          new SqlParameter("@sims_bell_slot_code", simsobj.sims_bell_slot_code),
                          new SqlParameter("@sims_bell_break",simsobj.sims_bell_break.Equals(true)?"Y":"N"),
                          new SqlParameter("@sims_bell_start_time", simsobj.sims_bell_start_time),
                          new SqlParameter("@sims_bell_end_time", simsobj.sims_bell_end_time),
                          new SqlParameter("@sims_bell_slot_desc",simsobj.sims_bell_slot_desc),
                          new SqlParameter("@sims_bell_status",simsobj.sims_bell_status==true?"A":"I"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getBellDayCode")]
        public HttpResponseMessage getBellDayCode(string bellCode, string bellyear)
        {
            List<Sim065> lstModules = new List<Sim065>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@sims_bell_code", bellCode),
                            new SqlParameter("@sims_bell_academic_year",bellyear)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim065 obj = new Sim065();
                            obj.sims_bell_day_code = dr["sims_bell_day_code"].ToString();
                            obj.sims_bell_day_desc = dr["sims_bell_day_desc"].ToString();
                            lstModules.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

    }
}