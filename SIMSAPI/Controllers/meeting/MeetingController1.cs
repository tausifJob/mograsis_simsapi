﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models;

namespace SIMSAPI.Controllers.meeting
{
    [RoutePrefix("api/meeting")]
    public class MeetingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region SCHEDULE MEETING

        [Route("getMinutesOfMeeting")]
        public HttpResponseMessage getMinutesOfMeeting()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMinutesOfMeeting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMinutesOfMeeting"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();                            
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date =DateTime.Parse(dr["sims_mom_date"].ToString()).ToShortDateString();
                            }
                            obj.sims_mom_start_time = dr["sims_mom_start_time"].ToString();
                            obj.sims_mom_end_time = dr["sims_mom_end_time"].ToString();
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();

                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getMeetingType")]
        public HttpResponseMessage getMeetingType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMeetingType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMeetingType"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "T")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getReferenceNo")]
        public HttpResponseMessage getReferenceNo(string refno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getReferenceNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getReferenceNo"));

            List<meetings> list = new List<meetings>();
            int count = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "C"),
                              new SqlParameter("@sims_mom_reference_number", refno)

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            if(!string.IsNullOrEmpty(dr["cnt"].ToString()))                            
                            count =int.Parse(dr["cnt"].ToString());
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, count);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, count);
            }
        }

        [Route("getApprover")]
        public HttpResponseMessage getApprover()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getReferenceNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getReferenceNo"));

            List<meetings> list = new List<meetings>();
            int count = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "A")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAccess")]
        public HttpResponseMessage getAccess(string empno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccess(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAccess"));

            List<meetings> list = new List<meetings>();
            bool res = false;
            int count=0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "B"),
                              new SqlParameter("@employee_code", empno)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["cnt"].ToString()))
                               count = int.Parse(dr["cnt"].ToString());
                            if (count>0)
                            {
                                res = true;
                            }else{
                                res = false;
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
        }

        [Route("getRequester")]
        public HttpResponseMessage getRequester()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequester(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getRequester"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "D")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }


        [Route("CUDMinutesOfMeeting")]
        public HttpResponseMessage CUDMinutesOfMeeting(List<meetings> obj1)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj1 != null)
                {
                     foreach (meetings obj in obj1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_reference_number", obj.sims_mom_reference_number),
                                new SqlParameter("@sims_mom_type_code", obj.sims_mom_type_code),
                                new SqlParameter("@sims_mom_subject", obj.sims_mom_subject),
                                new SqlParameter("@sims_mom_venue", obj.sims_mom_venue),
                                new SqlParameter("@sims_mom_date", obj.sims_mom_date),
                                new SqlParameter("@sims_mom_start_time", obj.sims_mom_start_time),                                
                                new SqlParameter("@sims_mom_end_time", obj.sims_mom_end_time),
                                new SqlParameter("@sims_mom_chairperson_user_code", obj.sims_mom_chairperson_user_code),
                                new SqlParameter("@sims_mom_recorder_user_code", obj.sims_mom_recorder_user_code),
                                new SqlParameter("@sims_mom_requester_user_code", obj.sims_mom_requester_user_code),
                                new SqlParameter("@sims_mom_approver_user_code", obj.sims_mom_approver_user_code),
                                new SqlParameter("@sims_mom_date_creation_user_code", obj.sims_mom_date_creation_user_code),
                                new SqlParameter("@sims_mom_agenda", obj.sims_mom_agenda),
                                new SqlParameter("@sims_mom_status", obj.sims_mom_status),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();

                        }
                    }
                     return Request.CreateResponse(HttpStatusCode.OK, res);
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CUDInsertAttendees")]
        public HttpResponseMessage CUDInsertAttendees(List<meetings> obj1)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj1 != null)
                {
                    foreach (meetings obj in obj1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_reference_number", obj.sims_mom_reference_number),
                                new SqlParameter("@sims_mom_chairperson_user_code", obj.sims_mom_chairperson_user_code),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, res);

                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        #endregion

        #region APPROVE MEETING

        [Route("getMeetings")]
        public HttpResponseMessage getMeetings(string fdate,string sdate,string requsrcode,string subject)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMeetings(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMeetings"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_mom_from_date", fdate),
                              new SqlParameter("@sims_mom_to_date", sdate),
                              new SqlParameter("@sims_mom_requester_user_code", requsrcode),
                              new SqlParameter("@sims_mom_subject", subject)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date = DateTime.Parse(dr["sims_mom_date"].ToString()).ToShortDateString();
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_start_time"].ToString()))
                            {
                                obj.sims_mom_start_time =DateTime.Parse (dr["sims_mom_start_time"].ToString()).ToShortTimeString();
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_end_time"].ToString()))
                            {
                                obj.sims_mom_end_time = DateTime.Parse(dr["sims_mom_end_time"].ToString()).ToShortTimeString();
                            }
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_requester_user_name = dr["em_first_name"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }


        [Route("getAttendees")]
        public HttpResponseMessage getMeeting(string sims_mom_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAttendees"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "D"),
                              new SqlParameter("@sims_mom_number", sims_mom_number),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_receipient_user_code"].ToString();
                            obj.sims_mom_chairperson_user_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getSubjects")]
        public HttpResponseMessage getSubjects()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjects(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getSubjects"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "E")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getRequester2")]
        public HttpResponseMessage getRequester2()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequester2(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getRequester2"));

            List<meetings> list = new List<meetings>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "R")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetings obj = new meetings();
                            obj.em_number = dr["sims_mom_requester_user_code"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getApproveAccess")]
        public HttpResponseMessage getApproveAccess(string empno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getApproveAccess(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getApproveAccess"));

            List<meetings> list = new List<meetings>();
            bool res=false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@employee_code", empno)
                         });

                    if (dr.HasRows)
                    {
                        res = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
        }


    
        #endregion
    }
}