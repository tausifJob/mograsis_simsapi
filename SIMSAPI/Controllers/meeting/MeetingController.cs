﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.meeting
{
    [RoutePrefix("api/meeting")]
    public class MeetingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region SCHEDULE MEETING

        [Route("getMinutesofMeeting")]
        public HttpResponseMessage getMeeting()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMinutesofMeeting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMinutesofMeeting"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();                            
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date =db.UIDDMMYYYYformat(dr["sims_mom_date"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_start_time"].ToString()))
                            {
                                obj.sims_mom_start_time =db.UIDDMMYYYYformat(dr["sims_mom_start_time"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_end_time"].ToString()))
                            {
                                obj.sims_mom_end_time =DateTime.Parse(dr["sims_mom_end_time"].ToString()).ToShortTimeString();
                            }
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getMeetingType")]
        public HttpResponseMessage getMeetingType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMeetingType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMeetingType"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "T")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getReferenceNo")]
        public HttpResponseMessage getReferenceNo(string refno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getReferenceNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getReferenceNo"));

            List<meetingsclass> list = new List<meetingsclass>();
            int count = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "C"),
                              new SqlParameter("@sims_mom_reference_number", refno)

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            if(!string.IsNullOrEmpty(dr["cnt"].ToString()))                            
                            count =int.Parse(dr["cnt"].ToString());
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, count);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, count);
            }
        }

        [Route("getApprover")]
        public HttpResponseMessage getApprover()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getReferenceNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getReferenceNo"));

            List<meetingsclass> list = new List<meetingsclass>();
            int count = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "A")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAccess")]
        public HttpResponseMessage getAccess(string empno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccess(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAccess"));

            List<meetingsclass> list = new List<meetingsclass>();
            bool res = false;
            int count=0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "B"),
                              new SqlParameter("@employee_code", empno)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["cnt"].ToString()))
                               count = int.Parse(dr["cnt"].ToString());
                            if (count>0)
                            {
                                res = true;
                            }else{
                                res = false;
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
        }

        [Route("getRequester")]
        public HttpResponseMessage getRequester()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequester(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getRequester"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "D")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("CUDMinutesOfMeeting")]
        public HttpResponseMessage CUDMinutesOfMeeting(List<meetingsclass> obj1)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj1 != null)
                {
                     foreach (meetingsclass obj in obj1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_reference_number", obj.sims_mom_reference_number),
                                new SqlParameter("@sims_mom_type_code", obj.sims_mom_type_code),
                                new SqlParameter("@sims_mom_subject", obj.sims_mom_subject),
                                new SqlParameter("@sims_mom_venue", obj.sims_mom_venue),
                                new SqlParameter("@sims_mom_date",db.DBYYYYMMDDformat(obj.sims_mom_date)),
                                new SqlParameter("@sims_mom_start_time", obj.sims_mom_start_time),                                
                                new SqlParameter("@sims_mom_end_time", obj.sims_mom_end_time),
                                new SqlParameter("@sims_mom_chairperson_user_code", obj.sims_mom_chairperson_user_code),
                                new SqlParameter("@sims_mom_recorder_user_code", obj.sims_mom_recorder_user_code),
                                new SqlParameter("@sims_mom_requester_user_code", obj.sims_mom_requester_user_code),
                                new SqlParameter("@sims_mom_approver_user_code", obj.sims_mom_approver_user_code),
                                new SqlParameter("@sims_mom_date_creation_user_code", obj.sims_mom_date_creation_user_code),
                                new SqlParameter("@sims_mom_agenda", obj.sims_mom_agenda),
                                new SqlParameter("@sims_mom_status", obj.sims_mom_status),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();

                        }
                    }
                     return Request.CreateResponse(HttpStatusCode.OK, res);
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                //res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CUDInsertAttendees")]
        public HttpResponseMessage CUDInsertAttendees(List<meetingsclass> obj1)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj1 != null)
                {
                    foreach (meetingsclass obj in obj1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_reference_number", obj.sims_mom_reference_number),
                                new SqlParameter("@sims_mom_chairperson_user_code", obj.sims_mom_chairperson_user_code),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, res);

                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        #endregion

        #region APPROVE MEETING

        [Route("getMeetings")]
        public HttpResponseMessage getMeetings(string fdate,string sdate,string requsrcode,string subject)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMeetings(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMeetings"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_mom_from_date", db.DBYYYYMMDDformat(fdate)),
                              new SqlParameter("@sims_mom_to_date",db.DBYYYYMMDDformat(sdate)),
                              new SqlParameter("@sims_mom_requester_user_code", requsrcode),
                              new SqlParameter("@sims_mom_subject", subject)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date = db.UIDDMMYYYYformat(dr["sims_mom_date"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_start_time"].ToString()))
                            {
                                obj.sims_mom_start_time =DateTime.Parse (dr["sims_mom_start_time"].ToString()).ToShortTimeString();
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_end_time"].ToString()))
                            {
                                obj.sims_mom_end_time = DateTime.Parse(dr["sims_mom_end_time"].ToString()).ToShortTimeString();
                            }
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_requester_user_name = dr["em_first_name"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getAttendees")]
        public HttpResponseMessage getMeeting(string sims_mom_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAttendees"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "D"),
                              new SqlParameter("@sims_mom_number", sims_mom_number),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_receipient_user_code"].ToString();
                            obj.sims_mom_attendees_name = dr["attendees name"].ToString();
                            obj.meeting_conductor = dr["meeting_conductor"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getSubjects")]
        public HttpResponseMessage getSubjects()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjects(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getSubjects"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "E")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getRequester2")]
        public HttpResponseMessage getRequester2()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequester2(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getRequester2"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "R")

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.em_number = dr["sims_mom_requester_user_code"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getApproveAccess")]
        public HttpResponseMessage getApproveAccess(string empno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getApproveAccess(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getApproveAccess"));

            List<meetingsclass> list = new List<meetingsclass>();
            bool res=false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting_approve]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@employee_code", empno)
                         });

                    if (dr.HasRows)
                    {
                        res = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
        }

        #endregion

        #region VIEW MEETING

        [Route("getScheduleMeeting")]
        public HttpResponseMessage getScheduleMeeting(string empcode,string momstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getScheduleMeeting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getScheduleMeeting"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "V"),
                              new SqlParameter("@employee_code", empcode),
                               new SqlParameter("@sims_mom_status",momstatus)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date = db.UIDDMMYYYYformat(dr["sims_mom_date"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_start_time"].ToString()))
                            {
                                obj.sims_mom_start_time =DateTime.Parse(dr["sims_mom_start_time"].ToString()).ToShortTimeString();
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_end_time"].ToString()))
                            {
                                obj.sims_mom_end_time = DateTime.Parse( dr["sims_mom_end_time"].ToString()).ToShortTimeString();
                            }
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_requester_user_name = dr["em_first_name"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();

                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("UpdateResponse")]
        public HttpResponseMessage UpdateResponse(meetingsclass obj)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj != null)
                {
                   
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@employee_code", obj.employee_code),
                                new SqlParameter("@sims_response_flag", obj.sims_response_flag),
                                new SqlParameter("@sims_mom_number", obj.sims_mom_number),
                                new SqlParameter("@sims_mom_receipient_response_remark", obj.sims_mom_receipient_response_remark)
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();


                        }
                    
                    return Request.CreateResponse(HttpStatusCode.OK, res);

                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                //res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion

        #region MINUTES OF MEETING

        [Route("getMeetings")]
        public HttpResponseMessage getMinutesOfMeeting(string empcode,string momstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMinutesOfMeeting(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMinutesOfMeeting"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "P"),
                              new SqlParameter("@employee_code", empcode),
                              new SqlParameter("@sims_mom_status", momstatus)

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            obj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            obj.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            obj.sims_mom_venue = dr["sims_mom_venue"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_mom_date"].ToString()))
                            {
                                obj.sims_mom_date = db.UIDDMMYYYYformat(dr["sims_mom_date"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_start_time"].ToString()))
                            {
                                obj.sims_mom_start_time = DateTime.Parse(dr["sims_mom_start_time"].ToString()).ToShortTimeString();
                            }
                            if (!string.IsNullOrEmpty(dr["sims_mom_end_time"].ToString()))
                            {
                                obj.sims_mom_end_time = DateTime.Parse(dr["sims_mom_end_time"].ToString()).ToShortTimeString();
                            }
                            obj.sims_mom_chairperson_user_code = dr["sims_mom_chairperson_user_code"].ToString();
                            obj.sims_mom_recorder_user_code = dr["sims_mom_recorder_user_code"].ToString();
                            obj.sims_mom_requester_user_code = dr["sims_mom_requester_user_code"].ToString();
                            obj.sims_mom_approver_user_code = dr["sims_mom_approver_user_code"].ToString();
                            obj.sims_mom_agenda = dr["sims_mom_agenda"].ToString();
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();
                            obj.sims_mom_communication_status = dr["sims_mom_communication_status"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("CUDMarkAttendance")]
        public HttpResponseMessage CUDMarkAttendance(List<meetingsclass> obj1)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj1 != null)
                {
                    foreach (meetingsclass obj in obj1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_number", obj.sims_mom_number),                                
                                new SqlParameter("@sims_mom_receipient_attendance", obj.sims_mom_receipient_attendance==true?"P":"A"),                                
                                new SqlParameter("@sims_mom_chairperson_user_code", obj.sims_mom_chairperson_user_code),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, res);

                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CUDInsertMinMeeting")]
        public HttpResponseMessage CUDInsertMinMeeting(meetingsclass obj)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (obj != null)
                {
                   // foreach (meetingsclass obj in obj1)
                    //{
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sims_mom_number", obj.sims_mom_number), 
                                new SqlParameter("@sims_mom_srl_no", obj.sims_mom_srl_no), 
                                new SqlParameter("@sims_mom_type_code", obj.sims_mom_type_code),
                              //  new SqlParameter("@sims_mom_srl_no", obj.sims_mom_srl_no),
                                new SqlParameter("@sims_mom_description", obj.sims_mom_description),
                                new SqlParameter("@sims_mom_raised_user_code", obj.sims_mom_raised_user_code),
                                new SqlParameter("@sims_mom_tobedone_user_code", obj.sims_mom_tobedone_user_code),
                                new SqlParameter("@sims_mom_tobedone_date", db.DBYYYYMMDDformat(obj.sims_mom_tobedone_date)),
                                new SqlParameter("@sims_mom_actual_start_date",db.DBYYYYMMDDformat(obj.sims_mom_actual_start_date)),
                                new SqlParameter("@sims_mom_actual_start_time", obj.sims_mom_actual_start_time),
                                new SqlParameter("@sims_mom_actual_end_time", obj.sims_mom_actual_end_time),
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();


                        }
                    //}
                    return Request.CreateResponse(HttpStatusCode.OK, res);

                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                //res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("getMeetingsPoints")]
        public HttpResponseMessage getMeetingsPoints(string momnumber)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMeetingsPoints(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getMeetingsPoints"));

            List<meetingsclass> list = new List<meetingsclass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_minutes_of_meeting]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "Y"),
                              new SqlParameter("@sims_mom_number", momnumber)
                          });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            meetingsclass obj = new meetingsclass();
                            obj.sims_mom_number = dr["sims_mom_number"].ToString();
                            obj.sims_mom_srl_no = dr["sims_mom_srl_no"].ToString();
                            obj.sims_mom_type_code = dr["sims_mom_type"].ToString();
                            obj.sims_mom_srl_no = dr["sims_mom_srl_no"].ToString();
                            obj.sims_mom_description = dr["sims_mom_description"].ToString();
                            obj.sims_mom_raised_user_code = dr["sims_mom_raised_user_code"].ToString();
                            obj.sims_mom_tobedone_user_code = dr["sims_mom_tobedone_user_code"].ToString();
                            obj.sims_mom_raised_user_name = dr["sims_mom_raised_user_name"].ToString();
                            obj.sims_mom_tobedone_user_name = dr["sims_mom_tobedone_user_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_mom_tobedone_date"].ToString()))
                            {
                                obj.sims_mom_date = db.UIDDMMYYYYformat(dr["sims_mom_tobedone_date"].ToString());
                            }
                            obj.sims_mom_status = dr["sims_mom_status"].ToString();

                            list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        #endregion

    }
}