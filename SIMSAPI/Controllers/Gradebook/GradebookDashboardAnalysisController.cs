﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/gradebookanalysisreport")]
    public class GradebookDashboardAnalysisController : ApiController
    {

        [Route("getgradebookcurrentdata")]
        public HttpResponseMessage getgradebookcurrentdata(string opr, string curCode, string acaYear, string grade_code, string section_code,string term_code, string subject_code,
            string nationality,string gender,string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebookoneprevdata")]
        public HttpResponseMessage getgradebookoneprevdata(string opr, string curCode, string acaYear, string grade_code, string section_code,string term_code, string subject_code,
             string nationality, string gender,string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                         new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebooktwoprevdata")]
        public HttpResponseMessage getgradebooktwoprevdata(string opr, string curCode, string acaYear, string grade_code, string section_code,string term_code, string subject_code,
             string nationality, string gender,string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                           new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getgradebookcurrentdataterm1")]
        public HttpResponseMessage getgradebookcurrentdataterm1(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
           string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebookoneprevdataterm1")]
        public HttpResponseMessage getgradebookoneprevdataterm1(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
            string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                         new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebooktwoprevdataterm1")]
        public HttpResponseMessage getgradebooktwoprevdataterm1(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
             string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                           new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getgradebookcurrentdataterm2")]
        public HttpResponseMessage getgradebookcurrentdataterm2(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
          string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebookoneprevdataterm2")]
        public HttpResponseMessage getgradebookoneprevdataterm2(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
            string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                         new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebooktwoprevdataterm2")]
        public HttpResponseMessage getgradebooktwoprevdataterm2(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
             string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                           new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebookcurrentdataterm3")]
        public HttpResponseMessage getgradebookcurrentdataterm3(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
          string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebookoneprevdataterm3")]
        public HttpResponseMessage getgradebookoneprevdataterm3(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
            string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                            new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                         new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getgradebooktwoprevdataterm3")]
        public HttpResponseMessage getgradebooktwoprevdataterm3(string opr, string curCode, string acaYear, string grade_code, string section_code, string term_code, string subject_code,
             string nationality, string gender, string sen)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@subject_code", subject_code),
                           new SqlParameter("@term", term_code),
                           new SqlParameter("@nationality", nationality),
                           new SqlParameter("@gender", gender),
                           new SqlParameter("@sen", sen)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.average = dr["average"].ToString();
                            simsobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.total_students = dr["total_students"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }




        [Route("getsubjectdata")]
        public HttpResponseMessage getsubjectdata(string curCode, string acaYear, string grade_code, string section_code,string group)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "H"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),
                           new SqlParameter("@section", section_code),
                           new SqlParameter("@group", group),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllTerm")]
        public HttpResponseMessage getAllTerm(string curCode, string acaYear)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "I"),
                               new SqlParameter("@cur_code", curCode),
                               new SqlParameter("@acad_year", acaYear)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string curCode, string acaYear)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "J"),
                               new SqlParameter("@cur_code", curCode),
                               new SqlParameter("@acad_year", acaYear)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string curCode, string acaYear, string grade_code)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "K"),
                            new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                            new SqlParameter("@grade_code", grade_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllnationality")]
        public HttpResponseMessage getAllnationality()
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "L"),
                               

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_nationality_code = dr["sims_nationality_code"].ToString();
                            simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllgender")]
        public HttpResponseMessage getAllgender()
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "M"),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllsubgroup")]
        public HttpResponseMessage getAllsubgroup(string curCode)
        {
            List<GDA001> list = new List<GDA001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_GradebookDashboardAnalysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "N"),
                            new SqlParameter("@cur_code", curCode),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GDA001 simsobj = new GDA001();
                            simsobj.sims_subject_group_code = dr["sims_subject_group_code"].ToString();
                            simsobj.sims_subject_group_name_en = dr["sims_subject_group_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



    }

}