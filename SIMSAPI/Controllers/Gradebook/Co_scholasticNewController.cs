﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Helper;
using System.Net;
using System.Data;
using System;
using SIMSAPI.Models.ERP.gradebookClass;
using System.Linq;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/co_scholastic")]
    public class Co_scholasticNewController : ApiController
    {

        [Route("get_co_scholasticNewCommon")]
        public HttpResponseMessage get_co_scholasticNewCommon(string academic_year, string cur_code, string config_code)
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "S"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@aca_year", academic_year),
                         new SqlParameter("@config_code", config_code)
                      });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        [Route("get_group_scale_code")]
        public HttpResponseMessage get_group_scale_code(string academic_year, string cur_code)
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "SC"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@aca_year", academic_year),
                      });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }


        [Route("get_config_details")]
        public HttpResponseMessage get_config_details(string academic_year, string cur_code)
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "E"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@aca_year", academic_year),
                      });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        [Route("CoScholConfDataOpr")]
        public HttpResponseMessage CoScholConfDataOpr(schol_group data)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code),
                    new SqlParameter("@sims_report_card_config_desc", data.sims_report_card_config_desc),
                  //  new SqlParameter("@sims_report_card_config_desc", data.sims_co_scholastic_group_code),
                    new SqlParameter("@entry_status", data.entry_status),
                    
                    new SqlParameter("@sims_report_card_config_status", (data.sims_report_card_config_status==true) ? 'A' : 'I'),
                    //new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code)

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("get_group_score_type")]
        public HttpResponseMessage get_group_score_type()
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "ST")
                      });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        [Route("GetAllCoScholData")]
        public HttpResponseMessage GetAllCoScholData(string config_code, string cur_code, string academic_year)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "G"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code)

               });
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_group_description = dr["sims_co_scholastic_group_desc"].ToString();
                        sg.sims_co_scholastic_group_display_order = dr["sims_co_scholastic_group_display_order"].ToString();
                        sg.sims_co_scholastic_group_heading = dr["sims_co_scholastic_group_heading"].ToString();
                        sg.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                        sg.sims_co_scholastic_group_name_ot = dr["sims_co_scholastic_group_name_ot"].ToString();
                        sg.sims_co_scholastic_group_note = dr["sims_co_scholastic_group_note"].ToString();
                        sg.sims_co_scholastic_group_status = dr["sims_co_scholastic_group_status"].ToString().Equals("A") ? true : false;
                        sg.sims_co_scholastic_group_dispaly_name = dr["sims_co_scholastic_group_name"].ToString();
                        sg.sims_co_scolastic_grading_scale = dr["sims_co_scholastic_grading_scale_code"].ToString();
                        sg.sims_co_scolastic_Score_type = dr["sims_co_scholastic_group_score_type"].ToString();
                        sg.sims_co_scholastic_group_type = dr["sims_co_scholastic_group_type"].ToString();
                        sg.sims_co_scholastic_group_type = dr["sims_co_scholastic_group_type"].ToString();
                        
                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();

                        sg.entry_status = dr["entry_status"].ToString();
                        sg.sims_subject_code = dr["sims_subject_code"].ToString();
                        sg.sims_report_card_config_flag = dr["sims_report_card_config_flag"].ToString();
                        sg.sims_report_card_config_desc = dr["sims_report_card_config_desc"].ToString();
                        sg.sims_report_card_type_desc = dr["sims_report_card_type_desc"].ToString();
                        sg.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                        sg.sims_report_card_scheme_grade_name = dr["sims_report_card_scheme_grade_name"].ToString();

                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CoScholGroupDataOpr")]
        public HttpResponseMessage CoScholGroupDataOpr(schol_group data)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_group_name", data.sims_co_scholastic_group_name),
                    new SqlParameter("@sims_co_scholastic_group_name_ot", data.sims_co_scholastic_group_name_ot),
                    new SqlParameter("@sims_co_scholastic_group_description", data.sims_co_scholastic_group_description),
                    new SqlParameter("@sims_co_scholastic_group_heading", data.sims_co_scholastic_group_heading),
                    new SqlParameter("@sims_co_scholastic_group_note", data.sims_co_scholastic_group_note),
                    new SqlParameter("@sims_co_scholastic_group_display_order", data.sims_co_scholastic_group_display_order),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_type", data.sims_co_scholastic_group_type),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_group_status", (data.sims_co_scholastic_group_status==true) ? 'A' : 'I'),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code)

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllGroupAttriData")]
        public HttpResponseMessage GetAllGroupAttriData(string config_code, string cur_code, string academic_year)
        {
            List<schol_Attribute> lst = new List<schol_Attribute>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code)

               });
                    while (dr.Read())
                    {
                        schol_Attribute sg = new schol_Attribute();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        sg.sims_co_scholastic_attribute_display_order = dr["sims_co_scholastic_attribute_display_order"].ToString();
                        sg.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                        sg.sims_co_scholastic_attribute_name_ot = dr["sims_co_scholastic_attribute_name_ot"].ToString();
                        sg.sims_co_scholastic_attribute_status = dr["sims_co_scholastic_attribute_status"].ToString().Equals("A") ? true : false;
                        sg.sims_co_scholastic_group_grade_scale_code = dr["sims_co_scholastic_grading_scale_code"].ToString();
                        sg.sims_co_scholastic_attribute_dispaly_name = dr["sims_co_scholastic_attribute_display_name"].ToString();

                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        sg.entry_status = dr["entry_status"].ToString();
                        sg.sims_report_card_scheme_grade_name = dr["sims_report_card_scheme_grade_name"].ToString();

                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("GetAllGroupAttriDataCmbo")]
        public HttpResponseMessage GetAllGroupAttriDataCmbo(string config_code, string cur_code, string academic_year)
        {
            List<schol_Attribute> lst = new List<schol_Attribute>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "B"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code)

               });
                    while (dr.Read())
                    {
                        schol_Attribute sg = new schol_Attribute();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_master_code"].ToString();
                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        sg.sims_co_scholastic_attribute_dispaly_name = dr["sims_co_scholastic_attribute_dispaly_name"].ToString();
                       
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

 


        [Route("CoScholAttributeDataOpr")]
        public HttpResponseMessage CoScholAttributeDataOpr(List<schol_Attribute> data1)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (schol_Attribute data in data1)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                             {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_attribute_name", data.sims_co_scholastic_attribute_name),
                    new SqlParameter("@sims_co_scholastic_attribute_name_ot", data.sims_co_scholastic_attribute_name_ot),
                    new SqlParameter("@sims_co_scholastic_grading_scale_code", data.sims_co_scholastic_group_grade_scale_code),
                    new SqlParameter("@sims_co_scholastic_attribute_display_order", data.sims_co_scholastic_attribute_display_order),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code),
                    new SqlParameter("@entry_status", data.entry_status),

                    new SqlParameter("@sims_co_scholastic_attribute_status", (data.sims_co_scholastic_attribute_status==true) ? 'A' : 'I'),
                    });
                        if (dr > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllGroupIndicData")]
        public HttpResponseMessage GetAllGroupIndicData(string config_code, string cur_code, string academic_year)
        {
            List<DescIndicator> lst = new List<DescIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "IN"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code)

               });
                    while (dr.Read())
                    {
                        DescIndicator sg = new DescIndicator();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_code = dr["sims_co_scholastic_descriptive_indicator_code"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_name = dr["sims_co_scholastic_descriptive_indicator_name"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_name_ot = dr["sims_co_scholastic_descriptive_indicator_name_ot"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_status = dr["sims_co_scholastic_descriptive_indicator_status"].ToString().Equals("A") ? true : false;
                        sg.sims_co_scholastic_descriptive_indicator_display_order = dr["sims_co_scholastic_descriptive_indicator_display_order"].ToString();
                        sg.sims_co_scolastic_grading_scale = dr["sims_co_scholastic_grading_scale_code"].ToString();
                        sg.sims_co_scolastic_Score_type = dr["sims_co_scholastic_group_score_type"].ToString();
                        sg.sims_co_scholastic_multiple_teacher = dr["sims_co_scholastic_multiple_teacher"].ToString().Equals("T") ? true : false;
                        sg.sims_co_scholastic_overall_result = dr["sims_co_scholastic_overall_result"].ToString();

                        sg.sims_co_scholastic_descriptive_indicator_start_date = db.UIDDMMYYYYformat( dr["sims_co_scholastic_descriptive_indicator_start_date"].ToString());
                        sg.sims_co_scholastic_descriptive_indicator_end_date =db.UIDDMMYYYYformat( dr["sims_co_scholastic_descriptive_indicator_end_date"].ToString());
                        sg.sims_co_scholastic_descriptive_indicator_freeze_date =db.UIDDMMYYYYformat( dr["sims_co_scholastic_descriptive_indicator_freeze_date"].ToString());
                        sg.sims_co_scholastic_descriptive_indicator_type = dr["sims_co_scholastic_descriptive_indicator_type"].ToString();

                        sg.sims_co_scholastic_descriptive_indicator_mark = dr["sims_co_scholastic_descriptive_indicator_mark"].ToString();
                        sg.sims_co_scholastic_descriptive_indicator_grade_scale = dr["sims_co_scholastic_descriptive_indicator_grade_scale"].ToString();
                        sg.entry_status = dr["entry_status"].ToString();

                        
                        sg.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_master_code"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();

                        sg.config_code = sg.sims_report_card_config_code + "/" + sg.sims_co_scholastic_group_code + "/" + sg.sims_co_scholastic_attribute_code;
                        
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CoScholIndicatorDataOpr")]
        public HttpResponseMessage CoScholIndicatorDataOpr(DescIndicator data)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_code",data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name", data.sims_co_scholastic_discriptive_indicator_name),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name_ot", data.sims_co_scholastic_discriptive_indicator_name_ot),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_multiple_teacher" ,(data.sims_co_scholastic_multiple_teacher==true) ? 'T' : 'F'),
                    new SqlParameter("@sims_co_scholastic_overall_result"   ,data.sims_co_scholastic_overall_result),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),

                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),

                });
                    if (dr > 0)
                    {
                        inserted = true;
                    }

                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CoScholIndicatorDataOprN")]
        public HttpResponseMessage CoScholIndicatorDataOprN(List<DescIndicator> data1)
        {
            bool inserted = false;
            try
            {
                foreach (DescIndicator data in data1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_code",data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name", data.sims_co_scholastic_discriptive_indicator_name),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name_ot", data.sims_co_scholastic_discriptive_indicator_name_ot),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_multiple_teacher" ,(data.sims_co_scholastic_multiple_teacher==true) ? 'T' : 'F'),
                    new SqlParameter("@sims_co_scholastic_overall_result"   ,data.sims_co_scholastic_overall_result),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),

                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_start_date",db.DBYYYYMMDDformat( data.sims_co_scholastic_descriptive_indicator_start_date)),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_end_date", db.DBYYYYMMDDformat(data.sims_co_scholastic_descriptive_indicator_end_date)),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_freeze_date",db.DBYYYYMMDDformat( data.sims_co_scholastic_descriptive_indicator_freeze_date)),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_type", data.sims_co_scholastic_descriptive_indicator_type),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_mark", data.sims_co_scholastic_descriptive_indicator_mark),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_grade_scale", data.sims_co_scholastic_descriptive_indicator_grade_scale),
                    new SqlParameter("@entry_status", data.entry_status),


                });
                        if (dr > 0)
                        {
                            inserted = true;
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }




        [Route("CUDGroupAttriIndicConfigData")]
        public HttpResponseMessage CUDGroupAttriIndicConfigData(List<DescIndicator> data1)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DescIndicator data in data1)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                             {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_code",data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_teacher_code",data.sims_co_scholastic_teacher_code),
                    new SqlParameter("@config_code",data.sims_report_card_config_code),
                    new SqlParameter("@sims_config_term_code",data.sims_report_card_term_code),
                    new SqlParameter("@sims_config_assign_number",data.sims_report_card_assign_number),
                    new SqlParameter("@sims_config_assign_max_score",data.sims_report_card_assign_max_score),
                    new SqlParameter("@sims_co_scholastic_grading_scale_code",data.sims_co_scholastic_grading_scale_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),
                    });
                        if (dr > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllGroupIndicconfigData")]
        public HttpResponseMessage GetAllGroupIndicconfigData(string config_code, string cur_code, string academic_year)
        {
            List<DescIndicator> lst = new List<DescIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "IC"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code),

               });
                    while (dr.Read())
                    {
                        DescIndicator sg = new DescIndicator();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_code = dr["sims_co_scholastic_descriptive_indicator_code"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_name = dr["sims_co_scholastic_descriptive_indicator_name"].ToString();
                        sg.sims_co_scholastic_attribute_dispaly_name = dr["sims_co_scholastic_descriptive_indicator_display_name"].ToString();
                        sg.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                        sg.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                        sg.sims_config_assign_number = dr["sims_config_assign_number"].ToString();
                        sg.sims_co_scholastic_discriptive_indicator_status = dr["sims_co_scholastic_descriptive_indicator_status"].ToString().Equals("A") ? true : false;
                        sg.sims_grade_code = dr["sims_grade_code"].ToString();
                        sg.sims_section_code = dr["sims_section_code"].ToString();
                        sg.sims_section_name = dr["sims_section_name_en"].ToString();
                        sg.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        sg.sims_co_scholastic_teacher_code = dr["sims_co_scholastic_teacher_code"].ToString();

                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetAllGrade")]
        public HttpResponseMessage GetAllGrade(string config_code, string cur_code, string academic_year)
        {
            List<DescIndicator> lst = new List<DescIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CG"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code),

               });
                    while (dr.Read())
                    {
                        DescIndicator sg = new DescIndicator();
                        sg.sims_grade_code = dr["sims_grade_code"].ToString();
                        sg.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetAllGradeSection")]
        public HttpResponseMessage GetAllGradeSection(string config_code, string cur_code, string academic_year, string grade_code)
        {
            List<DescIndicator> lst = new List<DescIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CS"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code),
                new SqlParameter("@sims_grade_code",grade_code),

               });
                    while (dr.Read())
                    {
                        DescIndicator sg = new DescIndicator();
                        sg.sims_section_code = dr["sims_section_code"].ToString();
                        sg.sims_section_name = dr["sims_section_name_en"].ToString();
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetAllresultType")]
        public HttpResponseMessage GetAllresultType()
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "OR"),

                      });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }


        [Route("GetAllTeachersName")]
        public HttpResponseMessage GetAllTeachersName(string grade_code, string section_code, string cur_code, string academic_year)
        {
            object o = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "TN"),
                         new SqlParameter("@sims_grade_code", grade_code),
                         new SqlParameter("@sims_section_code",section_code),
                         new SqlParameter("@sims_cur_code", cur_code),
                         new SqlParameter("@sims_academic_year", academic_year)
                         });
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, o);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }



        [Route("CoScholGroupDataOpr1")]
        public HttpResponseMessage CoScholGroupDataOpr1(schol_group data)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_group_name", data.sims_co_scholastic_group_name),
                    new SqlParameter("@sims_co_scholastic_group_name_ot", data.sims_co_scholastic_group_name_ot),
                    new SqlParameter("@sims_co_scholastic_group_description", data.sims_co_scholastic_group_description),
                    new SqlParameter("@sims_co_scholastic_group_heading", data.sims_co_scholastic_group_heading),
                    new SqlParameter("@sims_co_scholastic_group_note", data.sims_co_scholastic_group_note),
                    new SqlParameter("@sims_co_scholastic_group_display_order", data.sims_co_scholastic_group_display_order),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_type", data.sims_co_scholastic_group_type),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_group_status", (data.sims_co_scholastic_group_status==true) ? 'A' : 'I'),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code)

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CoScholIndicatorDataOpr1")]
        public HttpResponseMessage CoScholIndicatorDataOpr1(List<DescIndicator> data1)
        {
            bool inserted = false;
            try
            {
                foreach (DescIndicator data in data1)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_code",data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name", data.sims_co_scholastic_discriptive_indicator_name),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name_ot", data.sims_co_scholastic_discriptive_indicator_name_ot),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_multiple_teacher" ,(data.sims_co_scholastic_multiple_teacher==true) ? 'T' : 'F'),
                    new SqlParameter("@sims_co_scholastic_overall_result"   ,data.sims_co_scholastic_overall_result),
                    new SqlParameter("@sims_co_scholastic_descriptive_indicator_display_order", data.sims_co_scholastic_descriptive_indicator_display_order),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),
                });
                        if (dr > 0)
                        {
                            inserted = true;
                        }

                    }
                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("GetAllCoScholData1")]
        public HttpResponseMessage GetAllCoScholData1(string config_code, string cur_code, string academic_year ,string term)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "GSC"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
                new SqlParameter("@sims_report_card_config_code",config_code),
                new SqlParameter("@term_code",term)

               });
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                       
                        sg.sims_co_scholastic_group_display_order = dr["sims_co_scholastic_group_display_order"].ToString();
                       
                        sg.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                     
                        sg.sims_co_scholastic_group_status = dr["sims_co_scholastic_group_status"].ToString().Equals("A") ? true : false;
                     
                        sg.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();

                        sg.sims_co_scholastic_group_heading = dr["sims_report_card_term_code"].ToString();

                        sg.sims_co_scholastic_group_note = dr["sims_report_card_term_flag"].ToString();

                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("CoScholGroupDataOprN")]
        public HttpResponseMessage CoScholGroupDataOprN(List<schol_group> data1)
        {
            bool inserted = false;
            try
            {
                foreach (schol_group data in data1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_group_name", data.sims_co_scholastic_group_name),
                    new SqlParameter("@sims_co_scholastic_group_name_ot", data.sims_co_scholastic_group_name_ot),
                    new SqlParameter("@sims_co_scholastic_group_description", data.sims_co_scholastic_group_description),
                    new SqlParameter("@sims_co_scholastic_group_heading", data.sims_co_scholastic_group_heading),
                    new SqlParameter("@sims_co_scholastic_group_note", data.sims_co_scholastic_group_note),
                    new SqlParameter("@sims_co_scholastic_group_display_order", data.sims_co_scholastic_group_display_order),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_co_scholastic_group_type", data.sims_co_scholastic_group_type),
                    new SqlParameter("@sims_co_scholastic_group_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_group_status", (data.sims_co_scholastic_group_status==true) ? 'A' : 'I'),
                    new SqlParameter("@sims_report_card_config_code", data.sims_report_card_config_code),
                    new SqlParameter("@sims_subject_code", data.sims_subject_code),
                    new SqlParameter("@sims_report_card_config_flag", data.sims_report_card_config_flag),
                    new SqlParameter("@entry_status", data.entry_status)


                });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CoScholAttributeDataOprN")]
        public HttpResponseMessage CoScholAttributeDataOprN(List<schol_Attribute> data1)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (schol_Attribute data in data1)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            new List<SqlParameter>()
                             {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_attribute_name", data.sims_co_scholastic_attribute_name),
                    new SqlParameter("@sims_co_scholastic_attribute_name_ot", data.sims_co_scholastic_attribute_name_ot),
                    new SqlParameter("@sims_co_scholastic_grading_scale_code", data.sims_co_scholastic_group_grade_scale_code),
                    new SqlParameter("@sims_co_scholastic_attribute_display_order", data.sims_co_scholastic_attribute_display_order),
                    new SqlParameter("@sims_co_scholastic_attribute_status", (data.sims_co_scholastic_attribute_status==true) ? 'A' : 'I'),
                    
                    new SqlParameter("@config_code", data.sims_report_card_config_code),

                    new SqlParameter("@term_code", data.term_code),
                    new SqlParameter("@flag", data.flag),

                    });
                        if (dr > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("GetAllSubject")]
        public HttpResponseMessage GetAllSubject( string cur_code)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SS"),
                new SqlParameter("@CURR",cur_code),
               
               });
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.sims_subject_code = dr["sims_subject_code"].ToString();
                        sg.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                       
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }



        [Route("Getgrade_scale")]
        public HttpResponseMessage Getgrade_scale(string cur_code, string academic_year)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C"),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year),
               
               });
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.sims_co_scholastic_grade_scale_code = dr["sims_co_scholastic_grade_scale_code"].ToString();
                        sg.sims_co_scholastic_grade_scale_description = dr["sims_co_scholastic_grade_scale_description"].ToString();

                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("ImportSavedConfigSecData")]
        public HttpResponseMessage ImportSavedConfigSecData(String cur_code, String aca_year, String config_code)
        {
            List<GradeSecObj> mod_list = new List<GradeSecObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]", new List<SqlParameter>() {
                        new SqlParameter("@opr","AB"),
                        new SqlParameter("@cur_code",cur_code),
                        new SqlParameter("@aca_year",aca_year),
                        new SqlParameter("@config_code",config_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            GradeSecObj simsobj = new GradeSecObj();
                            str1 = dr["sims_grade_code"].ToString();
                            var v = from p in mod_list where p.grade_code == str1 select p;

                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();

                            simsobj.section = new List<SectionObj>();

                            SectionObj h = new SectionObj();

                            h.sims_section_code = dr["sims_section_code"].ToString();
                            h.sims_section_name = dr["sims_section_name_en"].ToString();
                            h.status = dr["section_status"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.section.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).section.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        } // End of importSavedConfigSecData


        #region CoscholasticConfigSave
        [Route("CoscholasticConfigSave")]
        public HttpResponseMessage CoscholasticConfigSave(string cTemp, List<gradeBookNewGradeSec> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradeBookNewGradeSec simsobj in data)
                    {
                        //foreach (SectionObj obj in simsobj.section)
                        //{
                        using (DBConnection db1 = new DBConnection())
                        {
                            //int ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_config_proc]",
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_config_proc]",

                            new List<SqlParameter>()
                            {
                                  new SqlParameter("@opr",cTemp),
                                  new SqlParameter("@aca_year",simsobj.aca_year),
                                  new SqlParameter("@cur_code",simsobj.cur_code),
                                  new SqlParameter("@config_code",simsobj.config_code),
                                  new SqlParameter("@sims_report_card_frequncy",simsobj.sims_report_card_frequncy),
                                  new SqlParameter("@sims_report_card_frequncy_flag",simsobj.sims_report_card_frequncy_flag),
                                  new SqlParameter("@grade_code",simsobj.grade_code),
                                  new SqlParameter("@section_code",simsobj.sims_section_code),
                                  new SqlParameter("@section_status",simsobj.status)
                            });

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            dr.Close();
                        }
                        // }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

    }
}