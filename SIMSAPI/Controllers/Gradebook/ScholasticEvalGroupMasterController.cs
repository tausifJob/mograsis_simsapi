﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/ScholasticGroupMaster")]
    public class ScholasticEvalGroupMasterController : ApiController
    {
        // GET: ScholasticGroupMaster
        [Route("GradeWith_Para")]
        public HttpResponseMessage GradeWith_Para(Sims028 pa)
        {
            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr",'G'),
                        new SqlParameter("@sims_cur_code", pa.sims_cur_code),
                        new SqlParameter("@sims_academic_year", pa.sims_academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("GetScholasticMasterGrade")]
        public HttpResponseMessage GetScholasticMasterGrade()
        {
            List<scholasticEvalGroupMaster> mod_list = new List<scholasticEvalGroupMaster>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            scholasticEvalGroupMaster simsobj = new scholasticEvalGroupMaster();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_scholastic_evaluation_group_code = dr["sims_scholastic_evaluation_group_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetALLScholasticGroupMaster")]
        public HttpResponseMessage GetALLScholasticGroupMaster()
        {
            List<scholasticEvalGroupMaster> mod_list = new List<scholasticEvalGroupMaster>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","A")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            scholasticEvalGroupMaster simsobj = new scholasticEvalGroupMaster();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_scholastic_evaluation_group_desc = dr["sims_scholastic_evaluation_group_desc"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_scholastic_evaluation_group_code = dr["sims_scholastic_evaluation_group_code"].ToString();
                            simsobj.sims_scholastic_evaluation_group_status = dr["sims_scholastic_evaluation_group_status"].ToString().Equals("A") ? true : false;
                            if (int.Parse(dr["cnt_group"].ToString()) > 0)
                                simsobj.update_status = true;
                            else
                                simsobj.update_status = false;

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("AddScholasticGroupMaster")]
        public HttpResponseMessage AddScholasticGroupMaster(string data1, List<scholasticEvalGroupMaster> data)
        {
            bool alert_status = false;
            string group_code = string.Empty;

            scholasticEvalGroupMaster groupObj = Newtonsoft.Json.JsonConvert.DeserializeObject<scholasticEvalGroupMaster>(data1);
            try
            {
                if (groupObj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@OPR",'I'),
                                new SqlParameter("@sims_cur_code", groupObj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", groupObj.sims_academic_year),
                                new SqlParameter("@sims_scholastic_evaluation_group_desc", groupObj.sims_scholastic_evaluation_group_desc),
                                new SqlParameter("@sims_status", groupObj.sims_scholastic_evaluation_group_status.Equals(true)?"A":"I")
                             });
                        if (dr.Read())
                        {
                            group_code = dr["group_code"].ToString();
                        }
                        //else
                        //{
                        //    alert_status = false;
                        //}
                        dr.Close();

                        foreach (scholasticEvalGroupMaster obj in data)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@OPR",'M'),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@groupCode", group_code),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_status", obj.sims_scholastic_evaluation_group_status.Equals(true)?"A":"I")
                             });
                            if (dr1.RecordsAffected > 0)
                            {
                                alert_status = true;
                            }
                            else
                            {
                                alert_status = false;
                            }
                            dr1.Close();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

        [Route("GradecodefromScholasticGradeDetails")]
        public HttpResponseMessage GradecodefromScholasticGradeDetails(scholasticEvalGroupMaster pa)
        {
            List<scholasticEvalGroupMaster> grade_list = new List<scholasticEvalGroupMaster>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr",'N'),
                        new SqlParameter("@sims_scholastic_evaluation_group_code", pa.sims_scholastic_evaluation_group_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            scholasticEvalGroupMaster simsobj = new scholasticEvalGroupMaster();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_scholastic_evaluation_group_code = dr["sims_scholastic_evaluation_group_code"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("UpdateScholasticGroupMaster")]
        public HttpResponseMessage UpdateScholasticGroupMaster(string data1, List<scholasticEvalGroupMaster> data)
        {
            bool alert_status = false;
            string group_code = string.Empty;

            scholasticEvalGroupMaster groupObj = Newtonsoft.Json.JsonConvert.DeserializeObject<scholasticEvalGroupMaster>(data1);
            try
            {
                if (groupObj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@OPR",'U'),
                                new SqlParameter("@sims_cur_code", groupObj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", groupObj.sims_academic_year),
                                new SqlParameter("@sims_scholastic_evaluation_group_desc", groupObj.sims_scholastic_evaluation_group_desc),
                                new SqlParameter("@sims_status", groupObj.sims_scholastic_evaluation_group_status.Equals(true)?"A":"I"),
                                new SqlParameter("@sims_scholastic_evaluation_group_code", groupObj.sims_scholastic_evaluation_group_code)
                             });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();

                        foreach (scholasticEvalGroupMaster obj in data)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[scholastic_eval_group_master_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@OPR",'O'),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_scholastic_evaluation_group_code", obj.sims_scholastic_evaluation_group_code),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_status", obj.sims_scholastic_evaluation_group_status.Equals(true)?"A":"I")
                             });
                            if (dr1.RecordsAffected > 0)
                            {
                                alert_status = true;
                            }
                            else
                            {
                                alert_status = false;
                            }
                            dr1.Close();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }
    }
}