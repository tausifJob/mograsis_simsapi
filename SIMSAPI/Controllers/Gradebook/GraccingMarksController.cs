﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/common/getgracingmarks")]
    [BasicAuthentication]
         public class GraccingMarksController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetMedicineType")]
        public HttpResponseMessage GetMedicineType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_medicine",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "A"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();
                            simsobj.sims_medicine_typename = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_medicine_type = dr["sims_appl_parameter"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims231> grade_list = new List<Sims231>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims231 simsobj = new Sims231();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }


        [Route("GetSims_Grace_Marks")]
        public HttpResponseMessage GetSims_Grace_Marks(string sims_activity_number)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_MedicalMedicine(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSims_MedicalMedicine"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sims_activity_number", sims_activity_number),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_class_name = dr["sims_class_name"].ToString();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_Prefrred_teacher_code = dr["sims_activity_Prefrred_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                           // simsobj.sims_activity_status = dr["sims_activity_status"].ToString();
                            if (dr["sims_activity_status"].ToString().Equals("Y"))
                            {
                                simsobj.sims_activity_status = true;
                            }
                            else
                            {
                                simsobj.sims_activity_status = false;
                            }
                           // new SqlParameter("@sims_act_status", simsobj.sims_activity_status.Equals(true)?"Y":"N"),
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_gracing_type = dr["sims_appl_parameter"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_Grace_Marks")]
        public HttpResponseMessage Get_Grace_Marks()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_MedicalMedicine(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSims_MedicalMedicine"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", "Z"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            //simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //simsobj.sims_section_code = dr["sims_section_code"].ToString();
                           // simsobj.sims_class_name = dr["sims_class_name"].ToString();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_Prefrred_teacher_code = dr["sims_activity_Prefrred_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            // simsobj.sims_activity_status = dr["sims_activity_status"].ToString();
                            if (dr["sims_activity_status"].ToString().Equals("Y"))
                            {
                                simsobj.sims_activity_status = true;
                            }
                            else
                            {
                                simsobj.sims_activity_status = false;
                            }
                            // new SqlParameter("@sims_act_status", simsobj.sims_activity_status.Equals(true)?"Y":"N"),
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_gracing_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_activity_symbol = dr["sims_activity_symbol"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateSims_Grace_Marks")]
        public HttpResponseMessage InsertUpdateSims_Grace_Marks(Sims231 simsobj)    
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_grace_type", simsobj.sims_gracing_type),
                            new SqlParameter("@sims_act_number", simsobj.sims_activity_number),
                            new SqlParameter("@sims_act_name", simsobj.sims_activity_name),
                            new SqlParameter("@sims_act_symbol", simsobj.sims_activity_symbol),
                            new SqlParameter("@sims_act_max_pt",simsobj.sims_activity_max_point),
                            new SqlParameter("@sims_act_pt_subject",simsobj.sims_activity_max_point_subject),
                            new SqlParameter("@sims_act_priority", simsobj.sims_activity_priority),
                            new SqlParameter("@sims_act_preferred_teacher", simsobj.sims_activity_Prefrred_teacher_code),
                            //new SqlParameter("@sims_act_status", simsobj.sims_activity_status),
                            new SqlParameter("@sims_act_status", simsobj.sims_activity_status.Equals(true)?"Y":"N"),


                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("UpdateNewSims_Grace_Marks")]
        public HttpResponseMessage UpdateNewSims_Grace_Marks(Sims231 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_leaveSlip()";
            //Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            //new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            //new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            //new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_grace_type", simsobj.sims_gracing_type),
                            new SqlParameter("@sims_act_name", simsobj.sims_activity_name),
                            new SqlParameter("@sims_act_number", simsobj.sims_activity_number),
                            new SqlParameter("@sims_act_symbol", simsobj.sims_activity_symbol),
                            new SqlParameter("@sims_act_max_pt",simsobj.sims_activity_max_point),
                            new SqlParameter("@sims_act_pt_subject",simsobj.sims_activity_max_point_subject),
                            new SqlParameter("@sims_act_priority", simsobj.sims_activity_priority),
                            new SqlParameter("@sims_act_preferred_teacher", simsobj.sims_activity_Prefrred_teacher_code),
                            //new SqlParameter("@sims_act_status", simsobj.sims_activity_status),
                            new SqlParameter("@sims_act_status", simsobj.sims_activity_status.Equals(true)?"Y":"N"),
                            
                        
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getGracetype")]
        public HttpResponseMessage getGracetype()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims231> mod_list = new List<Sims231>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "G"),
                            //new SqlParameter("@sims_cur_code", sims_cur_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims231 simsobj = new Sims231();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getteacher")]
        public HttpResponseMessage getteacher(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims231> mod_list = new List<Sims231>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code", sims_grade_code),
                            new SqlParameter("@sims_section_code", sims_section_code),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims231 simsobj = new Sims231();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }




        [Route("postgetteacher")]
        public HttpResponseMessage postgetteacher(Sims231 itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            List<Sims231> goaltarget_list = new List<Sims231>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", itsr.sims_cur_code),
                            new SqlParameter("@sims_academic_year",itsr.sims_academic_year),
                            new SqlParameter("@sims_grade_code", itsr.sims_grade_code),
                            new SqlParameter("@sims_section_code", itsr.sims_section_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims231 simsobj = new Sims231();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("postgetteacher")]
        public HttpResponseMessage postgetteacher()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            List<Sims231> goaltarget_list = new List<Sims231>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Get_Gracing_Marks_Proc",
                        new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", "T"),
                           
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims231 simsobj = new Sims231();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("dataforDelete")]
        public HttpResponseMessage dataforDelete(List<Sims097> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : dataforDelete()";
            Log.Debug(string.Format(debug, "PP", "dataforDelete"));

            bool inserted = false;
            try
            {
                foreach (Sims097 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims.Get_Gracing_Marks_Proc",
                            new List<SqlParameter>() 
                         { 

	                        new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_grace_type", simsobj.sims_gracing_type),
                            new SqlParameter("@sims_act_number", simsobj.sims_activity_number),
                            new SqlParameter("@sims_act_name", simsobj.sims_activity_name),
                            new SqlParameter("@sims_act_symbol", simsobj.sims_activity_symbol),
                            new SqlParameter("@sims_act_max_pt",simsobj.sims_activity_max_point),
                            new SqlParameter("@sims_act_pt_subject",simsobj.sims_activity_max_point_subject),
                            new SqlParameter("@sims_act_priority", simsobj.sims_activity_priority),
                            new SqlParameter("@sims_act_preferred_teacher", simsobj.sims_activity_Prefrred_teacher_code),
                            //new SqlParameter("@sims_act_status", simsobj.sims_activity_status),
                            //new SqlParameter("@cnt",simsobj.cnt),
                            //new SqlParameter("@section_list",simsobj.section_list),
                            new SqlParameter("@sims_act_status", simsobj.sims_activity_status.Equals(true)?"Y":"N"),

                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

       }

}