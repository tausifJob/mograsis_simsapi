﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
namespace SIMSAPI.Controllers.Gradebook_History
{
     [RoutePrefix("api/Gradebook_History")]
    public class Gradebook_History_Controller : ApiController
    {
        // GET: Gradebook_History_

       //  [Route("CreateHistory")]
       //  public HttpResponseMessage CreateHistory(List<SIMSAPI.Models.ERP.gradebookClass.marks_history> data)
       //  {
       //      bool inserted = false;
       //      try
       //      {
       //          using (DBConnection db = new DBConnection())
       //          {
       //              db.Open();
       //              foreach (marks_history inpt in data)
       //              {

       //                  SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_marks_history]",
       //             new List<SqlParameter>()
       //          {
       //         new SqlParameter("@opr", "I"),
       //          new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
       //          new SqlParameter("@sims_cur_code",inpt.sims_cur_code),
       //          new SqlParameter("@sims_grade_code",inpt.sims_grade_code),
       //          new SqlParameter("@sims_section_code",inpt.sims_section_code),
       //          new SqlParameter("@sims_gb_number",inpt.sims_gb_number),
       //          new SqlParameter("@sims_gb_cat_code",inpt.sims_gb_cat_code),
       //          new SqlParameter("@sims_gb_cat_assign_number",inpt.sims_gb_cat_assign_number),
       //          new SqlParameter("@sims_gb_cat_assign_enroll_number",inpt.sims_gb_cat_assign_enroll_number),
       //          new SqlParameter("@sims_gb_cat_assign_mark",inpt.sims_gb_cat_assign_mark),
       //          new SqlParameter("@sims_gb_cat_assign_final_grade",inpt.sims_gb_cat_assign_final_grade),
       //          new SqlParameter("@sims_gb_cat_assign_max_point",inpt.sims_gb_cat_assign_max_point),
       //          new SqlParameter("@sims_gb_cat_assign_max_correct",inpt.sims_gb_cat_assign_max_correct),
       //          new SqlParameter("@sims_gb_cat_assign_comment",inpt.sims_gb_cat_assign_comment),
       //          new SqlParameter("@sims_gb_cat_assign_status",inpt.sims_gb_cat_assign_status),
       //          new SqlParameter("@sims_gb_updated_by",inpt.sims_gb_updated_by),
       //});
       //                  if (dr.RecordsAffected > 0)
       //                  {
       //                      inserted = true;
       //                  }
       //                  dr.Close();
       //              }


       //          }
       //      }

       //      catch (Exception x)
       //      {
       //          return Request.CreateResponse(HttpStatusCode.OK, x.Message);
       //   }
       //      return Request.CreateResponse(HttpStatusCode.OK, inserted);
       //  }


         [Route("CreateHistory")]
         public HttpResponseMessage CreateHistory(List<marks_history> data)
         {
             SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
             bool insert = false;

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (marks_history inpt in data)
                     {
                         int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_gradebook_marks_history]",

                         new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr", "I"),
                                    new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                                    new SqlParameter("@sims_cur_code",inpt.sims_cur_code),
                                    new SqlParameter("@sims_grade_code",inpt.sims_grade_code),
                                    new SqlParameter("@sims_section_code",inpt.sims_section_code),
                                    new SqlParameter("@sims_gb_number",inpt.sims_gb_number),
                                    new SqlParameter("@sims_gb_cat_code",inpt.sims_gb_cat_code),
                                    new SqlParameter("@sims_gb_cat_assign_number",inpt.sims_gb_cat_assign_number),
                                    new SqlParameter("@sims_gb_cat_assign_enroll_number",inpt.sims_gb_cat_assign_enroll_number),
                                    new SqlParameter("@sims_gb_cat_assign_mark",inpt.sims_gb_cat_assign_mark),
                                    new SqlParameter("@sims_gb_cat_assign_final_grade",inpt.sims_gb_cat_assign_final_grade),
                                    new SqlParameter("@sims_gb_cat_assign_max_point",inpt.sims_gb_cat_assign_max_point),
                                    new SqlParameter("@sims_gb_cat_assign_max_correct",inpt.sims_gb_cat_assign_max_correct),
                                    new SqlParameter("@sims_gb_cat_assign_comment",inpt.sims_gb_cat_assign_comment),
                                    new SqlParameter("@sims_gb_cat_assign_status",inpt.sims_gb_cat_assign_status),
                                    new SqlParameter("@sims_gb_updated_by",inpt.sims_gb_updated_by),
                     });
                         if (ins > 0)
                         {
                             insert = true;

                         }
                         else
                         {
                             insert = false;

                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, insert);
                 }
             }
             catch (Exception x)
             {
             }
             return Request.CreateResponse(HttpStatusCode.OK, insert);
         }


         [Route("getGradeScaleflag")]
         public HttpResponseMessage getGradeScaleflag()
         {

             List<massUpdation> lst = new List<massUpdation>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_cat_assign_grading_scale_Category_assginment_level",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "B"),
                         });
                     while (dr.Read())
                     {
                         massUpdation o = new massUpdation();
                         o.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                         lst.Add(o);
                     }
                 }
             }
             catch (Exception ex)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }

         [Route("getshowrollno")]
         public HttpResponseMessage getshowrollno()
         {

             List<massUpdation> lst = new List<massUpdation>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_cat_assign_grading_scale_Category_assginment_level",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "D"),
                         });
                     while (dr.Read())
                     {
                         massUpdation o = new massUpdation();
                         o.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                         lst.Add(o);
                     }
                 }
             }
             catch (Exception ex)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }


         [Route("getShowGeneralCommentTeacher")]
         public HttpResponseMessage getShowGeneralCommentTeacher(string sims_cur_code,string sims_academic_year,string sims_grade_code,string sims_section_code,string login_user)
         {

             List<massUpdation> lst = new List<massUpdation>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_cat_assign_grading_scale_Category_assginment_level",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "E"),
                            new SqlParameter("@CUR_CODE", sims_cur_code),
                            new SqlParameter("@ACADEMIC_YEAR", sims_academic_year),
                            new SqlParameter("@GRADE_CODE", sims_grade_code),
                            new SqlParameter("@SECTION_CODE", sims_section_code),
                            new SqlParameter("@login_user", login_user),
                         });
                     while (dr.Read())
                     {
                         massUpdation o = new massUpdation();
                         o.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                         lst.Add(o);
                     }
                 }
             }
             catch (Exception ex)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }

         [Route("getGBScaleDataForUpdate")]
         public HttpResponseMessage getGBScaleDataForUpdate(string CUR_CODE, string ACADEMIC_YEAR, string GRADE_CODE, string SECTION_CODE, string GB_NUMBER)
         {

             List<massUpdation> lst = new List<massUpdation>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_cat_assign_grading_scale_Category_assginment_level",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "C"),
                            new SqlParameter("@CUR_CODE", CUR_CODE),
                            new SqlParameter("@ACADEMIC_YEAR", ACADEMIC_YEAR),
                            new SqlParameter("@GRADE_CODE", GRADE_CODE),
                            new SqlParameter("@SECTION_CODE", SECTION_CODE),
                            new SqlParameter("@GB_NUMBER", GB_NUMBER),
                         });
                     while (dr.Read())
                     {
                         massUpdation o = new massUpdation();
                         o.marks_student = dr["marks_student"].ToString();
                         lst.Add(o);
                     }
                 }
             }
             catch (Exception ex)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }
    }
}