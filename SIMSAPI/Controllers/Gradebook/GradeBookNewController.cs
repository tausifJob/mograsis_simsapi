﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.SIMS;
namespace SIMSAPI.Controllers.GradebookNew

{
    [RoutePrefix("api/GradebookNew")]
    public class GradeBookNewController : ApiController
    {
        #region GradeBookNewCommon

        [Route("GradeBookNewCommon")]
        public HttpResponseMessage GradeBookNewCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_report_card_config_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        #region ConfigInsertion
        [Route("ConfigInsertion")]
        public HttpResponseMessage ConfigInsertion(string cTemp, gradeBookNewConfig data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@config_code",data.sims_report_card_config_code),
                          new SqlParameter("@config_desc",data.sims_report_card_config_desc),
                          new SqlParameter("@config_status",data.sims_report_card_config_status)
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            data.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        #endregion

        #region TermInsertion
        [Route("TermInsertion")]
        public HttpResponseMessage TermInsertion(string cTemp, gradeBookNewTerm data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool ins = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@config_code",data.sims_report_card_config_code),
                          new SqlParameter("@term_code",data.sims_report_card_term_code),
                          new SqlParameter("@term_desc",data.sims_report_card_term_desc),
                          new SqlParameter("@term_desc_ot",data.sims_report_card_term_desc_ot),
                          new SqlParameter("@term_st_date",db.DBYYYYMMDDformat(data.sims_report_card_term_st_date)),
                          new SqlParameter("@term_end_date",db.DBYYYYMMDDformat(data.sims_report_card_term_end_date)),
                          new SqlParameter("@grade_scale",data.sims_report_card_term_grade_scale),
                          new SqlParameter("@term_status",data.sims_report_card_term_status),
                          new SqlParameter("@sims_term_display_order",data.sims_term_display_order),
                          new SqlParameter("@sims_report_card_master_term_code",data.sims_report_card_master_term_code),
                          new SqlParameter("@sims_report_card_is_co_scolastic",data.sims_report_card_is_co_scolastic),
                        });
                    if (dr.RecordsAffected > 0) {
                        ins = true;
                        data.inserted_flg = ins;
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        #endregion

        #region GradeSecSave
        [Route("GradeSecSave")]
        public HttpResponseMessage GradeSecSave(string cTemp, List<gradeBookNewGradeSec> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradeBookNewGradeSec simsobj in data)
                    {
                        //foreach (SectionObj obj in simsobj.section)
                        //{
                            using (DBConnection db1 = new DBConnection())
                            {
                                int ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",
                                new List<SqlParameter>()
                                {
                                  new SqlParameter("@opr",cTemp),
                                  new SqlParameter("@aca_year",simsobj.aca_year),
                                  new SqlParameter("@cur_code",simsobj.cur_code),
                                  new SqlParameter("@config_code",simsobj.config_code),
                                  new SqlParameter("@grade_code",simsobj.grade_code),
                                  new SqlParameter("@section_code",simsobj.sims_section_code),
                                  new SqlParameter("@section_status",simsobj.status)
                                });

                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                       // }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region ConfigCategoryInsertion
        [Route("ConfigCategoryInsertion")]
        public HttpResponseMessage ConfigCategoryInsertion(string cTemp, gradeBookNewConfigCategory data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@config_code",data.sims_report_card_config_code),
                          new SqlParameter("@term_code",data.sims_report_card_term_code),
                          new SqlParameter("@term_code_old",data.sims_report_card_term_code_old),
                          new SqlParameter("@cat_code",data.sims_report_card_category_code),
                          new SqlParameter("@cat_st_date",db.DBYYYYMMDDformat(data.sims_report_card_category_st_date)),
                          new SqlParameter("@cat_end_date",db.DBYYYYMMDDformat(data.sims_report_card_category_end_date)),
                          new SqlParameter("@cat_publish_date",db.DBYYYYMMDDformat(data.sims_report_card_category_publish_date)),
                          new SqlParameter("@cat_freeze_status",data.sims_report_card_category_freeze_status),
                          new SqlParameter("@cat_config_status",data.sims_report_card_category_config_status),

                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region ConfigAssignmentInsertion
        [Route("ConfigAssignmentInsertion")]
        public HttpResponseMessage ConfigAssignmentInsertion(string cTemp, gradeBookNewConfigAssignment data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@config_code",data.sims_report_card_config_code),
                          new SqlParameter("@term_code",data.sims_report_card_term_code),
                          new SqlParameter("@cat_code",data.sims_report_card_category_code),
                          new SqlParameter("@term_code_old",data.sims_report_card_term_code_old),
                          new SqlParameter("@cat_code_old",data.sims_report_card_category_code_old),
                          new SqlParameter("@assign_number",data.sims_report_card_assign_number),
                          new SqlParameter("@assign_st_date",db.DBYYYYMMDDformat(data.sims_report_card_assign_st_date)),
                          new SqlParameter("@assign_end_date",db.DBYYYYMMDDformat(data.sims_report_card_assign_end)),
                          new SqlParameter("@assign_publish_date",db.DBYYYYMMDDformat(data.sims_report_card_assign_publish_date)),
                          new SqlParameter("@assign_max_score",data.sims_report_card_assign_max_score),
                          new SqlParameter("@assign_freeze_status",data.sims_report_card_assign_freeze_status),
                          new SqlParameter("@assign_config_status",data.sims_report_card_assign_config_status),
                          new SqlParameter("@sims_allow_edit",data.sims_allow_edit),
                          new SqlParameter("@sims_assign_remark_visible",data.sims_assign_remark_visible),
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region CategoryInsertion
        [Route("CategoryInsertion")]
        public HttpResponseMessage CategoryInsertion(string cTemp, gradeBookNewCategory data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            int ins;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@cat_code",data.sims_report_card_category_code),
                          new SqlParameter("@cat_name",data.sims_report_card_category_name),
                          new SqlParameter("@cat_name_ot",data.sims_report_card_category_name_ot),
                          new SqlParameter("@cat_desc",data.sims_report_card_category_desc),
                          new SqlParameter("@cat_type", data.sims_report_card_category_type),
                          new SqlParameter("@cat_weightage_type",data.sims_report_card_category_weightage_type),
                          new SqlParameter("@cat_weightage_value",data.sims_report_card_category_weightage_value),
                          new SqlParameter("@max_of_assign_count",data.sims_report_card_category_max_of_assign_count),
                          new SqlParameter("@cat_score_type",data.sims_report_card_category_score_type),
                          new SqlParameter("@cat_points_poss",data.sims_report_card_category_points_possible),
                          new SqlParameter("@cat_color",data.sims_report_card_category_color_code),
                          new SqlParameter("@cat_grade_scale_code",data.sims_report_card_category_grade_scale_code),
                          new SqlParameter("@cat_include_in_final",data.sims_report_card_category_include_in_final_grade),
                          new SqlParameter("@cat_status",data.sims_report_card_category_status),
                          new SqlParameter("@parent_category_code",data.sims_report_card_parent_category_Code),
                          new SqlParameter("@sims_report_card_is_co_scolastic",data.sims_report_card_is_co_scolastic)
                        });
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }

        #endregion

        #region AssignmentInsertion
        [Route("AssignmentInsertion")]
        public HttpResponseMessage AssignmentInsertion(string cTemp, gradeBookNewAssignment data)
        {
            int ins;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",data.sims_cur_code),
                          new SqlParameter("@aca_year",data.sims_academic_year),
                          new SqlParameter("@assign_number",data.sims_report_card_assign_number),
                          new SqlParameter("@assign_name",data.sims_report_card_assign_name),
                          new SqlParameter("@assign_name_ot",data.sims_report_card_assign_name_ot),
                          new SqlParameter("@assign_desc",data.sims_report_card_assign_desc),
                          new SqlParameter("@assign_type",data.sims_report_card_assign_type),
                          new SqlParameter("@grade_scale",data.sims_report_card_assign_grade_scale_code),
                          new SqlParameter("@assign_include_in_final_grade",data.sims_report_card_assign_include_in_final_grade),
                          new SqlParameter("@assign_status",data.sims_report_card_assign_status),
                          new SqlParameter("@sims_report_card_assign_weightage_value",data.sims_report_card_assign_weightage_value),
                          new SqlParameter("@sims_report_card_assign_weightage_type",data.sims_report_card_assign_weightage_type),
                          new SqlParameter("@sims_report_card_assign_points_possible",data.sims_report_card_assign_points_possible),
                          new SqlParameter("@sims_report_card_is_co_scolastic",data.sims_report_card_is_co_scolastic),
                          new SqlParameter("@sims_report_card_assign_score_type",data.sims_report_card_assign_score_type),
                          new SqlParameter("@sims_report_card_parent_assignment_code",data.sims_report_card_parent_assignment_code),
                        });
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }

        #endregion

        #region AssignmentInsertion
        [Route("importSavedConfigSecData")]
        public HttpResponseMessage importSavedConfigSecData(String cur_code, String aca_year, String config_code)
        {
            List<GradeSecObj> mod_list = new List<GradeSecObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_config_proc", new List<SqlParameter>() {
                        new SqlParameter("@opr",'E'),
                        new SqlParameter("@cur_code",cur_code),
                        new SqlParameter("@aca_year",aca_year),
                        new SqlParameter("@config_code",config_code),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            GradeSecObj simsobj = new GradeSecObj();
                            str1 = dr["sims_grade_code"].ToString();
                            var v = from p in mod_list where p.grade_code == str1 select p;

                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();

                            simsobj.section = new List<SectionObj>();

                            SectionObj h = new SectionObj();

                            h.sims_section_code = dr["sims_section_code"].ToString();
                            h.sims_section_name = dr["sims_section_name_en"].ToString();
                            h.status = dr["section_status"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.section.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).section.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        } // End of importSavedConfigSecData
        #endregion

        #region SubjectConfig
        [Route("SubjectConfigCommon")]
        public HttpResponseMessage SubjectConfigCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_report_card_subject_config_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //[Route("getSubjectConfigData")]
        //public HttpResponseMessage getSubjectConfigData(string cur_code, string acad_year, string grade_code, string section_code)
        //{
        //    object[] arr = null;
        //    List<Sims028> rc = new List<Sims028>();
        //    List<subject_grade> lst = new List<subject_grade>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_subject_config_proc]",
        //           new List<SqlParameter>()
        //                 {
        //                            new SqlParameter("@opr", "C"),
        //                             new SqlParameter("@cur_code", cur_code),
        //                            new SqlParameter("@aca_year", acad_year),
        //                            new SqlParameter("@grade_code",grade_code),
        //                            new SqlParameter("@section_code",section_code),

        //               });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims028 obj = new Sims028();
        //                    obj.subjects = new List<subject_grade>();

        //                    string cur_code1 = dr["sims_cur_code"].ToString();
        //                    string acad_year1 = dr["sims_academic_year"].ToString();
        //                    string grade_code1 = dr["sims_grade_code"].ToString();
        //                    string section_code1 = dr["sims_section_code"].ToString();

        //                    subject_grade obj1 = new subject_grade();

        //                    obj1.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
        //                    obj1.sims_subject_code = dr["sims_subject_code"].ToString();


        //                    var v = (from p in rc where p.sims_cur_code == cur_code1 && p.sims_academic_year == acad_year1 && p.sims_grade_code == grade_code1 && p.sims_section_code == section_code1 select p);
        //                    //var v = (from p in rc select p);

        //                    if (v.Count() == 0)
        //                    {
        //                        obj.subjects.Add(obj1);
        //                        rc.Add(obj);

        //                    }
        //                    else
        //                    {
        //                        v.ElementAt(0).subjects.Add(obj1);
        //                    }

        //                }

        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, e);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, rc);
        //}

        [Route("getsubjectConfigInitial")]
        public HttpResponseMessage getsubjectConfigInitial(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_subjectConfigInitial]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

       
        [Route("getAllSubjectConfigData")]
        public HttpResponseMessage getAllSubjectConfigData(string cur_code, string acad_year, string config_code, string grade_code, string section_code,string term_code,string subject)
        {
            subject_config subCon = new subject_config();
            object[] arr;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subjectConfigInitial]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@cur_code",cur_code),
                                new SqlParameter("@aca_year",acad_year),
                                new SqlParameter("@config_code",config_code),
                                new SqlParameter("@grade_code",grade_code),
                                new SqlParameter("@section_code",section_code),
                                new SqlParameter("@term_code",term_code),
                                new SqlParameter("@subject_code",subject),
                         });
                    if (dr.HasRows)
                    {

                        subCon.TermDet = new List<termDet>();
                        while (dr.Read())
                        {
                            termDet fd = new termDet();
                            fd.sims_cur_code = dr["sims_cur_code"].ToString();
                            fd.sims_academic_year = dr["sims_academic_year"].ToString();
                            fd.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                            fd.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();
                            fd.sims_report_card_term_desc = dr["sims_report_card_term_desc"].ToString();
                            fd.term_cnt = dr["term_cnt"].ToString();
                            fd.cat_cnt = dr["cat_cnt"].ToString();
                            fd.assign_cnt = dr["assign_cnt"].ToString();
                            if (fd.CatDetChild == null)
                                fd.CatDetChild = new List<catDet>();
                            subCon.TermDet.Add(fd);
                        }
                        dr.NextResult();
                        subCon.CatDet = new List<catDet>();
                        while (dr.Read())
                        {
                            catDet id = new catDet();
                            id.sims_cur_code = dr["sims_cur_code"].ToString();
                            id.sims_academic_year = dr["sims_academic_year"].ToString();
                            id.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                            id.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();
                            id.sims_report_card_category_code = dr["sims_report_card_category_code"].ToString();
                            id.sims_report_card_category_name = dr["sims_report_card_category_name"].ToString();
                            id.sims_report_card_category_color_code = dr["sims_report_card_category_color_code"].ToString();
                            id.cat_cnt = dr["cat_cnt"].ToString();
                            id.assign_cnt = dr["assign_cnt"].ToString();
                            if (id.assignDetchild == null)
                                id.assignDetchild = new List<assignDet>();
                            subCon.CatDet.Add(id);
                        }
                        dr.NextResult();
                        subCon.AssignDet = new List<assignDet>();
                        while (dr.Read())
                        {
                            assignDet assgn = new assignDet();
                            assgn.sims_cur_code = dr["sims_cur_code"].ToString();
                            assgn.sims_academic_year = dr["sims_academic_year"].ToString();
                            assgn.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                            assgn.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();
                            assgn.sims_report_card_category_code = dr["sims_report_card_category_code"].ToString();
                            assgn.sims_report_card_assign_number = dr["sims_report_card_assign_number"].ToString();
                            assgn.sims_report_card_assign_name = dr["sims_report_card_assign_name"].ToString();
                            assgn.sims_report_card_assign_max_score1 = dr["sims_report_card_assign_max_score"].ToString();
                            assgn.sims_report_card_subject_grade_scale_code = dr["sims_report_card_subject_grade_scale_code"].ToString();
                            assgn.assign_cnt = dr["assign_cnt"].ToString();
                            if (assgn.ActualDet == null)
                                assgn.ActualDet = new List<actualDet>();

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();

                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_report_card_subject_config_proc]",
                                new List<SqlParameter>() 
                             { 
                                   new SqlParameter("@opr",'Z'),
                                    new SqlParameter("@cur_code",cur_code),
                                    new SqlParameter("@aca_year",acad_year),
                                    new SqlParameter("@config_code",config_code),
                                    new SqlParameter("@grade_code",grade_code),
                                    new SqlParameter("@section_code",section_code),
                                    new SqlParameter("@term_code",term_code),
                                    new SqlParameter("@subject_code",subject),
                                    new SqlParameter("@cat_code",assgn.sims_report_card_category_code),
                                    new SqlParameter("@assign_number",assgn.sims_report_card_assign_number),
                             });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        actualDet assgn1 = new actualDet();
                                        assgn1.sims_cur_code = dr1["sims_cur_code"].ToString();
                                        assgn1.sims_academic_year = dr1["sims_academic_year"].ToString();
                                        assgn1.sims_report_card_config_code = dr1["sims_report_card_config_code"].ToString();
                                        assgn1.sims_report_card_term_code = dr1["sims_report_card_term_code"].ToString();
                                        assgn1.sims_report_card_category_code = dr1["sims_report_card_category_code"].ToString();
                                        assgn1.sims_report_card_assign_number = dr1["sims_report_card_assign_number"].ToString();
                                        assgn1.sims_report_card_assign_name = dr1["sims_report_card_assign_name"].ToString();

                                        assgn1.sims_grade_code = dr1["sims_grade_code"].ToString();
                                        assgn1.sims_section_code = dr1["sims_section_code"].ToString();
                                        assgn1.sims_subject_code = dr1["sims_subject_code"].ToString();
                                        assgn1.sims_config_assign_max_score = dr1["sims_config_assign_max_score"].ToString();
                                        assgn1.sims_report_card_assign_max_score1 = dr["sims_report_card_assign_max_score"].ToString();

                                        //  if(!string.IsNullOrEmpty(dr1["sims_report_card_subject_grade_scale_code"].ToString()))
                                        assgn1.narr_grade_group_code = dr1["sims_report_card_subject_grade_scale_code"].ToString();
                                        assgn1.narr_grade_group_code_old = dr1["sims_report_card_subject_grade_scale_code"].ToString();
                                        assgn1.subject_flag = dr1["subject_flag"].ToString();
                                        assgn1.narr_grade_name = dr1["narr_grade_name"].ToString();
                                        assgn1.sims_report_card_assign_grade_scale_code = dr1["sims_report_card_assign_grade_scale_code"].ToString();
                                        assgn1.sims_report_card_assign_type = dr1["sims_report_card_assign_type"].ToString();
                                        assgn1.all_saved_config = dr1["all_saved_config"].ToString();
                                        if (dr1["Assignmentstatus"].ToString() == "A")
                                        {
                                            assgn1.assignmentstatus = true;
                                        }
                                        else
                                        {
                                            assgn1.assignmentstatus = false;
                                        }
                                        assgn1.checkeddelstatus = true;
                                        // assgn1.Assignmentstatus = dr1["Assignmentstatus"]=="A" ? true : false;
                                        //assgn.assign_cnt = dr["assign_cnt"].ToString();
                                        assgn.ActualDet.Add(assgn1);
                                    }
                                }
                            }

                            subCon.AssignDet.Add(assgn);

                        }

                        dr.NextResult();
                        subCon.SubjectDet = new List<subjectDet>();
                        while (dr.Read())
                        {
                            subjectDet sub = new subjectDet();
                            sub.sims_cur_code = dr["sims_cur_code"].ToString();
                            sub.sims_academic_year = dr["sims_academic_year"].ToString();
                            //sub.sims_grade_code = dr["sims_grade_code"].ToString();
                            //sub.sims_section_code = dr["sims_section_code"].ToString();
                            sub.sims_subject_code = dr["sims_subject_code"].ToString();
                            sub.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sub.sims_subject_color = dr["sims_subject_color"].ToString();
                            subCon.SubjectDet.Add(sub);
                        }
                    }
                }

                foreach (termDet t in subCon.TermDet)
                {

                    var rest = from c in subCon.CatDet where c.sims_cur_code == t.sims_cur_code && c.sims_academic_year == t.sims_academic_year && c.sims_report_card_config_code == t.sims_report_card_config_code && c.sims_report_card_term_code == t.sims_report_card_term_code select c;
                    foreach (catDet t1 in rest)
                    {
                        var rest1 = from c1 in subCon.AssignDet where c1.sims_cur_code == t1.sims_cur_code && c1.sims_academic_year == t1.sims_academic_year && c1.sims_report_card_config_code == t1.sims_report_card_config_code && c1.sims_report_card_term_code == t1.sims_report_card_term_code && c1.sims_report_card_category_code == t1.sims_report_card_category_code select c1;
                        t1.assignDetchild.AddRange(rest1);
                    }
                    t.CatDetChild.AddRange(rest);
                }
                return Request.CreateResponse(HttpStatusCode.OK, subCon);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subCon);
            }

        }

        [Route("getAllSubjectConfigDataSave")]
        public HttpResponseMessage getAllSubjectConfigDataSave(string cur_code, string acad_year, string config_code, string grade_code, string section_code, string term_code, string subject)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<actualDet> subject_assignment = new List<actualDet>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_subject_config_proc]",
                        new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr",'Z'),
                                    new SqlParameter("@cur_code",cur_code),
                                    new SqlParameter("@aca_year",acad_year),
                                    new SqlParameter("@config_code",config_code),
                                    new SqlParameter("@grade_code",grade_code),
                                    new SqlParameter("@section_code",section_code),
                                    new SqlParameter("@term_code",term_code),
                                    new SqlParameter("@subject_code",subject),
                                    //new SqlParameter("@cat_code",assgn.sims_report_card_category_code),
                                    //new SqlParameter("@assign_number",assgn.sims_report_card_assign_number),
                         });

                    while (dr.Read())
                    {
                        //Student s = new Student();
                        //s.stud_assign = new List<stud_assign>();

                        string str = dr["sims_subject_code"].ToString();
                        var v = (from p in subject_assignment where p.sims_subject_code == str select p);

                        actualDet subjectList = new actualDet();
                        subjectList.sub_assign = new List<sub_assign>();

                        subjectList.sims_subject_code = dr["sims_subject_code"].ToString();
                        subjectList.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                        subjectList.sims_report_card_assign_grade_scale_code = dr["sims_report_card_subject_grade_scale_code"].ToString();
                       // subjectList.assignmentstatus = dr["assignmentstatus"].ToString();

                        sub_assign assignments = new sub_assign();
                        assignments.sims_cur_code = dr["sims_cur_code"].ToString();
                        assignments.sims_academic_year = dr["sims_academic_year"].ToString();
                        assignments.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        assignments.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();
                        assignments.sims_report_card_category_code = dr["sims_report_card_category_code"].ToString();
                        assignments.sims_report_card_assign_number = dr["sims_report_card_assign_number"].ToString();
                        assignments.sims_report_card_assign_name = dr["sims_report_card_assign_name"].ToString();

                        assignments.sims_grade_code = dr["sims_grade_code"].ToString();
                        assignments.sims_section_code = dr["sims_section_code"].ToString();
                        assignments.sims_subject_code = dr["sims_subject_code"].ToString();
                        assignments.sims_config_assign_max_score = dr["sims_config_assign_max_score"].ToString();
                        assignments.sims_report_card_assign_max_score1 = dr["sims_report_card_assign_max_score"].ToString();

                        //  if(!string.IsNullOrEmpty(dr["sims_report_card_subject_grade_scale_code"].ToString()))
                        assignments.narr_grade_group_code = dr["sims_report_card_subject_grade_scale_code"].ToString();
                        assignments.narr_grade_group_code_old = dr["sims_report_card_subject_grade_scale_code"].ToString();
                        assignments.subject_flag = dr["subject_flag"].ToString();
                        assignments.narr_grade_name = dr["narr_grade_name"].ToString();
                        assignments.sims_report_card_assign_grade_scale_code = dr["sims_report_card_assign_grade_scale_code"].ToString();
                        assignments.sims_report_card_assign_type = dr["sims_report_card_assign_type"].ToString();
                        assignments.all_saved_config = dr["all_saved_config"].ToString();
                        if (dr["Assignmentstatus"].ToString() == "A")
                        {
                            subjectList.assignmentstatus = true;
                        }
                        else
                        {
                            subjectList.assignmentstatus = false;
                        }

                        if (dr["Assignmentstatus"].ToString() == "A")
                        {
                            assignments.assignmentstatus = true;
                        }
                        else
                        {
                            assignments.assignmentstatus = false;
                        }
                        assignments.checkeddelstatus = true;

                        if (v.Count() == 0)
                        {
                            subjectList.sub_assign.Add(assignments);
                            subject_assignment.Add(subjectList);
                        }
                        else
                        {
                            v.ElementAt(0).sub_assign.Add(assignments);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, subject_assignment);
        }


        [Route("CUD_Insertsubjectmapping")]
        public HttpResponseMessage CUD_Insertsubjectmapping(List<subject_config> lst)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int i = 1;
                    foreach (subject_config obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_subject_config_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@cur_code", obj.sims_cur_code),
                            new SqlParameter("@aca_year", obj.sims_academic_year),
                            new SqlParameter("@grade_code", obj.sims_grade_code),
                            new SqlParameter("@section_code", obj.sims_section_code),
                            new SqlParameter("@config_code", obj.sims_report_card_config_code),
                            new SqlParameter("@sims_subject_code1", obj.sims_subject_code1),
                            new SqlParameter("@sims_subject_code", obj.sims_subject_code),

                            new SqlParameter("@subject_co_scholastic_group_code", obj.sims_subject_code1),
                            new SqlParameter("@cat_code", obj.sims_report_card_category_code),
                            new SqlParameter("@assign_number", obj.sims_report_card_assign_number),
                            new SqlParameter("@term_code", obj.sims_report_card_term_code),
                            new SqlParameter("@assign_max_score", obj.sims_report_card_assign_max_score1),
                            new SqlParameter("@assgn_status", obj.assignmentstatus),
                            new SqlParameter("@grade_pt", obj.narr_grade_group_code),
                      });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("CUD_delsubjectmapping")]
        public HttpResponseMessage CUD_delsubjectmapping(subject_config data)
        {
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_subject_config_proc]",
                            new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@cur_code", data.sims_cur_code),
                            new SqlParameter("@aca_year", data.sims_academic_year),
                            new SqlParameter("@config_code",data.sims_report_card_config_code),
                            new SqlParameter("@grade_code", data.sims_grade_code),
                            new SqlParameter("@section_code", data.sims_section_code),
                            new SqlParameter("@subject_co_scholastic_group_code",data.sims_subject_code),
                            new SqlParameter("@term_code",data.sims_report_card_term_code),
                            new SqlParameter("@cat_code",data.sims_report_card_category_code),
                            new SqlParameter("@assign_number",data.sims_report_card_assign_number),
                            new SqlParameter("@grade_scale_code",data.narr_grade_group_code)
                        });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("insertsubjectattribute")]
        public HttpResponseMessage insertsubjectattribute(List<gradeBookNewTerm> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradeBookNewTerm d in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_config_proc]",

                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr","AO"),
                            new SqlParameter("@sims_cur_code",d.sims_cur_code),
                            new SqlParameter("@sims_academic_year",d.sims_academic_year),
                            new SqlParameter("@sims_grade_code",d.sims_grade_code),
                            new SqlParameter("@sims_section_code",d.sims_section_code),
                            new SqlParameter("@sims_gb_number",d.sims_gb_number),
                            new SqlParameter("@sims_gb_cat_code",d.sims_gb_cat_code),
                            new SqlParameter("@sims_gb_cat_assign_number",d.sims_gb_cat_assign_number),
                            new SqlParameter("@sims_subject_attribute_group_code",d.sims_subject_attribute_group_code),
                            new SqlParameter("@sims_subject_attribute_code",d.sims_subject_attribute_code),
                            new SqlParameter("@sims_cat_assign_subject_attribute_max_score",d.sims_cat_assign_subject_attribute_max_score),
                            new SqlParameter("@sims_gb_cat_assign_subject_attribute_max_score_correct",d.sims_gb_cat_assign_subject_attribute_max_score_correct),
                            new SqlParameter("@sims_report_card_config_code",d.sims_report_card_config_code),
                            new SqlParameter("@term_code",d.sims_report_card_term_code),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        #endregion
    }
}