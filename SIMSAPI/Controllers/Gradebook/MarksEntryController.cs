﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.MarksEntryNew
{
    [RoutePrefix("api/MarksEntryNew")]
    public class MarksEntryNewController : ApiController
    {
        #region MarksEntryCommon

        [Route("MarksEntryCommon")]
        public HttpResponseMessage MarksEntryCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_mark_entry_new_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        #region SubjectDataInsertion
        [Route("SubjectDataInsertion")]
        public HttpResponseMessage SubjectDataInsertion(string cTemp, List<SubjectData> data)
        {
            int ins;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SubjectData obj in data)
                    {
                        if (((obj.sims_student_assign_final_grade != null && obj.sims_student_assign_final_grade != "") || obj.sims_student_assign_exam_status != "P") && obj.isDirty)
                        {
                            using (DBConnection db1 = new DBConnection())
                            {
                                ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_mark_entry_new_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",obj.sims_cur_code),
                          new SqlParameter("@aca_year",obj.sims_academic_year),
                          new SqlParameter("@grade_code",obj.sims_grade_code),
                          new SqlParameter("@section_code",obj.sims_section_code),
                          new SqlParameter("@enroll_number",obj.sims_enroll_number),
                          new SqlParameter("@assign_max_point",obj.sims_gb_cat_assign_max_point),
                          new SqlParameter("@assign_comment",obj.sims_student_assign_comment),
                          new SqlParameter("@entered_by",obj.sims_student_assign_entered_by),
                          new SqlParameter("@exam_status",obj.sims_student_assign_exam_status),
                          new SqlParameter("@final_grade",obj.sims_student_assign_final_grade),
                          new SqlParameter("@received_point",obj.sims_student_assign_received_point),
                          new SqlParameter("@subject_srl_no",obj.sims_subject_srl_no)
                        });
                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region subject attribute insert
        [Route("GetAttributeStudents")]
        public HttpResponseMessage GetAttributeStudents(string cur_code, string aca_year, string grade_code, string section_code, string subject_code, string gradebook_number,string sims_attribute_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<SubjectAttributeStd> studentlist = new List<SubjectAttributeStd>();
        //    List<SubjectAttributeStd> student_attributelist = new List<SubjectAttributeStd>();


            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_mark_entry_new_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@aca_year", aca_year),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@section_code", section_code),
                            new SqlParameter("@subject_code", subject_code),
                            new SqlParameter("@gradebook_number", gradebook_number),
                            new SqlParameter("@sims_attribute_type", sims_attribute_type),
                         });

                    while (dr.Read())
                    {


                        //Student s = new Student();
                        //s.stud_assign = new List<stud_assign>();

                        string str = dr["sims_enroll_number"].ToString();
                        var v = (from p in studentlist where p.sims_enroll_number == str select p);

                        SubjectAttributeStd students = new SubjectAttributeStd();
                        students.stud_attr = new List<stud_attr>();

                        students.sims_cur_code = dr["sims_cur_code"].ToString();
                        students.sims_academic_year = dr["sims_academic_year"].ToString();
                        students.sims_grade_code = dr["sims_grade_code"].ToString();
                        students.sims_section_code = dr["sims_section_code"].ToString();
                        students.sims_enroll_number = dr["sims_enroll_number"].ToString();
                        students.student_name = dr["student_name"].ToString();
                        students.sims_student_assign_exam_status = dr["sims_student_assign_exam_status"].ToString();
                        students.sims_student_assign_completed_date = dr["sims_student_assign_completed_date"].ToString();
                        students.sims_student_assign_comment = dr["sims_student_assign_comment"].ToString();
                        students.disflag = dr["disflag"].ToString();
                        students.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();

                        stud_attr stud_attrList = new stud_attr();
                        stud_attrList.sims_cur_code = dr["sims_cur_code"].ToString();
                        stud_attrList.sims_academic_year = dr["sims_academic_year"].ToString();
                        stud_attrList.sims_grade_code = dr["sims_grade_code"].ToString();
                        stud_attrList.sims_section_code = dr["sims_section_code"].ToString();
                        stud_attrList.sims_enroll_number = dr["sims_enroll_number"].ToString();
                        stud_attrList.student_name = dr["student_name"].ToString();
                        stud_attrList.sims_allocation_status = dr["sims_allocation_status"].ToString();
                        stud_attrList.sims_subject_srl_no = dr["sims_subject_srl_no"].ToString();
                        //stud_attrList.sims_student_assign_final_grade = dr["sims_student_assign_final_grade"].ToString();
                        stud_attrList.sims_student_assign_received_point = dr["sims_student_assign_received_point"].ToString();
                        stud_attrList.sims_student_assign_exam_status = dr["sims_student_assign_exam_status"].ToString();
                        stud_attrList.sims_student_assign_comment = dr["sims_student_assign_comment"].ToString();
                        stud_attrList.sims_student_assign_completed_date = dr["sims_student_assign_completed_date"].ToString();
                        stud_attrList.sims_student_assign_entered_by = dr["sims_student_assign_entered_by"].ToString();
                        stud_attrList.sims_student_assign_status = dr["sims_student_assign_status"].ToString();
                        stud_attrList.sims_gb_cat_assign_max_point = dr["sims_gb_cat_assign_max_point"].ToString();
                        stud_attrList.sims_attribute_code = dr["sims_attribute_code"].ToString();
                        stud_attrList.sims_attribute_name = dr["sims_attribute_name"].ToString();
                        stud_attrList.sims_gb_number = dr["sims_gb_number"].ToString();
                        stud_attrList.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        stud_attrList.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        stud_attrList.sims_subject_attribute_group_code = dr["sims_subject_attribute_group_code"].ToString();
                        stud_attrList.sims_report_card_config_code = dr["sims_report_card_config_code"].ToString();
                        stud_attrList.disflag = dr["disflag"].ToString();
                        stud_attrList.sims_allow_edit = dr["sims_allow_edit"].ToString() == "A" ? false : true;
                        stud_attrList.sims_report_card_term_code = dr["sims_report_card_term_code"].ToString();

                        if (v.Count() == 0)
                        {
                            students.stud_attr.Add(stud_attrList);
                            studentlist.Add(students);
                        }
                        else
                        {
                            v.ElementAt(0).stud_attr.Add(stud_attrList);
                        }
                    }
                    // return Request.CreateResponse(HttpStatusCode.OK, student_assignmentmarks);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, studentlist);

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("insertsubjectattribute")]
        public HttpResponseMessage insertsubjectattribute(string cTemp, List<subAttrList> data)
        {
            int ins;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (subAttrList obj in data)
                    {
                            using (DBConnection db1 = new DBConnection())
                            {
                                ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_mark_entry_new_proc]",
                            new List<SqlParameter>()
                        {
                                new SqlParameter("@opr",cTemp),
                                new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                                new SqlParameter("@sims_section_code",obj.sims_section_code),
                                new SqlParameter("@sims_gb_number",obj.sims_gb_number),
                                new SqlParameter("@sims_gb_cat_code",obj.sims_gb_cat_code),
                                new SqlParameter("@sims_gb_cat_assign_number",obj.sims_gb_cat_assign_number),
                                new SqlParameter("@sims_subject_attribute_group_code",obj.sims_subject_attribute_group_code),
                                new SqlParameter("@sims_subject_attribute_code",obj.sims_subject_attribute_code),
                                new SqlParameter("@sims_subject_attribute_enroll_number",obj.sims_subject_attribute_enroll_number),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_mark",obj.sims_gb_cat_assign_subject_attribute_mark),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_final_grade",obj.sims_gb_cat_assign_subject_attribute_final_grade),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_max_point",obj.sims_gb_cat_assign_subject_attribute_max_point),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_max_correct",obj.sims_gb_cat_assign_subject_attribute_max_correct),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_comment",obj.sims_gb_cat_assign_subject_attribute_comment),
                                new SqlParameter("@sims_gb_cat_assign_subject_attribute_status",obj.sims_gb_cat_assign_subject_attribute_status),
                                new SqlParameter("@sims_report_card_config_code",obj.sims_report_card_config_code),
                                new SqlParameter("@sims_report_card_term_code",obj.sims_report_card_term_code),
                        });
                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                            }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region CoscholasticDataInsertion
        [Route("CoscholasticDataInsertion")]
        public HttpResponseMessage CoscholasticDataInsertion(string cTemp, List<CoscholasticData> data)
        {
            int ins, count =0;
            bool inserted = true;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (CoscholasticData obj in data)
                    {
                        if (obj.isDirty)
                        {
                            using (DBConnection db1 = new DBConnection())
                            {
                                ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_mark_entry_new_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@cur_code",obj.sims_cur_code),
                          new SqlParameter("@aca_year",obj.sims_academic_year),
                          new SqlParameter("@grade_code",obj.sims_grade_code),
                          new SqlParameter("@section_code",obj.sims_section_code),
                          new SqlParameter("@enroll_number",obj.sims_enroll_number),
                          new SqlParameter("@assign_comment",obj.sims_student_assign_comment),
                          new SqlParameter("@entered_by",obj.sims_student_assign_entered_by),
                          new SqlParameter("@exam_status",obj.sims_student_assign_status),
                          new SqlParameter("@final_grade",obj.sims_co_scholastic_discriptive_indicator_grade_code),
                          new SqlParameter("@received_point",obj.sims_report_card_co_scholastic_grade_point),
                          new SqlParameter("@subject_srl_no",obj.sims_subject_srl_no)
                        });
                                if (ins == 0)
                                {
                                    inserted = false;
                                    count = count + 1;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, (inserted == true) ? 0 : count);
        }

        #endregion


        [Route("all_combo")]
        public HttpResponseMessage all_combo(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_co_scholastic_mark_entry_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }



        [Route("AllList")]
        public HttpResponseMessage AllList(Group gb)
        {
            List<Students> studList = new List<Students>();
            Group o = new Group();
            List<grade_scale> main_grade_lst = new List<grade_scale>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_mark_entry_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "GM"),
                new SqlParameter("@cur_code", gb.cur_code),
                new SqlParameter("@aca_year", gb.aca_year),
                new SqlParameter("@grade_code", gb.grade_code),
                new SqlParameter("@section_code", gb.section_code),
                new SqlParameter("@sims_co_scholastic_group_code", gb.sims_co_scholastic_group_code),
                new SqlParameter("@sims_co_scholastic_attribute_code", gb.sims_co_scholastic_attribute_code),
                new SqlParameter("@sims_co_scholastic_descriptive_indicator_code", gb.sims_co_scholastic_descriptive_indicator_code),
                new SqlParameter("@term_code", gb.term_code),
                new SqlParameter("@flag", gb.flag),

                });

                    main_grade_lst = get_grade_scale(gb.cur_code, gb.aca_year);
                    #region Gradebook Collection Loop
                    if (dr.Read())
                    {
                        o.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                        o.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        o.sims_subject_code = dr["sims_subject_code"].ToString();
                        o.sims_subject_name = dr["sims_subject_name"].ToString();

                        o.attributes = new List<attributess>();
                    }
                    dr.NextResult();
                    #endregion

                    #region Gradebook Category Loop
                    while (dr.Read())
                    {
                        attributess gc = new attributess();

                        gc.sims_co_scholastic_attribute_master_code = dr["sims_co_scholastic_attribute_master_code"].ToString();
                        gc.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                      //  gc.section_code = dr["sims_section_code"].ToString();
                        gc.indicators = new List<indicator>();
                        if (o != null)
                        {
                            o.attributes.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region Assignment Collection Loop
                    while (dr.Read())
                    {
                        indicator assign = new indicator();
                        assign.sims_co_scholastic_descriptive_indicator_master_code = dr["sims_co_scholastic_descriptive_indicator_master_code"].ToString();
                        assign.sims_co_scholastic_descriptive_indicator_name = dr["sims_co_scholastic_descriptive_indicator_name"].ToString();
                        assign.sims_co_scholastic_attribute_master_code = dr["sims_co_scholastic_attribute_master_code"].ToString();

                        assign.sims_co_scholastic_descriptive_indicator_type = dr["sims_co_scholastic_descriptive_indicator_type"].ToString();
                        assign.sims_co_scholastic_descriptive_indicator_mark = dr["sims_co_scholastic_descriptive_indicator_mark"].ToString();


                        attributess gc = (o.attributes.Single(q => q.sims_co_scholastic_attribute_master_code == assign.sims_co_scholastic_attribute_master_code));
                        gc.indicators.Add(assign);
                    }
                    dr.NextResult();

                    while (dr.Read())
                    {
                        indicator std = new indicator();
                        Students s = new Students();
                       // grade_scale sc = new grade_scale();

                        s.indicators = new List<indicator>();

                        string str = dr["sims_enroll_number"].ToString();
                        std.sims_co_scholastic_descriptive_indicator_name = dr["sims_co_scholastic_descriptive_indicator_name"].ToString();
                        std.sims_co_scholastic_descriptive_indicator_master_code = dr["sims_co_scholastic_descriptive_indicator_master_code"].ToString();

                        std.sims_co_scholastic_cdi_srl_no = dr["sims_co_scholastic_cdi_srl_no"].ToString();

                        std.sims_co_scholastic_discriptive_indicator_grade_code = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();
                        std.sims_co_scholastic_discriptive_indicator_grade = dr["sims_co_scholastic_discriptive_indicator_grade"].ToString();
                        std.sims_report_card_co_scholastic_grade_point = dr["sims_report_card_co_scholastic_grade_point"].ToString();


                        std.sims_co_scholastic_descriptive_indicator_type = dr["sims_co_scholastic_descriptive_indicator_type"].ToString();
                        std.sims_co_scholastic_descriptive_indicator_mark = dr["sims_co_scholastic_descriptive_indicator_mark"].ToString();
                        std.sims_co_scholastic_descriptive_indicator_grade_scale = dr["sims_co_scholastic_descriptive_indicator_grade_scale"].ToString();
                        if (std.sims_co_scholastic_descriptive_indicator_type=="2"){
                            std.grade_scale_lst  = main_grade_lst.Where(x => x.sims_co_scholastic_grade_scale_code == std.sims_co_scholastic_descriptive_indicator_grade_scale).ToList();
                        }
                        s.Stud_Enroll = str + "";
                        s.Stud_Full_Name = dr["student_name"].ToString();
                           var v = from p in studList where p.Stud_Enroll == str select p;

                           if (std.sims_co_scholastic_descriptive_indicator_master_code != "")
                        {
                            std.insert = true;

                        }
                        else
                        {
                            std.insert = false;
                        }
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).indicators.Add(std);

                        }
                        else
                        {
                            studList.Add(s);
                            s.indicators.Add(std);
                        }

                    }
                }
            }
                    #endregion

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            object[] ob = new object[2];
            ob[0] = new object();
            ob[1] = new object();
            ob[0] = o;
            ob[1] = studList;
            return Request.CreateResponse(HttpStatusCode.OK, ob);
        }


        public List<grade_scale> get_grade_scale(string cur,string academic) {

            List<grade_scale> lst = new List<grade_scale>();

            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_co_scholastic_mark_entry_proc]",
                    new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "E"),
                new SqlParameter("@cur_code", cur),
                new SqlParameter("@aca_year",academic),
           
                });

                while (dr.Read())
                {
                    grade_scale assign = new grade_scale();
                    assign.sims_co_scholastic_grade_scale_code = dr["sims_co_scholastic_grade_scale_code"].ToString();
                    assign.sims_co_scholastic_grade_code = dr["sims_co_scholastic_grade_code"].ToString();
                    assign.sims_co_scholastic_grade_name = dr["sims_co_scholastic_grade_name"].ToString();
                    assign.sims_co_scholastic_grade_point_low = dr["sims_co_scholastic_grade_point_low"].ToString();
                    assign.sims_co_scholastic_grade_point_high = dr["sims_co_scholastic_grade_point_high"].ToString();
                    assign.sims_co_scholastic_grade_description = dr["sims_co_scholastic_grade_description"].ToString();
                    assign.sims_co_scholastic_grade_color_code = dr["sims_co_scholastic_grade_color_code"].ToString();


                    lst.Add(assign);
                }
            }
            return lst;
        }


        [Route("insertsubjectattribute")]
        public HttpResponseMessage insertsubjectattribute(List<co_scholastic_mark> data)
        {
            int ins;
            bool inserted = false;
            try
            {
               
                    foreach (co_scholastic_mark obj in data)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_co_scholastic_mark_entry_proc]",
                        new List<SqlParameter>()
                        {
                                new SqlParameter("@opr","F"),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@cur_code",obj.cur_code),
                                new SqlParameter("@sims_subject_srl_no",obj.sims_subject_srl_no),
                                new SqlParameter("@sims_co_scholastic_enroll_number",obj.sims_co_scholastic_enroll_number),
                                new SqlParameter("@sims_co_scholastic_discriptive_indicator_grade_code",obj.sims_co_scholastic_discriptive_indicator_grade_code),
                                new SqlParameter("@sims_co_scholastic_discriptive_indicator_grade",obj.sims_co_scholastic_discriptive_indicator_grade),
                                new SqlParameter("@sims_report_card_co_scholastic_grade_point",obj.sims_report_card_co_scholastic_grade_point),
                                new SqlParameter("@sims_student_assign_entered_by",obj.sims_student_assign_entered_by),
                               
                                
                        });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                        }
                    
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}