﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

using SIMSAPI.Models.ERP.gradebookClass;
using System.Web;
using System.IO;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LearningAsd")]
    public class LearningAsdController : ApiController
    {


        [Route("getTemplate")]
        public HttpResponseMessage getTemplate()
        {
            List<template> mod_list = new List<template>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            template hrmsObj = new template();
                            hrmsObj.sims_comment_template_name = dr["sims_comment_template_name"].ToString();
                            hrmsObj.sims_comment_template_id = dr["sims_comment_template_id"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }



        [Route("getTemplateDetails")]
        public HttpResponseMessage getTemplateDetails(string cur, string academic, string temp_id, string grade,string sec,string sub,string dt ,string slot)
        {
            List<template> mod_list = new List<template>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", academic),
                            new SqlParameter("@sims_comment_template_id",temp_id),


                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",sec),
                            new SqlParameter("@sims_lesson_subject_code",sub),
                            new SqlParameter("@sims_lesson_date",db.DBYYYYMMDDformat(dt)),
                           // new SqlParameter("@sims_lesson_period_no",slot),
                            new SqlParameter("@sims_lesson_created_for",slot),

                            //new SqlParameter("",temp_id),
                            //new SqlParameter("",temp_id),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            template hrmsObj = new template();
                            hrmsObj.sims_comment_template_id = dr["sims_comment_template_id"].ToString();
                            hrmsObj.sims_comment_code = dr["sims_comment_code"].ToString();
                            hrmsObj.sims_comment_type = dr["sims_comment_type"].ToString();
                            hrmsObj.sims_value_type = dr["sims_value_type"].ToString();

                            hrmsObj.sims_header_comment_code = dr["sims_header_comment_code"].ToString();
                            hrmsObj.sims_header_desc = dr["sims_header_desc"].ToString();

                            hrmsObj.desc = dr["sims_comment_description"].ToString();
                            hrmsObj.sims_drop_down_display_code = dr["sims_drop_down_display_code"].ToString();
                            if (hrmsObj.sims_value_type == "D" || hrmsObj.sims_value_type == "B")
                                hrmsObj.desc = hrmsObj.sims_drop_down_display_code;



                            hrmsObj.display_text = dr["sims_comment_description"].ToString();
                            hrmsObj.display_text_short = dr["display_text_short"].ToString();


                            hrmsObj.sims_lesson_id = dr["sims_lesson_id"].ToString();

                            if (dr["sims_comment_code_value"].ToString() == "1")
                                hrmsObj.sims_comment_code_value = true;


                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getgrade")]
        public HttpResponseMessage getgrade(string cur_code, string academic_year, string userName)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getsection")]
        public HttpResponseMessage getsection(string cur_code, string academic_year, string gradeCode, string userName)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", gradeCode),
                            new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSubject")]
        public HttpResponseMessage getSubject(string cur, string academic_year, string grade, string section, string user_name)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                            new SqlParameter("@user_name",user_name),

                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();


                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSubject")]
        public HttpResponseMessage getSubject(string cur, string academic_year, string grade, string section, string user_name,string date)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                            new SqlParameter("@user_name",user_name),
                            new SqlParameter("@sims_lesson_date",db.DBYYYYMMDDformat(date)),

                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();


                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("getSlot")]
        public HttpResponseMessage getSlot(string cur, string academic_year, string grade, string section,  string sims_teacher_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                             new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                           
                            new SqlParameter("@user_name",sims_teacher_code),
                            

                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.slot = dr["slot"].ToString();
                            simsobj.slot_name = dr["slot_name"].ToString();



                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSlot")]
        public HttpResponseMessage getSlot(string cur, string academic_year, string grade, string section, string sims_teacher_code, string date, string subject_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                             new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                           
                            new SqlParameter("@user_name",sims_teacher_code),
                            new SqlParameter("@sims_lesson_date",db.DBYYYYMMDDformat(date)),
                            
                            new SqlParameter("@sims_lesson_subject_code",subject_code),

                            
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.slot = dr["slot"].ToString();
                            simsobj.slot_name = dr["slot_name"].ToString();



                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("CUDLearning")]
        public HttpResponseMessage CUDLearning(template obj)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_comment_template_Employee_proc]",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "F"),
                  new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                  new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                  new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                  new SqlParameter("@sims_section_code", obj.sims_section_code),
                  new SqlParameter("@sims_lesson_subject_code", obj.sims_lesson_subject_code),
                  new SqlParameter("@sims_lesson_id", obj.sims_lesson_id),
                  new SqlParameter("@sims_lesson_date",db.DBYYYYMMDDformat( obj.sims_lesson_date)+" "+ obj.sims_lesson_date_time ),
                  new SqlParameter("@sims_lesson_period_no", obj.sims_lesson_period_no),
                  new SqlParameter("@user_name", obj.user_name),
                  new SqlParameter("@sims_lesson_remark", obj.sims_lesson_remark),
                  new SqlParameter("@sims_comment_description", obj.sims_comment_description),
                  new SqlParameter("@sims_comment_template_id", obj.sims_comment_template_id),
                  new SqlParameter("@sims_lesson_created_for", obj.sims_lesson_created_for),

                  

                
                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getEmployee")]
        public HttpResponseMessage getEmployee(string cur, string academic_year, string grade, string section, string user_name,string subject)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                            new SqlParameter("@user_name",user_name),
                            new SqlParameter("@sims_lesson_subject_code",subject),

                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();


                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }




        [Route("getDropDown")]
        public HttpResponseMessage getDropDown()
        {
            List<template> list = new List<template>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_comment_template_Employee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H"),
                            //new SqlParameter("@sims_cur_code", cur_code),
                            //new SqlParameter("@sims_academic_year", academic_year),
                            //new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            template simsobj = new template();
                            simsobj.sims_comment_code = dr["sims_comment_code"].ToString();
                            simsobj.sims_drop_down_display_text = dr["sims_drop_down_display_text"].ToString();
                            simsobj.sims_header_desc = dr["sims_header_desc"].ToString();
                            simsobj.sims_header_comment_code = dr["sims_header_comment_code"].ToString();
                          
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

     

    }
}