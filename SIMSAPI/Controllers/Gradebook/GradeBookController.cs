﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
namespace SIMSAPI.Controllers.Gradebook

{
    [RoutePrefix("api/Gradebook")]
    public class GradeBookController : ApiController
    {
        string str = "";
        string str1 = "";

        #region GradeBook

        [Route("Subjectget")]
        public HttpResponseMessage Subjectget(Sims508 data)
        {
            List<Sims508> lst = new List<Sims508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_PROC]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "ST"),
               new SqlParameter("@GB_TEACHER_CODE",data.sims_employee_code),
               new SqlParameter("@ACADEMIC_YEAR",data.sims_academic_year),
               new SqlParameter("@GRADE_CODE", data.sims_grade_code),
               new SqlParameter("@SECTION_CODE",data.sims_section_code),
               new SqlParameter("@CUR_CODE",data.sims_cur_code)


               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_subject_code = dr["sims_subject_code"].ToString();
                        obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        lst.Add(obj);
                    }

                }

            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetadminuserType")]
        public HttpResponseMessage GetadminuserType(string username, string appl_code)
        {
            List<student_report_card_comment> loginuser = new List<student_report_card_comment>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[comn_useradmin_for_appl]",
                   new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "IA"),
                         new SqlParameter("@comn_user_name", username),
                         new SqlParameter("@comn_appl_code", appl_code),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.boolean_value = dr["boolean_value"].ToString();
                            loginuser.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, loginuser);
        }

        [Route("GetRCCommentType")]
        public HttpResponseMessage GetRCCommentType()
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_header_proc] ",
                   new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "P"),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_comment_type_code = dr["sims_appl_parameter"].ToString();
                            obj.sims_comment_type_name = dr["sims_appl_form_field_value1"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        #region MASS UPDATION

        [Route("TableUpdationRecords")]
        public HttpResponseMessage TableUpdationRecords(massUpdation obj)
        {
            List<massUpdation> lst = new List<massUpdation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_MASS_UPDATE_PROC]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'C'),
               new SqlParameter("@sopr", obj.sims_appl_parameter),
               new SqlParameter("@CUR_CODE", obj.sims_cur_code),
               new SqlParameter("@TABLE_NAME", obj.sims_table_name_code),
               new SqlParameter("@TABLE_COLUMN_NAME", obj.column_name_code),
               new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
               new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
               new SqlParameter("@SECTION_CODE", obj.sims_section_code),
               new SqlParameter("@GB_TERM_CODE", obj.sims_term_code)
               });
                    while (dr.Read())
                    {
                        massUpdation o = new massUpdation();
                        o.column_old_value = dr[obj.column_name_code].ToString();
                        lst.Add(o);

                    }

                }

            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetTableNames")]
        public HttpResponseMessage GetTableNames()
        {
            List<massUpdation> lst = new List<massUpdation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_MASS_UPDATE_PROC]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'A')
               });
                    while (dr.Read())
                    {
                        massUpdation o = new massUpdation();
                        o.sims_table_name = dr["sims_appl_form_field_value3"].ToString();
                        o.sims_table_name_code = dr["sims_appl_form_field_value4"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("TableColumnNames")]
        public HttpResponseMessage TableColumnNames(massUpdation obj)
        {
            List<massUpdation> lst = new List<massUpdation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_MASS_UPDATE_PROC]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'B'),
               new SqlParameter("@TABLE_NAME", obj.sims_table_name_code)
               });
                    while (dr.Read())
                    {
                        massUpdation o = new massUpdation();
                        o.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                        o.column_name = dr["sims_appl_form_field_value2"].ToString();
                        o.column_name_code = dr["sims_appl_form_field_value1"].ToString();
                        o.sims_table_name = dr["sims_appl_form_field_value3"].ToString();
                        o.sims_table_name_code = dr["sims_appl_form_field_value4"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUDTableColumnNamesUpdate")]
        public HttpResponseMessage CUDTableColumnNamesUpdate(massUpdation obj)
        {
            bool updated = false;
            List<massUpdation> lst = new List<massUpdation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_MASS_UPDATE_PROC]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'U'),
               new SqlParameter("@sopr", obj.sims_appl_parameter),
               new SqlParameter("@CUR_CODE", obj.sims_cur_code),
               new SqlParameter("@TABLE_NAME", obj.sims_table_name_code),
               new SqlParameter("@TABLE_COLUMN_NAME", obj.column_name_code),
               new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
               new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
               new SqlParameter("@SECTION_CODE", obj.sims_section_code),
               new SqlParameter("@GB_TERM_CODE", obj.sims_term_code),
               new SqlParameter("@TABLE_COLUMN_OLD_VALUE",obj.column_old_value),
               new SqlParameter("@TABLE_COLUMN_NEW_VALUE",obj.column_new_value)
               });
                    if (dr.RecordsAffected > 0)
                    {
                        updated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                updated = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }



        #endregion


        #region Data Opration

        [Route("CategoryWiseStudentForSkip")]
        public HttpResponseMessage CategoryWiseStudentForSkip(gradebook user_code)
        {
            List<Subject> sub_lst = new List<Subject>();
            List<Students> lst = new List<Students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_SKIP_STUDNET_SUBJECTWISE_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "CW"),
                new SqlParameter("@CUR_CODE",user_code.cur_code),
                new SqlParameter("@ACADEMIC_YEAR",user_code.academic_year),
                new SqlParameter("@GRADE_CODE",user_code.grade_code),
                new SqlParameter("@SECTION_CODE",user_code.section_code),
                new SqlParameter("@GB_TERM_CODE",user_code.sims_term_code),
                new SqlParameter("@GB_NUMBER1",user_code.gb_number),
                new SqlParameter("@GB_CAT_NAME", user_code.sims_gb_cat_name),
                new SqlParameter("@GB_ASSIGN_NAME",user_code.sims_gb_assign_name)
                });
                    while (dr.Read())
                    {
                        Subject sub = new Subject();
                        sub.SubjectCode = dr["sims_gb_subject_code"].ToString();
                        sub.SubjectName = dr["sims_subject_name_en"].ToString();
                        sub.subcount = dr["row_cnt"].ToString();
                        sub_lst.Add(sub);

                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Students std = new Students();
                        std.Stud_Enroll = dr["sims_enroll_number"].ToString();
                        string str = dr["sims_enroll_number"].ToString();
                        std.Stud_Full_Name = dr["student_name"].ToString();

                        std.AssignMents = new List<SIMSAPI.Models.ERP.gradebookClass.Assignment>();
                        SIMSAPI.Models.ERP.gradebookClass.Assignment s = new SIMSAPI.Models.ERP.gradebookClass.Assignment();
                        s.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        s.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        s.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        s.Assignment_section_code = dr["sims_section_code"].ToString();
                        s.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                        s.Assignment_final_grade_desc = dr["sims_gb_cat_assign_final_grade"].ToString();
                        s.Assignment_Score_type = dr["sims_gb_cat_score_type"].ToString();
                        s.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        s.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        s.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        s.Assignment_subject_code = dr["sims_gb_subject_code"].ToString();
                        s.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        s.assign_select_status = dr["assign"].ToString().Equals("1") ? true : false;
                        var v = (from p in lst where p.Stud_Enroll == str select p);
                        if (v.Count() == 0)
                        {

                            std.AssignMents.Add(s);
                            lst.Add(std);
                        }
                        else
                        {
                            v.ElementAt(0).AssignMents.Add(s);
                        }
                    }
                    dr.NextResult();

                }
            }
            catch (Exception ex)
            {
            }

            object[] ob = new object[2];
            ob[0] = new object();
            ob[1] = new object();
            ob[0] = sub_lst;
            ob[1] = lst;


            return Request.CreateResponse(HttpStatusCode.OK, ob);
        }

        [Route("getCheckedUser")]
        public HttpResponseMessage getCheckedUser(string user_code, string co_scholastic)
        {
            bool adminUser = false;
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (co_scholastic == "N")
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "CU"),
                new SqlParameter("@GB_TEACHER_CODE",user_code),

                });
                        while (dr.Read())
                        {
                            adminUser = dr["adminUser"].ToString().Equals("Y") ? true : false;
                        }
                    }
                    else
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_co_scholastic_group_proc]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C"),
                new SqlParameter("@sims_co_scholastic_employee_code",user_code),

                });
                        while (dr.Read())
                        {
                            adminUser = dr["adminUser"].ToString().Equals("Y") ? true : false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, adminUser);
        }

        [Route("AllGradeBooks")]
        public HttpResponseMessage AllGradeBooks(gradebook gb)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "AG"),
                new SqlParameter("@IOPR", "G"),
                new SqlParameter("@CUR_CODE", gb.sims_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.sims_academic_year),
                new SqlParameter("@GB_TEACHER_CODE", gb.sims_teacher_code),
                new SqlParameter("@GB_TERM_CODE", gb.sims_gb_term_name)
                });
                    #region Gradebook Collection Loop
                    while (dr.Read())
                    {

                        str = dr["sims_gb_number"].ToString();
                        var v = (from p in lst where p.gb_number == str select p);

                        if (v.Count() == 0)
                        {
                            gradebook o = new gradebook();
                            o.ischecked = false;
                            o.cnt = 0;
                            o.Categories = new List<gradebook_category>();
                            o.cur_code = dr["sims_cur_code"].ToString();
                            o.gb_number = dr["sims_gb_number"].ToString();
                            o.academic_year = dr["sims_academic_year"].ToString();
                            o.grade_code = dr["sims_grade_code"].ToString();
                            o.grade_name = dr["GradeName"].ToString();
                            o.section_name = dr["SectionName"].ToString();
                            o.section_code = dr["sims_section_code"].ToString();
                            o.gb_name = dr["sims_gb_name"].ToString();
                            o.gb_type = dr["sims_gb_type"].ToString();
                            o.gb_type_desc = dr["typeDesc"].ToString();
                            o.gb_remark = dr["sims_gb_remarks"].ToString();
                            o.gb_start_date = dr["sims_gb_start_date"].ToString();
                            o.gb_end_date = dr["sims_gb_end_date"].ToString();
                            o.gb_term_code = dr["sims_gb_term_code"].ToString();
                            o.gb_term_name = dr["TermName"].ToString();
                            o.gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                            o.teacher_code = dr["sims_gb_teacher_code"].ToString();
                            o.teacher_name = dr["T_name"].ToString();
                            o.TeacherList = dr["TeacherList"].ToString();
                            o.Subject_Code = dr["sims_gb_subject_code"].ToString();
                            o.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            o.sims_homeroom_batch_code = dr["sims_homeroom_batch_code"].ToString();
                            o.Subject_name = dr["sims_subject_name_en"].ToString();
                            o.gb_status = dr["sims_gb_status"].Equals("A") ? true : false;
                            o.gb_freeze_status = dr["sims_gb_completed"].ToString().Equals("T") ? true : false;
                            string u = dr["AdminUser"].ToString();
                            o.adminUser = u.Equals("Y");
                            lst.Add(o);
                        }
                        else
                        {
                            gradebook_category sb = new gradebook_category();
                            sb.cat_name = dr["sims_gb_cat_name"].ToString();
                            sb.cur_code = dr["sims_cur_code"].ToString();
                            sb.academic_year = dr["sims_academic_year"].ToString();
                            sb.grade_code = dr["sims_grade_code"].ToString();
                            sb.grade_name = dr["GradeName"].ToString();
                            sb.section_name = dr["SectionName"].ToString();
                            sb.section_code = dr["sims_section_code"].ToString();
                          //  sb.sims_category_grade_scale = dr["sims_section_code"].ToString();
                            v.ElementAt(0).Categories.Add(sb);
                        }
                    }
                    dr.NextResult();
                    #endregion
                    dr.Close();
                }
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUDGradeBookDataOpr")]
        public HttpResponseMessage CUDGradeBookDataOpr(List<gradebook> simsobj)
        {
            string inserted = "";
            gradebook o = new gradebook();
            #region Insert/Update/Delete
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (gradebook data in simsobj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_NAME", data.gb_name),
                new SqlParameter("@GB_TYPE", data.gb_type),
                new SqlParameter("@GB_TERM_CODE", data.gb_term_code),
                new SqlParameter("@GB_GRADE_SCALE", data.gb_grade_scale),
                new SqlParameter("@GB_START_DATE", data.gb_start_date),
                new SqlParameter("@GB_END_DATE", data.gb_end_date),
                new SqlParameter("@GB_TEACHER_CODE", data.teacher_code),
                new SqlParameter("@GB_REMARK", data.gb_remark),
                new SqlParameter("@GB_TEACHERLIST", data.TeacherList),
                new SqlParameter("@GB_STATUS", data.gb_status==true?"A":"I"),
                new SqlParameter("@GB_SUBJECT_CODE", data.Subject_Code),
                new SqlParameter("@GB_HOMEROOM_CODE",data.sims_homeroom_code),
                new SqlParameter("@GB_HOMEROOM_BATCH_CODE",data.sims_batch_code),
                new SqlParameter("@GB_COMPLETED",data.gb_freeze_status==true?"T":"F")
                         });


                        if (dr.RecordsAffected > 0)
                        {
                            dr.Read();
                            inserted = dr["craeated"].ToString();
                        }
                        else
                        {

                            dr.Read();
                            inserted = dr["notcraeated"].ToString();
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                inserted = "Grade book not Created";
            }
            #endregion
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("CategoryDataOpr")]
        public HttpResponseMessage CategoryDataOpr(List<gradebook_category> data1)
        {
            gradebook_category o = new gradebook_category();
            o.insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradebook_category data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.cat_code),
                new SqlParameter("@GB_CAT_NAME", data.cat_name),
                new SqlParameter("@GB_WEIGHTAGE_TYPE", data.cat_weightage_type),
                new SqlParameter("@GB_WEIGHTAGE_PER", data.cat_weightage_per),
                new SqlParameter("@GB_CAT_SHORT_NAME", data.cat_short_name),
                new SqlParameter("@GB_COLOR_CODE", data.cat_color),
                new SqlParameter("@GB_CAT_POINTS_POSSIBLE", data.cat_points_possible),
                new SqlParameter("@GB_CAT_EXTRA_POINTS", data.cat_extra_points),
                new SqlParameter("@GB_CAT_SCORE_TYPES", data.cat_score_types),
                new SqlParameter("@GB_CAT_DROP_LOW", data.cat_drop_low),
                new SqlParameter("@GB_CAT_DROP_PER", data.cat_drop_per),
                new SqlParameter("@GB_CAT_PUBLISH_DATE", data.publishDate),
                new SqlParameter("@GB_CAT_PUBLISH_DAYS_BEFORE", ""),
                new SqlParameter("@GB_CAT_PUBLISH_ASSIGNMENT_SCORE", ""),
                new SqlParameter("@GB_CAT_INCLUDED_IN_GRADE", data.isIncludedInFinalGrade==true?"A":"I"),
                new SqlParameter("@GB_STATUS", data.status==true?"A":"I"),
                new SqlParameter("@sims_category_grade_scale", data.sims_category_grade_scale),
                         });

                        o.OPR = data.QueryResult;
                        o.QueryResult = dr.RecordsAffected + "";
                        o.cat_code = data.cat_code;
                        if (dr.Read())
                        {
                            o.AssignMents = new List<Assignment>();

                            o.cur_code = dr["sims_cur_code"].ToString();
                            o.cat_code = dr["sims_gb_cat_code"].ToString();
                            o.cat_color = dr["sims_gb_cat_color_code"].ToString();
                            o.cat_drop_low = dr["sims_gb_cat_drop_low"].ToString();
                            o.cat_drop_per = dr["sims_gb_cat_drop_per"].ToString();
                            o.cat_extra_points = dr["sims_gb_cat_extra_point"].ToString();
                            o.cat_name = dr["sims_gb_cat_name"].ToString();
                            o.cat_points_possible = dr["sims_gb_cat_points_possible"].ToString();
                            o.cat_score_types = dr["sims_gb_cat_score_type"].ToString();
                            o.cat_score_types_desc = dr["Score_Type"].ToString();
                            o.cat_short_name = dr["sims_gb_cat_short_name"].ToString();
                            o.cat_weightage_per = dr["sims_gb_cat_weightage_per"].ToString();
                            o.cat_weightage_type = dr["sims_gb_cat_weightage_type"].ToString();
                            o.cat_weightage_type_desc = dr["Weight_Desc"].ToString();
                            o.cur_name = dr["Curr_Name"].ToString();
                            o.academic_year = dr["sims_academic_year"].ToString();
                            o.grade_name = dr["Grade_Name"].ToString();
                            o.grade_code = dr["sims_grade_code"].ToString();
                            o.gb_name = dr["GB_NAME"].ToString();
                            o.gb_number = dr["sims_gb_number"].ToString();
                            o.isIncludedInFinalGrade = dr["sims_gb_cat_include_in_final_grade"].ToString().Equals("A") ? true : false;
                            o.publishDate = dr["sims_gb_cat_publish_assignment_on_date"].ToString();
                            o.section_name = dr["Section_Name"].ToString();
                            o.section_code = dr["sims_section_code"].ToString();
                            o.status = dr["sims_gb_cat_status"].Equals("A") ? true : false;
                            o.insert = true;
                            o.teacher_code = data.teacher_code;
                            try
                            {
                                o.sims_category_grade_scale = dr["sims_category_grade_scale"].ToString();
                            }
                            catch (Exception e) { }
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);

        }

        [Route("AssignmentDataOpr")]
        public HttpResponseMessage AssignmentDataOpr(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)//List<SubjectAttr> attrList
        {

            
            Assignment assign = new Assignment();
            assign.insert = false;
            Dictionary<string, string> oprcd = new Dictionary<string, string>();
            oprcd.Add("IA", "SB");
            oprcd.Add("UA", "US");

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (SIMSAPI.Models.ERP.gradebookClass.Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                new SqlParameter("@GB_ASSIGNMENT_NAME", data.Assignment_Name),
                new SqlParameter("@GB_ASSIGNMENT_OTHER_NAME", data.Assignment_Other_Name),
                new SqlParameter("@GB_ASSIGNMENT_TYPE", data.Assignment_Type),
                new SqlParameter("@GB_ASSIGNMENT_EXTRA_CREADIT", data.Assignment_Extra_Creadit),
                new SqlParameter("@GB_ASSIGNMENT_DATE", data.Assignment_Date),
                new SqlParameter("@GB_ASSIGNMENT_DUE_DATE", data.Assignment_Due_Date),
                new SqlParameter("@GB_ASSIGNMENT_EXAM_ID", null),
                new SqlParameter("@GB_ASSIGNMENT_MAX_SCORE", data.AssignmentMaxScore),
                new SqlParameter("@GB_ASSIGNMENT_MAX_SCORE_CORRECT", data.AssignmentMaxScore_correct),
                new SqlParameter("@GB_ASSIGNMENT_IP_BY_STD", data.Assignment_IP_BY_STD.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_GRADE_COPLETED_STATUS", data.Assignment_GradeCompleted_Status.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_SCORE_VISIBLE_TO_PORTAL", data.Assignment_Score_Visible_To_Portal.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_VISIBLE_TO_PORTAL", data.Assignment_Visible_To_Portal.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_EMAIL", data.Assignment_Email.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_NARRATIVE_GRADE", data.Assignment_NarrativeGrade.Equals(true)?"T":"F"),
                new SqlParameter("@GB_ASSIGNMENT_DUE_TIME", data.Assignment_Due_Time),
                new SqlParameter("@GB_ASSIGNMENT_REMARK", data.Assignment_Remark),
                new SqlParameter("@GB_ASSIGNMENT_WEIGHTAGE", data.Assignment_Weightage),
                new SqlParameter("@GB_ASSIGNMENT_INCLUDED_IN_FINAL_GRADE", data.AssignmentIncludedInFinalGrade.Equals(true)?"T":"F"),
                new SqlParameter("@sims_category_assignment_grade_scale", data.sims_category_assignment_grade_scale),
                new SqlParameter("@GB_TERM_CODE", data.Assignment_term_code),
                new SqlParameter("@sims_category_assignment_student_type", data.sims_category_assignment_student_type),

                         });

                        assign.OPR = data.QueryResult;
                        assign.QueryResult = dr.RecordsAffected + "";
                        assign.Assignment_Code = data.Assignment_Code;
                        if (dr.Read())
                        {
                            assign.insert = true;
                            assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                            assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                            assign.Assignment_cat_name = dr["sims_gb_cat_name"].ToString();
                            assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                            assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                            assign.Assignment_cur_name = dr["Curr_Name"].ToString();
                            assign.Assignment_Date = dr["sims_gb_cat_assign_date"].ToString();
                            assign.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                            assign.Assignment_Due_Time = dr["sims_gb_cat_assign_due_time"].ToString();
                            assign.Assignment_Email = dr["sims_gb_cat_assign_email"].Equals("T") ? true : false;
                            assign.Assignment_ExamID = dr["sims_gb_cat_assign_exam_id"].ToString();
                            assign.Assignment_Extra_Creadit = dr["sims_gb_cat_assign_extra_credit"].ToString();
                            assign.Assignment_gb_name = dr["GB_NAME"].ToString();
                            assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                            assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                            assign.Assignment_grade_name = dr["Grade_Name"].ToString();
                            assign.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].Equals("T") ? true : false;
                            assign.Assignment_IP_BY_STD = dr["sims_gb_cat_assign_input_by_standard"].Equals("T") ? true : false;
                            assign.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                            assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                            assign.Assignment_NarrativeGrade = dr["sims_gb_cat_assign_narrative_grade"].Equals("T") ? true : false;
                            assign.Assignment_Remark = dr["sims_gb_cat_assign_remarks"].ToString();
                            assign.Assignment_Score_type = dr["Score_Type"].ToString();
                            assign.Assignment_Score_Visible_To_Portal = dr["sims_gb_cat_assign_score_visible_to_portal"].Equals("T") ? true : false;
                            assign.Assignment_section_code = dr["sims_section_code"].ToString();
                            assign.Assignment_section_name = dr["Section_Name"].ToString();
                            assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                            assign.Assignment_Type_Desc = dr["Assignment_Type"].ToString();
                            assign.Assignment_Visible_To_Portal = dr["sims_gb_cat_assign_visible_to_portal"].Equals("T") ? true : false;
                            assign.Assignment_Weightage = dr["sims_gb_cat_assign_weightage"].ToString();
                            assign.AssignmentIncludedInFinalGrade = dr["sims_gb_cat_assign_include_in_final_grade"].Equals("T") ? true : false;
                            assign.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                            assign.Assignment_max_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                            assign.AttrCode = dr["Attr_Code"].ToString();
                            assign.teacher_code = data.teacher_code;
                            try
                            {
                                assign.sims_category_assignment_grade_scale = dr["sims_category_assignment_grade_scale"].ToString();
                            }
                            catch (Exception e) { }

                            try
                            {
                                assign.sims_category_assignment_student_type = dr["sims_category_assignment_student_type"].ToString();
                            }
                            catch (Exception e) { }
                        }


                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                //return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, assign);
        }

        [Route("AssignmentDataOprDelete")]
        public HttpResponseMessage AssignmentDataOprDelete(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)//List<SubjectAttr> attrList
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (SIMSAPI.Models.ERP.gradebookClass.Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                  new SqlParameter("@GB_ENROLL_NUMBER", data.StudEnroll),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("AllCateories")]
        public HttpResponseMessage AllCateories(gradebook data)
        {
            List<gradebook_category> lst = new List<gradebook_category>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "AG"),
                new SqlParameter("@IOPR", "A"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number)
                });

                    while (dr.Read())
                    {
                        gradebook_category gc = new gradebook_category();
                        gc.ischecked = false;
                        gc.cnt = 0;
                        gc.AssignMents = new List<Assignment>();
                        gc.cur_code = dr["sims_cur_code"].ToString();
                        gc.cat_code = dr["sims_gb_cat_code"].ToString();
                        gc.cat_color = dr["sims_gb_cat_color_code"].ToString();
                        gc.cat_drop_low = dr["sims_gb_cat_drop_low"].ToString();
                        gc.cat_drop_per = dr["sims_gb_cat_drop_per"].ToString();
                        gc.cat_extra_points = dr["sims_gb_cat_extra_point"].ToString();
                        gc.cat_name = dr["sims_gb_cat_name"].ToString();
                        gc.cat_points_possible = dr["sims_gb_cat_points_possible"].ToString();
                        gc.cat_score_types = dr["sims_gb_cat_score_type"].ToString();
                        gc.cat_score_types_desc = dr["Score_Type"].ToString();
                        gc.cat_short_name = dr["sims_gb_cat_short_name"].ToString();
                        gc.cat_weightage_per = dr["sims_gb_cat_weightage_per"].ToString();
                        gc.cat_weightage_type = dr["sims_gb_cat_weightage_type"].ToString();
                        gc.cat_weightage_type_desc = dr["Weight_Desc"].ToString();
                        gc.cur_name = dr["Curr_Name"].ToString();
                        gc.academic_year = dr["sims_academic_year"].ToString();
                        gc.grade_name = dr["Grade_Name"].ToString();
                        gc.grade_code = dr["sims_grade_code"].ToString();
                        gc.gb_name = dr["GB_NAME"].ToString();
                        gc.gb_number = dr["sims_gb_number"].ToString();
                        gc.isIncludedInFinalGrade = dr["sims_gb_cat_include_in_final_grade"].ToString().Equals("A") ? true : false;
                        gc.publishDate = dr["sims_gb_cat_publish_assignment_on_date"].ToString();
                        gc.section_name = dr["Section_Name"].ToString();
                        gc.section_code = dr["sims_section_code"].ToString();
                        gc.status = dr["sims_gb_cat_status"].Equals("A") ? true : false;
                        gc.gb_cat_freeze_status = dr["sims_gb_cat_completed"].ToString().Equals("T") ? true : false;
                        try
                        {
                            gc.sims_category_grade_scale = dr["sims_category_grade_scale"].ToString();
                        }
                        catch (Exception e) { }
                        lst.Add(gc);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Assignment assign = new Assignment();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_cat_name = dr["sims_gb_cat_name"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_cur_name = dr["Curr_Name"].ToString();
                        assign.Assignment_Date = dr["sims_gb_cat_assign_date"].ToString();
                        assign.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                        assign.Assignment_Due_Time = dr["sims_gb_cat_assign_due_time"].ToString();
                        assign.Assignment_Email = dr["sims_gb_cat_assign_email"].Equals("T") ? true : false;
                        assign.Assignment_ExamID = dr["sims_gb_cat_assign_exam_id"].ToString();
                        assign.Assignment_Extra_Creadit = dr["sims_gb_cat_assign_extra_credit"].ToString();
                        assign.Assignment_gb_name = dr["GB_NAME"].ToString();
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_grade_name = dr["Grade_Name"].ToString();
                        assign.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].Equals("T") ? true : false;
                        assign.Assignment_IP_BY_STD = dr["sims_gb_cat_assign_input_by_standard"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_Other_Name = dr["sims_gb_cat_assign_name_ot"].ToString();
                        assign.Assignment_NarrativeGrade = dr["sims_gb_cat_assign_narrative_grade"].Equals("T") ? true : false;
                        assign.Assignment_Remark = dr["sims_gb_cat_assign_remarks"].ToString();
                        assign.Assignment_Score_type = dr["sims_gb_cat_score_type"].ToString();
                        assign.Assignment_Score_type_Desc = dr["Score_Type"].ToString();

                        //assign.cat_score_types = dr["sims_gb_cat_score_type"].ToString();
                        //assign.cat_score_types_desc = dr["Score_Type"].ToString();
                        assign.Assignment_Score_Visible_To_Portal = dr["sims_gb_cat_assign_score_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();
                        assign.Assignment_section_name = dr["Section_Name"].ToString();
                        assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        assign.Assignment_Type_Desc = dr["Assignment_Type"].ToString();
                        assign.Assignment_Visible_To_Portal = dr["sims_gb_cat_assign_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_Weightage = dr["sims_gb_cat_assign_weightage"].ToString();
                        assign.AssignmentIncludedInFinalGrade = dr["sims_gb_cat_assign_include_in_final_grade"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.Assignment_max_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.AttrCode = dr["Attr_Code"].ToString();
                        assign.sims_gb_cat_assign_freeze = dr["sims_gb_cat_assign_editable"].ToString().Equals("T") ? true : false;
                        assign.teacher_code = data.teacher_code;
                        try
                        {
                            assign.sims_category_assignment_grade_scale = dr["sims_category_assignment_grade_scale"].ToString();
                        }
                        catch (Exception e) { }

                        try
                        {
                            assign.sims_category_assignment_student_type = dr["sims_category_assignment_student_type"].ToString();
                        }
                        catch (Exception e) { }

                        try
                        {
                            gradebook_category gc = lst.Single(q => q.gb_number == assign.Assignment_gb_number && q.cat_code == assign.Assignment_cat_code);
                            //&& q.cur_code == assign.Assignment_cur_code && q.grade_code == assign.Assignment_grade_code 
                            //&& q.section_code == assign.Assignment_section_code && q.academic_year == assign.Assignment_academic_year
                            //);
                            if (gc.AssignMents == null) gc.AssignMents = new List<Assignment>();
                            gc.AssignMents.Add(assign);
                        }


                        catch (Exception)
                        {
                        }

                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("AllAssignments")]
        public HttpResponseMessage AllAssignments(gradebook data)
        {
            List<Assignment> lst = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "GC"),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.cat_code)
                });

                    while (dr.Read())
                    {
                        Assignment assign = new Assignment();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_cat_name = dr["sims_gb_cat_name"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_cur_name = dr["Curr_Name"].ToString();
                        assign.Assignment_Date = dr["sims_gb_cat_assign_date"].ToString();
                        assign.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                        assign.Assignment_Due_Time = dr["sims_gb_cat_assign_due_time"].ToString();
                        assign.Assignment_Email = dr["sims_gb_cat_assign_email"].Equals("T") ? true : false;
                        assign.Assignment_ExamID = dr["sims_gb_cat_assign_exam_id"].ToString();
                        assign.Assignment_Extra_Creadit = dr["sims_gb_cat_assign_extra_credit"].ToString();
                        assign.Assignment_gb_name = dr["GB_NAME"].ToString();
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_grade_name = dr["Grade_Name"].ToString();
                        assign.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].Equals("T") ? true : false;
                        assign.Assignment_IP_BY_STD = dr["sims_gb_cat_assign_input_by_standard"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_NarrativeGrade = dr["sims_gb_cat_assign_narrative_grade"].Equals("T") ? true : false;
                        assign.Assignment_Remark = dr["sims_gb_cat_assign_remarks"].ToString();
                        assign.Assignment_Score_type = dr["sims_gb_cat_score_type"].ToString();
                        assign.Assignment_Score_type_Desc = dr["Score_Type"].ToString();
                        assign.Assignment_Score_Visible_To_Portal = dr["sims_gb_cat_assign_score_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();
                        assign.Assignment_section_name = dr["Section_Name"].ToString();
                        assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        assign.Assignment_Type_Desc = dr["Assignment_Type"].ToString();
                        assign.Assignment_Visible_To_Portal = dr["sims_gb_cat_assign_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_Weightage = dr["sims_gb_cat_assign_weightage"].ToString();
                        assign.AssignmentIncludedInFinalGrade = dr["sims_gb_cat_assign_include_in_final_grade"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.Assignment_max_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.AttrCode = dr["Attr_Code"].ToString();
                        assign.teacher_code = data.teacher_code;
                        lst.Add(assign);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("AllGradeSectionWiseStudent")]
        public HttpResponseMessage AllGradeSectionWiseStudent(gradebook gb)
        {
            List<Students> lst = new List<Students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "SS"),
                new SqlParameter("@CUR_CODE", gb.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.academic_year),
                new SqlParameter("@GRADE_CODE", gb.grade_code),
                new SqlParameter("@SECTION_CODE", gb.section_code),
                new SqlParameter("@GB_NUMBER", gb.gb_number),
                new SqlParameter("@GB_CAT_NUMBER", gb.cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", gb.assign_code),
                new SqlParameter("@assign_type", gb.assign_type)

                });

                    while (dr.Read())
                    {
                        Students stu = new Students();
                        stu.Stud_Enroll = dr["sims_student_enroll_number"].ToString();
                        stu.Stud_Full_Name = dr["student_name"].ToString();
                        stu.Stud_AcademicYear = dr["sims_academic_year"].ToString();
                        stu.Stud_CurrCode = dr["sims_cur_code"].ToString();
                        stu.Stud_GradeCode = dr["sims_grade_code"].ToString();
                        stu.Stud_SectionCode = dr["sims_section_code"].ToString();
                        stu.Stud_GradeName = dr["sims_grade_name_en"].ToString();
                        stu.Stud_SectionName = dr["sims_section_name_en"].ToString();
                        stu.Stud_status = true;
                        lst.Add(stu);
                    }
                }

            }

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("AllGradeBookNamegradesectionwise")]
        public HttpResponseMessage AllGradeBookNamegradesectionwise(gradebook gb)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "GN"),
                new SqlParameter("@CUR_CODE", gb.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.academic_year),
                new SqlParameter("@GRADE_CODE", gb.sims_grade_code),
                new SqlParameter("@SECTION_CODE", gb.sims_section_code),
                new SqlParameter("@GB_TERM_CODE", gb.gb_term_code),
                 new SqlParameter("@GB_TEACHER_CODE", gb.teacher_code),


                });

                    while (dr.Read())
                    {
                        gradebook stu = new gradebook();
                        stu.gb_number = dr["sims_gb_number"].ToString();
                        stu.gb_name = dr["sims_gb_name"].ToString();
                        lst.Add(stu);
                    }
                }

            }

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("AllGradeBookNamegradesectionwiseassignment")]
        public HttpResponseMessage AllGradeBookNamegradesectionwiseassignment(gradebook gb)
        {
            List<Students> studList = new List<Students>();
            gradebook o = new gradebook();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "GM"),
                new SqlParameter("@CUR_CODE", gb.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.academic_year),
                new SqlParameter("@GRADE_CODE", gb.sims_grade_code),
                new SqlParameter("@SECTION_CODE", gb.sims_section_code),
                new SqlParameter("@GB_TERM_CODE", gb.gb_term_code),
                 new SqlParameter("@GB_NUMBER", gb.gb_number),
                });

                    #region Gradebook Collection Loop
                    if (dr.Read())
                    {
                        o.cur_code = dr["sims_cur_code"].ToString();
                        o.academic_year = dr["sims_academic_year"].ToString();
                        o.grade_code = dr["sims_grade_code"].ToString();
                        o.section_code = dr["sims_section_code"].ToString();
                        o.gb_name = dr["sims_gb_name"].ToString();
                        o.gb_type = dr["sims_gb_type"].ToString();
                        o.gb_number = dr["sims_gb_number"].ToString();
                        o.gb_start_date = dr["sims_gb_start_date"].ToString();
                        o.gb_end_date = dr["sims_gb_end_date"].ToString();
                        o.gb_term_code = dr["sims_gb_term_code"].ToString();
                        o.gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                        o.gb_status = dr["sims_gb_status"].Equals("A") ? true : false;
                        o.sims_subject_name = dr["Subject_name"].ToString();
                        o.Categories = new List<gradebook_category>();
                    }
                    dr.NextResult();
                    #endregion

                    #region Gradebook Category Loop
                    while (dr.Read())
                    {
                        gradebook_category gc = new gradebook_category();
                        gc.cur_code = dr["sims_cur_code"].ToString();
                        gc.cat_code = dr["sims_gb_cat_code"].ToString();
                        gc.cat_name = dr["sims_gb_cat_name"].ToString();
                        gc.cat_score_types = dr["sims_gb_cat_score_type"].ToString();
                        gc.academic_year = dr["sims_academic_year"].ToString();
                        gc.grade_code = dr["sims_grade_code"].ToString();
                        gc.gb_number = dr["sims_gb_number"].ToString();
                        gc.section_code = dr["sims_section_code"].ToString();
                        gc.AssignMents = new List<Assignment>();
                        if (o != null)
                        {
                            o.Categories.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region Assignment Collection Loop
                    while (dr.Read())
                    {
                        Assignment assign = new Assignment();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();
                        assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();

                        gradebook_category gc = (o.Categories.Single(q => q.cat_code == assign.Assignment_cat_code));
                        gc.AssignMents.Add(assign);
                    }
                    dr.NextResult();

                    while (dr.Read())
                    {
                        Assignment std = new Assignment();
                        Students s = new Students();
                        s.AssignMents = new List<Assignment>();

                        string str = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        std.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        std.Assignment_Code1 = dr["sims_gb_cat_assign_number1"].ToString();
                        std.Assignment_cat_code1 = dr["sims_gb_Cat_code1"].ToString();
                        std.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        std.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        std.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        std.Assignment_section_code = dr["sims_section_code"].ToString();
                        std.sims_gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                        std.Assignment_cur_code = gb.cur_code + "";
                        std.Assignment_academic_year = gb.academic_year + "";
                        std.Stud_Enroll = str + "";
                        std.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        std.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        std.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        std.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        std.Assignment_gb_number = gb.gb_number + "";
                        s.Stud_Enroll = str + "";
                        s.Stud_Full_Name = dr["student_name"].ToString();
                        std.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                        std.Assignment_final_grade_desc = dr["assignment_final_grade_desc"].ToString();
                        std.Assignment_Score_type = dr["sims_gb_cat_score_type_name"].ToString();
                        std.sims_mark_final_grade_code = dr["sims_gb_cat_assign_final_grade"].ToString();
                        std.Assignment_Completed_Date = dr["sims_gb_cat_assign_completed_date"].ToString();
                        std.Assignment_subject_code = dr["sims_gb_subject_code"].ToString();
                        std.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].ToString().Equals("T") ? true : false;
                        var v = from p in studList where p.Stud_Enroll == str select p;

                        if (std.Assignment_Code != "")
                        {
                            std.insert = true;

                        }
                        else
                        {
                            std.insert = false;
                        }
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).AssignMents.Add(std);

                        }
                        else
                        {
                            studList.Add(s);
                            s.AssignMents.Add(std);
                        }

                    }
                }
            }
            #endregion

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            object[] ob = new object[2];
            ob[0] = new object();
            ob[1] = new object();
            ob[0] = o;
            ob[1] = studList;
            return Request.CreateResponse(HttpStatusCode.OK, ob);
        }

        public List<SIMSAPI.Models.ERP.gradebookClass.Assignment> GetAssignMents(SIMSAPI.Models.ERP.gradebookClass.Assignment obj)
        {
            List<SIMSAPI.Models.ERP.gradebookClass.Assignment> allstd = new List<SIMSAPI.Models.ERP.gradebookClass.Assignment>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "BB"),
                new SqlParameter("@CUR_CODE", obj.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", obj.Assignment_academic_year),
                new SqlParameter("@GB_NUMBER", obj.Assignment_gb_number),
                new SqlParameter("@ENROLL_NUMBER", obj.Stud_Enroll),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SIMSAPI.Models.ERP.gradebookClass.Assignment ass = new SIMSAPI.Models.ERP.gradebookClass.Assignment();
                            ass.Assignment_Comment = dr["sims_gb_cat_assign_comment"].ToString();
                            ass.Assignment_final_grade_desc = dr["Grade_Desc"].ToString();
                            ass.Assignment_Status = dr["sims_gb_cat_assign_status"].ToString();
                            ass.Stud_Enroll = obj.Stud_Enroll.ToString();
                            ass.Assignment_Status1 = dr["StatusDesc"].ToString();
                            ass.Assignment_Score_type = dr["ScoreType"].ToString();
                            ass.Assignment_gb_name = dr["sims_gb_name"].ToString();
                            ass.Assignment_cat_name = dr["sims_gb_cat_name"].ToString();
                            ass.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                            ass.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                            ass.Assignment_gb_number = dr["sims_gb_number"].ToString();
                            ass.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                            ass.Assignment_Completed_Date = dr["sims_gb_cat_assign_completed_date"].ToString();
                            ass.Assignment_max_point = dr["sims_gb_cat_assign_max_point"].ToString();
                            ass.Assignment_max_correct = dr["sims_gb_cat_assign_max_correct"].ToString();
                            ass.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                            ass.Assignment_Score_type = dr["sims_gb_cat_score_type_name"].ToString();
                            ass.sims_mark_final_grade_code = dr["sims_gb_cat_assign_final_grade"].ToString();
                            ass.AssignmentMaxScore = obj.AssignmentMaxScore.ToString();
                            allstd.Add(ass);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return allstd;
        }

        [Route("AssignmentDataOprInsert")]
        public HttpResponseMessage AssignmentDataOprInsert(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                new SqlParameter("@GB_ENROLL_NUMBER", data.StudEnroll),
                new SqlParameter("@GB_ASSIGNMENT_MAX_SCORE",data.AssignmentMaxScore),
                new SqlParameter("@GB_ASSIGNMENT_MAX_SCORE_CORRECT",data.Assignment_max_correct)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("AllSubjectWiseAssignmentStudentSkip")]//Skip Student Subject Wise
        public HttpResponseMessage AllSubjectWiseAssignmentStudentSkip(gradebook gb)
        {
            List<Students> studList = new List<Students>();
            List<Subject> sub = new List<Subject>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_SKIP_STUDNET_SUBJECTWISE_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "SS"),
                new SqlParameter("@CUR_CODE", gb.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.academic_year),
                new SqlParameter("@GRADE_CODE", gb.sims_grade_code ==""?null:gb.sims_grade_code),
                new SqlParameter("@SECTION_CODE", gb.sims_section_code==""?null:gb.sims_section_code),
                new SqlParameter("@GB_TERM_CODE", gb.gb_term_code),
                new SqlParameter("@ENROLL_NUMBER", gb.enroll_number==""?null:gb.enroll_number),
                });

                    #region Subject Collection Loop
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Subject o = new Subject();
                            o.SubjectCode = dr["sims_gb_subject_code"].ToString();
                            o.SubjectName = dr["sims_subject_name_en"].ToString();
                            sub.Add(o);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region Student And  Subject  Collection Loop
                    while (dr.Read())
                    {
                        Students gc = new Students();
                        gc.subject_list = new List<Subject>();

                        Subject s = new Subject();
                        string str = dr["sims_enroll_number"].ToString();
                        gc.Stud_Enroll = dr["sims_enroll_number"].ToString();
                        gc.Stud_Full_Name = dr["Student_name"].ToString();
                        //gc.Stud_status = dr["status"].ToString().Equals("1") ? true : false;
                        s.SubjectCurr = dr["sims_cur_code"].ToString();
                        s.SubjectAcademic = dr["sims_academic_year"].ToString();
                        s.SubjectGrade = dr["sims_grade_code"].ToString();
                        s.SubjectSection = dr["sims_section_code"].ToString();
                        s.status = dr["status"].ToString().Equals("1") ? true : false;
                        s.SubjectCode = dr["sims_subject_code"].ToString();
                        s.stud_enroll = dr["sims_enroll_number"].ToString();
                        var v = from p in studList where p.Stud_Enroll == str select p;

                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).subject_list.Add(s);

                        }
                        else
                        {
                            studList.Add(gc);
                            gc.subject_list.Add(s);
                        }
                    }
                    dr.Close();
                    #endregion

                }

            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            object[] ob = new object[2];
            ob[0] = new object();
            ob[1] = new object();
            ob[0] = sub;
            ob[1] = studList;
            return Request.CreateResponse(HttpStatusCode.OK, ob);
        }

        [Route("skipstudentforSubjectwise")] //Skip Student Subject Wise
        public HttpResponseMessage skipstudentforSubjectwise(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_SKIP_STUDNET_SUBJECTWISE_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GB_TERM_CODE", data.term_code),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_SUBJECT_CODE", data.Assignment_subject_code),
                new SqlParameter("@ENROLL_NUMBER", data.StudEnroll),
                new SqlParameter("@GB_CAT_NAME", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGN_NAME", data.Assignment_Code),
                new SqlParameter("@GB_NUMBER1",data.Assignment_gb_number)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("ActiveInactiveGradebooks")] //Skip Student Subject Wise
        public HttpResponseMessage ActiveInactiveGradebooks(List<gradebook> data1)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (gradebook data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ACTIVE_INACTIVE_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "DG"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GB_TERM_CODE", data.gb_term_code),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("ActiveInactiveGradebooks_category")] //Skip Student Subject Wise
        public HttpResponseMessage ActiveInactiveGradebooks_category(List<gradebook_category> data1)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (gradebook_category data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ACTIVE_INACTIVE_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "DC"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_CAT_NUMBER",  data.cat_code)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("ActiveInactiveGradebooks_category_assign")] //Skip Student Subject Wise
        public HttpResponseMessage ActiveInactiveGradebooks_category_assign(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ACTIVE_INACTIVE_NEW_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "DA"),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER",  data.Assignment_cat_code),
                  new SqlParameter("@GB_ASSIGNMENT_NUMBER",data.Assignment_Code)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getcateogryforstudentskip")] //Skip Student Subject Wise
        public HttpResponseMessage getcateogryforstudentskip(string gb_number)
        {
            List<gradebook_category> lst = new List<gradebook_category>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ACTIVE_INACTIVE_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "GC"),
                new SqlParameter("@GB_NUMBER", gb_number=="null"?null:gb_number)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            gradebook_category g = new gradebook_category();
                            g.cat_name = dr["sims_gb_cat_name"].ToString();
                            lst.Add(g);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getassignforstudentskip")] //Skip Student Subject Wise
        public HttpResponseMessage getassignforstudentskip(string gb_number, string gb_cat_code)
        {
            List<Assignment> lst = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ACTIVE_INACTIVE_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "CA"),
                new SqlParameter("@GB_NUMBER",gb_number=="null"?null:gb_number),
                new SqlParameter("@GB_CAT_NUMBER", gb_cat_code=="null"?null:gb_cat_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Assignment g = new Assignment();
                            g.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                            lst.Add(g);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        #endregion

        #region Others


        [Route("GetTeachers")]
        public HttpResponseMessage GetTeachers()
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_co_scholastic_descriptive_indicator_proc]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "T"),

                         });

                    while (dr.Read())
                    {
                        gradesectionsubjectteacher gst = new gradesectionsubjectteacher();
                        gst.sims_teacher_name = dr["sims_teacher_name"].ToString();
                        gst.sims_teacher_code = dr["sims_employee_code"].ToString();
                        lst.Add(gst);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("getAllTeacherName")]
        public HttpResponseMessage getAllTeacherName(string gb_number)
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "TN"),
                 new SqlParameter("@GB_NUMBER", gb_number),

                         });

                    while (dr.Read())
                    {
                        gradesectionsubjectteacher gst = new gradesectionsubjectteacher();
                        gst.sims_teacher_name = dr["sims_teacher_name"].ToString();
                        gst.sims_section_name_en = dr["sims_section_name_en"].ToString();
                        gst.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                        gst.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                        gst.sims_gb_teacher_status = dr["sims_gb_teacher_status"].Equals("A") ? true : false;
                        gst.sims_teacher_code = dr["sims_employee_code"].ToString();
                        gst.sims_homeroom_teacher_code = dr["sims_employee_code"].ToString();
                        gst.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                        lst.Add(gst);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("GetGradeSectionSubjectTeacher")]
        public HttpResponseMessage GetGradeSectionSubjectTeacher(string cur_code, string academic_year, string subject_code)
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_proc]",
                        new List<SqlParameter>()
                         {

                        new SqlParameter("@opr", "Z"),
                        new SqlParameter("@sims_academic_year", academic_year),
                        new SqlParameter("@sims_subject_code", subject_code),
                        new SqlParameter("@sims_cur_code", cur_code)

                         });

                    while (dr.Read())
                    {
                        gradesectionsubjectteacher gst = new gradesectionsubjectteacher();

                        str = dr["sims_teacher_code"].ToString();
                        var v = (from p in lst where p.sims_teacher_code == str select p);

                        if (v.Count() == 0)
                        {
                            gst.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            gst.gradesection = new List<GradeSection>();
                            gst.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            gst.sims_employee_code = dr["sims_employee_code"].ToString();
                            gst.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            gst.sims_subject_code = dr["sims_subject_code"].ToString();
                            gst.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            gst.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            gst.sims_section_code = dr["sims_bell_section_code"].ToString();
                            gst.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            lst.Add(gst);
                        }

                        else
                        {
                            GradeSection sb = new GradeSection();
                            sb.sims_section_code = dr["sims_bell_section_code"].ToString();
                            sb.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            sb.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sb.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            v.ElementAt(0).gradesection.Add(sb);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("GetGradeSectionHomeroomTeacher")]
        public HttpResponseMessage GetGradeSectionHomeroomTeacher(string cur_code, string academic_year, string Homeroom_code)
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "HT"),
                 new SqlParameter("@ACADEMIC_YEAR", academic_year),
                    new SqlParameter("@GB_HOMEROOM_CODE", Homeroom_code)
                         });

                    while (dr.Read())
                    {
                        gradesectionsubjectteacher gst = new gradesectionsubjectteacher();
                        gst.sims_teacher_name = dr["sims_teacher_name"].ToString();
                        gst.sims_employee_code = dr["sims_employee_code"].ToString();
                        gst.sims_homeroom_teacher_code = dr["sims_homeroom_teacher_code"].ToString();
                        gst.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                        lst.Add(gst);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GradeBookNameCreationBySubject")]
        public HttpResponseMessage GradeBookNameCreationBySubject(string opr, string cur_code)
        {
            List<sectionsubject> lst = new List<sectionsubject>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", opr),
                  new SqlParameter("@CUR_CODE", cur_code)
                         });
                    while (dr.Read())
                    {
                        sectionsubject su = new sectionsubject();
                        su.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                        su.sims_subject_code = dr["sims_subject_code"].ToString();
                        lst.Add(su);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("GradeBookNameCreationByHomeRoom")]
        public HttpResponseMessage GradeBookNameCreationByHomeRoom(string opr)
        {
            List<sectionsubject> lst = new List<sectionsubject>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", opr)
                         });
                    while (dr.Read())
                    {
                        sectionsubject su = new sectionsubject();

                        su.sims_cur_code = dr["sims_cur_code"].ToString();
                        su.sims_academic_year = dr["sims_academic_year"].ToString();
                        su.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                        su.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                        su.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                        su.sims_section_name_en = dr["sims_section_name_en"].ToString();
                        su.sims_section_code = dr["sims_section_code"].ToString();
                        su.sims_grade_code = dr["sims_grade_code"].ToString();
                        lst.Add(su);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("Getteachernamesubjectwise")]
        public HttpResponseMessage Getteachernamesubjectwise(string sub_code)
        {
            List<sectionsubject> lst = new List<sectionsubject>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C"),
                new SqlParameter("@Subject_code", sub_code)
                         });
                    while (dr.Read())
                    {
                        sectionsubject su = new sectionsubject();
                        su.sims_teacher_code = dr["sims_teacher_code"].ToString();
                        su.sims_teacher_name = dr["sims_teacher_name"].ToString();
                        su.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                        su.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                        su.sims_section_name_en = dr["sims_section_name_en"].ToString();
                        su.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                        su.sims_section_code = dr["sims_bell_section_code"].ToString();
                        su.sims_grade_code = dr["sims_bell_grade_code"].ToString();

                        lst.Add(su);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("GetGradeBookTypes")]
        public HttpResponseMessage GetGradeBookTypes()
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "G")
                         });
                    while (dr.Read())
                    {
                        gradebook gd = new gradebook();
                        gd.gb_type = dr["sims_appl_parameter"].ToString();
                        gd.gb_type_desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(gd);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("updateGgradingcompleted")]
        public HttpResponseMessage updateGgradingcompleted(SIMSAPI.Models.ERP.gradebookClass.Assignment obj)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                        new SqlParameter("@OPR","ZP"),
                        new SqlParameter("@CUR_CODE",obj.Assignment_cur_code),
                        new SqlParameter("@ACADEMIC_YEAR" ,obj.Assignment_academic_year),
                        new SqlParameter("@GRADE_CODE",obj.Assignment_grade_code),
                        new SqlParameter("@SECTION_CODE",obj.Assignment_section_code),
                        new SqlParameter("@GB_NUMBER",obj.Assignment_gb_number),
                        new SqlParameter("@GB_CAT_NUMBER",obj.Assignment_cat_code),
                        new SqlParameter("@GB_ASSIGNMENT_NUMBER",obj.Assignment_Code),
                        new SqlParameter("@GB_ASSIGNMENT_GRADE_COPLETED_STATUS",obj.Assignment_GradeCompleted_Status.Equals(true)?"T":"F")
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }

        [Route("All_GetTerms")]
        public HttpResponseMessage All_GetTerms(gradebook gb)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR","TT"),
                new SqlParameter("@CUR_CODE",gb.sims_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.sims_academic_year),
                new SqlParameter("@GB_TERM_CODE", gb.sims_gb_term_name)
                         });

                    while (dr.Read())
                    {
                        gradebook o = new gradebook();
                        o.termStartDate = dr["sims_term_start_date"].ToString();
                        o.termEndDate = dr["sims_term_end_date"].ToString();
                        lst.Add(o);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("InsertUpdateGrades")]
        public HttpResponseMessage InsertUpdateGrades(GradeScheme obj, string opr)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[MarkGRADE_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", opr),
                new SqlParameter("@GradeName", obj.GradeName),
                new SqlParameter("@Group_code", obj.GradeGroupCode),
                new SqlParameter("@GradeLow", obj.GradeLow),
                new SqlParameter("@GradeHigh", obj.GradeHigh),
                new SqlParameter("@Description", obj.Description),
                new SqlParameter("@TeacherCode", obj.TeacherCode),
                new SqlParameter("@GradeCode", obj.GradeCode)

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }

        [Route("GetTeacherDetails")]
        public HttpResponseMessage GetTeacherDetails(Subject UserCode)
        {
            TeacherDetails td = new TeacherDetails();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "S"),
                new SqlParameter("@CUR_CODE", UserCode.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", UserCode.SubjectAcademic),
                new SqlParameter("@GB_TEACHER_CODE", UserCode.SubTeacherEmpId)
                });
                    td.Subjects = new List<Subject>();
                    while (dr.Read())
                    {
                        Subject s = new Subject();
                        td.TeacherName = dr["TeacherName"].ToString();
                        td.TeacherId = dr["sims_bell_teacher_code"].ToString();
                        s.TeacherName = dr["TeacherName"].ToString();
                        s.SubTeacherEmpId = dr["teacher_id"].ToString();
                        s.SubTeacherId = dr["sims_bell_teacher_code"].ToString();
                        s.SubjectCode = dr["sims_bell_subject_code"].ToString();
                        s.SubjectGrade = dr["sims_bell_grade_code"].ToString();
                        s.SubjectGrade_Name = dr["GradeName"].ToString();
                        s.SubjectGroupCode = dr["Subject_Group_Code"].ToString();
                        s.SubjectName = dr["SubjectName"].ToString();
                        s.SubjectSection = dr["sims_bell_section_code"].ToString();
                        s.SubjectSection_Name = dr["SectionName"].ToString();
                        s.SubjectCurr = dr["Subject_Curr"].ToString();
                        s.SubjectAcademic = dr["Subject_Academic"].ToString();
                        td.Subjects.Add(s);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, td);

        }

        public List<Students> GetStudents(Subject sb)
        {
            List<Students> lst = new List<Students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "G"),
                new SqlParameter("@CUR_CODE", sb.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", sb.SubjectAcademic),
                new SqlParameter("@GRADE_CODE", sb.SubjectGrade),
                new SqlParameter("@SECTION_CODE", sb.SubjectSection),
                new SqlParameter("@GB_NUMBER", sb.GradeBookNumber),
                new SqlParameter("@GB_CAT_NUMBER", sb.categoryNumber),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", sb.AssignmentNumber)
                         });
                    while (dr.Read())
                    {
                        Students s = new Students();
                        s.Stud_Enroll = dr["sims_enroll_number"].ToString();
                        s.Stud_FirstName = dr["StudFirstName"].ToString();
                        s.Stud_MidName = dr["StudMiddName"].ToString();
                        s.Stud_LastName = dr["StudLastName"].ToString();
                        s.Included = "No";// dr["IsAssigned"].ToString();
                        s.Stud_Full_Name = s.Stud_FirstName + ' ' + s.Stud_MidName + ' ' + s.Stud_LastName;
                        s.Stud_AcademicYear = dr["sims_academic_year"].ToString();
                        s.Stud_CurrCode = dr["sims_cur_code"].ToString();
                        s.Stud_GradeCode = dr["sims_grade_code"].ToString();
                        s.Stud_SectionCode = dr["sims_section_code"].ToString();
                        lst.Add(s);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Assignment s = new Assignment();
                        s.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        s.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        s.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        s.Assignment_section_code = dr["sims_section_code"].ToString();
                        s.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        s.Assignment_gb_name = dr["GradeBookName"].ToString();
                        s.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        s.Assignment_cat_name = dr["CategoryName"].ToString();
                        s.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        s.Assignment_Name = dr["AssignMentName"].ToString();
                        s.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                        s.Assignment_final_grade = dr["sims_gb_cat_assign_final_grade"].ToString();
                        s.Assignment_final_grade_desc = dr["Grade_Desc"].ToString();
                        s.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        s.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        s.Assignment_max_correct = dr["sims_gb_cat_assign_max_correct"].ToString();
                        s.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_correct"].ToString();
                        s.Assignment_max_point = dr["sims_gb_cat_assign_max_point"].ToString();
                        s.Assignment_Comment = dr["sims_gb_cat_assign_comment"].ToString();
                        s.Assignment_Status = dr["sims_gb_cat_assign_status"].ToString();
                        s.Assignment_NarrativeGrade = dr["sims_gb_cat_assign_narrative_grade"].Equals("T") ? true : false;
                        s.Color_Code = dr["Color_Code"].ToString();
                        s.Assignment_GradeCompleted_Status = dr["GradingCompletedStatus"].Equals("T") ? true : false;
                        s.Assignment_Completed_Date = dr["assign_completed_date"].ToString();
                        s.Assignment_Due_Date = dr["AssignMent_Due_Date"].ToString();
                        string enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        s.Assignment_Score_type = dr["ScoreType"].ToString();
                        s.StudEnroll = enroll;
                        try
                        {
                            Students st = lst.Single(q => q.Stud_Enroll == enroll);
                            if (st.AssignMents == null) st.AssignMents = new List<Assignment>();
                            st.Included = "Yes";
                            st.AssignMents.Add(s);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Assignment s = new Assignment();
                        s.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        s.Assignment_section_code = dr["sims_section_code"].ToString();
                        s.Assignment_gb_number = dr["GradeBookName"].ToString();
                        s.Assignment_gb_name = dr["sims_gb_number"].ToString();
                        s.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        s.Assignment_cat_name = dr["CategoryName"].ToString();
                        s.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        s.Assignment_Name = dr["AssignMentName"].ToString();
                        s.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        s.Assignment_Mark = dr["sims_gb_cat_assign_subject_attribute_mark"].ToString();
                        s.AttrGroupCode = dr["sims_subject_attribute_group_code"].ToString();
                        s.AttrCode = dr["sims_subject_attribute_code"].ToString();
                        s.Assignment_final_grade = dr["sims_gb_cat_assign_subject_attribute_final_grade"].ToString();
                        s.Assignment_final_grade_desc = dr["Grade_Desc"].ToString();
                        s.Assignment_Completed_Date = dr["subject_attribute_completed_date"].ToString();
                        s.Assignment_Score_type = dr["ScoreType"].ToString();
                        string enroll = dr["sims_subject_attribute_enroll_number"].ToString();
                        s.StudEnroll = enroll;
                        try
                        {
                            Students st = lst.Single(q => q.Stud_Enroll == enroll);
                            if (st.AssignMents == null) st.AssignMents = new List<Assignment>();
                            st.Included = "Yes";
                            st.AssignMents.Add(s);
                            lst.Add(st);

                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lst;
        }

        [Route("GetWeightType")]
        public HttpResponseMessage GetWeightType()
        {
            List<gradebook_category> lst = new List<gradebook_category>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "W")
                });
                    while (dr.Read())
                    {
                        gradebook_category o = new gradebook_category();
                        o.cat_weightage_type = dr["sims_appl_parameter"].ToString();
                        o.cat_weightage_type_desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetUserClasses")]
        public HttpResponseMessage GetUserClasses(ClasssDetails data)
        {
            List<ClasssDetails> lst = new List<ClasssDetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_group_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "T"),
                new SqlParameter("@sims_co_scholastic_employee_code", data.employee_code)
                         });
                    while (dr.Read())
                    {
                        ClasssDetails o = new ClasssDetails();
                        o.curr_code = dr["sims_cur_code"].ToString();
                        o.curr_name = dr["sims_cur_full_name_en"].ToString();

                        o.academic_year = dr["sims_academic_year"].ToString();
                        o.academic_year_desc = dr["sims_academic_year_description"].ToString();
                        o.Aycur = dr["ay_Cur"].ToString();

                        o.grade_code = dr["sims_grade_code"].ToString();
                        o.grade_name = dr["sims_grade_name_en"].ToString();
                        o.GrCur = dr["gr_Cur"].ToString();
                        o.GrAca = dr["gr_Aca"].ToString();

                        o.section_code = dr["sims_section_code"].ToString();
                        o.section_name = dr["sims_section_name_en"].ToString();
                        o.SecCur = dr["sec_Cur"].ToString();
                        o.SecAcy = dr["sec_Aca"].ToString();
                        o.SecGr = dr["sec_Grade"].ToString();

                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetStudentStatus")]
        public HttpResponseMessage GetStudentStatus()
        {
            List<StudentStatus> lst = new List<StudentStatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "T")
                         });

                    while (dr.Read())
                    {
                        StudentStatus o = new StudentStatus();
                        o.status_id = dr["sims_appl_parameter"].ToString();
                        o.status_desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
                StudentStatus o1 = new StudentStatus();
                o1.status_id = "Clear";
                o1.status_desc = "Clear";
                lst.Add(o1);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetScoreTypes")]
        public HttpResponseMessage GetScoreTypes()
        {
            List<gradebook_category> lst = new List<gradebook_category>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "Z")
                         });

                    while (dr.Read())
                    {
                        gradebook_category o = new gradebook_category();
                        o.cat_score_types = dr["sims_appl_parameter"].ToString();
                        o.cat_score_types_desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetScoreTypes_co_scholastic")]
        public HttpResponseMessage GetScoreTypes_co_scholastic()
        {
            List<gradebook_category> lst = new List<gradebook_category>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "E")
                         });

                    while (dr.Read())
                    {
                        gradebook_category o = new gradebook_category();
                        o.cat_score_types = dr["sims_appl_parameter"].ToString();
                        o.cat_score_types_desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetAssignmentTypes")]
        public HttpResponseMessage GetAssignmentTypes()
        {
            List<Assignment> lst = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A")
                         });
                    while (dr.Read())
                    {
                        Assignment o = new Assignment();
                        o.Assignment_Type = dr["sims_appl_parameter"].ToString();
                        o.Assignment_Type_Desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetAssignmentforStudtype")]
        public HttpResponseMessage GetAssignmentforStudtype()
        {
            List<Assignment> lst = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_parameter_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A")
                         });
                    while (dr.Read())
                    {
                        Assignment o = new Assignment();
                        o.Assignment_Type = dr["sims_appl_parameter"].ToString();
                        o.Assignment_Type_Desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetAssignmentstudshow")]
        public HttpResponseMessage GetAssignmentstudshow()
        {
            List<Assignment> lst = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_parameter_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "B")
                         });
                    while (dr.Read())
                    {
                        Assignment o = new Assignment();
                        o.Assignment_Type = dr["sims_appl_parameter"].ToString();
                        o.Assignment_Type_Desc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("MarkGradeDataOpr")]
        public HttpResponseMessage MarkGradeDataOpr(GradeScheme obj)
        {

            bool inserted = false;
            List<GradeScheme> lst = new List<GradeScheme>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarkGRADE_PROC",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@OPR", "IG"),
                    new SqlParameter("@TeacherCode", null),
                    new SqlParameter("@CurrCode", null),
                    new SqlParameter("@AcademicYear", null),
                    new SqlParameter("@group_name", obj.GradeName),
                    new SqlParameter("@Description", obj.Description),
                    new SqlParameter("@narr_grade_status",obj.status==true?"A":"I")

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("GradeScaleDataOpr")]
        public HttpResponseMessage GradeScaleDataOpr(string teachercode, string opr)
        {
            List<NarrativeGrade> lst = new List<NarrativeGrade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[MarkGRADE_PROC]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", opr),
                new SqlParameter("@TeacherCode", teachercode)
                //new SqlParameter("@CurrCode", obj.CurrCode);
                //new SqlParameter("@AcademicYear", obj.AcademicYear);
                         });
                    while (dr.Read())
                    {
                        NarrativeGrade o = new NarrativeGrade();
                        o.GradeCode = dr["narr_grade_group_code"].ToString();
                        o.narr_grade_group_name = dr["narr_grade_group_name"].ToString();
                        o.GradeGroupCode = dr["narr_grade_group_code"].ToString();
                        o.GradeGroupDesc = dr["narr_grade_group_name"].ToString();
                        try
                        {
                            o.narr_grade_status = dr["narr_grade_status"].ToString();
                        }
                        catch (Exception e)
                        {

                          
                        }
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDGradeScaleDataOpr")]
        public HttpResponseMessage GradeScaleDataOpr(List<NarrativeGrade> data)
        {
            string insert = string.Empty;
            List<NarrativeGrade> lst = new List<NarrativeGrade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (NarrativeGrade obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[MarkGRADE_PROC]",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", obj.opr),
                new SqlParameter("@GradeName", obj.sims_mark_grade_name),
                new SqlParameter("@GradeCode",obj.sims_mark_grade_code),
                  new SqlParameter("@Group_code", obj.GradeGroupCode),
                new SqlParameter("@GradeLow",decimal.Parse(obj.sims_mark_grade_low)),
                  new SqlParameter("@GradeHigh", decimal.Parse(obj.sims_mark_grade_high)),
                  new SqlParameter("@Points",decimal.Parse(obj.sims_mark_grade_point)),
                 new SqlParameter("@Description", obj.sims_mark_grade_description)

                         });
                        if (dr.Read() && dr.RecordsAffected > 0)
                        {
                            insert = dr["Grade_cnat_deleted"].ToString();
                        }
                        else
                        {
                            if (obj.opr == "IM")
                            {
                                insert = "Grade Scale Not Inserted";
                            }
                            else if (obj.opr == "UM")
                            {
                                insert = "Grade Scale Not Updated";
                            }
                            else
                            {
                                insert = "Grade Scale Not Deleted";
                            }
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                insert = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

        [Route("getGradeScaleData")]
        public HttpResponseMessage getGradeScaleData()
        {

            List<NarrativeGrade> lst = new List<NarrativeGrade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[MarkGRADE_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "M"),
                new SqlParameter("@TeacherCode", null),
                         });
                    while (dr.Read())
                    {
                        NarrativeGrade o = new NarrativeGrade();
                        o.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
                        o.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                        o.sims_mark_grade_description = dr["sims_mark_grade_description"].ToString();
                        o.sims_mark_grade_low = dr["sims_mark_grade_low"].ToString();
                        o.sims_mark_grade_high = dr["sims_mark_grade_high"].ToString();
                        o.sims_mark_grade_point = dr["sims_mark_grade_point"].ToString();
                        o.GradeGroupCode = dr["sims_mark_grade"].ToString();
                        o.GradeGroupDesc = dr["Grade_D"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("getGradeScaleData_Cat_Assign")]
        public HttpResponseMessage getGradeScaleData_Cat_Assign(string CUR_CODE, string ACADEMIC_YEAR, string GRADE_CODE, string SECTION_CODE, string GB_NUMBER, string GB_CAT_NUMBER, string GB_ASSIGNMENT_NUMBER, string sims_gb_cat_assign_mark, string sims_assign_max_marks)
        {

            List<NarrativeGrade> lst = new List<NarrativeGrade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gradebook_cat_assign_grading_scale_Category_assginment_level",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "A"),
                            new SqlParameter("@CUR_CODE", CUR_CODE),
                            new SqlParameter("@ACADEMIC_YEAR", ACADEMIC_YEAR),
                            new SqlParameter("@GRADE_CODE", GRADE_CODE),
                            new SqlParameter("@SECTION_CODE", SECTION_CODE),
                            new SqlParameter("@GB_NUMBER", GB_NUMBER),
                            new SqlParameter("@GB_CAT_NUMBER", GB_CAT_NUMBER),
                            new SqlParameter("@GB_ASSIGNMENT_NUMBER", GB_ASSIGNMENT_NUMBER),
                            new SqlParameter("@sims_gb_cat_assign_mark", sims_gb_cat_assign_mark),
                            new SqlParameter("@sims_assign_max_marks", sims_assign_max_marks),
                         });
                    while (dr.Read())
                    {
                        NarrativeGrade o = new NarrativeGrade();
                        o.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
                        o.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                        o.sims_mark_grade_description = dr["sims_mark_grade_description"].ToString();
                        o.sims_mark_grade_low = dr["sims_mark_grade_low"].ToString();
                        o.sims_mark_grade_high = dr["sims_mark_grade_high"].ToString();
                        o.sims_mark_grade_point = dr["sims_mark_grade_point"].ToString();
                        o.GradeGroupCode = dr["sims_mark_grade"].ToString();
                        o.GradeGroupDesc = dr["Grade_D"].ToString();
                        lst.Add(o);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("AssignStudents")]
        public List<Students> AssignStudents(SIMSAPI.Models.ERP.gradebookClass.Assignment a, string opr, string AssignType)
        {
            List<Students> ls = new List<Students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_ASSIGNMENT_STUDENT_PROC",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", opr),
                new SqlParameter("@CUR_CODE", a.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", a.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", a.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", a.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", a.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", a.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", a.Assignment_Code),
                new SqlParameter("@EnrollList", a.StudEnroll),
                new SqlParameter("@GB_ASSIGN_SCORE", a.Assignment_Mark),
                new SqlParameter("@GB_ASSIGNMENT_MAX_POINT", a.Assignment_max_point),
                new SqlParameter("@GB_ASSIGNMENT_MAX_POINT_CORRECT", a.Assignment_max_correct),
                new SqlParameter("@GB_ASSIGNMENT_COMMENT", a.Assignment_Comment),
                new SqlParameter("@GB_ASSIGNMENT_COMPLETED_DATE", a.Assignment_Completed_Date),
                new SqlParameter("@GB_ASSIGNMENT_TYPE", a.Assignment_Type),
                new SqlParameter("@SUB_ATTR_GROUP_CODE", a.AttrGroupCode),
                new SqlParameter("@SUB_ATTR_CODE", a.AttrCode)
            });
                }
            }
            catch (Exception ex)
            {

            }
            Subject sb = new Subject();
            sb.SubjectCurr = a.Assignment_cur_code;
            sb.SubjectAcademic = a.Assignment_academic_year;
            sb.SubjectGrade = a.Assignment_grade_code;
            sb.SubjectSection = a.Assignment_section_code;
            sb.AssignmentNumber = a.Assignment_Code;
            sb.categoryNumber = a.Assignment_cat_code;
            sb.GradeBookNumber = a.Assignment_gb_number;
            ls = GetStudents(sb);
            return ls;

        }

        [Route("AssignMentStudents")]
        public HttpResponseMessage AssignMentStudents(Assignment a)
        {

            Assignment assign = new Assignment();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "AA"),
                new SqlParameter("@CUR_CODE", a.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", a.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", a.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", a.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", a.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", a.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", a.Assignment_Code),
                new SqlParameter("@GB_TEACHER_CODE", a.teacher_code)

                         });

                    assign.Assignment_academic_year = a.Assignment_academic_year.Clone().ToString();
                    assign.Assignment_cat_code = a.Assignment_cat_code.Clone().ToString();
                    assign.Assignment_cat_name = a.Assignment_cat_name.Clone().ToString();
                    assign.Assignment_Code = a.Assignment_Code.Clone().ToString();
                    assign.Assignment_cur_code = a.Assignment_cur_code.Clone().ToString();
                    assign.Assignment_gb_number = a.Assignment_gb_number.Clone().ToString();
                    assign.Assignment_grade_code = a.Assignment_grade_code.ToString();
                    assign.Assignment_Name = a.Assignment_Name.ToString();
                    assign.Assignment_section_code = a.Assignment_section_code.ToString();
                    assign.Assignment_Score_type = a.Assignment_Score_type.ToString();
                    assign.Assignment_GradeCompleted_Status = a.Assignment_GradeCompleted_Status;
                    assign.StudentList = new List<Students>();//Grade_Desc
                    while (dr.Read())
                    {
                        Students std = new Students();
                        assign.sims_gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                        std.Assignment_Comment = dr["sims_gb_cat_assign_comment"].ToString();
                        std.Assignment_Completed_Date = dr["sims_gb_cat_assign_completed_date"].ToString();
                        std.Assignment_final_grade_desc = dr["Grade_Desc"].ToString();
                        std.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                        std.sims_gb_grade_scale = assign.sims_gb_grade_scale.ToString();

                        std.sims_mark_final_grade_code = dr["sims_gb_cat_assign_final_grade"].ToString();
                        std.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        std.Assignment_max_correct = dr["sims_gb_cat_assign_max_correct"].ToString();
                        std.Assignment_max_point = dr["sims_gb_cat_assign_max_point"].ToString();
                        std.Assignment_Status = dr["sims_gb_cat_assign_status"].ToString();
                        std.Stud_Enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();

                        try {
                            std.Stud_roll = dr["sims_roll_number"].ToString();
                            std.sims_roll_number_display = dr["sims_roll_number_display"].ToString();
                        }catch(Exception e){}

                         try
                            {
                                std.Stud_roll_sort = int.Parse(new String(std.Stud_roll.Where(Char.IsDigit).ToArray())); 
                            }
                            catch (Exception ex) { }
                        try
                        {
                            std.sims_student_gender = dr["sims_student_gender"].ToString();
                        }
                        catch (Exception ex) { }

                        std.Color_Code = dr["Color_Code"].ToString();
                        std.Stud_Full_Name = dr["StudFullName"].ToString();
                        assign.StudentList.Add(std);
                    }
                    assign.StudentList.ForEach(q => q.Stud_AcademicYear = assign.Assignment_academic_year);
                    assign.StudentList.ForEach(q => q.Stud_CurrCode = assign.Assignment_cur_code);
                    assign.StudentList.ForEach(q => q.Stud_GradeCode = assign.Assignment_grade_code);
                    assign.StudentList.ForEach(q => q.Stud_SectionCode = assign.Assignment_section_code);
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, assign);
        }

        [Route("AllStudentAssignments")]
        public HttpResponseMessage AllStudentAssignments(SIMSAPI.Models.ERP.gradebookClass.Assignment obj)
        {
            List<Assignment> allstd = new List<Assignment>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "BB"),
                new SqlParameter("@CUR_CODE", obj.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", obj.Assignment_academic_year),
                new SqlParameter("@GB_NUMBER", obj.Assignment_gb_number),
                new SqlParameter("@ENROLL_NUMBER", obj.StudEnroll)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Assignment ass = new Assignment();
                            ass.Assignment_Comment = dr["sims_gb_cat_assign_comment"].ToString();
                            ass.Assignment_Completed_Date = dr["sims_gb_cat_assign_completed_date"].ToString();
                            ass.Assignment_final_grade_desc = dr["Grade_Desc"].ToString();
                            ass.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                            ass.sims_mark_final_grade_code = dr["sims_gb_cat_assign_final_grade"].ToString();
                            ass.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                            ass.Assignment_max_correct = dr["sims_gb_cat_assign_max_correct"].ToString();
                            ass.Assignment_max_point = dr["sims_gb_cat_assign_max_point"].ToString();
                            ass.Assignment_Status = dr["sims_gb_cat_assign_status"].ToString();
                            ass.Stud_Enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            ass.Color_Code = dr["Color_Code"].ToString();
                            ass.Assignment_gb_name = dr["sims_gb_name"].ToString();
                            ass.Assignment_cat_name = dr["sims_gb_cat_name"].ToString();
                            ass.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                            ass.Assignment_gb_number = dr["sims_gb_number"].ToString();
                            ass.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                            ass.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                            ass.Assignment_Status1 = dr["StatusDesc"].ToString();
                            ass.Color_Code = dr["Color_Code"].ToString();
                            ass.Assignment_Score_type = dr["ScoreType"].ToString();
                            ass.sims_gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                            ass.Assignment_academic_year = obj.Assignment_academic_year.Clone().ToString();
                            ass.Assignment_cur_code = obj.Assignment_cur_code.Clone().ToString();
                            ass.Assignment_grade_code = obj.Assignment_grade_code.ToString();
                            ass.Assignment_section_code = obj.Assignment_section_code.ToString();
                            ass.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                            ass.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].ToString().Equals("T") ? true : false;
                            allstd.Add(ass);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, allstd);
        }

        [Route("GetAssignMentStudents_ME")]
        public HttpResponseMessage GetAssignMentStudents_ME(SIMSAPI.Models.ERP.gradebookClass.Assignment a)
        {
            List<Assignment> assigncoll = new List<Assignment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gb_mark_enrty_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "AA"),
                new SqlParameter("@CUR_CODE", a.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", a.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", a.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", a.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", a.Assignment_gb_number),
                new SqlParameter("@GB_CAT_CODE", a.Assignment_cat_code)
                });
                    while (dr.Read())
                    {
                        Assignment assign = new Assignment();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_Score_type = dr["ScoreType"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        assign.Assignment_Extra_Creadit = dr["sims_gb_cat_assign_extra_credit"].ToString();
                        assign.Assignment_Date = dr["sims_gb_cat_assign_date"].ToString();
                        assign.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                        assign.Assignment_ExamID = dr["sims_gb_cat_assign_exam_id"].ToString();
                        assign.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        assign.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].Equals("T") ? true : false;
                        assign.StudEnroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        assign.Assignment_cat_name = dr["sims_gb_cat_name"].ToString(); ;
                        assign.Assignment_Mark = dr["sims_gb_cat_assign_mark"].ToString();
                        assign.teacher_code = string.Format("{0}{1}", assign.Assignment_cat_code, assign.Assignment_Code);
                        assign.Assignment_student_name = dr["StudentName"].ToString();
                        assign.Assignment_Mark_Total = "0";
                        assigncoll.Add(assign);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, assigncoll);

        }

        [Route("AssignMarksToStudentsM")]
        public HttpResponseMessage AssignMarksToStudentsM(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data)
        {
            bool inserted = false;
            string status;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment a in data)
                    {
                        if (a.Assignment_Mark == "") {
                            a.Assignment_Mark = null;
                        }


                        status = (a.Assignment_Status1 == "") ? null : a.Assignment_Status1;
                        status = (a.Assignment_Status1 == "Clear") ? null : a.Assignment_Status1;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_ASSIGNMENT_STUDENT_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "U"),
                new SqlParameter("@CUR_CODE", a.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", a.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", a.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", a.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", a.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", a.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", a.Assignment_Code),
                new SqlParameter("@GB_ASSIGNMENT_MAX_POINT", a.Assignment_max_point),
                new SqlParameter("@GB_ASSIGN_FINAL_GRADE", a.sims_mark_final_grade_code),
                new SqlParameter("@GB_ASSIGN_SCORE", a.Assignment_Mark),
                new SqlParameter("@GB_ASSIGNMENT_MAX_POINT_CORRECT", a.Assignment_max_correct),
                new SqlParameter("@GB_ASSIGNMENT_COMMENT", a.Assignment_Comment),
                new SqlParameter("@GB_ENROLL", a.StudEnroll),
                new SqlParameter("@AssignmentList", a.Assignment_Code),
                new SqlParameter("@STATUS", status),
                new SqlParameter("@GB_ASSIGNMENT_COMPLETED_DATE",a.Assignment_Completed_Date)
                });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("GetGradeBookForCopy")]
        public HttpResponseMessage GetGradeBookForCopy(string subCode, string termCode, string academic_year, string cur_code)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@OPR", "GB"),
                 new SqlParameter("@GB_NUMBER", subCode),
                 new SqlParameter("@ACADEMIC_YEAR", academic_year),
                 new SqlParameter("@CUR_CODE",cur_code)
                         });
                    #region Gradebook Collection Loop
                    while (dr.Read())
                    {
                        gradebook o = new gradebook();
                        o.cur_code = dr["sims_cur_code"].ToString();
                        o.academic_year = dr["sims_academic_year"].ToString();
                        o.grade_code = dr["sims_grade_code"].ToString();
                        o.section_code = dr["sims_section_code"].ToString();
                        o.gb_name = dr["sims_gb_name"].ToString();
                        o.gb_type = dr["sims_gb_type"].ToString();
                        o.gb_type_desc = dr["typeDesc"].ToString();
                        o.gb_number = dr["sims_gb_number"].ToString();
                        o.gb_remark = dr["sims_gb_remarks"].ToString();
                        o.gb_start_date = dr["sims_gb_start_date"].ToString();
                        o.gb_end_date = dr["sims_gb_end_date"].ToString();
                        o.gb_term_code = dr["sims_gb_term_code"].ToString();
                        o.gb_term_name = dr["TermName"].ToString();
                        o.gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                        o.gb_status = dr["sims_gb_status"].Equals("A") ? true : false;
                        o.Subject_Code = subCode;
                        lst.Add(o);
                    }
                    dr.NextResult();
                    #endregion

                    #region Gradebook Category Loop
                    while (dr.Read())
                    {
                        gradebook_category gc = new gradebook_category();
                        gc.cur_code = dr["sims_cur_code"].ToString();
                        gc.cat_code = dr["sims_gb_cat_code"].ToString();
                        gc.cat_color = dr["sims_gb_cat_color_code"].ToString();
                        gc.cat_drop_low = dr["sims_gb_cat_drop_low"].ToString();
                        gc.cat_drop_per = dr["sims_gb_cat_drop_per"].ToString();
                        gc.cat_extra_points = dr["sims_gb_cat_extra_point"].ToString();
                        gc.cat_name = dr["sims_gb_cat_name"].ToString();
                        gc.cat_points_possible = dr["sims_gb_cat_points_possible"].ToString();
                        gc.cat_score_types = dr["sims_gb_cat_score_type"].ToString();
                        gc.cat_score_types_desc = dr["Score_Type"].ToString();
                        gc.cat_short_name = dr["sims_gb_cat_short_name"].ToString();
                        gc.cat_weightage_per = dr["sims_gb_cat_weightage_per"].ToString();
                        gc.cat_weightage_type = dr["sims_gb_cat_weightage_type"].ToString();
                        gc.cat_weightage_type_desc = dr["Weight_Desc"].ToString();
                        gc.cur_name = dr["Curr_Name"].ToString();
                        gc.academic_year = dr["sims_academic_year"].ToString();
                        gc.grade_name = dr["Grade_Name"].ToString();
                        gc.grade_code = dr["sims_grade_code"].ToString();
                        gc.gb_name = dr["GB_NAME"].ToString();
                        gc.gb_number = dr["sims_gb_number"].ToString();
                        gc.isIncludedInFinalGrade = dr["sims_gb_cat_include_in_final_grade"].ToString().Equals("Y");
                        gc.publishDate = dr["sims_gb_cat_publish_assignment_on_date"].ToString();
                        gc.section_name = dr["Section_Name"].ToString();
                        gc.section_code = dr["sims_section_code"].ToString();
                        gc.status = dr["sims_gb_cat_status"].Equals("A") ? true : false;
                        gradebook o = lst.Single(q => q.gb_number == gc.gb_number);
                        if (o != null)
                        {
                            if (o.Categories == null) o.Categories = new List<gradebook_category>();
                            o.Categories.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region Assignment Collection Loop
                    while (dr.Read())
                    {
                        Assignment assign = new Assignment();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_cat_name = dr["CAT_NAME"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_cur_name = dr["Curr_Name"].ToString();
                        assign.Assignment_Date = dr["sims_gb_cat_assign_date"].ToString();
                        assign.Assignment_Due_Date = dr["sims_gb_cat_assign_due_date"].ToString();
                        assign.Assignment_Due_Time = dr["sims_gb_cat_assign_due_time"].ToString();
                        assign.Assignment_Email = dr["sims_gb_cat_assign_email"].Equals("T") ? true : false;
                        assign.Assignment_ExamID = dr["sims_gb_cat_assign_exam_id"].ToString();
                        assign.Assignment_Extra_Creadit = dr["sims_gb_cat_assign_extra_credit"].ToString();
                        assign.Assignment_gb_name = dr["GB_NAME"].ToString();
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_grade_name = dr["Grade_Name"].ToString();
                        assign.Assignment_GradeCompleted_Status = dr["sims_gb_cat_assign_grade_completed_status"].Equals("T") ? true : false;
                        assign.Assignment_IP_BY_STD = dr["sims_gb_cat_assign_input_by_standard"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore = dr["sims_gb_cat_assign_max_score"].ToString();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_NarrativeGrade = dr["sims_gb_cat_assign_narrative_grade"].Equals("T") ? true : false;
                        assign.Assignment_Remark = dr["sims_gb_cat_assign_remarks"].ToString();
                        //assign.Assignment_Score_type = dr[""].ToString();
                        //assign.Assignment_Score_type_Desc = dr[""].ToString();
                        assign.Assignment_Score_Visible_To_Portal = dr["sims_gb_cat_assign_score_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();
                        assign.Assignment_section_name = dr["Section_Name"].ToString();
                        assign.Assignment_Type = dr["sims_gb_cat_assign_type"].ToString();
                        assign.Assignment_Type_Desc = dr["Assignment_Type"].ToString();
                        assign.Assignment_Visible_To_Portal = dr["sims_gb_cat_assign_visible_to_portal"].Equals("T") ? true : false;
                        assign.Assignment_Weightage = dr["sims_gb_cat_assign_weightage"].ToString();
                        assign.AssignmentIncludedInFinalGrade = dr["sims_gb_cat_assign_include_in_final_grade"].Equals("T") ? true : false;
                        assign.AssignmentMaxScore_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        assign.Assignment_max_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                        gradebook_category gc = (lst.Single(a => a.gb_number == assign.Assignment_gb_number).Categories.Single(q => q.cat_code == assign.Assignment_cat_code));
                        if (gc.AssignMents == null) gc.AssignMents = new List<Assignment>();
                        gc.AssignMents.Add(assign);
                    }
                    dr.NextResult();
                    #endregion
                    dr.Close();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CopyGradeBook")]
        public HttpResponseMessage CopyGradeBook(gradebook data, string CatList, string AssignList)
        {
            bool b = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "CG"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_NAME", data.gb_name),
                new SqlParameter("@GB_TYPE", data.gb_type),
                new SqlParameter("@GB_TERM_CODE", data.gb_term_code),
                new SqlParameter("@GB_GRADE_SCALE", data.gb_grade_scale),
                new SqlParameter("@GB_START_DATE", data.gb_start_date),
                new SqlParameter("@GB_END_DATE", data.gb_end_date),
                new SqlParameter("@GB_TEACHER_CODE", data.teacher_code),
                new SqlParameter("@GB_REMARK", data.gb_remark),
                new SqlParameter("@GB_TEACHERLIST", data.TeacherList),
                new SqlParameter("@GB_STATUS", data.gb_status),
                new SqlParameter("@GB_SUBJECT_CODE", data.Subject_Code),
                new SqlParameter("@CAT_LIST", CatList),
                new SqlParameter("@ASSIGN_LIST", AssignList)
            });
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, b);


        }

        [Route("CopyGradeBookNameForCopyGradebook")]
        public HttpResponseMessage CopyGradeBookNameForCopyGradebook(gradebook data)
        {
            List<gradebook> lst = new List<gradebook>();
            bool b = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "GO"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GB_TERM_CODE", data.gb_term_code),

            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            gradebook o = new gradebook();
                            o.cur_code = dr["sims_cur_code"].ToString();
                            o.academic_year = dr["sims_academic_year"].ToString();
                            o.grade_code = dr["sims_grade_code"].ToString();
                            o.section_code = dr["sims_section_code"].ToString();
                            o.gb_name = dr["sims_gb_name"].ToString();
                            o.gb_type = dr["sims_gb_type"].ToString();
                            o.gb_number = dr["sims_gb_number"].ToString();
                            o.gb_remark = dr["sims_gb_remarks"].ToString();
                            o.gb_start_date = dr["sims_gb_start_date"].ToString();
                            o.gb_end_date = dr["sims_gb_end_date"].ToString();
                            o.gb_term_code = dr["sims_gb_term_code"].ToString();
                            o.gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                            o.gb_status = dr["sims_gb_status"].Equals("A") ? true : false;
                            o.Subject_Code = dr["sims_gb_subject_code"].ToString();
                            o.sims_homeroom_batch_code = dr["sims_gb_homeroom_batch_code"].ToString();
                            o.sims_homeroom_code = dr["sims_gb_homeroom_code"].ToString();
                            lst.Add(o);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CGradeSectionSubjectNameorCode")]
        public HttpResponseMessage CGradeSectionSubjectNameorCode(gradebook data)
        {
            List<Grade> lst = new List<Grade>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "GZ"),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),

            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grade g = new Grade();
                            g.Grade_Code = dr["sims_grade_code"].ToString();
                            g.Grade_Name = dr["sims_grade_name_en"].ToString();
                            g.Sectionslis = new List<Section>();

                            lst.Add(g);
                        }
                        dr.NextResult();
                        while (dr.Read())
                        {
                            Section ss = new Section();
                            ss.Sec_Grade_Code = dr["sims_grade_code"].ToString();
                            ss.Section_Code = dr["sims_section_code"].ToString();
                            ss.Section_Name = dr["sims_section_name_en"].ToString();
                            ss.Subjectslist = new List<Subject>();

                            Grade o = lst.Single(q => q.Grade_Code == ss.Sec_Grade_Code);

                            if (o != null)
                            {
                                if (o.Sectionslis == null) o.Sectionslis = new List<Section>();
                                o.Sectionslis.Add(ss);
                            }




                        }
                        dr.NextResult();
                        while (dr.Read())
                        {
                            Subject sub = new Subject();
                            sub.SubjectCode = dr["sims_subject_code"].ToString();
                            sub.SubjectName = dr["sims_subject_name_en"].ToString();
                            sub.SubjectGrade = dr["sims_grade_code"].ToString();
                            sub.SubjectSection = dr["sims_section_code"].ToString();
                            sub.SubjectAcademic = dr["sims_academic_year"].ToString();
                            sub.SubjectCurr = dr["sims_cur_code"].ToString();

                            Section a = lst.Single(q => q.Grade_Code == sub.SubjectGrade).Sectionslis.Single(b => b.Sec_Grade_Code == sub.SubjectGrade && b.Section_Code == sub.SubjectSection);

                            if (a != null)
                            {
                                if (a.Subjectslist == null) a.Subjectslist = new List<Subject>();
                                a.Subjectslist.Add(sub);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        #region gradebookcopy

        [Route("GradeBookCopy")]
        public HttpResponseMessage GradeBookCopy(List<gradebook> source)
        {

            int res = 0;
            try
            {
                foreach (gradebook objdes in source)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                            new List<SqlParameter>()
                                   {
                                new SqlParameter("@OPR", "CG"),
                                new SqlParameter("@CUR_CODE", objdes.cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", objdes.academic_year),
                                new SqlParameter("@GB_TERM_CODE", objdes.gb_term_code),
                                new SqlParameter("@GRADE_CODE", objdes.grade_code),
                                new SqlParameter("@SECTION_CODE", objdes.section_code),
                                new SqlParameter("@GB_NUMBER", objdes.gb_number),
                                new SqlParameter("@GB_TO_CUR", objdes.SubjectCurr),
                                new SqlParameter("@GB_TO_ACADEMIC_YEAR", objdes.SubjectAcademic),
                                new SqlParameter("@GB_TO_TERM_CODE", objdes.dTerm_code),
                                new SqlParameter("@GB_TO_GRADE_CODE", objdes.SubjectGrade),
                                new SqlParameter("@GB_TO_SECTION_CODE", objdes.SubjectSection),
                                new SqlParameter("@GB_TO_SUBJECT_CODE", objdes.SubjectCode),
                                new SqlParameter("@GB_TO_IsCategory", objdes.isCateogry),
                                new SqlParameter("@GB_TO_IsAssignment", objdes.isAssignment)
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            res = res + 1;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //res = 0;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CategoryCopy")]
        public HttpResponseMessage CategoryCopy(List<gradebook_category> source)
        {
            int res = 0;
            foreach (gradebook_category cat in source)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                             new List<SqlParameter>()
                                  {
                                new SqlParameter("@OPR", "CC"),
                                new SqlParameter("@GB_TO_GB_NUMBER", cat.gb_number),
                                new SqlParameter("@GB_TO_ACADEMIC_YEAR", cat.academic_year),
                                new SqlParameter("@GB_TO_CUR", cat.cur_code),
                                new SqlParameter("@GB_TO_GRADE_CODE", cat.grade_code),
                                new SqlParameter("@GB_TO_SECTION_CODE", cat.section_code),
                                new SqlParameter("@GB_TO_TERM_CODE", cat.dTerm_code),
                                new SqlParameter("@GB_NUMBER", cat.cat_gb_number),
                                new SqlParameter("@GB_CAT_NO_LST", cat.cat_code),
                                new SqlParameter("@GB_TO_IsAssignment", cat.withAssign)
                            });

                        if (dr.RecordsAffected > 0)
                        {
                            res = res + 1;
                        }
                        dr.Close();
                    }
                }
                catch (Exception ex)
                {
                    //res = 0;
                    return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("AssignmentCopy")]
        public HttpResponseMessage AssignmentCopy(List<gradebook_category> source)
        {
            int res = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradebook_category cat in source)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                             new List<SqlParameter>()
                                             {
                                            new SqlParameter("@OPR", "CA"),
                                            new SqlParameter("@GB_TO_GB_NUMBER", cat.to_gb_number),
                                            new SqlParameter("@GB_TO_GB_CATEGORY_LST", cat.to_cat_code),
                                            new SqlParameter("@GB_NUMBER", cat.gb_number),
                                            new SqlParameter("@GB_CAT_NO_LST", cat.cat_code),
                                            new SqlParameter("@GB_TO_TERM_CODE", cat.dTerm_code),
                                            new SqlParameter("@GB_ASSIGNMENT_LST", cat.Assignment_Code)
                                            });
                        if (dr.RecordsAffected > 0)
                        {
                            res = res + 1;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //res = 0;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("GradeBookDistCategorys")]
        public HttpResponseMessage GradeBookDistCategorys(string cur_code, string aca_year, string grade, string term, string teacher, string sections_list, string sub_list)
        {
            List<gradebook_category> cat = new List<gradebook_category>();
            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_NEW]",
                         new List<SqlParameter>()
                                             {

                new SqlParameter("@OPR", "GS"),
                new SqlParameter("@CUR_CODE", cur_code),
                new SqlParameter("@ACADEMIC_YEAR", aca_year),
                new SqlParameter("@GRADE_CODE", grade),
                new SqlParameter("@GB_TERM_CODE", term),
                new SqlParameter("@SECTION_CODE_LIST", sections_list),
                new SqlParameter("@GB_SUBJECT_CODE_LIST", sub_list)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            gradebook_category gc = new gradebook_category();
                            gc.cat_name = dr["sims_gb_cat_name"].ToString();
                            gc.cat_code = dr["sims_gb_cat_code"].ToString();
                            cat.Add(gc);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, cat);

        }

        [Route("getTermwiseGetGradeBookForCopy")]
        public HttpResponseMessage getTermwiseGetGradeBookForCopy(string termCode, string academic_year, string cur_code)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@OPR", "GT"),
                 new SqlParameter("@GB_TERM_CODE", termCode),
                 new SqlParameter("@ACADEMIC_YEAR",academic_year),
                 new SqlParameter("@CUR_CODE",cur_code)


                         });
                    #region Gradebook Collection Loop
                    while (dr.Read())
                    {
                        gradebook o = new gradebook();
                        o.cur_code = dr["sims_cur_code"].ToString();
                        o.academic_year = dr["sims_academic_year"].ToString();
                        o.grade_code = dr["sims_grade_code"].ToString();
                        o.section_code = dr["sims_section_code"].ToString();
                        o.gb_name = dr["sims_gb_name"].ToString();
                        o.gb_number = dr["sims_gb_number"].ToString();
                        o.gb_term_code = dr["sims_gb_term_code"].ToString();

                        lst.Add(o);
                    }

                    #region Gradebook Category Loop
                    dr.NextResult();
                    while (dr.Read())
                    {
                        gradebook_category gc = new gradebook_category();
                        gc.cur_code = dr["sims_cur_code"].ToString();
                        gc.cat_code = dr["sims_gb_cat_code"].ToString();
                        gc.cat_name = dr["sims_gb_cat_name"].ToString();
                        gc.gb_number = dr["sims_gb_number"].ToString();
                        gc.section_code = dr["sims_section_code"].ToString();
                        gc.grade_code = dr["sims_grade_code"].ToString();
                        gc.academic_year = dr["sims_academic_year"].ToString();
                        gradebook o = lst.SingleOrDefault(q => q.gb_number == gc.gb_number);
                        if (o != null)
                        {
                            if (o.Categories == null) o.Categories = new List<gradebook_category>();
                            o.Categories.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getTermwiseGetGradeBookForCopynew")]
        public HttpResponseMessage getTermwiseGetGradeBookForCopynew(string termCode, string academic_year, string cur_code, string grade_code)
        {
            List<gradebooklist> lst = new List<gradebooklist>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                        new List<SqlParameter>()
                         {
                 new SqlParameter("@OPR", "NC"),
                 new SqlParameter("@GB_TERM_CODE", termCode),
                 new SqlParameter("@ACADEMIC_YEAR",academic_year),
                 new SqlParameter("@CUR_CODE",cur_code),
                 new SqlParameter("@GRADE_CODE",grade_code)
                         });

                    while (dr.Read())
                    {
                        gradebooklist g = new gradebooklist();
                        g.sims_cur_code = dr["sims_cur_code"].ToString();
                        g.sims_section_code = dr["sims_section_code"].ToString();
                        g.sims_grade_code = dr["sims_grade_code"].ToString();
                        g.sims_academic_year = dr["sims_academic_year"].ToString();
                        g.sims_gb_name = dr["sims_gb_name"].ToString();
                        g.sims_gb_number = dr["sims_gb_number"].ToString();
                        g.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                        g.gb_cat_lst = new List<GB_Category_list>();
                        lst.Add(g);

                    }
                    dr.NextResult();

                    while (dr.Read())
                    {
                        GB_Category_list c = new GB_Category_list();
                        c.sims_cur_code = dr["sims_cur_code"].ToString();
                        c.sims_section_code = dr["sims_section_code"].ToString();
                        c.sims_grade_code = dr["sims_grade_code"].ToString();
                        c.sims_academic_year = dr["sims_academic_year"].ToString();
                        c.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                        c.sims_gb_number = dr["sims_gb_number"].ToString();
                        c.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();

                        c.assign_lst = new List<gb_cat_assign_list>();
                        gradebooklist o = lst.Single(r => r.sims_grade_code == c.sims_grade_code && r.sims_cur_code == c.sims_cur_code && r.sims_academic_year == c.sims_academic_year && r.sims_section_code == c.sims_section_code && r.sims_gb_number == c.sims_gb_number);

                        if (o != null)
                        {
                            if (o.gb_cat_lst == null) o.gb_cat_lst = new List<GB_Category_list>();
                            o.gb_cat_lst.Add(c);
                        }
                    }

                    dr.NextResult();

                    while (dr.Read())
                    {
                        gb_cat_assign_list d = new gb_cat_assign_list();
                        d.sims_cur_code = dr["sims_cur_code"].ToString();
                        d.sims_section_code = dr["sims_section_code"].ToString();
                        d.sims_grade_code = dr["sims_grade_code"].ToString();
                        d.sims_academic_year = dr["sims_academic_year"].ToString();
                        d.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                        d.sims_gb_number = dr["sims_gb_number"].ToString();
                        d.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        d.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();

                        GB_Category_list o = lst.Single(r => r.sims_grade_code == d.sims_grade_code && r.sims_cur_code == d.sims_cur_code && r.sims_academic_year == d.sims_academic_year && r.sims_section_code == d.sims_section_code && r.sims_gb_number == d.sims_gb_number).gb_cat_lst.Single(l => l.sims_grade_code == d.sims_grade_code && l.sims_cur_code == d.sims_cur_code && l.sims_academic_year == d.sims_academic_year && l.sims_section_code == d.sims_section_code && l.sims_gb_number == d.sims_gb_number && l.sims_gb_cat_code == d.sims_gb_cat_code);

                        if (o != null)
                        {
                            if (o.assign_lst == null) o.assign_lst = new List<gb_cat_assign_list>();
                            o.assign_lst.Add(d);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getclasswiseGetGradeBookForCopy")]
        public HttpResponseMessage getclasswiseGetGradeBookForCopy(string termCode, string academic_year, string cur_code)
        {
            List<Grade> lst = new List<Grade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@OPR", "CN"),
                 new SqlParameter("@GB_TERM_CODE", termCode),
                 new SqlParameter("@ACADEMIC_YEAR",academic_year),
                 new SqlParameter("@CUR_CODE",cur_code)
                         });

                    while (dr.Read())
                    {
                        Grade o = new Grade();
                        o.Grade_Name = dr["sims_grade_name_en"].ToString();
                        o.Grade_Code = dr["sims_grade_code"].ToString();
                        o.Sectionslis = new List<Section>();
                        lst.Add(o);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Section gc = new Section();
                        gc.Section_Code = dr["sims_section_code"].ToString();
                        gc.Sec_Grade_Code = dr["sims_grade_code"].ToString();
                        gc.Section_Name = dr["sims_section_name_en"].ToString();
                        gc.Subjectslist = new List<Subject>();
                        Grade o = lst.Single(r => r.Grade_Code == gc.Sec_Grade_Code);

                        if (o != null)
                        {
                            if (o.Sectionslis == null) o.Sectionslis = new List<Section>();
                            o.Sectionslis.Add(gc);
                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Subject s = new Subject();
                        s.SubjectSection = dr["sims_section_code"].ToString();
                        s.SubjectGrade = dr["sims_grade_code"].ToString();
                        s.SubjectName = dr["sims_subject_name_en"].ToString();
                        s.SubjectCode = dr["sims_subject_code"].ToString();
                        Section o = lst.Single(r => r.Grade_Code == s.SubjectGrade).Sectionslis.Single(q => q.Sec_Grade_Code == s.SubjectGrade && q.Section_Code == s.SubjectSection);

                        if (o != null)
                        {
                            if (o.Subjectslist == null) o.Subjectslist = new List<Subject>();
                            o.Subjectslist.Add(s);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getclasswiseGetGradeBookForsubjectlist")]
        public HttpResponseMessage getclasswiseGetGradeBookForsubjectlist(string academic_year, string cur_code)
        {
            List<Grade> lst = new List<Grade>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@OPR", "AC"),
                 new SqlParameter("@ACADEMIC_YEAR",academic_year),
                 new SqlParameter("@CUR_CODE",cur_code)
                         });

                    while (dr.Read())
                    {
                        Grade o = new Grade();
                        o.Grade_Name = dr["sims_grade_name_en"].ToString();
                        o.Grade_Code = dr["sims_grade_code"].ToString();
                        o.Sectionslis = new List<Section>();
                        lst.Add(o);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Section gc = new Section();
                        gc.Section_Code = dr["sims_section_code"].ToString();
                        gc.Sec_Grade_Code = dr["sims_grade_code"].ToString();
                        gc.Section_Name = dr["sims_section_name_en"].ToString();
                        gc.Subjectslist = new List<Subject>();
                        Grade o = lst.Single(r => r.Grade_Code == gc.Sec_Grade_Code);

                        if (o != null)
                        {
                            if (o.Sectionslis == null) o.Sectionslis = new List<Section>();
                            o.Sectionslis.Add(gc);
                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Subject s = new Subject();
                        s.SubjectSection = dr["sims_section_code"].ToString();
                        s.SubjectGrade = dr["sims_grade_code"].ToString();
                        s.SubjectName = dr["sims_subject_name_en"].ToString();
                        s.SubjectCode = dr["sims_subject_code"].ToString();
                        Section o = lst.Single(r => r.Grade_Code == s.SubjectGrade).Sectionslis.Single(q => q.Sec_Grade_Code == s.SubjectGrade && q.Section_Code == s.SubjectSection);

                        if (o != null)
                        {
                            if (o.Subjectslist == null) o.Subjectslist = new List<Subject>();
                            o.Subjectslist.Add(s);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ClassWiseGradeBookCopy")]
        public HttpResponseMessage ClassWiseGradeBookCopy(List<gradebook> source)
        {

            bool insert = false;
            try
            {
                foreach (gradebook objdes in source)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[COPY_GRADEBOOK_proc]",
                            new List<SqlParameter>()
                                   {
                                new SqlParameter("@OPR", "CW"),
                                new SqlParameter("@CUR_CODE", objdes.cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", objdes.academic_year),
                                new SqlParameter("@GB_TERM_CODE", objdes.gb_term_code),
                                new SqlParameter("@GRADE_CODE", objdes.grade_code),
                                new SqlParameter("@SECTION_CODE", objdes.section_code),

                                new SqlParameter("@GB_TO_CUR", objdes.SubjectCurr),
                                new SqlParameter("@GB_TO_ACADEMIC_YEAR", objdes.SubjectAcademic),
                                new SqlParameter("@GB_TO_TERM_CODE", objdes.dTerm_code),
                                new SqlParameter("@GB_TO_GRADE_CODE", objdes.SubjectGrade),
                                new SqlParameter("@GB_TO_SECTION_CODE", objdes.SubjectSection),
                                new SqlParameter("@GB_TO_SUBJECT_CODE", objdes.SubjectCode),

                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //insert = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        #endregion

        #region co_scholasticcopy

        [Route("getCo_ScholasticForCopy")]
        public HttpResponseMessage getCo_ScholasticForCopy(string cur_code, string academic_year, string grade_code)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CO_SCHOLASTIC_COPY_PROC]",
                        new List<SqlParameter>()
                         {


                 new SqlParameter("@OPR", "CS"),
                 new SqlParameter("@CO_SCHO_CUR",cur_code),
                 new SqlParameter("@CO_SCHO_ACADEMIC_YEAR",academic_year),
                 new SqlParameter("@CO_SCHO_GRADE_CODE",grade_code=="null"?null:grade_code)

                         });

                    while (dr.Read())
                    {
                        schol_group o = new schol_group();
                        o.sims_cur_code = dr["sims_cur_code"].ToString();
                        o.sims_academic_year = dr["sims_academic_year"].ToString();
                        o.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        o.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                        o.sims_grade_code = dr["sims_grade_code"].ToString();
                        o.sims_section_code = dr["sims_section_code"].ToString();

                        lst.Add(o);
                    }


                    dr.NextResult();
                    while (dr.Read())
                    {
                        schol_Attribute gc = new schol_Attribute();
                        gc.sims_cur_code = dr["sims_cur_code"].ToString();
                        gc.sims_academic_year = dr["sims_academic_year"].ToString();
                        gc.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        gc.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                        gc.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        gc.sims_section_code = dr["sims_section_code"].ToString();
                        gc.sims_grade_code = dr["sims_grade_code"].ToString();
                        schol_group o = lst.Single(q => q.sims_co_scholastic_group_code == gc.sims_co_scholastic_group_code && q.sims_grade_code + q.sims_section_code == gc.sims_grade_code + gc.sims_section_code);
                        if (o != null)
                        {
                            if (o.Attrlist == null)
                                o.Attrlist = new List<schol_Attribute>();
                            o.Attrlist.Add(gc);
                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        DescIndicator di = new DescIndicator();
                        di.sims_academic_year = dr["sims_academic_year"].ToString();
                        di.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        di.sims_co_scholastic_discriptive_indicator_code = dr["sims_co_scholastic_discriptive_indicator_code"].ToString();
                        di.sims_co_scholastic_discriptive_indicator_name = dr["sims_co_scholastic_discriptive_indicator_name"].ToString();
                        di.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        di.sims_cur_code = dr["sims_cur_code"].ToString();
                        di.sims_grade_code = dr["sims_grade_code"].ToString();
                        di.sims_section_code = dr["sims_section_code"].ToString();
                        schol_Attribute attr = lst.Single(q => q.sims_co_scholastic_group_code == di.sims_co_scholastic_group_code && q.sims_grade_code + q.sims_section_code == di.sims_grade_code + di.sims_section_code).Attrlist.Single(q => q.sims_co_scholastic_attribute_code == di.sims_co_scholastic_attribute_code && q.sims_grade_code + q.sims_section_code == di.sims_grade_code + di.sims_section_code);
                        if (attr.indlist == null) attr.indlist = new List<DescIndicator>();
                        attr.indlist.Add(di);
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ScholasticGroupCopy")]
        public HttpResponseMessage ScholasticGroupCopy(List<Co_Scholastic_copy> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Co_Scholastic_copy data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CO_SCHOLASTIC_COPY_PROC]",
                            new List<SqlParameter>()
                         {
                 new SqlParameter("@OPR", "CG"),
                 new SqlParameter("@CO_SCHO_CUR",data1.CO_SCHO_CUR),
                 new SqlParameter("@CO_SCHO_ACADEMIC_YEAR",data1.CO_SCHO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_GRADE_CODE",data1.CO_SCHO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_SECTION_CODE" ,data1.CO_SCHO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_CODE",data1.CO_SCHO_GROUP_CODE),
                 new SqlParameter("@CO_SCHO_TO_CUR",data1.CO_SCHO_TO_CUR),
                 new SqlParameter("@CO_SCHO_TO_ACADEMIC_YEAR",data1.CO_SCHO_TO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_TO_GRADE_CODE",data1.CO_SCHO_TO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_TO_SECTION_CODE" ,data1.CO_SCHO_TO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_TO_IsAttribute",data1.CO_SCHO_TO_IsAttribute),
                 new SqlParameter("@CO_SCHO_TO_IsIndicator",data1.CO_SCHO_TO_IsIndicator)

                });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("ScholasticAttributeCopy")]
        public HttpResponseMessage ScholasticAttributeCopy(List<Co_Scholastic_copy> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Co_Scholastic_copy data1 in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[CO_SCHOLASTIC_COPY_PROC]",
                            new List<SqlParameter>()
                         {
                 new SqlParameter("@OPR", "CA"),
                 new SqlParameter("@CO_SCHO_CUR",data1.CO_SCHO_CUR),
                 new SqlParameter("@CO_SCHO_ACADEMIC_YEAR",data1.CO_SCHO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_GRADE_CODE",data1.CO_SCHO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_SECTION_CODE" ,data1.CO_SCHO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_CODE",data1.CO_SCHO_GROUP_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_ATT_CODE ",data1.CO_SCHO_GROUP_ATT_CODE),
                 new SqlParameter("@CO_SCHO_TO_CUR",data1.CO_SCHO_TO_CUR),
                 new SqlParameter("@CO_SCHO_TO_ACADEMIC_YEAR",data1.CO_SCHO_TO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_TO_GRADE_CODE",data1.CO_SCHO_TO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_TO_SECTION_CODE" ,data1.CO_SCHO_TO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_TO_GROUP_CODE",data1.CO_SCHO_TO_GROUP_CODE),
                 new SqlParameter("@CO_SCHO_TO_IsIndicator",data1.CO_SCHO_TO_IsIndicator),


                });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("ScholasticIndicatorCopy")]
        public HttpResponseMessage ScholasticIndicatorCopy(List<Co_Scholastic_copy> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Co_Scholastic_copy data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CO_SCHOLASTIC_COPY_PROC]",
                            new List<SqlParameter>()
                         {
                 new SqlParameter("@OPR", "CI"),
                 new SqlParameter("@CO_SCHO_CUR",data1.CO_SCHO_CUR),
                 new SqlParameter("@CO_SCHO_ACADEMIC_YEAR",data1.CO_SCHO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_GRADE_CODE",data1.CO_SCHO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_SECTION_CODE" ,data1.CO_SCHO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_CODE",data1.CO_SCHO_GROUP_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_ATT_CODE ",data1.CO_SCHO_GROUP_ATT_CODE),
                 new SqlParameter("@CO_SCHO_GROUP_ATT_INDI_CODE",data1.CO_SCHO_GROUP_ATT_INDI_CODE),

                 new SqlParameter("@CO_SCHO_TO_CUR",data1.CO_SCHO_TO_CUR),
                 new SqlParameter("@CO_SCHO_TO_ACADEMIC_YEAR",data1.CO_SCHO_TO_ACADEMIC_YEAR),
                 new SqlParameter("@CO_SCHO_TO_GRADE_CODE",data1.CO_SCHO_TO_GRADE_CODE),
                 new SqlParameter("@CO_SCHO_TO_SECTION_CODE" ,data1.CO_SCHO_TO_SECTION_CODE),
                 new SqlParameter("@CO_SCHO_TO_GROUP_CODE",data1.CO_SCHO_TO_GROUP_CODE),
                 new SqlParameter("@CO_SCHO_TO_GROUP_ATT_CODE ",data1.CO_SCHO_TO_GROUP_ATT_CODE),
                });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        #endregion

        #region Report Card

        [Route("ReportCardlevels")]
        public HttpResponseMessage ReportCardlevels(string data)
        {
            Subject sb = Newtonsoft.Json.JsonConvert.DeserializeObject<Subject>(data);
            List<ReportCardLevel> lst = new List<ReportCardLevel>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "RL"),
                new SqlParameter("@CUR_CODE", sb.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", sb.SubjectAcademic),
                new SqlParameter("@GRADE_CODE", sb.SubjectGrade),
                new SqlParameter("@SECTION_CODE", sb.SubjectSection),
                new SqlParameter("@GB_TEACHER_CODE", sb.SubTeacherEmpId),
                new SqlParameter("@GB_TERM_CODE", sb.SubjectTerm),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ReportCardLevel rpl = new ReportCardLevel();
                            rpl.Level_code = dr["sims_level_code"].ToString();
                            rpl.Level_Name = dr["sims_report_card_name"].ToString();

                            lst.Add(rpl);
                        }
                        lst.ForEach(q => q.Academic_Year = sb.SubjectAcademic);
                        lst.ForEach(q => q.Curr_Code = sb.SubjectCurr);
                        lst.ForEach(q => q.Grade_Code = sb.SubjectGrade);
                        lst.ForEach(q => q.Section_code = sb.SubjectSection);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }



        [Route("getReportCards")]
        public HttpResponseMessage getReportCards(string data)
        {
            Subject sb = Newtonsoft.Json.JsonConvert.DeserializeObject<Subject>(data);

            List<ReportCardLevel> lst = new List<ReportCardLevel>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                        new List<SqlParameter>()

                         {
                new SqlParameter("@OPR", "RC"),
                new SqlParameter("@GB_TEACHER_CODE", sb.SubTeacherEmpId),
                new SqlParameter("@CUR_CODE", sb.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", sb.SubjectAcademic),
                new SqlParameter("@GRADE_CODE", sb.SubjectGrade),
                new SqlParameter("@SECTION_CODE", sb.SubjectSection),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", sb.AssignmentNumber)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ReportCardLevel rpl = new ReportCardLevel();
                            rpl.Report_Card_Name = dr["sims_report_card_name"].ToString();
                            rpl.Report_Card_Code = dr["sims_report_card_code"].ToString();
                            rpl.Report_Card_Desc = dr["sims_report_card_description"].ToString();
                            rpl.Report_Card_Decla_Date = dr["sims_report_card_declaration_date"].ToString();
                            rpl.Report_Card_Publishing_Date = dr["sims_report_card_publishing_date"].ToString();
                            rpl.Report_Card_Freez_Status = dr["sims_report_card_freeze_status"].ToString();
                            rpl.Report_Card_Parameter_Master = dr["sims_report_card_parameter_master_code"].ToString();
                            rpl.Report_Card_User_Code = dr["sims_report_card_created_user_code"].ToString();
                            rpl.Report_Card_Term = dr["sims_report_card_term"].ToString();
                            lst.Add(rpl);
                        }
                        lst.ForEach(q => q.Academic_Year = sb.SubjectAcademic);
                        lst.ForEach(q => q.Curr_Code = sb.SubjectCurr);
                        lst.ForEach(q => q.Grade_Code = sb.SubjectGrade);
                        lst.ForEach(q => q.Section_code = sb.SubjectSection);
                        lst.ForEach(q => q.Level_code = sb.SubTeacherEmpId);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetReportCardComments")]
        public HttpResponseMessage GetReportCardComments(string data)
        {

            ReportCardStudent sb = Newtonsoft.Json.JsonConvert.DeserializeObject<ReportCardStudent>(data);
            List<ReportCardComment> lst = new List<ReportCardComment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "S"),
                new SqlParameter("@sims_level_code", sb.Level_code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                });
                    while (dr.Read())
                    {
                        ReportCardComment rpl = new ReportCardComment();
                        rpl.ReportCardCode = dr["sims_report_card_code"].ToString();
                        rpl.Academic = dr["sims_academic_year"].ToString();
                        rpl.CommentCode = dr["sims_comment_code"].ToString();
                        rpl.CommentDesc = dr["sims_comment_desc"].ToString();
                        rpl.CurrCode = dr["sims_cur_code"].ToString();
                        rpl.LevelCode = dr["sims_level_code"].ToString();
                        rpl.Status = dr["sims_comment_status"].ToString();
                        lst.Add(rpl);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetReportCardStudent")]
        public HttpResponseMessage GetReportCardStudent(string data)
        {

            ReportCardStudent sb = Newtonsoft.Json.JsonConvert.DeserializeObject<ReportCardStudent>(data);

            List<ReportCardStudent> lst = new List<ReportCardStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_student_subject_comment_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                new SqlParameter("@sims_subject_code", sb.Subject_Code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_level_code", sb.Level_code)
                         });
                    while (dr.Read())
                    {
                        ReportCardStudent rpl = new ReportCardStudent();
                        rpl.Report_Card_Name = dr["sims_report_card_name"].ToString();
                        rpl.Report_Card_Code = dr["sims_report_card_code"].ToString();
                        rpl.Comment1_code = dr["sims_comment1_code"].ToString();
                        rpl.Comment1_desc = dr["Comment1"].ToString();
                        rpl.Comment2_code = dr["sims_comment2_code"].ToString();
                        rpl.Comment2_desc = dr["Comment2"].ToString();
                        rpl.enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        rpl.Level_code = dr["sims_level_code"].ToString();
                        rpl.Level_Name = dr["LevelName"].ToString();
                        rpl.Status = dr["sims_student_comment_status"].ToString();
                        rpl.Subject_Code = dr["sims_subject_code"].ToString();
                        rpl.StudName = dr["StudName"].ToString();
                        rpl.user1 = dr["sims_user1_code"].ToString();
                        rpl.user2 = dr["sims_user2_code"].ToString();
                        rpl.included = dr["avi"].ToString();
                        rpl.Academic_Year = sb.Academic_Year;
                        rpl.Curr_Code = sb.Curr_Code;
                        rpl.Grade_Code = sb.Grade_Code;
                        rpl.Section_code = sb.Section_code;
                        lst.Add(rpl);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("ReportCardDataOpr")]
        public HttpResponseMessage ReportCardDataOpr(List<ReportCardStudent> data)
        {
            bool b = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (ReportCardStudent sb in data)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("sims.sims_report_card_student_subject_comment_proc",
                            new List<SqlParameter>()
                         {

                new SqlParameter("@opr", sb.opr),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                new SqlParameter("@sims_subject_code", sb.Subject_Code),
                new SqlParameter("@sims_level_code", sb.Level_code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_enroll_number", sb.enroll),
                new SqlParameter("@sims_comment1_code", sb.Comment1_code),
                new SqlParameter("@sims_comment2_code", sb.Comment2_code),
                new SqlParameter("@sims_user1_code", sb.user1),
                new SqlParameter("@sims_user2_code", sb.user2),
                new SqlParameter("@sims_student_comment_stats", sb.Status),
                new SqlParameter("@sims_student_List_I", sb.EnrollList_I),
                new SqlParameter("@sims_student_List_U", sb.EnrollList_U),
                new SqlParameter("@commentNo", sb.commnetNo)
                });
                        if (dr > 0)
                        {
                            b = true;
                        }
                        else
                        {
                            b = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, b);

        }

        [Route("ReportCardDataOprF")]
        public HttpResponseMessage ReportCardDataOprF(List<ReportCardStudent> sb1)
        {
            bool b = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ReportCardStudent sb in sb1)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("sims.sims_report_card_student_final_comment_proc",
                           new List<SqlParameter>()
                         {
                new SqlParameter("@opr", sb.opr),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                new SqlParameter("@sims_level_code", sb.Level_code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_enroll_number", sb.enroll),
                new SqlParameter("@sims_comment1_code", sb.Comment1_code),
                new SqlParameter("@sims_comment2_code", sb.Comment2_code),
                new SqlParameter("@sims_comment3_code", sb.Comment3_code),
                new SqlParameter("@sims_user1_code", sb.user1),
                new SqlParameter("@sims_user2_code", sb.user2),
                new SqlParameter("@sims_user3_code", sb.user3),
                new SqlParameter("@sims_student_comment_stats", sb.Status),
                new SqlParameter("@sims_student_List_I", sb.EnrollList_I),
                new SqlParameter("@sims_student_List_U", sb.EnrollList_U),
                new SqlParameter("@commentNo", sb.commnetNo)
                         });

                        if (b = dr > 0)
                        {
                            b = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, b);
        }

        #endregion

        #region  schol_grou.
        [Route("GetAllCoScholData")]
        public HttpResponseMessage GetAllCoScholData(string employee_code, string cur_code, string academic_year)
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_group_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_co_scholastic_employee_code",employee_code),
                new SqlParameter("@CURR",cur_code),
                new SqlParameter("@ACADEMIC",academic_year)

               });
                    #region Group Collection
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.Attrlist = new List<schol_Attribute>();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_co_scholastic_group_assessment_count = dr["sims_co_scholastic_group_assessment_count"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_group_description = dr["sims_co_scholastic_group_description"].ToString();
                        sg.sims_co_scholastic_group_display_order = dr["sims_co_scholastic_group_display_order"].ToString();
                        sg.sims_co_scholastic_group_heading = dr["sims_co_scholastic_group_heading"].ToString();
                        sg.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name1"].ToString();
                        sg.sims_co_scholastic_group_name_old = dr["sims_co_scholastic_group_name"].ToString();

                        sg.sims_co_scholastic_group_name_ot = dr["sims_co_scholastic_group_name_ot"].ToString();
                        sg.sims_co_scholastic_group_note = dr["sims_co_scholastic_group_note"].ToString();
                        sg.sims_co_scholastic_group_status = dr["sims_co_scholastic_group_status"].ToString().Equals("A", StringComparison.CurrentCultureIgnoreCase);
                        sg.sims_co_scholastic_group_freeze_status = dr["sims_co_scholastic_group_freeze_status"].ToString().Equals("A", StringComparison.CurrentCultureIgnoreCase);
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_cur_name = dr["cur_name"].ToString();
                        sg.sims_grade_code = dr["sims_grade_code"].ToString();
                        sg.sims_grade_name = dr["grade_name"].ToString();
                        sg.sims_section_code = dr["sims_section_code"].ToString();
                        sg.sims_section_name = dr["section_name"].ToString();
                        sg.sims_co_scholastic_group_dispaly_name = dr["sims_co_scholastic_group_name"].ToString();
                        sg.sims_co_scolastic_employee_name = dr["EmpName"].ToString();
                        sg.sims_co_scolastic_employee_code = dr["sims_co_scholastic_group_employee_code"].ToString();
                        sg.sims_co_scolastic_grading_scale = dr["sims_co_scholastic_group_grade_scale_code"].ToString();
                        sg.sims_co_scolastic_Score_type = dr["sims_report_card_co_scholastic_score_type"].ToString();
                        sg.sims_co_scolastic_User_type = dr["UserType"].ToString();
                        sg.TeacherList = dr["TeacherList"].ToString();
                        lst.Add(sg);
                    }
                    dr.NextResult();
                    #endregion

                    #region AttributeCollection
                    while (dr.Read())
                    {
                        schol_Attribute sa = new schol_Attribute();
                        sa.sims_academic_year = dr["sims_academic_year"].ToString();
                        sa.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        sa.sims_co_scholastic_attribute_display_order = dr["sims_co_scholastic_attribute_display_order"].ToString();
                        sa.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                        sa.sims_co_scholastic_attribute_name_ot = dr["sims_co_scholastic_attribute_name_ot"].ToString();
                        sa.sims_co_scholastic_attribute_dispaly_name = sa.sims_co_scholastic_attribute_name;
                        sa.sims_co_scholastic_attribute_status = dr["sims_co_scholastic_attribute_status"].ToString().Equals("A", StringComparison.CurrentCultureIgnoreCase);
                        sa.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sa.sims_co_scholastic_group_name = dr["groupName"].ToString();
                        sa.sims_cur_code = dr["sims_cur_code"].ToString();
                        sa.sims_cur_name = dr["cur_name"].ToString();
                        sa.sims_grade_code = dr["sims_grade_code"].ToString();
                        sa.sims_grade_name = dr["grade_name"].ToString();
                        sa.sims_section_code = dr["sims_section_code"].ToString();
                        sa.sims_section_name = dr["section_name"].ToString();
                        sa.sims_co_scolastic_employee_code = employee_code;
                        sa.sims_co_scholastic_group_grade_scale_code = dr["sims_co_scholastic_group_grade_scale_code"].ToString();
                        sa.sims_co_scolastic_Score_type = dr["sims_report_card_co_scholastic_score_type"].ToString();
                        schol_group o = lst.Single(q => q.sims_co_scholastic_group_code == sa.sims_co_scholastic_group_code && q.sims_grade_code + q.sims_section_code == sa.sims_grade_code + sa.sims_section_code);
                        if (o != null)
                        {
                            if (o.Attrlist == null) o.Attrlist = new List<schol_Attribute>();
                            o.Attrlist.Add(sa);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region IndicatorCollection
                    while (dr.Read())
                    {
                        DescIndicator di = new DescIndicator();
                        di.sims_academic_year = dr["sims_academic_year"].ToString();
                        di.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                        di.sims_co_scholastic_attribute_name = dr["ATTR_NAME"].ToString();
                        di.sims_co_scholastic_attribute_dispaly_name = di.sims_co_scholastic_attribute_name;
                        di.sims_co_scholastic_discriptive_indicator_code = dr["sims_co_scholastic_discriptive_indicator_code"].ToString();
                        di.sims_co_scholastic_discriptive_indicator_name = dr["sims_co_scholastic_discriptive_indicator_name"].ToString();
                        di.sims_co_scholastic_discriptive_indicator_name_ot = dr["sims_co_scholastic_discriptive_indicator_name_ot"].ToString();
                        di.sims_co_scholastic_discriptive_dispaly_name = di.sims_co_scholastic_discriptive_indicator_name;
                        di.sims_co_scholastic_discriptive_indicator_status = dr["sims_co_scholastic_discriptive_indicator_status"].ToString().Equals("A");
                        di.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        di.sims_co_scholastic_group_name = dr["GROUP_NAME"].ToString();
                        di.sims_cur_code = dr["sims_cur_code"].ToString();
                        di.sims_cur_name = dr["cur_name"].ToString();
                        di.sims_grade_code = dr["sims_grade_code"].ToString();
                        di.sims_grade_name = dr["grade_name"].ToString();
                        di.sims_section_code = dr["sims_section_code"].ToString();
                        di.sims_section_name = dr["section_name"].ToString();
                        di.sims_co_scolastic_teacher_name = dr["sims_co_scolastic_teacher_name"].ToString();
                        di.grade_scaleGroupCode = dr["grade_scaleGroupCode"].ToString();
                        di.sims_co_scolastic_employee_code = dr["sims_co_scolastic_employee_code"].ToString();
                        di.sims_co_scolastic_Score_type = dr["sims_report_card_co_scholastic_score_type"].ToString();
                        schol_Attribute attr = lst.Single(q => q.sims_co_scholastic_group_code == di.sims_co_scholastic_group_code && q.sims_grade_code + q.sims_section_code == di.sims_grade_code + di.sims_section_code).Attrlist.Single(q => q.sims_co_scholastic_attribute_code == di.sims_co_scholastic_attribute_code && q.sims_grade_code + q.sims_section_code == di.sims_grade_code + di.sims_section_code);
                        if (attr.indlist == null) attr.indlist = new List<DescIndicator>();
                        attr.indlist.Add(di);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetAllTeacher")]
        public HttpResponseMessage GetAllTeacher()
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholatic_attribute_proc]",
                        new List<SqlParameter>()
                         {
                   new SqlParameter("@opr","T")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            gradesectionsubjectteacher teacher = new gradesectionsubjectteacher();
                            teacher.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            teacher.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            teacher.sims_employee_code = dr["sims_employee_code"].ToString();
                            lst.Add(teacher);

                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getAllCoScholTeacherName")]
        public HttpResponseMessage getAllCoScholTeacherName(string group_code, string grade_code, string section_code, string academic_year)
        {
            List<gradesectionsubjectteacher> lst = new List<gradesectionsubjectteacher>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_co_scholastic_group_proc]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "N"),
                 new SqlParameter("@sims_co_scholastic_group_code", group_code),
                    new SqlParameter("@sims_grade_code", grade_code),
                   new SqlParameter("@sims_section_code",section_code),
                    new SqlParameter("@sims_academic_year",academic_year)

                         });

                    while (dr.Read())
                    {
                        gradesectionsubjectteacher gst = new gradesectionsubjectteacher();
                        gst.sims_employee_code = dr["sims_co_scholastic_group_teacher_code"].ToString();
                        gst.teacher_freeze_status = dr["sims_co_scholastic_group_status"].ToString().Equals("A") ? false : true;
                        lst.Add(gst);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("CoScholGroupDataOprTeacherFreeze")]
        public HttpResponseMessage CoScholGroupDataOprTeacherFreeze(List<schol_group> data)
        {

            bool lst = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (schol_group data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_co_scholastic_group_proc]",
                            new List<SqlParameter>()
                         {

                   new SqlParameter("@opr", "Z"),
                   new SqlParameter("@sims_academic_year",data1.sims_academic_year),
                   new SqlParameter("@sims_cur_code",data1.sims_cur_code),
                   new SqlParameter("@sims_co_scholastic_group_code", data1.sims_co_scholastic_group_code),
                   new SqlParameter("@sims_grade_code", data1.sims_grade_code),
                   new SqlParameter("@sims_section_code",data1.sims_section_code),
                   new SqlParameter("@sims_co_scholastic_employee_code",data1.sims_co_scolastic_employee_code),
                   new SqlParameter("@teacher_freeze_status",(data1.teacher_freeze_status==true)?"I":"A")
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            lst = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);


        }

        [Route("GetAllIndicatorNames")]
        public HttpResponseMessage GetAllIndicatorNames(string employee_code)
        {
            List<menuclass> lst = new List<menuclass>();
            string str = "";
            string str1 = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_group_proc]",
                        new List<SqlParameter>() {  new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_co_scholastic_employee_code",employee_code)
                        });
                    while (dr.Read())
                    {
                        menuclass m = new menuclass();
                        m.secList = new List<submenuclass>();
                        schol_group sg = new schol_group();
                        sg.Attrlist = new List<schol_Attribute>();
                        sg.sims_cur_code = dr["sims_cur_code"].ToString();
                        sg.sims_academic_year = dr["sims_academic_year"].ToString();
                        sg.sims_cur_name = dr["cur_name"].ToString();
                        sg.sims_grade_code = dr["sims_grade_code"].ToString();
                        sg.sims_grade_name = dr["grade_name"].ToString();
                        sg.sims_section_code = dr["sims_section_code"].ToString();
                        sg.sims_section_name = dr["section_name"].ToString();
                        sg.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        sg.sims_co_scholastic_group_name = dr["sims_co_scholastic_group_name"].ToString();
                        m.sims_cur_code = sg.sims_cur_code;
                        m.sims_academic_year = sg.sims_academic_year;
                        m.sims_grade_code = sg.sims_grade_code;
                        m.sims_grade_name = sg.sims_grade_name;
                        int c = lst.Count(q => q.sims_grade_name == m.sims_grade_name && q.sims_grade_code == m.sims_grade_code);
                        if (c == 0)
                        {
                            submenuclass sb = new submenuclass();
                            sb.sims_cur_code = sg.sims_cur_code;
                            sb.sims_academic_year = sg.sims_academic_year;
                            sb.sims_grade_code = sg.sims_grade_code;
                            sb.sims_grade_name = sg.sims_grade_name;
                            sb.sims_section_code = sg.sims_section_code;
                            sb.sims_section_name = sg.sims_section_name;
                            sb.groupList = new List<schol_group>();
                            sb.groupList.Add(sg);
                            m.secList.Add(sb);
                            lst.Add(m);
                        }
                        else
                        {
                            submenuclass sb = new submenuclass();
                            sb.sims_cur_code = sg.sims_cur_code;
                            sb.sims_academic_year = sg.sims_academic_year;
                            sb.sims_grade_code = sg.sims_grade_code;
                            sb.sims_grade_name = sg.sims_grade_name;
                            sb.sims_section_code = sg.sims_section_code;
                            sb.sims_section_name = sg.sims_section_name;
                            sb.groupList = new List<schol_group>();
                            sb.groupList.Add(sg);
                            var gl = lst.Where(q => q.sims_grade_code == m.sims_grade_code && q.sims_grade_name == m.sims_grade_name);
                            gl.ElementAt(0).secList.Add(sb);
                        }
                    }
                    dr.NextResult();

                    while (dr.Read())
                    {
                        try
                        {
                            schol_Attribute a = new schol_Attribute();
                            a.indlist = new List<DescIndicator>();
                            a.sims_cur_code = dr["sims_cur_code"].ToString();
                            a.sims_academic_year = dr["sims_academic_year"].ToString();
                            a.sims_cur_name = dr["cur_name"].ToString();
                            a.sims_grade_code = dr["sims_grade_code"].ToString();
                            a.sims_grade_name = dr["grade_name"].ToString();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_name = dr["section_name"].ToString();
                            a.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                            a.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                            a.sims_co_scholastic_attribute_name = dr["sims_co_scholastic_attribute_name"].ToString();
                            var grd = (from p in lst where p.sims_grade_code == a.sims_grade_code select p).ToList();
                            if (grd.Count > 0)
                            {
                                var sec = (from p in grd.ElementAt(0).secList where p.sims_section_code == a.sims_section_code select p).ToList();
                                if (sec.Count > 0)
                                {
                                    str1 = a.sims_co_scholastic_group_code;
                                    var grup = (from p in sec.ElementAt(0).groupList where p.sims_co_scholastic_group_code == a.sims_co_scholastic_group_code select p).ToList();
                                    if (grup.Count > 0)
                                        grup.ElementAt(0).Attrlist.Add(a);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        try
                        {
                            DescIndicator a = new DescIndicator();
                            a.sims_cur_code = dr["sims_cur_code"].ToString();
                            a.sims_academic_year = dr["sims_academic_year"].ToString();
                            a.sims_cur_name = dr["cur_name"].ToString();
                            a.sims_grade_code = dr["sims_grade_code"].ToString();
                            a.sims_grade_name = dr["grade_name"].ToString();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_name = dr["section_name"].ToString();
                            a.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                            a.sims_co_scholastic_attribute_code = dr["sims_co_scholastic_attribute_code"].ToString();
                            a.sims_co_scholastic_discriptive_indicator_code = dr["sims_co_scholastic_discriptive_indicator_code"].ToString();
                            a.sims_co_scholastic_discriptive_indicator_name = dr["sims_co_scholastic_discriptive_indicator_name"].ToString();
                            var grd = (from p in lst where p.sims_grade_code == a.sims_grade_code select p).ToList();
                            if (grd.Count > 0)
                            {
                                var sec = (from p in grd.ElementAt(0).secList where p.sims_section_code == a.sims_section_code select p).ToList();
                                if (sec.Count > 0)
                                {
                                    var grup = (from p in sec.ElementAt(0).groupList where p.sims_co_scholastic_group_code == a.sims_co_scholastic_group_code select p).ToList();
                                    if (grup.Count > 0)
                                    {
                                        var attr = (from p in grup.ElementAt(0).Attrlist where p.sims_co_scholastic_attribute_code == a.sims_co_scholastic_attribute_code select p).ToList();
                                        if (attr.Count > 0)
                                        {
                                            attr.ElementAt(0).indlist.Add(a);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getGradeScaleForcoscholasticgroup")]
        public HttpResponseMessage getGradeScaleForcoscholasticgroup()
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_group_proc",
                        new List<SqlParameter>()
                         {
                   new SqlParameter("@opr","G")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            schol_group teacher = new schol_group();
                            teacher.sims_cur_code = dr["sims_cur_code"].ToString();
                            teacher.sims_academic_year = dr["sims_academic_year"].ToString();
                            teacher.sims_co_scolastic_grading_scale = dr["sims_co_scholastic_grade_scale_code"].ToString();
                            teacher.sims_co_scholastic_grade_scale_description = dr["sims_co_scholastic_grade_scale_description"].ToString();
                            teacher.sims_co_scholastic_grade_scale_status = dr["sims_co_scholastic_grade_scale_status"].ToString();
                            lst.Add(teacher);

                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CoScholGroupDataOpr")]
        public HttpResponseMessage CoScholGroupDataOpr(schol_group data)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_group_proc",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_group_code", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_group_name", data.sims_co_scholastic_group_name),
                    new SqlParameter("@sims_co_scholastic_group_name_ot", data.sims_co_scholastic_group_name_ot),
                    new SqlParameter("@sims_co_scholastic_group_description", data.sims_co_scholastic_group_description),
                    new SqlParameter("@sims_co_scholastic_group_heading", data.sims_co_scholastic_group_heading),
                    new SqlParameter("@sims_co_scholastic_group_note", data.sims_co_scholastic_group_note),
                    new SqlParameter("@sims_co_scholastic_group_assessment_count", data.sims_co_scholastic_group_assessment_count),
                    new SqlParameter("@sims_co_scholastic_group_display_order", data.sims_co_scholastic_group_display_order),
                    new SqlParameter("@sims_co_scholastic_grading_scale", data.sims_co_scolastic_grading_scale),
                    new SqlParameter("@sims_report_card_co_scholastic_score_type", data.sims_co_scolastic_Score_type),
                    new SqlParameter("@sims_co_scholastic_teacherList",data.sims_co_scholastic_teacherList),
                    new SqlParameter("@sims_grd_sec_list",data.sims_grd_sec_list),
                    new SqlParameter("@sims_co_scholastic_group_status ", (data.sims_co_scholastic_group_status==true) ? 'A' : 'I'),
                    new SqlParameter("@sims_co_scholastic_group_freeze_status ", (data.sims_co_scholastic_group_freeze_status==true) ? 'A' : 'I'),

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }


            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CoScholAttrDataOpr")]
        public HttpResponseMessage CoScholAttrDataOpr(List<schol_Attribute> data1)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (schol_Attribute data in data1)
                    {
                        if (!data.opr.Equals("A"))
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholatic_attribute_proc",
                                new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_group_code ", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_attribute_name", data.sims_co_scholastic_attribute_name),
                    new SqlParameter("@sims_co_scholastic_attribute_name_ot", data.sims_co_scholastic_attribute_name_ot),
                    new SqlParameter("@sims_co_scholastic_attribute_display_order", data.sims_co_scholastic_attribute_display_order),
                    new SqlParameter("@sims_co_scholastic_group_grade_scale_code",data.sims_co_scholastic_group_grade_scale_code),
                    new SqlParameter("@sims_co_scholastic_attribute_status", (data.sims_co_scholastic_attribute_status==true) ? 'A' : 'I'),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }


            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CoScholDescInd")]
        public HttpResponseMessage CoScholDescInd(List<DescIndicator> data1)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DescIndicator data in data1)
                    {
                        if (!data.opr.Equals("A"))
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_descriptive_indicator_proc",
                                new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_group_code ", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_term_code",data.sims_term_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name", data.sims_co_scholastic_discriptive_indicator_name),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name_ot", data.sims_co_scholastic_discriptive_indicator_name_ot),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),
                    new SqlParameter("@sims_co_scolastic_employee_code",data.sims_co_scolastic_employee_code),
                    new SqlParameter("@sims_co_scholastic_group_grade_scale_code",data.grade_scaleGroupCode)

                   });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CoScholDescIndInsert")]
        public HttpResponseMessage CoScholDescIndInsert(List<DescIndicator> data1)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DescIndicator data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_descriptive_indicator_proc",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_group_code ", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_term_code",data.sims_term_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name", data.sims_co_scholastic_discriptive_indicator_name),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_name_ot", data.sims_co_scholastic_discriptive_indicator_name_ot),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_status", (data.sims_co_scholastic_discriptive_indicator_status==true) ? 'A' : 'I'),
                    new SqlParameter("@StudentList",data.StudentList),
                    new SqlParameter("@sims_co_scholastic_teacher_code",data.sims_co_scolastic_employee_code),
                    new SqlParameter("@sims_co_scholastic_group_grade_scale_code",data.grade_scaleGroupCode)
                   });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CoScholTeacher")]
        public HttpResponseMessage CoScholTeacher(ScholStudent stdDet)
        {
            List<Subject> lst = new List<Subject>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                       new List<SqlParameter>()
                         {

                new SqlParameter("@opr", stdDet.opr),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.Stud_indic_teacher_code),
                new SqlParameter("@sims_co_scholastic_term_code",stdDet.sims_term_code)
                });
                    while (dr.Read())
                    {
                        Subject sg = new Subject();
                        sg.SubTeacherEmpId = dr["sims_employee_code"].ToString();
                        sg.SubTeacherId = dr["sims_teacher_code"].ToString();
                        sg.TeacherName = dr["sims_teacher_name_f"].ToString();
                        sg.TeacherStatus = dr["sims_co_scholastic_group_status"].ToString().Equals("I") ? true : false;
                        lst.Add(sg);
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        //[Route("CheckCoScholTeacher")]
        //public HttpResponseMessage CheckCoScholTeacher(ScholStudent stdDet)
        //{
        //    bool insert = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
        //               new List<SqlParameter>() 
        //                 { 

        //        new SqlParameter("@opr", "CC"),
        //        new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
        //        new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
        //        new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
        //        new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
        //        new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
        //        new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
        //        new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
        //        new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.Stud_indic_teacher_code),
        //        });
        //            while (dr.Read())
        //            {
        //                insert = true;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, insert);

        //}

        [Route("UnsertStudentCo_Sholastic_Indicator")]
        public HttpResponseMessage UnsertStudentCo_Sholastic_Indicator(ScholStudent stdDet)
        {
            List<ScholStudent> lst = new List<ScholStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_descriptive_indicator_proc]",
                       new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "Q"),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.Stud_indic_teacher_code),
                new SqlParameter("@sims_term_code", stdDet.sims_term_code),
                });
                    if (dr.RecordsAffected > 0)
                    {
                        dr.Close();

                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_student_proc]",
                      new List<SqlParameter>()
                         {

               new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_co_scholastic_term_code", stdDet.sims_term_code),
                new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.Stud_indic_teacher_code)//--------------
                         });
                        while (dr1.Read())
                        {
                            ScholStudent std = new ScholStudent();
                            std.IndCode = dr1["sims_co_scholastic_discriptive_indicator_code"].ToString();
                            std.Ind_Name = dr1["Ind_name"].ToString();
                            std.Stud_AcademicYear = dr1["sims_academic_year"].ToString();
                            std.Stud_CurrCode = dr1["sims_cur_code"].ToString();
                            std.Stud_Enroll = dr1["sims_co_scholastic_enroll_number"].ToString();
                            std.Stud_FirstName = dr1["StudFirstName"].ToString();
                            std.Stud_GradeCode = dr1["sims_grade_code"].ToString();
                            std.Stud_GradeName = dr1["GradeName"].ToString();
                            std.Stud_LastName = dr1["StudLastName"].ToString();
                            std.Stud_MidName = dr1["StudMiddName"].ToString();
                            std.Stud_Full_Name = std.Stud_FirstName + ' ' + std.Stud_MidName + ' ' + std.Stud_LastName;
                            std.Stud_SectionCode = dr1["sims_section_code"].ToString();
                            std.Grade_Point = dr1["sims_report_card_co_scholastic_grade_point"].ToString();
                            std.AttribNo = dr1["sims_co_scholastic_attribute_code"].ToString();
                            std.AttribName = dr1["attribute_name"].ToString();
                            std.Grade_Scale_C = dr1["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();///scale Code
                            std.Grade_C = dr1["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();///grade code
                            std.Group_No = dr1["sims_co_scholastic_group_code"].ToString();
                            std.Color_Code = dr1["ColorCode"].ToString();///Color Code
                            std.Grade_Desc = dr1["Desc"].ToString();///Description
                            std.Grade_D = dr1["GradeName"].ToString();///Grade Name
                            std.Group_Name = dr1["group_name"].ToString();
                            std.sims_co_scolastic_Score_type = dr1["Score_Type"].ToString();
                            std.Grade_Scale_C = dr1["sims_co_scholastic_group_grade_scale_code"].ToString();
                            lst.Add(std);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("ScholStudentDataOpr")]
        public HttpResponseMessage ScholStudentDataOpr(DescIndicator ClassDetails)
        {
            List<ScholStudent> lst = new List<ScholStudent>();
            string str = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_descriptive_indicator_proc",
                    new List<SqlParameter>()
                    {
                new SqlParameter("@opr", "G"),
                new SqlParameter("@sims_academic_year", ClassDetails.sims_academic_year),
                new SqlParameter("@sims_cur_code", ClassDetails.sims_cur_code),
                new SqlParameter("@sims_grade_code", ClassDetails.sims_grade_code),
                new SqlParameter("@sims_section_code", ClassDetails.sims_section_code),
                new SqlParameter("@sims_co_scholastic_group_code ", ClassDetails.sims_co_scholastic_group_code),
                new SqlParameter("@sims_co_scholastic_attribute_code", ClassDetails.sims_co_scholastic_attribute_code),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", ClassDetails.sims_co_scholastic_discriptive_indicator_code)
                   });
                    while (dr.Read())
                    {
                        str = dr["sims_enroll_number"].ToString();
                        var v = (from p in lst where p.Stud_Enroll == str select p);
                        if (v.Count() == 0)
                        {
                            ScholStudent std = new ScholStudent();
                            std.termlist = new List<termdesccode>();

                            termdesccode t = new termdesccode();
                            std.Stud_AcademicYear = dr["sims_academic_year"].ToString();
                            t.Stud_indic_teacher_code = dr["sims_co_scholastic_teacher_code"].ToString();
                            std.Stud_CurrCode = dr["sims_cur_code"].ToString();
                            std.Stud_Enroll = dr["sims_enroll_number"].ToString();
                            std.Stud_FirstName = dr["StudFirstName"].ToString();
                            std.Stud_GradeCode = dr["sims_grade_code"].ToString();
                            std.Stud_LastName = dr["StudLastName"].ToString();
                            std.Stud_MidName = dr["StudMiddName"].ToString();
                            std.Stud_SectionCode = dr["sims_section_code"].ToString();
                            std.StudFlag = !(dr["terms"] + "").Equals("0");
                            std.isEnabled = (dr["isEnabled"] + "").Equals("0");
                            std.Stud_Full_Name = std.Stud_FirstName + ' ' + std.Stud_MidName + ' ' + std.Stud_LastName;
                            t.sims_term_code = dr["sims_term_code"].ToString();
                            t.sims_term_desc = dr["sims_term_desc_en"].ToString();
                            t.StudFlag = !(dr["terms"] + "").Equals("0");
                            std.termlist.Add(t);
                            lst.Add(std);
                        }
                        else
                        {
                            termdesccode term = new termdesccode();
                            term.Stud_indic_teacher_code = dr["sims_co_scholastic_teacher_code"].ToString();
                            term.sims_term_code = dr["sims_term_code"].ToString();
                            term.sims_term_desc = dr["sims_term_desc_en"].ToString();
                            term.StudFlag = !(dr["terms"] + "").Equals("0");
                            v.ElementAt(0).termlist.Add(term);
                        }
                    }
                }
            }



            catch (Exception ex)
            {
                //
                //gberror.NotifyError("Co-Schol/Stud-assign-Fetch", "sims_report_card_co_scholastic_descriptive_indicator", ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ScholStudents")]
        public HttpResponseMessage ScholStudents(ScholStudent stdDet)
        {
            List<ScholStudent> lst = new List<ScholStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_co_scholastic_term_code", stdDet.sims_term_code),
                new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.SubTeacherEmpId)//--------------
                         });
                    while (dr.Read())
                    {
                        ScholStudent std = new ScholStudent();
                        std.IndCode = dr["sims_co_scholastic_discriptive_indicator_code"].ToString();
                        std.Ind_Name = dr["Ind_name"].ToString();
                        std.Stud_AcademicYear = dr["sims_academic_year"].ToString();
                        std.Stud_CurrCode = dr["sims_cur_code"].ToString();
                        std.Stud_Enroll = dr["sims_co_scholastic_enroll_number"].ToString();
                        std.Stud_FirstName = dr["StudFirstName"].ToString();
                        std.Stud_GradeCode = dr["sims_grade_code"].ToString();
                        std.Stud_GradeName = dr["GradeName"].ToString();
                        std.Stud_LastName = dr["StudLastName"].ToString();
                        std.Stud_MidName = dr["StudMiddName"].ToString();
                        std.Stud_Full_Name = std.Stud_FirstName + ' ' + std.Stud_MidName + ' ' + std.Stud_LastName;
                        std.Stud_SectionCode = dr["sims_section_code"].ToString();
                        std.Grade_Point = dr["sims_report_card_co_scholastic_grade_point"].ToString();
                        std.AttribNo = dr["sims_co_scholastic_attribute_code"].ToString();
                        std.AttribName = dr["attribute_name"].ToString();
                        std.Grade_Scale_C = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();///scale Code

                        try {
                            std.Grade_C = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString()+"_"+dr["sims_co_scholastic_discriptive_indicator_grade"].ToString();///grade code     sims_co_scholastic_discriptive_indicator_grade
                        }
                        catch (Exception e) {

                        }

                        std.Group_No = dr["sims_co_scholastic_group_code"].ToString();
                        std.Color_Code = dr["ColorCode"].ToString();///Color Code
                        std.Grade_Desc = dr["Desc"].ToString();///Description
                        std.Grade_D = dr["GradeName"].ToString();///Grade Name
                        std.Group_Name = dr["group_name"].ToString();
                        std.sims_co_scolastic_Score_type = dr["Score_Type"].ToString();
                        std.Grade_Scale_C = dr["sims_co_scholastic_group_grade_scale_code"].ToString();
                        lst.Add(std);
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //gberror.NotifyError("Co-Schol/Stud-Fetch", "sims_report_card_co_scholastic_student", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ScholStudentspearl")]
        public HttpResponseMessage ScholStudentspearl(ScholStudent stdDet)
        {
            List<ScholStudent> lst = new List<ScholStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SS"),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_co_scholastic_term_code", stdDet.sims_term_code),
                new SqlParameter("@sims_co_scholastic_teacher_code", stdDet.SubTeacherEmpId)//--------------
                         });
                    while (dr.Read())
                    {
                        string count = "0";

                        count = dr["countter"].ToString();

                        if (count != "0")
                        {
                            ScholStudent std = new ScholStudent();
                            std.IndCode = dr["sims_co_scholastic_discriptive_indicator_code"].ToString();
                            std.Ind_Name = dr["Ind_name"].ToString();
                            std.Stud_AcademicYear = dr["sims_academic_year"].ToString();
                            std.Stud_CurrCode = dr["sims_cur_code"].ToString();
                            std.Stud_Enroll = dr["sims_co_scholastic_enroll_number"].ToString();
                            std.Stud_FirstName = dr["StudFirstName"].ToString();
                            std.Stud_GradeCode = dr["sims_grade_code"].ToString();
                            std.Stud_GradeName = dr["GradeName"].ToString();
                            std.Stud_LastName = dr["StudLastName"].ToString();
                            std.Stud_MidName = dr["StudMiddName"].ToString();
                            std.Stud_Full_Name = std.Stud_FirstName + ' ' + std.Stud_MidName + ' ' + std.Stud_LastName;
                            std.Stud_SectionCode = dr["sims_section_code"].ToString();
                            std.Grade_Point = dr["sims_report_card_co_scholastic_grade_point"].ToString();
                            std.AttribNo = dr["sims_co_scholastic_attribute_code"].ToString();
                            std.AttribName = dr["attribute_name"].ToString();
                            std.Grade_Scale_C = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();///scale Code
                           // std.Grade_C = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString();///grade code
                            std.Group_No = dr["sims_co_scholastic_group_code"].ToString();
                            std.Color_Code = dr["ColorCode"].ToString();///Color Code
                            std.Grade_Desc = dr["Desc"].ToString();///Description
                            std.Grade_D = dr["GradeName"].ToString();///Grade Name
                            std.Group_Name = dr["group_name"].ToString();
                            std.sims_co_scolastic_Score_type = dr["Score_Type"].ToString();
                            std.Grade_Scale_C = dr["sims_co_scholastic_group_grade_scale_code"].ToString();

                            try
                            {
                                std.Grade_C = dr["sims_co_scholastic_discriptive_indicator_grade_code"].ToString() + "_" + dr["sims_co_scholastic_discriptive_indicator_grade"].ToString();///grade code     sims_co_scholastic_discriptive_indicator_grade
                            }
                            catch (Exception e)
                            {

                            }

                            lst.Add(std);
                        }
                        else
                        {
                            ScholStudent std1 = new ScholStudent();
                            std1.SubTeacherEmpId = dr["sims_employee_code"].ToString();
                            std1.Stud_Full_Name = dr["sims_teacher_name"].ToString();
                            std1.Stud_Enroll = "1";
                            lst.Add(std1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //gberror.NotifyError("Co-Schol/Stud-Fetch", "sims_report_card_co_scholastic_student", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("AssignScholGrade")]
        public HttpResponseMessage AssignScholGrade(List<ScholStudent> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ScholStudent stdDet in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", stdDet.opr),
                new SqlParameter("@sims_academic_year", stdDet.Stud_AcademicYear),
                new SqlParameter("@sims_cur_code", stdDet.Stud_CurrCode),
                new SqlParameter("@sims_grade_code", stdDet.Stud_GradeCode),
                new SqlParameter("@sims_section_code", stdDet.Stud_SectionCode),
                new SqlParameter("@sims_co_scholastic_group_code ", stdDet.Group_No),
                new SqlParameter("@sims_co_scholastic_attribute_code", stdDet.AttribNo),
                new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", stdDet.IndCode),
                new SqlParameter("@sims_enroll", stdDet.Stud_Enroll),
                new SqlParameter("@ScholGradeCode", stdDet.Grade_C.Substring(0,5)=="Clear"?null:stdDet.Grade_C),
                new SqlParameter("@ScholGradeSchol", stdDet.grade_desc_group),
                new SqlParameter("@StudentList", stdDet.Stud_Enroll),
                new SqlParameter("@DescIndList", stdDet.IndCode),
                new SqlParameter("@ScholGradePoint", stdDet.Grade_Point==""?null:stdDet.Grade_Point),
                new SqlParameter("@sims_co_scholastic_term_code",stdDet.sims_term_code),
                new SqlParameter("@sims_co_scholastic_teacher_code",stdDet.SubTeacherEmpId),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("ScholGrades")]
        public HttpResponseMessage ScholGrades(string ScholGradeCode,string curCode,string acaYear)
        {
            List<ScholGrade> lst = new List<ScholGrade>();
            try
            {

                string GradegruopCode = "";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "GS"),
                new SqlParameter("@ScholGradeCode",ScholGradeCode),
                new SqlParameter("@sims_cur_code",curCode),
                new SqlParameter("@sims_academic_year",acaYear)
                         });
                    while (dr.Read())
                    {
                        ScholGrade sg = new ScholGrade();
                        sg.Grade_Code = dr["sims_co_scholastic_grade_code"].ToString();
                        sg.Grade_Name = dr["Grade_DisplayMember"].ToString();
                        sg.Grade_Description = dr["sims_co_scholastic_grade_description"].ToString();
                        sg.Grade_Point_high = dr["sims_co_scholastic_grade_point_high"].ToString();
                        sg.Grade_Point_low = dr["sims_co_scholastic_grade_point_low"].ToString();
                        sg.GradegruopCode = dr["sims_co_scholastic_grade_scale_code"].ToString();
                        GradegruopCode = dr["sims_co_scholastic_grade_scale_code"].ToString();
                        lst.Add(sg);
                    }
                }

                ScholGrade sg1 = new ScholGrade();
                sg1.Grade_Code = "Clear";
                sg1.Grade_Name = "Clear";
                sg1.Grade_Description = "";
                sg1.Grade_Point_high = "";
                sg1.Grade_Point_low = "";
                sg1.GradegruopCode = GradegruopCode;
                lst.Add(sg1);

            }
            catch (Exception ex)
            {
                //    
                //    gberror.NotifyError("Co-Schol/Get-Grade", "sims_report_card_co_scholastic_student", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetTerm")]
        public HttpResponseMessage GetTerm()
        {
            List<termdesccode> lst = new List<termdesccode>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_descriptive_indicator_proc]",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "Z")
                });
                    while (dr.Read())
                    {
                        termdesccode sg = new termdesccode();
                        sg.sims_term_code = dr["sims_term_code"].ToString();
                        sg.sims_term_desc = dr["sims_term_desc"].ToString();
                        lst.Add(sg);
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //gberror.NotifyError("Co-Schol/Get-Grade-Point", "sims_report_card_co_scholastic_student", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getCommenttype")]
        public HttpResponseMessage getCommenttype()
        {
            List<student_report_card_sub_comment> lst = new List<student_report_card_sub_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_header_proc]",
                         new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", "P")

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_sub_comment obj = new student_report_card_sub_comment();
                            obj.sims_type = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_comment_code = dr["sims_appl_parameter"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("getCommenttype_new")]
        public HttpResponseMessage getCommenttype_new(string uname)
        {
            List<student_report_card_sub_comment> lst = new List<student_report_card_sub_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_header_proc]",
                         new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", "P"),
                    new SqlParameter("@user_name", uname)

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_sub_comment obj = new student_report_card_sub_comment();
                            obj.sims_type = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_comment_code = dr["sims_appl_parameter"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetCOScoreType")]
        public HttpResponseMessage GetCOScoreType()
        {
            List<schol_group> lst = new List<schol_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_student_proc",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "C")
                });
                    while (dr.Read())
                    {
                        schol_group sg = new schol_group();
                        sg.sims_co_scolastic_Score_type = dr["sims_appl_parameter"].ToString();
                        sg.sims_co_scolastic_Score_type_Descc = dr["sims_appl_form_field_value1"].ToString();
                        lst.Add(sg);
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //gberror.NotifyError("Co-Schol/Get-Grade-Point", "sims_report_card_co_scholastic_student", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CoScholDescIndstudent_delete")]
        public HttpResponseMessage CoScholDescIndstudent_delete(List<DescIndicator> data1)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DescIndicator data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholastic_descriptive_indicator_proc",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", data.opr),
                    new SqlParameter("@sims_academic_year", data.sims_academic_year),
                    new SqlParameter("@sims_cur_code", data.sims_cur_code),
                    new SqlParameter("@sims_grade_code", data.sims_grade_code),
                    new SqlParameter("@sims_section_code", data.sims_section_code),
                    new SqlParameter("@sims_co_scholastic_group_code ", data.sims_co_scholastic_group_code),
                    new SqlParameter("@sims_term_code",data.sims_term_code),
                    new SqlParameter("@sims_co_scholastic_attribute_code", data.sims_co_scholastic_attribute_code),
                    new SqlParameter("@sims_co_scholastic_discriptive_indicator_code", data.sims_co_scholastic_discriptive_indicator_code),
                    new SqlParameter("@StudentList",data.StudentList),
                    new SqlParameter("@sims_co_scholastic_teacher_code",data.sims_co_scolastic_employee_code)
                   });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #endregion

        #region Subject Attribute
        [Route("GetSubAttrGroup")]
        public HttpResponseMessage GetSubAttrGroup(SubjectAttr data)
        {
            List<SubjectAttr> lst = new List<SubjectAttr>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_subject_attribute_proc",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", data.CurrCode),
                new SqlParameter("@sims_academic_year", data.AcademicYear),
                new SqlParameter("@sims_grade_code", data.GradeCode),
                new SqlParameter("@sims_section_code", data.SectionCode),
                new SqlParameter("@sims_subject_code", data.SubjectCode),
                new SqlParameter("@sims_gb_number", data.gb_number),
                new SqlParameter("@sims_gb_cat_number", data.gb_cat_number),
                new SqlParameter("@sims_gb_cat_assign_number", data.gb_cat_assign_number),
                new SqlParameter("@sims_subject_attribute_group_code", data.Group_Code),
                new SqlParameter("@sims_subject_attribute_code", data.Attr_Code)
                });
                    while (dr.Read())
                    {
                        SubjectAttr sg = new SubjectAttr();
                        sg.AcademicYear = dr["sims_academic_year"].ToString();
                        sg.CurrCode = dr["sims_cur_code"].ToString();
                        sg.GradeCode = dr["sims_grade_code"].ToString();
                        sg.SectionCode = dr["sims_section_code"].ToString();
                        sg.Attr_Code = dr["sims_subject_attribute_code"].ToString();
                        sg.Attr_Name = dr["sims_subject_attribute_name"].ToString();
                        sg.Display_Order = dr["sims_subject_attribute_display_order"].ToString();
                        sg.Group_Code = dr["sims_subject_attribute_group_code"].ToString();
                        sg.Group_Name = dr["sims_attribute_group_name"].ToString();
                        sg.Attr_Marks = dr["MaxScore"].ToString();
                        sg.Attr_MaxScore_Grading = dr["MaxScoreGrading"].ToString();
                        sg.attr_Status = dr["sims_subject_attribute_status"].ToString().Equals("A");
                        lst.Add(sg);
                    }
                }
            }
            catch (Exception ex)
            {
                //gberror.NotifyError("AttrGroup", "sims.sims_report_card_attribute", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetAttributeStudent")]
        public HttpResponseMessage GetAttributeStudent(SubjectAttr data)
        {
            List<AttributeStudent> lst = new List<AttributeStudent>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_subject_attribute_proc",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "G"),
               new SqlParameter("@sims_cur_code", data.CurrCode),
               new SqlParameter("@sims_academic_year", data.AcademicYear),
               new SqlParameter("@sims_grade_code", data.GradeCode),
               new SqlParameter("@sims_section_code", data.SectionCode),
               new SqlParameter("@sims_subject_code", data.SubjectCode),
               new SqlParameter("@sims_gb_number", data.gb_number),
               new SqlParameter("@sims_gb_cat_number", data.gb_cat_number),
               new SqlParameter("@sims_gb_cat_assign_number", data.gb_cat_assign_number),
               new SqlParameter("@sims_subject_attribute_group_code", data.Group_Code),
               new SqlParameter("@sims_subject_attribute_code", data.Attr_Code)
                 });
                    while (dr.Read())
                    {
                        AttributeStudent atts = new AttributeStudent();
                        atts.Attr_cat_code = data.gb_cat_number;
                        atts.Attr_Code = data.Attr_Code;
                        atts.Attr_Comment = dr["sims_gb_cat_assign_subject_attribute_comment"].ToString();
                        atts.CompletedDate = dr["sims_gb_cat_assign_subject_attribute_completed_date"].ToString();
                        atts.Attr_final_grade = dr["sims_gb_cat_assign_subject_attribute_final_grade"].ToString();
                        atts.Attr_final_grade_desc = dr["GradeDesc"].ToString();
                        atts.Attr_gb_number = data.gb_number;
                        atts.Attr_Mark = dr["sims_gb_cat_assign_subject_attribute_mark"].ToString();
                        atts.Attr_max_correct = dr["sims_gb_cat_assign_subject_attribute_max_correct"].ToString();
                        atts.Attr_max_point = dr["sims_gb_cat_assign_subject_attribute_max_point"].ToString();
                        atts.Attr_MaxScore = dr["MaxScore"].ToString();
                        //atts.Attr_Name = dr["AttrName"].ToString();
                        atts.AttribNo = data.Attr_Code;
                        atts.Color_Code = dr["ColorCode"].ToString();
                        //atts.Group_Name = dr["GroupName"].ToString();
                        atts.Group_No = dr["sims_subject_attribute_group_code"].ToString();
                        atts.Stud_Enroll = dr["sims_subject_attribute_enroll_number"].ToString();
                        atts.Stud_FirstName = dr["StudFirstName"].ToString();
                        atts.Stud_GradeCode = data.GradeCode;
                        atts.Stud_LastName = dr["StudLastName"].ToString();
                        atts.Stud_MidName = dr["StudMiddName"].ToString();
                        atts.Stud_Full_Name = atts.Stud_FirstName + ' ' + atts.Stud_MidName + ' ' + atts.Stud_LastName;
                        atts.Stud_SectionCode = data.SectionCode;
                        lst.Add(atts);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("UpdateSubAttrStudents")]
        public HttpResponseMessage UpdateSubAttrStudents(AttributeStudent data, string opr)// M
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_subject_attribute_student_proc",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", opr),
                new SqlParameter("@sims_grade_code", data.Stud_GradeCode),
                new SqlParameter("@sims_section_code", data.Stud_SectionCode),
                new SqlParameter("@sims_subject_attribute_group_code ", data.Group_No),
                new SqlParameter("@sims_subject_attribute_code", data.AttribNo),
                new SqlParameter("@sims_gb_number", data.Attr_gb_number),
                new SqlParameter("@sims_gb_cat_number", data.Attr_cat_code),
                new SqlParameter("@sims_gb_cat_assign_number", data.Attr_cat_Assign_code),
                new SqlParameter("@Score", data.Attr_Mark),
                new SqlParameter("@FinalGrade", data.Attr_final_grade),
                new SqlParameter("@Comment", data.Attr_Comment),
                new SqlParameter("@status", data.Attr_Status),
                new SqlParameter("@CompletedDate", data.CompletedDate),
                new SqlParameter("@EnrollList", data.Stud_Enroll)
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region Property
        [Route("GetGbProps")]
        public HttpResponseMessage GetGbProps()
        {
            List<GBRootProp> lst = new List<GBRootProp>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_PROC",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "GP")
                         });
                    while (dr.Read())
                    {
                        GBRootProp gb = new GBRootProp();
                        gb.propName = dr["Property_Code"].ToString();
                        gb.propName_D = dr["Property"].ToString();
                        gb.db_prop = dr["DB_Prop"].ToString();
                        gb.RelationName = dr["DB_Rel"].ToString();
                        gb.PropertyName = gb.db_prop;
                        lst.Add(gb);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("MassupdateGbListProps")]
        public HttpResponseMessage MassupdateGbListProps(GBRoot gbr)
        {
            List<GBRoot> lst = new List<GBRoot>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_PROC",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "PR"),
                new SqlParameter("@IOPR", gbr.OPR),
                new SqlParameter("@ACADEMIC_YEAR", gbr.academic_year),
                new SqlParameter("@GRADE_CODE", gbr.grade_code),
                new SqlParameter("@SECTION_CODE", gbr.section_code),
                new SqlParameter("@GB_TERM_CODE", gbr.term_code)
                });
                    while (dr.Read())
                    {
                        GBRoot gb = new GBRoot();
                        gb.academic_year = dr["sims_academic_year"].ToString();
                        gb.curr_code = dr["sims_cur_code"].ToString();
                        gb.grade_code = dr["sims_grade_code"].ToString();
                        gb.section_code = dr["sims_section_code"].ToString();
                        gb.sims_gb_name = dr["sims_gb_name"].ToString();
                        gb.sims_gb_number = dr["sims_gb_number"].ToString();
                        gb.term_code = dr["sims_gb_term_code"].ToString();
                        string sts = dr["sims_gb_status"].ToString();
                        gb.Status = (sts == "A" || sts == "1" || sts == "T" || sts == "t" || sts == "a" || sts == "Y" || sts == "y") ? "Black" : "WhiteSmoke";
                        try
                        {
                            string str = dr[gbr.PropertyName].ToString();
                            gb.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                        }
                        catch (Exception)
                        {
                        }
                        lst.Add(gb);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        GB_Category cat = new GB_Category();
                        cat.Category_code = dr["sims_gb_cat_code"].ToString();
                        cat.Category_name = dr["sims_gb_cat_name"].ToString();
                        cat.sims_gb_number = dr["sims_gb_number"].ToString();
                        string sts = dr["sims_gb_cat_status"].ToString();
                        cat.Status = (sts == "A" || sts == "1" || sts == "T" || sts == "t" || sts == "a" || sts == "Y" || sts == "y") ? "Black" : "WhiteSmoke";
                        try
                        {
                            string str = dr[gbr.PropertyName].ToString();
                            cat.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                        }
                        catch (Exception)
                        {
                        }

                        try
                        {
                            GBRoot gb = lst.Single(q => q.sims_gb_number == cat.sims_gb_number);
                            if (gb.Categories == null) gb.Categories = new List<GB_Category>();
                            gb.Categories.Add(cat);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        CatAssignment assign = new CatAssignment();
                        assign.Assignment_name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_number = dr["sims_gb_cat_assign_number"].ToString();
                        assign.sims_gb_cat_number = dr["sims_gb_cat_code"].ToString();
                        assign.sims_gb_number = dr["sims_gb_number"].ToString();
                        try
                        {
                            string str = dr[gbr.PropertyName].ToString();
                            assign.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            GBRoot gbro = lst.Single(q => q.sims_gb_number == assign.sims_gb_number);
                            GB_Category cat = gbro.Categories.Single(q => q.Category_code == assign.sims_gb_cat_number);
                            if (cat.AssignMents == null) cat.AssignMents = new List<CatAssignment>();
                            cat.AssignMents.Add(assign);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            try
            {
                foreach (var GB in lst)
                {
                    if (GB.Categories != null)
                    {
                        foreach (var CAT in GB.Categories)
                        {
                            if (CAT.AssignMents != null)
                            {
                                int cnt = CAT.AssignMents.Count;
                                int checkedCount = CAT.AssignMents.Where(q => q.isSelected).Count();
                                CAT.isSelected = (cnt == checkedCount);
                            }
                        }
                        int GBcnt = GB.Categories.Count;
                        int GBcheckedCount = GB.Categories.Where(q => q.isSelected).Count();
                        GB.isSelected = (GBcnt == GBcheckedCount);
                    }
                }
            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("SaveMassupdateGbListProps")]
        public HttpResponseMessage SaveMassupdateGbListProps(List<SaveChart> lst)
        {
            string res = "";
            string val = "";
            int totAffected = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var item in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_PROC",
                            new List<SqlParameter>()
                             {
                                 new SqlParameter("@OPR", "SA"),
                                 new SqlParameter("@REL_NAME", item.RelationName),
                                 new SqlParameter("@FIELD_NAME", item.PropertyName),
                                 new SqlParameter("@GB_STATUS", item.Value),
                                 new SqlParameter("@CRITERIA", item.CriteriaValue),
                                 new SqlParameter("@ASSIGN_LIST", item.Level.Equals("1") ? item.GB_number : item.Level.Equals("2") ? item.GB_number + item.GB_Cat_number : item.GB_number + item.GB_Cat_number + item.GB_Cat_AssignNumber),
                             });
                        totAffected += 1;
                        dr.Close();
                    }
                    res = totAffected + "";
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }
        #endregion

        #region Sims527

        //[Route("")]
        //public List<Sims527> Sims526_get_academic_yr_code(string cur)
        //{
        //    List<Sims527> list = new List<Sims527>();

        //    try
        //    {
        //      
        //            
        //        SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_subject_agenda", con);
        //      
        //       new SqlParameter("@opr", "R");
        //       new SqlParameter("@sims_cur_code", cur);

        //       });
        //        while (dr.Read())
        //        {
        //            Sims527 obj = new Sims527();
        //            obj.sims_acaedmic_year = dr["sims_academic_year"].ToString();
        //            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
        //            list.Add(obj);
        //        }
        //        
        //        return list;

        //    }
        //    catch (Exception)
        //    {
        //        
        //        return list;
        //    }

        //}

        [Route("Sims527_get_teacher_code")]
        public HttpResponseMessage Sims527_get_teacher_code(string emp_id)
        {
            string teacher_id = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "B"),
                new SqlParameter("@sims_login_code", emp_id)
                         });
                    while (dr.Read())
                    {
                        teacher_id = dr["sims_teacher_code"].ToString();
                    }
                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, teacher_id);
        }

        [Route("Sims527_get_grade_code")]
        public HttpResponseMessage Sims527_get_grade_code(string teacher, bool flag, string academic, string cur)
        {
            List<Sims527> list = new List<Sims527>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C"),
                new SqlParameter("@st", flag==true?"0":"1"),
                new SqlParameter("@sims_login_code", teacher),
                new SqlParameter("@sims_acaedmic_year", academic),
                new SqlParameter("@sims_cur_code", cur)
                         });
                    while (dr.Read())
                    {
                        Sims527 obj = new Sims527();
                        obj.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                        obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Sims527_get_section_code")]
        public HttpResponseMessage Sims527_get_section_code(string teacher, string grade, bool flag, string academic, string cur)
        {

            List<Sims527> list = new List<Sims527>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "F"),
                new SqlParameter("@st", flag==true?"0":"1"),
                new SqlParameter("@sims_login_code", teacher),
                new SqlParameter("@sims_acaedmic_year", academic),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_grade_code", grade)
                         });
                    while (dr.Read())
                    {
                        Sims527 obj = new Sims527();
                        obj.sims_section_code = dr["sims_bell_section_code"].ToString();
                        obj.sims_section_name = dr["sims_section_name_en"].ToString();
                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("Sims527_get_subject_code")]
        public HttpResponseMessage Sims527_get_subject_code(string teacher, string grade, string section, bool flag, string cur, string ay)
        {
            List<Sims527> list = new List<Sims527>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "G"),
                new SqlParameter("@st", flag==true?"0":"1"),
                new SqlParameter("@sims_login_code", teacher),
                new SqlParameter("@sims_acaedmic_year", ay),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", section)
                         });
                    while (dr.Read())
                    {
                        Sims527 obj = new Sims527();
                        obj.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                        obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Sims527_get_Agenda_details")]
        public HttpResponseMessage Sims527_get_Agenda_details(Sims527 item, string start, string end)
        {
            List<Sims527> lst = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_cur_code", item.sims_cur_code),
                new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                new SqlParameter("@sims_grade_code", item.sims_grade_code),
                new SqlParameter("@sims_section_code", item.sims_section_code),
                new SqlParameter("@sims_subject_code", item.sims_subject_code),
                new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                new SqlParameter("@sdate", start),
                new SqlParameter("@edate", end)
                         });
                    while (dr.Read())
                    {
                        Sims527 obj = new Sims527();
                        obj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        obj.sims_agenda_date = DateTime.Parse(dr["sims_agenda_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_agenda_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_agenda_date"].ToString()).Day;
                        obj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        obj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                        obj.file = dr["sims_agenda_doc"].ToString();
                        if (dr["sims_agenda_visible_to_parent_portal"].ToString() == "Y")
                            obj.sims_agenda_visible_to_parent_portal = true;
                        else
                            obj.sims_agenda_visible_to_parent_portal = false;
                        if (dr["sims_agenda_status"].ToString() == "Y")
                            obj.sims_agenda_status = true;
                        else
                            obj.sims_agenda_status = false;

                        lst.Add(obj);
                    }
                }
            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("Sims527_get_holidays")]
        public HttpResponseMessage Sims527_get_holidays(string academic, string start, string end)
        {
            List<Sims527> lst = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "T"),
                new SqlParameter("@sims_acaedmic_year", academic),
                new SqlParameter("@sdate", start),
                new SqlParameter("@edate", end)
                         });
                    while (dr.Read())
                    {
                        Sims527 date = new Sims527();
                        date.sims_agenda_date = dr["sims_from_date"].ToString();
                        date.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();

                        lst.Add(date);
                    }
                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("Sims527_Insert_Agenda_details")]
        public HttpResponseMessage Sims527_Insert_Agenda_details(List<Sims527> obj)
        {
            bool Inserted = false;

            try
            {
                foreach (Sims527 item in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_agenda",
                            new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", 'V'),
                    new SqlParameter("@sims_cur_code", item.sims_cur_code),
                    new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                    new SqlParameter("@sims_grade_code", item.sims_grade_code),
                    new SqlParameter("@sims_section_code", item.sims_section_code),
                    new SqlParameter("@sims_subject_code", item.sims_subject_code),
                    new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                    new SqlParameter("@sims_agenda_date", item.sims_agenda_date)
                         });
                        dr.Close();
                        if (dr.HasRows)
                        {
                            int dr1 = db.ExecuteStoreProcedureforInsert("sims_section_subject_agenda",
                              new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", 'U'),
                        new SqlParameter("@sims_cur_code", item.sims_cur_code),
                        new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                        new SqlParameter("@sims_grade_code", item.sims_grade_code),
                        new SqlParameter("@sims_section_code", item.sims_section_code),
                        new SqlParameter("@sims_subject_code", item.sims_subject_code),
                        new SqlParameter("@sims_agenda_number", item.sims_agenda_number),
                        new SqlParameter("@sims_agenda_date", item.sims_agenda_date),
                        new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                        new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                        new SqlParameter("@sims_agenda_doc", item.file),
                        new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                         new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal==true?"N":"Y")
                         });
                            if (dr1 > 0)
                            {
                                Inserted = true;
                            }
                        }
                        else
                        {
                            int dr2 = db.ExecuteStoreProcedureforInsert("sims_section_subject_agenda",
                               new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", 'I'),
                        new SqlParameter("@sims_cur_code", item.sims_cur_code),
                        new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                        new SqlParameter("@sims_grade_code", item.sims_grade_code),
                        new SqlParameter("@sims_section_code", item.sims_section_code),
                        new SqlParameter("@sims_subject_code", item.sims_subject_code),
                        new SqlParameter("@sims_agenda_date", item.sims_agenda_date),
                        new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                        new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                        new SqlParameter("@sims_agenda_doc", item.file),
                        new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                       new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal==true?"Y":"N"),
                         });
                            if (dr2 > 0)
                            {
                                Inserted = true;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }


        [Route("Delete_agenda")]
        public HttpResponseMessage Delete_agenda(Sims527 obj)
        {
            bool Deleted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_subject_agenda",
                      new List<SqlParameter>()
                         {
                new SqlParameter("@opr", 'D'),
                new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                new SqlParameter("@sims_section_code", obj.sims_section_code),
                new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                new SqlParameter("@sims_agenda_number", obj.sims_agenda_number),
                new SqlParameter("@sims_agenda_date", obj.sims_agenda_date)
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        Deleted = true;
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, Deleted);

        }

        #endregion

        #region report card attribute group(Sims141)

        [Route("GenerateResult")]
        public HttpResponseMessage GenerateResult(SIMSAPI.Models.ERP.gradebookClass.Assignment assignment)
        {
            bool str = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.generateResult",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@cur_code", assignment.Assignment_cur_code),
               new SqlParameter("@acad_year", assignment.Assignment_academic_year),
               new SqlParameter("@grade_code", assignment.Assignment_grade_code),
               new SqlParameter("@section_code", assignment.Assignment_section_code),
               new SqlParameter("@search_student", assignment.Assignment_student_name),
               new SqlParameter("@term_code", assignment.teacher_code)
                });
                    if (dr.RecordsAffected > 0)
                    {
                        str = true;
                    }
                }
            }

            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, str);
        }

        [Route("GetReportCardGroupType")]
        public HttpResponseMessage GetReportCardGroupType()
        {

            List<Sims141> list = new List<Sims141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "P"),
               });
                    while (dr.Read())
                    {
                        Sims141 obj = new Sims141();
                        obj.sims_attribute_group_type = dr["sims_appl_parameter"].ToString();
                        obj.sims_attribute_group_type_en = dr["sims_appl_form_field_value1"].ToString();
                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetReportCardAttributeGroup")]
        public HttpResponseMessage GetReportCardAttributeGroup()
        {
            List<Sims141> list = new List<Sims141>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_attribute_group",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "S")
               });
                    while (dr.Read())
                    {
                        Sims141 simsobj = new Sims141();
                        simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                        simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        simsobj.sims_cur_name = dr["sims_cur_full_name_en"].ToString();
                        simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        simsobj.sims_section_code = dr["sims_section_code"].ToString();
                        simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                        simsobj.sims_attribute_group_code = dr["sims_attribute_group_code"].ToString();
                        simsobj.sims_attribute_group_name = dr["sims_attribute_group_name"].ToString();
                        simsobj.sims_attribute_group_name_ot = dr["sims_attribute_group_name_ot"].ToString();
                        simsobj.sims_attribute_group_type = dr["sims_attribute_group_type"].ToString();
                        simsobj.sims_attribute_group_type_en = dr["sims_appl_form_field"].ToString();
                        simsobj.sims_attribute_group_display_order = dr["sims_attribute_group_display_order"].ToString();
                        if (dr["sims_attribute_group_status"].ToString() == "A")
                            simsobj.sims_attribute_group_status = true;
                        else
                            simsobj.sims_attribute_group_status = false;
                        list.Add(simsobj);
                    }

                }
            }
            catch (Exception e)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("InsertReportCardAttributeGroup")]
        public HttpResponseMessage InsertReportCardAttributeGroup(Sims141 obj, List<string> section)
        {
            bool Inserted = false;
            try
            {

                foreach (string s in section)
                {
                    string[] temp = s.Split('#');
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_attribute_group",
                            new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", 'C'),
                   new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                   new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                   new SqlParameter("@sims_grade_code", temp[0]),
                   new SqlParameter("@sims_section_code", temp[1]),
                   new SqlParameter("@sims_attribute_group_type", obj.sims_attribute_group_type)
                   });
                        if (dr.HasRows)
                        {
                            continue;
                        }
                        else
                        {
                            SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sims_report_card_attribute_group",
                                 new List<SqlParameter>()
                         {
                       new SqlParameter("@opr", 'I'),
                       new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                       new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                       new SqlParameter("@sims_grade_code", temp[0]),
                       new SqlParameter("@sims_section_code", temp[1]),
                       new SqlParameter("@sims_attribute_group_name", obj.sims_attribute_group_name),
                       new SqlParameter("@sims_attribute_group_name_ot", obj.sims_attribute_group_name_ot),
                       new SqlParameter("@sims_attribute_group_type", obj.sims_attribute_group_type),
                       new SqlParameter("@sims_attribute_group_display_order", obj.sims_attribute_group_display_order),
                       new SqlParameter("@sims_attribute_group_status", obj.sims_attribute_group_status==true?"A":"I")
                    });
                            if (dr2.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }

                        }
                    }
                }
            }
            catch
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("UpdateReportCardAttributeGroup")]
        public HttpResponseMessage UpdateReportCardAttributeGroup(Sims141 simsobj)
        {
            bool updated = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_report_card_attribute_group",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "U"),
               new SqlParameter("@sims_attribute_group_code", simsobj.sims_attribute_group_code),
               new SqlParameter("@sims_attribute_group_name", simsobj.sims_attribute_group_name),
               new SqlParameter("@sims_attribute_group_name_ot", simsobj.sims_attribute_group_name_ot),
               new SqlParameter("@sims_attribute_group_display_order", simsobj.sims_attribute_group_display_order),
                    new SqlParameter("@sims_attribute_group_status",simsobj.sims_attribute_group_status == true?"A":"I")
                    });
                    if (ins > 0)
                    {
                        updated = true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("DeleteReportCardAttributeGroup")]
        public HttpResponseMessage DeleteReportCardAttributeGroup(Sims141 simsobj)
        {
            bool updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_report_card_attribute_group",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "D"),
               new SqlParameter("@sims_attribute_group_code", simsobj.sims_attribute_group_code)
            });
                    if (ins > 0)
                    {
                        updated = true;
                    }

                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        #endregion

        #region Report Card Allocation (Sims506)

        [Route("Sims506_Get_report_card_code")]
        public HttpResponseMessage Sims506_Get_report_card_code(string cur_code, string acadmic_yr, string grade_code, string section, string term)
        {
            List<Sims506> list = new List<Sims506>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "R"),
               new SqlParameter("@cur_code",cur_code),
               new SqlParameter("@academic_year",acadmic_yr),
               new SqlParameter("@grade_code",grade_code),
                 new SqlParameter("@section_code",section),
                    new SqlParameter("@sims_report_card_term",term)
                });
                    while (dr.Read())
                    {
                        Sims506 obj = new Sims506();
                        obj.sims_report_card_master_code = dr["sims_level_code"].ToString() + "#" + dr["sims_report_card_code"].ToString();
                        obj.sims_report_card_name = dr["level_name"].ToString() + " " + dr["sims_report_card_name"].ToString();
                        list.Add(obj);
                    }

                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        [Route("Sims506_Get_Report_card_allocation")]
        public HttpResponseMessage Sims506_Get_Report_card_allocation()
        {
            List<Sims506> list = new List<Sims506>();
            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'S')
                });
                    while (dr.Read())
                    {
                        Sims506 invsobj = new Sims506();
                        invsobj.sims_academic_year = dr["sims_report_card_academic_year"].ToString();
                        invsobj.sims_cur_code = dr["sims_report_card_cur_code"].ToString();
                        invsobj.sims_cur_name = dr["sims_report_card_cur_code"].ToString();
                        invsobj.sims_grade_code = dr["sims_report_card_grade_code"].ToString();
                        invsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        invsobj.sims_section_code = dr["sims_report_card_section_code"].ToString();
                        invsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                        invsobj.sims_report_card_level = dr["sims_report_card_level_code"].ToString();
                        invsobj.sims_report_card_level_name = dr["level"].ToString();
                        invsobj.sims_report_card_master_code = dr["sims_report_card_code"].ToString();
                        invsobj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                        invsobj.sims_gb_number = dr["sims_gb_number"].ToString();
                        invsobj.sims_gb_name = dr["sims_gb_name"].ToString();
                        invsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        invsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                        invsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        invsobj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                        invsobj.sims_report_card_serial_number = dr["sims_report_card_serial_number"].ToString();
                        invsobj.sims_report_card_allocation = dr["sims_report_card_allocation"].ToString();
                        if (dr["sims_report_card_status"].ToString() == "A")
                        {
                            invsobj.sims_report_card_status = true;
                        }
                        else
                        {
                            invsobj.sims_report_card_status = false;
                        }
                        invsobj.report_select = false;
                        list.Add(invsobj);
                    }

                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);


        }

        [Route("Sims506_Insert_Report_Card")]
        public HttpResponseMessage Sims506_Insert_Report_Card(List<string> lst, List<string> report, Sims506 obj)
        {
            bool Inserted = false;

            try
            {
                string val = "";

                foreach (string item in lst)
                {
                    foreach (string r in report)
                    {
                        string[] temp = item.Split('*');
                        string[] temp1 = temp[1].Split('#');
                        string[] abc = r.Split('#');
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                          new List<SqlParameter>()
                         {
                       new SqlParameter("@opr", 'C'),
                       new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
                       new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
                       new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
                       new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
                       new SqlParameter("@sims_report_card_level_code", abc[0]),
                       new SqlParameter("@sims_report_card_code", abc[1]),
                       new SqlParameter("@sims_gb_number", temp[0]),
                       new SqlParameter("@sims_gb_cat_code", temp1[0]),
                       new SqlParameter("@sims_gb_cat_assign_number", temp1[1])
                        });
                            if (dr.HasRows)
                            {
                                continue;
                            }
                            else
                            {
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("sims_report_card_allocation",
                              new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'I'),
                           new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
                           new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
                           new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
                           new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
                           new SqlParameter("@sims_report_card_level_code", abc[0]),
                           new SqlParameter("@sims_report_card_code", abc[1]),
                           new SqlParameter("@sims_gb_number", temp[0]),
                           new SqlParameter("@sims_gb_cat_code", temp1[0]),
                           new SqlParameter("@sims_gb_cat_assign_number", temp1[1]),
                           new SqlParameter("@sims_report_card_serial_number", obj.sims_report_card_serial_number),
                           new SqlParameter("@sims_report_card_allocation", obj.sims_report_card_allocation != ""? obj.sims_report_card_allocation:null),
                           new SqlParameter("@sims_report_card_status", obj.sims_report_card_status==true?"A":"I")
                        });
                                if (dr2.RecordsAffected > 0)
                                {
                                    Inserted = true;
                                }

                            }
                        }
                    }
                }
            }
            catch
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Sims506_Update_Report_card_allocation")]
        public HttpResponseMessage Sims506_Update_Report_card_allocation(Sims506 obj)
        {
            bool Inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                    new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'U'),
               new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
               new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
               new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
               new SqlParameter("@sims_report_card_level_code", obj.sims_report_card_level),
               new SqlParameter("@sims_report_card_code", obj.sims_report_card_master_code),
               new SqlParameter("@sims_gb_number", obj.sims_gb_number),
               new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
               new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number),
               new SqlParameter("@sims_report_card_serial_number", obj.sims_report_card_serial_number),
               new SqlParameter("@sims_report_card_allocation", obj.sims_report_card_allocation!= ""?obj.sims_report_card_allocation:null),
               new SqlParameter("@sims_report_card_status", obj.sims_report_card_status==true?"A":"I")
                     });

                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                }
            }
            catch(Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Sims506_Delete_Report_card")]
        public HttpResponseMessage Sims506_Delete_Report_card(Sims506 obj)
        {
            bool Deleted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                  new List<SqlParameter>()
                         {

               new SqlParameter("@opr", 'D'),
               new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
               new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
               new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
               new SqlParameter("@sims_report_card_level_code", obj.sims_report_card_level),
               new SqlParameter("@sims_report_card_code", obj.sims_report_card_master_code),
               new SqlParameter("@sims_gb_number", obj.sims_gb_number),
               new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
               new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number)
                });
                    if (dr.RecordsAffected > 0)
                    {
                        Deleted = true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Deleted);

        }
        #endregion

        #region Sims072 Sims report card subject attribute
        [Route("Get_acadmic_year")]
        public HttpResponseMessage Get_acadmic_year(string curcode)
        {
            List<Sim072> list = new List<Sim072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_gradbook_rollno_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "B"),
               new SqlParameter("@cur_code", curcode)
               });
                    while (dr.Read())
                    {
                        Sim072 obj = new Sim072();
                        obj.sims_academic_year = dr["sims_academic_year"].ToString();
                        obj.subject_select = false;
                        obj.group_select = false;
                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("Get_group_code")]
        public HttpResponseMessage Get_group_code(string cur_code, string acadmic_yr, string grade_code, string section)
        {
            List<Sim072> list = new List<Sim072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradbook_rollno_proc]",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "F"),
                new SqlParameter("@cur_code",cur_code),
               new SqlParameter("@academic_year",acadmic_yr),
               new SqlParameter("@grade_code",grade_code),
                 new SqlParameter("@section_code",section),
               });
                    while (dr.Read())
                    {
                        Sim072 obj = new Sim072();
                        obj.sims_subject_attribute_group_code = dr["sims_attribute_group_code"].ToString();
                        obj.group_name = dr["sims_attribute_group_name"].ToString();
                        list.Add(obj);
                    }

                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Get_All_subject_attribute")]
        public HttpResponseMessage Get_All_subject_attribute(string cur, string ay, string grade, string section)
        {
            List<Sim072> list = new List<Sim072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_subject_attribute_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'S'),
               new SqlParameter("@sims_academic_year", ay),
               new SqlParameter("@sims_cur_code", cur),
               new SqlParameter("@sims_grade_code", grade),
               new SqlParameter("@sims_section_code", section)
               });
                    while (dr.Read())
                    {
                        Sim072 invsobj = new Sim072();
                        invsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        invsobj.cur_name = dr["cur_name"].ToString();
                        invsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                        invsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        invsobj.grade_name = dr["grade_name"].ToString();
                        invsobj.sims_section_code = dr["sims_section_code"].ToString();
                        invsobj.section_name = dr["section_name"].ToString();
                        invsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        invsobj.subject_name = dr["subject_name"].ToString();
                        invsobj.sims_subject_attribute_name_ot = dr["sims_subject_attribute_name_ot"].ToString();
                        invsobj.sims_subject_attribute_group_code = dr["sims_subject_attribute_group_code"].ToString();
                        invsobj.group_name = dr["group_name"].ToString();
                        invsobj.sims_subject_attribute_code = dr["sims_subject_attribute_code"].ToString();
                        invsobj.sims_subject_attribute_name = dr["sims_subject_attribute_name"].ToString();
                        invsobj.sims_subject_attribute_display_order = dr["sims_subject_attribute_display_order"].ToString();
                        invsobj.sims_subject_attribute_status = dr["sims_subject_attribute_status"].Equals("A") ? true : false;
                        list.Add(invsobj);
                    }

                }
            }
            catch
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        //[Route("")]
        //public List<Sim072> Get_subject_chk(Sim072 obj)
        //{

        //    List<Sim072> list = new List<Sim072>();
        //    try
        //    {
        //      
        //            
        //        SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_subject_attribute", con);
        //      
        //       new SqlParameter("@opr", 'C');
        //       new SqlParameter("@sims_cur_code", obj.sims_cur_code);
        //       new SqlParameter("@sims_section_code", obj.sims_section_code);
        //       new SqlParameter("@sims_grade_code", obj.sims_grade_code);
        //       new SqlParameter("@sims_academic_year", obj.sims_academic_year);
        //       new SqlParameter("@sims_subject_code", obj.sims_subject_code);
        //       new SqlParameter("@sims_subject_attribute_group_code", obj.sims_subject_attribute_group_code);

        //       });
        //        while (dr.Read())
        //        {
        //            Sim072 invsobj = new Sim072();
        //            invsobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //            invsobj.cur_name = dr["cur_name"].ToString();
        //            invsobj.sims_academic_year = dr["sims_academic_year"].ToString();
        //            invsobj.sims_grade_code = dr["sims_grade_code"].ToString();
        //            invsobj.grade_name = dr["grade_name"].ToString();
        //            invsobj.sims_section_code = dr["sims_section_code"].ToString();
        //            invsobj.section_name = dr["section_name"].ToString();
        //            invsobj.sims_subject_code = dr["sims_subject_code"].ToString();
        //            invsobj.subject_name = dr["subject_name"].ToString();
        //            invsobj.sims_subject_attribute_group_code = dr["sims_subject_attribute_group_code"].ToString();
        //            invsobj.group_name = dr["group_name"].ToString();
        //            invsobj.sims_subject_attribute_code = dr["sims_subject_attribute_code"].ToString();
        //            invsobj.sims_subject_attribute_name = dr["sims_subject_attribute_name"].ToString();
        //            invsobj.sims_subject_attribute_display_order = dr["sims_subject_attribute_display_order"].ToString();
        //            if (dr["sims_subject_attribute_status"].ToString() == "A")
        //            {
        //                invsobj.sims_subject_attribute_status = true;
        //            }
        //            else
        //            {
        //                invsobj.sims_subject_attribute_status = false;
        //            }

        //            list.Add(invsobj);
        //        }
        //        
        //        return list;
        //    }
        //    catch
        //    {
        //        
        //        return list;
        //    }

        //}

        [Route("Subject_attribute")]
        public HttpResponseMessage Subject_attribute(string data, ArrayList data_param)
        {
            bool Inserted = false;

            List<Sim072> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Sim072>>(data_param[0].ToString());
            List<Sim072> sub = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Sim072>>(data_param[1].ToString());

            Sim072 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sim072>(data);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim072 g in grp)
                    {
                        foreach (Sim072 s in sub)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_subject_attribute_proc]",
                           new List<SqlParameter>()
                         {
                       new SqlParameter("@opr", 'C'),
                       new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                       new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                       new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                       new SqlParameter("@sims_section_code", obj.sims_section_code),
                       new SqlParameter("@sims_subject_attribute_group_code", g.subject_name),
                       new SqlParameter("@sims_subject_code",  s.subject_name)
                       });
                            if (dr.HasRows)
                            {
                                continue;
                            }
                            dr.Close();

                            SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[sims_report_card_subject_attribute_proc]",
                                 new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'I'),
                           new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                           new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                           new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                           new SqlParameter("@sims_section_code", obj.sims_section_code),
                           new SqlParameter("@sims_subject_attribute_group_code", g.subject_name),
                           new SqlParameter("@sims_subject_code", s.subject_name),
                           new SqlParameter("@sims_subject_attribute_name", obj.sims_subject_attribute_name),
                           new SqlParameter("@sims_subject_attribute_name_ot", obj.sims_subject_attribute_name_ot),
                           new SqlParameter("@sims_subject_attribute_display_order", obj.sims_subject_attribute_display_order),
                                new SqlParameter("@sims_subject_attribute_status", obj.sims_subject_attribute_status==true?"A":"I")
                         });
                            if (dr2.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }
                            dr2.Close();
                        }
                    }
                }
            }
            catch(Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }


        //[Route("")]
        //public bool Update_Subject_attribute(Sim072 obj)
        //{

        //    bool Inserted = false;

        //    try
        //    {
        //        
        //      
        //            
        //        SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_subject_attribute", con);
        //      
        //       
        //       new SqlParameter("@opr", 'F');
        //       new SqlParameter("@sims_cur_code", obj.sims_cur_code);
        //       new SqlParameter("@sims_academic_year", obj.sims_academic_year);
        //       new SqlParameter("@sims_grade_code", obj.sims_grade_code);
        //       new SqlParameter("@sims_section_code", obj.sims_section_code);
        //       new SqlParameter("@sims_subject_attribute_group_code", obj.sims_subject_attribute_group_code);
        //       new SqlParameter("@sims_subject_code", obj.sims_subject_code);

        //       });
        //        if (dr.HasRows)
        //        {
        //            Inserted = false;
        //        }
        //        else
        //        {
        //            
        //          
        //                
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_subject_attribute", con);
        //          
        //           
        //           new SqlParameter("@opr", 'U');
        //           new SqlParameter("@sims_subject_attribute_code", obj.sims_subject_attribute_code);
        //           new SqlParameter("@sims_subject_code", obj.sims_subject_code);
        //           new SqlParameter("@sims_subject_attribute_name", obj.sims_subject_attribute_name);
        //           new SqlParameter("@sims_subject_attribute_display_order", obj.sims_subject_attribute_display_order);
        //            if (obj.sims_subject_attribute_status)
        //               new SqlParameter("@sims_subject_attribute_status", "A");
        //            else
        //               new SqlParameter("@sims_subject_attribute_status", "I");



        //            int entry = cmd.ExecuteNonQuery();
        //            if (entry > 0)
        //            {
        //                Inserted = true;
        //            }

        //        }
        //        
        //        return Inserted;
        //    }
        //    catch
        //    {
        //        
        //        return Inserted;
        //    }
        //}

        [Route("Update_Subject_attribute")]
        public HttpResponseMessage Update_Subject_attribute(Sim072 obj)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_subject_attribute_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'U'),
               new SqlParameter("@sims_cur_code", obj.sims_cur_code),
               new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_section_code", obj.sims_section_code),
               new SqlParameter("@sims_subject_code", obj.sims_subject_code),
               new SqlParameter("@sims_subject_attribute_group_code", obj.sims_subject_attribute_group_code),
               new SqlParameter("@sims_subject_attribute_code", obj.sims_subject_attribute_code),
               new SqlParameter("@sims_subject_attribute_name", obj.sims_subject_attribute_name),
               new SqlParameter("@sims_subject_attribute_display_order", obj.sims_subject_attribute_display_order),
               new SqlParameter("@sims_subject_attribute_status", obj.sims_subject_attribute_status==true?"A":"I"),
               new SqlParameter("@sims_subject_attribute_name_ot", obj.sims_subject_attribute_name_ot)

              });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }

                }

            }
            catch(Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Delete_Subject_attribute")]
        public HttpResponseMessage Delete_Subject_attribute(Sim072 obj)
        {
            bool Deleted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_subject_attribute",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'D'),
               new SqlParameter("@sims_academic_year", obj.sims_academic_year),
               new SqlParameter("@sims_cur_code", obj.sims_cur_code),
               new SqlParameter("@sims_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_section_code", obj.sims_section_code),
               new SqlParameter("@sims_subject_code", obj.sims_subject_code),
               new SqlParameter("@sims_subject_attribute_group_code", obj.sims_subject_attribute_group_code),
               new SqlParameter("@sims_subject_attribute_code", obj.sims_subject_attribute_code)
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        Deleted = true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Deleted);

        }
        #endregion

        #region Upload Assignment Teacher (Sims508)

        [Route("Sims508_get_teacher_code")]
        public HttpResponseMessage Sims508_get_teacher_code(string emp_id)
        {
            string teacher_id = "";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_gradbook_rollno_proc]",
                         new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "C"),
               new SqlParameter("@sims_employee_code", emp_id )
               });
                    while (dr.Read())
                    {
                        teacher_id = dr["sims_teacher_code"].ToString() + "#" + dr["sims_academic_year"].ToString() + "*" + dr["sims_cur_code"].ToString();
                    }
                }
            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, teacher_id);

        }

        [Route("Sims508_get_grade_code")]
        public HttpResponseMessage Sims508_get_grade_code(string teacher, bool flag)
        {
            List<Sims508> list = new List<Sims508>();
            if (flag)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_bell_section_subject_teacher_proc]",
                      new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", "G"),
                   new SqlParameter("@sims_old_teacher_code",teacher)

                   });
                        while (dr.Read())
                        {
                            Sims508 obj = new Sims508();
                            obj.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {
                }

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("Sims508_get_section_code")]
        public HttpResponseMessage Sims508_get_section_code(string teacher, string grade, bool flag)
        {
            List<Sims508> list = new List<Sims508>();
            if (flag)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_bell_section_subject_teacher_proc]",
                       new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", "Z"),
                   new SqlParameter("@sims_old_teacher_code", teacher)
                   });
                        while (dr.Read())
                        {
                            Sims508 obj = new Sims508();
                            obj.sims_section_code = dr["sims_bell_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            list.Add(obj);
                        }

                    }

                }
                catch (Exception)
                {
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_get_students")]
        public HttpResponseMessage Sims508_get_students(string grade, string section)
        {
            List<Sims508> list = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims.sims_gradbook_rollno_proc]",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "D"),
               new SqlParameter("@grade_code",grade),
               new SqlParameter("@section_code", section)
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.stud_code = dr["stud_code"].ToString();
                        obj.stud_name = dr["stud_name"].ToString();
                        list.Add(obj);
                    }

                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_get_all_details")]
        public HttpResponseMessage Sims508_get_all_details()
        {
            List<Sims508> list = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "S")
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.stud_name = dr["sims_academic_year"].ToString();
                        obj.sims_cur_code = dr["sims_cur_code"].ToString();
                        obj.sims_grade_code = dr["sims_grade_code"].ToString();
                        obj.sims_section_code = dr["sims_section_code"].ToString();
                        obj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                        obj.sims_gb_number = dr["sims_gb_number"].ToString();
                        obj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        obj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        obj.sims_assignment_teacher_code = dr["sims_assignment_teacher_code"].ToString();
                        obj.sims_subject_code = dr["sims_assignment_subject_code"].ToString();
                        obj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                        obj.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                        obj.sims_assignment_start_date = DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                            obj.sims_assignment_submission_date = DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                            obj.sims_assignment_freeze_date = DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Day;
                        if (dr["sims_assignment_status"].ToString() == "A")
                            obj.sims_assignment_status = true;
                        else
                            obj.sims_assignment_status = false;

                        list.Add(obj);
                    }

                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_get_history_details")]
        public HttpResponseMessage Sims508_get_history_details(string teacher)
        {
            List<Sims508> list = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "A")
            });

                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_grade_code = dr["sims_grade_code"].ToString();
                        obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                        obj.sims_section_code = dr["sims_section_code"].ToString();
                        obj.sims_section_name = dr["sims_section_name_en"].ToString();
                        obj.sims_subject_code = dr["sims_assignment_subject_code"].ToString();
                        obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        obj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                        obj.sims_assignment_start_date = DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                            obj.sims_assignment_submission_date = DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                            obj.sims_assignment_freeze_date = DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Day;
                        if (dr["sims_assignment_status"].ToString() == "A")
                            obj.sims_assignment_status = true;
                        else
                            obj.sims_assignment_status = false;

                        list.Add(obj);
                    }
                }

            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_Insert_Assignment_details")]
        public HttpResponseMessage Sims508_Insert_Assignment_details(Sims508 obj, string[] doc)
        {
            bool Inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'I'),
               new SqlParameter("@sims_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_section_code", obj.sims_section_code),
               new SqlParameter("@sims_gb_number", obj.sims_gb_number),
               new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
               new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number),
               new SqlParameter("@sims_assignment_teacher_code", obj.sims_assignment_teacher_code),
               new SqlParameter("@sims_assignment_subject_code", obj.sims_subject_code),
               new SqlParameter("@sims_gb_term_code", obj.sims_term_code),
               new SqlParameter("@sims_assignment_title", obj.sims_assignment_title),
               new SqlParameter("@sims_assignment_desc", obj.sims_assignment_desc),
               new SqlParameter("@sims_assignment_start_date", DateTime.Parse(obj.sims_assignment_start_date).Year + "-" + DateTime.Parse(obj.sims_assignment_start_date).Month + "-" + DateTime.Parse(obj.sims_assignment_start_date).Day),
               new SqlParameter("@sims_assignment_submission_date", DateTime.Parse(obj.sims_assignment_submission_date).Year + "-" + DateTime.Parse(obj.sims_assignment_submission_date).Month + "-" + DateTime.Parse(obj.sims_assignment_submission_date).Day),
               new SqlParameter("@sims_assignment_freeze_date", DateTime.Parse(obj.sims_assignment_freeze_date).Year + "-" + DateTime.Parse(obj.sims_assignment_freeze_date).Month + "-" + DateTime.Parse(obj.sims_assignment_freeze_date).Day),
               new SqlParameter("@sims_assignment_status", obj.sims_assignment_status==true?"A":"I")

                });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();
                    int i = 0;

                    if (doc.Count() != 0)
                    {
                        for (int t = 0; t < doc.Count(); t++)
                        {
                            SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                           new List<SqlParameter>()
                         {
                       new SqlParameter("@opr", 'A'),
                       new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                       new SqlParameter("@sims_section_code", obj.sims_section_code),
                       new SqlParameter("@sims_assignment_doc_line_no", (t).ToString()),
                       new SqlParameter("@sims_assignment_doc_path", doc[t]),
                       new SqlParameter("@sims_assignment_doc_status",obj.sims_assignment_status==true?"A":"I")
                    });
                            if (dr2.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }
                        }
                    }

                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Sims508_chk_user_details")]
        public HttpResponseMessage Sims508_chk_user_details(string user)
        {
            bool Inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'V'),
               new SqlParameter("@sims_assignment_teacher_code", user)

               });
                    if (dr.HasRows)
                    {
                        Inserted = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Sims508_get_ass_details")]
        public HttpResponseMessage Sims508_get_ass_details(string grade, string section, string subject, string term)
        {
            List<Sims508> lst = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'T'),
               new SqlParameter("@sims_assignment_subject_code", subject),
               new SqlParameter("@sims_gb_term_code", term),
               new SqlParameter("@sims_grade_code", grade),
               new SqlParameter("@sims_section_code", section)
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        obj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                        obj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        obj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                        obj.sims_gb_number = dr["sims_gb_number"].ToString();
                        obj.sims_gb_name = dr["sims_gb_name"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_gb_cat_assign_date"].ToString()))
                            obj.sims_gb_cat_assign_date = DateTime.Parse(dr["sims_gb_cat_assign_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_gb_cat_assign_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_gb_cat_assign_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_gb_cat_assign_due_date"].ToString()))
                            obj.sims_gb_cat_assign_date = DateTime.Parse(dr["sims_gb_cat_assign_due_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_gb_cat_assign_due_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_gb_cat_assign_due_date"].ToString()).Day;
                        lst.Add(obj);
                    }

                }

            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("Sims508_Insert_student_details")]
        public HttpResponseMessage Sims508_Insert_student_details(Sims508 obj, List<string> stud_info)
        {
            bool Inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (string item in stud_info)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                       new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", 'B'),
                   new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                   new SqlParameter("@sims_section_code", obj.sims_section_code),
                   new SqlParameter("@sims_assignment_enroll_number", item)
                         });


                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                    }
                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("Sims508_get_Assignment_details")]
        public HttpResponseMessage Sims508_get_Assignment_details(string ass_no)
        {
            List<Sims508> list = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'P'),
               new SqlParameter("@sims_assignment_number", ass_no)
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_academic_year = dr["sims_academic_year"].ToString();
                        obj.sims_cur_code = dr["sims_cur_code"].ToString();
                        obj.sims_grade_code = dr["sims_grade_code"].ToString();
                        obj.sims_section_code = dr["sims_section_code"].ToString();
                        obj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                        obj.sims_gb_number = dr["sims_gb_number"].ToString();
                        obj.sims_gb_name = dr["sims_gb_name"].ToString();
                        obj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        obj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                        obj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        obj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                        obj.sims_assignment_teacher_code = dr["sims_assignment_teacher_code"].ToString();
                        obj.sims_subject_code = dr["sims_assignment_subject_code"].ToString();
                        obj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                        obj.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                        obj.sims_assignment_start_date = DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                            obj.sims_assignment_submission_date = DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                            obj.sims_assignment_freeze_date = DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Day;

                        obj.sims_term_code = dr["sims_assignment_term_code"].ToString();
                        obj.sims_assignment_status = (dr["sims_assignment_status"].ToString() == "A" ? true : false);

                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_get_Assignment_Documnet_details")]
        public HttpResponseMessage Sims508_get_Assignment_Documnet_details(string ass_no)
        {
            List<Sims508> list = new List<Sims508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "P"),
               new SqlParameter("@sims_assignment_number", "='" + ass_no + "'")
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_assignment_doc_path = dr["sims_assignment_doc_path"].ToString();
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("Sims508_get_Assignment_student_details")]
        public HttpResponseMessage Sims508_get_Assignment_student_details(string ass_no, string grade, string section)
        {
            List<Sims508> list = new List<Sims508>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "N"),
               new SqlParameter("@sims_grade_code", grade),
               new SqlParameter("@sims_section_code", section)
               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.stud_code = dr["sims_enroll_number"].ToString();
                        obj.stud_name = dr["Stud_name"].ToString();
                        if (dr["sims_assignment_status"].ToString() == "V")
                        {
                            obj.stud_status = "Veryfiy";
                            obj.status = true;
                        }
                        else if (dr["sims_assignment_status"].ToString() == "S")
                        {
                            obj.stud_status = "Submitted";
                            obj.status = true;
                        }
                        else if (dr["sims_assignment_status"].ToString() == "P")
                        {
                            obj.stud_status = "Pending";
                            obj.status = true;
                        }
                        else
                            obj.status = false;


                        list.Add(obj);
                    }

                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("Sims508_Update_Assignment_details")]
        public HttpResponseMessage Sims508_Update_Assignment_details(Sims508 obj, List<Sims508> doc)
        {
            bool Inserted = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                    new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'U'),
               new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
               new SqlParameter("@sims_gb_number", obj.sims_gb_number),
               new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
               new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number),
               new SqlParameter("@sims_assignment_title", obj.sims_assignment_title),
               new SqlParameter("@sims_assignment_desc", obj.sims_assignment_desc),
               new SqlParameter("@sims_assignment_submission_date", DateTime.Parse(obj.sims_assignment_submission_date).Year + "-" + DateTime.Parse(obj.sims_assignment_submission_date).Month + "-" + DateTime.Parse(obj.sims_assignment_submission_date).Day),
               new SqlParameter("@sims_assignment_freeze_date", DateTime.Parse(obj.sims_assignment_freeze_date).Year + "-" + DateTime.Parse(obj.sims_assignment_freeze_date).Month + "-" + DateTime.Parse(obj.sims_assignment_freeze_date).Day)
               });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();

                    //
                    int i = 0;
                    int t = 0;
                    foreach (Sims508 item in doc)
                    {
                        SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                        new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", 'C'),
                   new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                   new SqlParameter("@sims_section_code", obj.sims_section_code),
                   new SqlParameter("@sims_assignment_doc_line_no", (t).ToString()),
                   new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                   new SqlParameter("@sims_assignment_doc_path", item.sims_assignment_doc_path)
                   });
                        if (dr.HasRows)
                        {
                            t++;
                            continue;
                        }

                        else
                        {
                            dr2.Close();
                            SqlDataReader dr3 = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                        new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", 'D'),
                       new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                       new SqlParameter("@sims_section_code", obj.sims_section_code),
                       new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                       new SqlParameter("@sims_assignment_doc_line_no", (t).ToString()),
                       new SqlParameter("@sims_assignment_doc_path", item.sims_assignment_doc_path),
                        new SqlParameter("@sims_assignment_doc_status",obj.sims_assignment_status==true?"A":"I")
                    });
                            if (dr2.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }


        //[Route("")]
        //public HttpResponseMessage Sims508_Update_student_details(Sims508 obj, List<Sims508> stud_info)
        //{
        //    bool Inserted = false;
        //    try
        //    {
        //        foreach (Sims508 item in stud_info)
        //        {
        //            if (item.status)
        //            {
        //                using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //                SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
        //              new List<SqlParameter>() 
        //                 { 
        //               new SqlParameter("@opr", 'E'),
        //               new SqlParameter("@sims_grade_code", obj.sims_grade_code),
        //               new SqlParameter("@sims_section_code", obj.sims_section_code),
        //               new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
        //               new SqlParameter("@sims_assignment_enroll_number", item.stud_code),
        //                if (item.stud_status == "Verify" || item.stud_status == "Submitted" || item.stud_status == "Pending")
        //                    continue;
        //                });

        //                int entry = cmd.ExecuteNonQuery();
        //                if (entry > 0)
        //                {
        //                    Inserted = true;
        //                }
        //            }

        //            else
        //            {




        //                SqlDataReader dr = db.ExecuteStoreProcedure("sims_assignment", con);



        //               new SqlParameter("@opr", 'F');
        //               new SqlParameter("@sims_grade_code", obj.sims_grade_code);
        //               new SqlParameter("@sims_section_code", obj.sims_section_code);
        //               new SqlParameter("@sims_assignment_number", obj.sims_assignment_number);
        //               new SqlParameter("@sims_assignment_enroll_number", item.stud_code);

        //                int entry = cmd.ExecuteNonQuery();
        //                if (entry > 0)
        //                {
        //                    Inserted = true;
        //                }

        //            }
        //        }

        //        return Inserted;
        //    }
        //    catch
        //    {

        //        return Inserted;
        //    }

        //}
        //}

        [Route("Uccw311_Get_student_details")]
        public HttpResponseMessage Uccw311_Get_student_details(Sims508 obj)
        {
            List<Sims508> list = new List<Sims508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'Q'),
               new SqlParameter("@sims_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_section_code", obj.sims_section_code),
               new SqlParameter("@assignment_no", obj.sims_assignment_number)
               });
                    while (dr.Read())
                    {
                        Sims508 obj1 = new Sims508();

                        obj1.stud_code = dr["sims_enroll_number"].ToString();
                        obj1.stud_name = dr["stud_name"].ToString();
                        if (dr["sims_assignment_status"].ToString() == "V")
                        {
                            obj1.stud_status = "Veryfiy";
                            obj1.status = true;
                        }
                        else if (dr["sims_assignment_status"].ToString() == "S")
                        {
                            obj1.stud_status = "Submitted";
                            obj1.status = true;
                        }
                        else if (dr["sims_assignment_status"].ToString() == "P")
                        {
                            obj1.stud_status = "Pending";
                            obj1.status = true;
                        }
                        else
                            obj1.status = false;
                        list.Add(obj1);
                    }
                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        #endregion


        #region Upload Assignment Student(Sims511)

        [Route("Sims511_get_Stud_Assignments_code")]
        public HttpResponseMessage Sims511_get_Stud_Assignments_code(string enroll_no, string title)
        {
            List<Sims511> lst = new List<Sims511>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_Student_assignment_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "B"),
               new SqlParameter("@sims_assignment_enroll_number", enroll_no),
               new SqlParameter("@sims_assignment_title", title)
               });
                    while (dr.Read())
                    {
                        Sims511 obj = new Sims511();
                        obj.sims_grade_code = dr["sims_grade_code"].ToString();
                        obj.sims_section_code = dr["sims_section_code"].ToString();
                        obj.sims_assignment_enroll_number = dr["sims_assignment_enroll_number"].ToString();
                        obj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                        obj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                        obj.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                        obj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                        obj.sims_assignment_teacher_code = dr["sims_assignment_teacher_code"].ToString();
                        obj.sims_assignment_subject_code = dr["sims_assignment_subject_code"].ToString();
                        obj.sims_assignment_student_doc_student_remark = dr["sims_assignment_student_remark"].ToString();
                        obj.sims_assignment_student_doc_teacher_remark = dr["sims_assignment_student_doc_teacher_remark"].ToString();
                        obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_assignment_start_date"].ToString()))
                            obj.sims_assignment_start_date = DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                            obj.sims_assignment_submission_date = DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_submission_date"].ToString()).Day;
                        if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                            obj.sims_assignment_freeze_date = DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Day;
                        if (dr["sims_assignment_status"].ToString() == "P")
                            obj.sims_assignment_status = "Not Submitted";
                        else
                            obj.sims_assignment_status = "Submitted";

                        lst.Add(obj);
                    }
                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("Sims511_Insert_Assignment_details")]
        public HttpResponseMessage Sims511_Insert_Assignment_details(Sims511 obj, List<Sims511> doc)
        {
            bool Inserted = false;

            try
            {
                int i = 0;
                int t = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (doc.Count() != 0)
                    {
                        #region
                        foreach (Sims511 item in doc)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_Student_assignment_proc",
                           new List<SqlParameter>()
                         {
                       new SqlParameter("@opr", 'C'),
                       new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                       new SqlParameter("@sims_section_code", obj.sims_section_code),
                       new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                       new SqlParameter("@sims_assignment_enroll_number", obj.sims_assignment_enroll_number),
                       new SqlParameter("@sims_assignment_student_line_no", (t).ToString()),
                       new SqlParameter("@sims_assignment_student_doc_path", item.sims_assignment_student_doc_path)
                       });
                            if (dr.HasRows)
                            {
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sims_Student_assignment_proc",
                             new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'U'),
                           new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                           new SqlParameter("@sims_section_code", obj.sims_section_code),
                           new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                           new SqlParameter("@sims_assignment_enroll_number", obj.sims_assignment_enroll_number),
                           new SqlParameter("@sims_assignment_student_doc_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                           new SqlParameter("@sims_assignment_student_remark", obj.sims_assignment_student_doc_student_remark),
                           new SqlParameter("@sims_assignment_student_line_no", (t).ToString()),
                           new SqlParameter("@sims_assignment_student_doc_path", item.sims_assignment_student_doc_path),
                        });
                                t++;
                                if (dr2.RecordsAffected > 0)
                                {
                                    Inserted = true;
                                }
                                dr2.Close();
                            }
                            else
                            {
                                SqlDataReader dr4 = db.ExecuteStoreProcedure("sims.sims_Student_assignment_proc",
                               new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'I'),
                           new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                           new SqlParameter("@sims_section_code", obj.sims_section_code),
                           new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                           new SqlParameter("@sims_assignment_enroll_number", obj.sims_assignment_enroll_number),
                           new SqlParameter("@sims_assignment_student_doc_date", DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day),
                           new SqlParameter("@sims_assignment_student_remark", obj.sims_assignment_student_doc_student_remark),
                           new SqlParameter("@sims_assignment_student_line_no", (t).ToString()),
                           new SqlParameter("@sims_assignment_student_doc_path", item.sims_assignment_student_doc_path)
                         });
                                if (dr4.RecordsAffected > 0)
                                {
                                    Inserted = true;
                                }
                                t++;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        SqlDataReader dr5 = db.ExecuteStoreProcedure("sims_Student_assignment",
                        new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", 'Q'),
                   new SqlParameter("@sims_assignment_number", obj.sims_assignment_number),
                   new SqlParameter("@sims_assignment_enroll_number", obj.sims_assignment_enroll_number),
                   new SqlParameter("@sims_assignment_student_remark", obj.sims_assignment_student_doc_student_remark)
                         });
                        if (dr5.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                    }
                }
            }
            catch (Exception ex) { }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);


        }

        [Route("Sims511_get_Assignment_List")]
        public HttpResponseMessage Sims511_get_Assignment_List(string Assignemnt, string Student_no)
        {
            List<Sims511> lst = new List<Sims511>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@tbl_name", "Z"),
               new SqlParameter("@sims_assignment_number", Student_no),
               new SqlParameter("@sims_assignment_enroll_number", Assignemnt)
               });
                    while (dr.Read())
                    {
                        Sims511 obj = new Sims511();
                        obj.sims_assignment_student_doc_path = dr["sims_assignment_student_doc_path"].ToString();
                        lst.Add(obj);
                    }
                }
            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        #endregion

        //#region sims509(Teacher_assignment_view)


        [Route("GetGradeData")]
        public HttpResponseMessage GetGradeData(string academic_year,string cur_code)
        {
            List<GradeScheme> mod_list = new List<GradeScheme>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_co_scholatic_attribute_proc",
                           new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'G'),
               new SqlParameter("@sims_academic_year", academic_year),
               new SqlParameter("@sims_cur_code", cur_code)
               });
                    string grade_code = string.Empty;
                    GradeScheme simsobj = new GradeScheme();

                    while (dr.Read())
                    {
                        str = dr["sims_grade_code"].ToString();

                        if (string.IsNullOrEmpty(grade_code) || grade_code != str)
                        {
                            grade_code = str;
                            simsobj = new GradeScheme();
                        }
                        gradesection leav = new gradesection();
                        var v = (from p in mod_list where p.GradeCode == str select p);

                        if (v.Count() == 0)
                        {
                            simsobj.gradesectionlist = new List<gradesection>();
                            simsobj.GradeCode = dr["sims_grade_code"].ToString();
                            simsobj.GradeName = dr["sims_grade_name_en"].ToString();
                            //leav.section_code = dr["sims_section_code"].ToString();
                            //leav.section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                        //else
                        {
                            leav.section_code = dr["sims_section_code"].ToString();
                            leav.section_name = dr["sims_section_name_en"].ToString();
                            if (!simsobj.gradesectionlist.Contains(leav))
                                simsobj.gradesectionlist.Add(leav);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getTermforGradebook")]
        public HttpResponseMessage getTermforGradebook(string cur_code, string academic_year)
        {
            List<gradebook> mod_list = new List<gradebook>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GRADEBOOK_NEW_PROC]",
                           new List<SqlParameter>()
                         {

               new SqlParameter("@OPR","T"),
               new SqlParameter("@CUR_CODE", cur_code),
               new SqlParameter("@ACADEMIC_YEAR", academic_year)
               });
                    while (dr.Read())
                    {
                        gradebook simsobj = new gradebook();
                        simsobj.sims_term_code = dr["sims_term_code"].ToString();
                        simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        //[Route("")]
        //public List<sims509> GetTeacherAssignView(string username, string grade, string section, string ass_name)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'M');
        //       new SqlParameter("@user_name", username);
        //       new SqlParameter("@grade_name", grade);
        //       new SqlParameter("@section_name", section);
        //       new SqlParameter("@sims_assignment_name", ass_name);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_Assignment_No = dr["sims_assignment_number"].ToString();
        //            simsobj.sims509_Assignment_Name = dr["sims_assignment_title"].ToString();
        //            simsobj.sims509_subject = dr["SubjectName"].ToString();
        //            simsobj.sims509_grade = dr["Grade"].ToString();
        //            simsobj.sims509_section_name = dr["Section"].ToString();
        //            simsobj.sims509_submission = dr["submission"].ToString();
        //            string date = dr["sims_assignment_start_date"].ToString();
        //            if (!string.IsNullOrEmpty(date))
        //                simsobj.sims509_assign_date = DateTime.Parse(date).Year + "-" + DateTime.Parse(date).Month + "-" + DateTime.Parse(date).Day;
        //            string date1 = dr["sims_assignment_submission_date"].ToString();
        //            if (!string.IsNullOrEmpty(date1))
        //                simsobj.sims509_due_date = DateTime.Parse(date1).Year + "-" + DateTime.Parse(date1).Month + "-" + DateTime.Parse(date1).Day; ;
        //            simsobj.sims509_assgn_status = dr["sims_assignment_status"].ToString();
        //            simsobj.sims509_status = dr["ass_status"].ToString();
        //            if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
        //                simsobj.sims509_freez_date = DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_freeze_date"].ToString()).Day;
        //            if (dr["sims_assignment_status"].ToString() == "F")
        //            {
        //                simsobj.btnedit = false;
        //            }
        //            else
        //            {
        //                simsobj.btnedit = true;
        //            }
        //            // simsobj.color = dr["col"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }

        //[Route("")]
        //public List<sims509> GetAssignmentDetails(string assign_no)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'P');
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_Assignment_Name = dr["sims_assignment_title"].ToString();
        //            simsobj.sims509_Assignment_desc = dr["sims_assignment_desc"].ToString();
        //            string date = dr["sims_assignment_start_date"].ToString();
        //            if (!string.IsNullOrEmpty(date))
        //                simsobj.sims509_assign_date = DateTime.Parse(date).Year + "-" + DateTime.Parse(date).Month + "-" + DateTime.Parse(date).Day;
        //            string date1 = dr["sims_assignment_submission_date"].ToString();
        //            if (!string.IsNullOrEmpty(date1))
        //                simsobj.sims509_due_date = DateTime.Parse(date1).Year + "-" + DateTime.Parse(date1).Month + "-" + DateTime.Parse(date1).Day;
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }


        //[Route("")]
        //public List<sims509> GetStudentAssignmentDetails(string username, string assign_no)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'Q');
        //       new SqlParameter("@user_name", username);
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_stud_name = dr["StudentName"].ToString();
        //            simsobj.sims509_desc = dr["sims_assignment_teacher_remark"].ToString();
        //            simsobj.sims509_attachment = dr["count"].ToString();
        //            simsobj.sims509_assgn_status = dr["sims_assignment_status"].ToString();
        //            string date = dr["sims_assignment_student_doc_date"].ToString();
        //            if (!string.IsNullOrEmpty(date))
        //                simsobj.sims509_assign_date = DateTime.Parse(date).Year + "-" + DateTime.Parse(date).Month + "-" + DateTime.Parse(date).Day;
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }

        //[Route("")]
        //public List<sims509> GetStudentdocumentDetails(string username, string assign_no)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    List<sims509_doctdetails> docdetails = new List<sims509_doctdetails>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'R');
        //       new SqlParameter("@user_name", username);
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_enroll_no = dr["sims_assignment_enroll_number"].ToString();
        //            simsobj.sims509_stud_name = dr["StudentName"].ToString();
        //            simsobj.sims509_desc = dr["sims_assignment_teacher_remark"].ToString();
        //            simsobj.sims509_stud_doc_remark = dr["sims_assignment_student_remark"].ToString();
        //            docdetails = new List<sims509_doctdetails>();
        //            docdetails = Get_stud_doc_Details(dr["sims_assignment_enroll_number"].ToString(), username, assign_no);
        //            simsobj.doc_details = docdetails;
        //            // simsobj.sims509_attachment = dr["sims_assignment_student_doc_path"].ToString();
        //            // simsobj.sims509_doc_remark = dr["sims_assignment_student_doc_teacher_remark"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }


        //private List<sims509_doctdetails> Get_stud_doc_Details(string enroll_no, string username, string assign_no)
        //{
        //    List<sims509_doctdetails> group_list = new List<sims509_doctdetails>();
        //    try
        //    {
        //        if (con1.State == ConnectionState.Closed)
        //        {
        //            con1.Open();
        //        }
        //        cmd1 = new ("Teacher_assign_view");
        //        cmd1.CommandType = CommandType.StoredProcedure;
        //        cmd1.Connection = con1;
        //        new SqlParameter("@opr", 'N');
        //        new SqlParameter("@enroll_no", enroll_no);
        //        new SqlParameter("@user_name", username);
        //        new SqlParameter("@sims_assignment_number", assign_no);
        //        dr1 = cmd1.ExecuteReader();
        //        //string str=null;
        //        while (dr1.Read())
        //        {
        //            sims509_doctdetails obj = new sims509_doctdetails();
        //            obj.sims509_enroll_no = enroll_no;
        //            obj.sims509_doc = dr1["sims_assignment_student_doc_path"].ToString();
        //            obj.sims509_doc_remark = dr1["sims_assignment_student_doc_teacher_remark"].ToString();

        //            group_list.Add(obj);
        //        }
        //        cmd1.Dispose();
        //        con1.Close();
        //        return group_list;
        //    }
        //    catch (Exception e)
        //    {
        //        cmd1.Dispose();
        //        con1.Close();
        //        return group_list;
        //    }
        //}

        //[Route("")]
        //public bool Update_StudentdocumentDetails(sims509 simsobj)
        //{
        //    bool updated = false;
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //        new SqlParameter("@opr", 'U');
        //        new SqlParameter("@enroll_no", simsobj.sims509_enroll_no);
        //        new SqlParameter("@sims_assignment_teacher_remark", simsobj.sims509_desc);
        //        new SqlParameter("@sims_assignment_number", simsobj.sims509_Assignment_No);
        //        int up = cmd.ExecuteNonQuery();
        //        if (up > 0)
        //        {
        //            updated = true;
        //        }

        //        foreach (sims509_doctdetails st in simsobj.doc_details)
        //        {
        //            if (!string.IsNullOrEmpty(st.sims509_doc_remark))
        //                Update_StudentdocDetails(st);
        //        }

        //        return updated;
        //    }
        //    catch (Exception e)
        //    {

        //        return false;
        //    }

        //}

        //// [Route("")]
        //public bool Update_StudentdocDetails(sims509_doctdetails simsobj)
        //{
        //    bool updated = false;
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //        new SqlParameter("@opr", 'B');
        //        new SqlParameter("@enroll_no", simsobj.sims509_enroll_no);
        //        new SqlParameter("@sims_assignment_student_doc_path", simsobj.sims509_doc);
        //        new SqlParameter("@sims_assignment_teacher_remark", simsobj.sims509_doc_remark);
        //        int up = cmd.ExecuteNonQuery();
        //        if (up > 0)
        //        {
        //            updated = true;
        //        }


        //        return updated;


        //    }
        //    catch (Exception e)
        //    {

        //        return false;
        //    }

        //}

        //[Route("")]
        //public List<sims509> GetSubmissionDetails(string assign_no)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'D');
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_Assignment_Name = dr["sims_assignment_title"].ToString();
        //            string date1 = dr["sims_assignment_submission_date"].ToString();
        //            if (!string.IsNullOrEmpty(date1))
        //                simsobj.sims509_due_date = DateTime.Parse(date1).Year + "-" + DateTime.Parse(date1).Month + "-" + DateTime.Parse(date1).Day;
        //            simsobj.sims509_teacher_name = dr["Teacher_Name"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }


        //[Route("")]
        //public List<sims509> GetStudentSubmissionDetails(string assign_no, string username)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'C');
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       new SqlParameter("@user_name", username);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_enroll_no = dr["sims_assignment_enroll_number"].ToString();
        //            simsobj.sims509_stud_name = dr["StudentName"].ToString();
        //            simsobj.sims509_assgn_status = dr["sims_assignment_status"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }


        //[Route("")]
        //public List<sims509> GetDocumentDetails(string assign_no)
        //{
        //    List<sims509> mod_list = new List<sims509>();
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //       new SqlParameter("@opr", 'E');
        //       new SqlParameter("@sims_assignment_number", assign_no);
        //       });
        //        while (dr.Read())
        //        {
        //            sims509 simsobj = new sims509();
        //            simsobj.sims509_Assignment_Name = dr["sims_assignment_title"].ToString();
        //            string date1 = dr["sims_assignment_submission_date"].ToString();
        //            if (!string.IsNullOrEmpty(date1))
        //                simsobj.sims509_due_date = DateTime.Parse(date1).Year + "-" + DateTime.Parse(date1).Month + "-" + DateTime.Parse(date1).Day;
        //            simsobj.sims509_document = dr["sims_assignment_student_doc_path"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        dr.Close();
        //        cmd.Dispose();

        //        return mod_list;
        //    }

        //[Route("")]
        //public bool sims509_checkDocument(string assign_no)
        //{
        //    bool flag = false;
        //    try
        //    {


        //        SqlDataReader dr = db.ExecuteStoreProcedure("GetData", con);

        //       new SqlParameter("@tbl_name", "[sims].[sims_assignment_student]");
        //       new SqlParameter("@tbl_col_name1", "Count(*) as cnt");
        //       new SqlParameter("@tbl_cond", "sims_assignment_number='" + assign_no + "'");
        //       });
        //        dr.Read();
        //        int r = int.Parse(dr["cnt"].ToString());
        //        if (r > 0)
        //            flag = false;
        //        else
        //            flag = true;

        //        return flag;
        //    }


        //[Route("")]
        //public bool Delete_DocumentDetails(string assign_no)
        //{
        //    bool deleted = false;
        //    try
        //    {



        //        SqlDataReader dr = db.ExecuteStoreProcedure("Teacher_assign_view");


        //        new SqlParameter("@opr", 'L');
        //        new SqlParameter("@sims_assignment_number", assign_no);
        //        int del = cmd.ExecuteNonQuery();
        //        if (del > 0)
        //        {
        //            deleted = true;
        //        }
        //        cmd.Dispose();

        //        return deleted;
        //    }
        //    catch (Exception e)
        //    {

        //        return deleted;
        //    }
        //}
        #endregion

        #endregion

        [Route("AllGradeSection_subject")]
        public HttpResponseMessage AllGradeSection_subject(Sims508 data)
        {
            List<Sims508> lst = new List<Sims508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_header_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "SU"),
               new SqlParameter("@sims_gb_teacher_code",data.sims_employee_code),
               new SqlParameter("@sims_academic_year",data.sims_academic_year),
               new SqlParameter("@sims_grade_code", data.sims_grade_code),
               new SqlParameter("@sims_section_code",data.sims_section_code)

               });
                    while (dr.Read())
                    {
                        Sims508 obj = new Sims508();
                        obj.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                        obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        lst.Add(obj);
                    }

                }

            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        #region Co_scholastic_grade_scale

        [Route("GetAllgradeScaleforco_scholastic")]
        public HttpResponseMessage GetAllgradeScaleforco_scholastic(string cur_code, string academic_year)
        {
            List<GradeSchemeforco_scholastic> lst = new List<GradeSchemeforco_scholastic>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_co_scholastic_group_grade_scale_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'S'),
               new SqlParameter("@sims_cur_code", cur_code),
               new SqlParameter("@sims_academic_year", academic_year)
               });
                    while (dr.Read())
                    {
                        GradeSchemeforco_scholastic obj = new GradeSchemeforco_scholastic();
                        string str = "";
                        str = dr["sims_co_scholastic_grade_scale_code1"].ToString();
                        obj.gradelst = new List<ScholGrade>();
                        obj.GradeGroupCode = str + "";
                        obj.Curr_Code = dr["sims_cur_code"].ToString();
                        obj.Academic_Year = dr["sims_academic_year"].ToString();
                        obj.GradeGroupDesc = dr["sims_co_scholastic_grade_scale_description"].ToString();
                        obj.grade_status = dr["sims_co_scholastic_grade_scale_status"].ToString().Equals("A") ? true : false;
                        ScholGrade l = new ScholGrade();
                        l.Curr_Code = dr["sims_cur_code"].ToString();
                        l.Academic_Year = dr["sims_academic_year"].ToString();
                        l.Grade_Code = dr["sims_co_scholastic_grade_code"].ToString();
                        l.Grade_Name = dr["sims_co_scholastic_grade_name"].ToString();
                        l.Grade_Point_high = dr["sims_co_scholastic_grade_point_high"].ToString();
                        l.Grade_Point_low = dr["sims_co_scholastic_grade_point_low"].ToString();
                        l.Color_Code = dr["sims_co_scholastic_grade_color_code"].ToString();
                        l.Grade_Description = dr["sims_co_scholastic_grade_description"].ToString();
                        l.Status = dr["sims_co_scholastic_grade_status"].ToString().Equals("A") ? true : false;
                        l.GradegruopCode = str + "";
                        var v = (from p in lst where p.GradeGroupCode == str select p);
                        if (v.Count() == 0)
                        {
                            if (l.Grade_Code != "" && l.Grade_Description != "")
                            {
                                obj.gradelst.Add(l);
                            }
                            lst.Add(obj);
                        }
                        else
                        {
                            v.ElementAt(0).gradelst.Add(l);
                        }
                    }
                }
            }
            catch
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDgradeforco_scholastic")]
        public HttpResponseMessage CUDgradeforco_scholastic(List<ScholGrade> obj)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ScholGrade obj1 in obj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_grade_scale_proc]",
                       new List<SqlParameter>()
                         {
               new SqlParameter("@opr",obj1.opr),
               new SqlParameter("@sims_cur_code",obj1.Curr_Code),
               new SqlParameter("@sims_academic_year",obj1.Academic_Year),
               new SqlParameter("@sims_co_scholastic_grade_scale_code",obj1.GradegruopCode),
               new SqlParameter("@sims_co_scholastic_grade_code",obj1.Grade_Code),
               new SqlParameter("@sims_co_scholastic_grade_name",obj1.Grade_Name),
               new SqlParameter("@sims_co_scholastic_grade_point_low",obj1.Grade_Point_low),
               new SqlParameter("@sims_co_scholastic_grade_point_high",obj1.Grade_Point_high),
               new SqlParameter("@sims_co_scholastic_grade_description",obj1.Grade_Description),
               new SqlParameter("@sims_co_scholastic_grade_color_code",obj1.Color_Code),
               new SqlParameter("@sims_co_scholastic_grade_status",obj1.Status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch(Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

        [Route("CUDgradeScaleforco_scholastic")]
        public HttpResponseMessage CUDgradeScaleforco_scholastic(List<GradeSchemeforco_scholastic> obj)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (GradeSchemeforco_scholastic obj1 in obj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_co_scholastic_group_grade_scale_proc]",
                       new List<SqlParameter>()
                         {
               new SqlParameter("@opr",obj1.opr),
               new SqlParameter("@sims_cur_code",obj1.Curr_Code),
               new SqlParameter("@sims_academic_year",obj1.Academic_Year),
               new SqlParameter("@sims_co_scholastic_grade_scale_code",obj1.GradeGroupCode),
                new SqlParameter("@sims_co_scholastic_grade_scale_description",obj1.GradeGroupDesc),
               new SqlParameter("@sims_co_scholastic_grade_scale_status",obj1.grade_status.Equals(true)?"A":"I")

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

        #endregion

        #region Sims146(Report_card_level)

        [Route("GetReportCardLevel")]
        public HttpResponseMessage GetReportCardLevel()
        {
            List<Sims146> list = new List<Sims146>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_level_proc",
                 new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "S")
                });
                    while (dr.Read())
                    {
                        Sims146 simsobj = new Sims146();
                        simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                        simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        simsobj.sims_cur_name = dr["cur_name"].ToString();
                        simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.sims_grade_name = dr["grade_name"].ToString();
                        simsobj.sims_section_code = dr["sims_section_code"].ToString();
                        simsobj.sims_section_name = dr["section_name"].ToString();
                        simsobj.sims_level_code = dr["sims_level_code"].ToString();
                        simsobj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                        simsobj.sims_level_status = dr["sims_report_card_status"].ToString() == "A";
                        list.Add(simsobj);
                    }
                }

            }
            catch (Exception ex) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("InsertUpdateDeleteReportCardLevel")]
        public HttpResponseMessage InsertUpdateDeleteReportCardLevel(Sims146 simsobj, string opr)
        {
            int ins = 0;
            List<Sims146> lst = new List<Sims146>();
            try
            {
                string[] classes = simsobj.sims_section_code.Split(',');

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var item in classes)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_level",
                        new List<SqlParameter>()
                         {
                   new SqlParameter("@opr", opr),
                   new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                   new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                   new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                   new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                   new SqlParameter("@sims_level_code", simsobj.sims_level_code),
                   new SqlParameter("@class_code", item),
                   new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
                   new SqlParameter("@sims_level_status", simsobj.sims_level_status ? "A" : "I")
                         });
                    }
                }
            }
            catch (Exception e)
            {

            }
            List<Sims146> l = new List<Sims146>();
            if (ins > 0)
            {
                //l.AddRange(GetReportCardLevel());
            }
            //lst.Add(ins + "", l);

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        #endregion

        #region Sims138(Report Card)

        [Route("GetReportCardLevelName")]
        public HttpResponseMessage GetReportCardLevelName(string academic_year, string cur_name, string grade_name, string section_name)
        {
            List<Sims138> list = new List<Sims138>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetReportCardLevel",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@cur_code", cur_name),
               new SqlParameter("@academic_year", academic_year),
               new SqlParameter("@grade_code", grade_name),
               new SqlParameter("@section_code", section_name)
               });
                    while (dr.Read())
                    {
                        Sims138 simsobj = new Sims138();
                        simsobj.sims_level_name = dr["sims_report_card_name"].ToString();
                        list.Add(simsobj);
                    }

                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetParamDesc")]
        public HttpResponseMessage GetParamDesc(string cur_name, string academic_year, string grade_name, string section_name)
        {
            string cur_code = null, grade_code = null, section_code = null;
            List<Sims138> list = new List<Sims138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "Z"),
               new SqlParameter("@sims_cur_code", cur_code),
               new SqlParameter("@sims_academic_year", cur_code),
               new SqlParameter("@sims_grade_code",grade_code),
               new SqlParameter("@sims_section_code", section_code)
               });
                    while (dr.Read())
                    {
                        Sims138 simsobj = new Sims138();
                        simsobj.sims_report_card_parameter_master_code = dr["sims_report_card_parameter_master_code"].ToString();
                        simsobj.param_desc = dr["sims_report_card_param_description"].ToString();
                        list.Add(simsobj);

                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetReportCard")]
        public HttpResponseMessage GetReportCard()
        {
            List<Sims138> list = new List<Sims138>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_proc",
                 new List<SqlParameter>()
                         {
               new SqlParameter("@opr", 'S')
               });
                    while (dr.Read())
                    {
                        Sims138 obj = new Sims138();
                        obj.sims_academic_year = dr["sims_academic_year"].ToString();
                        obj.sims_cur_name = dr["cur_name"].ToString();
                        obj.sims_level_name = dr["sims_level_name"].ToString();
                        obj.sims_grade_name = dr["grade_name"].ToString();
                        obj.sims_section_name = dr["section_name"].ToString();
                        obj.sims_report_card_code = dr["sims_report_card_code"].ToString();
                        obj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                        obj.sims_report_card_desc = dr["sims_report_card_description"].ToString();
                        obj.sims_report_card_declaration_date = dr["sims_report_card_declaration_date"].ToString();
                        obj.sims_report_card_publish_date = dr["sims_report_card_publishing_date"].ToString();
                        obj.user_name = dr["user_name"].ToString();
                        obj.sims_report_card_created_user = dr["sims_report_card_created_user_code"].ToString();
                        if (dr["sims_report_card_freeze_status"].ToString() == "Y")
                            obj.sims_report_card_freeze_status = true;
                        else
                            obj.sims_report_card_freeze_status = false;
                        obj.sims_report_card_parameter_master_code = dr["sims_report_card_parameter_master_code"].ToString();
                        obj.param_desc = dr["param_desc"].ToString();
                        obj.term_name = dr["report_term"].ToString();
                        list.Add(obj);
                    }

                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetReportCode")]
        public HttpResponseMessage GetReportCode()
        {
            string code = "1";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                    new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "N")
               });
                    while (dr.Read())
                    {
                        code = dr["maxcode"].ToString();
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, code);

        }

        [Route("InsertReportCard")]
        public HttpResponseMessage InsertReportCard(Sims138 simsobj, string par_desc)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_proc",
                    new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "I"),
               new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
               new SqlParameter("@sims_cur_name", simsobj.sims_cur_name),
               new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
               new SqlParameter("@sims_section_name", simsobj.sims_section_name),
               new SqlParameter("@sims_level_name", simsobj.sims_level_name),
               new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_code),
               new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
               new SqlParameter("@sims_report_card_description", simsobj.sims_report_card_desc),
               new SqlParameter("@sims_report_card_declaration_date", Convert.ToDateTime(simsobj.sims_report_card_declaration_date)),
               new SqlParameter("@sims_report_card_publishing_date", Convert.ToDateTime(simsobj.sims_report_card_publish_date)),
               new SqlParameter("@sims_report_card_created_user_code", simsobj.sims_report_card_created_user),
               new SqlParameter("@sims_report_card_freeze_status",simsobj.sims_report_card_freeze_status == true?"Y":"N"),
               new SqlParameter("@sims_report_card_freeze_status", "N"),
               new SqlParameter("@sims_report_card_parameter_master_code_desc", simsobj.param_desc),
               new SqlParameter("@sims_report_card_parameter_master_code", par_desc),
               new SqlParameter("@sims_report_card_term", simsobj.term_name)
                    });
                    if (dr.RecordsAffected > 0)
                        inserted = true;
                    else
                        inserted = false;
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);

        }

        [Route("UpdateReportCard")]
        public HttpResponseMessage UpdateReportCard(Sims138 simsobj, string par_desc)
        {
            bool updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_proc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "U"),
               new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
               new SqlParameter("@sims_cur_name", simsobj.sims_cur_name),
               new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
               new SqlParameter("@sims_section_name", simsobj.sims_section_name),
               new SqlParameter("@sims_level_name", simsobj.sims_level_name),
               new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_code),
               new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
               new SqlParameter("@sims_report_card_description", simsobj.sims_report_card_desc),
               new SqlParameter("@sims_report_card_declaration_date", Convert.ToDateTime(simsobj.sims_report_card_declaration_date)),
               new SqlParameter("@sims_report_card_publishing_date", Convert.ToDateTime(simsobj.sims_report_card_publish_date)),
               new SqlParameter("@sims_report_card_created_user_code", simsobj.sims_report_card_created_user),
               new SqlParameter("@sims_report_card_freeze_status",simsobj.sims_report_card_freeze_status == true?"Y":"N"),
               new SqlParameter("@sims_report_card_freeze_status", "N"),
               new SqlParameter("@sims_report_card_parameter_master_code_desc", simsobj.param_desc),
               new SqlParameter("@sims_report_card_parameter_master_code", par_desc),
               new SqlParameter("@sims_report_card_term", simsobj.term_name)
                   });
                    if (dr.RecordsAffected > 0)
                        updated = true;
                    else
                        updated = false;
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("DeleteReportCard")]
        public HttpResponseMessage DeleteReportCard(Sims138 simsobj)
        {
            bool deleted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_porc",
                   new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "D"),
               new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
               new SqlParameter("@sims_cur_name", simsobj.sims_cur_name),
               new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
               new SqlParameter("@sims_section_name", simsobj.sims_section_name),
               new SqlParameter("@sims_level_name", simsobj.sims_level_name),
               new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_code),
               new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
               new SqlParameter("@sims_report_card_description", simsobj.sims_report_card_desc),
               new SqlParameter("@sims_report_card_declaration_date", Convert.ToDateTime(simsobj.sims_report_card_declaration_date)),
               new SqlParameter("@sims_report_card_publishing_date", Convert.ToDateTime(simsobj.sims_report_card_publish_date)),
               new SqlParameter("@sims_report_card_created_user_code", simsobj.sims_report_card_created_user),
               new SqlParameter("@sims_report_card_freeze_status",simsobj.sims_report_card_freeze_status == true?"Y":"N"),
               new SqlParameter("@sims_report_card_parameter_master_code_desc", simsobj.param_desc)
                   });
                    if (dr.RecordsAffected > 0)
                        deleted = true;
                    else
                        deleted = false;
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, deleted);

        }

        [Route("Reportlevels")]
        public HttpResponseMessage Reportlevels(Subject sb)
        {
            List<ReportCardLevel> lst = new List<ReportCardLevel>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_PROC",
                          new List<SqlParameter>()
                         {

                new SqlParameter("@OPR", "RL"),
                new SqlParameter("@CUR_CODE", sb.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", sb.SubjectAcademic),
                new SqlParameter("@GRADE_CODE", sb.SubjectGrade),
                new SqlParameter("@SECTION_CODE", sb.SubjectSection)
                });
                    while (dr.Read())
                    {
                        ReportCardLevel rpl = new ReportCardLevel();
                        rpl.Level_code = dr["sims_level_code"].ToString();
                        rpl.Level_Name = dr["sims_report_card_name"].ToString();
                        lst.Add(rpl);
                    }

                    lst.ForEach(q => q.Academic_Year = sb.SubjectAcademic);
                    lst.ForEach(q => q.Curr_Code = sb.SubjectCurr);
                    lst.ForEach(q => q.Grade_Code = sb.SubjectGrade);
                    lst.ForEach(q => q.Section_code = sb.SubjectSection);
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetReportCards")]
        public HttpResponseMessage GetReportCards(Subject sb)
        {
            List<ReportCardLevel> lst = new List<ReportCardLevel>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GRADEBOOK_PROC",
                          new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", "RC"),
                new SqlParameter("@CUR_CODE", sb.SubjectCurr),
                new SqlParameter("@ACADEMIC_YEAR", sb.SubjectAcademic),
                new SqlParameter("@GRADE_CODE", sb.SubjectGrade),
                new SqlParameter("@SECTION_CODE", sb.SubjectSection),
                new SqlParameter("@TERM_CODE", sb.SubjectTerm),
                new SqlParameter("@FIELD_NAME",sb.AssignmentNumber),//level code
                new SqlParameter("@GB_TEACHER_CODE", sb.SubTeacherEmpId)
              });
                    while (dr.Read())
                    {
                        ReportCardLevel rpl = new ReportCardLevel();
                        rpl.Report_Card_Name = dr["sims_report_card_name"].ToString();
                        rpl.Report_Card_Code = dr["sims_report_card_code"].ToString();
                        rpl.Report_Card_Desc = dr["sims_report_card_description"].ToString();
                        rpl.Report_Card_Decla_Date = dr["sims_report_card_declaration_date"].ToString();
                        rpl.Report_Card_Publishing_Date = dr["sims_report_card_publishing_date"].ToString();
                        rpl.Report_Card_Freez_Status = dr["sims_report_card_freeze_status"].ToString();
                        rpl.Report_Card_Parameter_Master = dr["sims_report_card_parameter_master_code"].ToString();
                        rpl.Report_Card_User_Code = dr["sims_report_card_created_user_code"].ToString();
                        rpl.Report_Card_Term = dr["sims_report_card_term"].ToString();
                        lst.Add(rpl);
                    }

                    lst.ForEach(q => q.Academic_Year = sb.SubjectAcademic);
                    lst.ForEach(q => q.Curr_Code = sb.SubjectCurr);
                    lst.ForEach(q => q.Grade_Code = sb.SubjectGrade);
                    lst.ForEach(q => q.Section_code = sb.SubjectSection);
                    lst.ForEach(q => q.Level_code = sb.SubTeacherEmpId);
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ReportCardStudent_Finalcomment")]
        public HttpResponseMessage ReportCardStudent_Finalcomment(ReportCardStudent sb)
        {
            List<ReportCardStudent> lst = new List<ReportCardStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_student_final_comment_proc",
                          new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_level_code", sb.Level_code)

              });
                    while (dr.Read())
                    {
                        ReportCardStudent rpl = new ReportCardStudent();
                        rpl.Report_Card_Name = dr["sims_report_card_name"].ToString();
                        rpl.Report_Card_Code = dr["sims_report_card_code"].ToString();
                        rpl.Comment1_code = dr["sims_comment1_code"].ToString();
                        rpl.Comment1_desc = dr["Comment1"].ToString();
                        rpl.Comment2_code = dr["sims_comment2_code"].ToString();
                        rpl.Comment2_desc = dr["Comment2"].ToString();
                        rpl.Comment3_code = dr["sims_comment3_code"].ToString();
                        rpl.Comment3_desc = dr["Comment3"].ToString();
                        rpl.enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        rpl.Level_code = dr["sims_level_code"].ToString();
                        rpl.Level_Name = dr["LevelName"].ToString();
                        rpl.Status = dr["sims_student_comment_status"].ToString();
                        //rpl.Subject_Code = dr["sims_subject_code"].ToString();
                        rpl.StudName = dr["StudName"].ToString();
                        rpl.user1 = dr["sims_user1_code"].ToString();
                        rpl.user2 = dr["sims_user2_code"].ToString();
                        rpl.user3 = dr["sims_user3_code"].ToString();
                        rpl.included = dr["avi"].ToString();
                        rpl.Academic_Year = sb.Academic_Year;
                        rpl.Curr_Code = sb.Curr_Code;
                        rpl.Grade_Code = sb.Grade_Code;
                        rpl.Section_code = sb.Section_code;
                        try
                        {
                            rpl.ColumnVisibility = dr["colVis"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        lst.Add(rpl);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        #endregion

        #region Report Card Comment

        [Route("GetRCGrade")]
        public HttpResponseMessage GetRCGrade(string curcode, string ayear, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                   new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "TG"),
                new SqlParameter("@CUR_CODE", curcode),
                new SqlParameter("@ACADEMIC_YEAR", ayear),
                new SqlParameter("@GB_TEACHER_CODE", teachercode)
            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCSection")]
        public HttpResponseMessage GetRCSection(string curcode, string ayear, string gradecode, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK_PROC]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "TS"),
                new SqlParameter("@CUR_CODE", curcode),
                new SqlParameter("@ACADEMIC_YEAR", ayear),
                new SqlParameter("@GRADE_CODE", gradecode),
                new SqlParameter("@GB_TEACHER_CODE", teachercode)
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCReportCardLevel")]
        public HttpResponseMessage GetRCReportCardLevel(string curcode, string ayear, string gradecode, string section)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "SL"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section)

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_report_card_level = dr["sims_level_code"].ToString();
                            obj.sims_report_card_level_name = dr["sims_report_card_name"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCReportCardNames")]
        public HttpResponseMessage GetRCReportCardNames(string curcode, string ayear, string gradecode, string section, string levelCode, string term)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SR"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_term_code", term),
                new SqlParameter("@sims_level_code", levelCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_report_card = dr["sims_report_card_code"].ToString();
                            obj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();

                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCStudentForCommentsheader")]
        public HttpResponseMessage GetRCStudentForCommentsheader(string curcode, string ayear, string gradecode, string section, string term_code, string comment_type)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_term_code", term_code),
                new SqlParameter("@sims_comment_type", comment_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            com.sims_comment_header_desc = dr["sims_comment_desc"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("GetRCStudentForCommentsheaderNew")]
        public HttpResponseMessage GetRCStudentForCommentsheaderNew(string curcode, string ayear, string gradecode, string section, string term_code, string comment_type, string subject_code)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_term_code", term_code),
                new SqlParameter("@sims_comment_type", comment_type),
                new SqlParameter("@sims_subject_code", subject_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            com.sims_comment_header_desc = dr["sims_comment_desc"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }



        [Route("GetRCStudentForCommentsheaderforoes")]
        public HttpResponseMessage GetRCStudentForCommentsheaderforoes(string curcode, string ayear, string gradecode, string section, string term_code, string comment_type, string subject_code)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_comment_type", comment_type),
                new SqlParameter ("@sims_subject_code",subject_code),
                new SqlParameter("@sims_term_code", term_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            com.sims_comment_header_desc = dr["sims_comment_desc"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }


        [Route("RCStudentForCommentspearl")]
        public HttpResponseMessage RCStudentForCommentspearl(student_report_card_comment data)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_header_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SZ"),
                new SqlParameter("@sims_cur_code", data.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year",data.sims_academic_year),
                new SqlParameter("@sims_grade_code", data.sims_grade_code),
                new SqlParameter("@sims_section_code", data.sims_section_code),
                new SqlParameter("@sims_level_code", data.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", data.sims_report_card),
                new SqlParameter("@sims_student_comment_id", data.sims_student_comment_id),
                new SqlParameter("@sims_comment_type", data.sims_comment_type)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str = dr["sims_enroll_number"].ToString();
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_Name = dr["StudentName"].ToString();
                            obj.sims_student_comment_id = dr["sims_student_comment_id"].ToString();
                            obj.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            obj.sims_type = dr["sims_comment_type"].ToString();
                            obj.sims_comment = dr["sims_comment_desc"].ToString();
                            obj.sims_student_check_status = false;
                            obj.sims_student_check_status2 = false;
                            var v = (from p in AcademicYear where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                AcademicYear.Add(obj);
                            }


                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("RCInsertCommentforStudentpearl")]
        public HttpResponseMessage RCInsertCommentforStudentpearl(List<student_report_card_comment> inpt1)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment inpt in inpt1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                     {
                new SqlParameter("@opr", "PC"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_comment_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_comment_status",inpt.sims_comment_status==true?"A":"I"),
                new SqlParameter("@sims_user_code",inpt.sims_user_code),
           });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("RCInsertCommentforStudentoes")]
        public HttpResponseMessage RCInsertCommentforStudentoes(student_report_card_comment inpt)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
               new List<SqlParameter>()
                 {
                new SqlParameter("@opr", "ON"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_student_root_comment_id", inpt.sims_student_comment_id1),
                new SqlParameter("@sims_comment_status",inpt.sims_comment_status==true?"A":"I"),
                new SqlParameter("@sims_user_code",inpt.sims_user_code),
       });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }


            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("GetRCStudentForComments")]
        public HttpResponseMessage GetRCStudentForComments(string curcode, string ayear, string gradecode, string section, string term_code, string sub_code)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "SS"),
                           new SqlParameter("@sims_cur_code", curcode),
                           new SqlParameter("@sims_academic_year", ayear),
                           new SqlParameter("@sims_grade_code", gradecode),
                           new SqlParameter("@sims_section_code", section),
                           new SqlParameter("@sims_term_code", term_code),
                           new SqlParameter("@sims_subject_code", sub_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str = dr["sims_enroll_number"].ToString();
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.coments_lst = new List<student_report_card_level1_comment>();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_Name = dr["StudentName"].ToString();
                            obj.sims_student_check_status = false;
                            obj.sims_student_check_status2 = false;
                            obj.sims_student_comment_id = dr["sims_student_comment_id"].ToString();

                            student_report_card_level1_comment com = new student_report_card_level1_comment();
                            com.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            com.sims_subject_code = dr["sims_subject_code"].ToString();
                            com.sims_type = dr["sims_comment_type"].ToString();
                            var v = (from p in AcademicYear where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                obj.coments_lst.Add(com);
                                AcademicYear.Add(obj);
                            }
                            else
                            {
                                v.ElementAt(0).coments_lst.Add(com);
                            }
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("GetRCComments")]
        public HttpResponseMessage GetRCComments(string curcode, string ayear, string sub_code, string report_levl, string report_card_code, string type)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "RC"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_subject_code", sub_code),
                new SqlParameter("@sims_level_code", report_levl),
                new SqlParameter("@sims_report_card_code", report_card_code),
                new SqlParameter("@sims_comment_type", type)

               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.ReportCardComment = dr["sims_comment_desc"].ToString();
                            obj.sims_student_comment_id = dr["sims_comment_code"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("RCCopyComments")]
        public HttpResponseMessage RCCopyComments(student_report_card_comment inpt)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CO"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_copy_enroll_number", inpt.copyEnrolls)
                         });
                    res = dr.RecordsAffected > 0 ? true : false;
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("RCCommentsforStudent")]
        public HttpResponseMessage RCCommentsforStudent(student_report_card_comment inpt)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SH"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_root_comment_id", null),
                new SqlParameter("@sims_comment_type", "H")


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_student_comment_header_id = dr["sims_student_comment_id"].ToString();
                            obj.sims_comment_header_desc = dr["sims_comment_desc"].ToString();
                            obj.sims_type = "H";
                            obj.sims_comment_status = dr["sims_comment_status"].ToString().Equals("A") ? true : false;
                            obj.sims_academic_year = inpt.sims_academic_year;
                            obj.sims_admission_cur_code = inpt.sims_admission_cur_code;
                            obj.sims_grade_code = inpt.sims_grade_code;
                            obj.sims_section_code = inpt.sims_section_code;
                            obj.sims_subject_code = inpt.sims_subject_code;
                            obj.sims_term_code = inpt.sims_term_code;
                            obj.sims_report_card_level = inpt.sims_report_card_level;
                            obj.sims_report_card = inpt.sims_report_card;


                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                                new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", "SH"),
                        new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                        new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                        new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                        new SqlParameter("@sims_section_code", inpt.sims_section_code),
                        new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                        new SqlParameter("@sims_term_code", inpt.sims_term_code),
                        new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                        new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                        new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                        new SqlParameter("@sims_student_root_comment_id", obj.sims_student_comment_header_id),
                        new SqlParameter("@sims_comment_type", "C")

                    });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        student_report_card_level1_comment comt = new student_report_card_level1_comment();
                                        comt.sims_comment_code = dr1["sims_student_comment_id"].ToString();
                                        comt.sims_comment = dr1["sims_comment_desc"].ToString();
                                        comt.sims_student_comment_header_id = obj.sims_student_comment_header_id;
                                        comt.sims_type = "C";
                                        comt.sims_comment_status = dr1["sims_comment_status"].ToString().Equals("A") ? true : false;
                                        obj.coments_lst.Add(comt);

                                        /* if (con1.State == ConnectionState.Closed)
                                            con1.Open();
                                        SqlCommand cmd2 = new SqlCommand("sims_report_card_comment_header", con1);
                                        cmd2.CommandType = CommandType.StoredProcedure;
                                        new SqlParameter("@opr", "CS");
                                        new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code);
                                        new SqlParameter("@sims_academic_year", inpt.sims_academic_year);
                                        new SqlParameter("@sims_grade_code", inpt.sims_grade_code);
                                        new SqlParameter("@sims_section_code", inpt.sims_section_code);
                                        new SqlParameter("@sims_subject_code", inpt.sims_subject_code);
                                        new SqlParameter("@sims_term_code", inpt.sims_term_code);
                                        new SqlParameter("@sims_level_code", inpt.sims_report_card_level);
                                        new SqlParameter("@sims_report_card_code", inpt.sims_report_card);
                                        new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number);
                                        new SqlParameter("@sims_comment_type", inpt.sims_comment_type);
                                        new SqlParameter("@header_id", obj.sims_student_comment_header_id);
                                        SqlDataReader dr2 = cmd2.ExecuteReader();
                                        if (dr2.HasRows)
                                        {
                                            while (dr2.Read())
                                            {
                                                student_report_card_sub_comment sub_comment = new student_report_card_sub_comment();
                                                sub_comment.sims_sub_comment_code = dr2["sims_student_comment_id"].ToString();
                                                sub_comment.sims_sub_comment = dr2["sims_student_comment_desc"].ToString();
                                                sub_comment.sims_student_comment_header_id = obj.sims_student_comment_header_id;
                                                sub_comment.sims_root_comment_id = comt.sims_comment_code;
                                                sub_comment.sims_type = "S";
                                                comt.sub_comments.Add(sub_comment);
                                            }
                                        }
                                        dr2.Close();
                                        dr2.Dispose();
                                        */

                                    }
                                }
                                db1.Dispose();
                                dr1.Close();
                            }


                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("RCCommentsforStudentoes")]
        public HttpResponseMessage RCCommentsforStudentoes(student_report_card_comment inpt)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "OS"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_root_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_comment_type", "C")


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();

                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_comment_code = dr["sims_comment_code"].ToString();
                            obj.sims_comment_type = dr["sims_comment_type"].ToString();
                            obj.sims_header_comment_code = dr["sims_header_comment_code"].ToString();
                            obj.sims_comment_desc = dr["sims_comment_desc"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();

                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }




        [Route("RCInsertHeaderforStudent")]
        public HttpResponseMessage RCInsertHeaderforStudent(List<student_report_card_comment> inpt_list)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment inpt in inpt_list)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                        new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", "IH"),
                        new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                        new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                        new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                        new SqlParameter("@sims_section_code", inpt.sims_section_code),
                        new SqlParameter("@sims_term_code", inpt.sims_term_code),
                        new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                        new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                        new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                        new SqlParameter("@sims_comment_type", inpt.sims_type),
                        new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                        new SqlParameter("@sims_comment_header_desc", inpt.sims_comment_header_desc),
                        new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_header_id)
                         });
                        dr.Close();
                    }


                    foreach (student_report_card_comment inpt in inpt_list)
                    {

                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                                    new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "IC"),
                            new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                            new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                            new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                            new SqlParameter("@sims_section_code", inpt.sims_section_code),
                            new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                            new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                            new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                            new SqlParameter("@sims_term_code", inpt.sims_term_code),
                            new SqlParameter("@sims_comment_type", inpt.sims_type),
                            new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                            new SqlParameter("@sims_comment_status", inpt.sims_comment_status == true ? "A" : "I"),
                            new SqlParameter("@sims_student_comment_id", inpt.sims_comment_code),
                            new SqlParameter("@sims_student_comment_desc", inpt.sims_comment),

                         });
                        if (dr1.RecordsAffected > 0)
                        {
                            res = true;
                        }
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CommentsStudent")]
        public HttpResponseMessage CommentsStudent(student_report_card_comment data)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_cur_code", data.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_level_code", data.sims_report_card_level),
                new SqlParameter("@sims_report_card_code",data.sims_report_card),
                new SqlParameter("@sims_grade_code", data.sims_grade_code),
                new SqlParameter("@sims_section_code", data.sims_section_code),
                new SqlParameter("@sims_subject_code", data.sims_subject_code),
                new SqlParameter("@sims_enroll_number", data.sims_enroll_number),
                new SqlParameter("@sims_term_code", data.sims_term_code)
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_comment_code = dr["sims_comment_desc"].ToString();
                            obj.sims_comment_type = dr["sims_comment_type"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("CommentsStudent1")]
        public HttpResponseMessage CommentsStudent1(student_report_card_comment data)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "PS"),
                new SqlParameter("@sims_cur_code", data.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_level_code", data.sims_report_card_level),
                new SqlParameter("@sims_report_card_code",data.sims_report_card),
                new SqlParameter("@sims_grade_code", data.sims_grade_code),
                new SqlParameter("@sims_section_code", data.sims_section_code),
                new SqlParameter("@sims_subject_code", data.sims_subject_code),
                new SqlParameter("@sims_enroll_number", data.sims_enroll_number),
                new SqlParameter("@sims_term_code", data.sims_term_code),
                new SqlParameter("@header_id",data.sims_student_comment_header_id)
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            obj.sims_comment_type = dr["sims_comment_type"].ToString();
                            obj.sims_comment = dr["sims_comment_desc"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("CommentsStudent1new")]
        public HttpResponseMessage CommentsStudent1new(student_report_card_comment data)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_report_card_comment_header_proc_temp_new]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "PS"),
                new SqlParameter("@sims_cur_code", data.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_level_code", data.sims_report_card_level),
                new SqlParameter("@sims_report_card_code",data.sims_report_card),
                new SqlParameter("@sims_grade_code", data.sims_grade_code),
                new SqlParameter("@sims_section_code", data.sims_section_code),
                new SqlParameter("@sims_subject_code", data.sims_subject_code),
                new SqlParameter("@sims_enroll_number", data.sims_enroll_number),
                new SqlParameter("@sims_term_code", data.sims_term_code),
                new SqlParameter("@header_id",data.sims_student_comment_header_id)
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_comment_code = dr["sims_student_comment_id"].ToString();
                            obj.sims_comment_type = dr["sims_comment_type"].ToString();
                            obj.sims_comment = dr["sims_comment_desc"].ToString();
                            obj.sims_header_comment_code = dr["sims_header_comment_code"].ToString();
                            obj.sims_sims_freeze_status = dr["sims_report_card_freeze_status"].ToString().Equals("Y") ? true : false;

                            AcademicYear.Add(obj);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }


        [Route("RCInsertCommentforStudent")]
        public HttpResponseMessage RCInsertCommentforStudent(List<student_report_card_comment> inpt1)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment inpt in inpt1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "IC"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_comment_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_comment_status",inpt.sims_comment_status==true?"A":"I"),
                new SqlParameter("@sims_user_code",inpt.sims_user_code),
               });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                }
            }

            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("RCNewInsertCommentforStudent")]
        public HttpResponseMessage RCNewInsertCommentforStudent(student_report_card_comment inpt)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "NI"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_comment_header_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_comment_type1",inpt.sims_comment_type1),
               });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("RCInsertSubCommentforStudent")]
        public HttpResponseMessage RCInsertSubCommentforStudent(student_report_card_comment inpt)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "IS"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_comment_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_student_root_comment_id", inpt.sims_root_comment_id)

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("RCDeleteComments")]
        public HttpResponseMessage RCDeleteComments(List<student_report_card_comment> inpt1)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment inpt in inpt1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "D"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_comment_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_student_comment_id", inpt.sims_student_comment_id),
                new SqlParameter("@sims_student_root_comment_id", inpt.sims_root_comment_id)

               });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region Report Card Comment Mapping

        [Route("RC_mapCommentsforStudent")]
        public HttpResponseMessage RC_mapCommentsforStudent(student_report_card_comment inpt)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_mapping_proc",
                   new List<SqlParameter>()
                  {
                new SqlParameter("@opr", "SH"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                //    new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_student_root_comment_id", null),
                new SqlParameter("@sims_comment_type", "H")
                  });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_student_comment_header_id = dr["sims_student_comment_id"].ToString();
                            obj.sims_comment_header_desc = dr["sims_comment_desc"].ToString();
                            obj.sims_type = "H";
                            obj.sims_comment_status = dr["sims_comment_status"].ToString().Equals("A") ? true : false;
                            obj.sims_academic_year = inpt.sims_academic_year;
                            obj.sims_admission_cur_code = inpt.sims_admission_cur_code;
                            obj.sims_grade_code = inpt.sims_grade_code;
                            obj.sims_section_code = inpt.sims_section_code;
                            obj.sims_subject_code = inpt.sims_subject_code;
                            obj.sims_term_code = inpt.sims_term_code;
                            obj.sims_report_card_level = inpt.sims_report_card_level;
                            obj.sims_report_card = inpt.sims_report_card;
                            obj.sims_enroll_number = inpt.sims_enroll_number;

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_report_card_comment_header_mapping_proc",
                                new List<SqlParameter>()
                        {
                        new SqlParameter("@opr", "SH1"),
                        new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                        new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                        new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                        new SqlParameter("@sims_section_code", inpt.sims_section_code),
                        new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                        new SqlParameter("@sims_term_code", inpt.sims_term_code),
                        new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                        new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                        new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                        new SqlParameter("@sims_student_root_comment_id", obj.sims_student_comment_header_id),
                        new SqlParameter("@sims_comment_type", "C")

                      });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        student_report_card_level1_comment comt = new student_report_card_level1_comment();
                                        comt.sims_comment_code = dr1["sims_student_comment_id"].ToString();
                                        comt.sims_comment = dr1["sims_comment_desc"].ToString();
                                        comt.sims_student_comment_header_id = obj.sims_student_comment_header_id;
                                        comt.sims_type = "C";
                                        comt.sims_comment_status = dr1["sims_comment_status"].ToString().Equals("A") ? true : false;
                                        obj.coments_lst.Add(comt);

                                        /* if (con1.State == ConnectionState.Closed)
                                            con1.Open();
                                        SqlCommand cmd2 = new SqlCommand("sims_report_card_comment_header_mapping", con1);
                                        cmd2.CommandType = CommandType.StoredProcedure;
                                        new SqlParameter("@opr", "CS");
                                        new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code);
                                        new SqlParameter("@sims_academic_year", inpt.sims_academic_year);
                                        new SqlParameter("@sims_grade_code", inpt.sims_grade_code);
                                        new SqlParameter("@sims_section_code", inpt.sims_section_code);
                                        new SqlParameter("@sims_subject_code", inpt.sims_subject_code);
                                        new SqlParameter("@sims_term_code", inpt.sims_term_code);
                                        new SqlParameter("@sims_level_code", inpt.sims_report_card_level);
                                        new SqlParameter("@sims_report_card_code", inpt.sims_report_card);
                                        new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number);
                                        new SqlParameter("@sims_comment_type", inpt.sims_comment_type);
                                        new SqlParameter("@header_id", obj.sims_student_comment_header_id);
                                        SqlDataReader dr2 = cmd2.ExecuteReader();
                                        if (dr2.HasRows)
                                        {
                                            while (dr2.Read())
                                            {
                                                student_report_card_sub_comment sub_comment = new student_report_card_sub_comment();
                                                sub_comment.sims_sub_comment_code = dr2["sims_student_comment_id"].ToString();
                                                sub_comment.sims_sub_comment = dr2["sims_student_comment_desc"].ToString();
                                                sub_comment.sims_student_comment_header_id = obj.sims_student_comment_header_id;
                                                sub_comment.sims_root_comment_id = comt.sims_comment_code;
                                                sub_comment.sims_type = "S";
                                                comt.sub_comments.Add(sub_comment);
                                            }
                                        }
                                        dr2.Close();
                                        dr2.Dispose();
                                        */

                                    }
                                }
                                dr1.Close();
                                AcademicYear.Add(obj);
                            }



                        }


                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }


        #endregion

        #region GradeBook_Agenda(PP030)
        [Route("Get_section_subject_agendaDetails")]
        public HttpResponseMessage Get_section_subject_agendaDetails(Sims527 obj, string date, string date1)
        {
            List<Sims527> term_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_proc]",
                  new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "P"),
               new SqlParameter("@sdate", date),
               new SqlParameter("@edate", date1),
               new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
               new SqlParameter("@sims_grade_code", obj.sims_grade_code),
               new SqlParameter("@sims_section_code", obj.sims_section_code)
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 simsobj = new Sims527();
                            simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                            simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                            simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                            simsobj.file = dr["sims_agenda_doc"].ToString();
                            if (string.IsNullOrEmpty(simsobj.file))
                                simsobj.vis = "Collapsed";
                            else
                                simsobj.vis = "Visible";
                            simsobj.sims_agenda_date = dr["sims_agenda_date"].ToString();
                            simsobj.day = dr["Day"].ToString() + '(' + dr["sims_agenda_date"].ToString() + ')';
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["subject"].ToString();
                            term_list.Add(simsobj);
                        }
                    }

                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }
        #endregion

        [Route("GetRCTerm")]
        public HttpResponseMessage GetRCTerm(string curcode, string ayear)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_header_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "T"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_term_code = dr["sims_term_code"].ToString();
                            obj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }


        [Route("CUDgradeScaleData")]
        public HttpResponseMessage CUDgradeScaleData(string opr,string gradeName,string description,bool status)
        {
            bool insert = false;
            //Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                  //  foreach (GradeScale simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.MarkGRADE_PROC",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", opr),
                               // new SqlParameter("@sims_library_catalogue_code",gradegroupnumber),
                                new SqlParameter("@group_name",gradeName),
                                new SqlParameter("@Description", description),
                                new SqlParameter("@narr_grade_status", status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /* message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDgradeScaleDataupdate")]
        public HttpResponseMessage CUDgradeScaleDataupdate(string opr, string groupcode,string gradeName, string description, bool status)
        {
            bool insert = false;
            //Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //  foreach (GradeScale simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.MarkGRADE_PROC",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", opr),
                                new SqlParameter("@groupcode",groupcode),
                                new SqlParameter("@group_name",gradeName),
                                new SqlParameter("@Description", description),
                                new SqlParameter("@narr_grade_status", status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /* message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDgradeScaleDataUpdate")]
        public HttpResponseMessage CUDgradeScaleDataUpdate(GradeScheme obj)
        {

            bool inserted = false;
            List<GradeScheme> lst = new List<GradeScheme>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarkGRADE_PROC",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@OPR", obj.opr),
                    new SqlParameter("@TeacherCode", null),
                    new SqlParameter("@CurrCode", null),
                    new SqlParameter("@AcademicYear", null),
                    new SqlParameter("@group_name", obj.GradeName),
                    new SqlParameter("@Description", obj.Description),
                    new SqlParameter("@narr_grade_status",obj.status==true?"A":"I")

                });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }


        #endregion

        #region GradeBookNewCommon

        [Route("GradeBookEffortCommon")]
        public HttpResponseMessage GradeBookEffortCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_gradebook_effortcopy_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

    }
}


