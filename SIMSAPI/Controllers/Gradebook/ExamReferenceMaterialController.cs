﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.gradebookClass;



namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/ExamRef")]
    public class ExamReferenceMaterialController : ApiController
    {

        [Route("getAllExamRefMaterial")]
        public HttpResponseMessage getAllExamRefMaterial()
        {
            List<ExRefM> achieve_list = new List<ExRefM>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ExRefM simsobj = new ExRefM();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name"].ToString();

                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name"].ToString();
                            simsobj.sims_gb_ref_gb_subject_code = dr["sims_gb_ref_gb_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name"].ToString();

                            simsobj.sims_gb_ref_gb_cat_code = dr["sims_gb_ref_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            simsobj.sims_gb_ref_name = dr["sims_gb_ref_name"].ToString();
                            simsobj.sims_gb_ref_desc = dr["sims_gb_ref_desc"].ToString();
                            simsobj.sims_gb_ref_status = dr["sims_gb_ref_status"].Equals("A") ? true : false;

                            simsobj.sims_gb_ref_number = dr["sims_gb_ref_number"].ToString();
                            simsobj.sims_gb_ref_line_number = dr["sims_gb_ref_line_number"].ToString();
                            simsobj.sims_gb_ref_publish_date = db.UIDDMMYYYYformat(dr["sims_gb_ref_publish_date"].ToString());
                            simsobj.sims_gb_ref_expiry_date = db.UIDDMMYYYYformat(dr["sims_gb_ref_expiry_date"].ToString());
                            simsobj.sims_gb_ref_filename = dr["sims_gb_ref_filename"].ToString();
                            simsobj.sims_gb_ref_display_name = dr["sims_gb_ref_display_name"].ToString();

                            achieve_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, achieve_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, achieve_list);
        }

        [Route("getSubjects")]
        public HttpResponseMessage getSubjects(string sims_grade_code)
        {
            List<ExRefM> mod_list = new List<ExRefM>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@GRADE_CODE",sims_grade_code)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ExRefM hrmsObj = new ExRefM();
                            hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                            hrmsObj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getGradebooks")]
        public HttpResponseMessage getGradebooks(string subject_code, string grade_code)
        {
            List<ExRefM> mod_list = new List<ExRefM>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                        new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", "A"),
                    new SqlParameter("@GB_REF_SUBJECT_CODE",subject_code),
                    new SqlParameter("@GRADE_CODE",grade_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ExRefM hrmsObj = new ExRefM();
                            hrmsObj.sims_gb_number = dr["sims_gb_number"].ToString();
                            hrmsObj.sims_gb_name = dr["sims_gb_name"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getGradebookCategory")]
        public HttpResponseMessage getGradebookCategory(string grade_code, string gb_number)
        {
            List<ExRefM> mod_list = new List<ExRefM>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                        new List<SqlParameter>() 
                         { 
                                  new SqlParameter("@opr","C"),
                                  new SqlParameter("@GRADE_CODE",grade_code),
                                  new SqlParameter("@GB_REF_GB_NUMBER",gb_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ExRefM hrmsObj = new ExRefM();
                            hrmsObj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            hrmsObj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getGradebookAssignment")]
        public HttpResponseMessage getGradebookAssignment(string grade_code, string gb_number, string gb_cat_code)
        {
            List<ExRefM> mod_list = new List<ExRefM>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                             new SqlParameter("@GRADE_CODE",grade_code),
                             new SqlParameter("@GB_REF_GB_NUMBER",gb_number),
                            new SqlParameter("@GB_REF_CAT_CODE",gb_cat_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ExRefM hrmsObj = new ExRefM();
                            hrmsObj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            hrmsObj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDExamRefMaterial")]
        public HttpResponseMessage CUDExamRefMaterial(string data, List<ExRefM> data1)
        {


            List<ExRefM> data2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ExRefM>>(data.ToString());

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ExRefM obj in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                         new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","I"),
                          new SqlParameter("@GB_REF_NUMBER",""),
                          new SqlParameter("@CUR_CODE",obj.sims_cur_code),
                          new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                          new SqlParameter("@GRADE_CODE",obj.sims_grade_code),
                          new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                          new SqlParameter("@GB_REF_GB_NUMBER",obj.sims_gb_number),
                          new SqlParameter("@GB_REF_SUBJECT_CODE", obj.sims_subject_code),
                          new SqlParameter("@GB_REF_CAT_CODE",obj.sims_gb_cat_code),
                          new SqlParameter("@GB_REF_CAT_ASSIGN_NUMBER",obj.sims_gb_cat_assign_number),
                          new SqlParameter("@GB_REF_NAME", obj.gb_ref_name),
                          new SqlParameter("@GB_REF_DESC",obj.gb_ref_desc),
                          new SqlParameter("@GB_REF_STATUS",obj.gb_status==true?"A":"I"),
                        });

                        if (dr.Read())
                        {

                            string ref_no = dr["ref_no"].ToString();
                            dr.Close();
                            foreach (ExRefM simsobj in data2)
                            {
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
                               new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","R"),
                          new SqlParameter("@GB_REF_NUMBER",ref_no),
                          new SqlParameter("@GB_REF_FILE_NAME",simsobj.gb_ref_file_name),
                          new SqlParameter("@GB_REF_DISPLAY_NAME",simsobj.gb_ref_display_name),
                          new SqlParameter("@GB_REF_PUBLISH_DATE",db.DBYYYYMMDDformat(obj.gb_ref_publish_date)),
                          new SqlParameter("@GB_REF_EXPIRY_DATE",db.DBYYYYMMDDformat(obj.gb_ref_expiry_date))

                        });

                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }
                                dr2.Close();
                            }
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("UpExamRefMaterial")]
        public HttpResponseMessage UpExamRefMaterial(List<ExRefM> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (ExRefM persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[gradebook_reference_details_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr","U"),
                                    new SqlParameter("@GB_REF_NUMBER",persobj.sims_gb_ref_number),
                                    new SqlParameter("@GB_REF_LINE_NUMBER",persobj.sims_gb_ref_line_number),
                                    new SqlParameter("@GB_REF_NAME", persobj.gb_ref_name),
                                    new SqlParameter("@GB_REF_DESC", persobj.gb_ref_desc),
                                    new SqlParameter("@GB_REF_STATUS", persobj.sims_gb_ref_status.Equals(true)?"A":"I"),
                                    new SqlParameter("@GB_REF_PUBLISH_DATE", db.DBYYYYMMDDformat(persobj.gb_ref_publish_date)),
                                    new SqlParameter("@GB_REF_EXPIRY_DATE",db.DBYYYYMMDDformat(persobj.gb_ref_expiry_date))
                                  
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("DelExamRefMaterial")]
        public HttpResponseMessage DelExamRefMaterial(List<ExRefM> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (ExRefM persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[gradebook_reference_details_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr","D"),
                                    new SqlParameter("@GB_REF_NUMBER",persobj.sims_gb_ref_number),
                                    new SqlParameter("@GB_REF_LINE_NUMBER",persobj.sims_gb_ref_line_number),
                                  
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}