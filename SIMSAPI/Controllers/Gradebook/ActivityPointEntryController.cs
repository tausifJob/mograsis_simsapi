﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using System.Web;

namespace SIMSAPI.Controllers.modules.StudentHealthController
{
    [RoutePrefix("api/StudentActivity")]
    [BasicAuthentication]
    public class ActivityPointEntryController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region

        [Route("GetAllActivity")]
        public HttpResponseMessage GetAllActivity(string cur_code, string academic_year)
        {
            List<ACT100> term_list = new List<ACT100>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_active_point_entry_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","T"),
                             new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_academic_year",academic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ACT100 simsobj = new ACT100();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();

                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        } 
         
        
        
        [Route("CUDStudentActivity")]
        public HttpResponseMessage CUDStudentActivity(List<ACT100> data)
        {

            bool inserted = false;
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ACT100 simsobj in data)
                    {
                        
                         

                            ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_active_point_entry_proc]",
                                        new List<SqlParameter>()
                        {

                               new SqlParameter("@opr", simsobj.opr),
                               
                               new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                               new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                               new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                               new SqlParameter("@sims_section_code",simsobj.sims_section_code),

                               new SqlParameter("@sims_student_enroll_number",simsobj.sims_enroll_number),
                               new SqlParameter("@stud_Add_Activity",simsobj.sims_activity_number),
                               new SqlParameter("@sims_activity_points",simsobj.sims_activity_points),
                               new SqlParameter("@sims_term_code",simsobj.sims_term_code),
                        });
                        
                        if (ins > 0)

                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getStudent")]
        public HttpResponseMessage getStudent(string sims_cur_code, string sims_academic_year, string sims_activity_number )
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<ACT100> stud_list = new List<ACT100>();

            if (sims_activity_number == "undefined")
            {
                sims_activity_number = null;
            }
             
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_active_point_entry_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_activity_number",sims_activity_number),
                             

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ACT100 simsobj = new ACT100();
                             

                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            //simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            try
                            {
                                simsobj.sims_roll_number = int.Parse(new String(dr["sims_roll_number"].ToString().Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_points = dr["sims_activity_points"].ToString();

                            stud_list.Add(simsobj);


                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }


        [Route("getStudentsActivityDetails")]
        public HttpResponseMessage getStudentsActivityDetails(string curcode, string academicyear, string activitynumber, string gradecode, string sectioncode, string termcode)
        {
            List<ACT100> studentexamdetail = new List<ACT100>();
            try
            {
                if (activitynumber == "undefined")
                {
                    activitynumber = null;
                }
                if (gradecode == "undefined" || gradecode == "null")
                {
                    gradecode = null;
                }
                if (sectioncode == "undefined" || sectioncode == "null")
                {
                    sectioncode = null;
                }
                if (termcode == "undefined" || termcode == "null")
                {
                    termcode = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_active_point_entry_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@sims_academic_year", academicyear),
                             new SqlParameter("@sims_activity_number", activitynumber),
                              new SqlParameter("@sims_grade_code", gradecode),
                              new SqlParameter("@sims_section_code", sectioncode),
                              new SqlParameter("@sims_term_code", termcode),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ACT100 sequence = new ACT100();

                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_student_full_name = dr["sims_student_full_name"].ToString();
                            sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            //sequence.sims_roll_number = dr["sims_roll_number"].ToString();

                            try
                            {
                                sequence.sims_roll_number = int.Parse(new String(dr["sims_roll_number"].ToString().Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }
                            sequence.sims_activity_number = dr["sims_activity_number"].ToString();
                            sequence.sims_activity_points = dr["sims_activity_points"].ToString();
                            sequence.sims_term_number = dr["sims_term_number"].ToString();

                            try
                            {
                                sequence.sims_activity_max_point = dr["sims_activity_max_point"].ToString();

                            }
                            catch (Exception ex)
                            { }

                            studentexamdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, studentexamdetail);
        }


        [Route("getTermforGradebook")]
        public HttpResponseMessage getTermforGradebook(string cur_code, string academic_year,string grade_code, string section_code)
        {

            
            List<ACT100> mod_list = new List<ACT100>();
            try
            {
                if (grade_code == "undefined")
                {
                    grade_code = null;
                }

                if (section_code == "undefined")
                {
                    section_code = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_active_point_entry_proc]",
                           new List<SqlParameter>()
                         {

               new SqlParameter("@OPR","H"),
               new SqlParameter("@sims_cur_code", cur_code),
               new SqlParameter("@sims_academic_year", academic_year),
                new SqlParameter("@sims_grade_code", grade_code),
                new SqlParameter("@sims_section_code", section_code)
               });
                    while (dr.Read())
                    {
                        ACT100 simsobj = new ACT100();
                        simsobj.sims_term_code = dr["sims_term_code"].ToString();
                        simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getActMaxMark")]
        public HttpResponseMessage getActMaxMark(string act_code)
        {


            List<ACT100> mod_list = new List<ACT100>();
            try
            {
                if (act_code == "undefined")
                {
                    act_code = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_active_point_entry_proc]",
                           new List<SqlParameter>()
                         {

               new SqlParameter("@OPR","K"),
               new SqlParameter("@sims_act_code", act_code),
               
               });
                    while (dr.Read())
                    {
                        ACT100 simsobj = new ACT100();
                        simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                        
                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        #endregion



    }
}