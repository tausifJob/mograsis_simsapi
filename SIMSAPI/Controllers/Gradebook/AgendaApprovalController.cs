﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

using SIMSAPI.Models.ERP.gradebookClass;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/AgendaApprovalController")]
    public class AgendaApprovalController : ApiController
    {

        [Route("GetCur")]
        public HttpResponseMessage GetCur()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                            hrmsObj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAca")]
        public HttpResponseMessage GetAca(string cur)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", cur)
                
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_acaedmic_year = dr["sims_academic_year"].ToString();
                            hrmsObj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getgrd")]
        public HttpResponseMessage Getgrd(string cur, string aca, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "G"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                            hrmsObj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getsec")]
        public HttpResponseMessage Getsec(string cur, string aca, string grd, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                            hrmsObj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetSubject")]
        public HttpResponseMessage GetSubject(string cur, string aca, string grd, string sec, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'B'),
                   new SqlParameter("@sims_cur_code", cur),
                   new SqlParameter("@sims_acaedmic_year", aca),
                   new SqlParameter("@sims_grade_code", grd),
                   new SqlParameter("@sims_section_code", sec),
                   new SqlParameter("@sims_login_code", user)
                

              });

                    while (dr.Read())
                    {
                        Sims527 hrmsObj = new Sims527();
                        hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                        hrmsObj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("UpdateAgendaStatus")]
        public HttpResponseMessage UpdateAgendaStatus(List<Sims527> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims527 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'R'),
                   new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                   new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
                   new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                   new SqlParameter("@sims_section_code", obj.sims_section_code),
                   new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                   new SqlParameter("@sims_agenda_number", obj.sims_agenda_number),
                   

              });
                        if (ins > 0)
                        {
                            inserted = true;
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("RejectAgenda")]
        public HttpResponseMessage RejectAgenda(List<Sims527> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims527 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'U'),
                   new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                   new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
                   new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                   new SqlParameter("@sims_section_code", obj.sims_section_code),
                   new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                   new SqlParameter("@sims_agenda_number", obj.sims_agenda_number),
                   

              });
                        if (ins > 0)
                        {
                            inserted = true;
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("listAgendaDetails")]
        public HttpResponseMessage listAgendaDetails(Sims527 obj)
        {
            List<Sims527> mod_list = new List<Sims527>();
            List<agenda_dates> op = new List<agenda_dates>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_approval]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'J'),
                   new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                   new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
                   new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                   new SqlParameter("@sims_section_code", obj.sims_section_code),
                   new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                   new SqlParameter("@FromDate", db.DBYYYYMMDDformat(obj.from_date)),
                   new SqlParameter("@ToDate", db.DBYYYYMMDDformat(obj.to_date)),
                   new SqlParameter("@sims_login_code", obj.user_code)
                

              });

                    while (dr.Read())
                    {
                        Sims527 hrmsObj = new Sims527();
                        hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                        hrmsObj.sims_acaedmic_year = dr["sims_acaedmic_year"].ToString();
                        hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                        hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                        hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                        hrmsObj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        hrmsObj.sims_grade_name = dr["Class_name"].ToString();

                        hrmsObj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        hrmsObj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        hrmsObj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_agenda_date"].ToString()))
                            hrmsObj.sims_agenda_date = db.UIDDMMYYYYformat(dr["sims_agenda_date"].ToString());

                        hrmsObj.sims_agenda_teacher_code = dr["sims_agenda_teacher_code"].ToString();
                        hrmsObj.sims_agenda_doc_line_no = dr["sims_agenda_doc_line_no"].ToString();
                        hrmsObj.sims_agenda_doc_sr_no = dr["sims_agenda_doc_sr_no"].ToString();
                        hrmsObj.sims_agenda_doc_name = dr["sims_agenda_doc_name"].ToString();
                        hrmsObj.sims_agenda_doc_name_en = dr["sims_agenda_doc_name_en"].ToString();



                        //hrmsObj.sims_doc = AgendaDetailsDoc(hrmsObj.sims_agenda_number);
                        mod_list.Add(hrmsObj);

                    }
                    foreach (var item in mod_list)
                    {
                        agenda_dates a = new agenda_dates();
                        a.sims_agenda_date = item.sims_agenda_date;
                        a.aid = "0";
                        a.sub_list = new List<subject_details>();
                        var v = from p in op where p.sims_agenda_date == a.sims_agenda_date select p;
                        if (v.Count() == 0)
                        {
                            a.sub_list = getAgendaSubjects(mod_list, a);
                            op.Add(a);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, op);

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<subject_details> getAgendaSubjects(List<Sims527> modList, agenda_dates a)
        {
            List<subject_details> op = new List<subject_details>();
            var i = 0;

            foreach (var item in modList)
            {
                if (item.sims_agenda_date == a.sims_agenda_date)// && item.sims_agenda_number == a.sims_agenda_number)
                {
                    subject_details asb = new subject_details();
                    asb.sims_agenda_date = item.sims_agenda_date;
                    //asb.sims_agenda_number = item.sims_agenda_number;
                    asb.sims_subject_code = item.sims_subject_code;
                    asb.sims_subject_name = item.sims_subject_name;
                    asb.sid = i + "";
                    i++;

                    asb.agendalist = new List<Agenda_details>();
                    var v = from p in op where p.sims_agenda_date == asb.sims_agenda_date && p.sims_subject_code == asb.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        asb.agendalist = getAgenda(modList, asb);
                        op.Add(asb);
                    }
                }
            }
            return op;
        }

        private List<Agenda_details> getAgenda(List<Sims527> modList, subject_details ad)
        {
            List<Agenda_details> op = new List<Agenda_details>();
            var i = 0;

            foreach (var item in modList)
            {
                if (item.sims_agenda_date == ad.sims_agenda_date && item.sims_subject_code == ad.sims_subject_code)
                {
                    Agenda_details a = new Agenda_details();
                    a.sims_agenda_date = item.sims_agenda_date;
                    a.sims_agenda_desc = item.sims_agenda_desc;
                    a.sims_agenda_name = item.sims_agenda_name;
                    a.sims_agenda_number = item.sims_agenda_number;
                    a.sims_grade_code = item.sims_grade_code;
                    a.sims_grade_name = item.sims_grade_name;
                    a.sims_cur_code = item.sims_cur_code;
                    a.sims_acaedmic_year = item.sims_acaedmic_year;
                    a.sims_section_code = item.sims_section_code;
                    a.sims_subject_code = item.sims_subject_code;
                    a.agid = i + "";
                    i++;
                    a.doc_list = new List<Sims527_doc>();
                    var v = from p in op where p.sims_agenda_number == a.sims_agenda_number && p.sims_agenda_date == a.sims_agenda_date && p.sims_subject_code == a.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        a.doc_list = getAgendaDocs(modList, a);
                        op.Add(a);
                    }
                }
            }
            return op;
        }

        private List<Sims527_doc> getAgendaDocs(List<Sims527> modList, Agenda_details a)
        {
            List<Sims527_doc> op = new List<Sims527_doc>();
            var i = 0;

            foreach (var item in modList)
            {
                if (item.sims_agenda_date == a.sims_agenda_date && item.sims_agenda_number == a.sims_agenda_number && item.sims_subject_code == a.sims_subject_code)
                {
                    Sims527_doc asb = new Sims527_doc();
                    asb.sims_agenda_date = item.sims_agenda_date;
                    asb.sims_agenda_number = item.sims_agenda_number;
                    asb.sims_subject_code = item.sims_subject_code;
                    asb.sims_agenda_doc_name = item.sims_agenda_doc_name;
                    asb.sims_agenda_doc_name_en = item.sims_agenda_doc_name_en;
                    asb.did = i + "";
                    i++;
                    op.Add(asb);
                }
            }
            return op;
        }

    }
}