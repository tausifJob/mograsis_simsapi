﻿using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/bellwAverage")]
    public class StudentBellowspecifiedAverageController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGradesNew")]
        public HttpResponseMessage getAllGradesNew(string cur_code, string academic_year)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@acad_year", academic_year)
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeNew")]
        public HttpResponseMessage getSectionFromGradeNew(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@acad_year", academic_year),
                                 new SqlParameter("@grade", grade_code),
                                  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getsubjecttype")]
        public HttpResponseMessage getsubjecttype(string cur_code)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<SBSA01> term_list = new List<SBSA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "B"),
                               new SqlParameter("@cur_code", cur_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBSA01 simsobj = new SBSA01();
                            simsobj.sims_subject_type_code = dr["sims_subject_type_code"].ToString();
                            simsobj.sims_subject_type_name_en = dr["sims_subject_type_name_en"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }

        [Route("getsubjectname")]
        public HttpResponseMessage getsubjectname(string cur_code, string academic_year, string grade_code, string section_code, string subject_type)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<SBSA01> term_list = new List<SBSA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", academic_year),
                             new SqlParameter("@grade", grade_code),
                             new SqlParameter("@section", section_code),
                             new SqlParameter("@type", subject_type)
                             

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBSA01 simsobj = new SBSA01();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }

        [Route("getAllTerm")]
        public HttpResponseMessage getAllTerm(string cur_code, string academic_year)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<SBSA01> term_list = new List<SBSA01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "U"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@acad_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBSA01 simsobj = new SBSA01();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }
            

        [Route("getbellowAverageReport")]
        public HttpResponseMessage getbellowAverageReport(string opr,string cur_code, string academic_year, string grade_code, string section_code, string term_code, string subject_type, string subject_code,string value)
        {
            List<SBSA01> lstCuriculum = new List<SBSA01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_BellowAverage2Report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", opr),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", academic_year),
                             new SqlParameter("@grade", grade_code),
                             new SqlParameter("@section", section_code),
                             new SqlParameter("@term", term_code),
                             new SqlParameter("@type", subject_type),
                             new SqlParameter ("@sub_code",subject_code),
                             new SqlParameter ("@value",value)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBSA01 sequence = new SBSA01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_subject_type_name_en = dr["sims_subject_type_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.final_marks = dr["final_marks"].ToString();                            
                           lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        


    }
}