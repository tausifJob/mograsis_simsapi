﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
using SIMSAPI.Models.GradebookReportCommon;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/GracingMarksTrans")]
    public class GracingMarksTransController : ApiController
    {
        // GET: GracingMarksTrans
        [Route("GetFailStudent")]
        public HttpResponseMessage GetFailStudent(string sims_cur_code,string sims_academic_year,string sims_grade_code,string sims_section_code,string sims_gb_term_code,string sort_by)
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gracing_marks_transaction]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_gb_term_code",sims_gb_term_code),
                            new SqlParameter("@sort_by",sort_by),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_symbol = dr["sims_activity_symbol"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_points = dr["sims_activity_points"].ToString();
                            simsobj.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.marks_needed_to_pass = dr["marks_needed_to_pass"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.passing_marks = dr["passing_marks"].ToString();
                            simsobj.balance_activity = dr["balance_activity"].ToString();
                            simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            simsobj.bal_act = dr["bal_act"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.activity_student_has = dr["activity_student_has"].ToString();
                            simsobj.sims_activity_points_gen = dr["sims_activity_points_gen"].ToString();
                            simsobj.points_from_other_than_general_gracing = dr["points_from_other_than_general_gracing"].ToString();
                            simsobj.distinct_sims_gb_cat_assign_enroll_number = dr["distinct_sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.distinct_sims_gb_subject_code = dr["distinct_sims_gb_subject_code"].ToString();
                            simsobj.total_student_pass_subject_count = dr["total_student_pass_subject_count"].ToString();
                            simsobj.sims_roll_number_sort_1 = dr["sims_roll_number_sort"].ToString();
                            simsobj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            simsobj.sims_sports_flag = dr["sims_sports_flag"].ToString();
                            simsobj.sims_gracing_type = dr["sims_gracing_type"].ToString();
                            simsobj.activities= new List<activity_list>();


                            try
                            {
                                simsobj.sims_roll_number_sort = int.Parse(new String(simsobj.sims_roll_number_sort_1.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            term_list.Add(simsobj);
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {
                            activity_list act = new activity_list();
                            act.sims_activity_number = dr["sims_activity_number"].ToString();
                            act.sims_activity_name = dr["sims_activity_name"].ToString();
                            act.sims_enrollment_number = dr["sims_enrollment_number"].ToString();

                            //GBRQ01 o = term_list.SingleOrDefault(q => q.sims_gb_cat_assign_enroll_number == act.sims_enrollment_number);
                           // var v = (from p in term_list where p.sims_gb_cat_assign_enroll_number == act.sims_enrollment_number select p);
                            var v = Enumerable.Range(0, term_list.Count)
                            .Where(i => term_list[i].sims_gb_cat_assign_enroll_number == act.sims_enrollment_number)
                            .ToList();

                            if (v.Count() > 0)
                            {
                               // var t = v;
                                foreach (var s in v) {
                                    term_list[s].activities.Add(act);
                                }
                                //if (o.activities == null) o.activities = new List<activity_list>();
                                //v.act                            }
                            }
                        }
                        dr.NextResult();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetsingleFailStudent")]
        public HttpResponseMessage GetsingleFailStudent(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string sims_gb_term_code,string sims_gb_cat_assign_enroll_number,string sims_activity_number)
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gracing_marks_transaction]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","SS"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_gb_term_code",sims_gb_term_code),
                            new SqlParameter("@sims_gb_cat_assign_enroll_number",sims_gb_cat_assign_enroll_number),
                            new SqlParameter("@sims_activity_number",sims_activity_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_symbol = dr["sims_activity_symbol"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_points = dr["sims_activity_points"].ToString();
                            simsobj.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.marks_needed_to_pass = dr["marks_needed_to_pass"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.passing_marks = dr["passing_marks"].ToString();
                            simsobj.balance_activity = dr["balance_activity"].ToString();
                            simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            simsobj.bal_act = dr["bal_act"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.distinct_sims_gb_cat_assign_enroll_number = dr["distinct_sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.distinct_sims_gb_subject_code = dr["distinct_sims_gb_subject_code"].ToString();
                            simsobj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();

                            try
                            {
                                simsobj.sims_roll_number_sort = int.Parse(new String(simsobj.sims_roll_number.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("getActivity")]
        public HttpResponseMessage getActivity()
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gracing_marks_transaction]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("InsertGraceMarks")]
        public HttpResponseMessage InsertGraceMarks(List<GBRQ01> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (GBRQ01 hrmsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_gracing_marks_transaction]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr","I"),
                                new SqlParameter("@sims_cur_code",hrmsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year",hrmsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code",hrmsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code",hrmsobj.sims_section_code),
                                new SqlParameter("@sims_enroll_number",hrmsobj.sims_enroll_number),
                                new SqlParameter("@sims_activity_number",hrmsobj.sims_activity_number),
                                new SqlParameter("@sims_subject_code",hrmsobj.sims_subject_code),
                                new SqlParameter("@sims_marks_obtain",hrmsobj.sims_marks_obtain),
                                new SqlParameter("@sims_marks_applied",hrmsobj.sims_marks_applied),
                                new SqlParameter("@sims_balance_activity_points",hrmsobj.sims_balance_activity_points),
                                new SqlParameter("@sims_activity_points_gen",hrmsobj.sims_activity_points_gen),
                                new SqlParameter("@points_from_other_than_general_gracing",hrmsobj.points_from_other_than_general_gracing),
                                new SqlParameter("@sims_passing_marks_of_student",hrmsobj.sims_passing_marks_of_student),
                                new SqlParameter("@user",hrmsobj.user),
                                new SqlParameter("@sims_gb_term_code",hrmsobj.sims_gb_term_code),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("GetFailStudentUD")]
        public HttpResponseMessage GetFailStudentUD(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string sims_gb_term_code)
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_gracing_marks_update_delete",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_gb_term_code",sims_gb_term_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_symbol = dr["sims_activity_symbol"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_points = dr["sims_activity_points"].ToString();
                            simsobj.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.marks_needed_to_pass = dr["marks_needed_to_pass"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.passing_marks = dr["passing_marks"].ToString();
                            simsobj.balance_activity = dr["balance_activity"].ToString();
                            simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            simsobj.bal_act = dr["bal_act"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.activity_student_has = dr["activity_student_has"].ToString();
                            simsobj.sims_activity_points_gen = dr["sims_activity_points_gen"].ToString();
                            simsobj.points_from_other_than_general_gracing = dr["points_from_other_than_general_gracing"].ToString();
                            simsobj.distinct_sims_gb_cat_assign_enroll_number = dr["distinct_sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.distinct_sims_gb_subject_code = dr["distinct_sims_gb_subject_code"].ToString();
                            simsobj.total_student_pass_subject_count = dr["total_student_pass_subject_count"].ToString();
                            simsobj.sims_roll_number_sort_1 = dr["sims_roll_number_sort"].ToString();
                            simsobj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            simsobj.activities = new List<activity_list>();


                            try
                            {
                                simsobj.sims_roll_number_sort = int.Parse(new String(simsobj.sims_roll_number_sort_1.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            term_list.Add(simsobj);
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {
                            activity_list act = new activity_list();
                            act.sims_activity_number = dr["sims_activity_number"].ToString();
                            act.sims_activity_name = dr["sims_activity_name"].ToString();
                            act.sims_enrollment_number = dr["sims_enrollment_number"].ToString();

                            //GBRQ01 o = term_list.SingleOrDefault(q => q.sims_gb_cat_assign_enroll_number == act.sims_enrollment_number);
                            // var v = (from p in term_list where p.sims_gb_cat_assign_enroll_number == act.sims_enrollment_number select p);
                            var v = Enumerable.Range(0, term_list.Count)
                            .Where(i => term_list[i].sims_gb_cat_assign_enroll_number == act.sims_enrollment_number)
                            .ToList();

                            if (v.Count() > 0)
                            {
                                // var t = v;
                                foreach (var s in v)
                                {
                                    term_list[s].activities.Add(act);
                                }
                                //if (o.activities == null) o.activities = new List<activity_list>();
                                //v.act                            }
                            }
                        }
                        dr.NextResult();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("IUpdateDeleteGrace")]
        public HttpResponseMessage IUpdateDeleteGrace(List<GBRQ01> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (GBRQ01 hrmsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_gracing_marks_update_delete",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr",hrmsobj.opr),
                                new SqlParameter("@sims_cur_code",hrmsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year",hrmsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code",hrmsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code",hrmsobj.sims_section_code),
                                new SqlParameter("@sims_enroll_number",hrmsobj.sims_enroll_number),
                                new SqlParameter("@sims_activity_number",hrmsobj.sims_activity_number),
                                new SqlParameter("@sims_subject_code",hrmsobj.sims_subject_code),
                                new SqlParameter("@sims_gb_term_code",hrmsobj.sims_gb_term_code),
                              });

                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("GetexcludeSub")]
        public HttpResponseMessage GetexcludeSub(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string sims_gb_term_code, string sims_gb_cat_assign_enroll_number, string sims_activity_number,string sims_subject_code,string sort_by)
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gracing_marks_transaction]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_gb_term_code",sims_gb_term_code),
                            new SqlParameter("@sims_gb_cat_assign_enroll_number",sims_gb_cat_assign_enroll_number),
                            new SqlParameter("@sims_activity_number",sims_activity_number),
                            new SqlParameter("@sims_subject_code",sims_subject_code),
                            new SqlParameter("@sort_by",sort_by),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_activity_number = dr["sims_activity_number"].ToString();
                            simsobj.sims_activity_name = dr["sims_activity_name"].ToString();
                            simsobj.sims_activity_symbol = dr["sims_activity_symbol"].ToString();
                            simsobj.sims_activity_max_point = dr["sims_activity_max_point"].ToString();
                            simsobj.sims_activity_max_point_subject = dr["sims_activity_max_point_subject"].ToString();
                            simsobj.sims_activity_priority = dr["sims_activity_priority"].ToString();
                            simsobj.sims_activity_points = dr["sims_activity_points"].ToString();
                            simsobj.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.marks_needed_to_pass = dr["marks_needed_to_pass"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.passing_marks = dr["passing_marks"].ToString();
                            simsobj.balance_activity = dr["balance_activity"].ToString();
                            simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            simsobj.bal_act = dr["bal_act"].ToString();
                            simsobj.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            simsobj.distinct_sims_gb_cat_assign_enroll_number = dr["distinct_sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.distinct_sims_gb_subject_code = dr["distinct_sims_gb_subject_code"].ToString();
                            simsobj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            simsobj.sims_activity_points_gen = dr["sims_activity_points_gen"].ToString();
                            simsobj.points_from_other_than_general_gracing = dr["points_from_other_than_general_gracing"].ToString();
                            simsobj.sims_sports_flag = dr["sims_sports_flag"].ToString();
                            simsobj.sims_gracing_type = dr["sims_gracing_type"].ToString();

                            try
                            {
                                simsobj.sims_roll_number_sort = int.Parse(new String(simsobj.sims_roll_number.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }


        [Route("getsortopt")]
        public HttpResponseMessage getsortopt()
        {
            List<GBRQ01> term_list = new List<GBRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gracing_marks_transaction]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }
    }
}