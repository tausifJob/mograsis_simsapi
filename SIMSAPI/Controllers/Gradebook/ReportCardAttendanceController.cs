﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Linq;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using System.Data;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/reportCardAttendance")]
    public class ReportCardAttendanceController : ApiController
    {
        [Route("GradeWithPara")]
        public HttpResponseMessage GradeWithPara(Sims028 pa)
        {
            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[report_card_attendance_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'G'),
                                new SqlParameter("@sims_cur_code", pa.sims_cur_code),
                                new SqlParameter("@sims_academic_year", pa.sims_academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("sectionCommon_p")]
        public HttpResponseMessage sectionCommon_p(Sims028 param)
        {            
            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[report_card_attendance_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_cur_code", param.sims_cur_code),
                                new SqlParameter("@sims_academic_year", param.sims_academic_year),
                                new SqlParameter("@sims_grade_code",param.sims_grade_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }


        [Route("GetAllterm")]
        public HttpResponseMessage GetAllterm(string cur_code, string academic_year)
        {
            List<admissionclasses> mod_list = new List<admissionclasses>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[report_card_attendance_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","T"),
                        new SqlParameter("@sims_cur_code", cur_code),
                        new SqlParameter("@sims_academic_year", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionclasses simsobj = new admissionclasses();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_term_start_date = db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                            simsobj.sims_term_end_date = db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("addReportCardAttendance")]
        public HttpResponseMessage CURDActivityExamMaster(string data1,List<reportCardAttend> data)
        {
            bool alert_status = false;
            reportCardAttend attObj = Newtonsoft.Json.JsonConvert.DeserializeObject<reportCardAttend>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[report_card_attendance_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@OPR",'D'),
                        new SqlParameter("@sims_grade_code", attObj.sims_grade_code),
                        new SqlParameter("@sims_cur_code", attObj.sims_cur_code),
                        new SqlParameter("@sims_academic_year", attObj.sims_academic_year),
                        new SqlParameter("@sims_section_code", attObj.sims_section_code),
                        new SqlParameter("@sims_enrollment_number", attObj.sims_enrollment_number),
                        new SqlParameter("@sims_term_code", attObj.sims_term_code)                      
                    });
                    if (dr1.RecordsAffected > 0)
                    {
                        alert_status = true;
                    }
                    
                    dr1.Close();

                    foreach (reportCardAttend obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[report_card_attendance_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_enrollment_number", obj.sims_enrollment_number),
                                new SqlParameter("@sims_term_code", obj.sims_term_code),
                                new SqlParameter("@sims_attendance_description", obj.sims_attendance_code),
                                new SqlParameter("@sims_attendance_cout", obj.sims_attendance_cout)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }



        //[Route("previewCardAttendance")]
        //public HttpResponseMessage previewCardAttendance(reportCardAttend obj)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            ds = db.ExecuteStoreProcedureDS("[sims].[report_card_attendance_proc]",
        //               new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr","P"),
        //                    new SqlParameter("@sims_cur_code",obj.sims_cur_code),
        //                    new SqlParameter("@sims_academic_year",obj.sims_academic_year),
        //                    new SqlParameter("@sims_grade_code",obj.sims_grade_code),
        //                    new SqlParameter("@sims_section_code",obj.sims_section_code),
        //                    new SqlParameter("@sims_start_date",obj.sims_term_start_date),
        //                    new SqlParameter("@sims_end_date",obj.sims_term_end_date),
        //                    new SqlParameter("@sims_enrollment_number", obj.sims_enrollment_number)                           
        //                 });

        //        }
        //    }
        //    catch (Exception x) {

        //        return Request.CreateResponse(HttpStatusCode.OK, ds);

        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, ds);

        //}


        [Route("previewCardAttendance")]
        public HttpResponseMessage previewCardAttendance(string section_code, reportCardAttend obj)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[report_card_attendance_proc]",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","P"),
                            new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                            //new SqlParameter("@sims_section_code",obj.sims_section_code),
                            new SqlParameter("@sims_start_date",db.DBYYYYMMDDformat(obj.sims_term_start_date)),
                            new SqlParameter("@sims_end_date",db.DBYYYYMMDDformat(obj.sims_term_end_date)),
                            new SqlParameter("@sims_enrollment_number", obj.sims_enrollment_number),
                            new SqlParameter("@sims_section_code", section_code==""?null:section_code)
                         });

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ds);

            }

            return Request.CreateResponse(HttpStatusCode.OK, ds);

        }


    }
}