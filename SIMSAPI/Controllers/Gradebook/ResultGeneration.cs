﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using System.Web;
using System.IO;
using System.Data;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/ResultGeneration")]
    [BasicAuthentication]
    public class ResultGenerationController : ApiController
    {
        [Route("sims_Cur")]
        public HttpResponseMessage sims_Cur()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc_filters",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","C")
                        });
                    ds.DataSetName = "Curr";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("sims_Academic")]
        public HttpResponseMessage sims_Academic(Dictionary<string, string> pr)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var v in pr)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + v.Key;
                        pr1.Value = pr[v.Key];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "A";
                    sp.Add(pr0);

                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc_filters", sp);
                    ds.DataSetName = "Academic";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("sims_Grd")]
        public HttpResponseMessage sims_Grd(Dictionary<string, string> pr)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var v in pr)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + v.Key;
                        pr1.Value = pr[v.Key];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "G";
                    sp.Add(pr0);

                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc_filters", sp);
                    ds.DataSetName = "Grade";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("sims_Sec")]
        public HttpResponseMessage sims_Sec(Dictionary<string, string> pr)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var v in pr)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + v.Key;
                        pr1.Value = pr[v.Key];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "S";
                    sp.Add(pr0);

                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc_filters", sp);
                    ds.DataSetName = "Section";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("sims_Term")]
        public HttpResponseMessage sims_Term(Dictionary<string, string> pr)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var v in pr)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + v.Key;
                        pr1.Value = pr[v.Key];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "T";
                    sp.Add(pr0);

                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc_filters", sp);
                    ds.DataSetName = "Term";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("sims_Generate")]
        public HttpResponseMessage sims_Generate(Dictionary<string, string> pr)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var v in pr)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + v.Key;
                        pr1.Value = pr[v.Key];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "G";
                    sp.Add(pr0);

                    DataSet ds = db.ExecuteStoreProcedureDS("sims.genrateResult_proc", sp);
                    ds.DataSetName = "Generate";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }
    }
}