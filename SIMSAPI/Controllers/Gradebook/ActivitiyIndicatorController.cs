﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/ActivityIndicator")]
    public class ActivitiyIndicatorController : ApiController
    {
        // GET: ActivitiyIndicator
        [Route("GetActivityGroup")]
        public HttpResponseMessage GetActivityGroup(string cur_code, string academic_year)
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_indicator_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","G"),
                        new SqlParameter("@sims_cur_code", cur_code),
                        new SqlParameter("@sims_academic_year", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.sims_activity_group_code = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_activity_group_desc = dr["sims_activity_group_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAllActivityGroupIndicator")]
        public HttpResponseMessage GetAllActivityGroupIndicator()
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_indicator_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_indicator_group_code = dr["sims_indicator_group_code"].ToString();
                            simsobj.sims_activity_group_code = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_indicator_group_short_name = dr["sims_indicator_group_short_name"].ToString();
                            simsobj.sims_indicator_group_name = dr["sims_indicator_group_name"].ToString();
                            simsobj.sims_indicator_group_remark = dr["sims_indicator_group_remark"].ToString();
                            simsobj.sims_indicator_group_value = dr["sims_indicator_group_value"].ToString(); 
                            simsobj.sims_activity_group_desc = dr["sims_activity_group_desc"].ToString();
                            simsobj.sims_indicator_group_desc = dr["sims_indicator_group_desc"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDActivityIndicatorGroup")]
        public HttpResponseMessage CURDActivityExamMaster(List<activityIndicator> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (activityIndicator obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_indicator_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_indicator_group_code", obj.sims_indicator_group_code),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_activity_group_code", obj.sims_activity_group_code),
                                new SqlParameter("@sims_indicator_group_short_name", obj.sims_indicator_group_short_name),
                                new SqlParameter("@sims_indicator_group_name", obj.sims_indicator_group_name),
                                new SqlParameter("@sims_indicator_group_desc", obj.sims_indicator_group_desc),                                
                                new SqlParameter("@sims_indicator_group_remark", obj.sims_indicator_group_remark),
                                new SqlParameter("@sims_indicator_group_value",obj.sims_indicator_group_value),
                                new SqlParameter("@sims_status", obj.sims_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }



        [Route("CURDReportCardBlock")]
        public HttpResponseMessage CURDReportCardBlock(string opr,string list,string type)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_block_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", opr),
                                new SqlParameter("@user_name", list),
                                new SqlParameter("@sims_type",type),
                               
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }

                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
          
        }


        [Route("GetReportCardBlock")]
        public HttpResponseMessage GetReportCardBlock()
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_block_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","B"),
                    
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.parent_name = dr["parent_name"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                           
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDReportCardBlockNew")]
        public HttpResponseMessage CURDReportCardBlockNew(sims_block obj)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_block_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@user_name", obj.list),
                                new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                                new SqlParameter("@sims_term_code",obj.sims_term_code),
                                new SqlParameter("@sims_created_by",obj.sims_created_by),
                                new SqlParameter("@sims_status",obj.sims_status),
//                                new SqlParameter("@sims_cur_code",obj.sims_cur_code),

                               
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        alert_status = true;
                    }
                    else
                    {
                        alert_status = false;
                    }
                    dr.Close();
                }

                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }

        }


        [Route("GetReportCardBlockNew")]
        public HttpResponseMessage GetReportCardBlockNew()
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_block_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","G"),
                    
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.sims_cur_code = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_acdemic_year = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_acdemic_year_code = dr["sims_academic_year"].ToString();

                            simsobj.class_nm = dr["class"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.stud_Name = dr["stud_Name"].ToString();
                            simsobj.sims_created_by = dr["sims_created_by"].ToString();
                            simsobj.sims_status_new = dr["sims_status"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }




    }
}