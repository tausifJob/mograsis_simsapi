﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
//using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/ActivityExamMaster")]
    public class ActivityExamMasterController : ApiController
    {
        // GET: ActivityExamMaster
        [Route("GetTerm_p")]
        public HttpResponseMessage GetTerm_p(string cur_code, string academic_year)
        {
            List<admissionclasses> mod_list = new List<admissionclasses>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_exam_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","T"),
                        new SqlParameter("@sims_cur_code", cur_code),
                        new SqlParameter("@sims_academic_year", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionclasses simsobj = new admissionclasses();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }
        //Get data
        [Route("GetAllActivityExamMaster")]
        public HttpResponseMessage GetAllActivityExamMaster()
        {
            List<activityExamMaster> mod_list = new List<activityExamMaster>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_exam_master_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityExamMaster simsobj = new activityExamMaster();
                            simsobj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_exam_code = dr["sims_exam_code"].ToString();
                            simsobj.sims_exam_term_code = dr["sims_exam_term_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_exam_marks_entry_start_date = db.UIDDMMYYYYformat(dr["sims_exam_marks_entry_start_date"].ToString());
                            simsobj.sims_exam_mark_entry_end_date = db.UIDDMMYYYYformat(dr["sims_exam_mark_entry_end_date"].ToString());
                            simsobj.sims_exam_status = dr["sims_exam_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_exam_desc = dr["sims_exam_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDActivityExamMaster")]
        public HttpResponseMessage CURDActivityExamMaster(List<activityExamMaster> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (activityExamMaster obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[activity_exam_master_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_exam_code", obj.sims_exam_code),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_exam_term_code", obj.sims_exam_term_code),                               
                                new SqlParameter("@sims_exam_marks_entry_start_date", db.DBYYYYMMDDformat(obj.sims_exam_marks_entry_start_date)),
                                new SqlParameter("@sims_exam_mark_entry_end_date", db.DBYYYYMMDDformat(obj.sims_exam_mark_entry_end_date)),
                                new SqlParameter("@sims_exam_desc", obj.sims_exam_desc),
                                new SqlParameter("@sims_exam_status", obj.sims_exam_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

    }
}