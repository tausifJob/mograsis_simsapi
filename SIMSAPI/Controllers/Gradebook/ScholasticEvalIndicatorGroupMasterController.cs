﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/ScholasticIndicatorGroup")]
    public class ScholasticEvalIndicatorGroupMasterController : ApiController
    {
        // GET: ActivitiyIndicator
        [Route("GetScholasticEvalGroup")]
        public HttpResponseMessage GetScholasticEvalGroup(string cur_code, string academic_year)
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_indicator_group_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","G"),
                        new SqlParameter("@sims_cur_code", cur_code),
                        new SqlParameter("@sims_academic_year", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.sims_scholastic_evaluation_group_code = dr["sims_scholastic_evaluation_group_code"].ToString();
                            simsobj.sims_scholastic_evaluation_group_desc = dr["sims_scholastic_evaluation_group_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAllScholasticEvalGroupIndicator")]
        public HttpResponseMessage GetAllScholasticEvalGroupIndicator()
        {
            List<activityIndicator> mod_list = new List<activityIndicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_indicator_group_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            activityIndicator simsobj = new activityIndicator();
                            simsobj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_indicator_group_code = dr["sims_indicator_group_code"].ToString();
                            simsobj.sims_scholastic_evaluation_group_code = dr["sims_scholastic_evaluation_group_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_indicator_group_short_name = dr["sims_indicator_group_short_name"].ToString();
                            simsobj.sims_indicator_group_name = dr["sims_indicator_group_name"].ToString();
                            simsobj.sims_indicator_group_remark = dr["sims_indicator_group_remark"].ToString();
                            simsobj.sims_indicator_group_value = dr["sims_indicator_group_value"].ToString();
                            simsobj.sims_scholastic_evaluation_group_desc = dr["sims_scholastic_evaluation_group_desc"].ToString();
                            simsobj.sims_indicator_group_desc = dr["sims_indicator_group_desc"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;

                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDScholasticEvalIndicatorGroup")]
        public HttpResponseMessage CURDScholasticEvalIndicatorGroup(List<activityIndicator> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (activityIndicator obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[scholastic_eval_indicator_group_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_indicator_group_code", obj.sims_indicator_group_code),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_scholastic_evaluation_group_code", obj.sims_scholastic_evaluation_group_code),
                                new SqlParameter("@sims_indicator_group_short_name", obj.sims_indicator_group_short_name),
                                new SqlParameter("@sims_indicator_group_name", obj.sims_indicator_group_name),
                                new SqlParameter("@sims_indicator_group_desc", obj.sims_indicator_group_desc),
                                new SqlParameter("@sims_indicator_group_remark", obj.sims_indicator_group_remark),
                                new SqlParameter("@sims_indicator_group_value",obj.sims_indicator_group_value),
                                new SqlParameter("@sims_status", obj.sims_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }
    }
}