﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

using SIMSAPI.Models.ERP.gradebookClass;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/AgendaCreationController")]
    public class AgendaCreationController : ApiController
    {
        #region Agenda Creation

        [Route("GetCur")]
        public HttpResponseMessage GetCur()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                            hrmsObj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("GetStage_name")]
        public HttpResponseMessage GetStage_name()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "AA")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_agenda_stage = dr["sims_stage_description"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAgenda_name")]
        public HttpResponseMessage GetAgenda_name()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Q")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_agenda_name = dr["sims_agenda_name"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("GetPageNO")]
        public HttpResponseMessage GetPageNO()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "AB")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_agenda_page_no = dr["sims_agenda_page_no"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }



        [Route("GetAgendaDoc")]
        public HttpResponseMessage GetAgendaDoc()
        {
            Sims527 obj = new Sims527();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "L")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            obj.oldpath = dr["oldPath"].ToString();
                            obj.newpath = dr["newPath"].ToString();
                            obj.ftpusername = dr["FTPUsername"].ToString();
                            obj.ftppassword = dr["FTPpassword"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);

        }

        [Route("GetTextFormat")]
        public HttpResponseMessage GetTextFormat()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "H")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 obj = new Sims527();
                            obj.file_format = dr["sims_appl_parameter"].ToString();


                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAca")]
        public HttpResponseMessage GetAca(string cur)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", cur)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_acaedmic_year = dr["sims_academic_year"].ToString();
                            hrmsObj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getgrd")]
        public HttpResponseMessage Getgrd(string cur, string aca, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_acaedmic_year", aca),
                            new SqlParameter("@sims_login_code", user)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                            hrmsObj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getsec")]
        public HttpResponseMessage Getsec(string cur, string aca, string grd, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_acaedmic_year", aca),
                            new SqlParameter("@sims_grade_code", grd),
                            new SqlParameter("@sims_login_code", user)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                            hrmsObj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetCurByUser")]
        public HttpResponseMessage GetCurByUser(string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                            new SqlParameter("@sims_login_code", user)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                            hrmsObj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }



        [Route("GetSubject")]
        public HttpResponseMessage GetSubject(string cur, string aca, string grd, string sec, string user)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_acaedmic_year", aca),
                            new SqlParameter("@sims_grade_code", grd),
                            new SqlParameter("@sims_section_code", sec),
                            new SqlParameter("@sims_login_code", user)
                        });

                    while (dr.Read())
                    {
                        Sims527 hrmsObj = new Sims527();
                        hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                        hrmsObj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        //[HttpPost(), Route("upload")]
        //public string UploadFiles(string filename, string location)
        //    {
        //        int iUploadedCnt = 0;

        //        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
        //        string sPath = ""; string fname = ""; string file = ""; string root = "";

        //        //sPath = WebConfigurationManager.AppSettings["webPath"];

        //        string path = location;

        //  //  $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Agenda/' + str;

        //        root = HttpContext.Current.Server.MapPath(path);


        //        Directory.CreateDirectory(root);

        //        // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

        //        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        //        // CHECK THE FILE COUNT.

        //        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        //        {
        //            System.Web.HttpPostedFile hpf = hfc[iCnt];

        //            if (hpf.ContentLength > 0)
        //            {
        //                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
        //                    string type = Path.GetFileName(hpf.ContentType);
        //                    file = filename;
        //                    hpf.SaveAs((root + filename ));
        //                    fname = Path.GetFileName(filename);
        //                    iUploadedCnt = iUploadedCnt + 1;
        //            }
        //        }

        //        // RETURN A MESSAGE (OPTIONAL).
        //        if (iUploadedCnt > 0)
        //        {
        //            return file;
        //        }
        //        else
        //        {
        //            return "false";
        //        }
        //    }


        [Route("AgendaDetails")]
        public HttpResponseMessage AgendaDetails(Sims527 obj)
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", obj.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                            new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                            new SqlParameter("@FromDate", db.DBYYYYMMDDformat(obj.from_date.ToString())),
                            new SqlParameter("@ToDate", db.DBYYYYMMDDformat(obj.to_date.ToString())),
                            new SqlParameter("@sims_login_code", obj.user_code)
                        //db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                        });

                    while (dr.Read())
                    {
                        Sims527 hrmsObj = new Sims527();
                        hrmsObj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        hrmsObj.sims_acaedmic_year = dr["sims_acaedmic_year"].ToString();
                        hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                        hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                        hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                        try
                        {
                            hrmsObj.sims_agenda_from_time = dr["sims_agenda_from_time"].ToString();
                            hrmsObj.sims_agenda_to_time = dr["sims_agenda_to_time"].ToString();
                            hrmsObj.sims_agenda_page_no = dr["sims_agenda_page_no"].ToString();
                            hrmsObj.sims_agenda_stage = dr["sims_agenda_stage"].ToString();
                        }
                        catch (Exception ex) { }

                        //hrmsObj.sims_agenda_date = DateTime.Parse(dr["AllDays"].ToString()).ToShortDateString();
                        hrmsObj.sims_agenda_date = db.UIDDMMYYYYformat(dr["AllDays"].ToString());
                        if (dr["sims_agenda_visible_to_parent_portal"].ToString() == "Y")
                            hrmsObj.sims_agenda_visible_to_parent_portal = true;
                        else
                            hrmsObj.sims_agenda_visible_to_parent_portal = false;
                        hrmsObj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        hrmsObj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                        hrmsObj.loading_ind = true;
                        hrmsObj.sims_doc = AgendaDetailsDoc(hrmsObj.sims_agenda_number);
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        public List<Sims527_doc> AgendaDetailsDoc(string agenda_number)
        {
            List<Sims527_doc> mod_list = new List<Sims527_doc>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'M'),
                   new SqlParameter("@sims_agenda_number", agenda_number),
                

              });

                    while (dr.Read())
                    {
                        Sims527_doc hrmsObj = new Sims527_doc();
                        hrmsObj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        hrmsObj.sims_agenda_doc_line_no = dr["sims_agenda_doc_line_no"].ToString();
                        hrmsObj.sims_agenda_doc_name = dr["sims_agenda_doc_name"].ToString();
                        hrmsObj.sims_agenda_doc_name_en = dr["sims_agenda_doc_name_en"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return mod_list;

        }

        [Route("AgendaUpdate")]
        public HttpResponseMessage AgendaUpdate(List<Sims527> testlist)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //string abc = DateTime.Parse(item.sims_agenda_date).Year + "-" + DateTime.Parse(item.sims_agenda_date).Month + "-" + DateTime.Parse(item.sims_agenda_date).Day;
                        string abc = db.DBYYYYMMDDformat(item.sims_agenda_date.ToString());
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", item.sims_grade_code),
                            new SqlParameter("@sims_section_code", item.sims_section_code),
                            new SqlParameter("@sims_subject_code", item.sims_subject_code),
                            new SqlParameter("@sims_agenda_number", item.sims_agenda_number),
                            new SqlParameter("@sims_agenda_date", abc),
                            new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                            new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                            new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                            new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal?'Y':'N'),
                            new SqlParameter("@sims_agenda_doc", item.file),
                        });
                        while (dr.Read())
                        {
                            chk = true;
                            item.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        }

                        bool b = AgendaUpdateDoc(item.sims_doc, item);

                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }


        [Route("AgendaUpdateGeis")]
        public HttpResponseMessage AgendaUpdateGeis(List<Sims527> testlist)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //string abc = DateTime.Parse(item.sims_agenda_date).Year + "-" + DateTime.Parse(item.sims_agenda_date).Month + "-" + DateTime.Parse(item.sims_agenda_date).Day;
                        string abc = db.DBYYYYMMDDformat(item.sims_agenda_date.ToString());
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", item.sims_grade_code),
                            new SqlParameter("@sims_section_code", item.sims_section_code),
                            new SqlParameter("@sims_subject_code", item.sims_subject_code),
                            new SqlParameter("@sims_agenda_number", item.sims_agenda_number),
                            new SqlParameter("@sims_agenda_date", abc),
                            new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                            new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                            new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                            new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal?'Y':'N'),
                            new SqlParameter("@sims_agenda_doc", item.file),

                            new SqlParameter("@sims_agenda_from_time", item.sims_agenda_from_time),
                            new SqlParameter("@sims_agenda_to_time", item.sims_agenda_to_time),
                            new SqlParameter("@sims_agenda_page_no", item.sims_agenda_page_no),
                            new SqlParameter("@sims_agenda_stage", item.sims_agenda_stage),

                        });
                        while (dr.Read())
                        {
                            chk = true;
                            item.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        }

                        bool b = AgendaUpdateDoc(item.sims_doc, item);

                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }


        public bool AgendaUpdateDoc(List<Sims527_doc> testlist, Sims527 test)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@sims_cur_code", test.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", test.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", test.sims_grade_code),
                            new SqlParameter("@sims_section_code", test.sims_section_code),
                            new SqlParameter("@sims_subject_code", test.sims_subject_code),
                            new SqlParameter("@sims_agenda_number", test.sims_agenda_number),
                            new SqlParameter("@sims_doc_line", item.sims_agenda_doc_line_no),
                            new SqlParameter("@sims_doc_name", item.sims_agenda_doc_name),
                            new SqlParameter("@sims_doc_name_en", item.sims_agenda_doc_name_en),
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }

            return chk;

        }

        #region MultiAgenda

        [Route("MultSec")]
        public HttpResponseMessage MultSec(List<Sims527> list)
        {
            string cur = "", aca = "", grd = "", user = "";
            foreach (var item in list)
            {
                cur = item.sims_cur_code;
                aca = item.sims_acaedmic_year;
                grd = grd + item.sims_grade_code + "/";
                user = item.user_code;
            }
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                            hrmsObj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("MultSub")]
        public HttpResponseMessage MultSub(List<Sims527> list)
        {
            string aca = "", sec = "", user = "";
            foreach (var item in list)
            {
                aca = item.sims_acaedmic_year;
                sec = sec + item.sims_section_code + "/";
                user = item.user_code;
            }
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Y"),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code",sec),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 hrmsObj = new Sims527();
                            hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                            hrmsObj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("AgendaUpdateMult")]
        public HttpResponseMessage AgendaUpdateMult(List<Sims527> testlist)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //string abc = DateTime.Parse(item.sims_agenda_date).Year + "-" + DateTime.Parse(item.sims_agenda_date).Month + "-" + DateTime.Parse(item.sims_agenda_date).Day;
                        string abc = db.DBYYYYMMDDformat(item.sims_agenda_date.ToString());
                        //db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                             
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", item.sims_grade_code),
                            new SqlParameter("@sims_section_code", item.sims_section_code),
                            new SqlParameter("@sims_subject_code", item.sims_subject_code),
                            new SqlParameter("@sims_agenda_date", abc),
                            new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                            new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                            new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                            new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal?'Y':'N'),
                            new SqlParameter("@sims_agenda_doc", item.file),

              });
                        while (dr.Read())
                        {
                            chk = true;
                            item.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        }

                        bool b = AgendaUpdateDocMult(item.sims_doc, item);

                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }

        [Route("AgendaUpdateMultAhis")]
        public HttpResponseMessage AgendaUpdateMultAhis(List<Sims527> testlist)
        {
            bool chk = false;
            try
            {
                //foreach (var item in testlist)
                //{
                //    using (DBConnection db = new DBConnection())
                //    {
                //        db.Open();
                //        DateTime abc = DateTime.Parse( DBYYYYMMDDformat(item.sims_agenda_date.ToString()));
                //        DateTime abc1 = DateTime.Parse(DBYYYYMMDDformat(item.sims_agenda_dateto.ToString()));
                //        while (abc <= abc1) {
                //            var t = abc;
                //           abc=abc.AddDays(1);

                //        }

                //    }
                //}



                foreach (var item in testlist)
                {

                    DateTime abc = DateTime.Parse(DBYYYYMMDDformat(item.sims_agenda_date.ToString()));
                    DateTime abc1 = DateTime.Parse(DBYYYYMMDDformat(item.sims_agenda_dateto.ToString()));
                    while (abc <= abc1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            //string abc = DateTime.Parse(item.sims_agenda_date).Year + "-" + DateTime.Parse(item.sims_agenda_date).Month + "-" + DateTime.Parse(item.sims_agenda_date).Day;
                            // string abc = db.DBYYYYMMDDformat(item.sims_agenda_date.ToString());
                            //db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                                new List<SqlParameter>() 
                         { 
                             
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", item.sims_grade_code),
                            new SqlParameter("@sims_section_code", item.sims_section_code),
                            new SqlParameter("@sims_subject_code", item.sims_subject_code),
                            new SqlParameter("@sims_agenda_date", abc),
                            new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                            new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                            new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                            new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal?'Y':'N'),
                            new SqlParameter("@sims_agenda_doc", item.file),

                             });
                            while (dr.Read())
                            {
                                chk = true;
                                item.sims_agenda_number = dr["sims_agenda_number"].ToString();
                            }

                            bool b = AgendaUpdateDocMult(item.sims_doc, item);

                            dr.Close();
                        }
                        abc = abc.AddDays(1);
                    }

                }


            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }




        [Route("AgendaUpdateMult_geis")]
        public HttpResponseMessage AgendaUpdateMult_geis(List<Sims527> testlist)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //string abc = DateTime.Parse(item.sims_agenda_date).Year + "-" + DateTime.Parse(item.sims_agenda_date).Month + "-" + DateTime.Parse(item.sims_agenda_date).Day;
                        string abc = db.DBYYYYMMDDformat(item.sims_agenda_date.ToString());
                        //db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                             
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", item.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", item.sims_grade_code),
                            new SqlParameter("@sims_section_code", item.sims_section_code),
                            new SqlParameter("@sims_subject_code", item.sims_subject_code),
                            new SqlParameter("@sims_agenda_date", abc),
                            new SqlParameter("@sims_agenda_name", item.sims_agenda_name),
                            new SqlParameter("@sims_agenda_desc", item.sims_agenda_desc),
                            new SqlParameter("@sims_agenda_teacher_code", item.sims_agenda_teacher_code),
                            new SqlParameter("@sims_agenda_visible_to_parent_portal", item.sims_agenda_visible_to_parent_portal?'Y':'N'),
                            new SqlParameter("@sims_agenda_doc", item.file),

                            new SqlParameter("@sims_agenda_from_time", item.sims_agenda_from_time),
                            new SqlParameter("@sims_agenda_to_time", item.sims_agenda_to_time),
                            new SqlParameter("@sims_agenda_page_no", item.sims_agenda_page_no),
                            new SqlParameter("@sims_agenda_stage", item.sims_agenda_stage),


              });
                        while (dr.Read())
                        {
                            chk = true;
                            item.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        }

                        bool b = AgendaUpdateDocMult(item.sims_doc, item);

                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }



        public bool AgendaUpdateDocMult(List<Sims527_doc> testlist, Sims527 test)
        {
            bool chk = false;
            try
            {
                foreach (var item in testlist)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_cur_code", test.sims_cur_code),
                            new SqlParameter("@sims_acaedmic_year", test.sims_acaedmic_year),
                            new SqlParameter("@sims_grade_code", test.sims_grade_code),
                            new SqlParameter("@sims_section_code", test.sims_section_code),
                            new SqlParameter("@sims_subject_code", test.sims_subject_code),
                            new SqlParameter("@sims_agenda_number", test.sims_agenda_number),
                            new SqlParameter("@sims_doc_line", item.sims_agenda_doc_line_no),
                            new SqlParameter("@sims_doc_name", item.sims_agenda_doc_name),
                            new SqlParameter("@sims_doc_name_en", item.sims_agenda_doc_name_en),
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }

            return chk;

        }

        [Route("AgendaGetDetails")]
        public HttpResponseMessage AgendaGetDetails(List<Sims527> list)
        {
            string sub = "", sec = "", usr = "", dte = "";
            foreach (var item in list)
            {
                sub = item.sims_subject_code;
                sec = sec + item.sims_section_code + "/";
                usr = item.user_code;
                //dte = DateTime.Parse(item.sims_agenda_date).Year.ToString() + '-' + DateTime.Parse(item.sims_agenda_date).Month.ToString() + '-' + DateTime.Parse(item.sims_agenda_date).Day.ToString();
                dte = item.sims_agenda_date.ToString();
            }
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@sims_grade_code",sec),
                            new SqlParameter("@sims_subject_code", sub),
                            new SqlParameter("@sims_agenda_date", db.DBYYYYMMDDformat(dte.ToString())),
                            new SqlParameter("@sims_login_code", usr)
                        });

                    while (dr.Read())
                    {
                        Sims527 hrmsObj = new Sims527();
                        hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                        hrmsObj.sims_acaedmic_year = dr["sims_acaedmic_year"].ToString();
                        hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                        hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                        hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                        hrmsObj.sims_agenda_date = dr["sims_agenda_date"].ToString();
                        hrmsObj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        hrmsObj.sims_grade_name = dr["class_code"].ToString();
                        hrmsObj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                        if (dr["sims_agenda_visible_to_parent_portal"].ToString() == "Y")
                            hrmsObj.sims_agenda_visible_to_parent_portal = true;
                        else
                            hrmsObj.sims_agenda_visible_to_parent_portal = false;
                        hrmsObj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        hrmsObj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                        hrmsObj.loading_ind = false;
                        hrmsObj.sims_agenda_teacher_code = dr["sims_agenda_teacher_code"].ToString();
                        hrmsObj.sims_doc = AgendaDetailsDoc(hrmsObj.sims_agenda_number);
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("UDeleteAgendaDoc")]
        public HttpResponseMessage UDeleteAgendaDoc(string agenda_number)
        {
            bool chk = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@sims_agenda_number", agenda_number),
                          });
                    if (dr.RecordsAffected > 0)
                    {
                        chk = true;
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }

        #endregion

        

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string newloc, string oldloc, string uname, string pass)
        {

            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = "";
            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + newloc;

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            //HttpFileCollection hfc = context.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        string type = Path.GetFileName(hpf.ContentType);
                        file = filename;
                        hpf.SaveAs((root + filename));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            string sourcefilepath = root + filename;
            string ftpusername = "";
            string ftppassword = "";
            if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "abps")
            {
                ftpusername = "FTP_USR_API"; // e.g. username
                ftppassword = "f^4Uy61i"; // e.g. password
            }
            else
            {
                ftpusername = uname; // e.g. username
                ftppassword = pass; // e.g. password
            }
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")

                if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "abps")
                {
                    serverpath = "ftp://api.mograsys.com/api.mograsys.com/kindoapi/content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Docs/Results/";
                }
                else
                {

                    serverpath = oldloc;//"ftp://45.114.142.68/Docs/Results/";


                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }


            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

         [HttpPost(), Route("uploadNew")]
        public void ProcessRequest(HttpContext context)
        {
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            int sdd = hfc.Count;
            string filename = "";
            string url = context.Request.UrlReferrer.AbsolutePath.ToLower();
            var folderPath = "~\\Uploads\\";
            //var folderPath = "http://api.mograsys.com//kindoapi//Content//sims//Images//";
            //if (url.Contains("a.aspx")) { folderPath = "~/uploads/"; }
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else { fname = file.FileName; }
                    filename = fname;
                    string extension = filename.Split(new char[] { '.' })[1];
                    string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg", "Tiff", "Tif", "pdf", "doc", "docx", "xls", "xlsx", "txt" };
                    bool isValidFile = false;
                    for (int j = 0; j < validFileTypes.Length; j++)
                    {
                        if (extension == validFileTypes[j])
                        {
                            isValidFile = true;
                            break;
                        }
                    }
                    if (!file.InputStream.CanRead)
                    {
                        isValidFile = false;
                    }

                    if (file.ContentLength < 256 && file.ContentLength > 10485760)
                    {
                        isValidFile = false;
                    }

                    if (file.ContentType.ToLower() != "image/jpg" &&
                        file.ContentType.ToLower() != "image/jpeg" &&
                        file.ContentType.ToLower() != "image/pjpeg" &&
                        file.ContentType.ToLower() != "image/gif" &&
                        file.ContentType.ToLower() != "image/x-png" &&
                        file.ContentType.ToLower() != "image/png" &&
                        file.ContentType.ToLower() != "application/pdf" &&
                        file.ContentType.ToLower() != "application/msword" &&
                        file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" &&
                        file.ContentType.ToLower() != "application/vnd.ms-excel" &&
                        file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                        file.ContentType.ToLower() != "text/plain"
                        )
                    {
                        isValidFile = false;
                    }

                    if (isValidFile)
                    {
                        string[] testPath = filename.Split(new char[] { '\\' });
                        string directoryPath = folderPath + testPath[0];// +"\\" + testPath[1];
                        fname = Path.Combine(context.Server.MapPath(folderPath), fname);
                        bool exists = System.IO.Directory.Exists(context.Server.MapPath(directoryPath));
                        if (!exists) { System.IO.Directory.CreateDirectory(context.Server.MapPath(directoryPath)); }
                        string path = HttpContext.Current.Server.MapPath(directoryPath);
                        string newFile = "\\file" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + extension;
                        string newFilePath = directoryPath + newFile;
                        filename = path + newFile;
                        file.SaveAs(filename);
                        filename = newFilePath;
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(filename);
                    }
                    else
                    {
                        context.Response.Write("");
                    }

                }
            }
        }

        public string DBYYYYMMDDformat(string dt)
        {
            var uigetdate = dt;
            string dbgetformatDate = null;
            if (uigetdate == null || uigetdate == "" || uigetdate == "N/A")
            {
                return dbgetformatDate;
            }
            else
            {
                var day = uigetdate.Split('-')[0];
                var month = uigetdate.Split('-')[1];
                var year = uigetdate.Split('-')[2];

                dbgetformatDate = year + "-" + month + "-" + day;


            }
            return dbgetformatDate;
        }


        [Route("GetAgendaFileName")]
        public HttpResponseMessage GetAgendaFileName()
        {
            List<Sims527> mod_list = new List<Sims527>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_agenda_new_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "H")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims527 obj = new Sims527();
                            obj.file_format = dr["sims_appl_parameter"].ToString();


                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        #endregion

    }
}