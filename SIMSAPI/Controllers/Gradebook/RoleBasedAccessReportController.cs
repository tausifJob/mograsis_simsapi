﻿using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/RoleBasedAccessReport")]
    public class RoleBasedAccessReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                  
        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGradesNew")]
        public HttpResponseMessage getAllGradesNew(string cur_code, string academic_year ,string user)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[RoleBasedAccessReportController_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                                new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeNew")]
        public HttpResponseMessage getSectionFromGradeNew(string cur_code, string grade_code, string academic_year,string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[RoleBasedAccessReportController_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code),
                                  new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllTerm")]
        public HttpResponseMessage getAllTerm(string cur_code, string academic_year)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<RBAR01> term_list = new List<RBAR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[RoleBasedAccessReportController_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "U"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RBAR01 simsobj = new RBAR01();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }

        [Route("getRoleBasedAccessReport")]
        public HttpResponseMessage getRoleBasedAccessReport(string cur_code, string academic_year, string grade_code, string section_code,string term_code,string user)
        {
            List<RBAR01> lstCuriculum = new List<RBAR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[RoleBasedAccessReportController_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@term_code", term_code),
                             new SqlParameter ("@emp_code",user)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RBAR01 sequence = new RBAR01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class_name = dr["class_name"].ToString();
                            sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sequence.sims_subject_code = dr["sims_subject_code"].ToString();
                            
                            sequence.report_call = dr["report_call"].ToString();
                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            sequence.sims_term_desc_en = dr["sims_term_desc_en"].ToString();


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getReportFromProc")]
        public HttpResponseMessage getReportFromProc(string report_call)
        {
            List<RBAR01> lstCuriculum = new List<RBAR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[RoleBasedAccessReportController_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'Q'),
                             new SqlParameter("@rpt_parameter", report_call),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RBAR01 sequence = new RBAR01();
                            sequence.sims_appl_form_field = dr["sims_appl_form_field"].ToString();
                           
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        
        [Route("get_Status_all")]
        public HttpResponseMessage get_Status_all()

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : get_Status_all(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<ILDV01> term_list = new List<ILDV01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[Invs].[InventoryLoanDetailView_Rpt]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "U")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ILDV01 simsobj = new ILDV01();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.status_desc = dr["status_desc"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }


        [Route("getInventoryLoanDetailViewReport")]
        public HttpResponseMessage getInventoryLoanDetailViewReport(string from_date, string to_date, string doc_status)
        {
            List<ILDV01> lstCuriculum = new List<ILDV01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[Invs].[InventoryLoanDetailView_Rpt]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'S'),
                             new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                             new SqlParameter("@doc_status", doc_status)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ILDV01 sequence = new ILDV01();
                            sequence.doc_prov_no = dr["doc_prov_no"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.em_designation = dr["em_designation"].ToString();
                            sequence.doc_prov_date = dr["doc_prov_date"].ToString();
                            sequence.status_desc = dr["material_status"].ToString();
                            sequence.requested_by = dr["requested_by"].ToString();
                            sequence.requested_mob = dr["requested_mob"].ToString();
                            sequence.requested_email = dr["requested_email"].ToString();
                            sequence.doc_no = dr["doc_no"].ToString();
                            sequence.doc_status = dr["doc_status"].ToString();
                            sequence.cus_account_no = dr["cus_account_no"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getReportFromProc1")]
        public HttpResponseMessage getReportFromProc1(string doc_prov_no)
        {
            List<ILDV01> lstCuriculum = new List<ILDV01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[Invs].[InventoryLoanDetailView_Rpt]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'Q')                            


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ILDV01 sequence = new ILDV01();
                            sequence.sims_appl_form_field = dr["sims_appl_form_field"].ToString();

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


    }
}