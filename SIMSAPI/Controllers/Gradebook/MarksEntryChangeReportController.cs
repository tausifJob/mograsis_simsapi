﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.gradebookClass;


namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/MarksEntryChangeReport")]
    [BasicAuthentication]
    public class MarksEntryChangeReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getSubjectsFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string academic_year, string grade_code, string section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarksEntryChangeReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_grade_code", grade_code),
                                new SqlParameter("@sims_section_code", section_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getStudentMarksList")]
        public HttpResponseMessage getStudentMarksList(string cur_code, string academic_year, string grade_code, string section_code, string subject_code,string term_code,string cat_code,string assign_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<assessment_student> grade_list = new List<assessment_student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarksEntryChangeReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_grade_code", grade_code),
                                new SqlParameter("@sims_section_code", section_code),
                                new SqlParameter("@sims_gb_subject_code", subject_code),
                                new SqlParameter("@sims_term_code", term_code),
                                new SqlParameter("@sims_gb_cat_code", cat_code),
                                new SqlParameter("@sims_gb_cat_assign_number", assign_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            assessment_student simsobj = new assessment_student();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            simsobj.student_full_Name = dr["Stud_Full_Name"].ToString();
                            simsobj.grade_section_name = dr["grade_section_name"].ToString();
                            simsobj.sims_subject_code = dr["sims_gb_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            simsobj.sims_assignment_name = dr["sims_gb_cat_assign_name"].ToString();
                            simsobj.new_marks = dr["new_marks"].ToString();
                            simsobj.old_marks = dr["old_marks"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getcategory")]
        public HttpResponseMessage getcategory(string cur_code, string academic_year, string grade_code, string section_code, string subject_code,string term_code,string opr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<assessment_student> grade_list = new List<assessment_student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarksEntryChangeReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", opr),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_grade_code", grade_code),
                                new SqlParameter("@sims_section_code", section_code),
                                new SqlParameter("@sims_gb_subject_code", subject_code),
                                new SqlParameter("@sims_term_code", term_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            assessment_student simsobj = new assessment_student();
                            simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getcategoryassignment")]
        public HttpResponseMessage getcategoryassignment(string cur_code, string academic_year, string grade_code, string section_code, string subject_code,string category, string term_code,string opr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<assessment_student> grade_list = new List<assessment_student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarksEntryChangeReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", opr),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_grade_code", grade_code),
                                new SqlParameter("@sims_section_code", section_code),
                                new SqlParameter("@sims_gb_subject_code", subject_code),
                                new SqlParameter("@sims_gb_cat_code", category),
                                new SqlParameter("@sims_term_code", term_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            assessment_student simsobj = new assessment_student();
                            simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            simsobj.sims_assignment_name = dr["sims_gb_cat_assign_name"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getterm")]
        public HttpResponseMessage getterm(string cur_code,string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<RCstudent_report_card_comment> grade_list = new List<RCstudent_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.MarksEntryChangeReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RCstudent_report_card_comment simsobj = new RCstudent_report_card_comment();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }
    }
}