﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

using SIMSAPI.Models.ERP.gradebookClass;
using System.Web;
using System.IO;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/Trait")]
    public class TraitController : ApiController
    {


        [Route("getSubjectsTraits")]
        public HttpResponseMessage getSubjectsTraits()
        {
            List<SubTrt> mod_list = new List<SubTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubTrt hrmsObj = new SubTrt();
                            hrmsObj.sims_subject_trait_code = dr["sims_appl_parameter"].ToString();
                            hrmsObj.sims_subject_trait_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getAllSubjectsTraits")]
        public HttpResponseMessage getAllSubjectsTraits()
        {
            List<SubTrt> mod_list = new List<SubTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubTrt hrmsObj = new SubTrt();
                            hrmsObj.sims_cur_code = dr["sims_cur_code"].ToString();
                            hrmsObj.sims_cur_desc = dr["sims_cur_name"].ToString();
                            hrmsObj.sims_academic_year = dr["sims_academic_year"].ToString();
                            hrmsObj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            hrmsObj.sims_term_code = dr["sims_term_code"].ToString();
                            hrmsObj.sims_term_desc = dr["sims_term_desc"].ToString();
                            hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                            hrmsObj.sims_grade_name = dr["sims_grade_name"].ToString();
                            hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                            hrmsObj.sims_section_name = dr["sims_section_name"].ToString();
                            hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                            hrmsObj.sims_subject_name_en = dr["sims_subject_name"].ToString();
                            hrmsObj.sims_trait_code = dr["sims_trait_code"].ToString();
                            hrmsObj.sims_trait_name = dr["sims_trait_name"].ToString();
                            hrmsObj.trait_status = dr["sims_status"].ToString().Equals("A") ? true : false;

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("getSubjects")]
        public HttpResponseMessage getSubjects()
        {
            List<SubTrt> mod_list = new List<SubTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubTrt hrmsObj = new SubTrt();
                            hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                            hrmsObj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("getSubjectsSectionwise")]
        public HttpResponseMessage getSubjectsSectionwise(string cur,string acad,string grade,string section)
        {
            List<SubTrt> mod_list = new List<SubTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", acad),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", section),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubTrt hrmsObj = new SubTrt();
                            hrmsObj.sims_subject_code = dr["sims_subject_code"].ToString();
                            hrmsObj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("getAllterm")]
        public HttpResponseMessage getAllterm(string cur_code ,string acadmic_year)
        {
            List<Sims179> mod_list = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_academic_year",acadmic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            mod_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("CUDTrait")]
        public HttpResponseMessage CUDTrait(List<SubTrt> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SubTrt simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_trait_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr","I"),
                                new SqlParameter("@Sr_No",""),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_term_code",simsobj.sims_term_code),
                                new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@sims_trait_code",""),
                                new SqlParameter("@sims_trait_name", simsobj.sims_trait_name),
                                new SqlParameter("@sims_status", simsobj.trait_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("TraitDelete")]
        public HttpResponseMessage TraitDelete(List<SubTrt> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SubTrt simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_trait_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr","D"),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_term_code",simsobj.sims_term_code),
                                new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@sims_trait_code",simsobj.sims_trait_code)
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {

            List<SubTrt> grade_list = new List<SubTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubTrt simsobj = new SubTrt();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }


        //[Route("getGradebooks")]
        //public HttpResponseMessage getGradebooks(string subject_code)
        //{
        //    List<ExRefM> mod_list = new List<ExRefM>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //        new SqlParameter("@opr", "A"),
        //          new SqlParameter("@GB_REF_SUBJECT_CODE",subject_code)
        //        });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    ExRefM hrmsObj = new ExRefM();
        //                    hrmsObj.sims_gb_number = dr["sims_gb_number"].ToString();
        //                    hrmsObj.sims_gb_name = dr["sims_gb_name"].ToString();

        //                    mod_list.Add(hrmsObj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}


        //[Route("getGradebookCategory")]
        //public HttpResponseMessage getGradebookCategory()
        //{
        //    List<ExRefM> mod_list = new List<ExRefM>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                new SqlParameter("@opr", "C"),

        //        });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    ExRefM hrmsObj = new ExRefM();
        //                    hrmsObj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
        //                    hrmsObj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();

        //                    mod_list.Add(hrmsObj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        //[Route("getGradebookAssignment")]
        //public HttpResponseMessage getGradebookAssignment()
        //{
        //    List<ExRefM> mod_list = new List<ExRefM>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //        new SqlParameter("@opr", "E")
        //        });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    ExRefM hrmsObj = new ExRefM();
        //                    hrmsObj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
        //                    hrmsObj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();

        //                    mod_list.Add(hrmsObj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        //[Route("CUDExamRefMaterial")]
        //public HttpResponseMessage insert_Assignment(List<ExRefM> data)
        //{

        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
        //             new List<SqlParameter>()
        //                {
        //                  new SqlParameter("@opr","I"),
        //                  new SqlParameter("@GB_REF_NUMBER",""),
        //                  new SqlParameter("@CUR_CODE",data[0].sims_cur_code),
        //                  new SqlParameter("@ACADEMIC_YEAR", data[0].sims_academic_year),
        //                  new SqlParameter("@GRADE_CODE",data[0].sims_grade_code),
        //                  new SqlParameter("@SECTION_CODE", data[0].sims_section_code),
        //                  new SqlParameter("@GB_REF_GB_NUMBER",data[0].sims_gb_number),
        //                  new SqlParameter("@GB_REF_SUBJECT_CODE", data[0].sims_subject_code),
        //                  new SqlParameter("@GB_REF_CAT_CODE",data[0].sims_gb_cat_code),
        //                  new SqlParameter("@GB_REF_CAT_ASSIGN_NUMBER",data[0].sims_gb_cat_assign_number),
        //                  new SqlParameter("@GB_REF_NAME", data[0].gb_ref_name),
        //                  new SqlParameter("@GB_REF_DESC",data[0].gb_ref_desc),
        //                  new SqlParameter("@GB_REF_STATUS",data[0].gb_status==true?"A":"I"),
        //                });

        //            if (dr.Read())
        //            {

        //                string ref_no = dr["ref_no"].ToString();
        //                dr.Close();
        //                foreach (ExRefM simsobj in data)
        //                {
        //                    SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[gradebook_reference_details_proc]",
        //                   new List<SqlParameter>()
        //                {
        //                  new SqlParameter("@opr","R"),
        //                  new SqlParameter("@GB_REF_NUMBER",ref_no),
        //                  new SqlParameter("@GB_REF_FILE_NAME",simsobj.gb_ref_file_name),
        //                  new SqlParameter("@GB_REF_DISPLAY_NAME",simsobj.gb_ref_display_name),
        //                  new SqlParameter("@GB_REF_PUBLISH_DATE",data[0].gb_ref_publish_date),
        //                  new SqlParameter("@GB_REF_EXPIRY_DATE",data[0].gb_ref_expiry_date)

        //                });

        //                    if (dr2.RecordsAffected > 0)
        //                    {
        //                        inserted = true;
        //                    }
        //                    dr2.Close();
        //                }
        //            }

        //            else
        //            {
        //                inserted = false;
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        // Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}


        #region  API FOr BASELINE MARKS
        [Route("getSectionsubjectforAll")]
        public HttpResponseMessage getSectionsubjectforAll(string curcode, string academicyear, string login_user, string grade, string section)
        {

            List<BaseMk> mod_list = new List<BaseMk>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_baselinemarks_proc]",
                    new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","Z"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@bell_academic_year", academicyear),
                             new SqlParameter("@user_name", login_user),
                             new SqlParameter("@bell_grade_code", grade),
                             new SqlParameter("@sims_section_code", section),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BaseMk newobj = new BaseMk();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.sims_subject_code = dr["sims_subject_code"].ToString();

                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getBaselineMarksData")]
        public HttpResponseMessage getBaselineMarksData(string curcode, string academicyear, string grade, string section, string subject, string login_user)
        {

            List<BaseMk> mod_list = new List<BaseMk>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_baselinemarks_proc]",
                    new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","S"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@bell_academic_year", academicyear),
                             new SqlParameter("@bell_grade_code", grade),
                             new SqlParameter("@sims_section_code", section),
                             new SqlParameter("@sims_subject_code", subject),
                             new SqlParameter("@user_name", login_user),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BaseMk newobj = new BaseMk();
                            newobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            newobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            newobj.sims_subject_baseline_obtained_score = dr["sims_subject_baseline_obtained_score"].ToString();
                            newobj.sims_subject_baseline_max_score = dr["sims_subject_baseline_max_score"].ToString();

                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getTargetMarksData")]
        public HttpResponseMessage getTargetMarksData(string curcode, string academicyear, string grade, string section, string subject, string login_user)
        {

            List<BaseMk> mod_list = new List<BaseMk>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_baselinemarks_proc]",
                    new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","T"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@bell_academic_year", academicyear),
                             new SqlParameter("@bell_grade_code", grade),
                             new SqlParameter("@sims_section_code", section),
                             new SqlParameter("@sims_subject_code", subject),
                             new SqlParameter("@user_name", login_user),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BaseMk newobj = new BaseMk();
                            newobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            newobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            newobj.sims_subject_target_obtained_score = dr["sims_subject_target_obtained_score"].ToString();
                            newobj.sims_subject_target_max_score = dr["sims_subject_target_max_score"].ToString();
                            mod_list.Add(newobj);

                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getBaselineTargetMarksData")]
        public HttpResponseMessage getBaselineTargetMarksData(string curcode, string academicyear, string grade, string section, string subject, string login_user)
        {

            List<BaseMk> mod_list = new List<BaseMk>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_baselinemarks_proc]",
                    new List<SqlParameter>()
                         {
                             new SqlParameter("@opr","B"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@bell_academic_year", academicyear),
                             new SqlParameter("@bell_grade_code", grade),
                             new SqlParameter("@sims_section_code", section),
                             new SqlParameter("@sims_subject_code", subject),
                             new SqlParameter("@user_name", login_user),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BaseMk newobj = new BaseMk();
                            newobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            newobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            newobj.sims_subject_baseline_obtained_score = dr["sims_subject_baseline_obtained_score"].ToString();
                            newobj.sims_subject_baseline_max_score = dr["sims_subject_baseline_max_score"].ToString();
                            newobj.sims_subject_target_obtained_score = dr["sims_subject_target_obtained_score"].ToString();
                            newobj.sims_subject_target_max_score = dr["sims_subject_target_max_score"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CUDBaselineMarks")]
        public HttpResponseMessage CUDBaselineMarks(List<BaseMk> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (BaseMk simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_baselinemarks_proc]",
                        new List<SqlParameter>()
                     {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@bell_academic_year", simsobj.bell_academic_year),
                             new SqlParameter("@bell_grade_code", simsobj.bell_grade_code),
                             new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                             new SqlParameter("@sims_subject_baseline_obtained_score",simsobj.sims_subject_baseline_obtained_score),
                             new SqlParameter("@sims_user_baseline_created_by",simsobj.sims_user_baseline_created_by),

                     });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDBaselineTargetMarks")]
        public HttpResponseMessage CUDBaselineTargetMarks(List<BaseMk> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (BaseMk simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_baselinemarks_proc]",
                        new List<SqlParameter>()
                     {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@bell_academic_year", simsobj.bell_academic_year),
                             new SqlParameter("@bell_grade_code", simsobj.bell_grade_code),
                             new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                             new SqlParameter("@sims_subject_baseline_obtained_score",simsobj.sims_subject_baseline_obtained_score),
                             new SqlParameter("@sims_user_baseline_created_by",simsobj.sims_user_baseline_created_by),

                             new SqlParameter("@sims_subject_target_obtained_score",simsobj.sims_subject_target_obtained_score),
                             new SqlParameter("@sims_user_target_created_by",simsobj.sims_user_target_created_by),

                     });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("CUDTargetMarks")]
        public HttpResponseMessage CUDTargetMarks(List<BaseMk> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (BaseMk simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_baselinemarks_proc]",
                        new List<SqlParameter>()
                     {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@bell_academic_year", simsobj.bell_academic_year),
                             new SqlParameter("@bell_grade_code", simsobj.bell_grade_code),
                             new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                             new SqlParameter("@sims_subject_target_obtained_score",simsobj.sims_subject_target_obtained_score),
                             new SqlParameter("@sims_user_target_created_by",simsobj.sims_user_target_created_by),

                     });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        #endregion


    }
}