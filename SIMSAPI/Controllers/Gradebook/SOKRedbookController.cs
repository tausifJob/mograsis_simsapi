﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;
namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/rdbook")]
    public class SOKRedbookController : ApiController
    {

        [Route("stdentdatardbook")]
        public HttpResponseMessage stdentdatardbook(redbook data)
        {
            List<redbook> lst = new List<redbook>();
            List<redbook> lst1 = new List<redbook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_moe_result_details_PROC]",
                        new List<SqlParameter>()
                     {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_cur_code", data.sims_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_grade_code", data.sims_grade_code),
                new SqlParameter("@sims_section_code", data.sims_section_code),
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            redbook rdb = new redbook();


                            rdb.new_attendance = dr["new_attendance"].ToString();
                            rdb.new_sims_student_absent_count_with_excuse = dr["new_sims_student_absent_count_with_excuse"].ToString();
                            rdb.new_sims_student_absent_count_without_excuse = dr["new_sims_student_absent_count_without_excuse"].ToString();
                            rdb.sims_academic_year = data.sims_academic_year + "";
                            rdb.sims_cur_code = data.sims_cur_code + "";
                            rdb.sims_grade_code = data.sims_grade_code + "";
                            rdb.sims_section_code = data.sims_section_code + "";
                            rdb.sims_attendance = dr["Student_attendance"].ToString();
                            rdb.student_name_ot = dr["student_name_ot"].ToString();
                            rdb.student_name_en = dr["student_name_en"].ToString();
                            rdb.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            rdb.sims_moe_grade_code = dr["sims_moe_grade_code"].ToString();
                            rdb.sims_moe_section_code = dr["sims_moe_section_code"].ToString();
                            rdb.sims_student_final_result = dr["sims_student_final_result"].ToString();
                            rdb.sims_student_final_remark = dr["sims_student_final_remark"].ToString();
                            rdb.sims_student_absent_count_with_excuse = dr["sims_student_absent_count_with_excuse"].ToString();
                            rdb.sims_student_absent_count_without_excuse = dr["sims_student_absent_count_without_excuse"].ToString();
                            rdb.sims_moe_grade_code = dr["sims_moe_grade_code"].ToString();
                            rdb.sims_moe_section_code = dr["sims_moe_section_code"].ToString();
                            rdb.sims_enrolled_student_count = dr["sims_enrolled_student_count"].ToString();
                            rdb.sims_enrolled_student_count_local = dr["sims_enrolled_student_count_local"].ToString();
                            rdb.sims_enrolled_student_count_expat = dr["sims_enrolled_student_count_expat"].ToString();
                            rdb.sims_passed_student_count = dr["sims_passed_student_count"].ToString();
                            rdb.sims_passed_student_count_local = dr["sims_passed_student_count_local"].ToString();
                            rdb.sims_passed_student_count_expat = dr["sims_passed_student_count_expat"].ToString();
                            rdb.sims_promoted_student_count = dr["sims_promoted_student_count"].ToString();
                            rdb.sims_retest_in_arabic = dr["sims_retest_in_arabic"].ToString();
                            rdb.sims_retest_in_islamic = dr["sims_retest_in_islamic"].ToString();
                            rdb.sims_retest_in_UAE = dr["sims_retest_in_UAE"].ToString();
                            rdb.sims_detained_student_count = dr["sims_detained_student_count"].ToString();
                            rdb.sims_present_student_count = dr["sims_present_student_count"].ToString();
                            rdb.sims_present_student_count_local = dr["sims_present_student_count_local"].ToString();
                            rdb.sims_present_student_count_expat = dr["sims_present_student_count_expat"].ToString();
                            rdb.sims_percentage_student_local = dr["sims_percentage_student_local"].ToString();
                            rdb.sims_percentage_student_expat = dr["sims_percentage_student_expat"].ToString();
                            rdb.sims_mode_view = dr["sims_mode_view"].ToString();
                            rdb.sims_student_attr2 = dr["sims_student_attr2"].ToString().Equals("A") ? true : false;
                            rdb.sims_student_attr1 = dr["sims_student_attr1"].ToString();


                            lst.Add(rdb);
                        }
                        dr.NextResult();
                        while (dr.Read())
                        {
                            redbook rdb1 = new redbook();

                            rdb1.sims_column_name = dr["COLUMN_NAME"].ToString();
                            rdb1.sims_column_name_dummy = dr["COLUMN_NAME_DUMMY"].ToString();
                            lst1.Add(rdb1);
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            object[] ob = new object[2];
            ob[0] = new object();
            ob[1] = new object();
            ob[0] = lst;
            ob[1] = lst1;

            return Request.CreateResponse(HttpStatusCode.OK, ob);

        }

        [Route("getresultstatus")]
        public HttpResponseMessage getresultstatus()
        {
            List<redbook> lst = new List<redbook>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_moe_result_details_PROC]",
                        new List<SqlParameter>()
                     {
                new SqlParameter("@opr", "R")

                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            redbook rdb = new redbook();

                            rdb.result_status_code = dr["sims_appl_parameter"].ToString();
                            rdb.result_status_name = dr["sims_appl_form_field_value1"].ToString();

                            lst.Add(rdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDstdentdatardbook")]
        public HttpResponseMessage CUDstdentdatardbook(List<redbook> data1)
        {


            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (redbook data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_moe_result_details_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "I"),
                new SqlParameter("@sims_academic_year",data.sims_academic_year),
                new SqlParameter("@sims_cur_code", data.sims_cur_code),
                new SqlParameter("@sims_grade_code",data.sims_grade_code),
                new SqlParameter("@sims_section_code",data.sims_section_code),
                new SqlParameter("@sims_attendance",data.sims_attendance),
                new SqlParameter("@sims_enroll_number",data.sims_enroll_number),
                new SqlParameter("@sims_student_final_result",data.sims_student_final_result),
                new SqlParameter("@sims_student_final_remark",data.sims_student_final_remark),
                new SqlParameter("@sims_student_absent_count_with_excuse",data.sims_student_absent_count_with_excuse),
                new SqlParameter("@sims_student_absent_count_without_excuse",data.sims_student_absent_count_without_excuse),
                 new SqlParameter("@sims_moe_grade_code", data.sims_moe_grade_code),
                new SqlParameter("@sims_moe_section_code", data.sims_moe_section_code),
                new SqlParameter("@sims_student_attr2",data.sims_student_attr2.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_attr1",data.sims_student_attr1)
                            });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;

                        }
                        dr.Close();

                    }

                    if (inserted == true)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_moe_result_details_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "II"),
                 new SqlParameter("@sims_academic_year",data1[0].sims_academic_year),
                new SqlParameter("@sims_cur_code", data1[0].sims_cur_code),
                new SqlParameter("@sims_grade_code",data1[0].sims_grade_code),
                new SqlParameter("@sims_section_code",data1[0].sims_section_code),
                new SqlParameter("@sims_moe_grade_code", data1[0].sims_grade_code),
                new SqlParameter("@sims_moe_section_code", data1[0].sims_section_code),
                new SqlParameter("@sims_enrolled_student_count", data1[0].sims_enrolled_student_count),
                new SqlParameter("@sims_enrolled_student_count_local", data1[0].sims_enrolled_student_count_local),
                new SqlParameter("@sims_enrolled_student_count_expat", data1[0].sims_enrolled_student_count_expat),
                new SqlParameter("@sims_passed_student_count", data1[0].sims_passed_student_count),
                new SqlParameter("@sims_passed_student_count_local", data1[0].sims_passed_student_count_local),
                new SqlParameter("@sims_passed_student_count_expat", data1[0].sims_passed_student_count_expat),
                new SqlParameter("@sims_promoted_student_count", data1[0].sims_promoted_student_count),
                new SqlParameter("@sims_retest_in_arabic", data1[0].sims_retest_in_arabic),
                new SqlParameter("@sims_retest_in_islamic", data1[0].sims_retest_in_islamic),
                new SqlParameter("@sims_retest_in_UAE", data1[0].sims_retest_in_UAE),
                new SqlParameter("@sims_detained_student_count", data1[0].sims_detained_student_count),
                new SqlParameter("@sims_present_student_count", data1[0].sims_present_student_count),
                new SqlParameter("@sims_present_student_count_local", data1[0].sims_present_student_count_local),
                new SqlParameter("@sims_present_student_count_expat", data1[0].sims_present_student_count_expat),
                new SqlParameter("@sims_percentage_student_local", data1[0].sims_percentage_student_local),
                new SqlParameter("@sims_percentage_student_expat", data1[0].sims_percentage_student_expat),
                new SqlParameter("@sims_mode_view", data1[0].sims_mode_view),

                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }


            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("CUDstdentdatardbookdetails")]
        public HttpResponseMessage CUDstdentdatardbookdetails(List<redbook> data1)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (redbook data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_gradebook_moe_result_details_PROC]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "IC"),

                new SqlParameter("@COLUMN_NAME", data.sims_column_name),
                new SqlParameter("@COLUMN_NAME_DUMMY", data.sims_column_name_dummy),


                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                        dr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }


            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

    }
}