﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;

namespace SIMSAPI.Controllers.Gradebook
{
    [RoutePrefix("api/CommentsDetailsEntryController")]
    public class CommentsDetailsEntryController : ApiController
    {
        [Route("ReportCardStudent_Finalcomment")]
        public HttpResponseMessage ReportCardStudent_Finalcomment(ReportCardStudent sb)
        {
            List<ReportCardStudent> lst = new List<ReportCardStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                          new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", sb.Curr_Code),
                new SqlParameter("@sims_academic_year", sb.Academic_Year),
                new SqlParameter("@sims_grade_code", sb.Grade_Code),
                new SqlParameter("@sims_section_code", sb.Section_code),
                new SqlParameter("@sims_report_card_code", sb.Report_Card_Code),
                new SqlParameter("@sims_level_code", sb.Level_code)

              });
                    while (dr.Read())
                    {
                        ReportCardStudent rpl = new ReportCardStudent();
                        rpl.Report_Card_Name = dr["sims_report_card_name"].ToString();
                        rpl.Report_Card_Code = dr["sims_report_card_code"].ToString();
                        rpl.Comment1_code = dr["sims_comment1_code"].ToString();
                        rpl.Comment1_desc = dr["Comment1"].ToString();
                        rpl.Comment2_code = dr["sims_comment2_code"].ToString();
                        rpl.Comment2_desc = dr["Comment2"].ToString();
                        rpl.Comment3_code = dr["sims_comment3_code"].ToString();
                        rpl.Comment3_desc = dr["Comment3"].ToString();
                        rpl.enroll = dr["sims_gb_cat_assign_enroll_number"].ToString();
                        rpl.Level_code = dr["sims_level_code"].ToString();
                        rpl.Level_Name = dr["LevelName"].ToString();
                        rpl.Status = dr["sims_student_comment_status"].ToString();
                        //rpl.Subject_Code = dr["sims_subject_code"].ToString();
                        rpl.StudName = dr["StudName"].ToString();
                        rpl.user1 = dr["sims_user1_code"].ToString();
                        rpl.user2 = dr["sims_user2_code"].ToString();
                        rpl.user3 = dr["sims_user3_code"].ToString();
                        rpl.included = dr["avi"].ToString();
                        rpl.Academic_Year = sb.Academic_Year;
                        rpl.Curr_Code = sb.Curr_Code;
                        rpl.Grade_Code = sb.Grade_Code;
                        rpl.Section_code = sb.Section_code;
                        try
                        {
                            rpl.ColumnVisibility = dr["colVis"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        lst.Add(rpl);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("RCNewInsertCommentforStudent")]
        public HttpResponseMessage RCNewInsertCommentforStudent(student_report_card_comment inpt)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_card_comment_header_proc",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "NI"),
                new SqlParameter("@sims_cur_code", inpt.sims_admission_cur_code),
                new SqlParameter("@sims_academic_year", inpt.sims_academic_year),
                new SqlParameter("@sims_grade_code", inpt.sims_grade_code),
                new SqlParameter("@sims_section_code", inpt.sims_section_code),
                new SqlParameter("@sims_term_code", inpt.sims_term_code),
                new SqlParameter("@sims_subject_code", inpt.sims_subject_code),
                new SqlParameter("@sims_level_code", inpt.sims_report_card_level),
                new SqlParameter("@sims_report_card_code", inpt.sims_report_card),
                new SqlParameter("@sims_comment_type", inpt.sims_comment_type),
                new SqlParameter("@sims_enroll_number", inpt.sims_enroll_number),
                new SqlParameter("@sims_comment_header_desc", inpt.sims_comment_header_desc),
                new SqlParameter("@header_id", inpt.sims_student_comment_header_id),
                new SqlParameter("@sims_comment_type1",inpt.sims_comment_type1),
               });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        #region GradeBookNewCommon

        [Route("CommentDetailsCommon")]
        public HttpResponseMessage CommentDetailsCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_report_card_comment_mapping_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        [Route("getStudentCommentlist")]
        public HttpResponseMessage getStudentCommentlist(string curcode, string ayear, string gradecode, string section, string term_code, string comment_type, string subject, string level, string reportcode)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "AA"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_term_code", term_code),
                new SqlParameter("@sims_comment_type", comment_type),
                new SqlParameter("@sims_subject_code", subject),
                new SqlParameter("@sims_level_code", level),
                new SqlParameter("@sims_report_card_code", reportcode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_comment_sr = dr["sims_comment_sr"].ToString();
                            com.sims_academic_year = dr["sims_academic_year"].ToString();
                            com.sims_cur_code = dr["sims_cur_code"].ToString();
                            com.sims_grade_code = dr["sims_grade_code"].ToString();
                            com.sims_section_code = dr["sims_section_code"].ToString();
                            com.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            com.sims_term_code = dr["sims_term_code"].ToString();
                            com.sims_subject_code = dr["sims_subject_code"].ToString();
                            com.sims_level_code = dr["sims_level_code"].ToString();
                            com.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            com.sims_student_comment_id = dr["sims_student_comment_id"].ToString();
                            com.sims_student_root_comment_display_order = dr["sims_student_root_comment_display_order"].ToString();
                            com.sims_comment_status_details = dr["sims_comment_status"].ToString();
                            com.sims_comment_type = dr["sims_comment_type"].ToString();
                            com.sims_user_code = dr["sims_user_code"].ToString();
                            com.sims_student_header_code = dr["sims_student_header_code"].ToString();
                            com.student_name = dr["student_name"].ToString();
                            com.sims_comment_desc = dr["sims_comment_desc"].ToString();
                            com.student_type = dr["student_type"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("gerRclevels")]
        public HttpResponseMessage gerRclevels(string curcode, string ayear, string gradecode, string section)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "L"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_level_code = dr["sims_level_code"].ToString();
                            com.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("gerRcname")]
        public HttpResponseMessage gerRcname(string curcode, string ayear, string gradecode, string section, string level, string term)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_level_code", level),
                new SqlParameter("@sims_term_code", term)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            com.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("getsubjectheader")]
        public HttpResponseMessage getsubjectheader(string curcode, string ayear, string gradecode, string section, string level, string report)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_level_code", level),
                new SqlParameter("@sims_report_card_code", report)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("getsubjecteffortheader")]
        public HttpResponseMessage getsubjecteffortheader(string curcode, string ayear, string gradecode, string section, string level, string report)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "EH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_level_code", level),
                new SqlParameter("@sims_report_card_code", report)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("getgeneraltheader")]
        public HttpResponseMessage getgeneraltheader(string curcode, string ayear, string gradecode, string section, string level, string report)
        {
            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_proc]",
                   new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "GH"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", ayear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_level_code", level),
                new SqlParameter("@sims_report_card_code", report)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            student_report_card_comment com = new student_report_card_comment();
                            com.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            AcademicYear.Add(com);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("CUInsertcomment")]
        public HttpResponseMessage InsertDetention(List<student_report_card_comment> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_comment_mapping_proc]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","NI"),
                                     new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                                     new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                                     new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                                     new SqlParameter("@sims_section_code",obj.sims_section_code),
                                     new SqlParameter("@sims_level_code",obj.sims_level_code),
                                     new SqlParameter("@sims_report_card_code",obj.sims_report_card_code),
                                    // new SqlParameter("@sims_comment_code",obj.sims_comment_code),
                                     new SqlParameter("@sims_comment_type",obj.sims_comment_type),
                                     new SqlParameter("@header_id",obj.sims_student_header_code),
                                     new SqlParameter("@sims_comment_header_desc",obj.sims_comment_desc),
                                     new SqlParameter("@sims_subject_code",obj.sims_subject_code),
                                  //   new SqlParameter("@sims_comment_status",obj.sims_comment_status),
                                     new SqlParameter("@sims_term_code",obj.sims_term_code),
                                     new SqlParameter("@sims_enroll_number",obj.sims_enroll_number)
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDEleteComment")]
        public HttpResponseMessage CUDEleteComment(List<student_report_card_comment> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_comment_mapping_proc]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","DD"),
                                     new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                                     new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                                     new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                                     new SqlParameter("@sims_section_code",obj.sims_section_code),
                                     new SqlParameter("@sims_level_code",obj.sims_level_code),
                                     new SqlParameter("@sims_report_card_code",obj.sims_report_card_code),
                                     new SqlParameter("@sims_subject_code",obj.sims_subject_code),
                                     new SqlParameter("@sims_enroll_number",obj.sims_enroll_number)
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getGradeBookComment")]
        public HttpResponseMessage getGradeBookComment(string curcode, string ayear, string gradecode, string section, string term_code, string comment_type, string subject, string user)
        {
            string str = "";
            string sub = "";
            List<grade_scale_new> main_grade_lst = new List<grade_scale_new>();

            main_grade_lst = get_grade_scale(curcode, ayear);
            List<student_report_card_comment_new> house = new List<student_report_card_comment_new>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_new_proc]",
                        new List<SqlParameter>()
                         {
                            
                 new SqlParameter("@opr", "B"),
                 new SqlParameter("@sims_cur_code", curcode),
                 new SqlParameter("@sims_academic_year", ayear),
                 new SqlParameter("@sims_grade_code", gradecode),
                 new SqlParameter("@sims_section_code", section),
                 new SqlParameter("@sims_term_code", term_code),
                 new SqlParameter("@sims_subject_code", subject),
                 new SqlParameter("@comment_type", comment_type),
                 new SqlParameter("@sims_user1_code", user)

                 
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["sims_enroll_number"].ToString();
                            sub = dr["sims_comment_code"].ToString();

                            var v = (from p in house where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                student_report_card_comment_new ob = new student_report_card_comment_new();
                                ob.sublist1 = new List<student_report_card_comment_new_sub>();
                                
                                ob.sims_enroll_number = str;


                                ob.StudName = dr["StudName"].ToString();

                                ob.s_sims_comment_code = dr["s_sims_comment_code"].ToString();
                                ob.s_sims_comment_desc = dr["s_sims_comment_desc"].ToString();
                               
                               
                                ob.sims_comment_code = dr["sims_comment_code"].ToString();
                                ob.sims_comment_desc = dr["sims_comment_desc"].ToString();


                                student_report_card_comment_new_sub sb = new student_report_card_comment_new_sub();
                                sb.s_sims_comment_code = dr["s_sims_comment_code"].ToString();
                                sb.s_sims_comment_desc = dr["s_sims_comment_desc"].ToString();
                                sb.sims_comment_code = dr["sims_comment_code"].ToString();
                                sb.sims_comment_desc = dr["sims_comment_desc"].ToString();

                                sb.sims_comment_desc_len = dr["sims_comment_desc_len"].ToString();
                                try
                                {
                                    sb.sims_grade_scale_code = dr["sims_grade_scale_code"].ToString();
                                    sb.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                                }
                                catch (Exception ex) { }
                                if(!string.IsNullOrEmpty(sb.sims_grade_scale_code)){

                                    sb.grade_scale_lst = main_grade_lst.Where(x => x.sims_mark_grade == sb.sims_grade_scale_code).ToList();
                                  
                                }

                                sb.sims_enroll_number = dr["sims_enroll_number"].ToString();
                               

                                ob.sublist1.Add(sb);
                                house.Add(ob);
                            }
                            else
                            {
                                student_report_card_comment_new_sub sb = new student_report_card_comment_new_sub();
                                sb.s_sims_comment_code = dr["s_sims_comment_code"].ToString();
                                sb.s_sims_comment_desc = dr["s_sims_comment_desc"].ToString();
                                sb.sims_comment_code = dr["sims_comment_code"].ToString();
                                sb.sims_comment_desc = dr["sims_comment_desc"].ToString();
                                sb.sims_comment_desc_len = dr["sims_comment_desc_len"].ToString();

                                sb.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                try
                                {
                                    sb.sims_grade_scale_code = dr["sims_grade_scale_code"].ToString();
                                    sb.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                                }
                                catch (Exception ex) { }

                                if (!string.IsNullOrEmpty(sb.sims_grade_scale_code))
                                {

                                    sb.grade_scale_lst = main_grade_lst.Where(x => x.sims_mark_grade == sb.sims_grade_scale_code).ToList();

                                }

                                //sb.sims_status = dr["Status"].Equals(1) ? true : false;
                                v.ElementAt(0).sublist1.Add(sb);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }





        [Route("CUInsertcommentN")]
        public HttpResponseMessage InsertDetentionN(List<student_report_card_comment> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_report_card_comment obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_comment_mapping_new_proc]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","C"),
                                     new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                                     new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                                     new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                                     new SqlParameter("@sims_section_code",obj.sims_section_code),
                                     new SqlParameter("@sims_level_code",obj.sims_level_code),
                                     new SqlParameter("@sims_report_card_code",obj.sims_report_card_code),
                                     new SqlParameter("@sims_comment_code",obj.sims_comment_code),
                                     new SqlParameter("@sims_comment_type",obj.sims_comment_type),
                                     new SqlParameter("@header_id",obj.sims_student_header_code),
                                     new SqlParameter("@sims_comment_header_desc",obj.sims_comment_desc),
                                     new SqlParameter("@sims_subject_code",obj.sims_subject_code),
                                  // new SqlParameter("@sims_comment_status",obj.sims_comment_status),
                                     new SqlParameter("@sims_term_code",obj.sims_term_code),
                                     new SqlParameter("@sims_enroll_number",obj.sims_enroll_number),
                                     new SqlParameter("@sims_user1_code",obj.sims_user_code),
                                     new SqlParameter("@sims_comment2_code",obj.sims_cur_level_code),
                                     new SqlParameter("@sims_comment3_code",obj.sims_grade_scale_code)



                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }




        public List<grade_scale_new> get_grade_scale(string cur, string academic)
        {

            List<grade_scale_new> lst = new List<grade_scale_new>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_comment_mapping_new_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "D"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_academic_year",academic),
           
                });

                    while (dr.Read())
                    {
                        grade_scale_new assign = new grade_scale_new();
                        assign.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
                        assign.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                        assign.sims_mark_grade_description = dr["sims_mark_grade_description"].ToString();
                        assign.sims_mark_grade = dr["sims_mark_grade"].ToString();

                        


                        lst.Add(assign);
                    }
                }
                return lst;
            }
            catch (Exception ex) {
                return lst;
            }
        }



    }
}