﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.modules.CampTransactionController
{
    [RoutePrefix("api/CampTransaction")]
    [BasicAuthentication]
    public class CampTransactionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("getCampName")]
        public HttpResponseMessage getCampName(string username)
        {          
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                     new List<SqlParameter>()
                     {
                         new SqlParameter("@opr", "A"),
                         new SqlParameter("@username", username)
                     });
                     
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                            obj.sims_health_package_code = dr["sims_health_package_code"].ToString();
                            obj.sims_health_cur_code = dr["sims_health_cur_code"].ToString();
                            obj.sims_health_camp_name_en = dr["sims_health_camp_name_en"].ToString();
                            obj.sims_health_camp_date = db.UIDDMMYYYYformat(dr["sims_health_camp_date"].ToString());
                            obj.sims_health_camp_desc = dr["sims_health_camp_desc"].ToString();
                            obj.sims_health_camp_status = dr["sims_health_camp_status"].ToString();
                            obj.sims_health_package_name_en = dr["sims_health_package_name_en"].ToString();
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();                            
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("getStudents")]
        public HttpResponseMessage getStudents(string cur_code, string academic_year, string grade_code, string section_code,string enroll_number)
        {
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                if (cur_code == "undefined" || cur_code == "")
                    cur_code = null;
                if (academic_year == "undefined" || academic_year == "")
                    academic_year = null;
                if (grade_code == "undefined" || grade_code == "")
                    grade_code = null;
                if (section_code == "undefined" || section_code == "")
                    section_code = null;
                if (enroll_number == "undefined" || enroll_number == "")
                    enroll_number = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "B"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@academic_year", academic_year),
                         new SqlParameter("@grade_code", grade_code),
                         new SqlParameter("@section_code",section_code),
                         new SqlParameter("@enroll_number",enroll_number)
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            obj.stud_name = dr["stud_name"].ToString();
                            obj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            obj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                            obj.stud_age = dr["stud_age"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();                        
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }


        [Route("getCampDetails")]
        public HttpResponseMessage getCampDetails(string package_code)
        {
            List<health_camp> code_list = new List<health_camp>();
            string str = string.Empty;
            try
            {
                if (package_code == "undefined" || package_code == "")
                    package_code = null;
         

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "C"),
                         new SqlParameter("@package_code", package_code)                        
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            obj.stud_name = dr["stud_name"].ToString();
                            obj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            obj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                            obj.stud_age = dr["stud_age"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }



        [Route("CUAllList")]
        public HttpResponseMessage CUAllList(health_camp_package gb)
        {
            //List<Students> studList = new List<Students>();
            health_camp_package o = new health_camp_package();
           // List<grade_scale> main_grade_lst = new List<grade_scale>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@package_code", gb.sims_health_package_code),
                            new SqlParameter("@sims_health_enroll_number", gb.sims_health_enroll_number),
                            new SqlParameter("@sims_health_camp_code", gb.sims_health_camp_code)                      
                         });

                   /// main_grade_lst = get_grade_scale(gb.cur_code, gb.aca_year);
                    #region Package Loop
                    if (true)
                    {
                        o.sims_health_package_code = gb.sims_health_package_code;
                        //o.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        //o.sims_subject_code = dr["sims_subject_code"].ToString();
                        //o.sims_subject_name = dr["sims_subject_name"].ToString();

                        o.test_lst = new List<health_camp_test>();
                    }
                   // dr.NextResult();
                    #endregion

                    #region Gradebook test Loop
                    while (dr.Read())
                    {
                        health_camp_test gc = new health_camp_test();

                        gc.sims_health_test_code = dr["sims_health_test_code"].ToString();
                        gc.sims_health_test_name_en = dr["sims_health_test_name_en"].ToString();
                        gc.sims_health_test_display_order = dr["sims_health_test_display_order"].ToString();
                        //gc.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();
                        gc.sims_health_enroll_number = dr["sims_health_enroll_number"].ToString();
                        //  gc.section_code = dr["sims_section_code"].ToString();
                        gc.parameter_lst = new List<health_camp_parameter>();
                        if (o != null)
                        {
                            o.test_lst.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region parameter Collection Loop
                    while (dr.Read())
                    {
                        health_camp_parameter assign = new health_camp_parameter();
                        assign.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                        assign.sims_health_parameter_name_en = dr["sims_health_parameter_name_en"].ToString();
                        assign.sims_health_test_code = dr["sims_health_test_code"].ToString();
                        assign.sims_health_parameter_min_value = dr["sims_health_parameter_min_value"].ToString();
                        assign.sims_health_parameter_max_value = dr["sims_health_parameter_max_value"].ToString();
                        assign.sims_health_parameter_default_value = dr["sims_health_parameter_default_value"].ToString();
                        assign.sims_health_parameter_unit = dr["sims_health_parameter_unit"].ToString();
                        assign.sims_health_parameter_isNumeric = dr["sims_health_parameter_isNumeric"].ToString();
                        assign.sims_health_parameter_display_order = dr["sims_health_parameter_display_order"].ToString();

                        //assign.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                        //assign.sims_health_camp_test_code = dr["sims_health_camp_test_code"].ToString();
                        //assign.sims_health_camp_parameter_code = dr["sims_health_camp_parameter_code"].ToString();
                        //assign.sims_health_camp_parameter_check_code = dr["sims_health_camp_parameter_check_code"].ToString();
                        assign.sims_health_camp_parameter_check_value = dr["sims_health_camp_parameter_check_value"].ToString();
                        assign.sims_health_camp_parameter_check_comment = dr["sims_health_camp_parameter_check_comment"].ToString();
                        //assign.sims_health_enroll_number = dr["sims_health_enroll_number"].ToString();
                        
                        //assign.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();

                        assign.check_lst = get_check(assign.sims_health_parameter_code, assign.sims_health_test_code, gb.sims_health_camp_code, gb.sims_health_enroll_number);
                                                                                    
                        health_camp_test gc = (o.test_lst.SingleOrDefault(q => q.sims_health_test_code == assign.sims_health_test_code ));
                        //&& q.sims_health_enroll_number == assign.sims_health_enroll_number
                        gc.parameter_lst.Add(assign);
                    }


                  

                }
            }
            #endregion

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError,ex.Message);
            }
            //object[] ob = new object[2];
            //ob[0] = new object();
            //ob[1] = new object();
            //ob[0] = o;
           // ob[1] = studList;
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        List<health_camp_check> get_check(string parameter_code , string test_code,string sims_health_camp_code, string sims_health_enroll_number)
        {
            List<health_camp_check> chk_lst = new List<health_camp_check>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@parameter_code", parameter_code),
                                new SqlParameter("@test_code", test_code),
                                new SqlParameter("@sims_health_camp_code", sims_health_camp_code),
                                new SqlParameter("@sims_health_enroll_number", sims_health_enroll_number)
                        });


                    while (dr.Read())
                    {
                        health_camp_check check = new health_camp_check();
                        check.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                        check.sims_health_parameter_check_code = dr["sims_health_parameter_check_code"].ToString();
                        check.sims_health_parameter_check_name_en = dr["sims_health_parameter_check_name_en"].ToString();
                        check.sims_health_parameter_check_min_value = dr["sims_health_parameter_check_min_value"].ToString();
                        check.sims_health_parameter_check_max_value = dr["sims_health_parameter_check_max_value"].ToString();
                        check.sims_health_parameter_check_comment = dr["sims_health_parameter_check_comment"].ToString();

                        check.sims_health_camp_test_code = dr["sims_health_camp_test_code"].ToString();
                        check.sims_health_camp_parameter_code = dr["sims_health_camp_parameter_code"].ToString();
                        check.sims_health_camp_parameter_check_code = dr["sims_health_camp_parameter_check_code"].ToString();
                        check.sims_health_camp_parameter_check_value = dr["sims_health_camp_parameter_check_value"].ToString();
                        check.sims_health_camp_parameter_check_comment = dr["sims_health_camp_parameter_check_comment"].ToString();
                        check.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();

                        //try
                        //{
                        //    health_camp_parameter gc = (c.parameter_lst.SingleOrDefault(q => q.sims_health_parameter_code == check.sims_health_parameter_code));
                        //    gc.check_lst.Add(check);
                        //}
                        //catch (Exception e) { }

                        chk_lst.Add(check);



                    }
                }
            }
            catch (Exception ex) { }
            return chk_lst;
        }



        // AddAddCampDetails
        [Route("AddCampDetails")]
        public HttpResponseMessage AddCampDetails(string data1, List<health_camp> postData)
        {
            bool alert_status = false;
            string sims_health_camp_report_code = string.Empty;
            
            health_camp campobj = Newtonsoft.Json.JsonConvert.DeserializeObject<health_camp>(data1);
            try
            {
                if (campobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@opr",'I'),
                                new SqlParameter("@sims_health_camp_code", campobj.sims_health_camp_code),
                                new SqlParameter("@sims_health_enroll_number", campobj.sims_health_enroll_number),
                                new SqlParameter("@sims_health_camp_employee_code",campobj.sims_health_camp_employee_code),                                
                                new SqlParameter("@sims_health_camp_transaction_date", db.DBYYYYMMDDformat(campobj.sims_health_camp_transaction_date)),
                                new SqlParameter("@sims_health_camp_report_code", campobj.sims_health_camp_report_code),                                
                                new SqlParameter("@sims_health_camp_student_absent_status", campobj.sims_health_camp_student_absent_status.Equals(true)?"Y":"N"),
                                new SqlParameter("@comn_audit_user_location", campobj.comn_audit_user_location),
                                new SqlParameter("@comn_audit_ip", campobj.comn_audit_ip)
                             });
                        if (dr.Read())
                        {
                            sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();                            
                        }
                       
                        dr.Close();

                        if(sims_health_camp_report_code !="" || sims_health_camp_report_code != null)
                        {                            
                            foreach (health_camp obj in postData)
                            {
                                SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                                  new List<SqlParameter>()
                                 {
                                    new SqlParameter("@opr",'E'),
                                    new SqlParameter("@sims_health_test_code", obj.sims_health_test_code),
                                    new SqlParameter("@sims_health_camp_parameter_code", obj.sims_health_parameter_code),
                                    new SqlParameter("@sims_health_camp_parameter_check_code", obj.sims_health_parameter_check_code),
                                    new SqlParameter("@sims_health_camp_parameter_check_value", obj.sims_health_camp_parameter_check_value),
                                    new SqlParameter("@sims_health_camp_parameter_check_comment", obj.sims_health_camp_parameter_check_comment),
                                    new SqlParameter("@sims_health_camp_transaction_date", db.DBYYYYMMDDformat(obj.sims_health_camp_transaction_date)),
                                    new SqlParameter("@sims_health_camp_employee_code", obj.sims_health_camp_employee_code),
                                    new SqlParameter("@sims_health_camp_transaction_status", obj.sims_health_camp_transaction_status),
                                    new SqlParameter("@sims_health_camp_report_code", sims_health_camp_report_code),
                                    new SqlParameter("@sims_health_camp_code", campobj.sims_health_camp_code),
                                    new SqlParameter("@sims_health_enroll_number", campobj.sims_health_enroll_number)                                    
                                 });
                                if (dr1.RecordsAffected > 0)
                                {
                                    alert_status = true;
                                }
                                else
                                {
                                    alert_status = false;
                                }
                                dr1.Close();
                            }
                        }
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }


        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "F"),
                        new SqlParameter("@cur_code", cur_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,e.Message);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code,string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                     new List<SqlParameter>()
                     {
                         new SqlParameter("@opr", "G"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@sims_grade_code", grade_code)
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,e.Message);
            }
        }

        [Route("getStudentsforDashboard")]
        public HttpResponseMessage getStudentsforDashboard(string cur_code, string academic_year, string camp_code)
        {
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                if (cur_code == "undefined" || cur_code == "")
                    cur_code = null;
                if (academic_year == "undefined" || academic_year == "")
                    academic_year = null;
                if (camp_code == "undefined" || camp_code == "")
                    camp_code = null;
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "H"),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@academic_year", academic_year),
                         new SqlParameter("@sims_health_camp_code", camp_code)                         
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            obj.stud_name = dr["stud_name"].ToString();                         
                            obj.sims_student_gender = dr["sims_student_gender"].ToString();
                            obj.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                            obj.sims_health_package_code = dr["sims_health_package_code"].ToString();                            
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("CUAllListforDashboard")]
        public HttpResponseMessage CUAllListforDashboard(health_camp_package gb)
        {
            //List<Students> studList = new List<Students>();
            health_camp_package o = new health_camp_package();
            // List<grade_scale> main_grade_lst = new List<grade_scale>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@package_code", gb.sims_health_package_code),
                            new SqlParameter("@sims_health_enroll_number", gb.sims_health_enroll_number),
                            new SqlParameter("@sims_health_camp_code", gb.sims_health_camp_code)
                         });

                    /// main_grade_lst = get_grade_scale(gb.cur_code, gb.aca_year);
                    #region Package Loop
                    if (true)
                    {
                        o.sims_health_package_code = gb.sims_health_package_code;
                        //o.sims_co_scholastic_group_code = dr["sims_co_scholastic_group_code"].ToString();
                        //o.sims_subject_code = dr["sims_subject_code"].ToString();
                        //o.sims_subject_name = dr["sims_subject_name"].ToString();

                        o.test_lst = new List<health_camp_test>();
                    }
                    // dr.NextResult();
                    #endregion

                    #region Gradebook test Loop
                    while (dr.Read())
                    {
                        health_camp_test gc = new health_camp_test();

                        gc.sims_health_test_code = dr["sims_health_test_code"].ToString();
                        gc.sims_health_test_name_en = dr["sims_health_test_name_en"].ToString();
                        gc.sims_health_test_display_order = dr["sims_health_test_display_order"].ToString();
                        //gc.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();
                        gc.sims_health_enroll_number = dr["sims_health_enroll_number"].ToString();
                        //  gc.section_code = dr["sims_section_code"].ToString();
                        gc.parameter_lst = new List<health_camp_parameter>();
                        if (o != null)
                        {
                            o.test_lst.Add(gc);
                        }
                    }
                    dr.NextResult();
                    #endregion

                    #region parameter Collection Loop
                    while (dr.Read())
                    {
                        health_camp_parameter assign = new health_camp_parameter();
                        assign.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                        assign.sims_health_parameter_name_en = dr["sims_health_parameter_name_en"].ToString();
                        assign.sims_health_test_code = dr["sims_health_test_code"].ToString();
                        assign.sims_health_parameter_min_value = dr["sims_health_parameter_min_value"].ToString();
                        assign.sims_health_parameter_max_value = dr["sims_health_parameter_max_value"].ToString();
                        assign.sims_health_parameter_default_value = dr["sims_health_parameter_default_value"].ToString();
                        assign.sims_health_parameter_unit = dr["sims_health_parameter_unit"].ToString();
                        assign.sims_health_parameter_isNumeric = dr["sims_health_parameter_isNumeric"].ToString();
                        assign.sims_health_parameter_display_order = dr["sims_health_parameter_display_order"].ToString();

                        //assign.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                        //assign.sims_health_camp_test_code = dr["sims_health_camp_test_code"].ToString();
                        //assign.sims_health_camp_parameter_code = dr["sims_health_camp_parameter_code"].ToString();
                        //assign.sims_health_camp_parameter_check_code = dr["sims_health_camp_parameter_check_code"].ToString();
                        assign.sims_health_camp_parameter_check_value = dr["sims_health_camp_parameter_check_value"].ToString();
                        assign.sims_health_camp_parameter_check_comment = dr["sims_health_camp_parameter_check_comment"].ToString();
                        //assign.sims_health_enroll_number = dr["sims_health_enroll_number"].ToString();

                        //assign.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();

                        assign.check_lst = get_checkforDashboard(assign.sims_health_parameter_code, assign.sims_health_test_code, gb.sims_health_camp_code, gb.sims_health_enroll_number);

                        health_camp_test gc = (o.test_lst.SingleOrDefault(q => q.sims_health_test_code == assign.sims_health_test_code));
                        //&& q.sims_health_enroll_number == assign.sims_health_enroll_number
                        gc.parameter_lst.Add(assign);
                    }
                    
                }
            }
            #endregion

            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            //object[] ob = new object[2];
            //ob[0] = new object();
            //ob[1] = new object();
            //ob[0] = o;
            // ob[1] = studList;
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        [Route("getCampNameForFileUpload")]
        public HttpResponseMessage getCampNameForFileUpload(string username, string cur_code, string academic_year)
        {
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "O"),
                         new SqlParameter("@username", username),
                         new SqlParameter("@cur_code", cur_code),
                         new SqlParameter("@academic_year", academic_year)
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                            obj.sims_health_package_code = dr["sims_health_package_code"].ToString();
                            obj.sims_health_cur_code = dr["sims_health_cur_code"].ToString();
                            obj.sims_health_camp_name_en = dr["sims_health_camp_name_en"].ToString();
                            obj.sims_health_camp_date = db.UIDDMMYYYYformat(dr["sims_health_camp_date"].ToString());
                            obj.sims_health_camp_desc = dr["sims_health_camp_desc"].ToString();
                            obj.sims_health_camp_status = dr["sims_health_camp_status"].ToString();
                            obj.sims_health_package_name_en = dr["sims_health_package_name_en"].ToString();
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        public HttpResponseMessage getTestName(string package_code, string camp_code)
        {
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "P"),
                         new SqlParameter("@package_code", package_code),
                         new SqlParameter("@sims_health_camp_code", camp_code)                        
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_health_test_code = dr["sims_health_test_code"].ToString();
                            obj.sims_health_test_name_en = dr["sims_health_test_name_en"].ToString();
                            
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }


        // Insertdocuments
        [Route("InsertCampDocuments")]
        public HttpResponseMessage InsertCampDocuments(List<health_camp> postData)
        {
            bool alert_status = false;
            string sims_health_csf_sr_no = string.Empty;

            //health_camp campobj = Newtonsoft.Json.JsonConvert.DeserializeObject<health_camp>(data1);
            try
            {
                if (postData != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                        //      new List<SqlParameter>()
                        //     {
                        //        new SqlParameter("@opr",'Q'),
                        //        new SqlParameter("@sims_health_camp_code", campobj.sims_health_camp_code),
                        //        new SqlParameter("@sims_health_enroll_number", campobj.sims_health_enroll_number)                              
                        //     });
                        //if (dr.Read())
                        //{
                        //    sims_health_csf_sr_no = dr["sims_health_csf_sr_no"].ToString();
                        //}

                        //dr.Close();

                        //if (sims_health_csf_sr_no != "" || sims_health_csf_sr_no != null)
                        //{
                            foreach (health_camp obj in postData)
                            {
                                SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                                  new List<SqlParameter>()
                                 {
                                    new SqlParameter("@opr",'Q'),
                                    new SqlParameter("@sims_health_camp_code", obj.sims_health_camp_code),
                                    new SqlParameter("@sims_health_enroll_number", obj.sims_health_enroll_number),
                                    new SqlParameter("@sims_health_csf_ref_no", obj.sims_health_csf_sr_no),
                                    new SqlParameter("@sims_health_test_code", obj.sims_health_test_code),
                                    new SqlParameter("@sims_health_test_file", obj.sims_health_test_file),
                                    new SqlParameter("@sims_health_test_file_uploaded_by", obj.sims_health_test_file_uploaded_by),
                                    new SqlParameter("@sims_health_file_status", obj.sims_health_file_status)
                                  });
                                
                                if (dr1.RecordsAffected > 0)
                                {
                                    alert_status = true;
                                }
                                else
                                {
                                    alert_status = false;
                                }
                                dr1.Close();
                            }
                        //}
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }
        
        [Route("getCampDocuments")]
        public HttpResponseMessage getCampDocuments(string sims_health_camp_code,string sims_health_test_code)
        {
            List<health_camp> code_list = new List<health_camp>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_health_camp_transaction_proc",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "R"),
                         new SqlParameter("@sims_health_camp_code", sims_health_camp_code),
                         new SqlParameter("@sims_health_test_code", sims_health_test_code)
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            health_camp obj = new health_camp();
                            obj.sims_health_csf_sr_no = dr["sims_health_csf_sr_no"].ToString();
                            obj.sims_health_enroll_number = dr["sims_health_enroll_number"].ToString();
                            obj.sims_health_camp_code = dr["sims_health_camp_code"].ToString();
                            obj.sims_health_test_code = dr["sims_health_test_code"].ToString();                            
                            obj.sims_health_test_file = dr["sims_health_test_file"].ToString();
                            obj.sims_health_csfd_sr_no = dr["sims_health_csfd_sr_no"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        // Delete Documents
        [Route("CampDocumentsDelete")]
        public HttpResponseMessage CampDocumentsDelete(List<health_camp> postData)
        {
            bool alert_status = false;

            try
            {
                if (postData != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (health_camp obj in postData)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                              new List<SqlParameter>()
                             {
                                    new SqlParameter("@opr",'S'),
                                    new SqlParameter("@sims_health_camp_code", obj.sims_health_camp_code),
                                    new SqlParameter("@sims_health_enroll_number", obj.sims_health_enroll_number),
                                    new SqlParameter("@sims_health_csf_ref_no", obj.sims_health_csf_sr_no),
                                    new SqlParameter("@sims_health_test_code", obj.sims_health_test_code),
                                    new SqlParameter("@sims_health_csfd_sr_no", obj.sims_health_csfd_sr_no)
                              });

                            if (dr1.RecordsAffected > 0)
                            {
                                alert_status = true;
                            }
                            else
                            {
                                alert_status = false;
                            }
                            dr1.Close();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

        List<health_camp_check> get_checkforDashboard(string parameter_code, string test_code, string sims_health_camp_code, string sims_health_enroll_number)
        {
            List<health_camp_check> chk_lst = new List<health_camp_check>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_camp_transaction_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "K"),
                                new SqlParameter("@parameter_code", parameter_code),
                                new SqlParameter("@test_code", test_code),
                                new SqlParameter("@sims_health_camp_code", sims_health_camp_code),
                                new SqlParameter("@sims_health_enroll_number", sims_health_enroll_number)
                        });


                    while (dr.Read())
                    {
                        health_camp_check check = new health_camp_check();
                        check.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                        check.sims_health_parameter_check_code = dr["sims_health_parameter_check_code"].ToString();
                        check.sims_health_parameter_check_name_en = dr["sims_health_parameter_check_name_en"].ToString();
                        check.sims_health_parameter_check_min_value = dr["sims_health_parameter_check_min_value"].ToString();
                        check.sims_health_parameter_check_max_value = dr["sims_health_parameter_check_max_value"].ToString();
                        check.sims_health_parameter_check_comment = dr["sims_health_parameter_check_comment"].ToString();

                        check.sims_health_camp_test_code = dr["sims_health_camp_test_code"].ToString();
                        check.sims_health_camp_parameter_code = dr["sims_health_camp_parameter_code"].ToString();
                        check.sims_health_camp_parameter_check_code = dr["sims_health_camp_parameter_check_code"].ToString();
                        check.sims_health_camp_parameter_check_value = dr["sims_health_camp_parameter_check_value"].ToString();
                        check.sims_health_camp_parameter_check_comment = dr["sims_health_camp_parameter_check_comment"].ToString();
                        check.sims_health_camp_report_code = dr["sims_health_camp_report_code"].ToString();

                        //try
                        //{
                        //    health_camp_parameter gc = (c.parameter_lst.SingleOrDefault(q => q.sims_health_parameter_code == check.sims_health_parameter_code));
                        //    gc.check_lst.Add(check);
                        //}
                        //catch (Exception e) { }

                        chk_lst.Add(check);
                        
                    }
                }
            }
            catch (Exception ex) { }
            return chk_lst;
        }


    }
}