﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/EntryOfCheckListReview")]
    public class EntryOfCheckListReviewController : ApiController
    {
        private string sims_checklist_section_review_number;
        // get section list
        [Route("get_sectionbyList")]
        public HttpResponseMessage get_sectionbyList()
        {

            List<Sims754> lst = new List<Sims754>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims754 simsobj = new Sims754();
                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }



        // get rseponse list
        [Route("get_responsebyList")]
        public HttpResponseMessage get_responsebyList()
        {

            List<Sims754> lst = new List<Sims754>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims754 simsobj = new Sims754();
                            simsobj.sims_response_code = dr["sims_response_code"].ToString();
                            simsobj.sims_response_title = dr["sims_response_title"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        // get evidences list
        [Route("get_evidencesbyList")]
        public HttpResponseMessage get_evidencesbyList()
        {

            List<Sims754> lst = new List<Sims754>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims754 simsobj = new Sims754();
                            simsobj.sims_checklist_section_review_number = dr["sims_checklist_section_review_number"].ToString();
                            simsobj.sims_checklist_section_parameter_number = dr["sims_checklist_section_parameter_number"].ToString();
                            simsobj.sims_checklist_section_parameter_name = dr["sims_checklist_section_parameter_name"].ToString();
                            simsobj.sims_checklist_section_review_parameter_action = dr["sims_checklist_section_review_parameter_action"].ToString();
                            simsobj.sims_checklist_section_review_parameter_response = dr["sims_checklist_section_review_parameter_response"].ToString();
                          

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        //insert and update
         [Route("CUDListReview")]
        public HttpResponseMessage CUDListEvidences(List<Sims754> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims754 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_checklist_section_review_PROC]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_checklist_section_review_checklist_section_number", simsobj.sims_checklist_section_number),
                                new SqlParameter("@sims_checklist_section_review_date", db.DBYYYYMMDDformat(simsobj.sims_checklist_section_review_date)), 
                                new SqlParameter("@sims_checklist_section_review_by", simsobj.sims_checklist_section_review_by),   
                                new SqlParameter("@sims_checklist_section_review_parameter_number", simsobj.sims_checklist_section_review_parameter_number),
                                new SqlParameter("@sims_checklist_section_review_parameter_response", simsobj.sims_checklist_section_review_parameter_response),
                               new SqlParameter("@sims_checklist_section_review_parameter_action", simsobj.sims_checklist_section_review_parameter_action),
                               new SqlParameter("@sims_checklist_section_review_transaction_date", db.DBYYYYMMDDformat(simsobj.sims_checklist_section_review_transaction_date)),
                                new SqlParameter("@sims_checklist_section_review_number", simsobj.sims_checklist_section_review_number),
                             //  new SqlParameter("@sims_checklist_section_review_approve_by", simsobj.sims_checklist_section_review_approve_by),
                              new SqlParameter("@sims_response_code", simsobj.sims_response_code),
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        //search 
         [Route("getsearchlist")]
         public HttpResponseMessage getsearchlist(string str, string str1)   //, string str2
         {

             List<Sims754> lst = new List<Sims754>();
             if (str == "undefined" || str == "")
             {
                 str = null;
             }
             if (str1 == "undefined" || str1 == "")
             {
                 str1 = null;
             }
             //if (str2 == "undefined" || str2 == "")
             //{
             //    str = null;
             //}
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                         new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),
                           
                           new SqlParameter("@sims_checklist_section_number",str),
                           new SqlParameter("@sims_checklist_section_review_date",db.DBYYYYMMDDformat(str1)),
                            //new SqlParameter("@sims_response_code",str)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims754 simsobj = new Sims754();

                             simsobj.sims_checklist_section_parameter_number = dr["sims_checklist_section_parameter_number"].ToString();
                             simsobj.sims_checklist_section_parameter_name = dr["sims_checklist_section_parameter_name"].ToString();
                             simsobj.sims_checklist_section_review_parameter_action = dr["sims_checklist_section_review_parameter_action"].ToString();
                             simsobj.sims_checklist_section_review_parameter_response = dr["sims_checklist_section_review_parameter_response"].ToString();
                             simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                             simsobj.sims_checklist_section_review_number = dr["sims_checklist_section_review_number"].ToString();
                             simsobj.sims_checklist_section_response_type = dr["sims_checklist_section_response_type"].ToString();
                             simsobj.sims_checklist_section_review_date = dr["sims_checklist_section_review_date"].ToString();
                             simsobj.sims_response_code = dr["sims_response_code"].ToString();
                             simsobj.sims_response_title = dr["sims_response_title"].ToString();
                             lst.Add(simsobj);

                         }
                     }
                 }
             }
             catch (Exception x)
             {

                 //message.strMessage = x.Message;
                 //message.systemMessage = string.Empty;
                 //message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }

        // chang section data
         [Route("changsectiondata")]
         public HttpResponseMessage changsectiondata(string str)   //, string str2 , string str1
         {

             List<Sims754> lst = new List<Sims754>();
             if (str == "undefined" || str == "")
             {
                 str = null;
             }
             //if (str1 == "undefined" || str1 == "")
             //{
             //    str1 = null;
             //}
             //if (str2 == "undefined" || str2 == "")
             //{
             //    str = null;
             //}
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                         new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'H'),
                           
                           new SqlParameter("@sims_checklist_section_number",str),
                           //new SqlParameter("@sims_checklist_section_review_date",db.DBYYYYMMDDformat(str1)),
                            //new SqlParameter("@sims_response_code",str)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims754 simsobj = new Sims754();
                              simsobj.sims_response_code = dr["sims_response_code"].ToString();
                             simsobj.sims_response_title = dr["sims_response_title"].ToString();
                             lst.Add(simsobj);

                         }
                     }
                 }
             }
             catch (Exception x)
             {

                 //message.strMessage = x.Message;
                 //message.systemMessage = string.Empty;
                 //message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }

         [Route("get_typebyList")]
         public HttpResponseMessage get_typebyList(string str)
         {

             List<Sims754> lst = new List<Sims754>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                         new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'N'),
                           new SqlParameter("@sims_response_code",str),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims754 simsobj = new Sims754();
                             simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();


                             lst.Add(simsobj);

                         }
                     }
                 }
             }
             catch (Exception x)
             {

                 //message.strMessage = x.Message;
                 //message.systemMessage = string.Empty;
                 //message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }

        // get data database
         [Route("get_ListReview")]
         public HttpResponseMessage get_ListReview()
         {

             List<Sims754> lst = new List<Sims754>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_review_PROC]",
                         new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims754 simsobj = new Sims754();

                             sims_checklist_section_review_number = dr["sims_checklist_section_review_number"].ToString();
                             simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                             (simsobj.sims_checklist_section_review_date) = dr["sims_checklist_section_review_date"].ToString();
                             simsobj.sims_checklist_section_review_by = dr["sims_checklist_section_review_by"].ToString();
                             simsobj.sims_checklist_section_review_parameter_number = dr["sims_checklist_section_review_parameter_number"].ToString();
                             simsobj.sims_checklist_section_review_parameter_response = dr["sims_checklist_section_review_parameter_response"].ToString();
                             simsobj.sims_checklist_section_review_parameter_action = dr["sims_checklist_section_review_parameter_action"].ToString();
                              (simsobj.sims_checklist_section_review_transaction_date) = dr["sims_checklist_section_review_transaction_date"].ToString();
                              simsobj.sims_checklist_section_response_type = dr["sims_checklist_section_response_type"].ToString();

                             lst.Add(simsobj);

                         }
                     }
                 }
             }
             catch (Exception x)
             {

                 //message.strMessage = x.Message;
                 //message.systemMessage = string.Empty;
                 //message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, lst);

         }
    }
} 