﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Attributes;
using log4net;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/Medical_Consent")]
    [BasicAuthentication]
    public class MedicalConsentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getStudentsParent")]
        public HttpResponseMessage getStudentsParent(string enroll)
        {
            List<Sims690> type_list = new List<Sims690>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_consent_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'J'),
                       new SqlParameter ("@sims_medical_consent_enroll_number",enroll)
                   });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims690 simsobj = new Sims690();
                            simsobj.sims_medical_consent_parent_number = dr["sims_sibling_parent_number"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("get_MedicalConsent")]
        public HttpResponseMessage get_MedicalConsent()
        {
            List<Sims690> type_list = new List<Sims690>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_consent_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims690 simsobj = new Sims690();
                            simsobj.sims_medical_consent_srl_no = dr["sims_medical_consent_srl_no"].ToString();
                            simsobj.sims_medical_consent_cur_code = dr["sims_medical_consent_cur_code"].ToString();
                            simsobj.cur_name = dr["cur_name"].ToString();
                            simsobj.sims_medical_consent_enroll_number = dr["sims_medical_consent_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.current_class = dr["grade"].ToString();
                            simsobj.sims_medical_consent_parent_number = dr["sims_medical_consent_parent_number"].ToString();
                            simsobj.parent_name = dr["parent_name"].ToString();
                            simsobj.sims_medical_consent_health_records = dr["sims_medical_consent_health_records"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_immunization = dr["sims_medical_consent_immunization"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_emergency_treatment = dr["sims_medical_consent_emergency_treatment"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_medical_treatment = dr["sims_medical_consent_medical_treatment"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_over_the_counter_medicine = dr["sims_medical_consent_over_the_counter_medicine"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_medicine_allowed = dr["sims_medical_consent_medicine_allowed"].Equals("Y") ? true : false;
                            simsobj.sims_medical_consent_attr1 = dr["sims_medical_consent_attr1"].ToString();
                            simsobj.sims_medical_consent_attr2 = dr["sims_medical_consent_attr2"].ToString();
                            simsobj.sims_medical_consent_attr3 = dr["sims_medical_consent_attr3"].ToString();
                            simsobj.sims_medical_consent_attr4 = dr["sims_medical_consent_attr4"].ToString();
                            simsobj.sims_medical_consent_attr5 = dr["sims_medical_consent_attr5"].ToString();
                            simsobj.sims_medical_consent_remark = dr["sims_medical_consent_remark"].ToString();
                            simsobj.sims_medical_consent_applicable_from_acad_year = dr["sims_medical_consent_applicable_from_acad_year"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_medical_consent_updated_on"].ToString()))
                            {
                                    //simsobj.sims_medical_consent_updated_on = DateTime.Parse(dr["sims_medical_consent_updated_on"].ToString()).ToShortDateString();
                                simsobj.sims_medical_consent_updated_on = db.UIDDMMYYYYformat(dr["sims_medical_consent_updated_on"].ToString());
                            }
                            simsobj.sims_medical_consent_approved_by = dr["sims_medical_consent_approved_by"].ToString();
                            simsobj.approved_by = dr["approved_by"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("Get_studentlist")]
        public HttpResponseMessage Get_studentlist(string enroll_no)
        {
            List<Sims613> lstcertificate = new List<Sims613>();
            if (enroll_no == "undefined" || enroll_no == "\"\"")
            {
                enroll_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_consent_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),
                           new SqlParameter("@sims_medical_consent_enroll_number", enroll_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims613 obj = new Sims613();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            //obj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_enroll_no = dr["sims_enroll_number"].ToString();
                            obj.stud_full_name = dr["S_name"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("CUDSims_MedicalConsent")]
        public HttpResponseMessage CUDSims_MedicalConsent(Sims690 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSims_MedicalConsent()";
            Log.Debug(string.Format(debug, "PP", "CUDSims_MedicalConsent"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_consent_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                           // new SqlParameter("@sims_medical_consent_srl_no", simsobj.sims_medical_consent_srl_no),
                            new SqlParameter("@sims_medical_consent_cur_code", simsobj.sims_medical_consent_cur_code),
                            new SqlParameter("@sims_medical_consent_enroll_number", simsobj.sims_medical_consent_enroll_number),
                            new SqlParameter("@sims_medical_consent_parent_number", simsobj.sims_medical_consent_parent_number),
                            new SqlParameter("@sims_medical_consent_health_records", simsobj.sims_medical_consent_health_records==true?"Y":"N"),
					        new SqlParameter("@sims_medical_consent_immunization",simsobj.sims_medical_consent_immunization==true?"Y":"N"),
					        new SqlParameter("@sims_medical_consent_emergency_treatment",simsobj.sims_medical_consent_emergency_treatment==true?"Y":"N"),
                            new SqlParameter("@sims_medical_consent_medical_treatment",simsobj.sims_medical_consent_medical_treatment==true?"Y":"N"),
					        new SqlParameter("@sims_medical_consent_over_the_counter_medicine",simsobj.sims_medical_consent_over_the_counter_medicine==true?"Y":"N"),
					        new SqlParameter("@sims_medical_consent_medicine_allowed",simsobj.sims_medical_consent_medicine_allowed==true?"Y":"N"),
					        new SqlParameter("@sims_medical_consent_attr1",simsobj.sims_medical_consent_attr1),
					        new SqlParameter("@sims_medical_consent_attr2",simsobj.sims_medical_consent_attr2),
					        new SqlParameter("@sims_medical_consent_attr3",simsobj.sims_medical_consent_attr3),
					        new SqlParameter("@sims_medical_consent_attr4",simsobj.sims_medical_consent_attr4),
					        new SqlParameter("@sims_medical_consent_attr5",simsobj.sims_medical_consent_attr5),
					        new SqlParameter("@sims_medical_consent_remark",simsobj.sims_medical_consent_remark),
					        new SqlParameter("@sims_medical_consent_applicable_from_acad_year",simsobj.sims_medical_consent_applicable_from_acad_year),
					        //new SqlParameter("@sims_medical_consent_updated_on",simsobj.sims_medical_consent_updated_on),
					        new SqlParameter("@sims_medical_consent_approved_by",simsobj.sims_medical_consent_approved_by)
                            //new SqlParameter("@sims_library_attribute_status", simsobj.sims_library_attribute_status==true?"Y":"N")
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
 
    }
}