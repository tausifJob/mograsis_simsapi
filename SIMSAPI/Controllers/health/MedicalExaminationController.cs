﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.MedicalExaminationController
{
    [RoutePrefix("api/common/MedicalExamination")]
    [BasicAuthentication]
    public class MedicalExaminationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllMedicalVisitNumber")]
        public HttpResponseMessage GetAllMedicalVisitNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims093> list = new List<Sims093>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_examination",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsobj = new Sims093();
                            simsobj.sims_visit_number = dr["sims_medical_visit_number"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllMedicalExaminationName")]
        public HttpResponseMessage GetAllMedicalExaminationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims093> list = new List<Sims093>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_examination",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsobj = new Sims093();
                            simsobj.sims_examination_code = dr["sims_examination_code"].ToString();
                            simsobj.sims_examination_name = dr["sims_examination_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllMedicalExamination")]
        public HttpResponseMessage GetAllMedicalExamination()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims093> list = new List<Sims093>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_examination",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsobj = new Sims093();
                            simsobj.sims_visit_number = dr["sims_visit_number"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_serial_number"].ToString()))
                            simsobj.sims_serial_number = dr["sims_serial_number"].ToString();
                            simsobj.sims_examination_name = dr["sims_examination_name"].ToString();
                            simsobj.sims_examination_code = dr["sims_examination_code"].ToString();
                            simsobj.sims_examination_value = dr["sims_examination_value"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalExamination")]
        public HttpResponseMessage InsertSims_MedicalExamination(Sims093 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalExamination()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalExamination"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_medical_examination",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_visit_number", simsobj.sims_visit_number),
                            new SqlParameter("@sims_serial_number", simsobj.sims_serial_number),
                            new SqlParameter("@sims_examination_code", simsobj.sims_examination_code),
                            new SqlParameter("@sims_examination_value", simsobj.sims_examination_value)
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Delete_Sims_MedicalExamination")]
        public HttpResponseMessage Delete_Sims_MedicalExamination(List<Sims093> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Delete_Sims_MedicalExamination()";
            Log.Debug(string.Format(debug, "PP", "Delete_Sims_MedicalExamination"));

            bool inserted = false;
            try
            {
                foreach (Sims093 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_medical_examination",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_visit_number", simsobj.sims_visit_number),
                            new SqlParameter("@sims_serial_number", simsobj.sims_serial_number),
                            new SqlParameter("@sims_examination_name", simsobj.sims_examination_name),
                            new SqlParameter("@sims_examination_value", simsobj.sims_examination_value)
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}