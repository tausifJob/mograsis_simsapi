﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.StudentHealthCSDController
{
    [RoutePrefix("api/StudentHealthSCD")]
    [BasicAuthentication]
    public class StudentHealthCSDController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region
        [Route("getAllStudentHealth")]
        public HttpResponseMessage getAllStudentHealth(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sim098> list = new List<Sim098>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim098 simsobj = new Sim098();
                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_health_card_issue_date"].ToString());
                            simsobj.sims_health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_health_card_expiry_date"].ToString());
                            simsobj.sims_health_card_issuing_authority1 = dr["sims_health_card_issuing_authority"].ToString();
                            simsobj.sims_blood_group_code = dr["sims_blood_group_code"].ToString();
                            simsobj.sims_blood_group_name = dr["sims_blood_group_name"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("Y") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_disability_status = dr["sims_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_health_hearing_status = dr["sims_health_hearing_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                            simsobj.sims_health_vision_status = dr["sims_health_vision_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                            simsobj.sims_health_other_status = dr["sims_health_other_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                            simsobj.sims_regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                            simsobj.sims_regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                            simsobj.sims_regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                            simsobj.sims_regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                            simsobj.sims_has_your_child_any_other_disability_status = dr["sims_health_other_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_has_your_child_any_other_disability = dr["sims_health_other_disability_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getGenderDOB")]
        public HttpResponseMessage getGenderDOB(string S_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "J"),
                              new SqlParameter("@sims_enroll_number", S_number),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            if (S_number != "null")
                            {
                                simsobj.sub_class = dr["Student_Info"].ToString();
                                simsobj.sims_student_img = dr["sims_student_img"].ToString();
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getBloodGroup")]
        public HttpResponseMessage getBloodGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBloodGroup(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getBloodGroup"));

            List<Sim098> code_list = new List<Sim098>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetEMPData_Temp", new List<SqlParameter>());
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim098 obj = new Sim098();
                            obj.sims_blood_group_name = dr["sims_appl_form_field_value1_blood"].ToString();
                            obj.sims_blood_group_code = dr["sims_appl_parameter_blood"].ToString();

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("CUDStudentHealth")]
        public HttpResponseMessage CUDStudentHealth(List<Sim098> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim098 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_student_proc]",
                         new List<SqlParameter>() 
                         { 

                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_student_enroll_number",simsobj.enroll_number),
                               new SqlParameter("@sims_health_card_number",simsobj.sims_health_card_number),
                               new SqlParameter("@sims_health_card_issue_date", db.DBYYYYMMDDformat(simsobj.sims_health_card_issue_date)),
                               new SqlParameter("@sims_health_card_expiry_date",db.DBYYYYMMDDformat(simsobj.sims_health_card_expiry_date)),
                               new SqlParameter("@sims_health_card_issuing_authority",simsobj.sims_health_card_issuing_authority1),
                               new SqlParameter("@sims_blood_group_code",simsobj.sims_blood_group_code),
                               new SqlParameter("@sims_height",simsobj.sims_height),
                               new SqlParameter("@sims_wieght",simsobj.sims_wieght),
                               new SqlParameter("@sims_teeth",simsobj.sims_teeth),
                               new SqlParameter("@sims_health_bmi",simsobj.sims_health_bmi),
                               new SqlParameter("@sims_medication_status",simsobj.sims_medication_status.Equals(true)?"Y":"N"), 
                               new SqlParameter("@sims_medication_desc",simsobj.sims_medication_desc),
                               new SqlParameter("@sims_disability_status",simsobj.sims_disability_status.Equals(true)?"Y":"N"), 
                               new SqlParameter("@sims_disability_desc", simsobj.sims_disability_desc),
                               new SqlParameter("@sims_health_restriction_status",simsobj.sims_health_restriction_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_restriction_desc",simsobj.sims_health_restriction_desc), 
                               new SqlParameter("@sims_health_hearing_status",simsobj.sims_health_hearing_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_hearing_desc", simsobj.sims_health_hearing_desc ),
                               new SqlParameter("@sims_health_vision_status",simsobj.sims_health_vision_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_vision_desc", simsobj.sims_health_vision_desc),
                               new SqlParameter("@sims_health_other_status",  simsobj.sims_health_other_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_other_desc",simsobj.sims_health_other_desc),
                               new SqlParameter("@sims_health_other_disability_status",  simsobj.sims_has_your_child_any_other_disability_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_other_disability_desc",simsobj.sims_has_your_child_any_other_disability),
                            
                               new SqlParameter("@sims_regular_hospital_name",simsobj.sims_regular_hospital_name), 
                               new SqlParameter("@sims_regular_hospital_phone",simsobj.sims_regular_hospital_phone),
                               new SqlParameter("@sims_regular_doctor_name",simsobj.sims_regular_doctor_name),
                               new SqlParameter("@sims_regular_doctor_phone", simsobj.sims_regular_doctor_phone),

                               new SqlParameter("@sims_Consent_medication_status",  simsobj.sims_Consent_medication_status.Equals(true)?"Y":"N"),
                               //new SqlParameter("@sims_Consent_medication_desc", simsobj.sims_Consent_medication_desc),
                               
                                new SqlParameter("@sims_Consent_medical_Examination_status",  simsobj.sims_Consent_medical_Examination_status.Equals(true)?"Y":"N"),
                               // new SqlParameter("@sims_Consent_medical_Examination_desc", simsobj.sims_Consent_medical_Examination_desc),
                               
                               new SqlParameter("@sims_Consent_medical_immunization_status",  simsobj.sims_Consent_medical_immunization_status.Equals(true)?"Y":"N"),
                              // new SqlParameter("@sims_Consent_medical_immunization_desc", simsobj.sims_Consent_medical_immunization_desc),
                              
                                new SqlParameter("@sims_submitted_vaccination_stauts",  simsobj.sims_submitted_vaccination_stauts.Equals(true)?"Y":"N"),
                                //new SqlParameter("@sims_submitted_vaccination_desc", simsobj.sims_submitted_vaccination_desc),
                               
                                new SqlParameter("@sims_medical_remark", simsobj.sims_medical_remark),


                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getStudHealthSearch")]
        public HttpResponseMessage getStudHealthSearch(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim098> stud_list = new List<Sim098>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'), 
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sim098 simsobj = new Sim098();
                            simsobj.icon = "fa fa-plus-circle";

                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_health_card_issue_date"].ToString());
                            simsobj.sims_health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_health_card_expiry_date"].ToString());
                            simsobj.sims_health_card_issuing_authority1 = dr["sims_health_card_issuing_authority"].ToString();
                            simsobj.sims_blood_group_code = dr["sims_blood_group_code"].ToString();
                            simsobj.sims_blood_group_name = dr["sims_blood_group_name"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("Y") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_disability_status = dr["sims_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_health_hearing_status = dr["sims_health_hearing_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                            simsobj.sims_health_vision_status = dr["sims_health_vision_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                            simsobj.sims_health_other_status = dr["sims_health_other_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                            simsobj.sims_regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                            simsobj.sims_regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                            simsobj.sims_regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                            simsobj.sims_regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                            simsobj.sims_has_your_child_any_other_disability_status = dr["sims_health_other_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_has_your_child_any_other_disability = dr["sims_health_other_disability_desc"].ToString();

                            simsobj.sims_Consent_medication_status = dr["sims_Consent_medication_status"].Equals("Y") ? true : false;
                            simsobj.sims_Consent_medication_desc = dr["sims_Consent_medication_desc"].ToString();
                            simsobj.sims_Consent_medical_Examination_status = dr["sims_Consent_medical_Examination_status"].Equals("Y") ? true : false;
                            simsobj.sims_Consent_medical_Examination_desc = dr["sims_Consent_medical_Examination_desc"].ToString();
                            simsobj.sims_Consent_medical_immunization_status = dr["sims_Consent_medical_immunization_status"].Equals("Y") ? true : false;
                            simsobj.sims_Consent_medical_immunization_desc = dr["sims_Consent_medical_immunization_desc"].ToString();
                            simsobj.sims_submitted_vaccination_stauts = dr["sims_submitted_vaccination_stauts"].Equals("Y") ? true : false;
                            simsobj.sims_submitted_vaccination_desc = dr["sims_submitted_vaccination_desc"].ToString();

                            simsobj.sims_medical_remark = dr["sims_medical_remark"].ToString();
                            
                            stud_list.Add(simsobj);


                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        #endregion

        #region
        /*
        [Route("getAllStudentHealth")]
        public HttpResponseMessage GetAllMedicalSusceptibilityCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sim098> list = new List<Sim098>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "S"),
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim098 simsobj = new Sim098();
                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_card_issue_date = dr["sims_health_card_issue_date"].ToString();
                            simsobj.sims_health_card_expiry_date = dr["sims_health_card_expiry_date"].ToString();
                            simsobj.sims_health_card_issuing_authority = dr["sims_health_card_issuing_authority"].ToString();
                            simsobj.sims_blood_group_code = dr["sims_blood_group_code"].ToString();
                            simsobj.sims_blood_group_name = dr["sims_blood_group_name"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("Y") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_disability_status = dr["sims_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_health_hearing_status = dr["sims_health_hearing_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                            simsobj.sims_health_vision_status = dr["sims_health_vision_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                            simsobj.sims_health_other_status = dr["sims_health_other_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                            simsobj.sims_regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                            simsobj.sims_regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                            simsobj.sims_regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                            simsobj.sims_regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getBloodGroup")]
        public HttpResponseMessage getBloodGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBloodGroup(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getBloodGroup"));

            List<Sim098> code_list = new List<Sim098>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetEMPData_Temp", new List<SqlParameter>());
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim098 obj = new Sim098();
                            obj.sims_blood_group_name = dr["sims_appl_form_field_value1_blood"].ToString();
                            obj.sims_blood_group_code = dr["sims_appl_parameter_blood"].ToString();

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("CUDStudentHealth")]
        public HttpResponseMessage CUDStudentHealth(List<Sim098> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim098 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_student_proc]",
                            new List<SqlParameter>() 
                         { 

                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_student_enroll_number",simsobj.enroll_number),
                               new SqlParameter("@sims_health_card_number",simsobj.sims_health_card_number),
                               new SqlParameter("@sims_health_card_issue_date", simsobj.sims_health_card_issue_date) ,
                               new SqlParameter("@sims_health_card_expiry_date", simsobj.sims_health_card_expiry_date ),
                               new SqlParameter("@sims_health_card_issuing_authority",simsobj.sims_health_card_issuing_authority),
                               new SqlParameter("@sims_blood_group_code",simsobj.sims_blood_group_code),
                               new SqlParameter("@sims_height",simsobj.sims_height),
                               new SqlParameter("@sims_wieght",simsobj.sims_wieght),
                               new SqlParameter("@sims_teeth",simsobj.sims_teeth),
                               new SqlParameter("@sims_health_bmi",simsobj.sims_health_bmi),
                               new SqlParameter("@sims_medication_status",simsobj.sims_medication_status.Equals(true)?"Y":"N"), 
                               new SqlParameter("@sims_medication_desc",simsobj.sims_medication_desc),
                               new SqlParameter("@sims_disability_status",simsobj.sims_disability_status.Equals(true)?"Y":"N"), 
                               new SqlParameter("@sims_disability_desc", simsobj.sims_disability_desc),
                               new SqlParameter("@sims_health_restriction_status",simsobj.sims_health_restriction_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_restriction_desc",simsobj.sims_health_restriction_desc), 
                               new SqlParameter("@sims_health_hearing_status",simsobj.sims_health_hearing_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_hearing_desc", simsobj.sims_health_hearing_desc ),
                               new SqlParameter("@sims_health_vision_status",simsobj.sims_health_vision_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_vision_desc", simsobj.sims_health_vision_desc),
                               new SqlParameter("@sims_health_other_status",  simsobj.sims_health_other_status.Equals(true)?"Y":"N"),
                               new SqlParameter("@sims_health_other_desc",simsobj.sims_health_other_desc),
                               new SqlParameter("@sims_regular_hospital_name",simsobj.sims_regular_hospital_name), 
                               new SqlParameter("@sims_regular_hospital_phone",simsobj.sims_regular_hospital_phone),
                               new SqlParameter("@sims_regular_doctor_name",simsobj.sims_regular_doctor_name),
                               new SqlParameter("@sims_regular_doctor_phone", simsobj.sims_regular_doctor_phone),
                         
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getStudHealthSearch")]
        public HttpResponseMessage getStudHealthSearch(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim098> stud_list = new List<Sim098>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'), 
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sim098 simsobj = new Sim098();
                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_card_issue_date = dr["sims_health_card_issue_date"].ToString();
                            simsobj.sims_health_card_expiry_date = dr["sims_health_card_expiry_date"].ToString();
                            simsobj.sims_health_card_issuing_authority = dr["sims_health_card_issuing_authority"].ToString();
                            simsobj.sims_blood_group_code = dr["sims_blood_group_code"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("Y") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_disability_status = dr["sims_disability_status"].Equals("Y") ? true : false;
                            simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_health_hearing_status = dr["sims_health_hearing_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                            simsobj.sims_health_vision_status = dr["sims_health_vision_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                            simsobj.sims_health_other_status = dr["sims_health_other_status"].Equals("Y") ? true : false;
                            simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                            simsobj.sims_regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                            simsobj.sims_regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                            simsobj.sims_regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                            simsobj.sims_regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                            stud_list.Add(simsobj);


                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        */
        #endregion

    }
}