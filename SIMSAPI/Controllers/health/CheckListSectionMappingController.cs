﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/checkListSectionMapping")]
    public class CheckListSectionMappingController: ApiController
    {

        [Route("get_sectionList")]
        public HttpResponseMessage get_sectionList()
        {

            List<Sims555> lst = new List<Sims555>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_mapping_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims555 simsobj = new Sims555();
                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();
                            

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_checkbyList")]
        public HttpResponseMessage get_checkbyList()
        {

            List<Sims555> lst = new List<Sims555>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_mapping_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims555 simsobj = new Sims555();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_typebyList")]
        public HttpResponseMessage get_typebyList()
        {

            List<Sims555> lst = new List<Sims555>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_mapping_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'T'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims555 simsobj = new Sims555();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDCheckList")]
        public HttpResponseMessage CUDdemo(List<Sims555> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a=0;
                    db.Open();
                    foreach (Sims555 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_checklist_section_mapping_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_checklist_section_mapping_checklist_section_number", simsobj.sims_checklist_section_mapping_checklist_section_number),
                                new SqlParameter("@sims_checklist_section_mapping_employee_code", simsobj.sims_checklist_section_mapping_employee_code),   
                                new SqlParameter("@sims_checklist_section_mapping_type", simsobj.sims_checklist_section_mapping_type),
                                new SqlParameter("@sims_checklist_section_mapping_apply_date", simsobj.sims_checklist_section_mapping_apply_date),
                                new SqlParameter("@sims_checklist_section_mapping_created_by", simsobj.sims_checklist_section_mapping_created_by),   
                                new SqlParameter("@sims_checklist_section_mapping_frequency", simsobj.sims_checklist_section_mapping_frequency),
                                new SqlParameter("@sims_checklist_section_mapping_frequency_count", simsobj.sims_checklist_section_mapping_frequency_count),
                                new SqlParameter("@sims_checklist_section_mapping_status", simsobj.sims_checklist_section_mapping_status),
                                new SqlParameter("@flagchec", a),
                               
                     });

                     
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("get_mappingList")]
        public HttpResponseMessage get_mappingList( string section)
        {

            List<Sims555> lst = new List<Sims555>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_mapping_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@SIMS_CHECKLIST_SECTION_MAPPING_CHECKLIST_SECTION_NUMBER", section),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims555 simsobj = new Sims555();
                            simsobj.sims_checklist_section_mapping_employee_code = dr["sims_checklist_section_mapping_employee_code"].ToString();
                            simsobj.sims_checklist_section_mapping_type = dr["sims_checklist_section_mapping_type"].ToString();

                            simsobj.sims_checklist_section_mapping_frequency = dr["sims_checklist_section_mapping_frequency"].ToString();
                            simsobj.sims_checklist_section_mapping_frequency_count = dr["sims_checklist_section_mapping_frequency_count"].ToString();
                            simsobj.sims_section_name = dr["SIMS_CHECKLIST_SECTION_NAME"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }




    }
}