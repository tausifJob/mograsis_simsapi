﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.Medical_Visit_Controller
{
    [RoutePrefix("api/common/Medical_Visit")]
    [BasicAuthentication]
    public class Medical_Visit_Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("GetStudent_Medical_Visit")]
        public HttpResponseMessage GetStudent_Medical_Visit()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudent_Medical_Visit(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudent_Medical_Visit"));

            List<Sims101> list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_visit",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                            simsobj.medical_visit_date = db.UIDDMMYYYYformat(dr["sims_medical_visit_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_employee_number = dr["sims_employee_number"].ToString();
                            simsobj.medical_attended_by = dr["sims_attended_by"].ToString();
                            simsobj.medical_complaint_desc = dr["sims_complaint_desc"].ToString();
                            simsobj.medical_attendant_observation = dr["sims_attendant_observation"].ToString();
                            simsobj.medical_health_education = dr["sims_health_education"].ToString();
                            simsobj.medical_followup_comment = dr["sims_followup_comment"].ToString();
                            simsobj.sims_user_code_created_by = dr["sims_user_code_created_by"].ToString();
                            simsobj.sims_Emp_name = dr["EmpName"].ToString();
                            simsobj.sims_student_name = dr["StudName"].ToString();
                            simsobj.sims_CreatedBy_name = dr["CreatedByName"].ToString();
                            simsobj.sims_AttendedBy_name = dr["AttByName"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertStudent_Medical_Visit")]
        public HttpResponseMessage InsertStudent_Medical_Visit(Sims101 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_medical_visit",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_medical_visit_number", simsobj.sims_medical_visit_number),
                            new SqlParameter("@sims_medical_visit_date", db.DBYYYYMMDDformat(simsobj.medical_visit_date)),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_employee_number", simsobj.sims_employee_number),
                            new SqlParameter("@sims_attended_by", simsobj.medical_attended_by),
                            new SqlParameter("@sims_complaint_desc", simsobj.medical_complaint_desc),
                            new SqlParameter("@sims_attendant_observation", simsobj.medical_attendant_observation),
                            new SqlParameter("@sims_health_education", simsobj.medical_health_education),
                            new SqlParameter("@sims_followup_comment", simsobj.medical_followup_comment),
                            new SqlParameter("@sims_user_code_created_by", simsobj.sims_user_code_created_by),
                            new SqlParameter("@sims_medical_examination_name", simsobj.sims_medical_examination_name),
                            new SqlParameter("@sims_appl_code", "Sim101"),
                            
                            new SqlParameter("@comn_sequence_code", "Mvn")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getAllMedicalVisitExamination")]
        public HttpResponseMessage getAllMedicalVisitExamination()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllMedicalVisitExamination(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getAllMedicalVisitExamination"));

            List<Sims102> list = new List<Sims102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_visit",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", 'E')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims102 simsobj = new Sims102();
                            simsobj.sims_examination_code = dr["sims_examination_code"].ToString();
                            simsobj.sims_examination_name = dr["sims_examination_name"].ToString();
                            simsobj.sims_examination_desc = dr["sims_examination_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getMedicineName")]
        public HttpResponseMessage getMedicineName(string str)
        {

            List<Sims202> lst = new List<Sims202>();
            if (str == "undefined" || str == "")
            {
                str = null;
            }
            //if (str1 == "undefined" || str1 == "")
            //{
            //    str1 = null;
            //}
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),
                           //new SqlParameter("@SIMS_CHECKLIST_SECTION_NAME",str),
                           new SqlParameter("@sims_medicine_code",str)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims202 simsobj = new Sims202();

                            simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }
    }
}