﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Attributes;
using log4net;

namespace SIMSAPI.Controllers.modules.Medical_Immunization_StudentController
{
    [RoutePrefix("api/Medical_Immunization_Student")]
    [BasicAuthentication]
    public class Medical_Immunization_StudentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getDosage")]
        public HttpResponseMessage getDosage(string str)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_student_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@value",str),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_dosage1 = dr["sims_immunization_dosage1"].ToString();
                            simsobj.sims_immunization_dosage2 = dr["sims_immunization_dosage2"].ToString();
                            simsobj.sims_immunization_dosage3 = dr["sims_immunization_dosage3"].ToString();
                            simsobj.sims_immunization_dosage4 = dr["sims_immunization_dosage4"].ToString();
                            simsobj.sims_immunization_dosage5 = dr["sims_immunization_dosage5"].ToString();
                            
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("GetAllMedicalImmunization_Code")]
        public HttpResponseMessage GetAllMedicalImmunization_Code()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "[sims].[sims_medical_immunization_student_proc1]"),
                            new SqlParameter("@tbl_col_name1", "[sims_immunization_desc],[sims_immunization_code]"),
                            new SqlParameter("@tbl_cond", "[sims_immunization_status]='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_student_code = dr["sims_immunization_code"].ToString();
                            simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_immunization_code = dr["sims_immunization_code"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSims_Medical_Student_Immunization")]
        public HttpResponseMessage GetSims_Medical_Student_Immunization(string immunization_desc)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));
            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_immunization",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                            new SqlParameter("@sims_immunization_code", immunization_desc),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_student_code = dr["sims_immunization_code"].ToString();
                            simsobj.sims_immunization_code = dr["sims_immunization_code"].ToString();
                            simsobj.sims_immunization_age_group_name = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_immunization_dosage1 = dr["sims_immunization_dosage1"].ToString();
                            simsobj.sims_immunization_dosage2 = dr["sims_immunization_dosage2"].ToString();
                            simsobj.sims_immunization_dosage3 = dr["sims_immunization_dosage3"].ToString();
                            simsobj.sims_immunization_dosage4 = dr["sims_immunization_dosage4"].ToString();
                            simsobj.sims_immunization_dosage5 = dr["sims_immunization_dosage5"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetStudent_Medical_Immunization")]
        public HttpResponseMessage GetStudent_Medical_Immunization()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_student_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_age_group_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_immunization_code = dr["sims_immunization_code"].ToString();
                            
                            //if (!string.IsNullOrEmpty(dr["sims_vaccination_date"].ToString()))
                            //    simsobj.sims_vaccination_date = DateTime.Parse(dr["sims_vaccination_date"].ToString()).ToShortDateString();
                            //if (!string.IsNullOrEmpty(dr["sims_vaccination_renewal_date"].ToString()))
                            //    simsobj.sims_vaccination_renewal_date = DateTime.Parse(dr["sims_vaccination_renewal_date"].ToString()).ToShortDateString();

                            simsobj.sims_vaccination_date = db.UIDDMMYYYYformat(dr["sims_vaccination_date"].ToString());
                            simsobj.sims_vaccination_renewal_date = db.UIDDMMYYYYformat(dr["sims_vaccination_renewal_date"].ToString());
                            // simsobj.sims_vaccination_date = dr["sims_vaccination_renewal_date"].ToString();
                            //  simsobj.sims_vaccination_renewal_date = dr["sims_vaccination_renewal_date"].ToString();
                            simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_immunization_dosage1 = dr["sims_immunization_dosage1_comment"].ToString();
                            simsobj.sims_immunization_dosage2 = dr["sims_immunization_dosage2_comment"].ToString();
                            simsobj.sims_immunization_dosage3 = dr["sims_immunization_dosage3_comment"].ToString();
                            simsobj.sims_immunization_dosage4 = dr["sims_immunization_dosage4_comment"].ToString();
                            simsobj.sims_immunization_dosage5 = dr["sims_immunization_dosage5_comment"].ToString();
                            ////simsobj.sims_enroll_numbers = dr["sims_enroll_numbers"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertStudent_Medical_Immunization")]
        public HttpResponseMessage InsertStudent_Medical_Immunization(List<Sims094> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims094 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_immunization_student_proc]",

                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_code),
                             new SqlParameter("@sims_immunization_code", simsobj.sims_immunization_code),
                             new SqlParameter("@sims_vaccination_date",db.DBYYYYMMDDformat(simsobj.sims_vaccination_date)),
                             new SqlParameter("@sims_vaccination_renewal_date", db.DBYYYYMMDDformat(simsobj.sims_vaccination_renewal_date)),
                             new SqlParameter("@sims_immunization_desc", simsobj.sims_immunization_desc),
                             new SqlParameter("@sims_immunization_dosage1_comment", simsobj.sims_immunization_dosage1),
                             new SqlParameter("@sims_immunization_dosage2_comment", simsobj.sims_immunization_dosage2),
                             new SqlParameter("@sims_immunization_dosage3_comment", simsobj.sims_immunization_dosage3),
                             new SqlParameter("@sims_immunization_dosage4_comment", simsobj.sims_immunization_dosage4),
                             new SqlParameter("@sims_immunization_dosage5_comment", simsobj.sims_immunization_dosage5),
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("Delete_sims_medical_immunization_age_group")]
        public HttpResponseMessage Delete_sims_medical_immunization_age_group(List<Sims094> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Delete_Sims_MedicalExamination()";
            Log.Debug(string.Format(debug, "PP", "Delete_Sims_MedicalExamination"));

            bool inserted = false;
            try
            {
                foreach (Sims094 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_medical_immunization_age_group",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_name),
                            new SqlParameter("@sims_immunization_code", simsobj.sims_immunization_desc),
                            new SqlParameter("@sims_vaccination_date",simsobj.sims_vaccination_date),
                            new SqlParameter("@sims_vaccination_renewal_date", db.DBYYYYMMDDformat(simsobj.sims_vaccination_renewal_date)),
                            new SqlParameter("@sims_immunization_desc", simsobj.sims_immunization_desc),
                            new SqlParameter("@sims_immunization_dosage1_comment", simsobj.sims_immunization_dosage1),
                            new SqlParameter("@sims_immunization_dosage2_comment", simsobj.sims_immunization_dosage2),
                            new SqlParameter("@sims_immunization_dosage3_comment", simsobj.sims_immunization_dosage3),
                            new SqlParameter("@sims_immunization_dosage4_comment", simsobj.sims_immunization_dosage4),
                            new SqlParameter("@sims_immunization_dosage5_comment", simsobj.sims_immunization_dosage5),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Delete_sims_medical_immunization_age_group1")]
        public HttpResponseMessage Delete_sims_medical_immunization_age_group1(List<Sims094New> collection1)
        {
            //  Comn006 collection=
            //  Comn006 collection = Newtonsoft.Json.JsonConvert.DeserializeObject<Comn006>(collection1);

            bool flag = false;
            foreach (Sims094New simsobj in collection1)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", "D"));
                        para.Add(new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number));
                        para.Add(new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_code));
                        para.Add(new SqlParameter("@sims_immunization_code", simsobj.sims_immunization_code));
                        SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application", para);
                        flag = dr.RecordsAffected > 0 ? true : false;
                    }
                }
                catch (Exception e)
                {

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

    }
}