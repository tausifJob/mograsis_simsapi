﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.MedicalImmunizationGroupController
{
    [RoutePrefix("api/common/MedicalExamination")]
    [BasicAuthentication]
    public class MedicalImmunizationGroupController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetSims_MedicalImmunizationGroup")]
        public HttpResponseMessage GetSims_MedicalImmunizationGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims095> list = new List<Sims095>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_age_group_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims095 simsobj = new Sims095();

                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_age_group_desc = dr["sims_immunization_age_group_desc"].ToString();
                            if (dr["sims_immunization_age_group_status"].ToString().Equals("A"))
                            {
                                simsobj.sims_immunization_age_group_status = true;
                            }
                            else
                            {
                                simsobj.sims_immunization_age_group_status = false;
                            }

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalImmunizationGroup")]
        public HttpResponseMessage InsertSims_MedicalImmunizationGroup(List<Sims095> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunizationGroup()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunizationGroup"));

            bool inserted = false;
            try
            {
                foreach (Sims095 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_immunization_age_group_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_code),
                            new SqlParameter("@sims_immunization_age_group_desc", simsobj.sims_immunization_age_group_desc),
                            new SqlParameter("@sims_immunization_age_group_status", simsobj.sims_immunization_age_group_status ? "A" : "I")
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
      
        [Route("CUDAgeGroup")]
        public HttpResponseMessage CUDAgeGroup(List<Sims095> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Delete_Sims_MedicalExamination()";
            Log.Debug(string.Format(debug, "PP", "Delete_Sims_MedicalExamination"));

            bool inserted = false;
            try
            {
                foreach (Sims095 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_immunization_age_group_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_code),
                            new SqlParameter("@sims_immunization_age_group_desc", simsobj.sims_immunization_age_group_desc),
                            new SqlParameter("@sims_immunization_age_group_status", simsobj.sims_immunization_age_group_status ? "A" : "I")
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}