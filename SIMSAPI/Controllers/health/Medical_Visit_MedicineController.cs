﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.Medical_Visit_MedicineController
{
    [RoutePrefix("api/common/Medical_Visit_Medicine")]
    [BasicAuthentication]
    public class Medical_Visit_MedicineController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllVisitNumber")]
        public HttpResponseMessage GetAllVisitNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudent_Medical_Visit(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudent_Medical_Visit"));

            List<Sims103> list = new List<Sims103>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_visit_medicine",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'G')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims103 simsobj = new Sims103();
                            simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                            simsobj.sims_medical_visit_numberName = dr["sims_medical_visit_numberName"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllMedicalMedicineCode")]
        public HttpResponseMessage GetAllMedicalMedicineCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllMedicalMedicineCode(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllMedicalMedicineCode"));

            List<Sims103> list = new List<Sims103>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_visit_medicine",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", 'A')
                        });
                    
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims103 simsobj = new Sims103();
                            simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSims_MedicalVisitMedicine")]
        public HttpResponseMessage GetSims_MedicalVisitMedicine()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_MedicalVisitMedicine(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSims_MedicalVisitMedicine"));

            List<Sims103> list = new List<Sims103>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_visit_medicine",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'S')
                        });
                    
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims103 simsobj = new Sims103();
                            simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                            simsobj.sims_serial_number = dr["sims_serial_number"].ToString();
                            simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            simsobj.sims_medicine_dosage = dr["sims_medicine_dosage"].ToString();
                            simsobj.sims_medicine_duration = dr["sims_medicine_duration"].ToString();
                             simsobj.sims_medical_visit_date = dr["sims_medical_visit_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.medicine_given_by = dr["medicine_given_by"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            




                            if (dr["sims_status"].ToString().Equals("A"))
                            {
                                simsobj.sims_status = true;
                            }
                            else
                            {
                                simsobj.sims_status = false;
                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalVisitMedicine")]
        public HttpResponseMessage InsertSims_MedicalVisitMedicine(List<Sims103> data)
        {
           bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims103 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims_medical_visit_medicine",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@sims_medical_visit_number", simsobj.sims_medical_visit_number),
                          new SqlParameter("@sims_serial_number", simsobj.sims_serial_number),
                          new SqlParameter("@sims_medicine_code", simsobj.sims_medicine_code),
                          new SqlParameter("@sims_medicine_dosage", simsobj.sims_medicine_dosage),
                          new SqlParameter("@sims_medicine_duration", simsobj.sims_medicine_duration),
                          new SqlParameter("@sims_status", simsobj.sims_status ? "A" : "I")
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}