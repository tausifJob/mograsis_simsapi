﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/ListSection")]
    public class ListSectionController : ApiController
    {


        [Route("get_parentbyList")]
        public HttpResponseMessage get_parentbyList()
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_sectionbyList")]
        public HttpResponseMessage get_sectionbyList()
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'T'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_listsection")]
        public HttpResponseMessage get_listsection()
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();

                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();
                            simsobj.sims_checklist_section_name_ot = dr["sims_checklist_section_name_ot"].ToString();
                            simsobj.sims_checklist_section_desc = dr["sims_checklist_section_desc"].ToString();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_parent_name = dr["sims_checklist_section_parent_name"].ToString();
                            simsobj.sims_checklist_section_response_type = dr["sims_checklist_section_response_type"].ToString();
                            simsobj.sims_response_code = dr["sims_response_code"].ToString();
                            simsobj.sims_checklist_section_status = dr["sims_checklist_section_status"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_responsedata")]
        public HttpResponseMessage get_responsedata(string str)
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'K'),
                           new SqlParameter("@sims_response_code",str)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();

                            simsobj.sims_response_title = dr["sims_response_title"].ToString();
                            simsobj.sims_response_status = dr["sims_response_status"].ToString().Equals("A") ? true : false;

                            simsobj.sims_response_detail = dr["sims_response_detail"].ToString();
                            simsobj.sims_response_detail_ot = dr["sims_response_detail_ot"].ToString();
                            simsobj.sims_response_detail_status = dr["sims_response_detail_status"].ToString().Equals("A") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_typebyList")]
        public HttpResponseMessage get_typebyList()
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDListSection")]
        public HttpResponseMessage CUDListSection(List<Sims789> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims789 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_checklist_section_number", simsobj.sims_checklist_section_number),
                                new SqlParameter("@sims_checklist_section_name", simsobj.sims_checklist_section_name),
                                new SqlParameter("@sims_checklist_section_name_ot", simsobj.sims_checklist_section_name_ot),   
                                new SqlParameter("@sims_checklist_section_desc", simsobj.sims_checklist_section_desc),
                                new SqlParameter("@sims_checklist_section_parent_number", simsobj.sims_checklist_section_parent_number),
                                new SqlParameter("@sims_checklist_section_response_type", simsobj.sims_checklist_section_response_type),
                                 new SqlParameter("@sims_response_code", simsobj.sims_response_code),
                                new SqlParameter("@sims_checklist_section_status", simsobj.sims_checklist_section_status==true?"1":"0"), 
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("searchdropdown")]
        public HttpResponseMessage searchdropdown(string str)
        {

            List<Sims789> lst = new List<Sims789>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Q'),
                           new SqlParameter("@sims_checklist_section_parent_number",str)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();

                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();
                            simsobj.sims_checklist_section_name_ot = dr["sims_checklist_section_name_ot"].ToString();
                            simsobj.sims_checklist_section_desc = dr["sims_checklist_section_desc"].ToString();
                            simsobj.sims_response_code = dr["sims_response_code"].ToString();
                            simsobj.sims_checklist_section_parent_name = dr["sims_checklist_section_parent_name"].ToString();
                            simsobj.sims_checklist_section_response_type = dr["sims_checklist_section_response_type"].ToString();
                            simsobj.sims_checklist_section_status = dr["sims_checklist_section_status"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }
        
        
       
            [Route("getsearchlist")]
        public HttpResponseMessage getsearchlist(string str, string str1)
        {

            List<Sims789> lst = new List<Sims789>();
                if(str== "undefined" ||str == " ")
                {
                    str = null;
                }
                if (str1 == "undefined" || str1 == "")
                {
                    str1 = null;
                }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),
                           new SqlParameter("@SIMS_CHECKLIST_SECTION_NAME",str),
                           new SqlParameter("@sims_checklist_section_parent_number",str1)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims789 simsobj = new Sims789();

                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();
                             simsobj.sims_checklist_section_name_ot = dr["sims_checklist_section_name_ot"].ToString();
                            simsobj.sims_checklist_section_desc = dr["sims_checklist_section_desc"].ToString();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_parent_name = dr["sims_checklist_section_parent_name"].ToString();
                            simsobj.sims_checklist_section_response_type = dr["sims_checklist_section_response_type"].ToString();
                            simsobj.sims_response_code = dr["sims_response_code"].ToString();
                            simsobj.sims_checklist_section_status = dr["sims_checklist_section_status"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


            // get rseponse list
            [Route("get_responsebyList")]
            public HttpResponseMessage get_responsebyList()
            {

                List<Sims754> lst = new List<Sims754>();

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[SIMS].[SIMS_LIST_SECTION_PROC]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),

                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims754 simsobj = new Sims754();
                                simsobj.sims_response_code = dr["sims_response_code"].ToString();
                                simsobj.sims_response_title = dr["sims_response_title"].ToString();


                                lst.Add(simsobj);

                            }
                        }
                    }
                }
                catch (Exception x)
                {

                    //message.strMessage = x.Message;
                    //message.systemMessage = string.Empty;
                    //message.messageType = MessageType.Success;

                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }

    }
}