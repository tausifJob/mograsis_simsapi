﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;


namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/ListEvidences")]
    public class ChecklistEvidencesController : ApiController
    {

        [Route("get_sectionbyList")]
        public HttpResponseMessage get_sectionbyList()
        {

            List<Sims624> lst = new List<Sims624>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_parameter_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims624 simsobj = new Sims624();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("get_sectionbyAllList")]
        public HttpResponseMessage get_sectionbyAllList()
        {

            List<Sims624> lst = new List<Sims624>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_parameter_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims624 simsobj = new Sims624();
                            simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDListEvidences")]
        public HttpResponseMessage CUDListEvidences(List<Sims624> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims624 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_checklist_section_parameter_PROC]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_checklist_section_parameter_number", simsobj.sims_checklist_section_parameter_number),
                                new SqlParameter("@sims_checklist_section_parameter_name", simsobj.sims_checklist_section_parameter_name),
                                new SqlParameter("@sims_checklist_section_parameter_ot", simsobj.sims_checklist_section_parameter_ot),   
                                new SqlParameter("@sims_checklist_section_parent_parameter_order", simsobj.sims_checklist_section_parent_parameter_order),
                                new SqlParameter("@sims_checklist_section_number", simsobj.sims_checklist_section_parent_number),
                                new SqlParameter("@sims_checklist_section_status_parameter", simsobj.sims_checklist_section_status_parameter==true?"1":"0"), 
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("get_listEvidences")]
        public HttpResponseMessage get_listEvidences()
        {

            List<Sims624> lst = new List<Sims624>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_parameter_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims624 simsobj = new Sims624();

                            simsobj.sims_checklist_section_parameter_number = dr["sims_checklist_section_parameter_number"].ToString();
                            simsobj.sims_checklist_section_parameter_name = dr["sims_checklist_section_parameter_name"].ToString();
                            simsobj.sims_checklist_section_parent_parameter_order = dr["sims_checklist_section_parent_parameter_order"].ToString();
                            simsobj.sims_checklist_section_parameter_ot = dr["sims_checklist_section_parameter_ot"].ToString();
                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();

                            //simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();

                            simsobj.sims_checklist_section_status_parameter = dr["sims_checklist_section_status_parameter"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("getsearchlist")]
        public HttpResponseMessage getsearchlist(string str )
        {

            List<Sims624> lst = new List<Sims624>();
            if (str == "undefined" || str == "")
            {
                str = null;
            }
            //if (str1 == "undefined" || str1 == "")
            //{
            //    str1 = null;
            //}
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_checklist_section_parameter_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),
                           //new SqlParameter("@SIMS_CHECKLIST_SECTION_NAME",str),
                           new SqlParameter("@sims_checklist_section_number",str)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims624 simsobj = new Sims624();

                            simsobj.sims_checklist_section_parameter_number = dr["sims_checklist_section_parameter_number"].ToString();
                            simsobj.sims_checklist_section_parameter_name = dr["sims_checklist_section_parameter_name"].ToString();
                            simsobj.sims_checklist_section_parameter_ot = dr["sims_checklist_section_parameter_ot"].ToString();
                            simsobj.sims_checklist_section_parent_parameter_order = dr["sims_checklist_section_parent_parameter_order"].ToString();
                           //simsobj.sims_checklist_section_parent_number = dr["sims_checklist_section_parent_number"].ToString();
                            simsobj.sims_checklist_section_number = dr["sims_checklist_section_number"].ToString();
                            simsobj.sims_checklist_section_name = dr["sims_checklist_section_name"].ToString();
                            simsobj.sims_checklist_section_status_parameter = dr["sims_checklist_section_status_parameter"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


    }
}