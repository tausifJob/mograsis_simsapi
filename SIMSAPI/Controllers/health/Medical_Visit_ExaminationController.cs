﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.Medical_Visit_ExaminationController
{
    [RoutePrefix("api/common/Medical_Visit_Examination")]
    [BasicAuthentication]
    public class Medical_Visit_ExaminationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetSims_MedicalVisitExamination")]
        public HttpResponseMessage GetSims_MedicalVisitExamination()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudent_Medical_Visit(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudent_Medical_Visit"));

            List<Sims102> list = new List<Sims102>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_examination_proc]",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", 'S')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims102 simsobj = new Sims102();
                            simsobj.sims_examination_code = dr["sims_examination_code"].ToString();
                            simsobj.sims_examination_name = dr["sims_examination_name"].ToString();
                            simsobj.sims_examination_desc = dr["sims_examination_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalVisitExamination")]
        public HttpResponseMessage InsertSims_MedicalVisitExamination(List<Sims102> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims102 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_visit_examination_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_examination_code", simsobj.sims_examination_code),
                            new SqlParameter("@sims_examination_name", simsobj.sims_examination_name),
                            new SqlParameter("@sims_examination_desc", simsobj.sims_examination_desc),
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}