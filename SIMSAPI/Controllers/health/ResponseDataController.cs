﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Web;
using System.IO;


namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/ResponseData")]
    public class ResponseDataController : ApiController
    {
          
       [Route("get_ResponsGrid")]
        public HttpResponseMessage get_ResponsGrid()
        {

            List<respoObj> lst = new List<respoObj>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_response_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            respoObj simsobj = new respoObj();

                            simsobj.sims_response_code = dr["sims_response_code"].ToString();
                            simsobj.sims_response_title = dr["sims_response_title"].ToString();
                            simsobj.sims_response_desc = dr["sims_response_desc"].ToString();
                            simsobj.response_count = dr["response_count"].ToString();
                            simsobj.sims_response_title_ot = dr["sims_response_title_ot"].ToString();
                            simsobj.sims_response_desc_ot = dr["sims_response_desc_ot"].ToString();

                            simsobj.sims_response_status = dr["sims_response_status"].Equals("A") ? true : false;
                     
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("ResponsDetails")]
        public HttpResponseMessage ResponsDetails(string rating)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<respoObj> type_list = new List<respoObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_response_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'E'),
                       new SqlParameter ("@sims_response_code", rating)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                respoObj simsobj = new respoObj();
                                simsobj.sims_response_code = dr["sims_response_code"].ToString();
                                simsobj.sims_response_detail_code = dr["sims_response_detail_code"].ToString();

                                simsobj.sims_response_title = dr["sims_response_title"].ToString();
                                simsobj.sims_response_title_ot = dr["sims_response_title_ot"].ToString();

                                simsobj.sims_response_desc = dr["sims_response_desc"].ToString();
                                simsobj.sims_response_desc_ot = dr["sims_response_desc_ot"].ToString();

                                simsobj.sims_response_status = dr["sims_response_status"].Equals("A") ? true : false; ;

                                simsobj.sims_response_detail = dr["sims_response_detail"].ToString();
                                simsobj.sims_response_detail_ot = dr["sims_response_detail_ot"].ToString();
                                simsobj.sims_response_detail_status = dr["sims_response_detail_status"].Equals("A") ? true : false;

                                //simsobj.sims_response_desc_ot = dr["sims_response_desc_ot"].ToString();
                                
                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }
          
        [Route("getResponseGroupCode")]
        public HttpResponseMessage getResponseGroupCode()
        {
            List<respoObj> mod_list = new List<respoObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_response_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","G")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            respoObj simsobj = new respoObj();
                            simsobj.responsecode = dr["sims_response_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDResponseSave")]
        public HttpResponseMessage CUDResponseSave(List<respoObj> obj)
        {
            object o = null;
            bool result = true;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                foreach (respoObj data in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_response_proc]",
                             new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",data.opr),
                          new SqlParameter("@sims_response_code",data.sims_response_code),
                          new SqlParameter("@sims_response_title",data.sims_response_title),
                          new SqlParameter("@sims_response_title_ot",data.sims_response_title_ot),
                          new SqlParameter("@sims_response_desc",data.sims_response_desc),
                          new SqlParameter("@sims_response_desc_ot",data.sims_response_desc_ot),
                          new SqlParameter("@sims_response_status",data.sims_response_status == true? "A":"I"),
                          new SqlParameter("@sims_response_detail_code",data.sims_response_detail_code),
                          new SqlParameter("@sims_response_detail",data.sims_response_detail),
                          new SqlParameter("@sims_response_detail_ot",data.sims_response_detail_ot),
                          new SqlParameter("@sims_response_detail_status",data.sims_response_detail_status == true? "A":"I")
                        
                        
                        });
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                result = false;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, result);
        }

        [Route("CUD_SaveResponseDetails")]
        public HttpResponseMessage CUD_SaveResponseDetails(respoObj v)
        {
            bool result = false;
            string responsecode = "";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_response_proc]",
                    new List<SqlParameter>() {
                          new SqlParameter("@opr", "IM"),
                          new SqlParameter("@sims_response_title",v.sims_response_title),
                          new SqlParameter("@sims_response_title_ot",v.sims_response_title_ot),
                          new SqlParameter("@sims_response_desc",v.sims_response_desc),
                          new SqlParameter("@sims_response_desc_ot",v.sims_response_desc_ot),
                          new SqlParameter("@sims_response_status",v.sims_response_status == true? "A":"I"),
                        });
                    while (dr.Read())
                    {
                        responsecode = dr["responsecode"].ToString();
                    }
                    int r = dr.RecordsAffected;
                    dr.Close();
                    if (r > 0)
                    {

                        result = true;
                        foreach (response_Details vd in v.response_det)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_response_proc]",
                            new List<SqlParameter>() {
                                  new SqlParameter("@opr", "ID"),
                                  new SqlParameter("@sims_response_code",responsecode),
                                  new SqlParameter("@sims_response_detail_code",vd.sims_response_detail_code),
                                  new SqlParameter("@sims_response_detail",vd.sims_response_detail),
                                  new SqlParameter("@sims_response_detail_ot",vd.sims_response_detail_ot),
                                    new SqlParameter("@sims_response_detail_status",vd.sims_response_detail_status == true? "A":"I")
                                 
                                });
                            dr1.Close();
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
