﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.MedicalStudentSusceptabilityController
{
    [RoutePrefix("api/MedicalStudentSusceptibilityCSDController")]
    [BasicAuthentication]
    public class MedicalStudentSusceptibilityCSDController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private object sims_medical_visit_number;

        /*
        [Route("GetAllMedicalSusceptibilityCode")]
        public HttpResponseMessage GetAllMedicalSusceptibilityCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims099> list = new List<Sims099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_susceptibility_proc1]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "S"),
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims099 simsobj = new Sims099();
                            simsobj.sims_enrollment_no = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_susceptibility_code = dr["sims_susceptibility_code"].ToString();
                            simsobj.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                            simsobj.susceptibility = dr["sims_susceptibility_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSusceptibilityDesc")]
        public HttpResponseMessage getSusceptibilityDesc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims099> list = new List<Sims099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_susceptibility_proc1]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "K"),
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims099 simsobj = new Sims099();
                            simsobj.sims_susceptibility_code = dr["sims_susceptibility_code"].ToString();
                            simsobj.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        

        [Route("CUDMedicalStudentSusceptibility")]
        public HttpResponseMessage CUDMedicalStudentSusceptibility(List<Sims099> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalStudentSusceptibility(),PARAMETERS";
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims099 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_student_susceptibility_proc1]",
                            new List<SqlParameter>() 
                         { 

                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_enrollment_number", simsobj.sims_enrollment_number),
                               new SqlParameter("@sims_susceptibility_code", simsobj.sims_susceptibility_code),
                               new SqlParameter("@sims_susceptibility_desc", simsobj.sims_susceptibility_desc),
                               new SqlParameter("@scode_old",simsobj.scode_old),

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        */

        #region API For Medical Student Susceptibility...

        [Route("GetAllMedicalSusceptibilityCode")]
        public HttpResponseMessage GetAllMedicalSusceptibilityCode(string cur_name, string academic_year, string grd_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims099> list = new List<Sims099>();
            List<Sims099> list1 = new List<Sims099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_susceptibility_proc1]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                         });

                    if (dr.HasRows)
                    {
                        string eno = null;
                        while (dr.Read())
                        {
                            Sims099 simsobj = new Sims099();
                            simsobj.sims_student_name = dr["sims_student_name"].ToString();
                            simsobj.susceptibility = dr["sims_susceptibility_desc"].ToString();
                            simsobj.GradeSection_Name = dr["GradeSection_Name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_enrollment_no = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_isexpanded = "None";
                            simsobj.sims_icon = "fa fa-plus-circle";
                            Sims099 simsobj1 = new Sims099();
                            simsobj1.sims_susceptibility_code = dr["sims_susceptibility_code"].ToString();
                            simsobj1.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                            eno = dr["sims_student_enroll_number"].ToString();
                            var v = from p in list where p.sims_enrollment_no == eno select p;
                            if (v.Count() > 0)
                            {
                                if (v.ElementAt(0).sims_susceptibility_desc_group == null)
                                {
                                    v.ElementAt(0).sims_susceptibility_desc_group = new List<Sims099>();
                                }
                                v.ElementAt(0).sims_susceptibility_desc_group.Add(simsobj1);
                            }
                            else
                            {
                                simsobj.sims_susceptibility_desc_group = new List<Sims099>();
                                simsobj.sims_susceptibility_desc_group.Add(simsobj1);
                                list.Add(simsobj);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSusceptibilityDesc")]
        public HttpResponseMessage getSusceptibilityDesc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims099> list = new List<Sims099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_susceptibility_proc1]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "K"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims099 simsobj = new Sims099();
                            simsobj.sims_susceptibility_code = dr["sims_susceptibility_code"].ToString();
                            simsobj.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CUDMedicalStudentSusceptibility")]
        public HttpResponseMessage CUDMedicalStudentSusceptibility(List<Sims099> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalStudentSusceptibility(),PARAMETERS";
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims099 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_student_susceptibility_proc1]",
                            new List<SqlParameter>() 
                         { 

                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_enrollment_number", simsobj.sims_enrollment_no),
                               new SqlParameter("@sims_susceptibility_code", simsobj.sims_susceptibility_code),
                               new SqlParameter("@sims_susceptibility_desc", simsobj.sims_susceptibility_desc),
                               new SqlParameter("@scode_old",simsobj.scode_old),

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region API For Medical Visit...

        [Route("getAutoGenerateMedicalvisitNo")]
        public HttpResponseMessage getAutoGenerateMedicalvisitNo()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            string mod_list = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mod_list = dr["comn_number_sequence"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAllMedicalMedicineCode")]
        public HttpResponseMessage getAllMedicalMedicineCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("getMedicalVisit")]
        public HttpResponseMessage getMedicalVisit()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "S"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                            simsobj.medical_visit_date = db.UIDDMMYYYYformat(dr["sims_medical_visit_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_employee_number = dr["sims_employee_number"].ToString();
                            simsobj.medical_attended_by = dr["sims_attended_by"].ToString();
                            simsobj.medical_complaint_desc = dr["sims_complaint_desc"].ToString();
                            simsobj.medical_attendant_observation = dr["sims_attendant_observation"].ToString();
                            simsobj.medical_health_education = dr["sims_health_education"].ToString();
                            simsobj.medical_followup_comment = dr["sims_followup_comment"].ToString();
                            simsobj.sims_user_code_created_by = dr["sims_user_code_created_by"].ToString();
                            simsobj.sims_Emp_name = dr["EmpName"].ToString();
                            simsobj.sims_student_name = dr["StudName"].ToString();
                            simsobj.sims_CreatedBy_name = dr["CreatedByName"].ToString();
                            simsobj.sims_AttendedBy_name = dr["AttByName"].ToString();
                            //simsobj.sims_serial_number = int.Parse(dr["sims_serial_number"].ToString());
                            //simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            //simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            //simsobj.sims_medicine_dosage = dr["sims_medicine_dosage"].ToString();
                            //simsobj.sims_medicine_duration = dr["sims_medicine_duration"].ToString();
                            simsobj.sims_status = dr["sims_status"].Equals("A") ? true : false;
                            simsobj.sims_drug_allergy = dr["sims_drug_allergy"].ToString();
                            simsobj.sims_blood_pressure = dr["sims_blood_pressure"].ToString();
                            simsobj.sims_temperature = dr["sims_temperature"].ToString();
                            simsobj.sims_pulse_rate = dr["sims_pulse_rate"].ToString();
                            simsobj.sims_respiratory_rate = dr["sims_respiratory_rate"].ToString();
                            simsobj.sims_medical_examination_name = dr["sims_medical_examination_name"].ToString();
                            simsobj.sims_spo2 = dr["sims_spo2"].ToString();
                            simsobj.sims_medical_visit_nursing_treatment = dr["sims_medical_visit_nursing_treatment"].ToString();

                            simsobj.sims_medical_visit_InTime = dr["sims_medical_visit_InTime"].ToString();
                            simsobj.sims_medical_visit_OutTime = dr["sims_medical_visit_OutTime"].ToString();
                            simsobj.sims_medical_stud_status = dr["sims_medical_stud_status"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_medical_stud_status_value"].ToString();

                            simsobj.sims_medical_primary_action = dr["sims_medical_primary_action"].ToString();
                            simsobj.sims_medical_action_date = db.UIDDMMYYYYformat(dr["sims_medical_action_date"].ToString());
                            simsobj.sims_medical_action_time = dr["sims_medical_action_time"].ToString();
                            simsobj.sims_medical_secondary_action = dr["sims_medical_secondary_action"].ToString();
                            simsobj.sims_medical_health_center = dr["sims_medical_health_center"].ToString();
                            simsobj.sims_medical_action_home = dr["sims_medical_action_home"].ToString();
                            simsobj.sims_medical_action_comment = dr["sims_medical_action_comment"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CUDMedicalVisit")]
        public HttpResponseMessage CUDMedicalVisit(List<Sims101> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalStudentSusceptibility(),PARAMETERS";
            bool insert = false;
            //if (simsobj.sims_enrollno)
            //if (simsobj.sims_empno)
            string st = string.Empty;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims101 simsobj in data)
                    {
                        // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_medical_visit_number", simsobj.sims_medical_visit_number),
                             new SqlParameter("@sims_medical_visit_date", db.DBYYYYMMDDformat(simsobj.medical_visit_date)),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_employee_number", simsobj.sims_employee_number),
                             new SqlParameter("@sims_attended_by", simsobj.medical_attended_by),
                             new SqlParameter("@sims_complaint_desc", simsobj.medical_complaint_desc),
                             new SqlParameter("@sims_attendant_observation", simsobj.medical_attendant_observation),
                             new SqlParameter("@sims_health_education", simsobj.medical_health_education),
                             new SqlParameter("@sims_followup_comment", simsobj.medical_followup_comment),
                             new SqlParameter("@sims_user_code_created_by", simsobj.sims_user_code_created_by),
                             new SqlParameter("@sims_appl_code", "Sim101"),
                             new SqlParameter("@comn_sequence_code", "Mvn"),
                             new SqlParameter("@sims_medicine_code", simsobj.sims_medicine_code),
                             new SqlParameter("@sims_medicine_dosage", simsobj.sims_medicine_dosage),
                             new SqlParameter("@sims_medicine_duration", simsobj.sims_medicine_duration),
                             new SqlParameter("@sims_status",simsobj.sims_status==true?"A":"I"),
                             new SqlParameter("@sims_drug_allergy", simsobj.sims_drug_allergy),
                             new SqlParameter("@sims_blood_pressure", simsobj.sims_blood_pressure),
                             new SqlParameter("@sims_temperature", simsobj.sims_temperature),
                             new SqlParameter("@sims_pulse_rate", simsobj.sims_pulse_rate),
                             new SqlParameter("@sims_respiratory_rate", simsobj.sims_respiratory_rate),
                             new SqlParameter("@sims_medical_examination_name", simsobj.sims_medical_examination_name),
                             new SqlParameter("@sims_spo2", simsobj.sims_spo2),
                              new SqlParameter("@sims_medical_visit_nursing_treatment", simsobj.sims_medical_visit_nursing_treatment),
                              new SqlParameter("@sims_medical_visit_InTime", simsobj.sims_medical_visit_InTime),
                             new SqlParameter("@sims_medical_visit_OutTime", simsobj.sims_medical_visit_OutTime),
                             new SqlParameter("@sims_medical_stud_status", simsobj.sims_medical_stud_status),
                         
                             new SqlParameter("@primary_value", simsobj.primary_value),
                             new SqlParameter("@sims_medical_action_date", db.DBYYYYMMDDformat(simsobj.sims_medical_action_date)),
                             new SqlParameter("@sims_medical_Action_Time", simsobj.sims_medical_Action_Time),
                             new SqlParameter("@sims_action_health_center", simsobj.sims_action_health_center),
                             new SqlParameter("@sims_home", simsobj.sims_home),
                             new SqlParameter("@secondary_value", simsobj.secondary_value),
                             new SqlParameter("@sims_medical_action_comment", simsobj.sims_medical_action_comment),
                                                         
                         });

                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    msg.strMessage = st;
                        //}

                        //if (dr.RecordsAffected > 0)
                        //{
                        //    inserted = true;
                        //}
                        //dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);

                }

            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, insert);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getGenderDOB")]
        public HttpResponseMessage getGenderDOB(string S_number, string E_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", "J"),
                             new SqlParameter("@sims_enroll_number", S_number),
                             new SqlParameter("@sims_employee_number", E_number),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            if (S_number != "null")
                            {
                                simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                                simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                simsobj.name_in_english = dr["name_in_english"].ToString();
                                simsobj.sub_class = dr["sub_class"].ToString();
                            }

                            else if (E_number != "null")
                            {
                                simsobj.emp_name = dr["emp_name"].ToString();
                                simsobj.em_sex = dr["em_sex"].ToString();
                                simsobj.em_img = dr["em_img"].ToString();
                                simsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            }

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getStudEmpData")]
        public HttpResponseMessage getStudEmpData(string S_number, string E_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>()
                        {
                              new SqlParameter("@opr", "K"),
                              new SqlParameter("@sims_enroll_number", S_number),
                              new SqlParameter("@sims_employee_number", E_number),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            if (S_number != "null")
                            {
                                simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                                simsobj.sims_medical_visit_date = db.UIDDMMYYYYformat(dr["sims_medical_visit_date"].ToString());
                                simsobj.sims_complaint_desc = dr["sims_complaint_desc"].ToString();
                                simsobj.sims_attendant_observation = dr["sims_attendant_observation"].ToString();
                                simsobj.sims_followup_comment = dr["sims_followup_comment"].ToString();
                                simsobj.sims_blood_pressure = dr["sims_blood_pressure"].ToString();
                                simsobj.sims_temperature = dr["sims_temperature"].ToString();
                                simsobj.sims_pulse_rate = dr["sims_pulse_rate"].ToString();
                                simsobj.sims_respiratory_rate = dr["sims_respiratory_rate"].ToString();
                                simsobj.sims_spo2 = dr["sims_spo2"].ToString();
                            }

                            else if (E_number != "null")
                            {
                                simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                                simsobj.sims_medical_visit_date = db.UIDDMMYYYYformat(dr["sims_medical_visit_date"].ToString());
                                simsobj.sims_complaint_desc = dr["sims_complaint_desc"].ToString();
                                simsobj.sims_attendant_observation = dr["sims_attendant_observation"].ToString();
                                simsobj.sims_followup_comment = dr["sims_followup_comment"].ToString();
                                simsobj.sims_blood_pressure = dr["sims_blood_pressure"].ToString();
                                simsobj.sims_temperature = dr["sims_temperature"].ToString();
                                simsobj.sims_pulse_rate = dr["sims_pulse_rate"].ToString();
                                simsobj.sims_respiratory_rate = dr["sims_respiratory_rate"].ToString();
                                simsobj.sims_spo2 = dr["sims_spo2"].ToString();
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDSaveMedicalVisit")]
        public HttpResponseMessage CUDSaveMedicalVisit(Sims101 v)
        {
            bool result = false;
            string sims_medical_visit_number = "";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                    new List<SqlParameter>() {
                          new SqlParameter("@opr", "IM"),
                          new SqlParameter("@sims_medical_visit_number", v.sims_medical_visit_number),
                             new SqlParameter("@sims_medical_visit_date", db.DBYYYYMMDDformat(v.medical_visit_date)),
                             new SqlParameter("@sims_enroll_number", v.sims_enroll_number),
                             new SqlParameter("@sims_employee_number", v.sims_employee_number),
                             new SqlParameter("@sims_attended_by", v.medical_attended_by),
                             new SqlParameter("@sims_complaint_desc", v.medical_complaint_desc),
                             new SqlParameter("@sims_attendant_observation", v.medical_attendant_observation),
                             new SqlParameter("@sims_health_education", v.medical_health_education),
                             new SqlParameter("@sims_followup_comment", v.medical_followup_comment),
                             new SqlParameter("@sims_user_code_created_by", v.sims_user_code_created_by),
                             new SqlParameter("@sims_appl_code", "Sim101"),
                             new SqlParameter("@comn_sequence_code", "Mvn"),
                             //new SqlParameter("@sims_medicine_code", v.sims_medicine_code),
                             //new SqlParameter("@sims_medicine_dosage", v.sims_medicine_dosage),
                             //new SqlParameter("@sims_medicine_duration", v.sims_medicine_duration),
                             new SqlParameter("@sims_status",v.sims_status==true?"A":"I"),
                             new SqlParameter("@sims_drug_allergy", v.sims_drug_allergy),
                             new SqlParameter("@sims_blood_pressure", v.sims_blood_pressure),
                             new SqlParameter("@sims_temperature", v.sims_temperature),
                             new SqlParameter("@sims_pulse_rate", v.sims_pulse_rate),
                             new SqlParameter("@sims_respiratory_rate", v.sims_respiratory_rate),
                             new SqlParameter("@sims_medical_examination_name", v.sims_medical_examination_name),
                             new SqlParameter("@sims_spo2", v.sims_spo2),
                             new SqlParameter("@sims_medical_visit_nursing_treatment", v.sims_medical_visit_nursing_treatment),

                             new SqlParameter("@sims_medical_visit_InTime", v.sims_medical_visit_InTime),
                             new SqlParameter("@sims_medical_visit_OutTime", v.sims_medical_visit_OutTime),
                             new SqlParameter("@sims_medical_stud_status", v.sims_medical_stud_status),

                             new SqlParameter("@sims_medical_primary_action", v.sims_medical_primary_action),
                             new SqlParameter("@sims_medical_action_date", db.DBYYYYMMDDformat(v.sims_medical_action_date)),
                             new SqlParameter("@sims_medical_action_time", v.sims_medical_action_time),
                             new SqlParameter("@sims_medical_secondary_action", v.sims_medical_secondary_action),
                             new SqlParameter("@sims_medical_health_center", v.sims_medical_health_center),
                             new SqlParameter("@sims_medical_action_home", v.sims_medical_action_home),
                             new SqlParameter("@sims_medical_action_comment", v.sims_medical_action_comment),
                        });
                    //while (dr.Read())
                    //{
                    //    sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                    //}
                    int r = dr.RecordsAffected;
                    dr.Close();
                    if (r > 0)
                    {
                        db.Open();
                        result = true;
                        foreach (medicin_Details vd in v.med_det)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                            new List<SqlParameter>() {
                                  new SqlParameter("@opr", "ID"),
                                  new SqlParameter("@sims_medical_visit_number",vd.sims_medical_visit_number),
                                  new SqlParameter("@sims_medicine_code",vd.sims_medicine_code),
                                  new SqlParameter("@sims_medicine_duration",vd.sims_medicine_duration),
                                   new SqlParameter("@sims_medicine_dosage",vd.sims_medicine_dosage),
                                  
                                });
                            dr1.Close();
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("medicinDetails")]
        public HttpResponseMessage medicinDetails(string vino)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<Sims101> type_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'E'),
                       new SqlParameter ("@sims_medical_visit_number", vino)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims101 simsobj = new Sims101();
                                simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                                simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();

                                simsobj.sims_medicine_duration = dr["sims_medicine_duration"].ToString();
                                simsobj.sims_medicine_dosage = dr["sims_medicine_dosage"].ToString();
                                simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();

                                //simsobj.sims_response_desc_ot = dr["sims_response_desc_ot"].ToString();

                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }
        #endregion

        #region API For Medical StudentTerm Detail...

        [Route("getTerm")]
        public HttpResponseMessage getTerm(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_term_details_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "J"),
                              new SqlParameter("@sims_cur_code", cur_code)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDMedicalTermDetails")]
        public HttpResponseMessage CUDMedicalTermDetails(List<Sims101> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims101 simsobj in data)
                    {


                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_student_term_details_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_term_code", simsobj.sims_term_code),
                             new SqlParameter("@sims_enrollment_number", simsobj.sims_enrollment_number),
                             new SqlParameter("@sims_health_card_number", simsobj.sims_health_card_number),
                             new SqlParameter("@sims_health_restriction_status", simsobj.sims_health_restriction_status==true?"T":"F"),
                             new SqlParameter("@sims_health_restriction_desc", simsobj.sims_health_restriction_desc),
                             new SqlParameter("@sims_medication_status", simsobj.sims_medication_status==true?"T":"F"),
                             new SqlParameter("@sims_medication_desc", simsobj.sims_medication_desc),
                             new SqlParameter("@sims_height", simsobj.sims_height),
                             new SqlParameter("@sims_wieght", simsobj.sims_wieght),
                             new SqlParameter("@sims_teeth", simsobj.sims_teeth),
                             new SqlParameter("@sims_health_bmi", simsobj.sims_health_bmi),
                             new SqlParameter("@sims_health_other_remark", simsobj.sims_health_other_remark),

                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAllTermDetails")]
        public HttpResponseMessage getAllTermDetails(string cur, string a_year, string grades, string section, string term)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            int i = 0;

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_student_term_details_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", cur),
                              new SqlParameter("@sims_academic_year", a_year),
                              new SqlParameter("@sims_sims_grades", grades),
                              new SqlParameter("@sims_sims_section",section),
                              new SqlParameter("@sims_term_code",term),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            i = i + 1;
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("T") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("T") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.sims_health_other_remark = dr["sims_health_other_remark"].ToString();
                            simsobj.datastatus = i.ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        #endregion

        #region API For Medical Checkup Schedule...

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<sim179> mod_list = new List<sim179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "L"),
                             new SqlParameter("@sims_cur_code", cur_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sim179 simssobj = new sim179();
                            simssobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simssobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simssobj.sims_academic_year_start_date = db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                            simssobj.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("gettermdate")]
        public HttpResponseMessage gettermdate(string ac_year, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_academic_year", ac_year),
                             new SqlParameter("@sims_cur_code", cur_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_term_start_date = db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                            simssobj.sims_term_end_date = db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                            simssobj.sims_term_date_en = dr["sims_term_date_en"].ToString();
                            simssobj.sims_term_code = dr["sims_term_code"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getdatedetails")]
        public HttpResponseMessage getdatedetails(string ac_year, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "E"),
                             new SqlParameter("@sims_academic_year", ac_year),
                             new SqlParameter("@sims_cur_code", cur_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_date_desc = dr["sims_appl_form_field_value1"].ToString();
                            simssobj.sims_date_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getgardedetails")]
        public HttpResponseMessage getgardedetails(string ac_year, string cur_code, string date_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@sims_academic_year", ac_year),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_medical_checkup_schedule_status", date_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simssobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getsectiondetails")]
        public HttpResponseMessage getsectiondetails(string ac_year, string cur_code, string date_code, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),
                             new SqlParameter("@sims_academic_year", ac_year),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_medical_checkup_schedule_status", date_code),
                             new SqlParameter("@sims_grade_code", grade_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_section_code = dr["sims_section_code"].ToString();
                            simssobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getdatedet")]
        public HttpResponseMessage getdatedet(string ac_year, string cur_code, string date_code, string grade_code, string section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "H"),
                             new SqlParameter("@sims_academic_year", ac_year),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_medical_checkup_schedule_status", date_code),
                             new SqlParameter("@sims_grade_code", grade_code),
                             new SqlParameter("@sims_section_code", section_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_medical_checkup_schedule_srl_no = dr["sims_medical_checkup_schedule_srl_no"].ToString();
                            simssobj.sims_medical_checkup_schedule_date = dr["sims_medical_checkup_schedule_date"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getAllStudentDetails")]
        public HttpResponseMessage getAllStudentDetails(string cur, string a_year, string grades, string section, string term)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "getSusceptibilityDesc"));

            int i = 0;

            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", cur),
                              new SqlParameter("@sims_academic_year", a_year),
                              new SqlParameter("@sims_grade_code", grades),
                              new SqlParameter("@sims_section_code",section),
                              new SqlParameter("@sims_term_code",term),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simsobj = new SimCMF();
                            i = i + 1;
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].Equals("T") ? true : false;
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].Equals("T") ? true : false;
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.sims_health_other_remark = dr["sims_health_other_remark"].ToString();
                            simsobj.datastatus = i.ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDMedicalAllDetails")]
        public HttpResponseMessage CUDMedicalAllDetails(List<SimCMF> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SimCMF simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_checkup_schedule_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@medical_checkup_schedule", simsobj.sims_medical_checkup_schedule_srl_no),
                             new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                             new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                             new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_term_code", simsobj.sims_term_code),
                             //new SqlParameter("@sims_medical_checkup_date", simsobj.sims_medical_checkup_date),
                             new SqlParameter("@sims_health_card_number", simsobj.sims_health_card_number),
                             new SqlParameter("@sims_health_restriction_status", simsobj.sims_health_restriction_status==true?"T":"F"),
                             new SqlParameter("@sims_health_restriction_desc", simsobj.sims_health_restriction_desc),
                             new SqlParameter("@sims_medication_status", simsobj.sims_medication_status==true?"T":"F"),
                             new SqlParameter("@sims_medication_desc", simsobj.sims_medication_desc),
                             new SqlParameter("@sims_height", simsobj.sims_height),
                             new SqlParameter("@sims_wieght", simsobj.sims_wieght),
                             new SqlParameter("@sims_teeth", simsobj.sims_teeth),
                             new SqlParameter("@sims_health_bmi", simsobj.sims_health_bmi),
                             new SqlParameter("@sims_health_other_remark", simsobj.sims_health_other_remark),
                        
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDcheckupdetails")]
        public HttpResponseMessage CUDcheckupdetails(List<SimCMF> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalStudentSusceptibility(),PARAMETERS";
            bool inserted = false;
            //if (simsobj.sims_enrollno)
            //if (simsobj.sims_empno)
            string st = string.Empty;
            string date = string.Empty;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SimCMF simsobj in data)
                    {
                        if (simsobj.opr == "C")
                        {
                            date = db.DBYYYYMMDDformat(simsobj.sims_jan_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_feb_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_mar_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_apr_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_may_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_jun_date) + "," +
                                   db.DBYYYYMMDDformat(simsobj.sims_jly_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_aug_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_sep_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_oct_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_nov_date) + "," + db.DBYYYYMMDDformat(simsobj.sims_dec_date) + ",";
                        }

                        else if (simsobj.opr == "I")
                        {
                            date = db.DBYYYYMMDDformat(simsobj.sims_year_date);
                        }

                        else if (simsobj.opr == "A")
                        {
                            date = db.DBYYYYMMDDformat(simsobj.sims_special_date);
                        }
                        else if (simsobj.opr == "Q")
                        {
                            date = db.DBYYYYMMDDformat(simsobj.sims_special_date);
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_checkup_schedule_proc]",
                            new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                             new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                             new SqlParameter("@sims_schedule_date", date),
                         });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        #endregion

        [Route("GetStudentRound")]
        public HttpResponseMessage GetStudentRound()
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "V"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetPrimaryAction")]
        public HttpResponseMessage GetPrimaryAction()
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "W"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.primary_parameter = dr["primary_parameter"].ToString();
                            simsobj.primary_value = dr["primary_value"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetSecondaryAction")]
        public HttpResponseMessage GetSecondaryAction()
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_visit_csd_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "O"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.secondary_parameter = dr["secondary_parameter"].ToString();
                            simsobj.secondary_value = dr["secondary_value"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        //public string sims_medica_action_date { get; set; }

       // public string sims_medical_action_date { get; set; }
    }
}