﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.Medical_ImmunizationController
{
    [RoutePrefix("api/common/Medical_Immunization")]
    [BasicAuthentication]
    public class Medical_ImmunizationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllMedicalImmunizationAgeGroupCode")]
        public HttpResponseMessage GetAllMedicalImmunizationAgeGroupCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_age_group_desc = dr["sims_immunization_age_group_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSims_MedicalImmunization")]
        public HttpResponseMessage GetSims_MedicalImmunization()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Sims094> list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_code = dr["sims_immunization_code"].ToString();
                            simsobj.sims_immunization_age_group_desc = dr["sims_immunization_age_group_desc"].ToString();
                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_immunization_dosage1 = dr["sims_immunization_dosage1"].ToString();
                            simsobj.sims_immunization_dosage2 = dr["sims_immunization_dosage2"].ToString();
                            simsobj.sims_immunization_dosage3 = dr["sims_immunization_dosage3"].ToString();
                            simsobj.sims_immunization_dosage4 = dr["sims_immunization_dosage4"].ToString();
                            simsobj.sims_immunization_dosage5 = dr["sims_immunization_dosage5"].ToString();
                            if (dr["sims_immunization_status"].ToString().Equals("A"))
                            {
                                simsobj.sims_immunization_status = true;
                            }
                            else
                            {
                                simsobj.sims_immunization_status = false;
                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalImmunization")]
        public HttpResponseMessage InsertSims_MedicalImmunization(Sims094 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_immunization_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_immunization_code", simsobj.sims_immunization_code),
                            new SqlParameter("@sims_immunization_age_group_code", simsobj.sims_immunization_age_group_code),
                            new SqlParameter("@sims_immunization_desc", simsobj.sims_immunization_desc),
                            new SqlParameter("@sims_immunization_dosage1", simsobj.sims_immunization_dosage1),
                            new SqlParameter("@sims_immunization_dosage2", simsobj.sims_immunization_dosage2),
                            new SqlParameter("@sims_immunization_dosage3", simsobj.sims_immunization_dosage3),
                            new SqlParameter("@sims_immunization_dosage4", simsobj.sims_immunization_dosage4),
                            new SqlParameter("@sims_immunization_dosage5", simsobj.sims_immunization_dosage5),
                            new SqlParameter("@sims_immunization_status", simsobj.sims_immunization_status ? "A" : "I")
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Delete_Sims_MedicalExamination")]
        public HttpResponseMessage Delete_InsertSims_MedicalImmunization(List<Sims094> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Delete_InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "Delete_InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                foreach (Sims094 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_immunization_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_immunization_code", simsobj.sims_immunization_code),
                            new SqlParameter("@sims_immunization_age_group_name", simsobj.sims_immunization_age_group_name),
                            new SqlParameter("@sims_immunization_desc", simsobj.sims_immunization_desc),
                            new SqlParameter("@sims_immunization_dosage1", simsobj.sims_immunization_dosage1),
                            new SqlParameter("@sims_immunization_dosage2", simsobj.sims_immunization_dosage2),
                            new SqlParameter("@sims_immunization_dosage3", simsobj.sims_immunization_dosage3),
                            new SqlParameter("@sims_immunization_dosage4", simsobj.sims_immunization_dosage4),
                            new SqlParameter("@sims_immunization_dosage5", simsobj.sims_immunization_dosage5),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}