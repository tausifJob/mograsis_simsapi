﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.MedicalParametersController
{
    [RoutePrefix("api/MedicalParametersController")]
    [BasicAuthentication]
    public class MedicalParametersController : ApiController
    {
        public object Log { get; private set; }

        [Route("getMedicalParameters")]
        public HttpResponseMessage getMedicalVisit()
        {

            List<Clar101> mod_list = new List<Clar101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "S"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Clar101 simsobj = new Clar101();
                            simsobj.sims_health_parameter_sr_no = dr["sims_health_parameter_sr_no"].ToString();
                            simsobj.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                            simsobj.sims_health_parameter_name_en = dr["sims_health_parameter_name_en"].ToString();
                            simsobj.sims_health_parameter_name_ot = dr["sims_health_parameter_name_ot"].ToString();
                            simsobj.sims_health_parameter_min_value = dr["sims_health_parameter_min_value"].ToString();
                            simsobj.sims_health_parameter_max_value = dr["sims_health_parameter_max_value"].ToString();
                            simsobj.sims_health_parameter_default_value = dr["sims_health_parameter_default_value"].ToString();
                            simsobj.sims_health_parameter_unit = dr["sims_health_parameter_unit"].ToString();
                            simsobj.sims_health_parameter_display_order = dr["sims_health_parameter_display_order"].ToString();
                             
                            simsobj.sims_health_parameter_image = dr["sims_health_parameter_image"].ToString();
                            simsobj.sims_health_parameter_status = dr["sims_health_parameter_status"].ToString();
                            simsobj.sims_health_parameter_created_by = dr["sims_health_parameter_created_by"].ToString();
                            simsobj.sims_health_parameter_create_datetime = db.UIDDMMYYYYformat(dr["sims_health_parameter_create_datetime"].ToString());

                            if (dr["sims_health_parameter_isNumeric"].ToString().Equals("Y"))
                            {
                                simsobj.sims_health_parameter_isNumeric = true;
                            }
                            else
                            {
                                simsobj.sims_health_parameter_isNumeric = false;
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
               
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAutoGenerateMedicalparameterNo")]
        public HttpResponseMessage getAutoGenerateMedicalparameterNo()
        {
             
            string mod_list = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mod_list = dr["comn_number_sequence"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                 
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDMedicalParameter")]
        public HttpResponseMessage CUDMedicalVisit(List<Clar101> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalStudentSusceptibility(),PARAMETERS";
            bool insert = false;
            //if (simsobj.sims_enrollno)
            //if (simsobj.sims_empno)
            string st = string.Empty;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Clar101 simsobj in data)
                    {
                        // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_health_parameter_clara]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_health_parameter_code", simsobj.sims_health_parameter_code),
                             new SqlParameter("@sims_health_parameter_check_code", simsobj.sims_health_parameter_check_code),
                             
                             new SqlParameter("@sims_health_parameter_name_en", simsobj.sims_health_parameter_name_en),
                             new SqlParameter("@sims_health_parameter_min_value", simsobj.sims_health_parameter_min_value),
                             new SqlParameter("@sims_health_parameter_max_value", simsobj.sims_health_parameter_max_value),
                             new SqlParameter("@sims_health_parameter_default_value", simsobj.sims_health_parameter_default_value),
                             new SqlParameter("@sims_health_parameter_unit", simsobj.sims_health_parameter_unit),
                             new SqlParameter("@sims_health_parameter_display_order", simsobj.sims_health_parameter_display_order),
                               new SqlParameter("@sims_health_parameter_isNumeric",simsobj.sims_health_parameter_isNumeric==true?"Y":"N"),

                             new SqlParameter("@sims_health_parameter_check_name_en", simsobj.sims_health_parameter_check_name_en),
                              
                             new SqlParameter("@sims_health_parameter_check_min_value", simsobj.sims_health_parameter_check_min_value),
                             new SqlParameter("@sims_health_parameter_check_max_value", simsobj.sims_health_parameter_check_max_value),
                             new SqlParameter("@sims_health_parameter_check_comment", simsobj.sims_health_parameter_check_comment),
                              new SqlParameter("@user", simsobj.user),
                         });

                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                         
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);

                }

            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, insert);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDSaveMedicalParameter")]
        public HttpResponseMessage CUDSaveMedicalVisit(Clar101 v)
        {
            bool result = false;
            //string sims_health_parameter_code = "";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                    new List<SqlParameter>() {
                          new SqlParameter("@opr", "IM"),
                          new SqlParameter("@sims_health_parameter_code", v.sims_health_parameter_code),
                             
                             new SqlParameter("@sims_health_parameter_name_en", v.sims_health_parameter_name_en),
                             new SqlParameter("@sims_health_parameter_min_value", v.sims_health_parameter_min_value),
                             new SqlParameter("@sims_health_parameter_max_value", v.sims_health_parameter_max_value),
                             new SqlParameter("@sims_health_parameter_default_value", v.sims_health_parameter_default_value),
                             new SqlParameter("@sims_health_parameter_unit", v.sims_health_parameter_unit),
                             new SqlParameter("@sims_health_parameter_display_order", v.sims_health_parameter_display_order),
                             new SqlParameter("@sims_health_parameter_isNumeric", v.sims_health_parameter_isNumeric ? "Y" : "N"),
                             new SqlParameter("@user", v.user),
                            
                            
                        });
                    //while (dr.Read())
                    //{
                    //    sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                    //}
                    int r = dr.RecordsAffected;
                    dr.Close();
                    if (r > 0)
                    {
                        db.Open();
                        result = true;
                        foreach (parameter_Details vd in v.med_det)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                            new List<SqlParameter>() {
                                  new SqlParameter("@opr", "ID"),
                                  new SqlParameter("@sims_health_parameter_code",vd.sims_health_parameter_code),
                                  new SqlParameter("@sims_health_parameter_check_name_en",vd.sims_health_parameter_check_name_en),
                                  new SqlParameter("@sims_health_parameter_check_min_value",vd.sims_health_parameter_check_min_value),
                                   new SqlParameter("@sims_health_parameter_check_max_value",vd.sims_health_parameter_check_max_value),
                                   new SqlParameter("@sims_health_parameter_check_comment",vd.sims_health_parameter_check_comment),
                                        new SqlParameter("@user", vd.user),

                                });
                            dr1.Close();
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("parameterDetails")]
        public HttpResponseMessage medicinDetails(string vino)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<parameter_Details> type_list = new List<parameter_Details>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_health_parameter_clara]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'E'),
                       new SqlParameter ("@sims_health_parameter_code", vino)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                parameter_Details simsobj = new parameter_Details();
                                simsobj.sims_health_parameter_code = dr["sims_health_parameter_code"].ToString();
                                simsobj.sims_health_parameter_check_code = dr["sims_health_parameter_check_code"].ToString();

                                simsobj.sims_health_parameter_check_name_en = dr["sims_health_parameter_check_name_en"].ToString();
                                simsobj.sims_health_parameter_check_name_ot = dr["sims_health_parameter_check_name_ot"].ToString();
                                simsobj.sims_health_parameter_check_min_value = dr["sims_health_parameter_check_min_value"].ToString();

                                simsobj.sims_health_parameter_check_max_value = dr["sims_health_parameter_check_max_value"].ToString();
                                simsobj.sims_health_parameter_check_comment = dr["sims_health_parameter_check_comment"].ToString();
                                simsobj.sims_health_parameter_check_status = dr["sims_health_parameter_check_status"].ToString();

                                simsobj.sims_health_parameter_check_created_by = dr["sims_health_parameter_check_created_by"].ToString();
                                simsobj.sims_health_parameter_check_created_datetime = dr["sims_health_parameter_check_created_datetime"].ToString();

                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

    }
}