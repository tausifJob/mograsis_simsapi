﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.MedicalMedicineController
{
    [RoutePrefix("api/common/MedicalMedicine")]
    [BasicAuthentication]
    public class MedicalMedicineController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetMedicineType")]
        public HttpResponseMessage GetMedicineType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_medicine",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "A"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();
                            simsobj.sims_medicine_typename = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_medicine_type = dr["sims_appl_parameter"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetMedicineAlert")]
        public HttpResponseMessage GetMedicineAlert()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineAlert(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineAlert"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_medicine",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "B"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();
                            simsobj.sims_medicine_expirey_alertname = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_medicine_expirey_alert = dr["sims_appl_parameter"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSims_MedicalMedicine")]
        public HttpResponseMessage GetSims_MedicalMedicine()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_MedicalMedicine(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSims_MedicalMedicine"));

            List<Sims097> list = new List<Sims097>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_medicine",
                        new List<SqlParameter>() 
                        { 
                             new SqlParameter("@opr", "S"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims097 simsobj = new Sims097();
                            simsobj.sims_medicine_code = dr["sims_medicine_code"].ToString();
                            simsobj.sims_medicine_name = dr["sims_medicine_name"].ToString();
                            simsobj.sims_medicine_type = dr["sims_medicine_type"].ToString();
                            simsobj.sims_medicine_typename = dr["sims_medicine_typename"].ToString();
                            simsobj.sims_medicine_batch_number = dr["sims_medicine_batch_number"].ToString();
                            string pd = db.UIDDMMYYYYformat(dr["sims_medicine_production_date"].ToString());
                            
                            if (!string.IsNullOrEmpty(pd))
                                //simsobj.sims_medicine_production_date = DateTime.Parse(dr["sims_medicine_production_date"].ToString()).ToShortDateString();
                                simsobj.sims_medicine_production_date = db.UIDDMMYYYYformat(dr["sims_medicine_production_date"].ToString());
                            
                            pd = db.UIDDMMYYYYformat(dr["sims_medicine_expirey_date"].ToString());
                            if (!string.IsNullOrEmpty(pd))
                                //simsobj.sims_medicine_expirey_date = DateTime.Parse(dr["sims_medicine_expirey_date"].ToString()).ToShortDateString();
                                simsobj.sims_medicine_expirey_date = db.UIDDMMYYYYformat(dr["sims_medicine_expirey_date"].ToString());
                            simsobj.sims_medicine_expirey_alert = dr["sims_medicine_expirey_alert"].ToString();
                            simsobj.sims_medicine_expirey_alertname = dr["sims_medicine_expirey_alertname"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateSims_MedicalMedicine")]
        public HttpResponseMessage InsertUpdateSims_MedicalMedicine(Sims097 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_medical_medicine",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_medicine_code", simsobj.sims_medicine_code),
                            new SqlParameter("@sims_medicine_name", simsobj.sims_medicine_name),
                            new SqlParameter("@sims_medicine_type", simsobj.sims_medicine_type),
                            new SqlParameter("@sims_medicine_batch_number", simsobj.sims_medicine_batch_number),
                            new SqlParameter("@sims_medicine_production_date",db.DBYYYYMMDDformat(simsobj.sims_medicine_production_date)),
                            new SqlParameter("@sims_medicine_expirey_date", db.DBYYYYMMDDformat(simsobj.sims_medicine_expirey_date)),
                            new SqlParameter("@sims_medicine_expirey_alert", simsobj.sims_medicine_expirey_alert),
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("dataforDelete")]
        public HttpResponseMessage dataforDelete(List<Sims097> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : dataforDelete()";
            Log.Debug(string.Format(debug, "PP", "dataforDelete"));

            bool inserted = false;
            try
            {
                foreach (Sims097 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_medical_medicine",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_medicine_code", simsobj.sims_medicine_code),
                            new SqlParameter("@sims_medicine_name", simsobj.sims_medicine_name),
                            new SqlParameter("@sims_medicine_type", simsobj.sims_medicine_type),
                            new SqlParameter("@sims_medicine_batch_number", simsobj.sims_medicine_batch_number),
                            new SqlParameter("@sims_medicine_production_date",db.DBYYYYMMDDformat(simsobj.sims_medicine_production_date)),
                            new SqlParameter("@sims_medicine_expirey_date", db.DBYYYYMMDDformat(simsobj.sims_medicine_expirey_date)),
                            new SqlParameter("@sims_medicine_expirey_alert", simsobj.sims_medicine_expirey_alert),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}