﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.MedicalSusceptibilityController
{
    [RoutePrefix("api/common/MedicalSusceptibility")]
    [BasicAuthentication]
    public class MedicalSusceptibilityController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetSims_MedicalSusceptibility")]
        public HttpResponseMessage GetSims_MedicalSusceptibility()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims100> list = new List<Sims100>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_susceptibility_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'S')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims100 simsobj = new Sims100();
                            simsobj.sims_susceptibility_code =dr["sims_susceptibility_code"].ToString();
                            simsobj.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                            if (dr["sims_susceptibility_status"].ToString().Equals("A"))
                            {
                                simsobj.sims_susceptibility_status = true;
                            }
                            else
                            {
                                simsobj.sims_susceptibility_status = false;
                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertSims_MedicalSusceptibility")]
        public HttpResponseMessage InsertSims_MedicalSusceptibility(Sims100 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_susceptibility_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_susceptibility_code", simsobj.sims_susceptibility_code),
                            new SqlParameter("@sims_susceptibility_desc", simsobj.sims_susceptibility_desc),
                            new SqlParameter("@sims_susceptibility_status", simsobj.sims_susceptibility_status ? "A" : "I")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        
        [Route("dataforDeletemedical")]
        public HttpResponseMessage dataforDeletemedical(List<Sims100> simsobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : dataforDeletemedical()";
            Log.Debug(string.Format(debug, "PP", "dataforDeletemedical"));

            bool inserted = false;
            try
            {
                foreach (Sims100 simsobj in simsobj1)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_medical_susceptibility_proc]",
                            new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_susceptibility_code", simsobj.sims_susceptibility_code),
                                new SqlParameter("@sims_susceptibility_desc", simsobj.sims_susceptibility_desc),
                                new SqlParameter("@sims_susceptibility_status", simsobj.sims_susceptibility_status ? "A" : "I")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}   