﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.health
{
    [RoutePrefix("api/MedicalStudentImmunizationApply")]
    public class MedicalStudentImmunizationApplyController : ApiController
    {
        [Route("getStudentApply")]
        public HttpResponseMessage getStudentApply(string cur_name, string academic_year, string grd_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims564> mod_list = new List<Sims564>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_apply_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims564 simsobj = new Sims564();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.student_age_group = dr["sims_immunization_age_group_desc"].ToString();
                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getStudentForupdate")]
        public HttpResponseMessage getStudentForupdate(string cur_name, string academic_year, string grd_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims564> mod_list = new List<Sims564>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_apply_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims564 simsobj = new Sims564();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.student_age_group = dr["sims_immunization_age_group_desc"].ToString();
                            simsobj.sims_immunization_age_group_code = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.student_immunization_code = dr["sims_immunization_code"].ToString();
                            simsobj.student_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.student_immunization_dosage1 = dr["sims_immunization_dosage1_comment"].ToString();
                            simsobj.student_immunization_dosage2 = dr["sims_immunization_dosage2_comment"].ToString();
                            simsobj.student_immunization_dosage3 = dr["sims_immunization_dosage3_comment"].ToString();
                            simsobj.student_immunization_dosage4 = dr["sims_immunization_dosage4_comment"].ToString();
                            simsobj.student_immunization_dosage5 = dr["sims_immunization_dosage5_comment"].ToString();
                            simsobj.sims_vaccination_date = db.UIDDMMYYYYformat(dr["sims_vaccination_date"].ToString());
                            simsobj.sims_vaccination_renewal_date = db.UIDDMMYYYYformat(dr["sims_vaccination_renewal_date"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getStudentImmunizationdesc")]
        public HttpResponseMessage getStudentImmunizationdesc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims564> mod_list = new List<Sims564>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_apply_proc]",
                    new List<SqlParameter>() { 
                                        new SqlParameter("@opr", "A"),
                                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims564 simsobj = new Sims564();
                            simsobj.student_immunization_code = dr["sims_immunization_code"].ToString();
                            simsobj.student_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.student_age_group = dr["sims_immunization_age_group_desc"].ToString();
                            simsobj.student_immunization_dosage1 = dr["sims_immunization_dosage1"].ToString();
                            simsobj.student_immunization_dosage2 = dr["sims_immunization_dosage2"].ToString();
                            simsobj.student_immunization_dosage3 = dr["sims_immunization_dosage3"].ToString();
                            simsobj.student_immunization_dosage4 = dr["sims_immunization_dosage4"].ToString();
                            simsobj.student_immunization_dosage5 = dr["sims_immunization_dosage5"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("StudentApplyImmunization")]
        public HttpResponseMessage StudentApplyImmunization(List<Sims564> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims564> mod_list = new List<Sims564>();
            string groupcode = null;
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                foreach (Sims564 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        if (obj.sims_vaccination_renewal_date == "undefined" || obj.sims_vaccination_renewal_date == "" || obj.sims_vaccination_renewal_date == null)
                            obj.sims_vaccination_renewal_date = null;

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_medical_immunization_apply_proc]",
                            new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", obj.opr),
                            new SqlParameter("@sims_enroll_number", obj.sims_enroll_number),
                            new SqlParameter("@sims_immunization_age_group_code", obj.sims_immunization_age_group_code),
                            new SqlParameter("@sims_immunization_code", obj.student_immunization_code),
                            new SqlParameter("@sims_vaccination_date", db.DBYYYYMMDDformat(obj.sims_vaccination_date)),
                            new SqlParameter("@sims_vaccination_renewal_date", db.DBYYYYMMDDformat(obj.sims_vaccination_renewal_date)),
                            new SqlParameter("@sims_immunization_desc", obj.student_immunization_desc),
                            new SqlParameter("@sims_immunization_dosage1_comment", obj.student_immunization_dosage1),
                            new SqlParameter("@sims_immunization_dosage2_comment", obj.student_immunization_dosage2),
                            new SqlParameter("@sims_immunization_dosage3_comment", obj.student_immunization_dosage3),
                            new SqlParameter("@sims_immunization_dosage4_comment", obj.student_immunization_dosage4),
                            new SqlParameter("@sims_immunization_dosage5_comment", obj.student_immunization_dosage5),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

    }
}