﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.Incidence
{
    [RoutePrefix("api/incidence")]
    public class IncidenceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //API Detention
        [Route("getDetentionByIndex")]
        public HttpResponseMessage getDetentionByIndex()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getDetention"));

            List<Dtcn01> incidence_list = new List<Dtcn01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Dtcn01 Simsobj = new Dtcn01();

                            Simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            Simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                            Simsobj.sims_detention_name_ot = dr["sims_detention_name_ot"].ToString();
                            Simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                            Simsobj.sims_display_order = dr["sims_display_order"].ToString();
                            Simsobj.sims_detention_level_status = dr["sims_detention_level_status"].ToString().Equals("A") ? true : false;
                            Simsobj.sims_level_created_user_code = dr["sims_level_created_user_code"].ToString();
                            Simsobj.sims_level_created_date = dr["sims_level_created_user_code"].ToString();

                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }


        [Route("CUDDetention")]
        public HttpResponseMessage CUDDetention(List<Dtcn01> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Dtcn01 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Detention_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                 new SqlParameter("@sims_detention_level_code",simsobj.sims_detention_level_code),
                                 new SqlParameter("@sims_detention_name", simsobj.sims_detention_name),
                                 new SqlParameter("@sims_detention_name_ot", simsobj.sims_detention_name_ot),
                                 new SqlParameter("@sims_detention_point", simsobj.sims_detention_point),
                                 new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                                 new SqlParameter("@sims_level_created_user_code", simsobj.sims_level_created_user_code),
                                 new SqlParameter("@sims_detention_level_status", simsobj.sims_detention_level_status==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}


