﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.Incidence
{
    [RoutePrefix("api/incidence")]
    public class IncidenceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region(Incidence Timeline)
        [Route("getAutoGenerateTimeline")]
        public HttpResponseMessage getAutoGenerateTimeline()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateTimeline(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateTimeline"));

            List<Sims153> code_list = new List<Sims153>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence_timeline]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('0000' + CONVERT(NVARCHAR(4),isnull(max(sims_incidence_timeline_code),0)+1),4) as max"),
                                new SqlParameter("@tbl_cond", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims153 obj = new Sims153();
                            if (dr["max"].ToString() != "")
                                obj.sims_incidence_timeline_code = (dr["max"].ToString());
                            else
                                obj.sims_incidence_timeline_code = "0001";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getIncidenceTimelineByIndex")]
        public HttpResponseMessage getIncidenceTimelineByIndex()
        {
            
            List<Sims153> timeline_list = new List<Sims153>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_timeline",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims153 Simsobj = new Sims153();
                            Simsobj.sims_incidence_timeline_code = dr["sims_incidence_timeline_code"].ToString();
                            Simsobj.sims_incidence_timeline_desc = dr["sims_incidence_timeline_desc"].ToString();
                            Simsobj.sims_incidence_timeline_start_time = Convert.ToDateTime(dr["sims_incidence_timeline_start_time"].ToString()).ToShortTimeString();
                            Simsobj.sims_incidence_timeline_end_time = Convert.ToDateTime(dr["sims_incidence_timeline_end_time"].ToString()).ToShortTimeString();
                            Simsobj.sims_incidence_timeline_status = dr["sims_incidence_timeline_status"].ToString().Equals("A") ? true : false;
                            timeline_list.Add(Simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, timeline_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, timeline_list);
        }

        [Route("CUDTimelineDetails")]
        public HttpResponseMessage CUDTimelineDetails(List<Sims153> data)
        {
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims153 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_timeline",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_incidence_timeline_code", simsobj.sims_incidence_timeline_code),
                                new SqlParameter("@sims_incidence_timeline_desc", simsobj.sims_incidence_timeline_desc),
                                new SqlParameter("@sims_incidence_timeline_start_time", Convert.ToDateTime(simsobj.sims_incidence_timeline_start_time.ToString()).ToShortTimeString()),
                                new SqlParameter("@sims_incidence_timeline_end_time", Convert.ToDateTime(simsobj.sims_incidence_timeline_end_time.ToString()).ToShortTimeString()),
                                new SqlParameter("@sims_incidence_timeline_status", simsobj.sims_incidence_timeline_status == true?"A":"I"),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();

                        }
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region(Incidence Warning)
        [Route("getAutoGenerateWarning")]
        public HttpResponseMessage getAutoGenerateWarning()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateWarning(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateWarning"));

            List<Sims154> code_list = new List<Sims154>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence_warning]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('0000' + CONVERT(NVARCHAR(4),isnull(max(sims_incidence_warning_code),0)+1),4) as max"),
                                new SqlParameter("@tbl_cond", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims154 obj = new Sims154();
                            if (dr["max"].ToString() != "")
                                obj.sims_incidence_warning_code = (dr["max"].ToString());
                            else
                                obj.sims_incidence_warning_code = "0001";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getIncidenceWarningByIndex")]
        public HttpResponseMessage getIncidenceWarningByIndex()
        {
            
            List<Sims154> warning_list = new List<Sims154>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_warning",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims154 Simsobj = new Sims154();
                            Simsobj.sims_incidence_warning_code = dr["sims_incidence_warning_code"].ToString();
                            Simsobj.sims_incidence_warning_desc = dr["sims_incidence_warning_desc"].ToString();
                            Simsobj.sims_incidence_warning_status = dr["sims_incidence_warning_status"].ToString().Equals("A") ? true : false;
                            warning_list.Add(Simsobj);

                          
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, warning_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, warning_list);
        }

        [Route("CUDWarningDetails")]
        public HttpResponseMessage CUDWarningDetails(List<Sims154> data)
        {
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims154 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_warning",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_incidence_warning_code",simsobj.sims_incidence_warning_code),
                                new SqlParameter("@sims_incidence_warning_desc",simsobj.sims_incidence_warning_desc),
                                new SqlParameter("@sims_incidence_warning_status", simsobj.sims_incidence_warning_status == true?"A":"I"),                                
                         });
                            if (dr.RecordsAffected > 0)
                            {
                              inserted=true;
                            }
                            dr.Close();
                           
                        }
                    }
                }
                else
                {
                    inserted=false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region(Incidence)

        [Route("getAutoGenerateIncidenceNumber")]
        public HttpResponseMessage getAutoGenerateIncidenceNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateIncidenceNumber(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateIncidenceNumber"));

            List<Sims149> code_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('0000000000' + CONVERT(NVARCHAR(4),isnull(max([sims_incidence_number]),0)+1),10) as max"),
                                new SqlParameter("@tbl_cond", "")                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 obj = new Sims149();
                            if (dr["max"].ToString() != "")
                                obj.sims_incidence_number = (dr["max"].ToString());
                            else
                                obj.sims_incidence_number = "0000000001";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getAllBuildingCode")]
        public HttpResponseMessage getAllBuildingCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBuildingCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllBuildingCode"));

            List<Sims149> building_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_building_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","S"),                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsObj = new Sims149();
                            simsObj.sims_incidence_building_code = dr["sims_building_code"].ToString();
                            simsObj.sims_incidence_building_desc = dr["sims_building_desc"].ToString();                            
                            building_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, building_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, building_list);
            }
        }

        [Route("getAllLocationCode")]
        public HttpResponseMessage getAllLocationCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLocationCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllLocationCode"));

            List<Sims149> location_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_location_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S")                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsObj = new Sims149();
                            simsObj.sims_incidence_location_code = dr["sims_location_code"].ToString();
                            simsObj.sims_incidence_location_desc = dr["sims_location_desc"].ToString();
                            location_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, location_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, location_list);
            }
        }

        [Route("getAllWarningCode")]
        public HttpResponseMessage getAllWarningCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllWarningCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllWarningCode"));

            List<Sims149> warning_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_warning_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsObj = new Sims149();
                            simsObj.sims_incidence_warning_code = dr["sims_incidence_warning_code"].ToString();
                            simsObj.sims_incidence_warning_desc = dr["sims_incidence_warning_desc"].ToString();
                            warning_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, warning_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, warning_list);
            }
        }

        [Route("getAllActionCodes")]
        public HttpResponseMessage getAllActionCodes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllActionCodes(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllActionCodes"));

            List<Sims149> actioncode_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_action_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsObj = new Sims149();
                            simsObj.sims_incidence_action_code = dr["sims_action_code"].ToString();
                            simsObj.sims_incidence_action_code_name = dr["Action_Des"].ToString();
                            actioncode_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, actioncode_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, actioncode_list);
            }
        }

        [Route("getllActionTypeNames")]
        public HttpResponseMessage getllActionTypeNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getllActionTypeNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getllActionTypeNames"));

            List<Sims149> actiontypename_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_form_field_value1],[sims_appl_parameter]"),
                                new SqlParameter("@tbl_cond", "[sims_mod_code]=" + "'" + CommonStaticClass.sims_mod_code_incidence + "'" + "and [sims_appl_code]=" + "'" + CommonStaticClass.sims_appl_code_incidence + "'" + "And [sims_appl_form_field]=" + "'" + CommonStaticClass.sims_appl_form_field_incidence + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 SimsObj = new Sims149();
                            SimsObj.sims_incidence_action_type = dr["sims_appl_parameter"].ToString();
                            SimsObj.sims_incidence_action_type_desc = dr["sims_appl_form_field_value1"].ToString();
                            actiontypename_list.Add(SimsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, actiontypename_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, actiontypename_list);
            }
        }

        [Route("getAllConsequenceCode")]
        public HttpResponseMessage getAllConsequenceCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllConsequenceCode(),PARAMETERS :: N0";
            Log.Debug(string.Format(debug, "STUDENT", "getAllConsequenceCode"));

            List<Sims149> consequence_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_consequence_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsObj = new Sims149();
                            simsObj.sims_incidence_consequence_code = dr["sims_incidence_consequence_code"].ToString();
                            simsObj.sims_incidence_consequence_code_name = dr["sims_incidence_consequence_desc"].ToString();
                            consequence_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, consequence_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, consequence_list);
            }
        }

        [Route("getConsequenceTypeNames")]
        public HttpResponseMessage getConsequenceTypeNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getConsequenceTypeNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getConsequenceTypeNames"));

            List<Sims149> type_list = new List<Sims149>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1","sims_appl_parameter, [sims_appl_form_field_value1]"),
                                new SqlParameter("@tbl_cond", "[sims_mod_code]=" + "'" + CommonStaticClass.sims_mod_code_incidence + "'" + "and [sims_appl_code]=" + "'" + CommonStaticClass.sims_appl_code_incidence + "'" + "And [sims_appl_form_field]=" + "'" + CommonStaticClass.sims_appl_form_field_incidence_1 + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 SimsObj = new Sims149();
                            SimsObj.sims_incidence_consequence_type = dr["sims_appl_parameter"].ToString();
                            SimsObj.sims_incidence_consequence_type_desc = dr["sims_appl_form_field_value1"].ToString();
                            type_list.Add(SimsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }

        [Route("getIncidencesByIndex")]
        public HttpResponseMessage getIncidencesByIndex()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getIncidences(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getIncidences"));
           
            List<Sims149> incidence_list = new List<Sims149>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_incidence),
                                new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence),
                                new SqlParameter("@sims_appl_form_field1", CommonStaticClass.sims_appl_form_field_incidence_1),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 Simsobj = new Sims149();
                            Simsobj.sims_incidence_number = dr["sims_incidence_number"].ToString();
                            Simsobj.sims_incidence_desc = dr["sims_incidence_desc"].ToString();
                            Simsobj.sims_incidence_enroll_number = dr["sims_incidence_enroll_number"].ToString();

                            Simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            Simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            
                            if (!string.IsNullOrEmpty(dr["sims_incidence_date"].ToString()))
                           
                            //Simsobj.sims_incidence_date = DateTime.Parse(dr["sims_incidence_date"].ToString()).ToShortDateString();
                                Simsobj.sims_incidence_date = dr["sims_incidence_date"].ToString();
                            Simsobj.sims_incidence_building_code = dr["sims_incidence_building_code"].ToString();
                            Simsobj.sims_incidence_building_desc = dr["Building_Description"].ToString();
                            Simsobj.sims_incidence_location_code = dr["sims_incidence_location_code"].ToString();
                            Simsobj.sims_incidence_location_desc = dr["Location_Description"].ToString();
                            Simsobj.sims_incidence_action_code = dr["sims_incidence_action_code"].ToString();
                            Simsobj.sims_incidence_action_type_desc = dr["Action_Type"].ToString();
                            Simsobj.sims_incidence_action_type = dr["sims_incidence_action_type"].ToString();
                            Simsobj.sims_incidence_action_code_name = dr["Action_Description"].ToString();
                            Simsobj.sims_incidence_action_desc = dr["sims_incidence_action_desc"].ToString();
                            Simsobj.sims_incidence_action_escalation_flag = dr["sims_incidence_action_escalation_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_incidence_action_sms_flag = dr["sims_incidence_action_sms_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_incidence_action_email_flag = dr["sims_incidence_action_email_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_incidence_action_portal_flag = dr["sims_incidence_action_portal_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_incidence_action_roll_over_flag = dr["sims_incidence_action_roll_over_flag"].ToString().Equals("Y") ? true : false;
                            if (!string.IsNullOrEmpty(dr["sims_incidence_action_point"].ToString()))
                            Simsobj.sims_incidence_action_point = decimal.Parse(dr["sims_incidence_action_point"].ToString());
                            Simsobj.sims_incidence_warning_code = dr["sims_incidence_warning_code"].ToString();
                            Simsobj.sims_incidence_warning_desc = dr["Warning_Description"].ToString();
                            Simsobj.sims_incidence_consequence_code = dr["sims_incidence_consequence_code"].ToString();
                            Simsobj.sims_incidence_consequence_code_name = dr["Consequence_Description"].ToString();
                            Simsobj.sims_incidence_consequence_desc = dr["sims_incidence_consequence_desc"].ToString();
                            Simsobj.sims_incidence_consequence_type = dr["sims_incidence_consequence_type"].ToString();
                            Simsobj.sims_incidence_consequence_type_desc = dr["Consequence_Type"].ToString();
                            Simsobj.sims_incidence_consequence_point = dr["sims_incidence_consequence_point"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_incidence_consequence_serve_start_date"].ToString()))
                                Simsobj.sims_incidence_consequence_serve_start_date = dr["sims_incidence_consequence_serve_start_date"].ToString();
                            Simsobj.sims_incidence_consequence_serve_close_flag = dr["sims_incidence_consequence_serve_close_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_incidence_user_code = dr["sims_incidence_user_code"].ToString();
                            Simsobj.sims_incidence_user_code_updated = dr["sims_incidence_user_code_updated"].ToString();
                            Simsobj.sims_incidence_status = dr["sims_incidence_status"].ToString().Equals("A") ? true : false;
                            incidence_list.Add(Simsobj);
                          
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }

        [Route("GetAllTeacherName")]
        public HttpResponseMessage GetAllTeacherName()
        {
            List<Sims149> term_list = new List<Sims149>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims149 simsobj = new Sims149();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_incidence_user_code_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_incidence_user_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_incidence_user_code_updated_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_incidence_user_code_updated = dr["sims_employee_code"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("CUDIncidenceDetails")]
        public HttpResponseMessage CUDIncidenceDetails(List<Sims149> data)
        {

            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims149 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_incidence_proc]",
                                new List<SqlParameter>() 
                         { 

                         new SqlParameter("@opr", simsobj.opr),
                         new SqlParameter("@sims_incidence_number",simsobj.sims_incidence_number),
                         new SqlParameter("@sims_incidence_desc", simsobj.sims_incidence_desc),
                         new SqlParameter("@sims_incidence_enroll_number", simsobj.sims_incidence_enroll_number),
                         new SqlParameter("@sims_incidence_date",db.DBYYYYMMDDformat(simsobj.sims_incidence_date)),
                         new SqlParameter("@sims_incidence_building_code", simsobj.sims_incidence_building_code),
                         new SqlParameter("@sims_incidence_location_code", simsobj.sims_incidence_location_code),
                         new SqlParameter("@sims_incidence_action_code", simsobj.sims_incidence_action_code),
                         new SqlParameter("@sims_incidence_action_type",  simsobj.sims_incidence_action_type),
                         new SqlParameter("@sims_incidence_action_desc", simsobj.sims_incidence_action_desc),
                         new SqlParameter("@sims_incidence_action_escalation_flag",simsobj.sims_incidence_action_escalation_flag == true? "Y":"N"),
                         new SqlParameter("@sims_incidence_action_sms_flag",simsobj.sims_incidence_action_sms_flag == true?"Y":"N"),                                
                         new SqlParameter("@sims_incidence_action_email_flag", simsobj.sims_incidence_action_email_flag == true?"Y":"N"),                                                             
                         new SqlParameter("@sims_incidence_action_portal_flag", simsobj.sims_incidence_action_portal_flag == true?"Y":"N"),                              
                         new SqlParameter("@sims_incidence_action_roll_over_flag", simsobj.sims_incidence_action_roll_over_flag == true?"Y":"N"),
                         new SqlParameter("@sims_incidence_action_point", int.Parse(simsobj.sims_incidence_action_point.ToString())),
                         new SqlParameter("@sims_incidence_warning_code", simsobj.sims_incidence_warning_code),
                         new SqlParameter("@sims_incidence_consequence_code", simsobj.sims_incidence_consequence_code),
                         new SqlParameter("@sims_incidence_consequence_desc", simsobj.sims_incidence_consequence_desc),
                         new SqlParameter("@sims_incidence_consequence_type",simsobj.sims_incidence_consequence_type),
                         new SqlParameter("@sims_incidence_consequence_point", simsobj.sims_incidence_consequence_point),
                         new SqlParameter("@sims_incidence_consequence_serve_start_date",db.DBYYYYMMDDformat(simsobj.sims_incidence_consequence_serve_start_date)),
                         new SqlParameter("@sims_incidence_consequence_serve_close_flag", simsobj.sims_incidence_consequence_serve_close_flag == true?"Y":"N"),
                         new SqlParameter("@sims_incidence_user_code", simsobj.sims_incidence_user_code),
                         new SqlParameter("@sims_incidence_user_code_updated", simsobj.sims_incidence_user_code_updated),
                         new SqlParameter("@sims_incidence_status", simsobj.sims_incidence_status == true?"A":"I"),
                         new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                         new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_incidence),
                         new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence),
                         new SqlParameter("@sims_appl_form_field1", CommonStaticClass.sims_appl_form_field_incidence_1)
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                        
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region(Incidence Action category)

        [Route("getAutoGenerateIncidenceActionCategory")]
        public HttpResponseMessage getAutoGenerateIncidenceActionCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateIncidenceActionCategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateIncidenceActionCategory"));

            List<Sims151> code_list = new List<Sims151>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence_action_category]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('00' + CONVERT(NVARCHAR(4),isnull(max([sims_incidence_action_code]),0)+1),2) as max"),
                                new SqlParameter("@tbl_cond", ""),                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims151 obj = new Sims151();
                            if (dr["max"].ToString() != "")
                                obj.sims_incidence_action_code = (dr["max"].ToString());
                            else
                                obj.sims_incidence_action_code = "01";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getIncidenceActionCategoryByIndex")]
        public HttpResponseMessage getIncidenceActionCategoryByIndex(int PageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getIncidenceActionCategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getIncidenceActionCategory"));
           
            List<Sims151> action_list = new List<Sims151>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_action_category",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims151 Simsobj = new Sims151();
                            Simsobj.sims_incidence_action_code = dr["sims_incidence_action_code"].ToString();
                            Simsobj.sims_incidence_action_code_desc = dr["sims_incidence_action_code_desc"].ToString();
                            Simsobj.sims_incidence_action_status = dr["sims_incidence_action_status"].ToString().Equals("A") ? true : false;
                            action_list.Add(Simsobj);

                                                   }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, action_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, action_list);
        }

        [Route("CUDIncidenceActionCategoryDetails")]
        public HttpResponseMessage CUDIncidenceActionCategoryDetails(List<Sims151> data)
        {
            bool inserted = false;
           
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims151 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_action_category",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_incidence_action_code",simsobj.sims_incidence_action_code),
                                new SqlParameter("@sims_incidence_action_code_desc", simsobj.sims_incidence_action_code_desc),
                                new SqlParameter("@sims_incidence_action_status", simsobj.sims_incidence_action_status == true?"A":"I"),                                
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            dr.Close();
                           
                        }
                    }
                }
                else
                {
                    inserted = false;
                   
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region(incidence consequence)
        [Route("getAutoGenerateConsequence")]
        public HttpResponseMessage getAutoGenerateConsequence()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateConsequence(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateConsequence"));

            List<Sims152> code_list = new List<Sims152>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence_consequence]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('0000' + CONVERT(NVARCHAR(4),isnull(max(sims_incidence_consequence_code),0)+1),4) as max"),
                                new SqlParameter("@tbl_cond", ""),                      
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims152 obj = new Sims152();
                            if (dr["max"].ToString() != "")
                                obj.sims_incidence_consequence_code = (dr["max"].ToString());
                            else
                                obj.sims_incidence_consequence_code = "0001";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getAllConsequenceTypeName")]
        public HttpResponseMessage getAllConsequenceTypeName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllConsequenceTypeName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllConsequenceTypeName"));

            List<Sims152> type_list = new List<Sims152>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_form_field_value1]"),
                                new SqlParameter("@tbl_cond", "[sims_mod_code]=" + "'" + CommonStaticClass.sims_mod_code_incidence + "'" + "and [sims_appl_code]=" + "'" + CommonStaticClass.sims_appl_code_incidence_consequence + "'" + "And [sims_appl_form_field]=" + "'" + CommonStaticClass.sims_appl_form_field_incidence_consequence + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims152 SimsObj = new Sims152();
                            SimsObj.sims_incidence_consequence_type = dr["sims_appl_form_field_value1"].ToString();
                            type_list.Add(SimsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }

        [Route("getConsequenceByIndex")]
        public HttpResponseMessage getConsequenceByIndex()
        {
           
            List<Sims152> consequence_list = new List<Sims152>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_consequence",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_incidence_consequence),
                                new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence_consequence),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims152 Simsobj = new Sims152();
                            Simsobj.sims_incidence_consequence_cur_code = dr["sims_incidence_consequence_cur_code"].ToString();
                            Simsobj.sims_incidence_consequence_cur_name = dr["Cur_name"].ToString();
                            Simsobj.sims_cur_level_code = dr["sims_incidence_consequence_cur_level_code"].ToString();
                            Simsobj.cur_level_code_name = dr["Cur_level_name"].ToString();
                            Simsobj.sims_incidence_consequence_code = dr["sims_incidence_consequence_code"].ToString();
                            Simsobj.sims_incidence_consequence_desc = dr["sims_incidence_consequence_desc"].ToString();
                            Simsobj.sims_incidence_consequence_type = dr["Consequence_Type"].ToString();
                            Simsobj.sims_incidence_consequence_point = dr["sims_incidence_consequence_point"].ToString();
                            Simsobj.sims_incidence_consequence_status = dr["sims_incidence_consequence_status"].ToString().Equals("A") ? true : false;
                            consequence_list.Add(Simsobj);

                           
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, consequence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, consequence_list);
        }

        [Route("CUDConsequenceDetails")]
        public HttpResponseMessage CUDConsequenceDetails(List<Sims152> data)
        {
            bool inserted = false;
        
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims152 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_consequence",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_incidence_consequence_cur_code", simsobj.sims_incidence_consequence_cur_code),// simsobj.sims_incidence_consequence_cur_code;
                                new SqlParameter("@sims_incidence_consequence_cur_level_code", simsobj.sims_incidence_consequence_cur_code),
                                new SqlParameter("@sims_incidence_consequence_code", simsobj.sims_incidence_consequence_code),
                                new SqlParameter("@sims_incidence_consequence_desc", simsobj.sims_incidence_consequence_desc),
                                new SqlParameter("@sims_incidence_consequence_type", simsobj.sims_incidence_consequence_type),//simsobj.sims_incidence_consequence_type;
                                new SqlParameter("@sims_incidence_consequence_point", Convert.ToInt32(simsobj.sims_incidence_consequence_point.ToString())),
                                new SqlParameter("@sims_incidence_consequence_status", simsobj.sims_incidence_consequence_status == true?"A":"I"),
                                new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_incidence_consequence),
                                new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence_consequence),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region(Incidence Action)

        [Route("getAutoGenerateIncidenceActionCode")]
        public HttpResponseMessage getAutoGenerateIncidenceActionCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateIncidenceNumber(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateIncidenceNumber"));

            List<Sims150> code_list = new List<Sims150>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_incidence_action]"),
                                new SqlParameter("@tbl_col_name1", "RIGHT('0000' + CONVERT(NVARCHAR(4),isnull(max([sims_action_sub_code]),0)+1),4) as max"),
                                new SqlParameter("@tbl_cond", ""),            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims150 obj = new Sims150();
                            if (dr["max"].ToString() != "")
                                obj.sims_action_sub_code = (dr["max"].ToString());
                            else
                                obj.sims_action_sub_code = "0001";
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getAllActionCode1")]
        public HttpResponseMessage getAllActionCode1()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllActionCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllActionCode"));

            List<Sims150> action_list = new List<Sims150>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                    new SqlParameter("@tbl_name", "[sims].[sims_incidence_action_category]"),
                                    new SqlParameter("@tbl_col_name1", "[sims_incidence_action_code_desc]"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims150 simsObj = new Sims150();
                            simsObj.sims_action_code = dr["sims_incidence_action_code_desc"].ToString();
                            action_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, action_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, action_list);
            }
        }

        [Route("getAllActionTypeNames")]
        public HttpResponseMessage getAllActionTypeNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllActionTypeNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllActionTypeNames"));

            List<Sims150> name_list = new List<Sims150>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_form_field_value1]"),
                                new SqlParameter("@tbl_cond", "[sims_mod_code]=" + "'" + CommonStaticClass.sims_mod_code_incidence + "'" + "and [sims_appl_code]=" + "'" + CommonStaticClass.sims_appl_code_incidence_action + "'" + "And [sims_appl_form_field]=" + "'" + CommonStaticClass.sims_appl_form_field_incidence_action + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims150 SimsObj = new Sims150();
                            SimsObj.sims_action_type = dr["sims_appl_form_field_value1"].ToString();
                            name_list.Add(SimsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, name_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, name_list);
            }
        }

        [Route("getAllActionColorTypeNames")]
        public HttpResponseMessage getAllActionColorTypeNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllActionColorTypeNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllActionColorTypeNames"));

            List<Sims150> name_list = new List<Sims150>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_form_field_value1]"),
                                new SqlParameter("@tbl_cond", "[sims_mod_code]=" + "'" + CommonStaticClass.sims_mod_code_incidence + "'" + "and [sims_appl_code]=" + "'" + CommonStaticClass.sims_appl_code_incidence_action + "'" + "And [sims_appl_form_field]=" + "'" + CommonStaticClass.sims_appl_form_field_incidence_action_1 + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims150 SimsObj = new Sims150();
                            SimsObj.sims_action_color_code = dr["sims_appl_form_field_value1"].ToString();
                            name_list.Add(SimsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, name_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, name_list);
            }
        }

        [Route("getIncidenceActionByIndex")]
        public HttpResponseMessage getIncidenceActionByIndex()
        {
            
            List<Sims150> incidenceaction_list = new List<Sims150>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_action",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_incidence_action),
                                new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence_action),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims150 Simsobj = new Sims150();
                            Simsobj.sims_cur_name = dr["Cur_name"].ToString();
                            Simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            Simsobj.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                            Simsobj.cur_level_name = dr["Cur_level_name"].ToString();
                            Simsobj.sims_action_code = dr["Action_Des"].ToString();
                            Simsobj.sims_action_sub_code = dr["sims_action_sub_code"].ToString();
                            Simsobj.sims_action_desc = dr["sims_action_desc"].ToString();
                            Simsobj.sims_action_type = dr["Action_Type"].ToString();
                            string s = dr["sims_action_color_code"].ToString();
                            Simsobj.sims_action_color_code = dr["sims_action_color_code"].ToString();
                            Simsobj.sims_action_escalation_flag = dr["sims_action_escalation_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_action_sms_flag = dr["sims_action_sms_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_action_email_flag = dr["sims_action_email_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_action_portal_flag = dr["sims_action_portal_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_action_roll_over_flag = dr["sims_action_roll_over_flag"].ToString().Equals("Y") ? true : false;
                            Simsobj.sims_action_point = dr["sims_action_point"].ToString();
                            Simsobj.sims_action_status = dr["sims_action_status"].ToString().Equals("A") ? true : false;
                            incidenceaction_list.Add(Simsobj);
                           
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidenceaction_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidenceaction_list);
        }

        [Route("CUDIncidenceActionDetails")]
        public HttpResponseMessage CUDIncidenceActionDetails(List<Sims150> data)
        {
            bool inserted = false;
           
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims150 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims_incidence_action",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@cur_code",simsobj.sims_cur_code),
                                new SqlParameter("@cur_level_code",simsobj.sims_cur_level_code),
                                new SqlParameter("@sims_action_code",simsobj.sims_action_code),
                                new SqlParameter("@sims_action_sub_code",simsobj.sims_action_sub_code),
                                new SqlParameter("@sims_action_desc",simsobj.sims_action_desc),
                                new SqlParameter("@sims_action_type",simsobj.sims_action_type),
                                new SqlParameter("@sims_action_color_code",simsobj.sims_action_color_code),
                                new SqlParameter("@sims_action_escalation_flag", simsobj.sims_action_escalation_flag == true?"Y":"N"),
                                new SqlParameter("@sims_action_sms_flag", simsobj.sims_action_sms_flag == true?"Y":"N"),
                                new SqlParameter("@sims_action_email_flag", simsobj.sims_action_email_flag == true?"Y":"N"),
                                new SqlParameter("@sims_action_portal_flag", simsobj.sims_action_portal_flag == true?"Y":"N"),
                                new SqlParameter("@sims_action_roll_over_flag", simsobj.sims_action_roll_over_flag == true?"Y":"N"),
                                new SqlParameter("@sims_action_point", !string.IsNullOrEmpty(simsobj.sims_action_point)?Convert.ToInt32(simsobj.sims_action_point.ToString()):0),
                                new SqlParameter("@sims_action_status",simsobj.sims_action_status == true? "A":"I"),
                                new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code_incidence),
                                new SqlParameter("@sims_appl_code",CommonStaticClass.sims_appl_code_incidence_action),
                                new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field_incidence_action),
                    });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;

                            }
                            dr.Close();
                        }
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region (get curiculum and curiculum level)
        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getCuriculum"));

            List<Sims001> lstCuriculum = new List<Sims001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@tbl_name", "sims.sims_cur"),
                            new SqlParameter("@tbl_col_name1", "*"),
                           new SqlParameter("@tbl_cond", "sims_cur_status='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims001 cur = new Sims001();
                            cur.sims_cur_code = dr["sims_cur_code"].ToString();
                            cur.sims_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getCuriculumLevels")]
        public HttpResponseMessage getCuriculumLevels(string curcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculumLevels(),PARAMETERS :: CURCODE{2}";
            Log.Debug(string.Format(debug, "COMMON", "getCuriculumLevels", curcode));

            List<Sims001_curlevel> curlevel_list = new List<Sims001_curlevel>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@tbl_name", "[sims].[sims_cur_level]"),
                            new SqlParameter("@tbl_col_name1", "[sims_cur_level_code],[sims_cur_level_name_en]"),
                           new SqlParameter("@tbl_cond", "[sims_cur_level_status]='A' and sims_cur_code='"+curcode+"'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims001_curlevel simsobj = new Sims001_curlevel();
                            simsobj.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                            simsobj.sims_cur_level_name_en = dr["sims_cur_level_name_en"].ToString();
                            curlevel_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, curlevel_list);
        }
        #endregion
    }
}