﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.STUDENT.sibling_details_mode;

namespace SIMSAPI.Controllers.incidence
{
    [RoutePrefix("api/DetentionHistoryRptController")]
    public class DetentionHistoryRptController : ApiController
    {
        [Route("getAttendanceCuriculum")]
        public HttpResponseMessage getAttendanceCuriculum()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendanceyear")]
        public HttpResponseMessage getAttendanceyear(string cur_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancegrade")]
        public HttpResponseMessage getAttendancegrade(string cur_code, string academic_year)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "N"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancesectionTWAR")]
        public HttpResponseMessage getAttendancesectionTWAR(string cur_code, string academic_year, string grade_code)
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_grade_code", grade_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancetableTWAR")]
        public HttpResponseMessage getAttendancetableTWAR(string cur_code, string academic_year, string grade_code, string sectionCode)
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_detention_history]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", academic_year),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@section_code", sectionCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_class_name = dr["grade"].ToString();
                            simsobj.sims_student_name = dr["student_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getReportName")]
        public HttpResponseMessage getReportName()
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_detention_history]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_report_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        
       
    }
}