﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;





namespace SIMSAPI.Controllers.Detension
{
    [RoutePrefix("api/detension")]

    public class DetensionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //API Detention
        [Route("getDetentionByIndex")]
        public HttpResponseMessage getDetentionByIndex()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getDetention"));

            List<Dtcn01> incidence_list = new List<Dtcn01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Dtcn01 Simsobj = new Dtcn01();

                            Simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            Simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                            Simsobj.sims_detention_name_ot = dr["sims_detention_name_ot"].ToString();
                            Simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                            Simsobj.sims_display_order = dr["sims_display_order"].ToString();
                            Simsobj.sims_detention_level_status = dr["sims_detention_level_status"].ToString().Equals("A") ? true : false;
                            Simsobj.sims_level_created_user_code = dr["sims_level_created_user_code"].ToString();
                            Simsobj.sims_level_created_date = dr["sims_level_created_user_code"].ToString();

                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }

        [Route("getDetentionByIndex1")]
        public HttpResponseMessage getDetentionByIndex1()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getDetention"));

            List<Dtcn01> incidence_list = new List<Dtcn01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "SS"),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Dtcn01 Simsobj = new Dtcn01();

                            Simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            Simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                            Simsobj.sims_detention_name_ot = dr["sims_detention_name_ot"].ToString();
                            Simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                            Simsobj.sims_display_order = dr["sims_display_order"].ToString();
                            Simsobj.sims_detention_level_status = dr["sims_detention_level_status"].ToString().Equals("A") ? true : false;
                            Simsobj.sims_level_created_user_code = dr["sims_level_created_user_code"].ToString();
                            Simsobj.sims_level_created_date = dr["sims_level_created_user_code"].ToString();
                            Simsobj.sims_level_created_date = dr["sims_level_created_user_code"].ToString();
                            Simsobj.sims_user_access_role = dr["sims_user_access_role"].ToString();
                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }


        [Route("CUDDetention")]
        public HttpResponseMessage CUDDetention(List<Dtcn01> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Dtcn01 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Detention_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                 new SqlParameter("@sims_detention_level_code",simsobj.sims_detention_level_code),
                                 new SqlParameter("@sims_detention_name", simsobj.sims_detention_name),
                                 new SqlParameter("@sims_detention_name_ot", simsobj.sims_detention_name_ot),
                                 new SqlParameter("@sims_detention_point", simsobj.sims_detention_point),
                                 new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                                 new SqlParameter("@sims_level_created_user_code", simsobj.sims_level_created_user_code),
                                 new SqlParameter("@sims_detention_level_status", simsobj.sims_detention_level_status==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDDetention1")]
        public HttpResponseMessage CUDDetention1(List<Dtcn01> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Dtcn01 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Detention_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                 new SqlParameter("@sims_detention_level_code",simsobj.sims_detention_level_code),
                                 new SqlParameter("@sims_detention_name", simsobj.sims_detention_name),
                                 new SqlParameter("@sims_detention_name_ot", simsobj.sims_detention_name_ot),
                                 new SqlParameter("@sims_detention_point", simsobj.sims_detention_point),
                                 new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                                 new SqlParameter("@sims_level_created_user_code", simsobj.sims_level_created_user_code),
                                 new SqlParameter("@sims_detention_level_status", simsobj.sims_detention_level_status==true?"A":"I"),
                                 new SqlParameter("@sims_user_access_role", simsobj.sims_user_access_role),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }




        //Detension Summary Report API//



        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getDetentionSummaryReport")]
        public HttpResponseMessage getDetentionSummaryReport(string cur_code, string academic_year, string grade_code, string section_code,string mom_start_date, string mom_end_date)
        {
            List<DSR001> lstCuriculum = new List<DSR001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@mom_start_date", db.DBYYYYMMDDformat(mom_start_date)),
                             new SqlParameter("@mom_end_date", db.DBYYYYMMDDformat(mom_end_date)),
                            // new SqlParameter("@user_number", lecture_count),
                             //new SqlParameter("@value", String.IsNullOrEmpty(lecture_count)?null:lecture_count)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSR001 sequence = new DSR001();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            sequence.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class_name = dr["class_name"].ToString();
                            sequence.sims_detention_name = dr["sims_detention_name"].ToString();
                            sequence.sims_detention_description = dr["sims_detention_description"].ToString();
                            sequence.sims_detention_point = dr["sims_detention_point"].ToString();
                            sequence.sims_detention_remark = dr["sims_detention_remark"].ToString();
                            sequence.date = dr["date"].ToString();
                            sequence.reg_by = dr["reg_by"].ToString();

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }



        [Route("remove_detention")]
        public HttpResponseMessage remove_detention(List<DSR001> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DSR001 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Detention_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),

                                 new SqlParameter("@cur_code",simsobj.cur_code),
                                 new SqlParameter("@academic_year", simsobj.academic_year),
                                 new SqlParameter("@enroll", simsobj.enroll),
                                 new SqlParameter("@sims_detention_level_code", simsobj.sims_detention_level_code),
                                 new SqlParameter("@sims_detention_desc_code", simsobj.sims_detention_desc_code),


                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getStudent_Histoy")]
        public HttpResponseMessage getStudent_Histoy(string cur_code, string academic_year, string grade_code, string section_code, string sims_enroll_number)
        {
            List<DSR001> grade_list = new List<DSR001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_cur_code", cur_code),
                              new SqlParameter("@sims_academic_year", academic_year),
                               new SqlParameter("@sims_grade_code", grade_code),
                                new SqlParameter("@sims_section_code", section_code),
                                new SqlParameter("@sims_enroll_number", sims_enroll_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSR001 simsobj = new DSR001();
                            simsobj.sims_detention_level_code = dr["sims_detention_name"].ToString();
                            simsobj.sims_detention_description = dr["sims_detention_description"].ToString();
                            simsobj.sims_detention_point = dr["sims_detention_point"].ToString();

                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_detention_transaction_created_date = dr["sims_detention_transaction_created_date"].ToString();
                            simsobj.sims_grade_code= dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }


        [Route("getUserRoles")]
        public HttpResponseMessage getUserRoles()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getDetention"));

            List<Dtcn01> incidence_list = new List<Dtcn01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'V'),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Dtcn01 Simsobj = new Dtcn01();

                            Simsobj.comn_role_code = dr["comn_role_code"].ToString();
                            Simsobj.comn_role_name = dr["comn_role_name"].ToString();
                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }

        [Route("getUserRoles1")]
        public HttpResponseMessage getUserRoles1(string level)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getDetention"));

            List<Dtcn01> incidence_list = new List<Dtcn01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Detention_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'W'),
                                new SqlParameter("@sims_detention_level_code", level),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Dtcn01 Simsobj = new Dtcn01();

                            Simsobj.comn_role_code = dr["comn_role_code"].ToString();
                            Simsobj.comn_role_name = dr["comn_role_name"].ToString();
                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }
    }
}
