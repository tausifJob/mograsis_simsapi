﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.STUDENT.sibling_details_mode;

namespace SIMSAPI.Controllers.Incidence
{
    [RoutePrefix("api/StudentDetentionTransactionController")]
    public class StudentDetentionTransactionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getStudentList")]
        public HttpResponseMessage getStudentList(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", acad_year),
                             new SqlParameter("@sims_grade_code",grade_code),
                             new SqlParameter("@sims_section_code", section_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.enroll_no = dr["sims_student_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_student_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getLevelCode")]
        public HttpResponseMessage getLevelCode()
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "L")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                         
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getLevelCodeNew")]
        public HttpResponseMessage getLevelCodeNew(string user)
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "LL"),
                             new SqlParameter("@user", user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            simsobj.sims_detention_name = dr["sims_detention_name"].ToString();

                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getStatus")]
        public HttpResponseMessage getStatus()
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getLevelDesc")]
        public HttpResponseMessage getLevelDesc(string sims_detention_level_code)
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "D"),
                             new SqlParameter("@sims_detention_level_code", sims_detention_level_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            simsobj.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                            simsobj.sims_detention_description = dr["sims_detention_description"].ToString();
                            simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("InsertDetention")]
        public HttpResponseMessage InsertDetention(List<DETTRA> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DETTRA hrmsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[StudentDetentionTransaction]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","I"),
                                    new SqlParameter("@sims_detention_transaction_number",0),
                                    new SqlParameter("@sims_cur_code", hrmsobj.sims_cur_code),
                                    new SqlParameter("@sims_academic_year", hrmsobj.sims_academic_year),
                                    new SqlParameter("@sims_grade_code", hrmsobj.sims_grade_code),
                                    new SqlParameter("@sims_section_code", hrmsobj.sims_section_code),
                                    new SqlParameter("@sims_enroll_number", hrmsobj.enroll_no),
                                    new SqlParameter("@sims_detention_level_code", hrmsobj.sims_detention_level_code),
                                    new SqlParameter("@sims_detention_desc_code", hrmsobj.sims_detention_desc_code),
                                    new SqlParameter("@sims_detention_point", hrmsobj.sims_detention_point),
                                    new SqlParameter("@sims_detention_remark", hrmsobj.sims_detention_remark),
                                    new SqlParameter("@sims_detention_transaction_notified_user_code", hrmsobj.sims_detention_transaction_notified_user_code),
                                    new SqlParameter("@sims_detention_transaction_status", hrmsobj.sims_detention_transaction_status),
                                    new SqlParameter("@sims_detention_transaction_created_user_code", hrmsobj.sims_detention_transaction_created_user_code),
                                    new SqlParameter("@sims_detention_transaction_created_date", ""),
                                    new SqlParameter("@sims_detention_transaction_updated_user_code", hrmsobj.sims_detention_transaction_updated_user_code),
                                    new SqlParameter("@sims_detention_transaction_updated_date", ""),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getStudentHistoy")]
        public HttpResponseMessage getStudentHistoy(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string sims_enroll_number)
        {
            List<DETTRA> grade_list = new List<DETTRA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[StudentDetentionTransaction]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_cur_code", sims_cur_code),
                              new SqlParameter("@sims_academic_year", sims_academic_year),
                               new SqlParameter("@sims_grade_code", sims_grade_code),
                                new SqlParameter("@sims_section_code", sims_section_code),
                                new SqlParameter("@sims_enroll_number", sims_enroll_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DETTRA simsobj = new DETTRA();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                            simsobj.sims_detention_description = dr["sims_detention_description"].ToString();
                            simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                            simsobj.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_detention_transaction_created_date = dr["sims_detention_transaction_created_date"].ToString();    
                            simsobj.enroll_no=dr["sims_enroll_number"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("CUDDeletetranaction")]
        public HttpResponseMessage CUDDeletetranaction(List<DETTRA> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (DETTRA hrmsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[StudentDetentionTransaction]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","F"),
                                    new SqlParameter("@sims_cur_code", hrmsobj.sims_cur_code),
                                    new SqlParameter("@sims_academic_year", hrmsobj.sims_academic_year),
                                    new SqlParameter("@sims_grade_code", hrmsobj.sims_grade_code),
                                    new SqlParameter("@sims_section_code", hrmsobj.sims_section_code),
                                    new SqlParameter("@sims_enroll_number", hrmsobj.enroll_no),
                                    new SqlParameter("@sims_detention_level_code", hrmsobj.sims_detention_level_code),
                                    new SqlParameter("@sims_detention_desc_code", hrmsobj.sims_detention_desc_code),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


   }
}