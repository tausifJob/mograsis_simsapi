﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/sibling_api")]
    public class sibling_details : ApiController
    {
        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getshow")]
        public HttpResponseMessage getshow()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sibling_details",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_appl_parameter"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }
    }
}