﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/UpdateFeeCatController")]
    public class UpdateFeeCatController : ApiController
    {
        #region Update Fee Category

        [Route("GetCur")]
        public HttpResponseMessage GetCur()
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())        
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims566 hrmsObj = new Sims566();
                            hrmsObj.cur = dr["sims_cur_code"].ToString();
                            hrmsObj.cur_desc = dr["sims_cur_short_name_en"].ToString();
                     
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAca")]
        public HttpResponseMessage GetAca()
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A")
                //new SqlParameter("@cur", cur)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims566 hrmsObj = new Sims566();
                            hrmsObj.aca = dr["sims_academic_year"].ToString();
                            hrmsObj.aca_desc = dr["sims_academic_year_description"].ToString();
                            hrmsObj.aca_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getgrd")]
        public HttpResponseMessage Getgrd(string cur,string aca)
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "G"),
                new SqlParameter("@cur", cur),
                new SqlParameter("@aca", aca)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims566 hrmsObj = new Sims566();
                            hrmsObj.grade = dr["sims_grade_code"].ToString();
                            hrmsObj.grade_desc = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getsec")]
        public HttpResponseMessage Getsec(string cur,string aca,string grd)
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "P"),
                new SqlParameter("@cur", cur),
                new SqlParameter("@aca", aca),
                new SqlParameter("@grade", grd)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims566 hrmsObj = new Sims566();
                            hrmsObj.sec = dr["sims_section_code"].ToString();
                            hrmsObj.sec_desc = dr["sims_section_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetStudent")]
        public HttpResponseMessage GetStudent(string cur, string aca, string grade, string sec,string enroll)
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'S'),
                   new SqlParameter("@cur", cur),
                   new SqlParameter("@aca", aca),
                   new SqlParameter("@grade", grade),
                   new SqlParameter("@sec", sec),
                   new SqlParameter("@enroll", enroll),
                

              });
                    
                        while (dr.Read())
                        {
                            Sims566 hrmsObj = new Sims566();
                            hrmsObj.enroll = dr["sims_enroll_number"].ToString();
                            hrmsObj.stud_name = dr["stud_name"].ToString();
                            hrmsObj.fee_code = dr["fee_code"].ToString();
                            hrmsObj.fee_cat = dr["fee_cat"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetFeeCategory")]
        public HttpResponseMessage GetFeeCategory()
        {
            List<Sims566> mod_list = new List<Sims566>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'F'),

              });

                    while (dr.Read())
                    {
                        Sims566 hrmsObj = new Sims566();
                        hrmsObj.fee_code = dr["sims_fee_category"].ToString();
                        hrmsObj.fee_cat = dr["sims_fee_category_description"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Update_Stud_fee_category")]
        public HttpResponseMessage Update_Stud_fee_category(List<Sims566> obj)
        {

            bool Updated = false;
            try
            {
                string aca = "";
                string enroll = "";

                foreach (var item in obj)
                {
                    aca = item.aca;
                    enroll = enroll + item.enroll + "!" + item.fee_code + "/";
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_update_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'U'),
                new SqlParameter("@aca", aca),
                new SqlParameter("@enroll",enroll),
                  });
                    if (dr.RecordsAffected > 0)
                    {
                        Updated = true;
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);

        }
       




        #endregion
    }
}