﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/Student")]
    [BasicAuthentication]
    public class DetentionThresholdController : ApiController
    {
        [Route("getDetentionThreshold")]
        public HttpResponseMessage getDetentionThreshold()
        {
            List<Thrsho> list = new List<Thrsho>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_detention_threshold_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Thrsho obj = new Thrsho();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["sims_cur_name"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_detention_threshold1 = dr["sims_detention_threshold1"].ToString();
                            obj.sims_detention_threshold2 = dr["sims_detention_threshold2"].ToString();
                            obj.sims_detention_threshold3 = dr["sims_detention_threshold3"].ToString();
                            obj.sims_detention_threshold_created_date = db.UIDDMMYYYYformat(dr["sims_detention_threshold_created_date"].ToString());                          
                            obj.sims_detention_threshold_created_user_code = dr["sims_detention_threshold_created_user_code"].ToString();
                            if (dr["sims_detention_threshold_status"].ToString() == "A")
                                obj.sims_detention_threshold_status = true;
                            else
                                obj.sims_detention_threshold_status = false;
                           
                            list.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateDeleteDetentionThreshold")]
        public HttpResponseMessage InsertUpdateDeleteDetentionThreshold(List<Thrsho> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var simsobj in data)
                    {
                        if (simsobj.sims_detention_threshold_status == true)
                            status = "A";
                        else
                            status = "I";

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_detention_threshold_proc]",
                        new List<SqlParameter>()
                            {

                                  new SqlParameter("@opr", simsobj.opr),
                                  new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                  new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                  new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                  new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                  new SqlParameter("@sims_detention_threshold1", simsobj.sims_detention_threshold1),
                                  new SqlParameter("@sims_detention_threshold1old", simsobj.sims_detention_threshold1old),
                                  new SqlParameter("@sims_detention_threshold2", simsobj.sims_detention_threshold2),
                                  new SqlParameter("@sims_detention_threshold2old", simsobj.sims_detention_threshold2old),
                                  new SqlParameter("@sims_detention_threshold3", simsobj.sims_detention_threshold3),
                                  new SqlParameter("@sims_detention_threshold3old", simsobj.sims_detention_threshold3old),
                                  //new SqlParameter("@sims_detention_threshold_created_date",simsobj.sims_detention_threshold_created_date),
                                  new SqlParameter("@sims_detention_threshold_created_date",db.DBYYYYMMDDformat(simsobj.sims_detention_threshold_created_date)),
                                  new SqlParameter("@sims_detention_threshold_created_user_code", simsobj.sims_detention_threshold_created_user_code),
                                  new SqlParameter("@sims_detention_threshold_status", status),
                    
                            });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                    }



                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

      
    }
}