﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Text.RegularExpressions;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/AdmissionDashboard_dpsdNew")]

    public class AdmissionDashboard_dpsdNewController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetStudents")]
        public HttpResponseMessage GetStudents(string curr_code, string AcadmicYear, string gradeCode, string admission_status)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                //if (admission_status="null")
                //{
                //    admission_status=null;
                //}
                //if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                //    str = "G";
                //else
                //    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "S1"),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@allgrade_code", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            //new SqlParameter("@sims_admission_employee_code",employee_code),
                            //new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            //new SqlParameter("@sims_admission_nationality_code",nationality_code),
                           // new SqlParameter("@sims_admission_gender",gender_code)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.section_name = dr["section"].ToString();


                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            }
                            catch (Exception rx) { }

                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                simsobj.actual_marks = dr["actual_marks"].ToString();
                                simsobj.max_marks = dr["max_marks"].ToString();
                            }
                            catch (Exception ee)
                            {

                            }

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();
                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {
                                simsobj.sims_admission_parent_id = dr["sims_admission_parent_id"].ToString();
                                if (!string.IsNullOrEmpty(simsobj.sims_admission_parent_id))
                                    simsobj.sims_admission_parent_nm = dr["sims_admission_parent_nm"].ToString() + "-" + dr["sims_admission_parent_id"].ToString();

                            }

                            try
                            {

                            }
                            catch (Exception ex) { }

                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("Get_Grade_CodebyCuriculum")]
        public HttpResponseMessage Get_Grade_CodebyCuriculum(string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Grade_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Grade_Section_Code"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G2"),
                            new SqlParameter("@CURR_CODE", cur_code),
                            new SqlParameter("@ACA_YEAR", acad_yr)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetStudents1")]
        public HttpResponseMessage GetStudents1(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string gender_code, string nationality_code, string employee_code, string sibling_enroll, string adm_no, string adm_name)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (adm_no == "undefined" || adm_no == "null")
                {
                    adm_no = null;
                }
                if (adm_name == "undefined" || adm_name == "null")
                {
                    adm_name = null;
                }
                if (gender_code == "null")
                {
                    gender_code = null;
                }
                if (nationality_code == "null")
                {
                    nationality_code = null;
                }
                if (sibling_enroll == "null")
                {
                    sibling_enroll = null;
                }
                if (employee_code == "null")
                {
                    employee_code = null;
                }
                if (admission_status == "null")
                {
                    admission_status = null;
                }

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                    str = "G";
                else if (!string.IsNullOrEmpty(sibling_enroll))
                    str = "G1";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            new SqlParameter("@sims_admission_employee_code",employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",nationality_code),
                            new SqlParameter("@sims_admission_gender",gender_code),
                            new SqlParameter("@adm_nos",adm_no),
                            new SqlParameter("@First_name",adm_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                                simsobj.sims_allocation_status = dr["sims_allocation_status"].ToString();


                            }
                            catch (Exception rx) { }

                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();
                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }
                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("GetStudents1")]
        public HttpResponseMessage GetStudents1(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string gender_code, string nationality_code, string employee_code, string sibling_enroll, string adm_no, string adm_name, string sen_status)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (sen_status == "undefined" || sen_status == "null")
                {
                    sen_status = null;
                }
                if (adm_no == "undefined" || adm_no == "null")
                {
                    adm_no = null;
                }
                if (adm_name == "undefined" || adm_name == "null")
                {
                    adm_name = null;
                }
                if (gender_code == "null")
                {
                    gender_code = null;
                }
                if (nationality_code == "null")
                {
                    nationality_code = null;
                }
                if (sibling_enroll == "null")
                {
                    sibling_enroll = null;
                }
                if (employee_code == "null")
                {
                    employee_code = null;
                }
                if (admission_status == "null")
                {
                    admission_status = null;
                }

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                    str = "G";
                else if (!string.IsNullOrEmpty(sibling_enroll))
                    str = "G1";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            new SqlParameter("@sims_admission_employee_code",employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",nationality_code),
                            new SqlParameter("@sims_admission_gender",gender_code),
                            new SqlParameter("@adm_nos",adm_no),
                            new SqlParameter("@First_name",adm_name),
                            new SqlParameter("@SEN_status",sen_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();

                            try
                            {
                                simsobj.admission_number_new = int.Parse(Regex.Match(dr["sims_admission_number"].ToString(), @"\d+").Value);
                                //simsobj.sims_ename = dr["sims_ename"].ToString();
                                simsobj.sims_allocation_status = dr["sims_allocation_status"].ToString();

                            }
                            catch (Exception ex) { }
                            try {  simsobj.sims_ename = dr["sims_ename"].ToString();}
                            catch (Exception exx) { }
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["communication_admi_date"].ToString()))
                                    simsobj.communication_admi_date1 = DateTime.Parse(dr["communication_admi_date"].ToString());


                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                                simsobj.sims_follow_up_date = db.UIDDMMYYYYformat(dr["sims_follow_up_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_follow_up_date"].ToString()))
                                    simsobj.sims_follow_up_date1 = DateTime.Parse(dr["sims_follow_up_date"].ToString());

                            }
                            catch (Exception rx) { }



                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_admission_date"].ToString()))
                                simsobj.admission_date1 = DateTime.Parse(dr["sims_admission_date"].ToString());

                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                           
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();
                            try {

                                simsobj.marketing_description = dr["sims_admission_marketing_description"].ToString();
                            }
                            catch (Exception EXE)
                            {

                            }
                            try
                            {

                                simsobj.sims_student_attribute10 = dr["sims_student_attribute10"].ToString();
                            }
                            catch (Exception ea)
                            {

                            }
                            try
                            {

                                simsobj.sims_student_attribute1 = dr["sims_student_attribute1"].ToString();
                            }
                            catch (Exception ea)
                            {

                            }
                            try
                            {
                                simsobj.actual_marks = dr["actual_marks"].ToString();
                                simsobj.max_marks = dr["max_marks"].ToString();
                            }
                            catch (Exception ee)
                            {

                            }

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();
                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }

                            try
                            {
                                simsobj.sims_admission_completed_status = dr["sims_completed_status"].ToString();

                            }
                            catch (Exception ex)
                            {
                            }


                            try
                            {
                                simsobj.sims_admission_transport_address = dr["sims_admission_transport_address"].ToString();

                                simsobj.sims_admission_total_memorization = dr["sims_admission_total_memorization"].ToString();
                                simsobj.sims_admission_exactly_total_memorization = dr["sims_admission_exactly_total_memorization"].ToString();
                                simsobj.sims_admission_attend_any_quran_center_before = dr["sims_admission_attend_any_quran_center_before"].ToString();
                                simsobj.sims_admission_grade = dr["sims_admission_grade"].ToString();
                                simsobj.sims_admission_grade_levels = dr["sims_admission_grade_levels"].ToString();
                                simsobj.sims_admission_levels = dr["sims_admission_levels"].ToString();
                                simsobj.sims_admission_total_memorization_to = dr["sims_admission_total_memorization_to"].ToString();
                                simsobj.sims_admission_exactly_total_memorization_to = dr["sims_admission_exactly_total_memorization_to"].ToString();


                            }
                            catch (Exception ex)
                            {
                            }


                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("waitingPaidStudents")]
        public HttpResponseMessage waitingPaidStudents(param p)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", p.curr_code, p.AcadmicYear, p.gradeCode, p.admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (p.gradeCode == null)
                {
                    p.gradeCode = "undefined";
                }
                if (p.gender_code == "null")
                {
                    p.gender_code = null;
                }
                if (p.nationality_code == "null")
                {
                    p.nationality_code = null;
                }
                if (p.sibling_enroll == "null")
                {
                    p.sibling_enroll = null;
                }
                if (p.employee_code == "null")
                {
                    p.employee_code = null;
                }
                if (p.adm_no == "null" || p.adm_no == "")
                {
                    p.adm_no = null;
                }
                if (p.adm_name == "null" || p.adm_name == "")
                {
                    p.adm_name = null;
                }

                if (string.IsNullOrEmpty(p.curr_code) || string.IsNullOrEmpty(p.AcadmicYear) || string.IsNullOrEmpty(p.gradeCode))
                    str = "GP";
                else
                    str = "SP";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_paidUnpaid_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", p.curr_code),
                            new SqlParameter("@ACA_YEAR", p.AcadmicYear),
                            new SqlParameter("@GRADE_CODE", p.gradeCode),
                            new SqlParameter("@ADMISSION_NUM", p.admission_status),
                            new SqlParameter("@sims_admission_employee_code",p.employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",p.sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",p.nationality_code),
                            new SqlParameter("@sims_admission_gender",p.gender_code),
                            new SqlParameter("@adm_nos",p.adm_no),
                            new SqlParameter("@First_name",p.adm_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();
                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            }
                            catch (Exception ex) { }


                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            // simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                simsobj.actual_marks = dr["actual_marks"].ToString();
                                simsobj.max_marks = dr["max_marks"].ToString();
                            }
                            catch (Exception ee)
                            {

                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }
                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";



                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("waitingUnPaidStudents")]
        public HttpResponseMessage waitingUnPaidStudents(param p)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", p.curr_code, p.AcadmicYear, p.gradeCode, p.admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (p.gradeCode == null)
                {
                    p.gradeCode = "undefined";
                }
                if (p.gender_code == "null")
                {
                    p.gender_code = null;
                }
                if (p.nationality_code == "null")
                {
                    p.nationality_code = null;
                }
                if (p.sibling_enroll == "null")
                {
                    p.sibling_enroll = null;
                }
                if (p.employee_code == "null")
                {
                    p.employee_code = null;
                }
                if (p.adm_no == "null" || p.adm_no == "")
                {
                    p.adm_no = null;
                }
                if (p.adm_name == "null" || p.adm_name == "")
                {
                    p.adm_name = null;
                }

                if (string.IsNullOrEmpty(p.curr_code) || string.IsNullOrEmpty(p.AcadmicYear) || string.IsNullOrEmpty(p.gradeCode))
                    str = "GU";
                else
                    str = "SU";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_paidUnpaid_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", p.curr_code),
                            new SqlParameter("@ACA_YEAR", p.AcadmicYear),
                            new SqlParameter("@GRADE_CODE", p.gradeCode),
                            new SqlParameter("@ADMISSION_NUM", p.admission_status),
                            new SqlParameter("@sims_admission_employee_code",p.employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",p.sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",p.nationality_code),
                            new SqlParameter("@sims_admission_gender",p.gender_code),
                            new SqlParameter("@adm_nos",p.adm_no),
                            new SqlParameter("@First_name",p.adm_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();

                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                            }
                            catch (Exception ex) { }

                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            // simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                simsobj.actual_marks = dr["actual_marks"].ToString();
                                simsobj.max_marks = dr["max_marks"].ToString();
                            }
                            catch (Exception ee)
                            {

                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }

                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("GetDashTilesDetails")]
        public HttpResponseMessage GetDashTilesDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetDashTilesDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.appl_count = dr["appl_count"].ToString();
                            simsobj.Completed_cnt = dr["Completed_cnt"].ToString();
                            simsobj.rejected_cnt = dr["rejected_cnt"].ToString();
                            simsobj.waiting_cnt = dr["waiting_cnt"].ToString();
                            simsobj.selected_cnt = dr["selected_cnt"].ToString();
                            simsobj.waiting_paid = dr["waiting_paid"].ToString();
                            simsobj.waiting_unpaid = dr["waiting_unpaid"].ToString();
                            try
                            {
                                simsobj.sen_cnt = dr["sen_cnt"].ToString();
                                simsobj.direct_cnt = dr["direct_cnt"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetDashTilesViewDetails")]
        public HttpResponseMessage GetDashTilesViewDetails(string curr_code, string AcadmicYear, string gradeCode, string admission_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetDashTilesViewDetails", curr_code, AcadmicYear, gradeCode, admission_status));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["name"].ToString();
                            simsobj.sims_admission_status = dr["sims_admission_status"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetTabStudentData")]
        public HttpResponseMessage GetTabStudentData(string admission_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTabStudentData()PARAMETERS ::admission_number{2}";
            Log.Debug(string.Format(debug, "ERP/Admission/", "GetTabStudentData", admission_number));

            Sims010_Edit simsobj = new Sims010_Edit();
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@CURR_CODE", ""),
                            new SqlParameter("@ACA_YEAR", ""),
                            new SqlParameter("@GRADE_CODE", ""),
                            new SqlParameter("@ADMISSION_NUM", admission_number)
                          
                         });
                        while (dr.Read())
                        {
                            #region
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.sims_admission_father_national_id = dr["sims_admission_father_national_id"].ToString();
                            simsobj.sims_admission_mother_national_id = dr["sims_admission_mother_national_id"].ToString();
                            simsobj.sims_admission_guardian_national_id = dr["sims_admission_guardian_national_id"].ToString();
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.appl_num = dr["sims_admission_application_number"].ToString();
                            simsobj.behaviour_desc = dr["sims_admission_behaviour_desc"].ToString();
                            if (dr["sims_admission_behaviour_status"].ToString() == "1")
                            {
                                simsobj.behaviour_status = true;
                            }
                            else
                            {
                                simsobj.behaviour_status = false;
                            }
                            simsobj.birth_country_code = dr["sims_admission_birth_country_code"].ToString();
                            simsobj.blood_group_code = dr["sims_admission_blood_group_code"].ToString();
                            try
                            {
                                simsobj.comm_date = db.UIDDMMYYYYformat(dr["sims_admission_commencement_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.comm_date = "";
                            }
                            simsobj.current_school_address = dr["sims_admission_current_school_address"].ToString();
                            simsobj.current_school_city = dr["sims_admission_current_school_city"].ToString();
                            simsobj.current_school_cur = dr["sims_admission_current_school_cur"].ToString();
                            simsobj.current_school_enroll_number = dr["sims_admission_current_school_enroll_number"].ToString();
                            simsobj.current_school_fax = dr["sims_admission_current_school_fax"].ToString();
                            try
                            {
                                simsobj.current_school_from_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_from_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.current_school_from_date = "";
                            }
                            simsobj.current_school_grade = dr["sims_admission_current_school_grade"].ToString();

                            simsobj.current_school_head_teacher = dr["sims_admission_current_school_head_teacher"].ToString();
                            simsobj.current_school_language = dr["sims_admission_current_school_language"].ToString();
                            simsobj.current_school_name = dr["sims_admission_current_school_name"].ToString();
                            simsobj.current_school_phone = dr["sims_admission_current_school_phone"].ToString();
                            if (dr["sims_admission_current_school_status"].ToString() == "1")
                            {
                                simsobj.current_school_status = true;
                            }
                            else
                            {
                                simsobj.current_school_status = false;
                            }

                            simsobj.current_school_to_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_to_date"].ToString());
                            simsobj.current_school_country = dr["current_school_country"].ToString();
                            simsobj.current_school_country_code = dr["sims_admission_current_school_country_code"].ToString();
                            if (dr["sims_admission_declaration_status"].ToString() == "1")
                            {
                                simsobj.declaration_status = true;
                            }
                            else
                            {
                                simsobj.declaration_status = false;
                            }
                            simsobj.disability_desc = dr["sims_admission_disability_desc"].ToString();
                            if (dr["sims_admission_disability_status"].ToString() == "1")
                            {
                                simsobj.disability_status = true;
                            }
                            else
                            {
                                simsobj.disability_status = false;
                            }
                            simsobj.dns = dr["sims_admission_dns"].ToString();
                            simsobj.employee_comp_code = dr["sims_admission_employee_comp_code"].ToString();
                            simsobj.employee_school_code = dr["sims_admission_employee_school_code"].ToString();
                            if (dr["sims_admission_employee_type"].ToString() == "1")
                            {
                                simsobj.employee_type = true;
                            }
                            else
                            {
                                simsobj.employee_type = false;
                            }
                            simsobj.family_name_ot = dr["sims_admission_family_name_ot"].ToString();
                            simsobj.ethinicity_code = dr["sims_admission_ethnicity_code"].ToString();
                            simsobj.family_name = dr["sims_admission_family_name_en"].ToString();
                            simsobj.father_appartment_number = dr["sims_admission_father_appartment_number"].ToString();
                            simsobj.father_area_number = dr["sims_admission_father_area_number"].ToString();
                            simsobj.father_building_number = dr["sims_admission_father_building_number"].ToString();
                            simsobj.father_city = dr["sims_admission_father_city"].ToString();
                            simsobj.father_company = dr["sims_admission_father_company"].ToString();
                            simsobj.father_country_code = dr["sims_admission_father_country_code"].ToString();
                            simsobj.father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.father_family_name = dr["sims_admission_father_family_name"].ToString();
                            simsobj.father_fax = dr["sims_admission_father_fax"].ToString();
                            simsobj.father_first_name = dr["sims_admission_father_first_name"].ToString();
                            simsobj.father_last_name = dr["sims_admission_father_last_name"].ToString();
                            simsobj.father_middle_name = dr["sims_admission_father_middle_name"].ToString();
                            simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                            simsobj.father_name_ot = dr["sims_admission_father_name_ot"].ToString();
                            simsobj.father_nationality1_code = dr["sims_admission_father_nationality1_code"].ToString();
                            simsobj.father_nationality2_code = dr["sims_admission_father_nationality2_code"].ToString();
                            simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();
                            simsobj.father_passport_number = dr["sims_admission_father_passport_number"].ToString();
                            simsobj.father_phone = dr["sims_admission_father_phone"].ToString();
                            simsobj.father_po_box = dr["sims_admission_father_po_box"].ToString();
                            simsobj.father_salutation_code = dr["sims_admission_father_salutation_code"].ToString();
                            simsobj.father_state = dr["sims_admission_father_state"].ToString();
                            simsobj.father_street_number = dr["sims_admission_father_street_number"].ToString();
                            simsobj.father_summary_address = dr["sims_admission_father_summary_address"].ToString();
                            simsobj.fee_payment_contact_pref_code = dr["sims_admission_fee_payment_contact_pref"].ToString();
                            if (dr["sims_admission_fees_paid_status"].ToString() == "1")
                            {
                                simsobj.fees_paid_status = true;
                            }
                            else
                            {
                                simsobj.fees_paid_status = false;
                            }
                            simsobj.first_name = dr["sims_admission_passport_first_name_en"].ToString();
                            simsobj.first_name_ot = dr["sims_admission_passport_first_name_ot"].ToString();
                            simsobj.gifted_desc = dr["sims_admission_gifted_desc"].ToString();
                            if (dr["sims_admission_gifted_status"].ToString() == "1")
                            {
                                simsobj.gifted_status = true;
                            }
                            else
                            {
                                simsobj.gifted_status = false;
                            }
                            simsobj.guardian_appartment_number = dr["sims_admission_guardian_appartment_number"].ToString();
                            simsobj.guardian_area_number = dr["sims_admission_guardian_area_number"].ToString();
                            simsobj.guardian_building_number = dr["sims_admission_guardian_building_number"].ToString();
                            simsobj.guardian_city = dr["sims_admission_guardian_city"].ToString();
                            simsobj.guardian_company = dr["sims_admission_guardian_company"].ToString();
                            simsobj.guardian_country_code = dr["sims_admission_guardian_country_code"].ToString();
                            simsobj.guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.guardian_family_name = dr["sims_admission_guardian_family_name"].ToString();
                            simsobj.guardian_fax = dr["sims_admission_guardian_fax"].ToString();
                            simsobj.guardian_first_name = dr["sims_admission_guardian_first_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_admission_guardian_last_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_admission_guardian_middle_name"].ToString();
                            simsobj.guardian_mobile = dr["sims_admission_guardian_mobile"].ToString();
                            simsobj.guardian_name_ot = dr["sims_admission_guardian_name_ot"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_admission_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality2_code = dr["sims_admission_guardian_nationality2_code"].ToString();
                            simsobj.guardian_occupation = dr["sims_admission_guardian_occupation"].ToString();
                            simsobj.guardian_passport_number = dr["sims_admission_guardian_passport_number"].ToString();
                            simsobj.guardian_phone = dr["sims_admission_guardian_phone"].ToString();
                            simsobj.guardian_po_box = dr["sims_admission_guardian_po_box"].ToString();
                            simsobj.guardian_salutation_code = dr["sims_admission_guardian_salutation_code"].ToString();
                            simsobj.guardian_state = dr["sims_admission_guardian_state"].ToString();
                            simsobj.guardian_street_number = dr["sims_admission_guardian_street_number"].ToString();
                            simsobj.guardian_summary_address = dr["sims_admission_guardian_summary_address"].ToString();
                            try
                            {
                                simsobj.health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_expiry_date = "";
                            }
                            try
                            {
                                simsobj.health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_issue_date = "";
                            }
                            simsobj.health_card_issuing_authority = dr["sims_admission_health_card_issuing_authority"].ToString();
                            simsobj.health_card_number = dr["sims_admission_health_card_number"].ToString();
                            simsobj.health_hearing_desc = dr["sims_admission_health_hearing_desc"].ToString();
                            if (dr["sims_admission_health_hearing_status"].ToString() == "1")
                            {
                                simsobj.health_hearing_status = true;
                            }
                            else
                            {
                                simsobj.health_hearing_status = false;
                            }
                            simsobj.health_other_desc = dr["sims_admission_health_other_desc"].ToString();
                            if (dr["sims_admission_health_other_status"].ToString() == "1")
                            {
                                simsobj.health_other_status = true;
                            }
                            else
                            {
                                simsobj.health_other_status = false;
                            }
                            simsobj.health_restriction_desc = dr["sims_admission_health_restriction_desc"].ToString();
                            if (dr["sims_admission_health_restriction_status"].ToString() == "1")
                            {
                                simsobj.health_restriction_status = true;
                            }
                            else
                            {
                                simsobj.health_restriction_status = false;
                            }
                            simsobj.health_vision_desc = dr["sims_admission_health_vision_desc"].ToString();
                            if (dr["sims_admission_health_vision_status"].ToString() == "1")
                            {
                                simsobj.health_vision_status = true;
                            }
                            else
                            {
                                simsobj.health_vision_status = false;
                            }
                            simsobj.last_name = dr["sims_admission_passport_last_name_en"].ToString();
                            simsobj.last_name_ot = dr["sims_admission_passport_last_name_ot"].ToString();
                            simsobj.language_support_desc = dr["sims_admission_language_support_desc"].ToString();
                            if (dr["sims_admission_language_support_status"].ToString() == "1")
                            {
                                simsobj.language_support_status = true;
                            }
                            else
                            {
                                simsobj.language_support_status = false;
                            }
                            simsobj.legal_custody_code = dr["sims_admission_legal_custody"].ToString();
                            simsobj.main_language_code = dr["sims_admission_main_language_code"].ToString();
                            simsobj.main_language_r_code = dr["sims_admission_main_language_r"].ToString();
                            simsobj.main_language_s_code = dr["sims_admission_main_language_s"].ToString();
                            simsobj.main_language_w_code = dr["sims_admission_main_language_w"].ToString();
                            simsobj.other_language_code = dr["sims_admission_other_language"].ToString();
                            if (dr["sims_admission_marketing_code"].ToString() == "1")
                            {
                                simsobj.marketing_code = true;
                            }
                            else
                            {
                                simsobj.marketing_code = false;
                            }
                            simsobj.marketing_description = dr["sims_admission_marketing_description"].ToString();
                            simsobj.medication_desc = dr["sims_admission_medication_desc"].ToString();
                            if (dr["sims_admission_medication_status"].ToString() == "1")
                            {
                                simsobj.medication_status = true;
                            }
                            else
                            {
                                simsobj.medication_status = false;
                            }
                            simsobj.middle_name = dr["sims_admission_passport_middle_name_en"].ToString();
                            simsobj.midd_name_ot = dr["sims_admission_passport_middle_name_ot"].ToString();
                            simsobj.motherTounge_language_code = dr["sims_admission_main_language_m"].ToString();
                            simsobj.mother_appartment_number = dr["sims_admission_mother_appartment_number"].ToString();
                            simsobj.mother_area_number = dr["sims_admission_mother_area_number"].ToString();
                            simsobj.mother_building_number = dr["sims_admission_mother_building_number"].ToString();
                            simsobj.mother_city = dr["sims_admission_mother_city"].ToString();
                            simsobj.mother_company = dr["sims_admission_mother_company"].ToString();
                            simsobj.mother_country_code = dr["sims_admission_mother_country_code"].ToString();
                            simsobj.mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.mother_family_name = dr["sims_admission_mother_family_name"].ToString();
                            simsobj.mother_fax = dr["sims_admission_mother_fax"].ToString();
                            simsobj.mother_first_name = dr["sims_admission_mother_first_name"].ToString();
                            simsobj.mother_last_name = dr["sims_admission_mother_last_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_admission_mother_middle_name"].ToString();
                            simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            simsobj.mother_name_ot = dr["sims_admission_mother_name_ot"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_admission_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality2_code = dr["sims_admission_mother_nationality2_code"].ToString();
                            simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();
                            simsobj.mother_passport_number = dr["sims_admission_mother_passport_number"].ToString();
                            simsobj.mother_phone = dr["sims_admission_mother_phone"].ToString();
                            simsobj.mother_po_box = dr["sims_admission_mother_po_box"].ToString();
                            simsobj.mother_salutation_code = dr["sims_admission_mother_salutation_code"].ToString();
                            simsobj.mother_state = dr["sims_admission_mother_state"].ToString();
                            simsobj.mother_street_number = dr["sims_admission_mother_street_number"].ToString();
                            simsobj.mother_summary_address = dr["sims_admission_mother_summary_address"].ToString();
                            simsobj.music_desc = dr["sims_admission_music_desc"].ToString();
                            if (dr["sims_admission_music_status"].ToString() == "1")
                            {
                                simsobj.music_status = true;
                            }
                            else
                            {
                                simsobj.music_status = false;
                            }
                            simsobj.national_id = dr["sims_admission_national_id"].ToString();
                            try
                            {
                                simsobj.national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_issue_date = "";
                            }

                            try
                            {
                                simsobj.national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_expiry_date = "";
                            }
                            simsobj.nicke_name = dr["sims_admission_nickname"].ToString();
                            simsobj.parent_id = dr["sims_admission_parent_id"].ToString();
                            if (dr["sims_admission_parent_status_code"].ToString() == "1")
                            {
                                simsobj.parent_status_code = true;
                            }
                            else
                            {
                                simsobj.parent_status_code = false;
                            }
                            try
                            {
                                simsobj.passport_expiry = db.UIDDMMYYYYformat(dr["sims_admission_passport_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_expiry = "";
                            }
                            simsobj.passport_issue_auth = dr["sims_admission_passport_issuing_authority"].ToString();
                            try
                            {
                                simsobj.passport_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_issue_date = "";
                            }
                            simsobj.passport_issue_place = dr["sims_admission_passport_issue_place"].ToString();
                            simsobj.passport_num = dr["sims_admission_passport_number"].ToString();
                            simsobj.primary_contact_pref_code = dr["sims_admission_primary_contact_pref"].ToString();
                            if (dr["sims_admission_primary_contact_code"].ToString() == "1")
                            {
                                simsobj.primary_contact_code = true;
                            }
                            else
                            {
                                simsobj.primary_contact_code = false;
                            }
                            simsobj.primary_contact_pref_desc = dr["primary_contact_pref_desc"].ToString();
                            simsobj.pros_appl_num = dr["sims_admission_pros_application_number"].ToString();
                            simsobj.religion_code = dr["sims_admission_religion_code"].ToString();
                            simsobj.sibling_dob = dr["sims_admission_sibling_dob"].ToString();
                            simsobj.sibling_name = dr["sims_admission_sibling_name"].ToString();
                            simsobj.sibling_school_code = dr["sims_admission_sibling_school_code"].ToString();
                            simsobj.sibling_school_name = dr["sibSchoolName"].ToString();
                            simsobj.sports_desc = dr["sims_admission_sports_desc"].ToString();
                            if (dr["sims_admission_sports_status"].ToString() == "1")
                            {
                                simsobj.sports_status = true;
                            }
                            else
                            {
                                simsobj.sports_status = false;
                            }
                            simsobj.status = dr["sims_admission_status"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.user_code = dr["sims_admission_user_code"].ToString();
                            try
                            {
                                simsobj.visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_expiry_date = "";
                            }

                            try
                            {
                                simsobj.visa_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_issue_date = "";
                            }
                            simsobj.visa_issuing_authority = dr["sims_admission_visa_issuing_authority"].ToString();
                            simsobj.visa_issuing_place = dr["sims_admission_visa_issuing_place"].ToString();
                            simsobj.visa_number = dr["sims_admission_visa_number"].ToString();
                            simsobj.visa_type_desc = dr["sims_admission_visa_type_desc"].ToString();
                            simsobj.visa_type = dr["sims_admission_visa_type"].ToString();
                            if (dr["sims_admission_transport_status"].ToString() == "1")
                            {
                                simsobj.transport_status = true;
                            }
                            else
                            {
                                simsobj.transport_status = false;
                            }
                            simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();
                            simsobj.ip = dr["sims_admission_ip"].ToString();
                            #endregion
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_student_attribute1 = dr["sims_student_attribute1"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_student_attribute4"].ToString();

                            simsobj.sims_student_health_respiratory_status = dr["sims_student_health_respiratory_status"].ToString();
                            simsobj.sims_student_health_respiratory_desc = dr["sims_student_health_respiratory_desc"].ToString();
                            simsobj.sims_student_health_hay_fever_status = dr["sims_student_health_hay_fever_status"].ToString();
                            simsobj.sims_student_health_hay_fever_desc = dr["sims_student_health_hay_fever_desc"].ToString();
                            simsobj.sims_student_health_epilepsy_status = dr["sims_student_health_epilepsy_status"].ToString();
                            simsobj.sims_student_health_epilepsy_desc = dr["sims_student_health_epilepsy_desc"].ToString();
                            simsobj.sims_student_health_skin_status = dr["sims_student_health_skin_status"].ToString();
                            simsobj.sims_student_health_skin_desc = dr["sims_student_health_skin_desc"].ToString();
                            simsobj.sims_student_health_diabetes_status = dr["sims_student_health_diabetes_status"].ToString();
                            simsobj.sims_student_health_diabetes_desc = dr["sims_student_health_diabetes_desc"].ToString();
                            simsobj.sims_student_health_surgery_status = dr["sims_student_health_surgery_status"].ToString();
                            simsobj.sims_student_health_surgery_desc = dr["sims_student_health_surgery_desc"].ToString();


                            simsobj.admission_number = admission_number;
                            simsobj.stud_full_name = simsobj.first_name + " " + simsobj.middle_name + " " + simsobj.last_name;// o.stud_full_name;
                            simsobj.fee_category_code = dr["sims_admission_fee_category"].ToString();
                            try
                            {
                                simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());//o.admission_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.admission_date = "";
                            }
                            simsobj.pros_num = dr["pros_no"].ToString();//o.pros_no;
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.term_code = dr["sims_admission_term_code"].ToString();
                            try
                            {
                                simsobj.tent_join_date = db.UIDDMMYYYYformat(dr["tent_join_date"].ToString());//o.tent_join_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.tent_join_date = "";
                            }
                            simsobj.gender_desc = dr["gender"].ToString();// o.gender;
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            try
                            {
                                simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());//o.birth_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.birth_date = "";
                            }
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.academic_year_desc = dr["academic_year_desc"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.nationality_code = dr["sims_admission_nationality_code"].ToString();//o.nation;
                            if (dr["sibling_status"].ToString() == "1")
                            {
                                simsobj.sibling_status = true;
                            }
                            else
                            {
                                simsobj.sibling_status = false;
                            }
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            // simsobj.sibling_enroll = (simsobj.sibling_status.Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.employee_code = dr["emp_code"].ToString();
                            // simsobj.section_code = "0018";

                            try
                            {

                                if (dr["sims_student_attribute9"].ToString() == "1")
                                {
                                    simsobj.sims_student_attribute9 = true;
                                }
                                else
                                {
                                    simsobj.sims_student_attribute9 = false;
                                }

                                if (dr["sims_admission_learning_therapy_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_learning_therapy_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_learning_therapy_status = false;
                                }

                                simsobj.sims_admission_learning_therapy_desc = dr["sims_admission_learning_therapy_desc"].ToString();

                                if (dr["sims_admission_special_education_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_special_education_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_special_education_status = false;
                                }

                                simsobj.sims_admission_special_education_desc = dr["sims_admission_special_education_desc"].ToString();

                                if (dr["sims_admission_falled_grade_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_falled_grade_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_falled_grade_status = false;
                                }

                                simsobj.sims_admission_falled_grade_desc = dr["sims_admission_falled_grade_desc"].ToString();

                                if (dr["sims_admission_communication_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_communication_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_communication_status = false;
                                }
                                simsobj.sims_admission_communication_desc = dr["sims_admission_communication_desc"].ToString();
                                if (dr["sims_admission_specialAct_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_specialAct_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_specialAct_status = false;
                                }
                                simsobj.sims_admission_specialAct_desc = dr["sims_admission_specialAct_desc"].ToString();
                                if (dr["sims_student_attribute9"].ToString() == "1")
                                {
                                    simsobj.sims_student_attribute9 = true;
                                }
                                else
                                {
                                    simsobj.sims_student_attribute9 = false;
                                }
                                simsobj.sims_student_attribute5 = dr["sims_student_attribute5"].ToString();
                                simsobj.sims_student_attribute6 = dr["sims_student_attribute6"].ToString();
                                simsobj.sims_student_attribute7 = dr["sims_student_attribute7"].ToString();
                                simsobj.sims_student_attribute8 = dr["sims_student_attribute8"].ToString();

                                simsobj.sims_student_attribute10 = dr["sims_student_attribute10"].ToString();
                                simsobj.sims_student_attribute11 = dr["sims_student_attribute11"].ToString();
                                simsobj.sims_student_attribute12 = dr["sims_student_attribute12"].ToString();
                            }
                            catch (Exception et)
                            {
                            }


                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_fee_month_code"].ToString()))
                                    simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();

                                if (!string.IsNullOrEmpty(dr["feemonth_name"].ToString()))
                                    simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();

                                if (!string.IsNullOrEmpty(dr["comn_seq"].ToString()))
                                    simsobj.comn_seq = dr["comn_seq"].ToString();



                                simsobj.sims_admission_second_lang_code = dr["sims_admission_second_lang_code"].ToString();
                                simsobj.sims_admission_second_lang_name = dr["sims_admission_second_lang_name"].ToString();

                                simsobj.sims_admission_third_lang_code = dr["sims_admission_third_lang_code"].ToString();
                                simsobj.sims_admission_third_lang_name = dr["sims_admission_third__lang_name"].ToString();

                                simsobj.home_appartment_number = dr["sims_admission_home_appartment_number"].ToString();
                                simsobj.home_building_number = dr["sims_admission_home_building_number"].ToString();
                                simsobj.home_street_number = dr["sims_admission_home_street_number"].ToString();
                                simsobj.home_area_number = dr["sims_admission_home_area_number"].ToString();
                                simsobj.home_city = dr["sims_admission_home_city"].ToString();
                                simsobj.home_state = dr["sims_admission_home_state"].ToString();
                                simsobj.home_country_code = dr["sims_admission_home_country_code"].ToString();
                                simsobj.home_summary_address = dr["sims_admission_home_summary_address"].ToString();
                                simsobj.home_po_box = dr["sims_admission_home_po_box"].ToString();
                                simsobj.home_landmark = dr["sims_admission_home_landmark"].ToString();
                                simsobj.home_phone = dr["sims_admission_home_phone"].ToString();

                            }
                            catch (Exception ee)
                            {
                            }


                            try
                            {
                                if (dr["sims_admission_sen_approve_status"].ToString() == "1")
                                    simsobj.sen_disable = true;
                                else
                                    simsobj.sen_disable = false;
                            }
                            catch (Exception ex) { }

                            try
                            {
                                simsobj.sims_admission_passport_full_name_en = dr["sims_admission_passport_full_name_en"].ToString();

                            }
                            catch (Exception ex) { }

                        }



                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("GetAll_SimsProspects")]
        public HttpResponseMessage GetAll_SimsProspects(string pros_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: pros_no{2}";
            Log.Debug(string.Format(debug, "Admission", "GetAll_SimsProspects", pros_no));
            string cnt = "0", str = "";

            Sims034 pros_obj = new Sims034();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Sims_Prospect",
                        new List<SqlParameter>()
                        {
                           new SqlParameter("@opr", "S"),
                           new SqlParameter("@sims_pros_number", pros_no)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            pros_obj.sims_pros_number = dr["sims_pros_number"].ToString();
                            pros_obj.sims_pros_application_number = dr["sims_pros_application_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_date_created"].ToString()))
                                pros_obj.sims_pros_date_created = db.UIDDMMYYYYformat(dr["sims_pros_date_created"].ToString());
                            pros_obj.sims_pros_school_code = dr["sims_pros_school_code"].ToString();
                            pros_obj.sims_pros_cur_code = dr["sims_pros_cur_code"].ToString();
                            pros_obj.sims_pros_academic_year = dr["sims_pros_academic_year"].ToString();
                            pros_obj.sims_pros_grade_code = dr["sims_pros_grade_code"].ToString();
                            pros_obj.sims_pros_term_code = dr["sims_pros_term_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_tentative_joining_date"].ToString()))
                                //pros_obj.sims_pros_tentative_joining_date = DateTime.Parse(dr["sims_pros_tentative_joining_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_tentative_joining_date = db.UIDDMMYYYYformat(dr["sims_pros_tentative_joining_date"].ToString());
                            pros_obj.sims_pros_passport_number = dr["sims_pros_passport_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_passport_issue_date"].ToString()))
                                //pros_obj.sims_pros_passport_issue_date = DateTime.Parse(dr["sims_pros_passport_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_passport_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_passport_expiry_date"].ToString()))
                                //pros_obj.sims_pros_passport_expiry_date = DateTime.Parse(dr["sims_pros_passport_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_passport_expiry_date"].ToString());
                            pros_obj.sims_pros_passport_issuing_authority = dr["sims_pros_passport_issuing_authority"].ToString();
                            pros_obj.sims_pros_passport_issue_place = dr["sims_pros_passport_issue_place"].ToString();
                            pros_obj.sims_pros_passport_first_name_en = dr["sims_pros_passport_first_name_en"].ToString();
                            pros_obj.sims_pros_passport_middle_name_en = dr["sims_pros_passport_middle_name_en"].ToString();
                            pros_obj.sims_pros_passport_last_name_en = dr["sims_pros_passport_last_name_en"].ToString();
                            pros_obj.sims_pros_family_name_en = dr["sims_pros_family_name_en"].ToString();
                            pros_obj.sims_pros_nickname = dr["sims_pros_nickname"].ToString();
                            pros_obj.sims_pros_passport_first_name_ot = dr["sims_pros_passport_first_name_ot"].ToString();
                            pros_obj.sims_pros_passport_middle_name_ot = dr["sims_pros_passport_middle_name_ot"].ToString();
                            pros_obj.sims_pros_passport_last_name_ot = dr["sims_pros_passport_last_name_ot"].ToString();
                            pros_obj.sims_pros_family_name_ot = dr["sims_pros_family_name_ot"].ToString();
                            pros_obj.sims_pros_gender = dr["sims_pros_gender"].ToString();
                            pros_obj.sims_pros_religion_code = dr["sims_pros_religion_code"].ToString();
                            pros_obj.sims_pros_dob = dr["sims_pros_dob"].ToString();
                            pros_obj.sims_pros_birth_country_code = dr["sims_pros_birth_country_code"].ToString();
                            pros_obj.sims_pros_nationality_code = dr["sims_pros_nationality_code"].ToString();
                            pros_obj.sims_pros_ethnicity_code = dr["sims_pros_ethnicity_code"].ToString();
                            pros_obj.sims_pros_visa_number = dr["sims_pros_visa_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_visa_issue_date"].ToString()))
                                //pros_obj.sims_pros_visa_issue_date = DateTime.Parse(dr["sims_pros_visa_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_visa_expiry_date"].ToString()))
                                //pros_obj.sims_pros_visa_expiry_date = DateTime.Parse(dr["sims_pros_visa_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_expiry_date"].ToString());
                            pros_obj.sims_pros_visa_issuing_place = dr["sims_pros_visa_issuing_place"].ToString();
                            pros_obj.sims_pros_visa_issuing_authority = dr["sims_pros_visa_issuing_authority"].ToString();
                            pros_obj.sims_pros_visa_type = dr["sims_pros_visa_type"].ToString();
                            pros_obj.sims_pros_national_id = dr["sims_pros_national_id"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_national_id_issue_date"].ToString()))
                                //pros_obj.sims_pros_national_id_issue_date = DateTime.Parse(dr["sims_pros_national_id_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_national_id_expiry_date"].ToString()))
                                //pros_obj.sims_pros_national_id_expiry_date = DateTime.Parse(dr["sims_pros_national_id_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_expiry_date"].ToString());
                            pros_obj.sims_pros_main_language_code = dr["sims_pros_main_language_code"].ToString();
                            pros_obj.sims_pros_main_language_r = dr["sims_pros_main_language_r"].ToString();
                            pros_obj.sims_pros_main_language_w = dr["sims_pros_main_language_w"].ToString();
                            pros_obj.sims_pros_main_language_s = dr["sims_pros_main_language_s"].ToString();
                            pros_obj.sims_pros_main_language_m = dr["sims_pros_main_language_m"].ToString();
                            pros_obj.sims_pros_other_language = dr["sims_pros_other_language"].ToString();
                            pros_obj.sims_pros_sibling_status = dr["sims_pros_sibling_status"].ToString();
                            pros_obj.sims_pros_sibling_name = dr["sims_pros_sibling_name"].ToString();
                            pros_obj.sims_pros_sibling_enroll_number = dr["sims_pros_sibling_enroll_number"].ToString();
                            pros_obj.sims_pros_parent_id = dr["sims_pros_parent_id"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_sibling_dob"].ToString()))
                                //pros_obj.sims_pros_sibling_dob = DateTime.Parse(dr["sims_pros_sibling_dob"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_sibling_dob = db.UIDDMMYYYYformat(dr["sims_pros_sibling_dob"].ToString());
                            pros_obj.sims_pros_sibling_school_code = dr["sims_pros_sibling_school_code"].ToString();
                            pros_obj.sims_pros_primary_contact_code = dr["sims_pros_primary_contact_code"].ToString();
                            pros_obj.sims_pros_primary_contact_pref = dr["sims_pros_primary_contact_pref"].ToString();
                            pros_obj.sims_pros_fee_payment_contact_pref = dr["sims_pros_fee_payment_contact_pref"].ToString();
                            pros_obj.sims_pros_transport_status = dr["sims_pros_transport_status"].ToString();
                            pros_obj.sims_pros_transport_desc = dr["sims_pros_transport_desc"].ToString();
                            pros_obj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].ToString();
                            pros_obj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                            pros_obj.sims_pros_father_salutation_code = dr["sims_pros_father_salutation_code"].ToString();
                            pros_obj.sims_pros_father_first_name = dr["sims_pros_father_first_name"].ToString();
                            pros_obj.sims_pros_father_middle_name = dr["sims_pros_father_middle_name"].ToString();
                            pros_obj.sims_pros_father_last_name = dr["sims_pros_father_last_name"].ToString();
                            pros_obj.sims_pros_father_family_name = dr["sims_pros_father_family_name"].ToString();
                            pros_obj.sims_pros_father_name_ot = dr["sims_pros_father_name_ot"].ToString();
                            pros_obj.sims_pros_father_nationality1_code = dr["sims_pros_father_nationality1_code"].ToString();
                            pros_obj.sims_pros_father_nationality2_code = dr["sims_pros_father_nationality2_code"].ToString();
                            pros_obj.sims_pros_father_appartment_number = dr["sims_pros_father_appartment_number"].ToString();
                            pros_obj.sims_pros_father_building_number = dr["sims_pros_father_building_number"].ToString();
                            pros_obj.sims_pros_father_street_number = dr["sims_pros_father_street_number"].ToString();
                            pros_obj.sims_pros_father_area_number = dr["sims_pros_father_area_number"].ToString();
                            pros_obj.sims_pros_father_summary_address = dr["sims_pros_father_summary_address"].ToString();
                            pros_obj.sims_pros_father_city = dr["sims_pros_father_city"].ToString();
                            pros_obj.sims_pros_father_state = dr["sims_pros_father_state"].ToString();
                            pros_obj.sims_pros_father_country_code = dr["sims_pros_father_country_code"].ToString();
                            pros_obj.sims_pros_father_phone = dr["sims_pros_father_phone"].ToString();
                            pros_obj.sims_pros_father_mobile = dr["sims_pros_father_mobile"].ToString();
                            pros_obj.sims_pros_father_email = dr["sims_pros_father_email"].ToString();
                            pros_obj.sims_pros_father_fax = dr["sims_pros_father_fax"].ToString();
                            pros_obj.sims_pros_father_po_box = dr["sims_pros_father_po_box"].ToString();
                            pros_obj.sims_pros_father_occupation = dr["sims_pros_father_occupation"].ToString();
                            pros_obj.sims_pros_father_company = dr["sims_pros_father_company"].ToString();
                            pros_obj.sims_pros_father_passport_number = dr["sims_pros_father_passport_number"].ToString();
                            pros_obj.sims_pros_mother_salutation_code = dr["sims_pros_mother_salutation_code"].ToString();
                            pros_obj.sims_pros_mother_first_name = dr["sims_pros_mother_first_name"].ToString();
                            pros_obj.sims_pros_mother_middle_name = dr["sims_pros_mother_middle_name"].ToString();
                            pros_obj.sims_pros_mother_last_name = dr["sims_pros_mother_last_name"].ToString();
                            pros_obj.sims_pros_mother_family_name = dr["sims_pros_mother_family_name"].ToString();
                            pros_obj.sims_pros_mother_name_ot = dr["sims_pros_mother_name_ot"].ToString();
                            pros_obj.sims_pros_mother_nationality1_code = dr["sims_pros_mother_nationality1_code"].ToString();
                            pros_obj.sims_pros_mother_nationality2_code = dr["sims_pros_mother_nationality2_code"].ToString();
                            pros_obj.sims_pros_mother_appartment_number = dr["sims_pros_mother_appartment_number"].ToString();
                            pros_obj.sims_pros_mother_building_number = dr["sims_pros_mother_building_number"].ToString();
                            pros_obj.sims_pros_mother_street_number = dr["sims_pros_mother_street_number"].ToString();
                            pros_obj.sims_pros_mother_area_number = dr["sims_pros_mother_area_number"].ToString();
                            pros_obj.sims_pros_mother_summary_address = dr["sims_pros_mother_summary_address"].ToString();
                            pros_obj.sims_pros_mother_city = dr["sims_pros_mother_city"].ToString();
                            pros_obj.sims_pros_mother_state = dr["sims_pros_mother_state"].ToString();
                            pros_obj.sims_pros_mother_country_code = dr["sims_pros_mother_country_code"].ToString();
                            pros_obj.sims_pros_mother_phone = dr["sims_pros_mother_phone"].ToString();
                            pros_obj.sims_pros_mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                            pros_obj.sims_pros_mother_email = dr["sims_pros_mother_email"].ToString();
                            pros_obj.sims_pros_mother_fax = dr["sims_pros_mother_fax"].ToString();
                            pros_obj.sims_pros_mother_po_box = dr["sims_pros_mother_po_box"].ToString();
                            pros_obj.sims_pros_mother_occupation = dr["sims_pros_mother_occupation"].ToString();
                            pros_obj.sims_pros_mother_company = dr["sims_pros_mother_company"].ToString();
                            pros_obj.sims_pros_mother_passport_number = dr["sims_pros_mother_passport_number"].ToString();
                            pros_obj.sims_pros_guardian_salutation_code = dr["sims_pros_guardian_salutation_code"].ToString();
                            pros_obj.sims_pros_guardian_first_name = dr["sims_pros_guardian_first_name"].ToString();
                            pros_obj.sims_pros_guardian_middle_name = dr["sims_pros_guardian_middle_name"].ToString();
                            pros_obj.sims_pros_guardian_last_name = dr["sims_pros_guardian_last_name"].ToString();
                            pros_obj.sims_pros_guardian_family_name = dr["sims_pros_guardian_family_name"].ToString();
                            pros_obj.sims_pros_guardian_name_ot = dr["sims_pros_guardian_name_ot"].ToString();
                            pros_obj.sims_pros_guardian_nationality1_code = dr["sims_pros_guardian_nationality1_code"].ToString();
                            pros_obj.sims_pros_guardian_nationality2_code = dr["sims_pros_guardian_nationality2_code"].ToString();
                            pros_obj.sims_pros_guardian_appartment_number = dr["sims_pros_guardian_appartment_number"].ToString();
                            pros_obj.sims_pros_guardian_building_number = dr["sims_pros_guardian_building_number"].ToString();
                            pros_obj.sims_pros_guardian_street_number = dr["sims_pros_guardian_street_number"].ToString();
                            pros_obj.sims_pros_guardian_area_number = dr["sims_pros_guardian_area_number"].ToString();
                            pros_obj.sims_pros_guardian_summary_address = dr["sims_pros_guardian_summary_address"].ToString();
                            pros_obj.sims_pros_guardian_city = dr["sims_pros_guardian_city"].ToString();
                            pros_obj.sims_pros_guardian_state = dr["sims_pros_guardian_state"].ToString();
                            pros_obj.sims_pros_guardian_country_code = dr["sims_pros_guardian_country_code"].ToString();
                            pros_obj.sims_pros_guardian_phone = dr["sims_pros_guardian_phone"].ToString();
                            pros_obj.sims_pros_guardian_mobile = dr["sims_pros_guardian_mobile"].ToString();
                            pros_obj.sims_pros_guardian_email = dr["sims_pros_guardian_email"].ToString();
                            pros_obj.sims_pros_guardian_fax = dr["sims_pros_guardian_fax"].ToString();
                            pros_obj.sims_pros_guardian_po_box = dr["sims_pros_guardian_po_box"].ToString();
                            pros_obj.sims_pros_guardian_occupation = dr["sims_pros_guardian_occupation"].ToString();
                            pros_obj.sims_pros_guardian_company = dr["sims_pros_guardian_company"].ToString();
                            pros_obj.sims_pros_guardian_relationship_code = dr["sims_pros_guardian_relationship_code"].ToString();
                            pros_obj.sims_pros_guardian_passport_number = dr["sims_pros_guardian_passport_number"].ToString();
                            pros_obj.sims_pros_current_school_status = dr["sims_pros_current_school_status"].ToString();
                            pros_obj.sims_pros_current_school_name = dr["sims_pros_current_school_name"].ToString();
                            pros_obj.sims_pros_current_school_grade = dr["sims_pros_current_school_grade"].ToString();
                            pros_obj.sims_pros_current_school_cur = dr["sims_pros_current_school_cur"].ToString();
                            pros_obj.sims_pros_current_school_enroll_number = dr["sims_pros_current_school_enroll_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_current_school_from_date"].ToString()))
                                //pros_obj.sims_pros_current_school_from_date = DateTime.Parse(dr["sims_pros_current_school_from_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_current_school_from_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_from_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_current_school_to_date"].ToString()))
                                //pros_obj.sims_pros_current_school_to_date = DateTime.Parse(dr["sims_pros_current_school_to_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_current_school_to_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_to_date"].ToString());
                            pros_obj.sims_pros_current_school_language = dr["sims_pros_current_school_language"].ToString();
                            pros_obj.sims_pros_current_school_head_teacher = dr["sims_pros_current_school_head_teacher"].ToString();
                            pros_obj.sims_pros_current_school_phone = dr["sims_pros_current_school_phone"].ToString();
                            pros_obj.sims_pros_current_school_fax = dr["sims_pros_current_school_fax"].ToString();
                            pros_obj.sims_pros_current_school_city = dr["sims_pros_current_school_city"].ToString();
                            pros_obj.sims_pros_current_school_country_code = dr["sims_pros_current_school_country_code"].ToString();
                            pros_obj.sims_pros_current_school_address = dr["sims_pros_current_school_address"].ToString();
                            pros_obj.sims_pros_marketing_code = dr["sims_pros_marketing_code"].ToString();
                            pros_obj.sims_pros_marketing_description = dr["sims_pros_marketing_description"].ToString();
                            pros_obj.sims_pros_parent_status_code = dr["sims_pros_parent_status_code"].ToString();
                            pros_obj.sims_pros_legal_custody = dr["sims_pros_legal_custody"].ToString();
                            pros_obj.sims_pros_health_card_number = dr["sims_pros_health_card_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_health_card_issue_date"].ToString()))
                                //pros_obj.sims_pros_health_card_issue_date = DateTime.Parse(dr["sims_pros_health_card_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_health_card_expiry_date"].ToString()))
                                //pros_obj.sims_pros_health_card_expiry_date = DateTime.Parse(dr["sims_pros_health_card_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_expiry_date"].ToString());
                            pros_obj.sims_pros_health_card_issuing_authority = dr["sims_pros_health_card_issuing_authority"].ToString();
                            pros_obj.sims_pros_blood_group_code = dr["sims_pros_blood_group_code"].ToString();
                            pros_obj.sims_pros_medication_status = dr["sims_pros_medication_status"].ToString();
                            pros_obj.sims_pros_medication_desc = dr["sims_pros_medication_desc"].ToString();
                            pros_obj.sims_pros_disability_status = dr["sims_pros_disability_status"].ToString();
                            pros_obj.sims_pros_disability_desc = dr["sims_pros_disability_desc"].ToString();
                            pros_obj.sims_pros_behaviour_status = dr["sims_pros_behaviour_status"].ToString();
                            pros_obj.sims_pros_behaviour_desc = dr["sims_pros_behaviour_desc"].ToString();
                            pros_obj.sims_pros_health_restriction_status = dr["sims_pros_health_restriction_status"].ToString();
                            pros_obj.sims_pros_health_restriction_desc = dr["sims_pros_health_restriction_desc"].ToString();
                            pros_obj.sims_pros_health_hearing_status = dr["sims_pros_health_hearing_status"].ToString();
                            pros_obj.sims_pros_health_hearing_desc = dr["sims_pros_health_hearing_desc"].ToString();
                            pros_obj.sims_pros_health_vision_status = dr["sims_pros_health_vision_status"].ToString();
                            pros_obj.sims_pros_health_vision_desc = dr["sims_pros_health_vision_desc"].ToString();
                            pros_obj.sims_pros_health_other_status = dr["sims_pros_health_other_status"].ToString();
                            pros_obj.sims_pros_health_other_desc = dr["sims_pros_health_other_desc"].ToString();
                            pros_obj.sims_pros_language_support_status = dr["sims_pros_language_support_status"].ToString();
                            pros_obj.sims_pros_language_support_desc = dr["sims_pros_language_support_desc"].ToString();
                            pros_obj.sims_pros_gifted_status = dr["sims_pros_gifted_status"].ToString();
                            pros_obj.sims_pros_gifted_desc = dr["sims_pros_gifted_desc"].ToString();
                            pros_obj.sims_pros_music_status = dr["sims_pros_music_status"].ToString();
                            pros_obj.sims_pros_music_desc = dr["sims_pros_music_desc"].ToString();
                            pros_obj.sims_pros_sports_status = dr["sims_pros_sports_status"].ToString();
                            pros_obj.sims_pros_sports_desc = dr["sims_pros_sports_desc"].ToString();
                            pros_obj.sims_pros_declaration_status = dr["sims_pros_declaration_status"].ToString();
                            pros_obj.sims_pros_status = dr["sims_pros_status"].ToString();
                            pros_obj.sims_pros_ip = dr["sims_pros_ip"].ToString();
                            pros_obj.sims_pros_dns = dr["sims_pros_dns"].ToString();
                            pros_obj.sims_pros_user_code = dr["sims_pros_user_code"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("getRegistration")]
        public HttpResponseMessage getRegistration()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRegistration(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Registration", "Registration"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_appl_parameter_reg = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1_reg = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetcriteriaMarks")]
        public HttpResponseMessage GetcriteriaMarks(string admission_num)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetcriteriaMarks(),PARAMETERS :: admission_num{2}";
            Log.Debug(string.Format(debug, "Student", "GetcriteriaMarks", admission_num));

            List<Uccw230> type_list = new List<Uccw230>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()//sims.Sims_admission_detail
                        {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@ADMISSION_NUM", admission_num)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw230 simsobj = new Uccw230();
                            simsobj.admis_num = admission_num;
                            simsobj.sims_admission_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            simsobj.sims_criteria_type = dr["sims_criteria_type"].ToString();
                            simsobj.sims_admission_marks = dr["sims_admission_marks"].ToString();
                            simsobj.sims_admission_rating = dr["sims_admission_rating"].ToString();
                            simsobj.sims_marks = dr["sims_marks"].ToString();
                            try
                            {
                                simsobj.sims_admission_criteria_section_number = dr["sims_admission_criteria_section_number"].ToString();
                                simsobj.sims_admission_criteria_part_number = dr["sims_admission_criteria_part_number"].ToString();

                                simsobj.sims_admission_memo_from = dr["sims_admission_memo_from"].ToString();
                                simsobj.sims_admission_memo_to = dr["sims_admission_memo_to"].ToString();
                                simsobj.sims_admission_mutiple = dr["sims_admission_mutiple"].ToString();
                                simsobj.sims_admission_rating1 = dr["sims_admission_rating1"].ToString();


                            }
                            catch (Exception ex)
                            {
                            }

                            // simsobj.sims_criteria_status = dr["sims_criteria_status"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetCriteriaName")]
        public HttpResponseMessage GetCriteriaName(string admission_num)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCriteriaName(),PARAMETERS :: admission_num{2}";
            Log.Debug(string.Format(debug, "Student", "GetCriteriaName", admission_num));

            List<Uccw229> type_list = new List<Uccw229>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@sims_admission_number", admission_num)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw229 simsobj = new Uccw229();
                            simsobj.admis_num = admission_num;
                            simsobj.sims_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            simsobj.sims_criteria_type = dr["sims_criteria_type"].ToString();

                            simsobj.count = int.Parse(dr["count"].ToString());

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                               new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'C'),
                                    new SqlParameter("@sims_admission_number", admission_num),
                                    new SqlParameter("@sims_admission_criteria_code", simsobj.sims_criteria_code)
                                });
                                if (dr1.HasRows)
                                {
                                    string sims_admission_doc_path = "";
                                    bool status = false, verify = false;
                                    while (dr1.Read())
                                    {
                                        Uccw229 simsobj1 = new Uccw229();
                                        simsobj1.sims_admission_doc_path = dr1["sims_admission_doc_path"].ToString();
                                        sims_admission_doc_path = sims_admission_doc_path + ',' + simsobj1.sims_admission_doc_path;

                                        simsobj1.if_uploaded = !string.IsNullOrEmpty(simsobj1.sims_admission_doc_path);
                                        if (dr1["sims_admission_doc_status"].ToString() == "0")
                                            simsobj1.sims_admission_doc_status = false;
                                        else if (dr1["sims_admission_doc_status"].ToString() == "1")
                                            simsobj1.sims_admission_doc_status = true;
                                        if (dr1["sims_admission_doc_verify"].ToString() == "0")
                                            simsobj1.sims_admission_doc_verify = false;
                                        else if (dr1["sims_admission_doc_verify"].ToString() == "1")
                                            simsobj1.sims_admission_doc_verify = true;

                                        simsobj1.if_status = simsobj1.if_uploaded;//for isenable
                                        simsobj1.if_verify = (simsobj1.if_status && simsobj1.if_uploaded);
                                        status = simsobj1.sims_admission_doc_status;
                                        verify = simsobj1.sims_admission_doc_verify;

                                    }
                                    simsobj.sims_admission_doc_path = sims_admission_doc_path.Substring(1);
                                    simsobj.sims_admission_doc_status = status;
                                    simsobj.sims_admission_doc_verify = verify;
                                }
                            }
                            //for isenable
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetcriteriaMarksGradeScale")]
        public HttpResponseMessage GetcriteriaMarksGradeScale()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetcriteriaMarks(),PARAMETERS :: admission_num{2}";
            //Log.Debug(string.Format(debug, "Student", "GetcriteriaMarks", admission_num));

            List<Uccw230> type_list = new List<Uccw230>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "L6"),
                            //new SqlParameter("@ADMISSION_NUM", admission_num)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw230 simsobj = new Uccw230();
                            //simsobj.admis_num = admission_num;
                            simsobj.sims_admission_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_mark_assesment_grade_name"].ToString();
                            simsobj.sims_mark_assesment_grade_point = dr["sims_mark_assesment_grade_point"].ToString();

                            simsobj.sims_mark_assesment_grade_description = dr["sims_mark_assesment_grade_description"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }




        [Route("CUD_UpdateAdmissionMarks")]
        public HttpResponseMessage CUD_UpdateAdmissionMarks(List<Uccw230> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAdmissionMarks(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "AdmissionMarks", "UpdateAdmissionMarks"));

            List<Uccw230> type_list = new List<Uccw230>();
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Uccw230 simsobj in lstsimsobj)
                    {
                        if (!string.IsNullOrEmpty(simsobj.sims_admission_marks))
                        {
                            if (decimal.Parse(simsobj.sims_marks) < decimal.Parse(simsobj.sims_admission_marks))
                            {
                                simsobj.sims_admission_marks = "";
                            }
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_update_admissionmarks_proc",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_criteria_code", simsobj.sims_admission_criteria_code),
                            new SqlParameter("@sims_admission_marks", simsobj.sims_admission_marks),
                            new SqlParameter("@sims_admission_rating", simsobj.sims_admission_rating),
                            new SqlParameter("@sims_admission_date_created", System.DateTime.Now.Date),
                            new SqlParameter("@sims_admission_user_code", "")
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            message.strMessage = "Admission Marks Updated Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                        else
                        {
                            inserted = false;
                            return Request.CreateResponse(HttpStatusCode.OK, "Fail");
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "Error In Updating Admission Marks Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("CUD_UpdateAdmissionMarksOba")]
        public HttpResponseMessage CUD_UpdateAdmissionMarksOba(List<Uccw230> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAdmissionMarks(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "AdmissionMarks", "UpdateAdmissionMarks"));

            List<Uccw230> type_list = new List<Uccw230>();
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Uccw230 simsobj in lstsimsobj)
                    {
                        if (!string.IsNullOrEmpty(simsobj.sims_admission_marks))
                        {
                            if (decimal.Parse(simsobj.sims_marks) < decimal.Parse(simsobj.sims_admission_marks))
                            {
                                simsobj.sims_admission_marks = "";
                            }
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_update_admissionmarks_proc",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_criteria_code", simsobj.sims_admission_criteria_code),
                            new SqlParameter("@sims_admission_marks", simsobj.sims_admission_marks),
                            new SqlParameter("@sims_admission_rating", simsobj.sims_admission_rating),
                            new SqlParameter("@sims_admission_date_created", System.DateTime.Now.Date),
                            new SqlParameter("@sims_admission_user_code", simsobj.sims_admission_user_code),

                            new SqlParameter("@sims_admission_memo_from", simsobj.sims_admission_memo_from),
                            new SqlParameter("@sims_admission_memo_to",simsobj.sims_admission_memo_to),
                            new SqlParameter("@sims_admission_mutiple", simsobj.sims_admission_mutiple),

                            new SqlParameter("@sims_admission_rating1", simsobj.sims_admission_rating1),



                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            message.strMessage = "Admission Marks Updated Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                        else
                        {
                            inserted = false;
                            return Request.CreateResponse(HttpStatusCode.OK, "Fail");
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "Error In Updating Admission Marks Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("CUD_Insert_Admission_Doc")]
        public HttpResponseMessage CUD_Insert_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Insert_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Insert_Admission_Doc"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("insert_admission_doc",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                            new SqlParameter("@sims_admission_doc_path", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify", simsobj.sims_admission_doc_verify== true?"1":"0")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUD_Delete_Admission_Doc")]
        public HttpResponseMessage CUD_Delete_Admission_Doc(string adm_no, string criteria_code, string doc_path)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Insert_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Insert_Admission_Doc"));
            // bool deleted = false;
            Message message = new Message();
            try
            {
                // if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr","D"),
                            new SqlParameter("@sims_admission_number", adm_no),
                            new SqlParameter("@sims_admission_criteria_code",criteria_code),
                            new SqlParameter("@sims_admission_doc_path", doc_path),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Document Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUD_Update_Admission_Doc")]
        public HttpResponseMessage CUD_Update_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                             new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                         });
                        if (ins > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("CUD_Update_Admission_DocList")]
        public HttpResponseMessage CUD_Update_Admission_DocList(List<Uccw229> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            Array str;
            string str2;
            try
            {
                if (lstsimsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw229 simsobj in lstsimsobj)
                        {
                            str2 = simsobj.sims_admission_doc_path;
                            str = str2.Split(',');

                            foreach (string k in str)
                            {
                                string sims_admission_doc_path = k;
                                //}
                                int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                                    new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr","NNN"),
                                new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                                new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                                // new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                                // new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_path_old", sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_path_new", sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                                new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                                });
                                if (ins > 0)
                                {
                                    updated = true;
                                }
                                else
                                {
                                    updated = false;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("GetAdmissionTemplates")]
        public HttpResponseMessage GetAdmissionTemplates()
        {
            List<Sims010_Auth> temp_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'X')

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_type = dr["sims_msg_type"].ToString();
                            simsobj.sims_msg_subject = dr["sims_msg_subject"].ToString();
                            simsobj.sims_msg_body = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                            temp_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, temp_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, temp_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, temp_list);
            }
        }

        [Route("GetAdmissionTemplatesBody")]
        public HttpResponseMessage GetAdmissionTemplatesBody(string template_subject)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_msg_subject", template_subject),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_body = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
        }

        [Route("GetAdmission_EmailHistory")]
        public HttpResponseMessage GetAdmission_EmailHistory(string adnum)
        {
            List<Sims010_Auth> lst_ad = new List<Sims010_Auth>();
            lst_ad = GetAdmission_EmailIds1(adnum);
            string emailid = string.Empty;

            foreach (Sims010_Auth o in lst_ad)
            {
                emailid = o.emailid;
            }
            List<Comn_email> lst_emails = new List<Comn_email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@emailid", emailid),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn_email simsemail = new Comn_email();
                            simsemail.subject = dr["sims_email_subject"].ToString();
                            //simsemail.emaildate = Convert.ToDateTime(dr["sims_email_date"].ToString()).ToShortDateString();
                            simsemail.emaildate = db.UIDDMMYYYYformat(dr["sims_email_date"].ToString());
                            simsemail.body = dr["sims_email_message"].ToString();
                            lst_emails.Add(simsemail);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
            }
        }

        [Route("GetAdmission_EmailIds")]
        public HttpResponseMessage GetAdmission_EmailIds(string admission_nos)
        {
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Z'),
                            new SqlParameter("@adm_nos", admission_nos),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.emailid = dr["EmailID"].ToString();
                            simsobj.chk_email = true;
                            email_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, email_list);
            }
        }

        private List<Sims010_Auth> GetAdmission_EmailIds1(string admission_nos)
        {
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'Z'),
                            new SqlParameter("@adm_nos", admission_nos),
                         });
                    while (dr.Read())
                    {
                        Sims010_Auth simsobj = new Sims010_Auth();
                        simsobj.emailid = dr["EmailID"].ToString();
                        simsobj.chk_email = true;
                        email_list.Add(simsobj);
                    }
                }
                return email_list;
            }
            catch (Exception x)
            {
                return email_list;
            }
        }

        [Route("getCommunication")]
        public HttpResponseMessage getCommunication(string adm_no)
        {
            List<Uccw034> type_list = new List<Uccw034>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_comm_proc]",
                        new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "S"),
                       new SqlParameter("@ADMISSION_NUM", adm_no),
                       new SqlParameter("@COMM_MODE", ""),
                       new SqlParameter("@COMM_DATE", ""),
                       new SqlParameter("@COMM_DESC", ""),
                       new SqlParameter("@COMM_ENQRER_REM", ""),
                       new SqlParameter("@COMM_STATUS","A"),
                    });
                    while (dr.Read())
                    {
                        Uccw034 simsobj = new Uccw034();
                        simsobj.admis_num = adm_no;
                        simsobj.comm_desc = dr["sims_admission_comm_desc"].ToString();
                        simsobj.comm_method = dr["sims_admission_comm_mode"].ToString();
                        simsobj.comm_mode_desc = dr["Mode"].ToString();
                        simsobj.comm_date = db.UIDDMMYYYYformat(dr["sims_admission_comm_date"].ToString());
                        //simsobj.comm_date = DateTime.Parse(dr["sims_admission_comm_date"].ToString()).ToShortDateString();
                        simsobj.enq_rem = dr["sims_admission_comm_inquirer_remark"].ToString();
                        simsobj.status = dr["sims_admission_comm_status"].ToString();
                        type_list.Add(simsobj);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUDCommunication")]
        public HttpResponseMessage CUDCommunication(List<Uccw034> data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw034 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_comm_proc]",
                                new List<SqlParameter>() 
                         {                             
                                new SqlParameter("@opr", "I"),//simsobj.opr
                                new SqlParameter("@ADMISSION_NUM", simsobj.admis_num),
                                new SqlParameter("@COMM_MODE", simsobj.comm_method),
                                new SqlParameter("@COMM_DATE", db.DBYYYYMMDDformat(simsobj.comm_date)),
                                new SqlParameter("@COMM_DESC", simsobj.comm_desc),
                                new SqlParameter("@COMM_ENQRER_REM", simsobj.enq_rem),
                                new SqlParameter("@COMM_STATUS", simsobj.status),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetNationalityCntDetails")]
        public HttpResponseMessage GetNationalityCntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetNationalityCntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetNationalityCntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            //new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.nationality_code = dr["nation_cnt"].ToString();
                            simsobj.nationality_name = dr["nationality"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetGendercntDetails")]
        public HttpResponseMessage GetGendercntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGendercntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetGendercntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.gender_cnt = dr["gender_cnt"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetEmployeeCodecntDetails")]
        public HttpResponseMessage GetEmployeeCodecntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeeCodecntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetEmployeeCodecntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'K'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            //new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.empl_code = dr["sims_admission_employee_code"].ToString();
                            simsobj.empcode_cnt = dr["emp_cnt"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetsiblingcntDetails")]
        public HttpResponseMessage GetsiblingcntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetsiblingcntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "L1"),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            //simsobj.sibling_enroll = dr["sims_admission_sibling_enroll_number"].ToString();
                            simsobj.siblingNo_cnt = dr["sibling_cnt"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetAdm_dashRpt")]
        public HttpResponseMessage GetAdm_dashRpt()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string reportName = "Sims.SIMR41";
            lst.Add(new SqlParameter("@OPR", "FR"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]", lst);//sims.sims_student_fee_TRANS1
                    if (dr.Read())
                    {
                        reportName = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, reportName);
        }

        [Route("GetVisible")]
        public HttpResponseMessage GetVisible()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "WS"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetuserID")]
        public HttpResponseMessage GetuserID(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "WR"),
                            new SqlParameter("@user_name", user),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.comn_user_role_id = dr["comn_user_role_id"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUDUpdateAdm_Studentimage")]
        public HttpResponseMessage CUDUpdateAdm_Studentimage(Sims010_Edit data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateAdmission"));
            Sims010_Edit res = new Sims010_Edit();
            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Sims_admission_detail_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@ADMISSION_NUM", data.admission_number),
                            new SqlParameter("@sims_student_img", data.sims_student_img),
                             new SqlParameter("@sims_mother_img", data.sims_mother_img),
                              new SqlParameter("@sims_father_img", data.sims_father_img),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("GetDashTilesDetailsForRecruitment")]
        public HttpResponseMessage GetDashTilesDetailsForRecruitment()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            //Log.Debug(string.Format(debug, "Student", "GetDashTilesDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                //if (gradeCode == "undefined")
                //{
                //    gradeCode = null;
                //}

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'S'),
                           // new SqlParameter("@CURR_CODE", curr_code),
                           // new SqlParameter("@ACA_YEAR", AcadmicYear),
                           // new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.appl_count = dr["appl_count"].ToString();
                            simsobj.Completed_cnt = dr["Completed_cnt"].ToString();
                            simsobj.rejected_cnt = dr["rejected_cnt"].ToString();
                            simsobj.waiting_cnt = dr["waiting_cnt"].ToString();
                            simsobj.selected_cnt = dr["selected_cnt"].ToString();
                            simsobj.waiting_paid = dr["waiting_paid"].ToString();
                            simsobj.waiting_unpaid = dr["waiting_unpaid"].ToString();
                            try
                            {
                                simsobj.sen_cnt = dr["sen_cnt"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }



        [Route("GetDashEmployeeDetailsForRecruitment")]
        public HttpResponseMessage GetDashEmployeeDetailsForRecruitment(string status)
        {

            List<recu> type_list = new List<recu>();
            try
            {
                if (status == "undefined")
                {
                    status = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@em_application_status", status),

                           // new SqlParameter("@ACA_YEAR", AcadmicYear),
                           // new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            recu simsobj = new recu();
                            simsobj.em_applicant_id = dr["em_applicant_id"].ToString();
                            simsobj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.ename = dr["ename"].ToString();
                            simsobj.em_date_of_birth = dr["em_date_of_birth"].ToString();


                            simsobj.em_sex = dr["em_sex"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_application_date = dr["em_application_date"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetDashEmployeeDetailsForRecruitment")]
        public HttpResponseMessage GetDashEmployeeDetailsForRecruitment(string status, string applicant_id)
        {

            List<recu> type_list = new List<recu>();
            try
            {
                if (status == "undefined")
                {
                    status = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@em_application_status", status),
                            new SqlParameter("@em_applicant_id", applicant_id),

                           // new SqlParameter("@ACA_YEAR", AcadmicYear),
                           // new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            recu simsobj = new recu();
                            simsobj.em_applicant_id = dr["em_applicant_id"].ToString();
                            simsobj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.ename = dr["ename"].ToString();
                            simsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());


                            simsobj.em_sex = dr["em_sex"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_application_date = db.UIDDMMYYYYformat(dr["em_application_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_application_date"].ToString()))
                                simsobj.em_application_date1 = DateTime.Parse(dr["em_application_date"].ToString());

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }




        [Route("CUDUpdateStatusForRecruitment")]
        public HttpResponseMessage CUDUpdateStatusForRecruitment(recu data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateAdmission"));

            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_recruitment_dashboard_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@em_applicant_id", data.em_applicant_id),
                            new SqlParameter("@em_application_status", data.em_application_status),
                           
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("GetNationalityCntDetailsForRecruitment")]
        public HttpResponseMessage GetNationalityCntDetailsForRecruitment()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetNationalityCntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            //Log.Debug(string.Format(debug, "Student", "GetNationalityCntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    //if (gradeCode == "undefined")
                    //{
                    //    gradeCode = null;
                    //}

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'C'),
                            //new SqlParameter("@CURR_CODE", curr_code),
                            //new SqlParameter("@ACA_YEAR", AcadmicYear),
                            //new SqlParameter("@GRADE_CODE", gradeCode),
                            //new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.nationality_code = dr["nation_cnt"].ToString();
                            simsobj.nationality_name = dr["nationality"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetGendercntDetailsForRecruitment")]
        public HttpResponseMessage GetGendercntDetailsForRecruitment()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGendercntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            //Log.Debug(string.Format(debug, "Student", "GetGendercntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    //if (gradeCode == "undefined")
                    //{
                    //    gradeCode = null;
                    //}
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'D'),
                            //new SqlParameter("@CURR_CODE", curr_code),
                            //new SqlParameter("@ACA_YEAR", AcadmicYear),
                            //new SqlParameter("@GRADE_CODE", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.gender_cnt = dr["gender_cnt"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }


        [Route("GetCalendar_Data")]
        public HttpResponseMessage GetCalendar_Data(string curr_code, string AcadmicYear, string gradeCode)
        {
            List<reg_dash> type_list = new List<reg_dash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_regsiter_dashboard_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_cur_code", curr_code),
                            new SqlParameter("@sims_academic_year", AcadmicYear),
                            new SqlParameter("@sims_grade_code", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reg_dash simsobj = new reg_dash();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            simsobj.event_nm = dr["event_nm"].ToString();
                            //simsobj.gender = dr["gender"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetCalendar_Data_details")]
        public HttpResponseMessage GetCalendar_Data_details(string curr_code, string AcadmicYear, string gradeCode, string date)
        {
            List<reg_dash> type_list = new List<reg_dash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_regsiter_dashboard_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sims_cur_code", curr_code),
                            new SqlParameter("@sims_academic_year", AcadmicYear),
                            new SqlParameter("@sims_grade_code", gradeCode),
                            new SqlParameter("@sims_date", date)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reg_dash simsobj = new reg_dash();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            simsobj.event_nm = dr["event_nm"].ToString();

                            simsobj.sims_assesment_time = dr["sims_assesment_time"].ToString();
                            simsobj.em_name = dr["em_name"].ToString();
                            simsobj.sims_admission_no = dr["sims_admission_no"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }


        [Route("GetEmail_details")]
        public HttpResponseMessage GetEmail_details(string curr_code, string AcadmicYear, string gradeCode)
        {
            List<reg_dash> type_list = new List<reg_dash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_regsiter_dashboard_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@sims_cur_code", curr_code),
                            new SqlParameter("@sims_academic_year", AcadmicYear),
                            new SqlParameter("@sims_grade_code", gradeCode),
                            //new SqlParameter("@sims_date", date)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reg_dash simsobj = new reg_dash();
                            simsobj.sims_admission_comm_desc = dr["sims_admission_comm_desc"].ToString();
                            simsobj.sims_admission_comm_admission_number = dr["sims_admission_comm_admission_number"].ToString();

                            simsobj.sims_admission_comm_date = dr["sims_admission_comm_date"].ToString();

                            simsobj.std_name = dr["std_name"].ToString();



                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }



        [Route("GetAlert_details")]
        public HttpResponseMessage GetAlert_details(string curr_code, string AcadmicYear, string gradeCode)
        {
            List<reg_dash> type_list = new List<reg_dash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_regsiter_dashboard_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@sims_cur_code", curr_code),
                            new SqlParameter("@sims_academic_year", AcadmicYear),
                            new SqlParameter("@sims_grade_code", gradeCode),
                            //new SqlParameter("@sims_date", date)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reg_dash simsobj = new reg_dash();
                            simsobj.sims_comm_message_remark = dr["sims_comm_message_remark"].ToString();
                            simsobj.sims_comm_message_feedback = dr["sims_comm_message_feedback"].ToString();

                            simsobj.sims_comm_user_by = dr["sims_comm_user_by"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();

                            simsobj.sims_comm_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_follow_up_date = dr["sims_follow_up_date"].ToString();


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }


        [Route("GetAdm_Email_details")]
        public HttpResponseMessage GetAdm_Email_details(string admission_no)
        {
            List<reg_dash> type_list = new List<reg_dash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (admission_no == "undefined")
                    {
                        admission_no = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_regsiter_dashboard_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'E'),
                         
                            new SqlParameter("@sims_admission_no", admission_no),
                            //new SqlParameter("@sims_date", date)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reg_dash simsobj = new reg_dash();
                            simsobj.sims_admission_comm_desc = dr["sims_email_message"].ToString();
                            simsobj.std_name = dr["sims_email_subject"].ToString();
                            simsobj.sims_admission_comm_date = dr["sims_email_date"].ToString();


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }



        [Route("getAllpartSection")]
        public HttpResponseMessage getAllpartSection()
        {
            List<part_Section> subCon_lst = new List<part_Section>();
            List<part_Section> subCon_lst1 = new List<part_Section>();

            Object[] arr = new Object[2];
            //object[] arr;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","L7"),
                               
                         });
                    if (dr.HasRows)
                    {


                        while (dr.Read())
                        {
                            part_Section fd = new part_Section();
                            fd.sims_admission_criteria_part_number = dr["sims_admission_criteria_part_number"].ToString();
                            fd.sims_admission_criteria_part_Name = dr["sims_admission_criteria_part_Name"].ToString();

                            subCon_lst.Add(fd);
                        }
                        dr.NextResult();

                        while (dr.Read())
                        {
                            part_Section id = new part_Section();
                            id.sims_admission_criteria_part_number = dr["sims_admission_criteria_part_number"].ToString();
                            id.sims_admission_criteria_section_number = dr["sims_admission_criteria_section_number"].ToString();
                            id.sims_admission_criteria_section_Name = dr["sims_admission_criteria_section_Name"].ToString();

                            subCon_lst1.Add(id);
                        }



                        arr[0] = subCon_lst;
                        arr[1] = subCon_lst1;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, arr);
        }



        [Route("GetInterviewDetailsForRecruitment")]
        public HttpResponseMessage GetInterviewDetailsForRecruitment()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            //Log.Debug(string.Format(debug, "Student", "GetDashTilesDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                //if (gradeCode == "undefined")
                //{
                //    gradeCode = null;
                //}

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_recruitment_dashboard_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'H'),
                           // new SqlParameter("@CURR_CODE", curr_code),
                           // new SqlParameter("@ACA_YEAR", AcadmicYear),
                           // new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.total_opeing_cnt = dr["total_opeing_cnt"].ToString();
                            simsobj.total_upcomeing_cnt = dr["total_upcomeing_cnt"].ToString();
                            simsobj.total_ltrissued_cnt = dr["total_ltrissued_cnt"].ToString();
                            simsobj.total_penvacncy_cnt = dr["total_penvacncy_cnt"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetStudentsDmc")]
        public HttpResponseMessage GetStudentsDmc(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string gender_code, string nationality_code, string employee_code, string sibling_enroll, string adm_no, string adm_name, string sen_status)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (sen_status == "undefined" || sen_status == "null")
                {
                    sen_status = null;
                }
                if (adm_no == "undefined" || adm_no == "null")
                {
                    adm_no = null;
                }
                if (adm_name == "undefined" || adm_name == "null")
                {
                    adm_name = null;
                }
                if (gender_code == "null")
                {
                    gender_code = null;
                }
                if (nationality_code == "null")
                {
                    nationality_code = null;
                }
                if (sibling_enroll == "null")
                {
                    sibling_enroll = null;
                }
                if (employee_code == "null")
                {
                    employee_code = null;
                }
                if (admission_status == "null")
                {
                    admission_status = null;
                }

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                    str = "G";
                else if (!string.IsNullOrEmpty(sibling_enroll))
                    str = "G1";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            new SqlParameter("@sims_admission_employee_code",employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",nationality_code),
                            new SqlParameter("@sims_admission_gender",gender_code),
                            new SqlParameter("@adm_nos",adm_no),
                            new SqlParameter("@First_name",adm_name),
                            new SqlParameter("@SEN_status",sen_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();

                            try
                            {
                                Sims010_Edit simsobj1 = new Sims010_Edit();
                                simsobj1 = GetStudent_PrevExamDetails(simsobj.admission_number);


                                simsobj.current_school_name = simsobj1.current_school_name;
                                simsobj.sims_admission_boardname = simsobj1.sims_admission_boardname;
                                simsobj.sims_admission_center_name = simsobj1.sims_admission_center_name;
                                simsobj.sims_admission_passingyear_exam = simsobj1.sims_admission_passingyear_exam;
                                simsobj.sims_admission_seat_no = simsobj1.sims_admission_seat_no;
                                simsobj.sims_admission_no_of_attempts = simsobj1.sims_admission_no_of_attempts;
                                simsobj.sims_admission_marks_obtained = simsobj1.sims_admission_marks_obtained;
                                simsobj.sims_admission_marks_out_of = simsobj1.sims_admission_marks_out_of;
                                simsobj.sims_admission_percentage_obtained = simsobj1.sims_admission_percentage_obtained;


                                simsobj.current_fyschool_name = simsobj1.current_fyschool_name;
                                simsobj.sims_admission_fyboardname = simsobj1.sims_admission_fyboardname;
                                simsobj.sims_admission_fycenter_name = simsobj1.sims_admission_fycenter_name;
                                simsobj.sims_admission_fypassingyear_exam = simsobj1.sims_admission_fypassingyear_exam;
                                simsobj.sims_admission_fyseat_no = simsobj1.sims_admission_fyseat_no;
                                simsobj.sims_fyadmission_no_of_attempts_sem1 = simsobj1.sims_fyadmission_no_of_attempts_sem1;
                                simsobj.sims_fyadmission_marks_obtained_sem1 = simsobj1.sims_fyadmission_marks_obtained_sem1;
                                simsobj.sims_fyadmission_marks_out_of_Sem1 = simsobj1.sims_fyadmission_marks_out_of_Sem1;
                                simsobj.sims_fyadmission_percentage_obtained_sem1 = simsobj1.sims_fyadmission_percentage_obtained_sem1;

                                simsobj.sims_fyadmission_no_of_attempts_sem2 = simsobj1.sims_fyadmission_no_of_attempts_sem2;
                                simsobj.sims_fyadmission_marks_obtained_sem2 = simsobj1.sims_fyadmission_marks_obtained_sem2;
                                simsobj.sims_fyadmission_marks_out_of_Sem2 = simsobj1.sims_fyadmission_marks_out_of_Sem2;
                                simsobj.sims_fyadmission_percentage_obtained_sem2 = simsobj1.sims_fyadmission_percentage_obtained_sem2;


                                simsobj.current_syschool_name = simsobj1.current_syschool_name;
                                simsobj.sims_admission_syboardname = simsobj1.sims_admission_syboardname;
                                simsobj.sims_admission_sycenter_name = simsobj1.sims_admission_sycenter_name;
                                simsobj.sims_admission_sypassingyear_exam = simsobj1.sims_admission_sypassingyear_exam;
                                simsobj.sims_admission_syseat_no = simsobj1.sims_admission_syseat_no;
                                simsobj.sims_syadmission_no_of_attempts_sem3 = simsobj1.sims_syadmission_no_of_attempts_sem3;
                                simsobj.sims_syadmission_marks_obtained_sem3 = simsobj1.sims_syadmission_marks_obtained_sem3;
                                simsobj.sims_syadmission_marks_out_of_Sem3 = simsobj1.sims_syadmission_marks_out_of_Sem3;
                                simsobj.sims_syadmission_percentage_obtained_sem3 = simsobj1.sims_syadmission_percentage_obtained_sem3;

                                simsobj.sims_syadmission_no_of_attempts_sem4 = simsobj1.sims_syadmission_no_of_attempts_sem4;
                                simsobj.sims_syadmission_marks_obtained_sem4 = simsobj1.sims_syadmission_marks_obtained_sem4;
                                simsobj.sims_syadmission_marks_out_of_sem4 = simsobj1.sims_syadmission_marks_out_of_sem4;
                                simsobj.sims_syadmission_percentage_obtained_sem4 = simsobj1.sims_syadmission_percentage_obtained_sem4;


                            }
                            catch
                            {

                            }

                            try
                            {
                                simsobj.admission_number_new = int.Parse(Regex.Match(dr["sims_admission_number"].ToString(), @"\d+").Value);
                                //simsobj.sims_ename = dr["sims_ename"].ToString();
                                simsobj.sims_allocation_status = dr["sims_allocation_status"].ToString();

                            }
                            catch (Exception ex) { }

                            try
                            {
                                simsobj.sims_student_mobile = dr["sims_admission_student_mobile"].ToString();

                            }
                            catch (Exception ex) { }

                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["communication_admi_date"].ToString()))
                                    simsobj.communication_admi_date1 = DateTime.Parse(dr["communication_admi_date"].ToString());


                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                                simsobj.sims_follow_up_date = db.UIDDMMYYYYformat(dr["sims_follow_up_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_follow_up_date"].ToString()))
                                    simsobj.sims_follow_up_date1 = DateTime.Parse(dr["sims_follow_up_date"].ToString());

                            }
                            catch (Exception rx) { }



                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_admission_date"].ToString()))
                                simsobj.admission_date1 = DateTime.Parse(dr["sims_admission_date"].ToString());

                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                simsobj.actual_marks = dr["actual_marks"].ToString();
                                simsobj.max_marks = dr["max_marks"].ToString();
                            }
                            catch (Exception ee)
                            {

                            }

                            try
                            {
                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                simsobj.term_code = dr["term_code"].ToString();
                                simsobj.term_name = dr["term_desc"].ToString();

                                simsobj.section_code = dr["sims_admission_section_code"].ToString();
                                simsobj.section_name = dr["section"].ToString();
                            }
                            catch (Exception et)
                            {
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }

                            try
                            {
                                simsobj.sims_admission_completed_status = dr["sims_completed_status"].ToString();

                            }
                            catch (Exception ex)
                            {
                            }


                            try
                            {
                                simsobj.sims_admission_transport_address = dr["sims_admission_transport_address"].ToString();

                                simsobj.sims_admission_total_memorization = dr["sims_admission_total_memorization"].ToString();
                                simsobj.sims_admission_exactly_total_memorization = dr["sims_admission_exactly_total_memorization"].ToString();
                                simsobj.sims_admission_attend_any_quran_center_before = dr["sims_admission_attend_any_quran_center_before"].ToString();
                                simsobj.sims_admission_grade = dr["sims_admission_grade"].ToString();
                                simsobj.sims_admission_grade_levels = dr["sims_admission_grade_levels"].ToString();
                                simsobj.sims_admission_levels = dr["sims_admission_levels"].ToString();
                                simsobj.sims_admission_total_memorization_to = dr["sims_admission_total_memorization_to"].ToString();
                                simsobj.sims_admission_exactly_total_memorization_to = dr["sims_admission_exactly_total_memorization_to"].ToString();


                            }
                            catch (Exception ex)
                            {
                            }


                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("getAdmissionList")]
        public HttpResponseMessage getAdmissionList()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "ATTENDANCECRITERIA"));
            List<Sims010_Edit> listall = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetAdmissionData_Temp_New",
                        new List<SqlParameter>()
                        {
                            // new SqlParameter("@opr", 'S'),[sims].[GetAdmissionData_Temp_proc]GetAdmissionData_Temp

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit getlist = new Sims010_Edit();
                            {
                                getlist.school_code = dr["lic_school_code"].ToString();
                                getlist.school_name = dr["lic_school_name"].ToString();
                                //getlist.sibling_school_code = getlist.school_code;
                                //getlist.sibling_school_name = getlist.school_name;
                                //getlist.employee_school_code = getlist.school_code;
                                //getlist.employee_school_name = getlist.school_name;
                            }
                            {
                                getlist.curr_code = dr["sims_cur_code"].ToString();
                                getlist.curr_name = dr["sims_cur_full_name_en"].ToString();
                            }
                            {
                                getlist.birth_country_code = dr["sims_country_code"].ToString();
                                getlist.birth_country = dr["sims_country_name_en"].ToString();
                                getlist.father_country_code = getlist.birth_country_code;
                                getlist.father_country = getlist.birth_country;
                                getlist.mother_country_code = getlist.birth_country_code;
                                getlist.mother_country = getlist.birth_country;
                                getlist.guardian_country_code = getlist.birth_country_code;
                                getlist.guardian_country = getlist.birth_country;
                                getlist.current_school_country_code = getlist.birth_country_code;
                                getlist.current_school_country = getlist.birth_country;
                            }
                            {

                                getlist.nationality_code = dr["sims_nationality_code"].ToString();
                                getlist.nationality = dr["sims_nationality_name_en"].ToString();
                                getlist.father_nationality1_code = getlist.nationality_code;
                                getlist.father_nationality1_desc = getlist.nationality;
                                getlist.father_nationality2_code = getlist.nationality_code;
                                getlist.father_nationality2_desc = getlist.nationality;

                                getlist.mother_nationality1_code = getlist.nationality_code;
                                getlist.mother_nationality1_desc = getlist.nationality;
                                getlist.mother_nationality2_code = getlist.nationality_code;
                                getlist.mother_nationality2_desc = getlist.nationality;

                                getlist.guardian_nationality1_code = getlist.nationality_code;
                                getlist.guardian_nationality1_desc = getlist.nationality;
                                getlist.guardian_nationality2_code = getlist.nationality_code;
                                getlist.guardian_nationality2_desc = getlist.nationality;
                            }

                            {
                                getlist.main_language_code = dr["sims_language_code"].ToString();
                                getlist.main_language_desc = dr["sims_language_name_en"].ToString();
                                getlist.other_language_code = getlist.main_language_code;
                                getlist.other_language = getlist.main_language_desc;
                            }
                            //if (string.IsNullOrEmpty(dr[12].ToString()) == false)
                            {
                                getlist.ethinicity_code = dr["sims_ethnicity_code"].ToString();
                                getlist.ethinicity = dr["sims_ethnicity_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[13].ToString()) == false)
                            {
                                getlist.religion_code = dr["sims_religion_code"].ToString();
                                getlist.religion_desc = dr["sims_religion_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[15].ToString()) == false)
                            {
                                getlist.gender_code = dr["sims_appl_parameter_gender"].ToString();
                                getlist.gender_desc = dr["sims_appl_form_field_value1_gender"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[17].ToString()) == false)
                            {
                                getlist.legal_custody_code = dr["sims_appl_parameter_legal"].ToString();
                                getlist.legal_custody_name = dr["sims_appl_form_field_value1_legal"].ToString();
                                getlist.fee_payment_contact_pref_code = getlist.legal_custody_code;
                                getlist.fee_payment_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_code = getlist.legal_custody_code;
                            }
                            //if (string.IsNullOrEmpty(dr[19].ToString()) == false)
                            {
                                getlist.blood_group_code = dr["sims_appl_parameter_blood"].ToString();
                                getlist.blood_group_desc = dr["sims_appl_form_field_value1_blood"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[21].ToString()) == false) 
                            //Salutation Code
                            {
                                getlist.mother_salutation_code = dr["sims_appl_parameter_salut"].ToString();
                                getlist.mother_salutation_desc = dr["sims_appl_form_field_value1_salut"].ToString();
                                getlist.father_salutation_code = getlist.mother_salutation_code;
                                getlist.father_salutation_desc = getlist.mother_salutation_desc;
                                getlist.guardian_salutation_code = getlist.mother_salutation_code;
                                getlist.guardian_salutation_desc = getlist.mother_salutation_desc;
                            }
                            {
                                getlist.academic_year = dr["sims_acadmic_year"].ToString();
                                getlist.academic_year_desc = dr["sims_acadmic_year_desc"].ToString();
                                getlist.Aycur = dr["AyCur"].ToString();
                            }
                            {
                                getlist.grade_code = dr["sims_grade_code"].ToString();
                                getlist.grade_name = dr["sims_grade_name"].ToString();
                                getlist.GrCur = dr["GrCur"].ToString();
                                getlist.GrAca = dr["GrAca"].ToString();
                            }
                            {
                                getlist.section_code = dr["sims_section_code"].ToString();
                                getlist.section_name = dr["sims_section_name"].ToString();
                                getlist.SecAcy = dr["secAca"].ToString();
                                getlist.SecCur = dr["SecCur"].ToString();
                                getlist.SecGr = dr["SecGr"].ToString();
                            }
                            {
                                getlist.term_code = dr["sims_term_code"].ToString();
                                getlist.term_name = dr["sims_term_desc"].ToString();
                                getlist.termCur = dr["termCur"].ToString();
                                getlist.termAy = dr["TermAy"].ToString();
                            }
                            {
                                getlist.fee_category_code = dr["sims_fee_category_code"].ToString();
                                getlist.fee_category_desc = dr["sims_fee_category_desc"].ToString();
                            }
                            {
                                getlist.main_language_r_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_r = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_w_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_w = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_s_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_s = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                            }
                            {
                                getlist.visa_type_code = dr["sims_appl_parameter_Visa_code"].ToString();
                                getlist.visa_type_desc = dr["sims_appl_form_field_value1_Visa_type"].ToString();
                                if (!string.IsNullOrEmpty(dr["regdate"].ToString()))
                                    // getlist.admission_date = DateTime.Parse(dr["regdate"].ToString()).Year() + '-' + DateTime.Parse(dr["regdate"].ToString()).Month() + '-' + DateTime.Parse(dr["regdate"].ToString()).Day()).t;
                                    getlist.admission_date = Convert.ToDateTime(dr["regdate"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["regdate"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["regdate"].ToString()).Day.ToString();
                            }

                            try
                            {
                                getlist.sims_appl_parameter_cat_code = dr["sims_appl_parameter_cat_code"].ToString();
                                getlist.sims_appl_form_field_value1_cat_desc = dr["sims_appl_form_field_value1_cat_desc"].ToString();

                                getlist.sims_appl_parameter_marital_statuscode = dr["sims_appl_parameter_marital_statuscode"].ToString();
                                getlist.sims_appl_form_field_value1_marital_statusdesc = dr["sims_appl_form_field_value1_marital_statusdesc"].ToString();

                                getlist.sims_appl_parameter_castCatcode = dr["sims_appl_parameter_castCatcode"].ToString();
                                getlist.sims_appl_form_field_value1_castcatdesc = dr["sims_appl_form_field_value1_castcatdesc"].ToString();

                                getlist.sims_city_code = dr["sims_city_code"].ToString();
                                getlist.sims_city_state_code = dr["sims_city_state_code"].ToString();
                                getlist.sims_city_name_en = dr["sims_city_name_en"].ToString();
                                getlist.sims_state_code = dr["sims_state_code"].ToString();
                                getlist.sims_state_name_en = dr["sims_state_name_en"].ToString();

                            }
                            catch (Exception ex1)
                            {
                            }

                            //{
                            //    getlist.sims_birth_date_from = dr["sims_birth_date_from"].ToString();
                            //    getlist.sims_birth_date_to = dr["sims_birth_date_to"].ToString();
                            //}

                            listall.Add(getlist);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, listall);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }
        [Route("GetAgecompare")]
        public HttpResponseMessage GetAgecompare(string cur_code, string acad_yr, string grade)
        {
            Sims010_Edit em = new Sims010_Edit();
            List<Sims010_Edit> lstAge = new List<Sims010_Edit>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Y'),
                            new SqlParameter("@sims_admission_cur_code",cur_code),
                            new SqlParameter("@sims_admission_academic_year",acad_yr),
                            new SqlParameter("@sims_admission_grade_code",grade),
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            em.sims_birth_date_from = dr["sims_birth_date_from"].ToString();
                            em.sims_birth_date_to = dr["sims_birth_date_to"].ToString();
                            lstAge.Add(em);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lstAge);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstAge);
        }


        [Route("GetSubjectSvcc")]
        public HttpResponseMessage GetSubjectSvcc(string cur_code, string acad_yr, string grade, string adm_no)
        {

            if (adm_no == "undefined")
                adm_no = null;
            List<Sims010_Edit> lstAge = new List<Sims010_Edit>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[get_admission_subject_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_admission_number",adm_no),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit em = new Sims010_Edit();
                            em.sims_subject_code = dr["sims_subject_code"].ToString();
                            em.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            em.sims_subject_group_code = dr["sims_subject_group_code"].ToString();
                            try
                            {
                                em.sims_subject_pref = dr["sims_subject_pref"].ToString();
                                em.sims_language_desc_remark = dr["sims_language_desc_remark"].ToString();

                            }
                            catch (Exception ex) { }
                            lstAge.Add(em);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lstAge);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstAge);
        }
        [Route("GetIncomeSvcc")]
        public HttpResponseMessage GetIncomeSvcc()
        {
            List<Sims010_Edit> lstAge = new List<Sims010_Edit>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[get_admission_subject_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'B'),
                            
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit em = new Sims010_Edit();
                            em.birth_country_code = dr["sims_appl_parameter"].ToString();
                            em.birth_country = dr["sims_appl_form_field_value1"].ToString();
                            lstAge.Add(em);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lstAge);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstAge);
        }

        public Sims010_Edit GetStudent_PrevExamDetails(string admission_number)
        {

            Sims010_Edit simsobj = new Sims010_Edit();


            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M1"),
                            new SqlParameter("@ADMISSION_NUM", admission_number)
                          
                         });
                        while (dr.Read())
                        {
                            if (dr["sims_standard_code"].ToString() == "H.S.S.C.E")
                            {
                                simsobj.current_school_name = dr["sims_institute_name"].ToString();
                                simsobj.sims_admission_boardname = dr["sims_board_name"].ToString();
                                simsobj.sims_admission_center_name = dr["sims_institute_place"].ToString();
                                simsobj.sims_admission_passingyear_exam = dr["sims_yearmonth_passing"].ToString();
                                simsobj.sims_admission_seat_no = dr["sims_seat_no"].ToString();
                                simsobj.sims_admission_no_of_attempts = dr["sims_no_attempts_sem1"].ToString();
                                simsobj.sims_admission_marks_obtained = dr["sims_marks_obtained_sem1"].ToString();
                                simsobj.sims_admission_marks_out_of = dr["sims_marks_outof_sem1"].ToString();
                                simsobj.sims_admission_percentage_obtained = dr["sims_percentage_sem1"].ToString();
                            }
                            if (dr["sims_standard_code"].ToString() == "F.Y")
                            {
                                simsobj.current_fyschool_name = dr["sims_institute_name"].ToString();
                                simsobj.sims_admission_fyboardname = dr["sims_board_name"].ToString();
                                simsobj.sims_admission_fycenter_name = dr["sims_institute_place"].ToString();
                                simsobj.sims_admission_fypassingyear_exam = dr["sims_yearmonth_passing"].ToString();
                                simsobj.sims_admission_fyseat_no = dr["sims_seat_no"].ToString();
                                simsobj.sims_fyadmission_no_of_attempts_sem1 = dr["sims_no_attempts_sem1"].ToString();
                                simsobj.sims_fyadmission_marks_obtained_sem1 = dr["sims_marks_obtained_sem1"].ToString();
                                simsobj.sims_fyadmission_marks_out_of_Sem1 = dr["sims_marks_outof_sem1"].ToString();
                                simsobj.sims_fyadmission_percentage_obtained_sem1 = dr["sims_percentage_sem1"].ToString();

                                simsobj.sims_fyadmission_no_of_attempts_sem2 = dr["sims_no_attempts_sem2"].ToString();
                                simsobj.sims_fyadmission_marks_obtained_sem2 = dr["sims_marks_obtained_sem2"].ToString();
                                simsobj.sims_fyadmission_marks_out_of_Sem2 = dr["sims_marks_outeof_sem2"].ToString();
                                simsobj.sims_fyadmission_percentage_obtained_sem2 = dr["sims_percentage_sem2"].ToString();
                            }
                            if (dr["sims_standard_code"].ToString() == "S.Y")
                            {
                                simsobj.current_syschool_name = dr["sims_institute_name"].ToString();
                                simsobj.sims_admission_syboardname = dr["sims_board_name"].ToString();
                                simsobj.sims_admission_sycenter_name = dr["sims_institute_place"].ToString();
                                simsobj.sims_admission_sypassingyear_exam = dr["sims_yearmonth_passing"].ToString();
                                simsobj.sims_admission_syseat_no = dr["sims_seat_no"].ToString();
                                simsobj.sims_syadmission_no_of_attempts_sem3 = dr["sims_no_attempts_sem1"].ToString();
                                simsobj.sims_syadmission_marks_obtained_sem3 = dr["sims_marks_obtained_sem1"].ToString();
                                simsobj.sims_syadmission_marks_out_of_Sem3 = dr["sims_marks_outof_sem1"].ToString();
                                simsobj.sims_syadmission_percentage_obtained_sem3 = dr["sims_percentage_sem1"].ToString();

                                simsobj.sims_syadmission_no_of_attempts_sem4 = dr["sims_no_attempts_sem2"].ToString();
                                simsobj.sims_syadmission_marks_obtained_sem4 = dr["sims_marks_obtained_sem2"].ToString();
                                simsobj.sims_syadmission_marks_out_of_sem4 = dr["sims_marks_outeof_sem2"].ToString();
                                simsobj.sims_syadmission_percentage_obtained_sem4 = dr["sims_percentage_sem2"].ToString();
                            }
                        }


                    }
                }

            }
            catch (Exception x)
            {

            }

            return simsobj;
        }

        

    }
}
