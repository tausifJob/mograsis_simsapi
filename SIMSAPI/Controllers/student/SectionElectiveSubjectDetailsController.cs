﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.COMMON;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/sectionElectiveSubject")]
    public class SectionElectiveSubjectDetailsController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims561> mod_list = new List<Sims561>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }


        [Route("getSubject")]
        public HttpResponseMessage getSubject()
        {
            List<Sims045> lstSubject = new List<Sims045>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "SL"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims045 simsobj = new Sims045();
                            simsobj.subject_code = dr["sims_subject_code"].ToString();
                            simsobj.subject_name_en = dr["sims_subject_name_en"].ToString();

                            lstSubject.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstSubject);
        }

        [Route("getAllSectionElectiveSubject")]
        public HttpResponseMessage getAllSectionElectiveSubject()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionElectiveSubject(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getAllSectionElectiveSubject"));

            List<sims_section_elective_subject_master> sectionElectiveS_list = new List<sims_section_elective_subject_master>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_section_elective_subject_master ssesm =new sims_section_elective_subject_master();
                            ssesm.sims_language_code = dr["sims_language_code"].ToString();
                            ssesm.sims_cur_code = dr["sims_cur_code"].ToString();
                            ssesm.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            ssesm.sims_academic_year = dr["sims_academic_year"].ToString();
                            ssesm.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            ssesm.sims_language_desc = dr["sims_language_desc"].ToString();
                            ssesm.sims_language_desc_remark = dr["sims_language_desc_remark"].ToString();
                            ssesm.sims_elective_subject = dr["sims_elective_subject"].ToString();
                            ssesm.sims_compulsory_subject = dr["sims_compulsory_subject"].ToString();                            
                            ssesm.sims_status = dr["sims_status"].Equals("A") ? true : false;
                            ssesm.sims_status_chk = dr["sims_status"].ToString();
                            sectionElectiveS_list.Add(ssesm);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sectionElectiveS_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("getElectiveGrdSecSubject")]
        public HttpResponseMessage getElectiveGrdSecSubject(string langcode)
        {
            sims_section_elective_subject_master s1 = new sims_section_elective_subject_master();
            s1.gradelist = new List<string>();
            s1.subjectlist = new List<string>();
            s1.sectionlist = new List<string>();
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getElectiveGrdSecSubject(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getElectiveGrdSecSubject"));
            List<sims_section_elective_subject_master> sectionElectiveS_list = new List<sims_section_elective_subject_master>();

            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'D'),
                              new SqlParameter("@sims_language_code", langcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           // sims_section_elective_subject_master ssesm = new sims_section_elective_subject_master();
                            s1.sims_language_code = dr["sims_language_code"].ToString();
                            s1.sims_grade_code = dr["sims_grade_code"].ToString();
                            s1.sims_section_code = dr["sims_section_code"].ToString();
                            s1.sims_subject_code = dr["sims_subject_code"].ToString();
                            s1.sims_status = dr["sims_status"].Equals("A") ? true : false;
                            var g = from p in s1.gradelist where p == dr["sims_grade_code"].ToString() select p;
                            if (g.Count() == 0)
                            {
                                s1.gradelist.Add(dr["sims_grade_code"].ToString());

                            }
                            var s = from p in s1.sectionlist where p == dr["sims_grade_code"].ToString() + dr["sims_section_code"].ToString() select p;
                            if (s.Count() == 0)
                            {
                                s1.sectionlist.Add(dr["sims_grade_code"].ToString() + dr["sims_section_code"].ToString());

                            }

                            var sb = from p in s1.subjectlist where p == dr["sims_subject_code"].ToString() select p;
                            if (sb.Count() == 0)
                            {
                                s1.subjectlist.Add(dr["sims_subject_code"].ToString());

                            }

                           // sectionElectiveS_list.Add(ssesm);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, s1);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("addSectionElectiveSubject")]
        public HttpResponseMessage addSectionElectiveSubject(List<sims_section_elective_subject_master> ssesm1)
        {
            Message message = new Message();
            try
            {
                if (ssesm1 != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        foreach (sims_section_elective_subject_master ssesm in ssesm1)
                        { 
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]", 
                            new List<SqlParameter>(){
                                new SqlParameter("@opr", 'I'),
	                            new SqlParameter("@sims_cur_code",ssesm.sims_cur_code),
	                            new SqlParameter("@sims_academic_year",ssesm.sims_academic_year),
	                            new SqlParameter("@sims_language_desc",ssesm.sims_language_code),
	                            new SqlParameter("@sims_language_desc_remark",ssesm.sims_language_desc_remark),
	                            new SqlParameter("@sims_elective_subject",ssesm.sims_elective_subject),
	                            new SqlParameter("@sims_compulsory_subject",ssesm.sims_compulsory_subject),
	                            new SqlParameter("@sims_line_no",ssesm.sims_line_no),
	                            new SqlParameter("@sims_grade_code",ssesm.sims_grade_code),
	                            new SqlParameter("@sims_section_code",ssesm.sims_section_code),
	                            new SqlParameter("@sims_subject_code",ssesm.subject_code),
	                            new SqlParameter("@sims_display_order",ssesm.sims_display_order),
                                new SqlParameter("@sims_status", ssesm.sims_status== true?"A":"I"),
	                           
                         });

                        if (dr1.RecordsAffected > 0)
                        {
                            dr1.Read();
                            ssesm.sims_language_code = dr1["sims_language_code"].ToString();
                            dr1.Close();
                            dr1.Dispose();
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                foreach (sims_section_elective_subject_master sses in ssesm1)
                                {
                                    SqlDataReader dr2 = db2.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]", new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'J'),
	                                new SqlParameter("@sims_language_code",sses.sims_language_code),
	                                new SqlParameter("@sims_grade_code",sses.sims_grade_code),
	                                new SqlParameter("@sims_section_code",sses.sims_section_code),
	                                new SqlParameter("@sims_subject_code",sses.subject_code),
                                    new SqlParameter("@sims_status", ssesm.sims_status== true?"A":"I"),
                                });

                                    dr2.Close();
                                    dr2.Dispose();
                                }
                            }
                            message.strMessage = "Section Elective Subject Added Successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Section Elective Subject Already Exists";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }
                }
                }
                else
                {
                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("updateSectionElectiveSubject")]
        public HttpResponseMessage updateSectionElectiveSubject(List<sims_section_elective_subject_master> ssesm)
        {
            Message message = new Message();
            try
            {
                if (ssesm != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                      
                            SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]", new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'O'),
	                            new SqlParameter("@sims_academic_year",ssesm[0].sims_academic_year),
	                            new SqlParameter("@sims_language_desc",ssesm[0].sims_language_code),

                        });


                            if (dr1.RecordsAffected > 0)
                            {
                                dr1.Read();
                                dr1.Close();
                                dr1.Dispose();
                                using (DBConnection db2 = new DBConnection())
                                {
                                    db2.Open();
                                    foreach (sims_section_elective_subject_master s in ssesm)
                                    {
                                      SqlDataReader dr2 = db2.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", 'U'),
    	                                new SqlParameter("@sims_cur_code",s.sims_cur_code),
	                                    new SqlParameter("@sims_academic_year",s.sims_academic_year),
	                                    new SqlParameter("@sims_language_code",s.sims_language_code),
	                                    new SqlParameter("@sims_language_desc",s.sims_language_code),
	                                    new SqlParameter("@sims_language_desc_remark",s.sims_language_desc_remark),
	                                    new SqlParameter("@sims_elective_subject",s.sims_elective_subject),
	                                    new SqlParameter("@sims_compulsory_subject",s.sims_compulsory_subject),
	                                    new SqlParameter("@sims_line_no",s.sims_line_no),
	                                    new SqlParameter("@sims_grade_code",s.sims_grade_code),
	                                    new SqlParameter("@sims_section_code",s.sims_section_code),
	                                    new SqlParameter("@sims_subject_code",s.subject_code),
	                                    new SqlParameter("@sims_display_order",s.sims_display_order),	                            
                                        new SqlParameter("@sims_status", s.sims_status== true?"A":"I"),
                                    });

                                        dr2.Close();
                                        dr2.Dispose();
                                    }
                                }
                                message.strMessage = "Record Updated Successfully";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Record Not Updated";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }
                        }
                    
                }
                else
                {
                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                

            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("SectionElectiveSubjectDelete")]
        public HttpResponseMessage SectionElectiveSubjectDelete(string sims_language_code)
        {
            Message message = new Message();
            try
            {

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                            SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_section_elective_subject_proc]", new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'P'),
	                            new SqlParameter("@sims_language_code",sims_language_code),

                        });

                            if (dr1.RecordsAffected > 0)
                            {
                                dr1.Read();
                                dr1.Close();
                                dr1.Dispose();
                                message.strMessage = "Record Deleted Successfully";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Record Not Updated";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }
                        }
                    

            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

    }
}