﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;
using System.Data;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/classwiseteachermapping")]
    public class ClassWiseTeacherMappingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllClass_Name")]
        public HttpResponseMessage GetAllClass_Name(string data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllTeacher_Name(),PARAMETERS ::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllTeacher_Name", data));

            sim178 form = Newtonsoft.Json.JsonConvert.DeserializeObject<sim178>(data);

            List<sim178> mod_list = new List<sim178>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_teacher_mapping_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur",form.sims_cur_code),
                            new SqlParameter("@sims_academic_year",form.sims_academic_year)
                         });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sim178 simsobj = new sim178();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_class_teacher_code = dr["sims_class_teacher_code"].ToString();
                            simsobj.sims_class_assistant_teacher_code = dr["sims_class_assistant_teacher_code"].ToString();
                            simsobj.sims_class_teacher_status = dr["sims_class_teacher_status"].ToString();

                            simsobj.sims_supervisor_code = dr["sims_supervisor_code"].ToString();

                            try
                            {
                                simsobj.class_teacher_name = dr["class_teacher_name"].ToString();
                                simsobj.class_assistant_name = dr["class_assistant_name"].ToString();
                                simsobj.sims_supervisor_name = dr["sims_supervisor_name"].ToString();
                            }
                            catch (Exception ex)
                            {

                                throw;
                            }
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("teacherdetails")]
        public HttpResponseMessage teacherdetails(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "G";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_class_wise_teacher_mapping_proc]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }





        //[Route("IUDnsertClassTeacher")]
        //public HttpResponseMessage IUDnsertClassTeacher(List<sim178> simsobj1)    //(List<simsClass> lst)
        //{


        //    bool inserted = false;

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (sim178 simsobj in simsobj1)
        //            {
        //                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_teacher_proc]",
        //                    new List<SqlParameter>()
        //                 {

        //         new SqlParameter("@opr", simsobj.opr),
        //         new SqlParameter("@sims_cur", simsobj.sims_cur_code),
        //         new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
        //         new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
        //         new SqlParameter("@sims_section_code", simsobj.sims_section_code),
        //         new SqlParameter("@sims_class_teacher_code", simsobj.sims_teacher_code),
        //          });

        //                if (dr.RecordsAffected > 0)
        //                {
        //                    inserted = true;

        //                }
        //                dr.Close();
        //            }


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, e.Message);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}

        [Route("InsertUpdateClassTeacher")]
        public HttpResponseMessage InsertUpdateClassTeacher(List<sim178> simsobj)    //(List<simsClass> lst)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertUpdateClassTeacher(),PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "STUDENT", "InsertUpdateClassTeacher", simsobj));

            Message message = new Message();

            bool inserted = false;

            for (int i = 0; i < simsobj.Count; i++)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_teacher_mapping_proc]",
                            new List<SqlParameter>()
                         {

                 new SqlParameter("@opr", simsobj[i].opr),
                 new SqlParameter("@sims_cur", simsobj[i].sims_cur_code),
                 new SqlParameter("@sims_academic_year", simsobj[i].sims_academic_year),
                 new SqlParameter("@sims_grade_code", simsobj[i].sims_grade_code),
                 new SqlParameter("@sims_section_code", simsobj[i].sims_section_code),
                 new SqlParameter("@sims_class_teacher_code", simsobj[i].sims_teacher_code),
                 new SqlParameter("@sims_teacher_assi_code", simsobj[i].sims_teacher_assi_code),
                 new SqlParameter("@sims_teacher_super_code", simsobj[i].sims_teacher_super_code),
                  });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;

                        }
                        else
                        {

                            inserted = false;
                        }
                    }


                }

                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //[Route("CheckExist")]
        //public HttpResponseMessage CheckExist(sim178 simsobj)    //(List<simsClass> lst)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllTeacher_Name(),PARAMETERS ::data{2}";
        //    Log.Debug(string.Format(debug, "STUDENT", "GetAllTeacher_Name", simsobj));

        //    Message message = new Message();
        //    List<isexistteacher> teacher = new List<isexistteacher>();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_teacher_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                 new SqlParameter("@opr", simsobj.opr),
        //                 new SqlParameter("@sims_cur", simsobj.sims_cur_code),
        //                 new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
        //                 new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
        //                 new SqlParameter("@sims_section_code", simsobj.sims_section_code),
        //                 new SqlParameter("@sims_class_teacher_code", simsobj.sims_teacher_code),

        //              });

        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    isexistteacher obj = new isexistteacher();
        //                    obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
        //                    obj.sims_section_name = dr["sims_section_name_en"].ToString();
        //                    obj.sims_teacher_code = dr["sims_teacher_code"].ToString();
        //                    obj.sims_teacher_name = dr["sims_teacher_name"].ToString();
        //                    obj.sims_grade_code = dr["sims_grade_code"].ToString();
        //                    obj.sims_section_code = dr["sims_section_code"].ToString();
        //                    obj.isexist = true;

        //                    teacher.Add(obj);
        //                }
        //            }
        //            else
        //            {
        //                isexistteacher obj = new isexistteacher();
        //                obj.isexist = false;
        //                teacher.Add(obj);
        //            }

        //        }

        //        message.systemMessage = string.Empty;
        //        message.messageType = MessageType.Success;

        //        return Request.CreateResponse(HttpStatusCode.OK, teacher);
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, teacher);
        //}
    }
}