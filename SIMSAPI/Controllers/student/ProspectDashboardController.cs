﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using System.Text.RegularExpressions;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/ProspectDashboard")]


    //waitlist dashboard
    public class ProspectDashboardController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("GetProspectDetail")]
        public HttpResponseMessage GetProspectDetail(string curr_code, string AcadmicYear, string gradeCode, string admission_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";
            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (gradeCode == "null" || gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear))////|| string.IsNullOrEmpty(gradeCode)
                    str = "G";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", str),
                             new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.pros_num = dr["sims_pros_number"].ToString();

                            try
                            {
                                simsobj.pros_num1 = int.Parse(Regex.Match(dr["sims_pros_number"].ToString(), @"\d+").Value);
                            }
                            catch (Exception ex) { }
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_pros_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_pros_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_pros_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_pros_gender"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["sims_pros_term_code"].ToString();
                            simsobj.birth_date = db.DBYYYYMMDDformat(dr["dob"].ToString());
                            simsobj.communication_date = db.DBYYYYMMDDformat(dr["communication_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["communication_date"].ToString()))
                                simsobj.communication_date1 = DateTime.Parse(dr["communication_date"].ToString());
                            try
                            {
                                simsobj.pref_date = db.DBYYYYMMDDformat(dr["sims_book_tour_preferred_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_book_tour_preferred_date"].ToString()))
                                    simsobj.pref_date1 = DateTime.Parse(dr["sims_book_tour_preferred_date"].ToString());

                            }
                            catch (Exception ex) { }
                            simsobj.age = dr["sims_pros_dob"].ToString();
                            simsobj.sims_pros_national_id = dr["sims_pros_national_id"].ToString();
                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.pros_date = db.DBYYYYMMDDformat(dr["Pros_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["Pros_date"].ToString()))

                                simsobj.pros_date1 = DateTime.Parse(dr["Pros_date"].ToString());

                            simsobj.FatherName = dr["FatherName"].ToString();
                            try
                            {
                                simsobj.Father_email = dr["sims_pros_father_email"].ToString();
                                simsobj.Mother_email = dr["sims_pros_mother_email"].ToString();
                                simsobj.Guardian_email = dr["sims_pros_guardian_email"].ToString();
                                simsobj.father_mobile = dr["sims_pros_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                                simsobj.sims_pros_attribute1 = dr["sims_pros_attribute1"].ToString();

                            }
                            catch (Exception ex)
                            {
                            }
                            simsobj.quota_code = dr["quota_code"].ToString();
                            simsobj.sims_admission_quota_code = dr["sims_admission_quota_code"].ToString();
                            simsobj.sims_admission_parent_REG_id = dr["sims_pros_parent_REG_id"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();

                            try
                            {
                                simsobj.sims_follow_up_date = db.DBYYYYMMDDformat(dr["sims_follow_up_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_follow_up_date"].ToString()))
                                    simsobj.sims_follow_up_date1 = DateTime.Parse(dr["sims_follow_up_date"].ToString());
                                simsobj.sims_pros_marketing_description = dr["sims_pros_marketing_description"].ToString();
                                simsobj.sims_ename = dr["sims_ename"].ToString();


                            }

                            catch (Exception ex) { }

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }
      
        [Route("GetProspectDetail")]
        public HttpResponseMessage GetProspectDetail(string curr_code, string AcadmicYear, string gradeCode, string admission_status,string pco)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";
            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (gradeCode == "null" || gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                if (pco == "null" || pco == "undefined")
                {
                    pco = null;
                }

                

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear))////|| string.IsNullOrEmpty(gradeCode)
                    str = "G";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", str),
                             new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                             new SqlParameter("@pros_nos", pco)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.pros_num = dr["sims_pros_number"].ToString();

                            try
                            {
                                simsobj.pros_num1 = int.Parse(Regex.Match(dr["sims_pros_number"].ToString(), @"\d+").Value);
                            }
                            catch (Exception ex) { }
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_pros_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_pros_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_pros_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_pros_gender"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["sims_pros_term_code"].ToString();
                            simsobj.birth_date = db.DBYYYYMMDDformat(dr["dob"].ToString());
                            simsobj.communication_date = db.DBYYYYMMDDformat(dr["communication_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["communication_date"].ToString()))
                                simsobj.communication_date1 = DateTime.Parse(dr["communication_date"].ToString());
                            try
                            {
                                simsobj.pref_date = db.DBYYYYMMDDformat(dr["sims_book_tour_preferred_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_book_tour_preferred_date"].ToString()))
                                    simsobj.pref_date1 = DateTime.Parse(dr["sims_book_tour_preferred_date"].ToString());

                            }
                            catch (Exception ex) { }
                            simsobj.age = dr["sims_pros_dob"].ToString();
                            simsobj.sims_pros_national_id = dr["sims_pros_national_id"].ToString();
                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.pros_date = db.DBYYYYMMDDformat(dr["Pros_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["Pros_date"].ToString()))

                                simsobj.pros_date1 = DateTime.Parse(dr["Pros_date"].ToString());

                            simsobj.FatherName = dr["FatherName"].ToString();
                            try
                            {
                                simsobj.Father_email = dr["sims_pros_father_email"].ToString();
                                simsobj.Mother_email = dr["sims_pros_mother_email"].ToString();
                                simsobj.Guardian_email = dr["sims_pros_guardian_email"].ToString();
                                simsobj.father_mobile = dr["sims_pros_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                                simsobj.sims_pros_attribute1 = dr["sims_pros_attribute1"].ToString();

                            }
                            catch (Exception ex)
                            {
                            }
                            simsobj.quota_code = dr["quota_code"].ToString();
                            simsobj.sims_admission_quota_code = dr["sims_admission_quota_code"].ToString();
                            simsobj.sims_admission_parent_REG_id = dr["sims_pros_parent_REG_id"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();

                            try
                            {
                                simsobj.sims_follow_up_date = db.DBYYYYMMDDformat(dr["sims_follow_up_date"].ToString());
                                if (!string.IsNullOrEmpty(dr["sims_follow_up_date"].ToString()))
                                    simsobj.sims_follow_up_date1 = DateTime.Parse(dr["sims_follow_up_date"].ToString());
                                simsobj.sims_pros_marketing_description = dr["sims_pros_marketing_description"].ToString();
                                simsobj.sims_ename = dr["sims_ename"].ToString();


                            }

                            catch (Exception ex) { }

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }
        //view data in Reregistration Dashboard
        [Route("GetTabStudentData")]
        public HttpResponseMessage GetTabStudentData(string prospect_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTabStudentData()PARAMETERS ::admission_number{2}";
            Log.Debug(string.Format(debug, "ERP/Admission/", "GetTabStudentData", prospect_number));

            Sims010_Edit simsobj = new Sims010_Edit();
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@CURR_CODE", ""),
                            new SqlParameter("@ACA_YEAR", ""),
                            new SqlParameter("@GRADE_CODE", ""),
                            new SqlParameter("@ADMISSION_NUM", prospect_number)
                          
                         });
                        while (dr.Read())
                        {
                            #region
                            simsobj.school_code = dr["sims_pros_school_code"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.pros_appl_num = dr["sims_pros_application_number"].ToString();
                            simsobj.behaviour_desc = dr["sims_pros_behaviour_desc"].ToString();
                            if (dr["sims_pros_behaviour_status"].ToString() == "1")
                            {
                                simsobj.behaviour_status = true;
                            }
                            else
                            {
                                simsobj.behaviour_status = false;
                            }
                            simsobj.birth_country_code = dr["sims_pros_birth_country_code"].ToString();
                            simsobj.blood_group_code = dr["sims_pros_blood_group_code"].ToString();
                            //try
                            //{
                            //    simsobj.comm_date = dr["sims_admission_commencement_date"].ToString();
                            //}
                            //catch (Exception ex)
                            //{
                            //    simsobj.comm_date = "";
                            //}
                            simsobj.current_school_address = dr["sims_pros_current_school_address"].ToString();
                            simsobj.current_school_city = dr["sims_pros_current_school_city"].ToString();
                            simsobj.current_school_cur = dr["sims_pros_current_school_cur"].ToString();
                            simsobj.current_school_enroll_number = dr["sims_pros_current_school_enroll_number"].ToString();
                            simsobj.current_school_fax = dr["sims_pros_current_school_fax"].ToString();
                            try
                            {
                                simsobj.current_school_from_date = db.DBYYYYMMDDformat(dr["sims_pros_current_school_from_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.current_school_from_date = "";
                            }
                            simsobj.current_school_grade = dr["sims_pros_current_school_grade"].ToString();
                            simsobj.current_school_head_teacher = dr["sims_pros_current_school_head_teacher"].ToString();
                            simsobj.current_school_language = dr["sims_pros_current_school_language"].ToString();
                            simsobj.current_school_name = dr["sims_pros_current_school_name"].ToString();
                            simsobj.current_school_phone = dr["sims_pros_current_school_phone"].ToString();
                            if (dr["sims_pros_current_school_status"].ToString() == "1")
                            {
                                simsobj.current_school_status = true;
                            }
                            else
                            {
                                simsobj.current_school_status = false;
                            }

                            simsobj.current_school_to_date = db.DBYYYYMMDDformat(dr["sims_pros_current_school_to_date"].ToString());
                            simsobj.current_school_country = dr["current_school_country"].ToString();
                            simsobj.current_school_country_code = dr["sims_pros_current_school_country_code"].ToString();
                            if (dr["sims_pros_declaration_status"].ToString() == "1")
                            {
                                simsobj.declaration_status = true;
                            }
                            else
                            {
                                simsobj.declaration_status = false;
                            }
                            simsobj.disability_desc = dr["sims_pros_disability_desc"].ToString();
                            if (dr["sims_pros_disability_status"].ToString() == "1")
                            {
                                simsobj.disability_status = true;
                            }
                            else
                            {
                                simsobj.disability_status = false;
                            }
                            simsobj.dns = dr["sims_pros_dns"].ToString();
                            //simsobj.employee_comp_code = dr["sims_admission_employee_comp_code"].ToString();
                            //simsobj.employee_school_code = dr["sims_admission_employee_school_code"].ToString();
                            //if (dr["sims_pros_employee_type"].ToString() == "1")
                            //{
                            //    simsobj.employee_type = true;
                            //}
                            //else
                            //{
                            //    simsobj.employee_type = false;
                            //}
                            simsobj.family_name_ot = dr["sims_pros_family_name_ot"].ToString();
                            simsobj.ethinicity_code = dr["sims_pros_ethnicity_code"].ToString();
                            simsobj.family_name = dr["sims_pros_family_name_en"].ToString();
                            simsobj.father_appartment_number = dr["sims_pros_father_appartment_number"].ToString();
                            simsobj.father_area_number = dr["sims_pros_father_area_number"].ToString();
                            simsobj.father_building_number = dr["sims_pros_father_building_number"].ToString();
                            simsobj.father_city = dr["sims_pros_father_city"].ToString();
                            simsobj.father_company = dr["sims_pros_father_company"].ToString();
                            simsobj.father_country_code = dr["sims_pros_father_country_code"].ToString();
                            simsobj.father_email = dr["sims_pros_father_email"].ToString();
                            simsobj.father_family_name = dr["sims_pros_father_family_name"].ToString();
                            simsobj.father_fax = dr["sims_pros_father_fax"].ToString();
                            simsobj.father_first_name = dr["sims_pros_father_first_name"].ToString();
                            simsobj.father_last_name = dr["sims_pros_father_last_name"].ToString();
                            simsobj.father_middle_name = dr["sims_pros_father_middle_name"].ToString();
                            simsobj.father_mobile = dr["sims_pros_father_mobile"].ToString();
                            simsobj.father_name_ot = dr["sims_pros_father_name_ot"].ToString();
                            simsobj.father_nationality1_code = dr["sims_pros_father_nationality1_code"].ToString();
                            simsobj.father_nationality2_code = dr["sims_pros_father_nationality2_code"].ToString();
                            simsobj.father_occupation = dr["sims_pros_father_occupation"].ToString();
                            simsobj.father_passport_number = dr["sims_pros_father_passport_number"].ToString();
                            simsobj.father_phone = dr["sims_pros_father_phone"].ToString();
                            simsobj.father_po_box = dr["sims_pros_father_po_box"].ToString();
                            simsobj.father_salutation_code = dr["sims_pros_father_salutation_code"].ToString();
                            simsobj.father_state = dr["sims_pros_father_state"].ToString();
                            simsobj.father_street_number = dr["sims_pros_father_street_number"].ToString();
                            simsobj.father_summary_address = dr["sims_pros_father_summary_address"].ToString();
                            simsobj.fee_payment_contact_pref_code = dr["sims_pros_fee_payment_contact_pref"].ToString();
                            //if (dr["sims_admission_fees_paid_status"].ToString() == "1")
                            //{
                            //    simsobj.fees_paid_status = true;
                            //}
                            //else
                            //{
                            //    simsobj.fees_paid_status = false;
                            //}
                            simsobj.first_name = dr["sims_pros_passport_first_name_en"].ToString();
                            simsobj.first_name_ot = dr["sims_pros_passport_first_name_ot"].ToString();
                            simsobj.gifted_desc = dr["sims_pros_gifted_desc"].ToString();
                            if (dr["sims_pros_gifted_status"].ToString() == "1")
                            {
                                simsobj.gifted_status = true;
                            }
                            else
                            {
                                simsobj.gifted_status = false;
                            }
                            simsobj.guardian_appartment_number = dr["sims_pros_guardian_appartment_number"].ToString();
                            simsobj.guardian_area_number = dr["sims_pros_guardian_area_number"].ToString();
                            simsobj.guardian_building_number = dr["sims_pros_guardian_building_number"].ToString();
                            simsobj.guardian_city = dr["sims_pros_guardian_city"].ToString();
                            simsobj.guardian_company = dr["sims_pros_guardian_company"].ToString();
                            simsobj.guardian_country_code = dr["sims_pros_guardian_country_code"].ToString();
                            simsobj.guardian_email = dr["sims_pros_guardian_email"].ToString();
                            simsobj.guardian_family_name = dr["sims_pros_guardian_family_name"].ToString();
                            simsobj.guardian_fax = dr["sims_pros_guardian_fax"].ToString();
                            simsobj.guardian_first_name = dr["sims_pros_guardian_first_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_pros_guardian_last_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_pros_guardian_middle_name"].ToString();
                            simsobj.guardian_mobile = dr["sims_pros_guardian_mobile"].ToString();
                            simsobj.guardian_name_ot = dr["sims_pros_guardian_name_ot"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_pros_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality2_code = dr["sims_pros_guardian_nationality2_code"].ToString();
                            simsobj.guardian_occupation = dr["sims_pros_guardian_occupation"].ToString();
                            simsobj.guardian_passport_number = dr["sims_pros_guardian_passport_number"].ToString();
                            simsobj.guardian_phone = dr["sims_pros_guardian_phone"].ToString();
                            simsobj.guardian_po_box = dr["sims_pros_guardian_po_box"].ToString();
                            simsobj.guardian_salutation_code = dr["sims_pros_guardian_salutation_code"].ToString();
                            simsobj.guardian_state = dr["sims_pros_guardian_state"].ToString();
                            simsobj.guardian_street_number = dr["sims_pros_guardian_street_number"].ToString();
                            simsobj.guardian_summary_address = dr["sims_pros_guardian_summary_address"].ToString();
                            try
                            {
                                simsobj.health_card_expiry_date = db.DBYYYYMMDDformat(dr["sims_pros_health_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_expiry_date = "";
                            }
                            try
                            {
                                simsobj.health_card_issue_date = db.DBYYYYMMDDformat(dr["sims_pros_health_card_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_issue_date = "";
                            }
                            simsobj.health_card_issuing_authority = dr["sims_pros_health_card_issuing_authority"].ToString();
                            simsobj.health_card_number = dr["sims_pros_health_card_number"].ToString();
                            simsobj.health_hearing_desc = dr["sims_pros_health_hearing_desc"].ToString();
                            if (dr["sims_pros_health_hearing_status"].ToString() == "1")
                            {
                                simsobj.health_hearing_status = true;
                            }
                            else
                            {
                                simsobj.health_hearing_status = false;
                            }
                            simsobj.health_other_desc = dr["sims_pros_health_other_desc"].ToString();
                            if (dr["sims_pros_health_other_status"].ToString() == "1")
                            {
                                simsobj.health_other_status = true;
                            }
                            else
                            {
                                simsobj.health_other_status = false;
                            }
                            simsobj.health_restriction_desc = dr["sims_pros_health_restriction_desc"].ToString();
                            if (dr["sims_pros_health_restriction_status"].ToString() == "1")
                            {
                                simsobj.health_restriction_status = true;
                            }
                            else
                            {
                                simsobj.health_restriction_status = false;
                            }
                            simsobj.health_vision_desc = dr["sims_pros_health_vision_desc"].ToString();
                            if (dr["sims_pros_health_vision_status"].ToString() == "1")
                            {
                                simsobj.health_vision_status = true;
                            }
                            else
                            {
                                simsobj.health_vision_status = false;
                            }
                            simsobj.last_name = dr["sims_pros_passport_last_name_en"].ToString();
                            simsobj.last_name_ot = dr["sims_pros_passport_last_name_ot"].ToString();
                            simsobj.language_support_desc = dr["sims_pros_language_support_desc"].ToString();
                            if (dr["sims_pros_language_support_status"].ToString() == "1")
                            {
                                simsobj.language_support_status = true;
                            }
                            else
                            {
                                simsobj.language_support_status = false;
                            }
                            simsobj.legal_custody_code = dr["sims_pros_legal_custody"].ToString();
                            simsobj.main_language_code = dr["sims_pros_main_language_code"].ToString();
                            simsobj.main_language_r_code = dr["sims_pros_main_language_r"].ToString();
                            simsobj.main_language_s_code = dr["sims_pros_main_language_s"].ToString();
                            simsobj.main_language_w_code = dr["sims_pros_main_language_w"].ToString();
                            simsobj.other_language_code = dr["sims_pros_other_language"].ToString();
                            if (dr["sims_pros_marketing_code"].ToString() == "1")
                            {
                                simsobj.marketing_code = true;
                            }
                            else
                            {
                                simsobj.marketing_code = false;
                            }
                            simsobj.marketing_description = dr["sims_pros_marketing_description"].ToString();
                            simsobj.medication_desc = dr["sims_pros_medication_desc"].ToString();
                            if (dr["sims_pros_medication_status"].ToString() == "1")
                            {
                                simsobj.medication_status = true;
                            }
                            else
                            {
                                simsobj.medication_status = false;
                            }
                            simsobj.middle_name = dr["sims_pros_passport_middle_name_en"].ToString();
                            simsobj.midd_name_ot = dr["sims_pros_passport_middle_name_ot"].ToString();
                            simsobj.motherTounge_language_code = dr["sims_pros_main_language_m"].ToString();
                            simsobj.mother_appartment_number = dr["sims_pros_mother_appartment_number"].ToString();
                            simsobj.mother_area_number = dr["sims_pros_mother_area_number"].ToString();
                            simsobj.mother_building_number = dr["sims_pros_mother_building_number"].ToString();
                            simsobj.mother_city = dr["sims_pros_mother_city"].ToString();
                            simsobj.mother_company = dr["sims_pros_mother_company"].ToString();
                            simsobj.mother_country_code = dr["sims_pros_mother_country_code"].ToString();
                            simsobj.mother_email = dr["sims_pros_mother_email"].ToString();
                            simsobj.mother_family_name = dr["sims_pros_mother_family_name"].ToString();
                            simsobj.mother_fax = dr["sims_pros_mother_fax"].ToString();
                            simsobj.mother_first_name = dr["sims_pros_mother_first_name"].ToString();
                            simsobj.mother_last_name = dr["sims_pros_mother_last_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_pros_mother_middle_name"].ToString();
                            simsobj.mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                            simsobj.mother_name_ot = dr["sims_pros_mother_name_ot"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_pros_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality2_code = dr["sims_pros_mother_nationality2_code"].ToString();
                            simsobj.mother_occupation = dr["sims_pros_mother_occupation"].ToString();
                            simsobj.mother_passport_number = dr["sims_pros_mother_passport_number"].ToString();
                            simsobj.mother_phone = dr["sims_pros_mother_phone"].ToString();
                            simsobj.mother_po_box = dr["sims_pros_mother_po_box"].ToString();
                            simsobj.mother_salutation_code = dr["sims_pros_mother_salutation_code"].ToString();
                            simsobj.mother_state = dr["sims_pros_mother_state"].ToString();
                            simsobj.mother_street_number = dr["sims_pros_mother_street_number"].ToString();
                            simsobj.mother_summary_address = dr["sims_pros_mother_summary_address"].ToString();
                            simsobj.music_desc = dr["sims_pros_music_desc"].ToString();
                            if (dr["sims_pros_music_status"].ToString() == "1")
                            {
                                simsobj.music_status = true;
                            }
                            else
                            {
                                simsobj.music_status = false;
                            }
                            simsobj.national_id = dr["sims_pros_national_id"].ToString();
                            try
                            {
                                simsobj.national_id_issue_date = db.DBYYYYMMDDformat(dr["sims_pros_national_id_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_issue_date = "";
                            }

                            try
                            {
                                simsobj.national_id_expiry_date = db.DBYYYYMMDDformat(dr["sims_pros_national_id_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_expiry_date = "";
                            }
                            simsobj.nicke_name = dr["sims_pros_nickname"].ToString();
                            simsobj.parent_id = dr["sims_pros_parent_id"].ToString();
                            if (dr["sims_pros_parent_status_code"].ToString() == "1")
                            {
                                simsobj.parent_status_code = true;
                            }
                            else
                            {
                                simsobj.parent_status_code = false;
                            }
                            try
                            {
                                simsobj.passport_expiry = db.DBYYYYMMDDformat(dr["sims_pros_passport_expiry_date"].ToString());
                                simsobj.passport_expiry_n = db.DBYYYYMMDDformat(dr["sims_pros_passport_expiry_date"].ToString());

                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_expiry = "";
                            }
                            simsobj.passport_issue_auth = dr["sims_pros_passport_issuing_authority"].ToString();
                            try
                            {
                                simsobj.passport_issue_date = db.DBYYYYMMDDformat(dr["sims_pros_passport_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_issue_date = "";
                            }
                            simsobj.passport_issue_place = dr["sims_pros_passport_issue_place"].ToString();
                            simsobj.passport_num = dr["sims_pros_passport_number"].ToString();
                            simsobj.primary_contact_pref_code = dr["sims_pros_primary_contact_pref"].ToString();
                            if (dr["sims_pros_primary_contact_code"].ToString() == "1")
                            {
                                simsobj.primary_contact_code = true;
                            }
                            else
                            {
                                simsobj.primary_contact_code = false;
                            }
                            simsobj.primary_contact_pref_desc = dr["primary_contact_pref_desc"].ToString();
                            simsobj.pros_appl_num = dr["sims_pros_application_number"].ToString();
                            simsobj.religion_code = dr["sims_pros_religion_code"].ToString();
                            simsobj.sibling_dob = dr["sims_pros_sibling_dob"].ToString();
                            simsobj.sibling_name = dr["sims_pros_sibling_name"].ToString();
                            simsobj.sibling_school_code = dr["sims_pros_sibling_school_code"].ToString();
                            simsobj.sibling_school_name = dr["sibSchoolName"].ToString();
                            simsobj.sports_desc = dr["sims_pros_sports_desc"].ToString();
                            if (dr["sims_pros_sports_status"].ToString() == "1")
                            {
                                simsobj.sports_status = true;
                            }
                            else
                            {
                                simsobj.sports_status = false;
                            }
                            simsobj.status = dr["sims_pros_status"].ToString();
                            // simsobj.section_code = dr["sims_pros_section_code"].ToString();
                            simsobj.user_code = dr["sims_pros_user_code"].ToString();
                            try
                            {
                                simsobj.visa_expiry_date = db.DBYYYYMMDDformat(dr["sims_pros_visa_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_expiry_date = "";
                            }

                            try
                            {
                                simsobj.visa_issue_date = db.DBYYYYMMDDformat(dr["sims_pros_visa_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_issue_date = "";
                            }
                            simsobj.visa_issuing_authority = dr["sims_pros_visa_issuing_authority"].ToString();
                            simsobj.visa_issuing_place = dr["sims_pros_visa_issuing_place"].ToString();
                            simsobj.visa_number = dr["sims_pros_visa_number"].ToString();
                            simsobj.visa_type_desc = dr["sims_pros_visa_type_desc"].ToString();
                            simsobj.visa_type = dr["sims_pros_visa_type"].ToString();
                            if (dr["sims_pros_transport_status"].ToString() == "1")
                            {
                                simsobj.transport_status = true;
                            }
                            else
                            {
                                simsobj.transport_status = false;
                            }
                            simsobj.transport_desc = dr["sims_pros_transport_desc"].ToString();
                            simsobj.ip = dr["sims_pros_ip"].ToString();
                            #endregion
                            #region
                            simsobj.sims_student_attribute1 = dr["sims_pros_attribute1"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_pros_attribute2"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_pros_attribute3"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_pros_attribute4"].ToString();

                            simsobj.sims_student_health_respiratory_status = dr["sims_pros_health_respiratory_status"].ToString();
                            simsobj.sims_student_health_respiratory_desc = dr["sims_pros_health_respiratory_desc"].ToString();
                            simsobj.sims_student_health_hay_fever_status = dr["sims_pros_health_hay_fever_status"].ToString();
                            simsobj.sims_student_health_hay_fever_desc = dr["sims_pros_health_hay_fever_desc"].ToString();
                            simsobj.sims_student_health_epilepsy_status = dr["sims_pros_health_epilepsy_status"].ToString();
                            simsobj.sims_student_health_epilepsy_desc = dr["sims_pros_health_epilepsy_desc"].ToString();
                            simsobj.sims_student_health_skin_status = dr["sims_pros_health_skin_status"].ToString();
                            simsobj.sims_student_health_skin_desc = dr["sims_pros_health_skin_desc"].ToString();
                            simsobj.sims_student_health_diabetes_status = dr["sims_pros_health_diabetes_status"].ToString();
                            simsobj.sims_student_health_diabetes_desc = dr["sims_pros_health_diabetes_desc"].ToString();
                            simsobj.sims_student_health_surgery_status = dr["sims_pros_health_surgery_status"].ToString();
                            simsobj.sims_student_health_surgery_desc = dr["sims_pros_health_surgery_desc"].ToString();


                            simsobj.admission_number = prospect_number;
                            simsobj.stud_full_name = simsobj.first_name + " " + simsobj.middle_name + " " + simsobj.last_name;// o.stud_full_name;
                            //simsobj.fee_category_code = dr["sims_admission_fee_category"].ToString();
                            try
                            {
                                simsobj.admission_date = db.DBYYYYMMDDformat(dr["sims_pros_date_created"].ToString());//o.admission_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.admission_date = "";
                            }
                            simsobj.pros_num = prospect_number;
                            simsobj.school_code = dr["sims_pros_school_code"].ToString();
                            simsobj.term_code = dr["sims_pros_term_code"].ToString();
                            try
                            {
                                simsobj.tent_join_date = db.DBYYYYMMDDformat(dr["tent_join_date"].ToString());//o.tent_join_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.tent_join_date = "";
                            }
                            simsobj.gender_desc = dr["gender"].ToString();// o.gender;
                            simsobj.gender_code = dr["sims_pros_gender"].ToString();
                            try
                            {
                                simsobj.birth_date = db.DBYYYYMMDDformat(dr["dob"].ToString());//o.birth_date;
                                simsobj.sims_admission_mother_national_id = dr["sims_pros_mother_national_id"].ToString();

                            }
                            catch (Exception ex)
                            {
                                simsobj.birth_date = "";
                            }
                            simsobj.curr_code = dr["sims_pros_cur_code"].ToString();
                            simsobj.academic_year_desc = dr["academic_year_desc"].ToString();
                            simsobj.academic_year = dr["sims_pros_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_pros_grade_code"].ToString();
                            simsobj.nationality_code = dr["sims_pros_nationality_code"].ToString();//o.nation;
                            if (dr["sibling_status"].ToString() == "1")
                            {
                                simsobj.sibling_status = true;
                            }
                            else
                            {
                                simsobj.sibling_status = false;
                            }
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            simsobj.quota_code = dr["quota_code"].ToString();
                            simsobj.sims_admission_quota_code = dr["sims_admission_quota_code"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.employee_code = dr["sims_parent_is_employment_number"].ToString();
                            simsobj.reason = dr["sims_pros_attribute1"].ToString();
                            simsobj.sims_admission_father_national_id = dr["sims_pros_father_national_id"].ToString();
                            simsobj.category_code = dr["sims_student_category"].ToString();
                            #endregion
                            // simsobj.employee_code = dr["emp_code"].ToString();
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("CUDUpdateProspectAdmission")]
        public HttpResponseMessage CUDUpdateProspectAdmission(List<Sims010_Edit> simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateProspectAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateProspectAdmission"));
            Sims010_Edit res = new Sims010_Edit();

            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit data in simsobj)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospects_proc]",
                                new List<SqlParameter>() 
                         { 
                           
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@sims_admission_main_language_m", data.motherTounge_language_code),
                            //new SqlParameter("@sims_admission_application_number", data.appl_num),
                            new SqlParameter("@sims_admission_number", data.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", data.pros_appl_num),
                            new SqlParameter("@sims_admission_date",db.DBYYYYMMDDformat(data.admission_date)),
                            #region
                            new SqlParameter("@sims_admission_school_code", data.school_code),
                            new SqlParameter("@sims_admission_cur_code", data.curr_code),
                            new SqlParameter("@sims_admission_academic_year", data.academic_year),
                            new SqlParameter("@sims_admission_grade_code", data.grade_code),
                          
                            new SqlParameter("@sims_admission_term_code", data.term_code),
                            new SqlParameter("@sims_admission_tentative_joining_date",db.DBYYYYMMDDformat(data.tent_join_date)),
                
                            new SqlParameter("@sims_admission_passport_first_name_en", data.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", data.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", data.last_name),
                            new SqlParameter("@sims_admission_family_name_en", data.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", data.first_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", data.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", data.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", data.family_name_ot),

                            new SqlParameter("@sims_pros_passport_first_name_ot", data.first_name_ot),
                            new SqlParameter("@sims_pros_passport_middle_name_ot", data.midd_name_ot),
                            new SqlParameter("@sims_pros_passport_last_name_ot", data.last_name_ot),
                            new SqlParameter("@sims_pros_family_name_ot", data.family_name_ot),

                            new SqlParameter("@sims_admission_nickname", data.nicke_name),
                            new SqlParameter("@sims_admission_dob",db.DBYYYYMMDDformat(data.birth_date)),
                            new SqlParameter("@sims_admission_birth_country_code", data.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", data.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", data.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", data.gender_code),
                            new SqlParameter("@sims_admission_religion_code", data.religion_code),
                            new SqlParameter("@sims_admission_passport_number", data.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date",db.DBYYYYMMDDformat(data.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date",db.DBYYYYMMDDformat(data.passport_expiry)),
                            new SqlParameter("@sims_admission_passport_issuing_authority", data.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", data.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", data.visa_number),
                            new SqlParameter("@sims_admission_visa_type", data.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", data.visa_issuing_authority),
                            new SqlParameter("@sims_admission_visa_issue_date", db.DBYYYYMMDDformat(data.visa_issue_date)),
                            new SqlParameter("@sims_admission_visa_expiry_date",db.DBYYYYMMDDformat(data.visa_expiry_date)),
                            new SqlParameter("@sims_admission_visa_issuing_place", data.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", data.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date", db.DBYYYYMMDDformat(data.national_id_issue_date)),
                            new SqlParameter("@sims_admission_national_id_expiry_date",db.DBYYYYMMDDformat(data.national_id_expiry_date)),
                            new SqlParameter("@sims_admission_sibling_status", data.sibling_status),
                            new SqlParameter("@sims_admission_parent_id", data.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", data.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", data.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",db.DBYYYYMMDDformat(data.sibling_dob)),
                            new SqlParameter("@sims_admission_sibling_school_code", data.sibling_school_code),
                            new SqlParameter("@sims_admission_main_language_code", data.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", data.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", data.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", data.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", data.primary_contact_code),
                            new SqlParameter("@sims_admission_primary_contact_pref", data.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", data.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", data.transport_status),
                            new SqlParameter("@sims_admission_transport_desc", data.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", data.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", data.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", data.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", data.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", data.family_name),
                            new SqlParameter("@sims_admission_father_name_ot", data.family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", data.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", data.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", data.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", data.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", data.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", data.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", data.father_city),
                            new SqlParameter("@sims_admission_father_state_name", data.father_state),
                            new SqlParameter("@sims_admission_father_country_code", data.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", data.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", data.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", data.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", data.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", data.father_fax),
                            new SqlParameter("@sims_admission_father_email", data.father_email),
                            new SqlParameter("@sims_admission_father_occupation", data.father_occupation),
                            new SqlParameter("@sims_admission_father_company", data.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", data.father_passport_number),
                            new SqlParameter("@sims_admission_guardian_salutation_code", data.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", data.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", data.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", data.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", data.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", data.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", data.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", data.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", data.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", data.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", data.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", data.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", data.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", data.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", data.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", data.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", data.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", data.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", data.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", data.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", data.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", data.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", data.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", data.passport_num),
                            new SqlParameter("@sims_admission_mother_salutation_code", data.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", data.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", data.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", data.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", data.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", data.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", data.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", data.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", data.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", data.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", data.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", data.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", data.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", data.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", data.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", data.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", data.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", data.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", data.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", data.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", data.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", data.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", data.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", data.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", data.current_school_status),
                            new SqlParameter("@sims_admission_current_school_name", data.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", data.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", data.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", data.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date",data.current_school_from_date),
                            new SqlParameter("@sims_admission_current_school_to_date",data.current_school_to_date),
                            new SqlParameter("@sims_admission_current_school_language", data.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", data.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", data.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", data.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", data.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", data.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", data.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", data.marketing_code),
                            new SqlParameter("@sims_admission_marketing_description", data.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", data.parent_status_code),
                            new SqlParameter("@sims_admission_legal_custody", data.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", data.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date", db.DBYYYYMMDDformat(data.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date",db.DBYYYYMMDDformat(data.health_card_expiry_date)),
                            new SqlParameter("@sims_admission_health_card_issuing_authority", data.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", data.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", data.medication_status),
                            new SqlParameter("@sims_admission_medication_desc", data.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", data.disability_status),
                            new SqlParameter("@sims_admission_disability_desc", data.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", data.behaviour_status),
                            new SqlParameter("@sims_admission_behaviour_desc", data.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", data.health_restriction_status),
                            new SqlParameter("@sims_admission_health_restriction_desc", data.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", data.health_hearing_status),
                            new SqlParameter("@sims_admission_health_hearing_desc", data.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", data.health_vision_status),
                            new SqlParameter("@sims_admission_health_vision_desc", data.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", data.health_other_status),
                            new SqlParameter("@sims_admission_health_other_desc", data.health_other_desc),
                            new SqlParameter("@sims_admission_gifted_status", data.gifted_status),
                            new SqlParameter("@sims_admission_gifted_desc", data.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", data.music_status),
                            new SqlParameter("@sims_admission_music_desc", data.music_desc),
                            new SqlParameter("@sims_admission_sports_status", data.sports_status),
                            new SqlParameter("@sims_admission_sports_desc", data.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", data.language_support_status),
                            new SqlParameter("@sims_admission_language_support_desc", data.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", data.declaration_status),
                            new SqlParameter("@sims_quota_code",data.quota_code),
                            new SqlParameter("@sims_admission_ip", data.ip),
                            new SqlParameter("@sims_admission_dns", data.dns),
                            new SqlParameter("@sims_admission_user_code", data.user_code),
                            new SqlParameter("@sims_admission_status", ""),
                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),
                            new SqlParameter("@sims_admission_recommendation",data.sims_admission_recommendation),
                            new SqlParameter("@sims_parent_is_employment_number",data.employee_code),
                            new SqlParameter("@sims_pros_attribute1",data.reason),
                            new SqlParameter("@sims_pros_father_national_id",data.sims_admission_father_national_id),
                            new SqlParameter("@sims_pros_mother_national_id",data.sims_admission_mother_national_id),
                            new SqlParameter("@sims_student_category",data.category_code)
                            #endregion
                         });

                            if (dr.RecordsAffected > 0)
                            {
                                if (data.opr.Equals("U"))
                                    message.strMessage = "Prospect Admission Data Updated Sucessfully";
                                if (data.opr.Equals("O"))
                                {

                                    if (dr.Read())
                                    {

                                        message.strMessage = dr["pros_num"].ToString() + " -Prospect Admission Data Inserted Sucessfully";

                                    }

                                }
                                message.systemMessage = string.Empty;
                                message.status = true;
                                message.messageType = MessageType.Success;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("ApproveMultipleNew")]
        public HttpResponseMessage ApproveMultipleNew(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAdmPromoteData()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveMultiple", "admission_number"));

            List<string> lst = new List<string>();
            //lst = obj.pros_num.Substring(0, obj.pros_num.Length - 1).Split(',').ToList();
            lst = obj.pros_num.Substring(0, obj.pros_num.Length).Split(',').ToList();
            Message message = new Message();
            bool msg = false;
            try
            {
                if (lst != null)
                {

                    foreach (string admno in lst)
                    {
                        try
                        {
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();
                                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospects_proc]",
                                        new List<SqlParameter>() 
                            { 
                           
                            new SqlParameter("@opr", "N"),
                            new SqlParameter("@sims_admission_pros_number", admno),
                               });

                                if (dr.RecordsAffected > 0)
                                {
                                    msg = true;
                                }

                            }
                        }
                        catch (Exception ex) { }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, msg);

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
        }


        [Route("ApproveMultiple")]
        public HttpResponseMessage ApproveMultiple(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAdmPromoteData()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveMultiple", "admission_number"));

            List<string> lst = new List<string>();
            //lst = obj.pros_num.Substring(0, obj.pros_num.Length - 1).Split(',').ToList();
            lst = obj.pros_num.Substring(0, obj.pros_num.Length).Split(',').ToList();
            Message message = new Message();

            try
            {
                if (lst != null)
                {
                    foreach (string admno in lst)
                    {
                        ApproveStudent1(GetTabStudentData1(admno));
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, message);

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        private Sims010_Edit GetTabStudentData1(string pros_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTabStudentData1()PARAMETERS ::admission_number{2}";
            Log.Debug(string.Format(debug, "ERP/Admission/", "GetTabStudentData1", pros_number));

            Sims010_Edit simsobj = new Sims010_Edit();
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@CURR_CODE", ""),
                            new SqlParameter("@ACA_YEAR", ""),
                            new SqlParameter("@GRADE_CODE", ""),
                            new SqlParameter("@ADMISSION_NUM", pros_number)
                          
                         });
                        while (dr.Read())
                        {
                            #region
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_admission_parent_REG_id = dr["sims_pros_parent_REG_id"].ToString();
                            simsobj.school_code = dr["sims_pros_school_code"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.pros_appl_num = dr["sims_pros_application_number"].ToString();
                            simsobj.behaviour_desc = dr["sims_pros_behaviour_desc"].ToString();
                            if (dr["sims_pros_behaviour_status"].ToString() == "1")
                            {
                                simsobj.behaviour_status = true;
                            }
                            else
                            {
                                simsobj.behaviour_status = false;
                            }
                            simsobj.birth_country_code = dr["sims_pros_birth_country_code"].ToString();
                            simsobj.blood_group_code = dr["sims_pros_blood_group_code"].ToString();
                            //try
                            //{
                            //    simsobj.comm_date = dr["sims_admission_commencement_date"].ToString();
                            //}
                            //catch (Exception ex)
                            //{
                            //    simsobj.comm_date = "";
                            //}
                            simsobj.current_school_address = dr["sims_pros_current_school_address"].ToString();
                            simsobj.current_school_city = dr["sims_pros_current_school_city"].ToString();
                            simsobj.current_school_cur = dr["sims_pros_current_school_cur"].ToString();
                            simsobj.current_school_enroll_number = dr["sims_pros_current_school_enroll_number"].ToString();
                            simsobj.current_school_fax = dr["sims_pros_current_school_fax"].ToString();
                            try
                            {
                                simsobj.current_school_from_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_from_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.current_school_from_date = "";
                            }
                            simsobj.current_school_grade = dr["sims_pros_current_school_grade"].ToString();
                            simsobj.current_school_head_teacher = dr["sims_pros_current_school_head_teacher"].ToString();
                            simsobj.current_school_language = dr["sims_pros_current_school_language"].ToString();
                            simsobj.current_school_name = dr["sims_pros_current_school_name"].ToString();
                            simsobj.current_school_phone = dr["sims_pros_current_school_phone"].ToString();
                            if (dr["sims_pros_current_school_status"].ToString() == "1")
                            {
                                simsobj.current_school_status = true;
                            }
                            else
                            {
                                simsobj.current_school_status = false;
                            }

                            simsobj.current_school_to_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_to_date"].ToString());
                            simsobj.current_school_country = dr["current_school_country"].ToString();
                            simsobj.current_school_country_code = dr["sims_pros_current_school_country_code"].ToString();
                            if (dr["sims_pros_declaration_status"].ToString() == "1")
                            {
                                simsobj.declaration_status = true;
                            }
                            else
                            {
                                simsobj.declaration_status = false;
                            }
                            simsobj.disability_desc = dr["sims_pros_disability_desc"].ToString();
                            if (dr["sims_pros_disability_status"].ToString() == "1")
                            {
                                simsobj.disability_status = true;
                            }
                            else
                            {
                                simsobj.disability_status = false;
                            }
                            simsobj.dns = dr["sims_pros_dns"].ToString();
                            //simsobj.employee_comp_code = dr["sims_admission_employee_comp_code"].ToString();
                            //simsobj.employee_school_code = dr["sims_admission_employee_school_code"].ToString();
                            //if (dr["sims_pros_employee_type"].ToString() == "1")
                            //{
                            //    simsobj.employee_type = true;
                            //}
                            //else
                            //{
                            //    simsobj.employee_type = false;
                            //}
                            simsobj.family_name_ot = dr["sims_pros_family_name_ot"].ToString();
                            simsobj.ethinicity_code = dr["sims_pros_ethnicity_code"].ToString();
                            simsobj.family_name = dr["sims_pros_family_name_en"].ToString();
                            simsobj.father_appartment_number = dr["sims_pros_father_appartment_number"].ToString();
                            simsobj.father_area_number = dr["sims_pros_father_area_number"].ToString();
                            simsobj.father_building_number = dr["sims_pros_father_building_number"].ToString();
                            simsobj.father_city = dr["sims_pros_father_city"].ToString();
                            simsobj.father_company = dr["sims_pros_father_company"].ToString();
                            simsobj.father_country_code = dr["sims_pros_father_country_code"].ToString();
                            simsobj.father_email = dr["sims_pros_father_email"].ToString();
                            simsobj.father_family_name = dr["sims_pros_father_family_name"].ToString();
                            simsobj.father_fax = dr["sims_pros_father_fax"].ToString();
                            simsobj.father_first_name = dr["sims_pros_father_first_name"].ToString();
                            simsobj.father_last_name = dr["sims_pros_father_last_name"].ToString();
                            simsobj.father_middle_name = dr["sims_pros_father_middle_name"].ToString();
                            simsobj.father_mobile = dr["sims_pros_father_mobile"].ToString();
                            simsobj.father_name_ot = dr["sims_pros_father_name_ot"].ToString();
                            simsobj.father_nationality1_code = dr["sims_pros_father_nationality1_code"].ToString();
                            simsobj.father_nationality2_code = dr["sims_pros_father_nationality2_code"].ToString();
                            simsobj.father_occupation = dr["sims_pros_father_occupation"].ToString();
                            simsobj.father_passport_number = dr["sims_pros_father_passport_number"].ToString();
                            simsobj.father_phone = dr["sims_pros_father_phone"].ToString();
                            simsobj.father_po_box = dr["sims_pros_father_po_box"].ToString();
                            simsobj.father_salutation_code = dr["sims_pros_father_salutation_code"].ToString();
                            simsobj.father_state = dr["sims_pros_father_state"].ToString();
                            simsobj.father_street_number = dr["sims_pros_father_street_number"].ToString();
                            simsobj.father_summary_address = dr["sims_pros_father_summary_address"].ToString();
                            simsobj.fee_payment_contact_pref_code = dr["sims_pros_fee_payment_contact_pref"].ToString();
                            //if (dr["sims_admission_fees_paid_status"].ToString() == "1")
                            //{
                            //    simsobj.fees_paid_status = true;
                            //}
                            //else
                            //{
                            //    simsobj.fees_paid_status = false;
                            //}
                            simsobj.first_name = dr["sims_pros_passport_first_name_en"].ToString();
                            simsobj.first_name_ot = dr["sims_pros_passport_first_name_ot"].ToString();
                            simsobj.gifted_desc = dr["sims_pros_gifted_desc"].ToString();
                            if (dr["sims_pros_gifted_status"].ToString() == "1")
                            {
                                simsobj.gifted_status = true;
                            }
                            else
                            {
                                simsobj.gifted_status = false;
                            }
                            simsobj.guardian_appartment_number = dr["sims_pros_guardian_appartment_number"].ToString();
                            simsobj.guardian_area_number = dr["sims_pros_guardian_area_number"].ToString();
                            simsobj.guardian_building_number = dr["sims_pros_guardian_building_number"].ToString();
                            simsobj.guardian_city = dr["sims_pros_guardian_city"].ToString();
                            simsobj.guardian_company = dr["sims_pros_guardian_company"].ToString();
                            simsobj.guardian_country_code = dr["sims_pros_guardian_country_code"].ToString();
                            simsobj.guardian_email = dr["sims_pros_guardian_email"].ToString();
                            simsobj.guardian_family_name = dr["sims_pros_guardian_family_name"].ToString();
                            simsobj.guardian_fax = dr["sims_pros_guardian_fax"].ToString();
                            simsobj.guardian_first_name = dr["sims_pros_guardian_first_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_pros_guardian_last_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_pros_guardian_middle_name"].ToString();
                            simsobj.guardian_mobile = dr["sims_pros_guardian_mobile"].ToString();
                            simsobj.guardian_name_ot = dr["sims_pros_guardian_name_ot"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_pros_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality2_code = dr["sims_pros_guardian_nationality2_code"].ToString();
                            simsobj.guardian_occupation = dr["sims_pros_guardian_occupation"].ToString();
                            simsobj.guardian_passport_number = dr["sims_pros_guardian_passport_number"].ToString();
                            simsobj.guardian_phone = dr["sims_pros_guardian_phone"].ToString();
                            simsobj.guardian_po_box = dr["sims_pros_guardian_po_box"].ToString();
                            simsobj.guardian_salutation_code = dr["sims_pros_guardian_salutation_code"].ToString();
                            simsobj.guardian_state = dr["sims_pros_guardian_state"].ToString();
                            simsobj.guardian_street_number = dr["sims_pros_guardian_street_number"].ToString();
                            simsobj.guardian_summary_address = dr["sims_pros_guardian_summary_address"].ToString();
                            try
                            {
                                simsobj.health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_expiry_date = "";
                            }
                            try
                            {
                                simsobj.health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_issue_date = "";
                            }
                            simsobj.health_card_issuing_authority = dr["sims_pros_health_card_issuing_authority"].ToString();
                            simsobj.health_card_number = dr["sims_pros_health_card_number"].ToString();
                            simsobj.health_hearing_desc = dr["sims_pros_health_hearing_desc"].ToString();
                            if (dr["sims_pros_health_hearing_status"].ToString() == "1")
                            {
                                simsobj.health_hearing_status = true;
                            }
                            else
                            {
                                simsobj.health_hearing_status = false;
                            }
                            simsobj.health_other_desc = dr["sims_pros_health_other_desc"].ToString();
                            if (dr["sims_pros_health_other_status"].ToString() == "1")
                            {
                                simsobj.health_other_status = true;
                            }
                            else
                            {
                                simsobj.health_other_status = false;
                            }
                            simsobj.health_restriction_desc = dr["sims_pros_health_restriction_desc"].ToString();
                            if (dr["sims_pros_health_restriction_status"].ToString() == "1")
                            {
                                simsobj.health_restriction_status = true;
                            }
                            else
                            {
                                simsobj.health_restriction_status = false;
                            }
                            simsobj.health_vision_desc = dr["sims_pros_health_vision_desc"].ToString();
                            if (dr["sims_pros_health_vision_status"].ToString() == "1")
                            {
                                simsobj.health_vision_status = true;
                            }
                            else
                            {
                                simsobj.health_vision_status = false;
                            }
                            simsobj.last_name = dr["sims_pros_passport_last_name_en"].ToString();
                            simsobj.last_name_ot = dr["sims_pros_passport_last_name_ot"].ToString();
                            simsobj.language_support_desc = dr["sims_pros_language_support_desc"].ToString();
                            if (dr["sims_pros_language_support_status"].ToString() == "1")
                            {
                                simsobj.language_support_status = true;
                            }
                            else
                            {
                                simsobj.language_support_status = false;
                            }
                            simsobj.legal_custody_code = dr["sims_pros_legal_custody"].ToString();
                            simsobj.main_language_code = dr["sims_pros_main_language_code"].ToString();
                            simsobj.main_language_r_code = dr["sims_pros_main_language_r"].ToString();
                            simsobj.main_language_s_code = dr["sims_pros_main_language_s"].ToString();
                            simsobj.main_language_w_code = dr["sims_pros_main_language_w"].ToString();
                            simsobj.other_language_code = dr["sims_pros_other_language"].ToString();
                            if (dr["sims_pros_marketing_code"].ToString() == "1")
                            {
                                simsobj.marketing_code = true;
                            }
                            else
                            {
                                simsobj.marketing_code = false;
                            }
                            simsobj.marketing_description = dr["sims_pros_marketing_description"].ToString();
                            simsobj.medication_desc = dr["sims_pros_medication_desc"].ToString();
                            if (dr["sims_pros_medication_status"].ToString() == "1")
                            {
                                simsobj.medication_status = true;
                            }
                            else
                            {
                                simsobj.medication_status = false;
                            }
                            simsobj.middle_name = dr["sims_pros_passport_middle_name_en"].ToString();
                            simsobj.midd_name_ot = dr["sims_pros_passport_middle_name_ot"].ToString();
                            simsobj.motherTounge_language_code = dr["sims_pros_main_language_m"].ToString();
                            simsobj.mother_appartment_number = dr["sims_pros_mother_appartment_number"].ToString();
                            simsobj.mother_area_number = dr["sims_pros_mother_area_number"].ToString();
                            simsobj.mother_building_number = dr["sims_pros_mother_building_number"].ToString();
                            simsobj.mother_city = dr["sims_pros_mother_city"].ToString();
                            simsobj.mother_company = dr["sims_pros_mother_company"].ToString();
                            simsobj.mother_country_code = dr["sims_pros_mother_country_code"].ToString();
                            simsobj.mother_email = dr["sims_pros_mother_email"].ToString();
                            simsobj.mother_family_name = dr["sims_pros_mother_family_name"].ToString();
                            simsobj.mother_fax = dr["sims_pros_mother_fax"].ToString();
                            simsobj.mother_first_name = dr["sims_pros_mother_first_name"].ToString();
                            simsobj.mother_last_name = dr["sims_pros_mother_last_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_pros_mother_middle_name"].ToString();
                            simsobj.mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                            simsobj.mother_name_ot = dr["sims_pros_mother_name_ot"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_pros_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality2_code = dr["sims_pros_mother_nationality2_code"].ToString();
                            simsobj.mother_occupation = dr["sims_pros_mother_occupation"].ToString();
                            simsobj.mother_passport_number = dr["sims_pros_mother_passport_number"].ToString();
                            simsobj.mother_phone = dr["sims_pros_mother_phone"].ToString();
                            simsobj.mother_po_box = dr["sims_pros_mother_po_box"].ToString();
                            simsobj.mother_salutation_code = dr["sims_pros_mother_salutation_code"].ToString();
                            simsobj.mother_state = dr["sims_pros_mother_state"].ToString();
                            simsobj.mother_street_number = dr["sims_pros_mother_street_number"].ToString();
                            simsobj.mother_summary_address = dr["sims_pros_mother_summary_address"].ToString();
                            simsobj.music_desc = dr["sims_pros_music_desc"].ToString();
                            if (dr["sims_pros_music_status"].ToString() == "1")
                            {
                                simsobj.music_status = true;
                            }
                            else
                            {
                                simsobj.music_status = false;
                            }
                            simsobj.national_id = dr["sims_pros_national_id"].ToString();
                            try
                            {
                                simsobj.national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_issue_date = "";
                            }

                            try
                            {
                                simsobj.national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_expiry_date = "";
                            }
                            simsobj.nicke_name = dr["sims_pros_nickname"].ToString();
                            simsobj.parent_id = dr["sims_pros_parent_id"].ToString();
                            if (dr["sims_pros_parent_status_code"].ToString() == "1")
                            {
                                simsobj.parent_status_code = true;
                            }
                            else
                            {
                                simsobj.parent_status_code = false;
                            }
                            try
                            {
                                simsobj.passport_expiry = db.UIDDMMYYYYformat(dr["sims_pros_passport_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_expiry = "";
                            }
                            simsobj.passport_issue_auth = dr["sims_pros_passport_issuing_authority"].ToString();
                            try
                            {
                                simsobj.passport_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_passport_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_issue_date = "";
                            }
                            simsobj.passport_issue_place = dr["sims_pros_passport_issue_place"].ToString();
                            simsobj.passport_num = dr["sims_pros_passport_number"].ToString();
                            simsobj.primary_contact_pref_code = dr["sims_pros_primary_contact_pref"].ToString();
                            if (dr["sims_pros_primary_contact_code"].ToString() == "1")
                            {
                                simsobj.primary_contact_code = true;
                            }
                            else
                            {
                                simsobj.primary_contact_code = false;
                            }
                            simsobj.primary_contact_pref_desc = dr["primary_contact_pref_desc"].ToString();
                            simsobj.pros_appl_num = dr["sims_pros_application_number"].ToString();
                            simsobj.religion_code = dr["sims_pros_religion_code"].ToString();
                            simsobj.sibling_dob = dr["sims_pros_sibling_dob"].ToString();
                            simsobj.sibling_name = dr["sims_pros_sibling_name"].ToString();
                            simsobj.sibling_school_code = dr["sims_pros_sibling_school_code"].ToString();
                            simsobj.sibling_school_name = dr["sibSchoolName"].ToString();
                            simsobj.sports_desc = dr["sims_pros_sports_desc"].ToString();
                            if (dr["sims_pros_sports_status"].ToString() == "1")
                            {
                                simsobj.sports_status = true;
                            }
                            else
                            {
                                simsobj.sports_status = false;
                            }
                            simsobj.status = dr["sims_pros_status"].ToString();
                            // simsobj.section_code = dr["sims_pros_section_code"].ToString();
                            simsobj.user_code = dr["sims_pros_user_code"].ToString();
                            try
                            {
                                simsobj.visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_expiry_date = "";
                            }

                            try
                            {
                                simsobj.visa_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_issue_date = "";
                            }
                            simsobj.visa_issuing_authority = dr["sims_pros_visa_issuing_authority"].ToString();
                            simsobj.visa_issuing_place = dr["sims_pros_visa_issuing_place"].ToString();
                            simsobj.visa_number = dr["sims_pros_visa_number"].ToString();
                            simsobj.visa_type_desc = dr["sims_pros_visa_type_desc"].ToString();
                            simsobj.visa_type = dr["sims_pros_visa_type"].ToString();
                            if (dr["sims_pros_transport_status"].ToString() == "1")
                            {
                                simsobj.transport_status = true;
                            }
                            else
                            {
                                simsobj.transport_status = false;
                            }
                            simsobj.transport_desc = dr["sims_pros_transport_desc"].ToString();
                            simsobj.ip = dr["sims_pros_ip"].ToString();
                            #endregion
                            #region
                            simsobj.sims_student_attribute1 = dr["sims_pros_attribute1"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_pros_attribute2"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_pros_attribute3"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_pros_attribute4"].ToString();

                            simsobj.sims_student_health_respiratory_status = dr["sims_pros_health_respiratory_status"].ToString();
                            simsobj.sims_student_health_respiratory_desc = dr["sims_pros_health_respiratory_desc"].ToString();
                            simsobj.sims_student_health_hay_fever_status = dr["sims_pros_health_hay_fever_status"].ToString();
                            simsobj.sims_student_health_hay_fever_desc = dr["sims_pros_health_hay_fever_desc"].ToString();
                            simsobj.sims_student_health_epilepsy_status = dr["sims_pros_health_epilepsy_status"].ToString();
                            simsobj.sims_student_health_epilepsy_desc = dr["sims_pros_health_epilepsy_desc"].ToString();
                            simsobj.sims_student_health_skin_status = dr["sims_pros_health_skin_status"].ToString();
                            simsobj.sims_student_health_skin_desc = dr["sims_pros_health_skin_desc"].ToString();
                            simsobj.sims_student_health_diabetes_status = dr["sims_pros_health_diabetes_status"].ToString();
                            simsobj.sims_student_health_diabetes_desc = dr["sims_pros_health_diabetes_desc"].ToString();
                            simsobj.sims_student_health_surgery_status = dr["sims_pros_health_surgery_status"].ToString();
                            simsobj.sims_student_health_surgery_desc = dr["sims_pros_health_surgery_desc"].ToString();


                            simsobj.admission_number = pros_number;
                            simsobj.stud_full_name = simsobj.first_name + " " + simsobj.middle_name + " " + simsobj.last_name;// o.stud_full_name;
                            //simsobj.fee_category_code = dr["sims_admission_fee_category"].ToString();
                            try
                            {
                                simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_pros_date_created"].ToString());//o.admission_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.admission_date = "";
                            }
                            simsobj.pros_num = pros_number;
                            simsobj.school_code = dr["sims_pros_school_code"].ToString();
                            simsobj.term_code = dr["sims_pros_term_code"].ToString();
                            try
                            {
                                simsobj.tent_join_date = db.UIDDMMYYYYformat(dr["tent_join_date"].ToString());//o.tent_join_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.tent_join_date = "";
                            }
                            simsobj.gender_desc = dr["gender"].ToString();// o.gender;
                            simsobj.gender_code = dr["sims_pros_gender"].ToString();
                            try
                            {
                                simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());//o.birth_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.birth_date = "";
                            }
                            simsobj.curr_code = dr["sims_pros_cur_code"].ToString();
                            simsobj.academic_year_desc = dr["academic_year_desc"].ToString();
                            simsobj.academic_year = dr["sims_pros_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_pros_grade_code"].ToString();
                            simsobj.nationality_code = dr["sims_pros_nationality_code"].ToString();//o.nation;
                            if (dr["sibling_status"].ToString() == "1")
                            {
                                simsobj.sibling_status = true;
                            }
                            else
                            {
                                simsobj.sibling_status = false;
                            }
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            #endregion
                        }

                        return simsobj;
                    }
                }
                return simsobj;
            }
            catch (Exception x)
            {
                return simsobj;
            }
        }

        private List<Sims010_Edit> ApproveStudent1(Sims010_Edit simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ApproveStudent()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveStudent"));

            // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            List<Sims010_Edit> lst = new List<Sims010_Edit>();
            Message message = new Message();
            //string enroll="", parent_id="", student_name="";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospects_proc]",
                            new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'A'),
                            //new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            #region
                            //new SqlParameter("@sims_admission_application_number", simsobj.appl_num),
                            //new SqlParameter("@sims_admission_pros_number", simsobj.pros_num),
                            //new SqlParameter("@sims_admission_pros_application_number", simsobj.pros_appl_num),
                            new SqlParameter("@sims_admission_parent_REG_id",simsobj.sims_admission_parent_REG_id),
                            new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            new SqlParameter("@sims_admission_pros_number", simsobj.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", simsobj.pros_appl_num),
                            new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                            new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                           // new SqlParameter("@sims_admission_status",simsobj.sims_pros_status),
                            new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                            new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                            new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                            new SqlParameter("@sims_admission_date", db.DBYYYYMMDDformat(simsobj.admission_date)),

                            
                            new SqlParameter("@sims_admission_school_code", simsobj.school_code),
                            new SqlParameter("@sims_admission_cur_code", simsobj.curr_code),
                            new SqlParameter("@sims_admission_academic_year", simsobj.academic_year),
                            new SqlParameter("@sims_admission_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_admission_section_code", simsobj.section_code),
                            new SqlParameter("@sims_admission_term_code", simsobj.term_code),
                            new SqlParameter("@sims_admission_tentative_joining_date",db.DBYYYYMMDDformat(simsobj.tent_join_date)),
                           
                            new SqlParameter("@sims_admission_passport_first_name_en", simsobj.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", simsobj.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", simsobj.last_name),
                            new SqlParameter("@sims_admission_family_name_en", simsobj.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", simsobj.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", simsobj.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_nickname", simsobj.nicke_name),
                            new SqlParameter("@sims_admission_dob",db.DBYYYYMMDDformat(simsobj.birth_date)),
                            new SqlParameter("@sims_admission_commencement_date",db.DBYYYYMMDDformat(simsobj.comm_date)),
                           
                            new SqlParameter("@sims_admission_birth_country_code", simsobj.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", simsobj.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", simsobj.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", simsobj.gender_code),
                            new SqlParameter("@sims_admission_religion_code", simsobj.religion_code),
                            new SqlParameter("@sims_admission_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date",db.DBYYYYMMDDformat(simsobj.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.passport_expiry)),
                           
                            new SqlParameter("@sims_admission_passport_issuing_authority", simsobj.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", simsobj.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", simsobj.visa_number),
                            new SqlParameter("@sims_admission_visa_type", simsobj.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", simsobj.visa_issuing_authority),
                            new SqlParameter("@sims_admission_visa_issue_date",db.DBYYYYMMDDformat(simsobj.visa_issue_date)),
                            new SqlParameter("@sims_admission_visa_expiry_date",db.DBYYYYMMDDformat(simsobj.visa_expiry_date)),
                           
                            new SqlParameter("@sims_admission_visa_issuing_place", simsobj.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", simsobj.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date",db.DBYYYYMMDDformat(simsobj.national_id_issue_date)),
                                new SqlParameter("@sims_admission_national_id_expiry_date",db.DBYYYYMMDDformat(simsobj.national_id_expiry_date)),
                
                        
                            new SqlParameter("@sims_admission_sibling_status", simsobj.sibling_status),
                            new SqlParameter("@sims_admission_parent_id", simsobj.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", simsobj.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", simsobj.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",simsobj.sibling_dob),
                        
                            new SqlParameter("@sims_admission_sibling_school_code", simsobj.sibling_school_code),
                            new SqlParameter("@sims_admission_employee_type", simsobj.employee_type),
                            new SqlParameter("@sims_admission_employee_code", simsobj.employee_code),
                            new SqlParameter("@sims_admission_employee_school_code", simsobj.employee_school_code),


                            new SqlParameter("@sims_admission_main_language_m", simsobj.motherTounge_language_code),


                            new SqlParameter("@sims_admission_main_language_code", simsobj.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", simsobj.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", simsobj.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", simsobj.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", simsobj.primary_contact_code),
                            new SqlParameter("@sims_admission_primary_contact_pref", simsobj.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", simsobj.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", simsobj.transport_status),
                            new SqlParameter("@sims_admission_transport_desc", simsobj.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", simsobj.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", simsobj.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", simsobj.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", simsobj.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", simsobj.family_name),
                            new SqlParameter("@sims_admission_father_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", simsobj.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", simsobj.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", simsobj.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", simsobj.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", simsobj.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", simsobj.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", simsobj.father_city),
                            new SqlParameter("@sims_admission_father_state_name", simsobj.father_state),
                            new SqlParameter("@sims_admission_father_country_code", simsobj.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", simsobj.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", simsobj.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", simsobj.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", simsobj.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", simsobj.father_fax),
                            new SqlParameter("@sims_admission_father_email", simsobj.father_email),
                            new SqlParameter("@sims_admission_father_occupation", simsobj.father_occupation),
                            new SqlParameter("@sims_admission_father_company", simsobj.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", simsobj.father_passport_number),
                            new SqlParameter("@sims_admission_guardian_salutation_code", simsobj.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", simsobj.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", simsobj.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", simsobj.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", simsobj.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", simsobj.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", simsobj.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", simsobj.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", simsobj.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", simsobj.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", simsobj.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", simsobj.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", simsobj.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", simsobj.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", simsobj.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", simsobj.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", simsobj.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", simsobj.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", simsobj.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", simsobj.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", simsobj.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", simsobj.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", simsobj.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_mother_salutation_code", simsobj.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", simsobj.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", simsobj.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", simsobj.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", simsobj.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", simsobj.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", simsobj.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", simsobj.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", simsobj.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", simsobj.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", simsobj.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", simsobj.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", simsobj.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", simsobj.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", simsobj.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", simsobj.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", simsobj.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", simsobj.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", simsobj.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", simsobj.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", simsobj.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", simsobj.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", simsobj.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", simsobj.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", simsobj.current_school_status),
                            new SqlParameter("@sims_admission_current_school_name", simsobj.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", simsobj.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", simsobj.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", simsobj.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date",db.DBYYYYMMDDformat(simsobj.current_school_from_date)),
                            new SqlParameter("@sims_admission_current_school_to_date",db.DBYYYYMMDDformat(simsobj.current_school_to_date)),
                            
                            new SqlParameter("@sims_admission_current_school_language", simsobj.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", simsobj.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", simsobj.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", simsobj.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", simsobj.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", simsobj.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", simsobj.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", simsobj.marketing_code),
                            new SqlParameter("@sims_admission_marketing_description", simsobj.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", simsobj.parent_status_code),
                            new SqlParameter("@sims_admission_legal_custody", simsobj.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", simsobj.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date",db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date",db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),
                           
                            new SqlParameter("@sims_admission_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", simsobj.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", simsobj.medication_status),
                            new SqlParameter("@sims_admission_medication_desc", simsobj.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", simsobj.disability_status),
                            new SqlParameter("@sims_admission_disability_desc", simsobj.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", simsobj.behaviour_status),
                            new SqlParameter("@sims_admission_behaviour_desc", simsobj.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", simsobj.health_restriction_status),
                            new SqlParameter("@sims_admission_health_restriction_desc", simsobj.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", simsobj.health_hearing_status),
                            new SqlParameter("@sims_admission_health_hearing_desc", simsobj.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", simsobj.health_vision_status),
                            new SqlParameter("@sims_admission_health_vision_desc", simsobj.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", simsobj.health_other_status),
                            new SqlParameter("@sims_admission_health_other_desc", simsobj.health_other_desc),

                            new SqlParameter("@sims_admission_gifted_status", simsobj.gifted_status),
                            new SqlParameter("@sims_admission_gifted_desc", simsobj.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", simsobj.music_status),
                            new SqlParameter("@sims_admission_music_desc", simsobj.music_desc),
                            new SqlParameter("@sims_admission_sports_status", simsobj.sports_status),
                            new SqlParameter("@sims_admission_sports_desc", simsobj.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", simsobj.language_support_status),
                            new SqlParameter("@sims_admission_language_support_desc", simsobj.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", simsobj.declaration_status),
                            new SqlParameter("@sims_admission_fees_paid_status", simsobj.fees_paid_status),
                            new SqlParameter("@sims_admission_fee_category_code", simsobj.fee_category_code),
                            new SqlParameter("@sims_admission_ip", simsobj.ip),
                            new SqlParameter("@sims_admission_dns", simsobj.dns),
                            new SqlParameter("@sims_admission_user_code", simsobj.user_code),
                            new SqlParameter("@sims_admission_status", ""),

                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),

                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),


                            new SqlParameter("@sims_student_attribute1", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_student_attribute2", simsobj.sims_student_attribute2),
                            new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3),
                            new SqlParameter("@sims_student_attribute4", simsobj.sims_student_attribute4),

                            new SqlParameter("@sims_student_health_respiratory_status", simsobj.sims_student_health_respiratory_status),
                            new SqlParameter("@sims_student_health_respiratory_desc", simsobj.sims_student_health_respiratory_desc),
                            new SqlParameter("@sims_student_health_hay_fever_status", simsobj.sims_student_health_hay_fever_status),
                            new SqlParameter("@sims_student_health_hay_fever_desc", simsobj.sims_student_health_hay_fever_desc),
                            new SqlParameter("@sims_student_health_epilepsy_status", simsobj.sims_student_health_epilepsy_status),
                            new SqlParameter("@sims_student_health_epilepsy_desc", simsobj.sims_student_health_epilepsy_desc),
                            new SqlParameter("@sims_student_health_skin_status", simsobj.sims_student_health_skin_status),
                            new SqlParameter("@sims_student_health_skin_desc", simsobj.sims_student_health_skin_desc),
                            new SqlParameter("@sims_student_health_diabetes_status", simsobj.sims_student_health_diabetes_status),
                            new SqlParameter("@sims_student_health_diabetes_desc", simsobj.sims_student_health_diabetes_desc),
                            new SqlParameter("@sims_student_health_surgery_status", simsobj.sims_student_health_surgery_status),
                            new SqlParameter("@sims_student_health_surgery_desc", simsobj.sims_student_health_surgery_desc),
                            new SqlParameter("@sims_student_category",simsobj.category_code)
                            #endregion
                         });

                        return lst;
                    }
                }
                return lst;
            }
            catch (Exception x)
            {
                return lst;
            }
        }

        [Route("AdmissionStatusOpr")]
        public HttpResponseMessage AdmissionStatusOpr(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AdmissionStatusOpr(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "AdmissionStatus"));
            int cnt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospects_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", "J"),//J
                             new SqlParameter("@sims_admission_numberColl", obj.pros_num),
                             new SqlParameter("@sims_admission_status", obj.status),
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        cnt = 1;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, cnt);
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, cnt);
            }

        }

        [Route("GetCommMethods")]
        public HttpResponseMessage GetCommMethods()
        {
            List<Uccw034> mod_list = new List<Uccw034>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                        new List<SqlParameter>()
                    {
                    new SqlParameter("@opr",'V'),
                    });
                    while (dr.Read())
                    {
                        Uccw034 cm = new Uccw034();
                        cm.comm_method = dr["sims_appl_parameter"].ToString();
                        cm.comm_mode_desc = dr["sims_appl_form_field_value1"].ToString();
                        mod_list.Add(cm);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getCommunication")]
        public HttpResponseMessage getCommunication(string pros_no)
        {
            List<Uccw034> type_list = new List<Uccw034>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_pros_comm_proc]",
                        new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "S"),
                       new SqlParameter("@ADMISSION_NUM", pros_no),
                       new SqlParameter("@COMM_MODE", ""),
                       new SqlParameter("@COMM_DATE", ""),
                       new SqlParameter("@COMM_DESC", ""),
                       new SqlParameter("@COMM_ENQRER_REM", ""),
                       new SqlParameter("@COMM_STATUS","A"),
                    });
                    while (dr.Read())
                    {
                        Uccw034 simsobj = new Uccw034();
                        simsobj.pros_num = pros_no;
                        simsobj.comm_desc = dr["sims_pros_comm_desc"].ToString();
                        simsobj.comm_method = dr["sims_pros_comm_mode"].ToString();
                        simsobj.comm_mode_desc = dr["Mode"].ToString();
                        //simsobj.comm_date = DateTime.Parse(dr["sims_pros_comm_date"].ToString()).ToShortDateString();
                        simsobj.comm_date = db.UIDDMMYYYYformat(dr["sims_pros_comm_date"].ToString());
                        simsobj.enq_rem = dr["sims_pros_comm_inquirer_remark"].ToString();
                        simsobj.status = dr["sims_pros_comm_status"].ToString();
                        type_list.Add(simsobj);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUDCommunication")]
        public HttpResponseMessage CUDCommunication(List<Uccw034> data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw034 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_pros_comm_proc]",
                                new List<SqlParameter>() 
                         {                             
                                new SqlParameter("@opr","I"),//simsobj.opr
                                new SqlParameter("@ADMISSION_NUM", simsobj.pros_num),
                                new SqlParameter("@COMM_MODE", simsobj.comm_method),
                                new SqlParameter("@COMM_DATE", db.DBYYYYMMDDformat(simsobj.comm_date)),
                                new SqlParameter("@COMM_DESC", simsobj.comm_desc),
                                new SqlParameter("@COMM_ENQRER_REM", simsobj.enq_rem),
                                new SqlParameter("@COMM_STATUS", simsobj.status),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAdmission_EmailIds")]
        public HttpResponseMessage GetAdmission_EmailIds(string pros_nos)
        {
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospect_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Z'),
                            new SqlParameter("@pros_nos", pros_nos),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.emailid = dr["EmailID"].ToString();
                            simsobj.chk_email = true;
                            email_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, email_list);
            }
        }


        //walk in

        [Route("GetCurriculum")]
        public HttpResponseMessage GetCurriculum()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "C")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.cur_code = dr["sims_cur_code"].ToString();
                            c.curr_name = dr["sims_cur_full_name_en"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }
        [Route("exportbtn")]
        public HttpResponseMessage exportbtn(string user_name)
        {
            string flg = string.Empty;
           
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_reregistration_dashboard_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "BB"),
                             new SqlParameter("@parent_id",user_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            flg = dr["flg"].ToString();
                           
                          
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, flg);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, flg);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, flg);
                }
            }
        }

        [Route("GetAcademicYear")]
        public HttpResponseMessage GetAcademicYear()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "B")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.academic_year = dr["sims_academic_year"].ToString();
                            c.academic_year_desc = dr["sims_academic_year_description"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("GetAllGradeSec")]
        public HttpResponseMessage GetAllGradeSec(string currYear)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@academic_code", currYear)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.gradeSectionCode = dr["gradeSectionCode"].ToString();
                            c.gradeSectionName = dr["gradeSectionName"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("GetAboutUsData")]
        public HttpResponseMessage GetAboutUsData()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "E")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.referal_code = dr["sims_appl_parameter"].ToString();
                            c.referal_name = dr["Sims_appl_form_field_value1"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("AddEnquirySisoNew")]
        public HttpResponseMessage AddEnquirySisoNew(enquiry en)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: SUBJECT {2},MESSAGE {2},USER {2}";

            bool inserted = false;
            List<enquiry> enquiry = new List<enquiry>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@sname", en.sname),
                            new SqlParameter("@gender", en.gender),
                            new SqlParameter("@suddob", en.dob),
                            new SqlParameter("@sims_grade_code", en.grade),
                            new SqlParameter("@pname", en.pname),
                            new SqlParameter("@mobnumber",en.mobile),
                            new SqlParameter("@email",en.email),
                            new SqlParameter("@sims_admission_password",en.password),
                            new SqlParameter("@academic_code",en.academic_year),
                              new SqlParameter("@referal_code",en.referal_code),
                              new SqlParameter("@school_code",en.school_code),

                         });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("GetPreviousSchool")]
        public HttpResponseMessage GetPreviousSchool()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "G")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.referal_code = dr["sims_appl_parameter"].ToString();
                            c.referal_name = dr["Sims_appl_form_field_value1"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("UpdateEnquirySisoNew")]
        public HttpResponseMessage UpdateEnquirySisoNew(enquiry en)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: SUBJECT {2},MESSAGE {2},USER {2}";

            bool inserted = false;
            List<enquiry> enquiry = new List<enquiry>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H"),
                            new SqlParameter("@sname", en.sname),
                            new SqlParameter("@gender", en.gender),
                            new SqlParameter("@suddob", en.dob),
                            new SqlParameter("@sims_grade_code", en.grade),
                            new SqlParameter("@pname", en.pname),
                            new SqlParameter("@mobnumber",en.mobile),
                            new SqlParameter("@email",en.email),
                            new SqlParameter("@sims_admission_password",en.password),
                            new SqlParameter("@academic_code",en.academic_year),
                              new SqlParameter("@referal_code",en.referal_code),
                              new SqlParameter("@school_code",en.school_code),
                              new SqlParameter("@pros_number",en.pros_number),


                         });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetMobileDetails")]
        public HttpResponseMessage GetMobileDetails(string mobile)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<enquiry> result = new List<enquiry>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@mobnumber", mobile)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            enquiry c = new enquiry();
                            c.sims_pros_attribute1 = dr["sims_pros_attribute1"].ToString();
                            c.sims_pros_status = dr["sims_pros_status"].ToString();
                            c.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            c.sims_grade_name_en = dr["sims_grade_name_en"].ToString();

                            c.sims_pros_number = dr["sims_pros_number"].ToString();
                            c.pname = dr["pname"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }



        [Route("GetRegistrationDetail")]
        public HttpResponseMessage GetRegistrationDetail(string curr_code, string AcadmicYear, string gradeCode, string admission_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";
            List<reregistration> type_list = new List<reregistration>();
            try
            {
                if (gradeCode == "null" || gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                //if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear))////|| string.IsNullOrEmpty(gradeCode)
                //    str = "G";
                //else
                str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_reregistration_dashboard_proc",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", str),
                             new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reregistration simsobj = new reregistration();
                            simsobj.sims_parent_login_code = dr["sims_parent_login_code"].ToString();

                            try
                            {
                                simsobj.sims_parent_login_code1 = int.Parse(Regex.Match(dr["sims_parent_login_code"].ToString(), @"\d+").Value);
                            }
                            catch (Exception ex) { }
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.std_status = dr["std_status"].ToString();
                            simsobj.std_communication_date = dr["std_communication_date"].ToString();
                            simsobj.std_follow_date = dr["std_follow_date"].ToString();
                            simsobj.std_assigned_employee = dr["std_assigned_employee"].ToString();
                            simsobj.pname = dr["pname"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simsobj.std_next_grade = dr["std_next_grade"].ToString();

                            simsobj.std_willing = dr["std_willing"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.std_reg_date = dr["std_reg_date"].ToString();
                            simsobj.cnt = dr["cnt"].ToString();



                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }


        [Route("GetRegistrationDetail")]
        public HttpResponseMessage GetRegistrationDetail(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string parent, string std_willing)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";
            List<reregistration> type_list = new List<reregistration>();
            try
            {
                if (gradeCode == "null" || gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                if (parent == "null" || parent == "undefined")
                {
                    parent = null;
                }
                if (std_willing == "null" || std_willing == "undefined")
                {
                    std_willing = null;
                }
                //if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear))////|| string.IsNullOrEmpty(gradeCode)
                //    str = "G";
                //else
                str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_reregistration_dashboard_proc",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", str),
                             new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@parent_id", parent),

                            new SqlParameter("@ADMISSION_NUM", admission_status),

                            new SqlParameter("@std_willing", std_willing)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reregistration simsobj = new reregistration();
                            simsobj.sims_parent_login_code = dr["sims_parent_login_code"].ToString();

                            try
                            {
                                simsobj.sims_parent_login_code1 = int.Parse(Regex.Match(dr["sims_parent_login_code"].ToString(), @"\d+").Value);
                            }
                            catch (Exception ex) { }
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.std_status = dr["std_status"].ToString();
                            if (!string.IsNullOrEmpty(dr["std_communication_date"].ToString()))
                            {
                                simsobj.std_communication_date1 = DateTime.Parse(db.DBYYYYMMDDformat(dr["std_communication_date"].ToString()));
                            }
                            simsobj.std_communication_date = dr["std_communication_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["std_follow_date"].ToString()))
                            {
                                simsobj.std_follow_date1 = DateTime.Parse(db.DBYYYYMMDDformat(dr["std_follow_date"].ToString()));
                            }
                            simsobj.std_follow_date = dr["std_follow_date"].ToString();
                            simsobj.std_assigned_employee = dr["std_assigned_employee"].ToString();
                            simsobj.pname = dr["pname"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simsobj.std_next_grade = dr["std_next_grade"].ToString();

                            simsobj.std_willing = dr["std_willing"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.std_reg_date = dr["std_reg_date"].ToString();
                            simsobj.cnt = dr["cnt"].ToString();
                            simsobj.sims_class = dr["class"].ToString();
                            if (dr["sims_status"].ToString() == "O")
                                simsobj.sims_status = "Online Payment";
                            else if (dr["sims_status"].ToString() == "S")
                                simsobj.sims_status = "Will pay at schoool";
                            else
                                simsobj.sims_status = "Offline";




                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }



        [Route("GetRegistrationDetailNEw")]
        public HttpResponseMessage GetRegistrationDetailNEw(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string parent)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";
            List<reregistration> type_list = new List<reregistration>();
            try
            {
                if (gradeCode == "null" || gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                if (parent == "null" || parent == "undefined")
                {
                    parent = null;
                }

                //if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear))////|| string.IsNullOrEmpty(gradeCode)
                //    str = "G";
                //else
                str = "A";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_reregistration_dashboard_proc",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", str),
                             new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@parent_id", parent),

                            new SqlParameter("@ADMISSION_NUM", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reregistration simsobj = new reregistration();

                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.std_next_grade = dr["std_next_grade"].ToString();
                            //if(dr["sims_seat_confirm_status"].ToString() =="Y")
                            simsobj.status = dr["sims_seat_confirm_status"].ToString();
                            simsobj.sims_seat_cofirm_tran_id = dr["sims_seat_cofirm_tran_id"].ToString();
                            if (dr["is_disabled"].ToString() == "Y")
                                simsobj.is_disabled = true;



                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }




        [Route("ApproveMultipleNewOA")]
        public HttpResponseMessage ApproveMultipleNewOA(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAdmPromoteData()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveMultiple", "admission_number"));

            List<string> lst = new List<string>();
            //lst = obj.pros_num.Substring(0, obj.pros_num.Length - 1).Split(',').ToList();
            lst = obj.pros_num.Substring(0, obj.pros_num.Length).Split(',').ToList();
            Message message = new Message();
            bool msg = false;
            try
            {
                if (lst != null)
                {

                    foreach (string admno in lst)
                    {
                        try
                        {
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();
                                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_prospects_proc]",
                                        new List<SqlParameter>() 
                            { 
                           
                            new SqlParameter("@opr", obj.opr),
                            new SqlParameter("@sims_admission_pros_number", admno),
                               });

                                if (dr.RecordsAffected > 0)
                                {
                                    msg = true;
                                }

                            }
                        }
                        catch (Exception ex) { }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, msg);

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
        }



        [Route("AddEnquiryNew")]
        public HttpResponseMessage AddEnquiryNew(enquiry en)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: SUBJECT {2},MESSAGE {2},USER {2}";

            bool inserted = false;
            List<enquiry> enquiry = new List<enquiry>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims.sims_admission_enquiry_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_admission_parent_first_name", en.sname),
                           
                            new SqlParameter("@sims_admission_parent_qualification", en.grade),
                          
                            new SqlParameter("@sims_admission_parent_mobile",en.mobile),
                            new SqlParameter("@sims_admission_parent_email",en.email),
                           // new SqlParameter("@sims_admission_password",en.password),
                            new SqlParameter("@sims_academic_year",en.academic_year),
                           
                           //   new SqlParameter("@school_code",en.school_code),

                         });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}