﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Helper;
using System.Data.SqlClient;

namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/common/AdmissionSubject")]
    public class secondthirdSubjectController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_secondlangDetails")]
        public HttpResponseMessage Get_secondlangDetails(string cur_code, string acad_yr, string grade_code, string section_code, string lang_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","A"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_Code",section_code),
                            new SqlParameter("@sims_language_code",lang_code)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            
                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        [Route("Get_thirdlangDetails")]
        public HttpResponseMessage Get_thirdlangDetails(string cur_code, string acad_yr, string grade_code, string section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_Code",section_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        ////[Route("Get_thirdlangDetails")]
        ////public HttpResponseMessage Get_thirdlangDetails(string cur_code, string acad_yr, string grade_code, string section_code)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
        //    List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
        //                new List<SqlParameter>()
        //                {
        //                    new SqlParameter("@opr","B"),
        //                    new SqlParameter("@sims_cur_code",cur_code),
        //                    new SqlParameter("@sims_Academic_year",acad_yr),
        //                    new SqlParameter("@sims_grade_code",grade_code),
        //                    new SqlParameter("@sims_section_Code",section_code),
        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims010_Edit obj = new Sims010_Edit();
        //                    obj.sims_subject_code = dr["sims_subject_code"].ToString();
        //                    obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
        //                    subject_lst.Add(obj);
        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
        //            }
        //            else
        //             return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
        //       }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
        //    }
        //}

        [Route("Get_subjectMapping1")]
        public HttpResponseMessage Get_subjectMapping1(string cur_code, string acad_yr, string grade_code, string section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_Code",section_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.enroll_number = dr["sims_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_subject_code = dr["second_lang"].ToString();
                            obj.sims_admission_third_lang_code = dr["third_lang"].ToString();
                            obj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            obj.sims_student_attribute4 = dr["sims_student_attribute4"].ToString();

                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        [Route("Get_subjectMapping")]
        public HttpResponseMessage Get_subjectMapping(string cur_code, string acad_yr, string grade_code, string section_code,string lang_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                if(lang_code=="undefined")
                {
                    lang_code="";
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_Code",section_code),
                            new SqlParameter("@sims_language_code",lang_code)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.enroll_number = dr["sims_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            //obj.SubjectDet1 = new List<subjectDet1>();


                            //string r = "2,3,4,5,6";

                            List<subjectDet1> f = new List<subjectDet1>();
                            string sub_code =dr["sims_subject_code"].ToString();
                           // string sub_code1 = "2,3,4,5,6";

                            List<string> TagIds = sub_code.Split(',').ToList();
                            obj.SubjectDet1 = TagIds;
                           // var strCommanSepArray = sub_code.Split(",");
                           // f.AddRange(sub_code.Split(new char[] { ',' }));

                           // string hsubject = sub_code;
                            //try
                            //{
                            //    for (int e = 0; e < sub_code1.Length; e++)
                            //    {
                            //        subjectDet1 sub = new subjectDet1();
                            //        try
                            //        {
                            //            int subjectCode = sub_code.IndexOf(",");
                            //            sub.sims_subject_code = sub_code.Substring(0, subjectCode);
                            //            f.Add(sub);
                            //            sub_code = sub_code.Substring(subjectCode + 1);
                            //        }
                            //        catch (Exception hh)
                            //        {
                            //            subjectDet1 sub_code11 = new subjectDet1();
                            //            sub_code11.sims_subject_code = sub_code;
                            //            f.Add(sub_code11);
                            //        }
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    subjectDet1 sub_code12 = new subjectDet1();
                            //    sub_code12.sims_subject_code = sub_code;
                            //    f.Add(sub_code12);
                            //}

                            //while (dr.Read())
                            //{
                            //   
                            //  {
                            //    subjectDet1 fd = new subjectDet1();
                            //    obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            //    obj.SubjectDet1.Add(fd);
                            //}

                           // obj.sims_admission_third_lang_code = dr["third_lang"].ToString();
                            //obj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            //obj.sims_student_attribute4 = dr["sims_student_attribute4"].ToString();

                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        
        }


        [Route("Get_subjectMappingN")]
        public HttpResponseMessage Get_subjectMappingN(string cur_code, string acad_yr, string grade_code, string section_code, string lang_code)
        {
            //  string cur_code = string.Empty;
            string str = "";
            List<smain> mod_list = new List<smain>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                    new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","H"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_Code",section_code),
                            new SqlParameter("@sims_language_code",lang_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.enroll_number == str select p);

                            smain ob = new smain();
                            ob.sublist = new List<subject_lst>();
                            ob.enroll_number = dr["sims_enroll_number"].ToString();
                            ob.student_name = dr["student_name"].ToString();
                            ob.sims_subject_code = dr["sims_subject_code"].ToString();
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();





                            subject_lst sb = new subject_lst();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_compulsory_subject = dr["sims_compulsory_subject"].ToString();
                            sb.enroll_number = dr["sims_enroll_number"].ToString();
                            
                            
                            if (!string.IsNullOrEmpty(dr["sims_status"].ToString()))
                                sb.sims_status = true;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }



        [Route("CUDsubject")]
        public HttpResponseMessage CUDsubject(List<Sims010_Edit> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims010_Edit simobj in data)
                    {


                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_subject_language_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sims_cur_code",simobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", simobj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",simobj.sims_sims_grades),
                          new SqlParameter("@sims_section_code",simobj.sims_sims_section),
                          new SqlParameter("@sims_enrollment_number", simobj.sims_enrollment_number),
                          new SqlParameter("@sims_student_attribute1",simobj.sims_student_attribute1),
                          new SqlParameter("@sims_student_attribute2", simobj.sims_student_attribute2),
                          new SqlParameter("@sims_student_attribute3",simobj.sims_student_attribute3),
                          new SqlParameter("@sims_student_attribute4", simobj.sims_student_attribute4),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year, string user_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "G"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@user_name", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year, string user_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code),
                                 new SqlParameter("@user_name", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("Get_langDetails")]
        public HttpResponseMessage Get_langDetails(string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "Get_langDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_subject_language_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","F"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_Academic_year",acad_yr),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.sims_language_code = dr["sims_language_code"].ToString();
                            obj.sims_language_desc = dr["sims_language_desc"].ToString();
                            obj.sims_elective_subject = dr["sims_elective_subject"].ToString();
                            obj.sims_compulsory_subject = dr["sims_compulsory_subject"].ToString();
                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        [Route("CUDsubjectNew")]
        public HttpResponseMessage CUDsubjectNew(List<Sims010_Edit> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims010_Edit simobj in data)
                    {


                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_subject_language_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sims_cur_code",simobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", simobj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",simobj.sims_sims_grades),
                          new SqlParameter("@sims_section_code",simobj.sims_sims_section),
                          new SqlParameter("@sims_enrollment_number", simobj.sims_enrollment_number),
                          new SqlParameter("@sims_student_attribute1",simobj.sims_student_attribute1),
                          new SqlParameter("@sims_student_attribute2", simobj.sims_student_attribute2),
                          new SqlParameter("@sims_student_attribute3",simobj.sims_student_attribute3),
                          new SqlParameter("@sims_student_attribute4", simobj.sims_student_attribute4),
                          new SqlParameter("@sims_language_code", simobj.sims_language_code),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            //inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


   


    }
}