﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/SectionScreening")]

    public class SectionScreeningController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllCriteriaNames")]
        public HttpResponseMessage getAllCriteriaNames()
        {
            List<Sim038> criteria = new List<Sim038>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_criteria_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 crit = new Sim038();
                            crit.screening_criteria_code = dr["sims_criteria_code"].ToString();
                            crit.adm_mark_criteria_code_name = dr["sims_criteria_name_en"].ToString();
                            crit.screening_criteria_code_name = dr["sims_criteria_name_en"].ToString();
                            crit.screening_criteria_type = dr["sims_criteria_type"].ToString();
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

        [Route("Get_All_Section_Screening")]
        public HttpResponseMessage Get_Section_Screening()
        {
            List<Sim038> mod_list = new List<Sim038>();
            int cnt = 1;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_screening_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'K'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();
                            simsobj.myid = cnt;
                            simsobj.screening_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.screening_cur_code_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.screening_academic_year = dr["acad_year"].ToString();
                            simsobj.screening_academic_year_desc = dr["sims_academic_year"].ToString();
                            simsobj.screening_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.screening_grade_code_name = dr["sims_grade_name_en"].ToString();
                            simsobj.screening_section_code = dr["sims_section_code"].ToString();
                            simsobj.screening_section_code_name = dr["sims_section_name_en"].ToString();
                            simsobj.screening_grade_sec = dr["grade_sec"].ToString();
                            simsobj.screening_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.screening_criteria_code_name = dr["sims_criteria_name_en"].ToString();
                            simsobj.screening_marks = dr["sims_marks"].ToString();
                            simsobj.screening_rating = dr["sims_rating"].ToString();
                            try
                            {
                                simsobj.sims_grade_scale = dr["sims_grade_scale"].ToString();
                            }
                            catch(Exception ex) { };

                            mod_list.Add(simsobj);
                            cnt++;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("Get_Section_Screening")]
        public HttpResponseMessage Get_Section_Screening(string cur_code, string academic_year, string grade_code)
        {
            if (cur_code == "undefined")
            {
                cur_code = null;
            }
            if (academic_year == "undefined")
            {
                academic_year = null;
            }
            if (grade_code == "undefined")
            {
                grade_code = null;
            }

            List<Sim038> mod_list = new List<Sim038>();
            int cnt = 1;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_screening_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@sims_cur_code1", cur_code),
                              new SqlParameter("@sims_academic_year1", academic_year),
                              new SqlParameter("@sims_grade_name2", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();
                            simsobj.myid = cnt;
                            simsobj.screening_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.screening_cur_code_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.screening_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.screening_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.screening_grade_code_name = dr["sims_grade_name_en"].ToString();
                            simsobj.screening_section_code = dr["sims_section_code"].ToString();
                            simsobj.screening_section_code_name = dr["sims_section_name_en"].ToString();
                            simsobj.screening_grade_sec = dr["grade_sec"].ToString();
                            simsobj.screening_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.screening_criteria_code_name = dr["sims_criteria_name_en"].ToString();
                            simsobj.screening_marks = dr["sims_marks"].ToString();
                            simsobj.screening_rating = dr["sims_rating"].ToString();
                            mod_list.Add(simsobj);
                            cnt++;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getAllSection")]
        public HttpResponseMessage getAllSection(string cur_code, string ac_year, string grade)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSection(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Section", "Section"));

            List<Sim035> mod_list = new List<Sim035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@IGNORED_SECTION_LIST", grade)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim035 simsobj = new Sim035();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["section"].ToString();
                            simsobj.sims_grade_sec = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("CUDInsertSection_Screening")]
        public HttpResponseMessage CUDInsertSection_Screening(List<Sim038> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertSection_Screening(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CUDInsertSection_Screening"));
            bool inserted = false;
            //  Sim038  simsobj =  bool inserted = false; Newtonsoft.Json.JsonConvert.DeserializeObject<Sim038>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sim038 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_screening_proc",
                                new List<SqlParameter>() 
                            {                             
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_name", simsobj.screening_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.screening_academic_year),
                                new SqlParameter("@Grade_List", simsobj.screening_grade_code),
                                new SqlParameter("@sims_section_name", simsobj.screening_grade_sec),
                                new SqlParameter("@sims_criteria_name", simsobj.screening_criteria_code),
                                new SqlParameter("@sims_marks", simsobj.screening_marks),
                                new SqlParameter("@sims_rating", simsobj.screening_rating)
                          });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDSection_Screening")]
        public HttpResponseMessage CUDSection_Screening(List<Sim038> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSection_Screening(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CUDSection_Screening"));
            bool inserted = false;
            //  Sim038  simsobj =  bool inserted = false; Newtonsoft.Json.JsonConvert.DeserializeObject<Sim038>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sim038 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("sims.sims_section_screening_proc",
                                new List<SqlParameter>() 
                            {                             
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_name", simsobj.screening_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.screening_academic_year),
                                new SqlParameter("@Grade_List", simsobj.screening_grade_code),
                                new SqlParameter("@sims_section_name", ""),
                                new SqlParameter("@sims_criteria_name", simsobj.screening_criteria_code),
                                new SqlParameter("@sims_marks", simsobj.screening_marks),
                                new SqlParameter("@sims_rating", simsobj.screening_rating),
                                new SqlParameter("@sims_grade_scale", simsobj.sims_grade_scale)
                          });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        

        [Route("Get_Change_Academic_year")]
        public HttpResponseMessage Get_Change_Academic_year(string cur_code, string academic_year, string grade_code)
        {
            if (cur_code == "undefined")
            {
                cur_code = null;
            }
            if (academic_year == "undefined")
            {
                academic_year = null;
            }
            if (grade_code == "undefined")
            {
                grade_code = null;
            }

            List<Sim038> mod_list = new List<Sim038>();
           

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_change_academic_year_for_admission_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'A'),
                              new SqlParameter("@sims_cur_code", cur_code),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();

                            simsobj.sims_admission_number= dr["sims_admission_number"].ToString();
                            simsobj.full_name= dr["full_name"].ToString();
                            simsobj.sims_admission_date= dr["sims_admission_date"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                           
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        [Route("Get_Change_Academic_year")]
        public HttpResponseMessage Get_Change_Academic_year(string cur_code, string academic_year, string grade_code, string admission_no)
        {
            if (cur_code == "undefined")
            {
                cur_code = null;
            }
            if (academic_year == "undefined")
            {
                academic_year = null;
            }
            if (grade_code == "undefined")
            {
                grade_code = null;
            }

            if (admission_no == "undefined")
            {
                admission_no = null;
            }

            List<Sim038> mod_list = new List<Sim038>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_change_academic_year_for_admission_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'A'),
                              new SqlParameter("@sims_cur_code", cur_code),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_grade_code", grade_code),
                              new SqlParameter("@sims_admission_number", admission_no),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();

                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            simsobj.full_name = dr["full_name"].ToString();
                            simsobj.sims_admission_date = dr["sims_admission_date"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }




        [Route("CUDChange_Academic_year")]
        public HttpResponseMessage CUDChange_Academic_year(List<Sim038> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSection_Screening(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CUDSection_Screening"));
            bool inserted = false;
          
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sim038 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("sims.sims_change_academic_year_for_admission_proc",
                                new List<SqlParameter>() 
                            {                             
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_academic_year", simsobj.sims_admission_academic_year),
                                new SqlParameter("@sims_previous_academic_year", simsobj.sims_student_attribute12),

                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code_old),
                                new SqlParameter("@sims_cur_code_new", simsobj.sims_cur_code_new),


                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code_old),
                                new SqlParameter("@sims_grade_code_new", simsobj.sims_grade_code_new),

                                
                                




                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                               
                               
                          });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getGradeScale")]
        public HttpResponseMessage getGradeScale(string cur_code,string academic_year)
        {
            List<Sim038> criteria = new List<Sim038>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_screening_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'E'),
                        new SqlParameter("@sims_cur_code1", cur_code),
                        new SqlParameter("@sims_academic_year1", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 crit = new Sim038();
                            crit.sims_mark_assesment_grade = dr["sims_mark_assesment_grade"].ToString();
                            crit.sims_mark_assesment_grade_name = dr["sims_mark_assesment_grade_name"].ToString();
                            crit.sims_mark_assesment_grade_description = dr["sims_mark_assesment_grade_description"].ToString();                            
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

        [Route("getRating")]
        public HttpResponseMessage getRating()
        {
            List<Sim038> mod_list = new List<Sim038>();
            
            

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_screening_proc",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();
                            simsobj.sims_survey_rating_group_code = dr["sims_survey_rating_group_code"].ToString();
                            simsobj.sims_survey_rating_group_desc_en = dr["sims_survey_rating_group_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }



    }
}