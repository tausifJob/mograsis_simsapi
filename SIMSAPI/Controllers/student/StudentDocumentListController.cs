﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.StudentDocumentListController
{
    public class StudentDocumentListController : ApiController
    {
        [Route("getStudentDocumentList")]
        public HttpResponseMessage getStudentDocumentList(string cur_name, string academic_year, string grd_code, string sec_code, string student_en)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims561> mod_list = new List<Sims561>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_document_details_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                            simsobj.sims_enroll_number = dr["sims_admission_enroll_number"].ToString();
                            simsobj.student_full_name = dr["sims_student_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["Garde_Name"].ToString();
                            simsobj.sims_section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.sims_section_name = dr["Section_Name"].ToString();
                            simsobj.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
                            simsobj.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
                            simsobj.sims_admission_date_created = db.UIDDMMYYYYformat(dr["sims_admission_date_created"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

    }
}