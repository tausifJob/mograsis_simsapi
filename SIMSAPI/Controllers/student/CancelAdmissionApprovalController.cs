﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/CancelAdmissionApproval")]
    public class CancelAdmissionApprovalController : ApiController
    {
        [Route("getCancelAdmissionApprovalRequestList")]
        public HttpResponseMessage getCancelAdmissionApprovalRequestList(string user, string status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_emp_id", user),
                            new SqlParameter("@sims_tc_certificate_request_status", status),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_certificate_request_number = dr["sims_tc_certificate_request_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_request_date"].ToString()))
                                simsobj.sims_tc_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_request_date"].ToString());

                           

                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_expected_date"].ToString()))
                                simsobj.sims_tc_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_expected_date"].ToString());

                            simsobj.sims_enroll_number = dr["sims_tc_certificate_enroll_number"].ToString();
                            simsobj.sims_tc_certificate_requested_by = dr["sims_tc_certificate_requested_by"].ToString();
                            simsobj.sims_tc_certificate_reason = dr["sims_tc_certificate_reason"].ToString();
                            simsobj.sims_tc_certificate_request_status = dr["sims_tc_certificate_request_status"].ToString(); // obj.sims_academic_year;
                            simsobj.sims_tc_certificate_cur_code = dr["sims_tc_certificate_cur_code"].ToString();
                            simsobj.sims_tc_certificate_academic_year_desc = dr["sims_tc_certificate_academic_year_desc"].ToString();
                            simsobj.sims_tc_certificate_academic_year = dr["sims_tc_certificate_academic_year"].ToString();
                            
                            


                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getCancelAdmissionApprovalAccessList")]
        public HttpResponseMessage getCancelAdmissionApprovalAccessList(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim999> mod_list = new List<Sim999>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@sims_emp_id", user),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim999 obj = new Sim999();
                            obj.sims_clr_sr_no = dr["sims_clr_sr_no"].ToString();
                            obj.sims_clr_cur_code = dr["sims_clr_cur_code"].ToString();
                            obj.sims_clr_academic_year = dr["sims_clr_academic_year"].ToString();
                            obj.sims_clr_employee_id = dr["sims_clr_employee_id"].ToString();
                            obj.sims_clr_access_code = dr["sims_clr_access_code"].ToString();
                            obj.sims_clr_class_code = dr["sims_clr_class_code"].ToString();
                            obj.sims_clr_preference_order = dr["sims_clr_preference_order"].ToString();
                            obj.sims_clr_status = dr["sims_clr_status"].ToString();
                            obj.sims_clr_created_by = dr["sims_clr_created_by"].ToString();
                            obj.sims_clr_created_date = db.UIDDMMYYYYformat(dr["sims_clr_created_date"].ToString());
                            obj.sims_clr_access_from_date = db.UIDDMMYYYYformat(dr["sims_clr_access_from_date"].ToString());
                            obj.sims_clr_access_to_date = db.UIDDMMYYYYformat(dr["sims_clr_access_to_date"].ToString());
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getApprovalDept")]
        public HttpResponseMessage getApprovalDept(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getApprovalDept()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() {                             
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_emp_id", user),                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            simsobj.sims_fee_clr_status = dr["sims_fee_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_finn_clr_status = dr["sims_finn_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_inv_clr_status = dr["sims_inv_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_inci_clr_status = dr["sims_inci_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_lib_clr_status = dr["sims_lib_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_trans_clr_status = dr["sims_trans_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_acad_clr_status = dr["sims_acad_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_clr_super_user_flag = dr["sims_clr_super_user_flag"].Equals("T") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("AdmCheckedDept")]
        public HttpResponseMessage CheckedDept(string enroll)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : AdmCheckedDept()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() {                             
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_enroll_number", enroll),                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            simsobj.sims_fee_clr_status = dr["sims_fee_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_fee_clr_remark = dr["sims_fee_clr_remark"].ToString();
                            simsobj.sims_finn_clr_status = dr["sims_finn_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_finn_clr_remark = dr["sims_finn_clr_remark"].ToString();
                            simsobj.sims_inv_clr_status = dr["sims_inv_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_inv_clr_remark = dr["sims_inv_clr_remark"].ToString();
                            simsobj.sims_inci_clr_status = dr["sims_inci_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_inci_clr_remark = dr["sims_inci_clr_remark"].ToString();
                            simsobj.sims_lib_clr_status = dr["sims_lib_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_lib_clr_remark = dr["sims_lib_clr_remark"].ToString();
                            simsobj.sims_trans_clr_status = dr["sims_trans_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_trans_clr_remark = dr["sims_trans_clr_remark"].ToString();
                            simsobj.sims_acad_clr_status = dr["sims_acad_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_acad_clr_remark = dr["sims_acad_clr_remark"].ToString();
                            simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_admin_clr_remark = dr["sims_admin_clr_remark"].ToString();
                            simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_other1_clr_remark = dr["sims_other1_clr_remark"].ToString();
                            simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_other2_clr_remark = dr["sims_other2_clr_remark"].ToString();
                            simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].Equals("A") ? true : false;
                            simsobj.sims_other3_clr_remark = dr["sims_other3_clr_remark"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("InsertClearStatus")]
        public HttpResponseMessage InsertClearStatus(List<Sim670> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim670 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_certificate_tc_clearance]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),                                
                                //new SqlParameter("@sims_sr_no", simsobj.sims_sr_no),
                                new SqlParameter("@sims_enroll_number",simsobj.sims_enroll_number),
                                new SqlParameter("@sims_fee_clr_status",simsobj.sims_fee_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_fee_clr_by",simsobj.sims_fee_clr_by),
                                new SqlParameter("@sims_fee_clr_date",db.DBYYYYMMDDformat(simsobj.sims_fee_clr_date)),
                                new SqlParameter("@sims_fee_clr_remark", simsobj.sims_fee_clr_remark),
                                new SqlParameter("@sims_finn_clr_status", simsobj.sims_finn_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_finn_clr_by", simsobj.sims_finn_clr_by),
                                new SqlParameter("@sims_finn_clr_date", db.DBYYYYMMDDformat(simsobj.sims_finn_clr_date)),
                                new SqlParameter("@sims_finn_clr_remark", simsobj.sims_finn_clr_remark),
                                new SqlParameter("@sims_inv_clr_status", simsobj.sims_inv_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_inv_clr_by", simsobj.sims_inv_clr_by),
                                new SqlParameter("@sims_inv_clr_date", db.DBYYYYMMDDformat(simsobj.sims_inv_clr_date)),
                                new SqlParameter("@sims_inv_clr_remark", simsobj.sims_inv_clr_remark),
                                new SqlParameter("@sims_inci_clr_status", simsobj.sims_inci_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_inci_clr_by", simsobj.sims_inci_clr_by),
                                new SqlParameter("@sims_inci_clr_date", db.DBYYYYMMDDformat(simsobj.sims_inci_clr_date)),
                                new SqlParameter("@sims_inci_clr_remark", simsobj.sims_inci_clr_remark),
                                new SqlParameter("@sims_lib_clr_status", simsobj.sims_lib_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_lib_clr_by", simsobj.sims_lib_clr_by),
                                new SqlParameter("@sims_lib_clr_date", db.DBYYYYMMDDformat(simsobj.sims_lib_clr_date)),
                                new SqlParameter("@sims_lib_clr_remark", simsobj.sims_lib_clr_remark),
                                new SqlParameter("@sims_trans_clr_status", simsobj.sims_trans_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_trans_clr_by", simsobj.sims_trans_clr_by),
                                new SqlParameter("@sims_trans_clr_date", db.DBYYYYMMDDformat(simsobj.sims_trans_clr_date)),
                                new SqlParameter("@sims_trans_clr_remark", simsobj.sims_trans_clr_remark),
                                new SqlParameter("@sims_acad_clr_status", simsobj.sims_acad_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_acad_clr_by", simsobj.sims_acad_clr_by),
                                new SqlParameter("@sims_acad_clr_date", db.DBYYYYMMDDformat(simsobj.sims_acad_clr_date)),
                                new SqlParameter("@sims_acad_clr_remark", simsobj.sims_acad_clr_remark),
                                new SqlParameter("@sims_admin_clr_status", simsobj.sims_admin_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_admin_clr_by", simsobj.sims_admin_clr_by),
                                new SqlParameter("@sims_admin_clr_date", db.DBYYYYMMDDformat(simsobj.sims_admin_clr_date)),
                                new SqlParameter("@sims_admin_clr_remark", simsobj.sims_admin_clr_remark),
                                new SqlParameter("@sims_other1_clr_status", simsobj.sims_other1_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_other1_clr_by", simsobj.sims_other1_clr_by),
                                new SqlParameter("@sims_other1_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other1_clr_date)),
                                new SqlParameter("@sims_other1_clr_remark", simsobj.sims_other1_clr_remark),
                                new SqlParameter("@sims_other2_clr_status", simsobj.sims_other2_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_other2_clr_by", simsobj.sims_other2_clr_by),
                                new SqlParameter("@sims_other2_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other2_clr_date)),
                                new SqlParameter("@sims_other2_clr_remark", simsobj.sims_other2_clr_remark),
                                new SqlParameter("@sims_other3_clr_status", simsobj.sims_other3_clr_status==true?"A":"P"),
                                new SqlParameter("@sims_other3_clr_by", simsobj.sims_other3_clr_by),
                                new SqlParameter("@sims_other3_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other3_clr_date)),
                                new SqlParameter("@sims_other3_clr_remark", simsobj.sims_other3_clr_remark),
                                new SqlParameter("@sims_clr_status", simsobj.sims_clr_status==true?"A":"P")                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getgradesaction")]
        public HttpResponseMessage getgradesaction(string enroll, string year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getApprovalDept()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() {                             
                            new SqlParameter("@opr", "K"),
                            new SqlParameter("@sims_enroll_number", enroll),
                            new SqlParameter("@sims_academic_year",year)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getsectionfeedetails")]
        public HttpResponseMessage getsectionfeedetails(string enroll, string cur, string ac_year, string sec)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getsectionfeedetails()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() {                             
                            new SqlParameter("@opr", "Q"),
                            new SqlParameter("@sims_enroll_number", enroll),
                            new SqlParameter("@sims_tc_certificate_cur_code", cur),
                            new SqlParameter("@sims_academic_year",ac_year),
                            new SqlParameter("@sims_section_code",sec)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_period1 = dr["january_fees"].ToString();
                            simsobj.sims_fee_period1_paid = dr["paid1"].ToString();
                            simsobj.sims_fee_period1_concession_paid = dr["concession_paid1"].ToString();

                            simsobj.sims_fee_period2 = dr["february_fees"].ToString();
                            simsobj.sims_fee_period2_paid = dr["paid2"].ToString();
                            simsobj.sims_fee_period2_concession_paid = dr["concession_paid2"].ToString();

                            simsobj.sims_fee_period3 = dr["march_fees"].ToString();
                            simsobj.sims_fee_period3_paid = dr["paid3"].ToString();
                            simsobj.sims_fee_period3_concession_paid = dr["concession_paid3"].ToString();

                            simsobj.sims_fee_period4 = dr["april_fees"].ToString();
                            simsobj.sims_fee_period4_paid = dr["paid4"].ToString();
                            simsobj.sims_fee_period4_concession_paid = dr["concession_paid4"].ToString();

                            simsobj.sims_fee_period5 = dr["may_fees"].ToString();
                            simsobj.sims_fee_period5_paid = dr["paid5"].ToString();
                            simsobj.sims_fee_period5_concession_paid = dr["concession_paid5"].ToString();

                            simsobj.sims_fee_period6 = dr["june_fees"].ToString();
                            simsobj.sims_fee_period6_paid = dr["paid6"].ToString();
                            simsobj.sims_fee_period6_concession_paid = dr["concession_paid6"].ToString();

                            simsobj.sims_fee_period7 = dr["july_fees"].ToString();
                            simsobj.sims_fee_period7_paid = dr["paid7"].ToString();
                            simsobj.sims_fee_period7_concession_paid = dr["concession_paid7"].ToString();

                            simsobj.sims_fee_period8 = dr["august_fees"].ToString();
                            simsobj.sims_fee_period8_paid = dr["paid8"].ToString();
                            simsobj.sims_fee_period8_concession_paid = dr["concession_paid8"].ToString();

                            simsobj.sims_fee_period9 = dr["september_fees"].ToString();
                            simsobj.sims_fee_period9_paid = dr["paid9"].ToString();
                            simsobj.sims_fee_period9_concession_paid = dr["concession_paid9"].ToString();

                            simsobj.sims_fee_period10 = dr["october_fees"].ToString();
                            simsobj.sims_fee_period10_paid = dr["paid10"].ToString();
                            simsobj.sims_fee_period10_concession_paid = dr["concession_paid10"].ToString();

                            simsobj.sims_fee_period11 = dr["november_fees"].ToString();
                            simsobj.sims_fee_period11_paid = dr["paid11"].ToString();
                            simsobj.sims_fee_period11_concession_paid = dr["concession_paid11"].ToString();

                            simsobj.sims_fee_period12 = dr["december_fees"].ToString();
                            simsobj.sims_fee_period12_paid = dr["paid12"].ToString();
                            simsobj.sims_fee_period12_concession_paid = dr["concession_paid12"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("UpdateFeeDetails")]
        public HttpResponseMessage UpdateFeeDetails(List<Sim670> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim670 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_certificate_tc_clearance]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", 'U'),                                
                          //new SqlParameter("@sims_sr_no", simsobj.sims_sr_no),
                          new SqlParameter("@sims_enroll_number",simsobj.sims_enroll_number),
                          new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code), 
                          new SqlParameter("@sims_section_code",simsobj.sims_section_code), 
                          new SqlParameter("@sims_fee_period1_paid",simsobj.sims_fee_period1_paid),
                          new SqlParameter("@sims_fee_period2_paid",simsobj.sims_fee_period2_paid),
                          new SqlParameter("@sims_fee_period3_paid",simsobj.sims_fee_period3_paid),
                          new SqlParameter("@sims_fee_period4_paid",simsobj.sims_fee_period4_paid),
                          new SqlParameter("@sims_fee_period5_paid",simsobj.sims_fee_period5_paid),
                          new SqlParameter("@sims_fee_period6_paid",simsobj.sims_fee_period6_paid),
                          new SqlParameter("@sims_fee_period7_paid",simsobj.sims_fee_period7_paid),
                          new SqlParameter("@sims_fee_period8_paid",simsobj.sims_fee_period8_paid),
                          new SqlParameter("@sims_fee_period9_paid",simsobj.sims_fee_period9_paid),
                          new SqlParameter("@sims_fee_period10_paid",simsobj.sims_fee_period10_paid),
                          new SqlParameter("@sims_fee_period11_paid",simsobj.sims_fee_period11_paid),
                          new SqlParameter("@sims_fee_period12_paid",simsobj.sims_fee_period12_paid)
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("AllRequestDetailsList")]
        public HttpResponseMessage AllRequestDetailsList(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_emp_id", user),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_certificate_request_number = dr["sims_tc_certificate_request_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_request_date"].ToString()))
                                simsobj.sims_tc_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_request_date"].ToString());
                            simsobj.sims_tc_certificate_requested_by = dr["sims_tc_certificate_requested_by"].ToString();
                            simsobj.sims_tc_certificate_reason = dr["sims_tc_certificate_reason"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_expected_date"].ToString()))
                                simsobj.sims_tc_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_expected_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_tc_certificate_enroll_number"].ToString();
                            //simsobj.sims_student_name = string.Format("{0}({1})", dr["StudentName"].ToString(), simsobj.sims_enroll_number);
                            //simsobj.sims_tc_certificate_issued_by = dr["sims_tc_certificate_issued_by"].ToString();
                            simsobj.sims_tc_certificate_request_status = dr["sims_tc_certificate_request_status"].ToString(); // obj.sims_academic_year;
                            simsobj.sims_tc_certificate_cur_code = dr["sims_tc_certificate_cur_code"].ToString();
                            simsobj.sims_tc_certificate_academic_year_desc = dr["sims_tc_certificate_academic_year_desc"].ToString();

                            //simsobj.sims_clr_sr_no = dr["sims_clr_sr_no"].ToString();
                            //simsobj.sims_clr_cur_code = dr["sims_clr_cur_code"].ToString();
                            //simsobj.sims_clr_academic_year = dr["sims_clr_academic_year"].ToString();
                            //simsobj.sims_clr_employee_id = dr["sims_clr_employee_id"].ToString();
                            //simsobj.sims_clr_access_code = dr["sims_clr_access_code"].ToString();
                            //simsobj.sims_clr_class_code = dr["sims_clr_class_code"].ToString();
                            //simsobj.sims_clr_preference_order = dr["sims_clr_preference_order"].ToString();
                            //simsobj.sims_clr_status = dr["sims_clr_status"].ToString();
                            //simsobj.sims_clr_created_by = dr["sims_clr_created_by"].ToString();
                            //simsobj.sims_clr_created_date = db.UIDDMMYYYYformat(dr["sims_clr_created_date"].ToString());
                            //simsobj.sims_clr_access_from_date = db.UIDDMMYYYYformat(dr["sims_clr_access_from_date"].ToString());
                            //simsobj.sims_clr_access_to_date = db.UIDDMMYYYYformat(dr["sims_clr_access_to_date"].ToString());

                            simsobj.sims_sr_no = dr["sims_sr_no"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_clr_access_code = dr["sims_clr_access_code"].ToString();
                            simsobj.sims_clr_class_code = dr["sims_clr_class_code"].ToString();
                            simsobj.sims_clr_status = dr["sims_clr_status"].ToString();
                            simsobj.sims_clr_by = dr["sims_clr_by"].ToString();
                            simsobj.sims_clr_date = db.UIDDMMYYYYformat(dr["sims_clr_date"].ToString());
                            simsobj.sims_clr_remark = dr["sims_clr_remark"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getApprovalDeptNew")]
        public HttpResponseMessage getApprovalDeptNew(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getApprovalDept()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sim670> mod_list = new List<Sim670>();
            Sim670 simsobj = new Sim670();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                        new List<SqlParameter>() {                             
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_emp_id", user),                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            if (dr["sims_clr_access_code"].ToString() == "1")
                                simsobj.sims_fee_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "2")
                                simsobj.sims_finn_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "3")
                                simsobj.sims_trans_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "4")
                                simsobj.sims_inci_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "5")
                                simsobj.sims_acad_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "6")
                                simsobj.sims_inv_clr_status = true;

                            if (dr["sims_clr_access_code"].ToString() == "7")
                                simsobj.sims_lib_clr_status = true;
                             
                           
                            
                            //simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].Equals("T") ? true : false;
                            //simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].Equals("T") ? true : false;
                            //simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("T") ? true : false;
                            //simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].Equals("T") ? true : false;
                            //simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("T") ? true : false;
                            //simsobj.sims_clr_super_user_flag = dr["sims_clr_super_user_flag"].Equals("T") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("InsertClearStatusNew")]
        public HttpResponseMessage InsertClearStatusNew(List<Sim670> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim670 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_certificate_tc_clearance]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),                                
                                //new SqlParameter("@sims_sr_no", simsobj.sims_sr_no),
                                new SqlParameter("@sims_enroll_number",simsobj.sims_enroll_number),
                                new SqlParameter("@sims_clr_tran_status",simsobj.sims_clr_tran_status==true?"A":"P"),
                                new SqlParameter("@sims_clr_tran_employee_id",simsobj.sims_clr_tran_employee_id),
                                new SqlParameter("@sims_clr_tran_created_date",db.DBYYYYMMDDformat(simsobj.sims_clr_tran_created_date)),
                                new SqlParameter("@sims_clr_tran_remark", simsobj.sims_clr_tran_remark),
                                new SqlParameter("@sims_clr_tran_access_code", simsobj.sims_clr_tran_access_code),

                                //new SqlParameter("@sims_finn_clr_status", simsobj.sims_finn_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_finn_clr_by", simsobj.sims_finn_clr_by),
                                //new SqlParameter("@sims_finn_clr_date", db.DBYYYYMMDDformat(simsobj.sims_finn_clr_date)),
                                //new SqlParameter("@sims_finn_clr_remark", simsobj.sims_finn_clr_remark),
                                //new SqlParameter("@sims_inv_clr_status", simsobj.sims_inv_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_inv_clr_by", simsobj.sims_inv_clr_by),
                                //new SqlParameter("@sims_inv_clr_date", db.DBYYYYMMDDformat(simsobj.sims_inv_clr_date)),
                                //new SqlParameter("@sims_inv_clr_remark", simsobj.sims_inv_clr_remark),
                                //new SqlParameter("@sims_inci_clr_status", simsobj.sims_inci_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_inci_clr_by", simsobj.sims_inci_clr_by),
                                //new SqlParameter("@sims_inci_clr_date", db.DBYYYYMMDDformat(simsobj.sims_inci_clr_date)),
                                //new SqlParameter("@sims_inci_clr_remark", simsobj.sims_inci_clr_remark),
                                //new SqlParameter("@sims_lib_clr_status", simsobj.sims_lib_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_lib_clr_by", simsobj.sims_lib_clr_by),
                                //new SqlParameter("@sims_lib_clr_date", db.DBYYYYMMDDformat(simsobj.sims_lib_clr_date)),
                                //new SqlParameter("@sims_lib_clr_remark", simsobj.sims_lib_clr_remark),
                                //new SqlParameter("@sims_trans_clr_status", simsobj.sims_trans_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_trans_clr_by", simsobj.sims_trans_clr_by),
                                //new SqlParameter("@sims_trans_clr_date", db.DBYYYYMMDDformat(simsobj.sims_trans_clr_date)),
                                //new SqlParameter("@sims_trans_clr_remark", simsobj.sims_trans_clr_remark),
                                //new SqlParameter("@sims_acad_clr_status", simsobj.sims_acad_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_acad_clr_by", simsobj.sims_acad_clr_by),
                                //new SqlParameter("@sims_acad_clr_date", db.DBYYYYMMDDformat(simsobj.sims_acad_clr_date)),
                                //new SqlParameter("@sims_acad_clr_remark", simsobj.sims_acad_clr_remark),
                                //new SqlParameter("@sims_admin_clr_status", simsobj.sims_admin_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_admin_clr_by", simsobj.sims_admin_clr_by),
                                //new SqlParameter("@sims_admin_clr_date", db.DBYYYYMMDDformat(simsobj.sims_admin_clr_date)),
                                //new SqlParameter("@sims_admin_clr_remark", simsobj.sims_admin_clr_remark),
                                //new SqlParameter("@sims_other1_clr_status", simsobj.sims_other1_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_other1_clr_by", simsobj.sims_other1_clr_by),
                                //new SqlParameter("@sims_other1_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other1_clr_date)),
                                //new SqlParameter("@sims_other1_clr_remark", simsobj.sims_other1_clr_remark),
                                //new SqlParameter("@sims_other2_clr_status", simsobj.sims_other2_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_other2_clr_by", simsobj.sims_other2_clr_by),
                                //new SqlParameter("@sims_other2_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other2_clr_date)),
                                //new SqlParameter("@sims_other2_clr_remark", simsobj.sims_other2_clr_remark),
                                //new SqlParameter("@sims_other3_clr_status", simsobj.sims_other3_clr_status==true?"A":"P"),
                                //new SqlParameter("@sims_other3_clr_by", simsobj.sims_other3_clr_by),
                                //new SqlParameter("@sims_other3_clr_date", db.DBYYYYMMDDformat(simsobj.sims_other3_clr_date)),
                                //new SqlParameter("@sims_other3_clr_remark", simsobj.sims_other3_clr_remark),
                                //new SqlParameter("@sims_clr_status", simsobj.sims_clr_status==true?"A":"P")
                        });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDTccertificaterequiestUpdate")]
        public HttpResponseMessage CUDTccertificaterequiestUpdate(List<Sims668> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims668 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_tc_clearance",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_tc_certificate_cur_code", simsobj.sims_tc_certificate_cur_code),
                            new SqlParameter("@sims_tc_certificate_academic_year", simsobj.sims_tc_certificate_academic_year),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_tc_certificate_reason", simsobj.sims_tc_certificate_reason),
                            new SqlParameter("@sims_tc_certificate_requested_by", simsobj.sims_tc_certificate_requested_by),
                            new SqlParameter("@sims_tc_certificate_request_date",db.DBYYYYMMDDformat(simsobj.sims_tc_certificate_request_date)),
                            
                             

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}