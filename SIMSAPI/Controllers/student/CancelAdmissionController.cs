﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/CancelAdmission")]

    public class CancelAdmissionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[Route("getCancelAdmission")]
        //public HttpResponseMessage getCancelAdmission(string enroll_no)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CancelAdmission(),PARAMETERS :: enroll_no{2}";
        //    Log.Debug(string.Format(debug, "STUDENT", "CancelAdmission", enroll_no));

        //    CancelAdmission cnclAdm = new CancelAdmission();
        //    cnclAdm.Enroll = enroll_no;
        //    Sims010_Auth aut = new Sims010_Auth();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CANCEL_ADMISSION_proc]",
        //                new List<SqlParameter>() 
        //                { 
        //                      // new SqlParameter("@opr", 'X'),
        //                       new SqlParameter("@ENROLL", enroll_no),
        //                       new SqlParameter("@FEE_IGNORED", "0")
        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    aut.admission_date = dr["sims_student_date"].ToString();
        //                    aut.birth_date = dr["sims_student_dob"].ToString();
        //                    aut.curr_code = dr["CurrName"].ToString() + "/" + dr["academic_year"].ToString() + "/" + dr["GradeName"].ToString() + "/" + dr["SecName"].ToString();
        //                    aut.curr_name = dr["Curr_Code"].ToString();
        //                    aut.academic_year = dr["academic_year"].ToString();
        //                    aut.grade_code = dr["Grade_Code"].ToString();
        //                    aut.section_code = dr["Section_Code"].ToString();
        //                    aut.FatherName = dr["FATHER_NAME"].ToString();
        //                    aut.gender = dr["sims_student_gender"].ToString();
        //                    aut.stud_full_name_reg = dr["StudFullNameOther"].ToString();
        //                    aut.Stud_Summary_Address = dr["SUMMARY_ADDRESS"].ToString();
        //                    aut.House = dr["sims_student_house"].ToString();
        //                    aut.stud_full_name = dr["StudFullName"].ToString();
        //                    aut.ClassTeacherName = "";
        //                    aut.FatherNameReg = dr["FATHER_NAME_OTHER"].ToString();
        //                    aut.FatherSummary_Address = dr["SUMMARY_ADDRESS"].ToString();
        //                    cnclAdm.StudentDet = new Sims010_Auth();
        //                    cnclAdm.StudentDet = aut;
        //                }
        //                dr.NextResult();
        //                cnclAdm.FeeDet = new List<FeeDets>();
        //                while (dr.Read())
        //                {
        //                    FeeDets fd = new FeeDets();
        //                    fd.Concession = dr["CONCESSION"].ToString();
        //                    fd.FeeDescription = dr["FEE_DESC"].ToString();
        //                    fd.PaidAmmount = dr["PAID_AMT"].ToString();
        //                    fd.RemainingAmmount = dr["REMAINING_AMMOUNT"].ToString();
        //                    fd.TotalAmmount = dr["TOTAL_FEES"].ToString();
        //                    fd.FeeCode = dr["sims_fee_code"].ToString();
        //                    fd.Ignored = false;
        //                    cnclAdm.FeeDet.Add(fd);
        //                }
        //                dr.NextResult();
        //                cnclAdm.IncidenceDet = new List<IncidenceDets>();
        //                while (dr.Read())
        //                {
        //                    IncidenceDets id = new IncidenceDets();
        //                    id.ActionDesc = dr["INCIDENCE_ACTION"].ToString();
        //                    id.Building = dr["BUILD_DESC"].ToString();
        //                    id.Description = "";
        //                    //id.incidence = dr[""].ToString();
        //                    id.Location = dr["LOCATION_DESC"].ToString();
        //                    id.Status = dr["sims_incidence_status"].ToString();
        //                    id.Warning = dr["INCIDENCE_WARNING"].ToString();
        //                    cnclAdm.IncidenceDet.Add(id);
        //                }
        //                dr.NextResult();
        //                cnclAdm.LibDet = new List<LibDets>();
        //                while (dr.Read())
        //                {
        //                    LibDets lb = new LibDets();
        //                    lb.BookName = dr["ITEM_DESC"].ToString();
        //                    lb.ExpctdReturnDate = dr["sims_library_item_expected_return_date"].ToString();
        //                    lb.ItemRate = dr["sims_library_item_rate"].ToString();
        //                    lb.Quantity = dr["sims_library_item_qty"].ToString();
        //                    lb.Status = dr["sims_library_item_transaction_status"].ToString();
        //                    cnclAdm.LibDet.Add(lb);
        //                }
        //                dr.NextResult();
        //                cnclAdm.ExamDet = new List<ExamDet>();
        //                while (dr.Read())
        //                {
        //                    ExamDet ed = new ExamDet();
        //                    ed.BoardName = dr["BOARD_NAME"].ToString();
        //                    ed.ExamName = dr["EXAM_NAME"].ToString();
        //                    ed.RegNumber = dr["sims_board_exam_registration_no"].ToString();
        //                    ed.Status = dr["sims_board_exam_student_status"].ToString();
        //                    cnclAdm.ExamDet.Add(ed);
        //                }
        //                dr.NextResult();
        //                cnclAdm.InvsDet = new List<InvsDets>();
        //                while (dr.Read())
        //                {
        //                    InvsDets id = new InvsDets();
        //                    id.Ammount = dr["dd_sale_value_final"].ToString();
        //                    id.ItemName = dr["im_desc"].ToString();
        //                    id.ItemQty = dr["dd_outstanding_qty"].ToString();
        //                    cnclAdm.InvsDet.Add(id);
        //                }
        //            }
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, cnclAdm);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, cnclAdm);
        //    }
        //}

        [Route("CancelAdmission")]
        public HttpResponseMessage CancelAdmission(string enroll_no, string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CancelAdmission(),PARAMETERS :: enroll_no{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CancelAdmission", enroll_no));

            CancelAdmission cnclAdm = new CancelAdmission();
            cnclAdm.Enroll = enroll_no;
            Sims010_Auth aut = new Sims010_Auth();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CANCEL_ADMISSION_proc]",
                        new List<SqlParameter>() 
                         { 
                               // new SqlParameter("@opr", 'X'),
                                new SqlParameter("@ENROLL", enroll_no),
                                new SqlParameter("@CURR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acad_yr),
                                new SqlParameter("@FEE_IGNORED", "0")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            aut.admission_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                            aut.birth_date = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            aut.curr_code = dr["CurrName"].ToString() + "/" + dr["academic_year"].ToString() + "/" + dr["GradeName"].ToString() + "/" + dr["SecName"].ToString();
                            aut.curr_name = dr["CurrName"].ToString();
                            aut.academic_year = dr["academic_year"].ToString();
                            aut.academic_year_desc = dr["academic_year_desc"].ToString();
                            aut.grade_code = dr["Grade_Code"].ToString();
                            aut.section_code = dr["Section_Code"].ToString();
                            aut.FatherName = dr["FATHER_NAME"].ToString();
                            aut.gender = dr["sims_student_gender"].ToString();
                            aut.stud_full_name_reg = dr["StudFullNameOther"].ToString();
                            aut.Stud_Summary_Address = dr["SUMMARY_ADDRESS"].ToString();
                            aut.House = dr["sims_student_house"].ToString();
                            aut.stud_full_name = dr["StudFullName"].ToString();
                            aut.ClassTeacherName = dr["ClassTeacherName"].ToString();
                            aut.TCApplied = dr["TCApplied"].ToString();
                            aut.FatherNameReg = dr["FATHER_NAME_OTHER"].ToString();
                            aut.FatherSummary_Address = dr["SUMMARY_ADDRESS"].ToString();
                            aut.empl_code = dr["GradeName"].ToString() + "/" + dr["SecName"].ToString();
                            cnclAdm.StudentDet = new Sims010_Auth();
                            cnclAdm.StudentDet = aut;

                            //aut.admission_date = dr["sims_student_date"].ToString();
                            //aut.birth_date = dr["sims_student_dob"].ToString();
                            //aut.curr_name = dr["CurrName"].ToString();
                            //aut.empl_code = dr["GradeName"].ToString() + "/" + dr["SecName"].ToString();
                            //aut.curr_code = dr["Curr_Code"].ToString();
                            //aut.academic_year = dr["academic_year"].ToString();
                            //aut.academic_year_desc = dr["academic_year_desc"].ToString();
                            //aut.grade_code = dr["Grade_Code"].ToString();
                            //aut.grade_name = dr["GradeName"].ToString();
                            //aut.section_code = dr["Section_Code"].ToString();
                            //aut.section_name = dr["SecName"].ToString();
                            //aut.FatherName = dr["FATHER_NAME"].ToString();
                            //aut.gender = dr["sims_student_gender"].ToString();
                            //aut.ClassTeacherName = "";
                            //aut.stud_full_name_reg = dr["StudFullNameOther"].ToString();
                            //aut.Stud_Summary_Address = dr["SUMMARY_ADDRESS"].ToString();
                            //aut.House = dr["sims_student_house"].ToString();
                            //aut.stud_full_name = dr["StudFullName"].ToString();
                            //aut.TCApplied = dr["TCApplied"].ToString();
                            //aut.FatherNameReg = dr["FATHER_NAME_OTHER"].ToString();
                            //aut.FatherSummary_Address = dr["SUMMARY_ADDRESS"].ToString();
                            //cnclAdm.StudentDet = new Sims010_Auth();
                            //cnclAdm.StudentDet = aut;
                            //aut.NextYearReserved = dr["NextYearReserved"].ToString();
                        }
                        dr.NextResult();
                        cnclAdm.FeeDet = new List<FeeDets>();
                        while (dr.Read())
                        {
                            FeeDets fd = new FeeDets();
                            fd.Concession = dr["CONCESSION"].ToString();
                            fd.FeeDescription = dr["FEE_DESC"].ToString();
                            fd.PaidAmmount = dr["PAID_AMT"].ToString();
                            fd.RemainingAmmount = dr["REMAINING_AMMOUNT"].ToString();
                            fd.TotalAmmount = dr["TOTAL_FEES"].ToString();
                            fd.FeeCode = dr["sims_fee_code"].ToString();
                            fd.Ignored = false;
                            cnclAdm.FeeDet.Add(fd);
                        }
                        dr.NextResult();
                        cnclAdm.IncidenceDet = new List<IncidenceDets>();
                        while (dr.Read())
                        {
                            IncidenceDets id = new IncidenceDets();
                            id.ActionDesc = dr["INCIDENCE_ACTION"].ToString();
                            id.Building = dr["BUILD_DESC"].ToString();
                            id.Description = "";
                            id.Location = dr["LOCATION_DESC"].ToString();
                            id.Status = dr["sims_incidence_status"].ToString();
                            id.Warning = dr["INCIDENCE_WARNING"].ToString();
                            cnclAdm.IncidenceDet.Add(id);
                        }
                        dr.NextResult();
                        cnclAdm.LibDet = new List<LibDets>();
                        while (dr.Read())
                        {
                            LibDets lb = new LibDets();
                            lb.BookName = dr["ITEM_DESC"].ToString();
                            lb.ExpctdReturnDate = db.UIDDMMYYYYformat(dr["sims_library_item_expected_return_date"].ToString());
                            lb.ItemRate = dr["sims_library_item_rate"].ToString();
                            lb.Quantity = dr["sims_library_item_qty"].ToString();
                            lb.Status = dr["sims_library_item_transaction_status"].ToString();
                            cnclAdm.LibDet.Add(lb);
                        }
                        dr.NextResult();
                        cnclAdm.ExamDet = new List<ExamDet>();
                        while (dr.Read())
                        {
                            ExamDet ed = new ExamDet();
                            ed.BoardName = dr["BOARD_NAME"].ToString();
                            ed.ExamName = dr["EXAM_NAME"].ToString();
                            ed.RegNumber = dr["sims_board_exam_registration_no"].ToString();
                            ed.Status = dr["sims_board_exam_student_status"].ToString();
                            cnclAdm.ExamDet.Add(ed);
                        }
                        dr.NextResult();
                        cnclAdm.InvsDet = new List<InvsDets>();
                        while (dr.Read())
                        {
                            InvsDets id = new InvsDets();
                            id.Ammount = dr["dd_sale_value_final"].ToString();
                            id.ItemName = dr["im_desc"].ToString();
                            id.ItemQty = dr["dd_outstanding_qty"].ToString();
                            cnclAdm.InvsDet.Add(id);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, cnclAdm);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, cnclAdm);
            }
        }

        [Route("CancelAdmissionProceed")]
        public HttpResponseMessage CancelAdmissionProceed(Sims010_Edit ed)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CancelAdmissionProceed()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/CancelAdmission/", "CancelAdmissionProceed"));
            Sims010_Edit simsobj = ed;
            string cnclAdm = string.Empty;

            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[CANCEL_ADMISSION_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@ENROLL", ed.enroll),
                            new SqlParameter("@REASON", ed.reason),
                            new SqlParameter("@FEE_IGNORED", ed.fees_paid_status),
                            new SqlParameter("@IGNORED_FEE_LIST", ed.fee_category_code),
                            new SqlParameter("@cancel_date",db.DBYYYYMMDDformat(ed.cancel_date)),
                            new SqlParameter("@ACADEMIC_YEAR",ed.academic_year),
                            new SqlParameter("@CURR_CODE",ed.cur_code),
                            new SqlParameter("@USERCODE",ed.username),
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                cnclAdm = dr[0].ToString();

                            }
                        }
                        //if (dr.RecordsAffected > 0)
                        //{
                        //    if (simsobj.opr.Equals("P"))
                        //        message.strMessage = "Admission Canceled";
                        //    message.systemMessage = string.Empty;
                        //    message.messageType = MessageType.Success;
                        //}
                        return Request.CreateResponse(HttpStatusCode.OK, cnclAdm);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, cnclAdm);
                }
            }
            catch (Exception x)
            {
                /*Log.Error(x);
                if (simsobj.opr.Equals("P"))
                    message.strMessage = "Error In Cancellig the Admission";*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }
    }
}