﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Web;
using System.IO;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/TelephonicConversationDetails")]
    public class TelephonicConversationDetailsController : ApiController
    {
        [Route("CUDTelephonicConversationDetails")]
        public HttpResponseMessage CUDTelephonicConversationDetails(List<Sims664> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims664 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_communication_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_comm_message_remark", simsobj.sims_comm_message_remark),
                                new SqlParameter("@sims_comm_message_feedback", simsobj.sims_comm_message_feedback),
                                new SqlParameter("@sims_comm_user_by", simsobj.sims_comm_user_by),   
                                new SqlParameter("@sims_comm_date", db.DBYYYYMMDDformat(simsobj.sims_comm_date)),
                                 new SqlParameter("@sims_comm_user_code", simsobj.sims_comm_user_code),
                                 new SqlParameter("@sims_comm_number", simsobj.sims_comm_number),
                                new SqlParameter("@sims_follow_up_date", db.DBYYYYMMDDformat(simsobj.sims_follow_up_date)),


                                //new SqlParameter("@sims_checklist_section_status_parameter", simsobj.sims_checklist_section_status_parameter==true?"1":"0"), 
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("get_TelephonicConversationDetails")]
        public HttpResponseMessage get_TelephonicConversationDetails()
        {

            List<Sims664> lst = new List<Sims664>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_communication_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims664 simsobj = new Sims664();

                            simsobj.sims_comm_message_remark = dr["sims_comm_message_remark"].ToString();
                            simsobj.sims_comm_message_feedback = dr["sims_comm_message_feedback"].ToString();
                            simsobj.sims_comm_user_by = dr["sims_comm_user_by"].ToString();
                            simsobj.sims_comm_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_user_code = dr["sims_comm_user_code"].ToString();
                            
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("get_TelephonicConversationDetails")]
        public HttpResponseMessage get_TelephonicConversationDetails(string pros)
        {

            List<Sims664> lst = new List<Sims664>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_communication_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_comm_user_code", pros),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims664 simsobj = new Sims664();

                            simsobj.sims_comm_message_remark = dr["sims_comm_message_remark"].ToString();
                            simsobj.sims_comm_message_feedback = dr["sims_comm_message_feedback"].ToString();
                            simsobj.sims_comm_user_by = dr["sims_comm_user_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_comm_date"].ToString()))
                                simsobj.sims_comm_date1 = DateTime.Parse( db.DBYYYYMMDDformat( dr["sims_comm_date"].ToString()));

                            simsobj.sims_comm_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_user_code = dr["sims_comm_user_code"].ToString();
                            simsobj.sims_comm_number = dr["sims_comm_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["sims_follow_up_date"].ToString()))
                                simsobj.sims_follow_up_date1 = DateTime.Parse(db.DBYYYYMMDDformat( dr["sims_follow_up_date"].ToString()));
                            simsobj.sims_follow_up_date = dr["sims_follow_up_date"].ToString();

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("CUDTelephonicConversationDetailsNew")]
        public HttpResponseMessage CUDTelephonicConversationDetailsNew(List<Sims664> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims664 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_communication_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_comm_message_remark", simsobj.sims_comm_message_remark),
                                new SqlParameter("@sims_comm_message_feedback", simsobj.sims_comm_message_feedback),
                                new SqlParameter("@sims_comm_user_by", simsobj.sims_comm_user_by),   
                                new SqlParameter("@sims_comm_date", db.DBYYYYMMDDformat(simsobj.sims_comm_date)),
                                 new SqlParameter("@sims_comm_user_code", simsobj.sims_comm_user_code),
                                 new SqlParameter("@sims_comm_number", simsobj.sims_comm_number),
                                new SqlParameter("@sims_follow_up_date", db.DBYYYYMMDDformat(simsobj.sims_follow_up_date)),
                                 new SqlParameter("@willing_to_register", simsobj.willing_to_register),
                                 new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                          
                          
                                 new SqlParameter("@sims_comm_user_person_relation", simsobj.sims_seat_cofirm_tran_id),
                                 

                                //new SqlParameter("@sims_checklist_section_status_parameter", simsobj.sims_checklist_section_status_parameter==true?"1":"0"), 
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}