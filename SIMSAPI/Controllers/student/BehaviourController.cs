﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/Behaviour")]
    public class BehaviourController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Seelct
        [Route("getAllBehaviour")]
        public HttpResponseMessage getAllBehaviour()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBehaviour(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBehaviour"));

            List<Sims190> goaltarget_list = new List<Sims190>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_behaviour_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims190 simsobj = new Sims190();
                            simsobj.sims_behaviour_cur_code = dr["sims_behaviour_cur_code"].ToString();
                            simsobj.sims_behaviour_cur_level_code = dr["sims_behaviour_cur_level_code"].ToString();
                            simsobj.sims_behaviour_type = dr["sims_behaviour_type"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_cur_level_name_en = dr["sims_cur_level_name_en"].ToString();
                            simsobj.sims_behaviour_code = dr["sims_behaviour_code"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_behaviour_description = dr["sims_behaviour_description"].ToString();
                            simsobj.sims_behaviour_points = dr["sims_behaviour_points"].ToString();
                            simsobj.sims_behaviour_img = dr["sims_behaviour_img"].ToString();
                            simsobj.sims_behaviour_status = dr["sims_behaviour_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //GetLedgerNumber
        [Route("getCuriculumLevel")]
        public HttpResponseMessage getCuriculumLevel(string curCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculumLevel()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Sims190> doc_list = new List<Sims190>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_behaviour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                             new SqlParameter("@sims_cur_code", curCode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims190 simsobj = new Sims190();
                            simsobj.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                            simsobj.sims_cur_level_name_en = dr["sims_cur_level_name_en"].ToString();
                         
                            doc_list.Add(simsobj);
                            
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetLedgerNumber
        [Route("GetBehaviourType")]
        public HttpResponseMessage GetBehaviourType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetBehaviourType()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Sims190> doc_list = new List<Sims190>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_behaviour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims190 simsobj = new Sims190();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                           
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDInsertBehaviour")]
        public HttpResponseMessage CUDStudentDevice(List<Sims190> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims190 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_behaviour_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_behaviour_cur_code",simsobj.sims_behaviour_cur_code),
                                new SqlParameter("@sims_behaviour_cur_level_code", simsobj.sims_behaviour_cur_level_code),
                                new SqlParameter("@sims_behaviour_code", simsobj.sims_behaviour_code),
                                new SqlParameter("@sims_behaviour_type",simsobj.sims_behaviour_type),
                                new SqlParameter("@sims_behaviour_description", simsobj.sims_behaviour_description),
                                new SqlParameter("@sims_behaviour_points", simsobj.sims_behaviour_points),
                                new SqlParameter("@sims_behaviour_img",simsobj.sims_behaviour_img),
                                new SqlParameter("@sims_behaviour_status", simsobj.sims_behaviour_status==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



    }
}