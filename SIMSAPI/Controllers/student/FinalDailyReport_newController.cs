﻿using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/DailyReport_new")]
    public class FinalDailyReport_newController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGradesNew")]
        public HttpResponseMessage getAllGradesNew(string cur_code, string academic_year, string user)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                                new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeNew")]
        public HttpResponseMessage getSectionFromGradeNew(string cur_code, string grade_code, string academic_year, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code),
                                  new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }

        }


        [Route("getFinalDailyReport")]
        public HttpResponseMessage getFinalDailyReport(string cur_code, string academic_year, string grade_code, string section_code, string date)
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@sims_obsprog_date",db.DBYYYYMMDDformat(date)),
                                

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_student_img = dr["sims_student_img"].ToString();
                            sequence.parent_name = dr["parent_name"].ToString();

                            sequence.flag = dr["cnt"].ToString();


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        //Home room Report
        [Route("getHomeRoomReport")]
        public HttpResponseMessage getHomeRoomReport(string cur_code, string academic_year, string homerum_code, string date, string user)
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "AA"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@homerum_code", homerum_code),
                             
                             new SqlParameter("@sims_obsprog_date",db.DBYYYYMMDDformat(date)),
                              new SqlParameter ("@emp_code",user),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_student_img = dr["sims_student_img"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();

                            sequence.HomeroomCode = dr["HomeroomCode"].ToString();
                            sequence.sims_homeroom_name = dr["sims_homeroom_name"].ToString();

                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.parent_name = dr["parent_name"].ToString();

                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.flag = dr["cnt"].ToString();

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getHomeRoomBatch")]
        public HttpResponseMessage getHomeRoomBatch(string cur_code, string academic_year, string username)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AB"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@academic_year", academic_year),
                            new SqlParameter("@teacher_code", username)
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.homeroombatch = dr["HomeRoomBatch"].ToString();
                            simsobj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            //simsobj.sims_batch_code = dr["sims_batch_code"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
      
        [Route("geteditedData")]
        public HttpResponseMessage geteditedData(string cur_code, string academic_year, string enroll_number, string date)
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'Q'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@sims_enroll_number", enroll_number),
                             new SqlParameter("@sims_obsprog_date",db.DBYYYYMMDDformat(date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_no"].ToString();
                            sequence.sims_obsprog_number = dr["sims_obsprog_number"].ToString();
                            sequence.sims_obsprog_date = dr["sims_obsprog_date"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_subcode2 = dr["sims_obsprog_subcode2"].ToString();
                            sequence.sims_obsprog_subcode3 = dr["sims_obsprog_subcode3"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_remark = dr["sims_obsprog_code_tag_remark"].ToString();
                            sequence.sims_obsprog_incidence_desc = dr["sims_obsprog_incidence_desc"].ToString();
                            sequence.sims_obsprog_incidence_location = dr["sims_obsprog_incidence_location"].ToString();
                            sequence.sims_obsprog_incidence_teacher_remark = dr["sims_obsprog_incidence_teacher_remark"].ToString();
                            sequence.sims_obsprog_incidence_parent_remark = dr["sims_obsprog_incidence_parent_remark"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_teacher_remark = dr["sims_obsprog_teacher_remark"].ToString();
                            sequence.sims_obsprog_parent_remark = dr["sims_obsprog_parent_remark"].ToString();
                            sequence.sims_obsprog_communication_flag = dr["sims_obsprog_communication_flag"].ToString();
                            sequence.sims_obsprog_status = dr["sims_obsprog_status"].ToString();
                            sequence.sims_obsprog_created_date = dr["sims_obsprog_created_date"].ToString();
                            sequence.sims_obsprog__created_by = dr["sims_obsprog__created_by"].ToString();
                            sequence.sims_obsprog_path = dr["sims_obsprog_path"].ToString();


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getFeeling")]
        public HttpResponseMessage getFeeling()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.feeling_code = dr["feeling_code"].ToString();
                            sequence.feeling_desc = dr["feeling_desc"].ToString();
                            sequence.sims_obsprog_subcode2 = dr["sims_obsprog_subcode2"].ToString();
                            sequence.sims_obsprog_subcode2_description = dr["sims_obsprog_subcode2_description"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getActivity")]
        public HttpResponseMessage getActivity()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.activity_code = dr["activity_code"].ToString();
                            sequence.activity_desc = dr["activity_desc"].ToString();
                            sequence.sims_obsprog_subcode2 = dr["sims_obsprog_subcode2"].ToString();
                            sequence.sims_obsprog_subcode2_description = dr["sims_obsprog_subcode2_description"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencydry")]
        public HttpResponseMessage getFrequencydry()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.dry_frequency_code = dr["dry_frequency_code"].ToString();
                            sequence.dry_frequency_desc = dr["dry_frequency_desc"].ToString();
                            sequence.dry_sims_obsprog_code_tag_code = dr["dry_sims_obsprog_code_tag_code"].ToString();
                            sequence.nppy_sims_obsprog_subcode1 = dr["nppy_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencywet")]
        public HttpResponseMessage getFrequencywet()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "D"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.wet_frequency_code = dr["wet_frequency_code"].ToString();
                            sequence.wet_frequency_desc = dr["wet_frequency_desc"].ToString();
                            sequence.wet_sims_obsprog_code_tag_code = dr["wet_sims_obsprog_code_tag_code"].ToString();
                            sequence.nppy_sims_obsprog_subcode1 = dr["nppy_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencybm")]
        public HttpResponseMessage getFrequencybm()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "E"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.bm_frequency_code = dr["bm_frequency_code"].ToString();
                            sequence.bm_frequency_desc = dr["bm_frequency_desc"].ToString();
                            sequence.bm_sims_obsprog_code_tag_code = dr["bm_sims_obsprog_code_tag_code"].ToString();
                            sequence.nppy_sims_obsprog_subcode1 = dr["nppy_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyn1")]
        public HttpResponseMessage getFrequencyn1()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "F"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.n1_frequency_code = dr["n1_frequency_code"].ToString();
                            sequence.n1_frequency_desc = dr["n1_frequency_desc"].ToString();
                            sequence.n1_sims_obsprog_code_tag_code = dr["n1_sims_obsprog_code_tag_code"].ToString();
                            sequence.toi_sims_obsprog_subcode1 = dr["toi_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyn2")]
        public HttpResponseMessage getFrequencyn2()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.n2_frequency_code = dr["n2_frequency_code"].ToString();
                            sequence.n2_frequency_desc = dr["n2_frequency_desc"].ToString();
                            sequence.n2_sims_obsprog_code_tag_code = dr["n2_sims_obsprog_code_tag_code"].ToString();
                            sequence.toi_sims_obsprog_subcode1 = dr["toi_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyask")]
        public HttpResponseMessage getFrequencyask()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "H"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.ask_frequency_code = dr["ask_frequency_code"].ToString();
                            sequence.ask_frequency_desc = dr["ask_frequency_desc"].ToString();
                            sequence.ask_sims_obsprog_code_tag_code = dr["ask_sims_obsprog_code_tag_code"].ToString();
                            sequence.toi_sims_obsprog_subcode1 = dr["toi_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencymilk")]
        public HttpResponseMessage getFrequencymilk()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "J"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.milk_frequency_code = dr["milk_frequency_code"].ToString();
                            sequence.milk_frequency_desc = dr["milk_frequency_desc"].ToString();
                            sequence.milk_sims_obsprog_code_tag_code = dr["milk_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencywat")]
        public HttpResponseMessage getFrequencywat()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "K"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.wat_frequency_code = dr["wat_frequency_code"].ToString();
                            sequence.wat_frequency_desc = dr["wat_frequency_desc"].ToString();
                            sequence.wat_sims_obsprog_code_tag_code = dr["wat_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyjui")]
        public HttpResponseMessage getFrequencyjui()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "L"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.jui_frequency_code = dr["jui_frequency_code"].ToString();
                            sequence.jui_frequency_desc = dr["jui_frequency_desc"].ToString();
                            sequence.jui_sims_obsprog_code_tag_code = dr["jui_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_quantity_code_description = dr["sims_obsprog_code_tag_parameter_frequency_quantity_code_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencymor")]
        public HttpResponseMessage getFrequencymor()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "M"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.mor_frequency_code = dr["mor_frequency_code"].ToString();
                            sequence.mor_frequency_desc = dr["mor_frequency_desc"].ToString();
                            sequence.mor_sims_obsprog_code_tag_code = dr["mor_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencylunch")]
        public HttpResponseMessage getFrequencylunch()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "O"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.lunch_frequency_code = dr["lunch_frequency_code"].ToString();
                            sequence.lunch_frequency_desc = dr["lunch_frequency_desc"].ToString();
                            sequence.lunch_sims_obsprog_code_tag_code = dr["lunch_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyaft")]
        public HttpResponseMessage getFrequencyaft()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "P"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.aft_frequency_code = dr["aft_frequency_code"].ToString();
                            sequence.aft_frequency_desc = dr["aft_frequency_desc"].ToString();
                            sequence.aft_sims_obsprog_code_tag_code = dr["aft_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtymilk")]
        public HttpResponseMessage getqtymilk()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "M"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.milk_frequency_qty_code = dr["milk_frequency_qty_code"].ToString();
                            sequence.milk_frequency_qty_desc = dr["milk_frequency_qty_desc"].ToString();
                            sequence.milk_sims_obsprog_code_tag_code = dr["milk_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtywat")]
        public HttpResponseMessage getqtywat()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "O"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.wat_frequency_qty_code = dr["wat_frequency_qty_code"].ToString();
                            sequence.wat_frequency_qty_desc = dr["wat_frequency_qty_desc"].ToString();
                            sequence.wat_sims_obsprog_code_tag_code = dr["wat_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtyjui")]
        public HttpResponseMessage getqtyjui()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "P"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.jui_frequency_qty_code = dr["jui_frequency_qty_code"].ToString();
                            sequence.jui_frequency_qty_desc = dr["jui_frequency_qty_desc"].ToString();
                            sequence.jui_sims_obsprog_code_tag_code = dr["jui_sims_obsprog_code_tag_code"].ToString();
                            sequence.flu_sims_obsprog_subcode1 = dr["flu_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtymor")]
        public HttpResponseMessage getqtymor()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "T"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.mor_frequency_code = dr["mor_frequency_code"].ToString();
                            sequence.mor_frequency_desc = dr["mor_frequency_desc"].ToString();
                            sequence.mor_sims_obsprog_code_tag_code = dr["mor_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtylunch")]
        public HttpResponseMessage getqtylunch()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "V"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.lunch_frequency_code = dr["lunch_frequency_code"].ToString();
                            sequence.lunch_frequency_desc = dr["lunch_frequency_desc"].ToString();
                            sequence.lunch_sims_obsprog_code_tag_code = dr["lunch_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getqtyaft")]
        public HttpResponseMessage getqtyaft()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Y"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.aft_frequency_code = dr["aft_frequency_code"].ToString();
                            sequence.aft_frequency_desc = dr["aft_frequency_desc"].ToString();
                            sequence.aft_sims_obsprog_code_tag_code = dr["aft_sims_obsprog_code_tag_code"].ToString();
                            sequence.foo_sims_obsprog_subcode1 = dr["foo_sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getFrequencyclothing")]
        public HttpResponseMessage getFrequencyclothing()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "1"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencydiaper")]
        public HttpResponseMessage getFrequencydiaper()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "2"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyformula")]
        public HttpResponseMessage getFrequencyformula()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "3"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencywetwipes")]
        public HttpResponseMessage getFrequencywetwipes()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "4"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequencyothers")]
        public HttpResponseMessage getFrequencyothers()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "5"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_code_tag_code = dr["sims_obsprog_code_tag_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_code = dr["sims_obsprog_code_tag_parameter_frequency_code"].ToString();
                            sequence.sims_obsprog_code_tag_parameter_frequency_description = dr["sims_obsprog_code_tag_parameter_frequency_description"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getsleeptime")]
        public HttpResponseMessage getsleeptime()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "6"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_subcode2 = dr["sims_obsprog_subcode2"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getwaketime")]
        public HttpResponseMessage getwaketime()
        {
            List<Fdrn01> lstCuriculum = new List<Fdrn01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc_new]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "7"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdrn01 sequence = new Fdrn01();
                            sequence.sims_obsprog_code = dr["sims_obsprog_code"].ToString();
                            sequence.sims_obsprog_subcode1 = dr["sims_obsprog_subcode1"].ToString();
                            sequence.sims_obsprog_subcode2 = dr["sims_obsprog_subcode2"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("CUDDetails")]
        public HttpResponseMessage CUDDetails(List<Fdrn01> data)
        {
            bool insert = false;

            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fdrn01 simsobj in data)
                    {

                        if (!string.IsNullOrEmpty(simsobj.sims_obsprog_code))
                        {

                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_final_daily_report_proc_new]",

                        new List<SqlParameter>()
                     {

                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@cur_code",simsobj.sims_cur_code),
                                 new SqlParameter("@academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                 new SqlParameter("@sims_obsprog_date", db.DBYYYYMMDDformat(simsobj.mom_start_date)),
                                 new SqlParameter("@sims_obsprog_code",simsobj.sims_obsprog_code),
                                 new SqlParameter("@sims_obsprog_subcode1",simsobj.sims_obsprog_subcode1),
                                 new SqlParameter("@sims_obsprog_subcode2",simsobj.sims_obsprog_subcode2),
                                 new SqlParameter("@sims_obsprog_subcode3",simsobj.sims_obsprog_subcode3),
                                 new SqlParameter("@sims_obsprog_code_tag_code",simsobj.sims_obsprog_code_tag_code),
                                 new SqlParameter("@sims_obsprog_code_tag_parameter_frequency_code",simsobj.sims_obsprog_code_tag_parameter_frequency_code),
                                 new SqlParameter("@sims_obsprog_code_tag_parameter_frequency_quantity_code",simsobj.sims_obsprog_code_tag_parameter_frequency_quantity_code),
                                 new SqlParameter("@sims_obsprog_code_tag_remark",simsobj.sims_obsprog_code_tag_remark),
                                 new SqlParameter("@sims_obsprog_teacher_remark",simsobj.sims_student_teacher_comment),
                                 new SqlParameter("@sims_obsprog_parent_remark", simsobj.sims_student_parent_comment),
                                 new SqlParameter("@sims_obsprog_created_by", simsobj.sims_student_created_by),
                                 new SqlParameter("@sims_obsprog_incidence_desc",simsobj.sims_obsprog_incidence_desc),
                                 new SqlParameter("@sims_obsprog_incidence_location",simsobj.sims_obsprog_incidence_location),
                                 new SqlParameter("@sims_obsprog_incidence_teacher_remark",simsobj.sims_obsprog_incidence_teacher_remark),
                                 new SqlParameter("@sims_obsprog_incidence_parent_remark",simsobj.sims_obsprog_incidence_parent_remark),


                     });
                            if (ins > 0)
                            {
                                insert = true;

                            }
                            else
                            {
                                insert = false;

                            }
                        }

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDDetails1")]
        public HttpResponseMessage CUDDetails1(List<Fdrn01> data)
        {
            bool insert = false;

            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fdrn01 simsobj1 in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_final_daily_report_proc_new]",

                    new List<SqlParameter>()
                 {

                                 new SqlParameter("@opr", simsobj1.opr1),
                                 new SqlParameter("@cur_code",simsobj1.sims_cur_code1),
                                 new SqlParameter("@academic_year", simsobj1.sims_academic_year1),
                                 new SqlParameter("@sims_enroll_number", simsobj1.sims_enroll_number1),
                                 new SqlParameter("@sims_obsprog_date", db.DBYYYYMMDDformat(simsobj1.mom_start_date1)),
                                 new SqlParameter("@sims_obsprog_created_by", simsobj1.sims_student_created_by1),
                                 new SqlParameter("@sims_obsprog_incidence_desc",simsobj1.sims_obsprog_incidence_desc),
                                 new SqlParameter("@sims_obsprog_incidence_location",simsobj1.sims_obsprog_incidence_location),
                                 new SqlParameter("@sims_obsprog_incidence_teacher_remark",simsobj1.sims_obsprog_incidence_teacher_remark),
                                 new SqlParameter("@sims_obsprog_incidence_parent_remark",simsobj1.sims_obsprog_incidence_parent_remark),


                 });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }


                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("recordsd")]
        public HttpResponseMessage recordsd(string cur_code, string academic_year, string sims_enroll_number, string sims_obsprog_date)
        {
            bool insert = false;

            try
            {
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_final_daily_report_proc_new]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","10"),
                        new SqlParameter("@cur_code", cur_code),
                        new SqlParameter("@academic_year", academic_year),
                        new SqlParameter("@sims_enroll_number", sims_enroll_number),
                        new SqlParameter("@sims_obsprog_date", db.DBYYYYMMDDformat(sims_obsprog_date)),

                    });
                    if (ins > 0)
                    {
                        insert = true;

                    }
                    else
                    {
                        insert = false;

                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}