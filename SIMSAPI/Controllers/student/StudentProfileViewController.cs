﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.Common;

namespace SIMSAPI.Controllers.student
{
     [RoutePrefix("api/StudetProfile")]
    public class StudentProfileViewController: ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("Get_StudentGradeSection")]
         public HttpResponseMessage Get_StudentGradeSection(string enroll_no)
         {
          

             List<Sims042> goaltarget_list = new List<Sims042>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                      
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "A"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims042 simsobj = new Sims042();
                             simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                             try
                             {
                                 simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                             }
                             catch (Exception ex) { }

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_SimsActiveForm")]
         public HttpResponseMessage Get_SimsActiveForm()
         {
           

             List<simsClass> goaltarget_list = new List<simsClass>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                      
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "B"),
                          
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             simsClass simsobj = new simsClass();
                             simsobj.sims_active_form = dr["sims_appl_form_field_value1"].ToString();
                             simsobj.sims_active_form_status = dr["sims_appl_parameter"].ToString();                            
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentDetails")]
         public HttpResponseMessage Get_StudentDetails(string student_enroll, string parent_no)
         {
            

             List<Sims042> goaltarget_list = new List<Sims042>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                        
                        new List<SqlParameter>()
                         {
                          
                             new SqlParameter("@opr", 'S'),
                             new SqlParameter("@sims_student_enroll_number", student_enroll),
                             new SqlParameter("@parent_id", parent_no),
                             new SqlParameter("@sims_student_img", ""),
              
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims042 simsobj = new Sims042();
                                    simsobj.student_full_name = dr["Name in English"].ToString();
                                    simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                                    simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                                    simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                                    simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                                    simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                                    simsobj.student_full_name_ot = dr["Name in Arabic"].ToString();
                                    simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                                    simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                                    simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                                    simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                                    simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                    simsobj.sims_student_religion_code = dr["sims_student_religion_code"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_dob"].ToString()))
                                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                                    else
                                        simsobj.sims_student_dob = "-";
                                    simsobj.sims_student_birth_country_code = dr["sims_student_birth_country_code"].ToString();
                                    simsobj.sims_student_nationality_code = dr["sims_student_nationality_code"].ToString();
                                    //simsobj.sims_student_ethnicity_code = dr["sims_student_ethnicity_code"].ToString();
                                    simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_visa_issue_date"].ToString()))
                                        simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_visa_expiry_date"].ToString()))
                                        simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                                    simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                                    simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                                    simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                                    simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_national_id_issue_date"].ToString()))
                                        simsobj.sims_student_national_id_issue_date =dr["sims_student_national_id_issue_date"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_national_id_expiry_date"].ToString()))
                                        simsobj.sims_student_national_id_expiry_date =  dr["sims_student_national_id_expiry_date"].ToString();
                                    simsobj.sims_student_image = dr["sims_student_img"].ToString();
                                    simsobj.sims_student_main_language = dr["sims_student_main_language_code"].ToString();
                                    simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                                    simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                                    simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                                    if (dr["sims_student_transport_status"].ToString().Equals("A"))
                                        simsobj.sims_student_transport_status = true;
                                    else
                                        simsobj.sims_student_transport_status = false;

                                    simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                                    simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                                    simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                                    simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_date"].ToString()))
                                        simsobj.sims_student_date =dr["sims_student_date"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_commence_date"].ToString()))
                                        simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                                    simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                                    simsobj.sims_student_current_school_code = dr["sims_student_current_school_code"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_last_login"].ToString()))
                                        simsobj.sims_student_last_login = dr["sims_student_last_login"].ToString();
                                    simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                                    simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                                    simsobj.sims_student_house = dr["sims_student_house"].ToString();
                                    simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                                    simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_ea_registration_date"].ToString()))
                                        simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                                    simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                                    simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                                    simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                                    if (!string.IsNullOrEmpty(dr["sims_student_passport_issue_date"].ToString()))
                                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());
                                    if (!string.IsNullOrEmpty(dr["sims_student_passport_expiry_date"].ToString()))
                                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                                    simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                                    simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                                    //simsobj.student_honour_roll = dr["sims_student_honour_roll"].ToString();
                                    simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                    simsobj.sims_parent_father_city = dr["sims_parent_father_city"].ToString();
                                    simsobj.sims_parent_father_state = dr["sims_parent_father_state"].ToString();
                                    simsobj.sims_parent_country_code = dr["sims_parent_country_code"].ToString();
                                    simsobj.sims_parent_father_po_box = dr["sims_parent_father_po_box"].ToString();
                                    simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                                    simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                    simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                    simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                                    simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                                    try
                                    {
                                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                                    }
                                    catch (Exception ex) { }

                                     goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_SiblingDetails")]
         public HttpResponseMessage Get_SiblingDetails(string enroll_no)
         {


             List<Sims042> goaltarget_list = new List<Sims042>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "C"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims042 simsobj = new Sims042();
                             simsobj.sims_sibling_enroll_number = dr["sims_student_enroll_number"].ToString();
                             simsobj.sims_sibling_name = dr["StudentName"].ToString();
                             simsobj.sims_sibling_student_nickname = dr["sims_student_nickname"].ToString();
                             simsobj.sims_sibling_academic_year = dr["sims_academic_year"].ToString();
                             simsobj.sims_sibling_grade_name_en = dr["sims_grade_name_en"].ToString();
                             simsobj.sims_sibling_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_sibling_section_name_en = dr["sims_section_name_en"].ToString();
                             simsobj.sims_sibling_section_code = dr["sims_section_code"].ToString();
                             simsobj.sims_sibling_student_img_path =  dr["sims_student_img"].ToString();//string.Format("{0}/Images/StudentImage/{1}", commonStaticClass.imgpath,
                             simsobj.sims_parent_number = dr["sims_sibling_parent_number"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_TransportRouteStudent")]
         public HttpResponseMessage Get_TransportRouteStudent(string enroll_no)
         {


             List<PP011> goaltarget_list = new List<PP011>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "D"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             PP011 simsobj = new PP011();
                             simsobj.sims_transport_enroll_number = dr["sims_transport_enroll_number"].ToString();
                             simsobj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                             simsobj.sims_transport_route_code_name = dr["sims_transport_route_name"].ToString();
                             simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                             simsobj.sims_transport_effective_from = DateTime.Parse(dr["sims_transport_effective_from"].ToString()).ToShortDateString();
                             simsobj.sims_transport_effective_upto = DateTime.Parse(dr["sims_transport_effective_upto"].ToString()).ToShortDateString();
                             simsobj.sims_transport_pickup_stop_name = dr["sims_transport_pickup_stop_code"].ToString();
                             simsobj.sims_transport_drop_stop_name = dr["sims_transport_drop_stop_code"].ToString();
                             simsobj.sims_student_img = dr["sims_student_img"].ToString();
                             simsobj.sims_driver_name = dr["sims_driver_name"].ToString();
                             simsobj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentAttendancechartview")]
         public HttpResponseMessage Get_StudentAttendancechartview(string enroll_no)
         {


             List<Uccw257> goaltarget_list = new List<Uccw257>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "V"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Uccw257 simsobj = new Uccw257();

                             simsobj.sims_month_no = dr["att_month"].ToString();
                             simsobj.sims_gb_name = dr["month_name"].ToString();
                             simsobj.sims_attendance_cd_p = Int32.Parse(dr["Present_count"].ToString());
                             simsobj.sims_attendance_cd_a = Int32.Parse(dr["Absent_count"].ToString());
                             simsobj.sims_attendance_cd_ae = Int32.Parse(dr["AbsentExcused_count"].ToString());
                             simsobj.sims_attendance_cd_t = Int32.Parse(dr["Tardy_count"].ToString());
                             simsobj.sims_attendance_cd_te = Int32.Parse(dr["TardyExcused_count"].ToString());
                             simsobj.sims_attendance_cd_um = Int32.Parse(dr["UnMarked_count"].ToString());

                             simsobj.sims_attendance_cd_w = Int32.Parse(dr["Weekend"].ToString());
                             simsobj.sims_attendance_cd_h = Int32.Parse(dr["Holidays"].ToString());


                             simsobj.sims_attendance_totaldays = Int32.Parse(dr["total_days"].ToString());

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentAttendancedayview")]
         public HttpResponseMessage Get_StudentAttendancedayview(string enroll_no, string status, string month)
         {


             List<Uccw257> goaltarget_list = new List<Uccw257>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "O"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           new SqlParameter("@sims_day_status",status),
                           new SqlParameter("@sims_day_month",month),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Uccw257 simsobj = new Uccw257();
                             simsobj.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentResult")]
         public HttpResponseMessage Get_StudentResult(string enroll_no, string grade_cd, string section_cd)
         {


             List<Uccw257> goaltarget_list = new List<Uccw257>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "E"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                            new SqlParameter("@sims_grade_code",grade_cd),
                             new SqlParameter("@sims_section_code",section_cd),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                            
                             Uccw257 simsobj = new Uccw257();
                             simsobj.sims_gb_name = dr["sims_gb_name"].ToString();
                             simsobj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                             simsobj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                             simsobj.sims_gb_cat_assign_max_score_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                             simsobj.final_mark = dr["final_mark"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentMedicalDetails")]
         public HttpResponseMessage Get_StudentMedicalDetails(string enroll_no)
         {


             List<PP009> goaltarget_list = new List<PP009>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                        
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "F"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {

                             PP009 simsobj = new PP009();
                             simsobj.health_card_number = dr["sims_health_card_number"].ToString();
                             simsobj.health_card_issue_date = dr["sims_health_card_issue_date"].ToString();
                               simsobj.health_card_expiry_date = dr["sims_health_card_expiry_date"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_health_card_issue_date"].ToString()))
                                 //simsobj.health_card_issue_date = DateTime.Parse(dr["sims_health_card_issue_date"].ToString()).ToShortDateString();
                             //if (!string.IsNullOrEmpty(dr["sims_health_card_expiry_date"].ToString()))
                             //    simsobj.health_card_expiry_date = DateTime.Parse(dr["sims_health_card_expiry_date"].ToString()).ToShortDateString();
                             simsobj.health_card_issuing_authority = dr["sims_health_card_issuing_authority"].ToString();
                             simsobj.student_blood_group_code = dr["sims_blood_group_code"].ToString();
                             simsobj.student_health_restriction_status = dr["sims_health_restriction_desc"].ToString();
                             simsobj.student_disability_status = dr["sims_disability_desc"].ToString();
                             simsobj.student_medication_status = dr["sims_medication_desc"].ToString();
                             simsobj.health_other_status = dr["sims_health_other_desc"].ToString();
                             simsobj.health_hearing_status = dr["sims_health_hearing_desc"].ToString();
                             simsobj.health_vision_status = dr["sims_health_vision_desc"].ToString();
                             simsobj.regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                             simsobj.regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                             simsobj.regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                             simsobj.regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                             simsobj.student_height = dr["sims_height"].ToString();
                             simsobj.student_wieght = dr["sims_wieght"].ToString();
                             simsobj.student_teeth = dr["sims_teeth"].ToString();
                             simsobj.sims_student_image = dr["sims_student_img"].ToString();
                             simsobj.enable = dr["enable"].ToString().Equals("1");
                             simsobj.visible = dr["visible"].ToString().Equals("1");
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_StudentMedicalVisit")]
         public HttpResponseMessage Get_StudentMedicalVisit(string enroll_no)
         {


             List<PP009> goaltarget_list = new List<PP009>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "G"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             PP009 simsobj = new PP009();
                             simsobj.medical_visit_date = dr["sims_medical_visit_date"].ToString();
                             simsobj.medical_attended_by = dr["sims_attended_by"].ToString();
                             simsobj.medical_complaint_desc = dr["sims_complaint_desc"].ToString();
                             simsobj.medical_attendant_observation = dr["sims_attendant_observation"].ToString();
                             simsobj.medical_followup_comment = dr["sims_followup_comment"].ToString();
                             simsobj.medical_health_education = dr["sims_health_education"].ToString();

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_LibraryTransaction")]
         public HttpResponseMessage Get_LibraryTransaction(string enroll_no)
         {


             List<sims136Detail> goaltarget_list = new List<sims136Detail>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                      
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "H"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             sims136Detail simsobj = new sims136Detail();
                            
                             simsobj.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                             simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                             simsobj.sims_library_transaction_type = dr["sims_library_transaction_type_name"].ToString();
                             simsobj.sims_library_transaction_remarks = dr["sims_library_transaction_remarks"].ToString();

                            try
                            {
                                simsobj.sims_library_item_issue_date = dr["sims_library_item_issue_date"].ToString();
                                simsobj.sims_library_item_expected_return_date = dr["sims_library_item_expected_return_date"].ToString();
                                simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                                simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            }
                            catch (Exception x)
                            {

                                 
                            }
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_TimetableFileName")]
         public HttpResponseMessage Get_TimetableFileName(string grade_cd, string section_cd)
         {


             string name = string.Empty;
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                       
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "I"),
                             new SqlParameter("@sims_grade_code",grade_cd),
                           new SqlParameter("@sims_section_code",section_cd),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             name = dr["sims_timetable_filename"].ToString();
                            

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, name);

             }
             return Request.CreateResponse(HttpStatusCode.OK, name);
         }

         [Route("Get_FeeDetails")]
         public HttpResponseMessage Get_FeeDetails(string enroll_no)
         {

             List<PP008> goaltarget_list = new List<PP008>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",
                         
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "J"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             PP008 simsobj = new PP008();
                             simsobj.sims_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                             simsobj.sims_con_fee = Convert.ToDouble(dr["conc_Fee"].ToString());
                             simsobj.sims_con_fee_paid = Convert.ToDouble(dr["conc_Fee_Paid"].ToString());
                             simsobj.sims_term_desc = dr["sims_fee_term_desc_en"].ToString();
                             simsobj.sims_expected_fee = Convert.ToDouble(dr["expected_Fee"].ToString());
                             simsobj.sims_paid_fee = Convert.ToDouble(dr["paid_Fee"].ToString());
                             simsobj.sims_remaining_fee = Convert.ToDouble(dr["remaining_Fee"].ToString());

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_FeeDetails")]
         public HttpResponseMessage Get_FeeDetails(string enroll_no,string academic)
         {

             List<PP008> goaltarget_list = new List<PP008>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "J"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           new SqlParameter("@sims_section_code",academic),

                           
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             PP008 simsobj = new PP008();
                             simsobj.sims_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                             simsobj.sims_con_fee = Convert.ToDouble(dr["conc_Fee"].ToString());
                             simsobj.sims_con_fee_paid = Convert.ToDouble(dr["conc_Fee_Paid"].ToString());
                             simsobj.sims_term_desc = dr["sims_fee_term_desc_en"].ToString();
                             simsobj.sims_expected_fee = Convert.ToDouble(dr["expected_Fee"].ToString());
                             simsobj.sims_paid_fee = Convert.ToDouble(dr["paid_Fee"].ToString());
                             simsobj.sims_remaining_fee = Convert.ToDouble(dr["remaining_Fee"].ToString());

                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_SimsIncidence")]
         public HttpResponseMessage Get_SimsIncidence(string enroll_no)
         {


             List<Sims149> goaltarget_list = new List<Sims149>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "K"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims149 Simsobj = new Sims149();

                             Simsobj.sims_incidence_number = dr["sims_incidence_number"].ToString();
                             Simsobj.sims_incidence_enroll_number = dr["sims_incidence_enroll_number"].ToString();
                             Simsobj.sims_incidence_date = DateTime.Parse(dr["sims_incidence_date"].ToString()).ToShortDateString();
                             Simsobj.sims_incidence_building_code = dr["Building_Description"].ToString();
                             Simsobj.sims_incidence_location_code = dr["Location_Description"].ToString();
                             Simsobj.sims_incidence_action_code = dr["Action_Description"].ToString();
                             Simsobj.sims_incidence_action_type = dr["Action_Type"].ToString();
                             Simsobj.sims_incidence_action_desc = dr["sims_incidence_action_desc"].ToString();
                             Simsobj.sims_incidence_action_point = decimal.Parse(dr["sims_incidence_action_point"].ToString());
                             Simsobj.sims_incidence_warning_code = dr["Warning_Description"].ToString();
                             Simsobj.sims_incidence_consequence_code = dr["Consequence_Description"].ToString();
                             Simsobj.sims_incidence_consequence_desc = dr["sims_incidence_consequence_desc"].ToString();
                             Simsobj.sims_incidence_consequence_type = dr["Consequence_Type"].ToString();
                             Simsobj.sims_incidence_consequence_point = dr["sims_incidence_consequence_point"].ToString();
                             Simsobj.sims_incidence_consequence_serve_start_date = DateTime.Parse(dr["sims_incidence_consequence_serve_start_date"].ToString()).ToShortDateString();

                             if (dr["sims_incidence_consequence_serve_close_flag"].ToString().Equals("Y"))
                             {
                                 Simsobj.sims_incidence_consequence_serve_close_flag = true;
                             }
                             else
                             {
                                 Simsobj.sims_incidence_consequence_serve_close_flag = false;
                             }

                             Simsobj.sims_incidence_user_code = dr["sims_incidence_user_code"].ToString();
                             Simsobj.sims_incidence_user_code_updated = dr["sims_incidence_user_code_updated"].ToString();

                             if (dr["sims_incidence_status"].ToString().Equals("A"))
                             {
                                 Simsobj.sims_incidence_status = true;
                             }
                             else
                             {
                                 Simsobj.sims_incidence_status = false;
                             }
                             goaltarget_list.Add(Simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("Get_SimsDetention")]
         public HttpResponseMessage Get_SimsDetention(string enroll_no)
         {


             List<Sims149> goaltarget_list = new List<Sims149>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "W"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                           
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims149 Simsobj = new Sims149();

                             Simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                             Simsobj.sims_detention_description = dr["sims_detention_description"].ToString();
                             Simsobj.sims_detention_point = dr["sims_detention_point"].ToString();
                             Simsobj.sims_detention_transaction_created_date = dr["sims_detention_transaction_created_date"].ToString();

                             goaltarget_list.Add(Simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getParentDetails")]
         public HttpResponseMessage getParentDetails(string enroll_no)
         {

             string parent_number = "";

             List<admissionClass> parent_details = new List<admissionClass>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_sibling_proc", new List<SqlParameter>() { 
                  new SqlParameter("@opr", "A"),
                  new SqlParameter("@sims_sibling_student_enroll_number", enroll_no),
                    });
                     if (dr1.HasRows)
                     {

                         while (dr1.Read())
                         {
                             parent_number = dr1["sims_sibling_parent_number"].ToString();

                         }
                     }
                     dr1.Dispose();

                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_proc", new List<SqlParameter>() { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_parent_number", parent_number),
                new SqlParameter("@sims_parent_father_img", ""),
                new SqlParameter("@sims_parent_mother_img", ""),
                new SqlParameter("@sims_parent_guardian_img", ""),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1)

                    });
                     while (dr.Read())
                     {
                         admissionClass simsobj = new admissionClass();
                         simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                         simsobj.sims_admission_login_name = dr["sims_parent_login_code"].ToString();
                         simsobj.sims_admission_father_salutation_code = dr["sims_parent_father_salutation_code"].ToString();
                         simsobj.sims_admission_father_first_name = dr["sims_parent_father_first_name"].ToString();
                         simsobj.sims_admission_father_middle_name = dr["sims_parent_father_middle_name"].ToString();
                         simsobj.sims_admission_father_last_name = dr["sims_parent_father_last_name"].ToString();
                         simsobj.sims_admission_family_name_en = dr["sims_parent_father_family_name"].ToString();
                         simsobj.sims_admission_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                         simsobj.sims_admission_father_nationality1_code = dr["sims_parent_father_nationality1_code"].ToString();
                         simsobj.sims_admission_father_nationality2_code = dr["sims_parent_father_nationality2_code"].ToString();
                         simsobj.sims_admission_father_appartment_number = dr["sims_parent_father_appartment_number"].ToString();
                         simsobj.sims_admission_father_building_number = dr["sims_parent_father_building_number"].ToString();
                         simsobj.sims_admission_father_street_number = dr["sims_parent_father_street_number"].ToString();
                         simsobj.sims_admission_father_area_number = dr["sims_parent_father_area_number"].ToString();
                         simsobj.sims_admission_father_city = dr["sims_parent_father_city"].ToString();
                         simsobj.sims_admission_father_state = dr["sims_parent_father_state"].ToString();
                         simsobj.sims_admission_father_country_code = dr["sims_parent_father_country_code"].ToString();
                         simsobj.sims_admission_father_po_box = dr["sims_parent_father_po_box"].ToString();
                         simsobj.sims_admission_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                         simsobj.sims_admission_father_phone = dr["sims_parent_father_phone"].ToString();
                         simsobj.sims_admission_father_mobile = dr["sims_parent_father_mobile"].ToString();
                         simsobj.sims_admission_father_fax = dr["sims_parent_father_fax"].ToString();
                         simsobj.sims_admission_father_email = dr["sims_parent_father_email"].ToString();
                         simsobj.sims_admission_father_occupation = dr["sims_parent_father_occupation"].ToString();
                         simsobj.sims_parent_father_occupation_local_language = dr["sims_parent_father_occupation_local_language"].ToString();
                         simsobj.sims_parent_father_occupation_location_local_language = dr["sims_parent_father_occupation_location_local_language"].ToString();
                         simsobj.sims_admission_father_company = dr["sims_parent_father_company"].ToString();
                         simsobj.sims_admission_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                         simsobj.sims_admisison_father_image = dr["sims_parent_father_img"].ToString();
                         //mother
                         simsobj.sims_admission_mother_salutation_code = dr["sims_parent_mother_salutation_code"].ToString();
                         simsobj.sims_admission_mother_first_name = dr["sims_parent_mother_first_name"].ToString();
                         simsobj.sims_admission_mother_middle_name = dr["sims_parent_mother_middle_name"].ToString();
                         simsobj.sims_admission_mother_last_name = dr["sims_parent_mother_last_name"].ToString();
                         simsobj.sims_admission_mother_family_name_en = dr["sims_parent_mother_family_name"].ToString();
                         simsobj.sims_admission_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                         simsobj.sims_admission_mother_nationality1_code = dr["sims_parent_mother_nationality1_code"].ToString();
                         simsobj.sims_admission_mother_nationality2_code = dr["sims_parent_mother_nationality2_code"].ToString();
                         simsobj.sims_admission_mother_appartment_number = dr["sims_parent_mother_appartment_number"].ToString();
                         simsobj.sims_admission_mother_building_number = dr["sims_parent_mother_building_number"].ToString();
                         simsobj.sims_admission_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                         simsobj.sims_admission_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                         simsobj.sims_admission_mother_city = dr["sims_parent_mother_city"].ToString();
                         simsobj.sims_admission_mother_state = dr["sims_parent_mother_state"].ToString();
                         simsobj.sims_admission_mother_country_code = dr["sims_parent_mother_country_code"].ToString();
                         simsobj.sims_admission_mother_po_box = dr["sims_parent_mother_po_box"].ToString();
                         simsobj.sims_admission_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                         simsobj.sims_admission_mother_phone = dr["sims_parent_mother_phone"].ToString();
                         simsobj.sims_admission_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                         simsobj.sims_admission_mother_fax = dr["sims_parent_mother_fax"].ToString();
                         simsobj.sims_admission_mother_email = dr["sims_parent_mother_email"].ToString();
                         simsobj.sims_admission_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                         simsobj.sims_parent_mother_occupation_local_language = dr["sims_parent_mother_occupation_local_language"].ToString();
                         simsobj.sims_admission_mother_company = dr["sims_parent_mother_company"].ToString();
                         simsobj.sims_admission_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                         simsobj.sims_admisison_mother_image = dr["sims_parent_mother_img"].ToString();
                         //guardian
                         simsobj.sims_admission_guardian_salutation_code = dr["sims_parent_guardian_salutation_code"].ToString();
                         simsobj.sims_admission_guardian_first_name = dr["sims_parent_guardian_first_name"].ToString();
                         simsobj.sims_admission_guardian_middle_name = dr["sims_parent_guardian_middle_name"].ToString();
                         simsobj.sims_admission_guardian_last_name = dr["sims_parent_guardian_last_name"].ToString();
                         simsobj.sims_admission_guardian_family_name_en = dr["sims_parent_guardian_family_name"].ToString();
                         simsobj.sims_admission_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                         simsobj.sims_admission_guardian_nationality1_code = dr["sims_parent_guardian_nationality1_code"].ToString();
                         simsobj.sims_admission_guardian_nationality2_code = dr["sims_parent_guardian_nationality2_code"].ToString();
                         simsobj.sims_admission_guardian_appartment_number = dr["sims_parent_guardian_appartment_number"].ToString();
                         simsobj.sims_admission_guardian_building_number = dr["sims_parent_guardian_building_number"].ToString();
                         simsobj.sims_admission_guardian_street_number = dr["sims_parent_guardian_street_number"].ToString();
                         simsobj.sims_admission_guardian_area_number = dr["sims_parent_guardian_area_number"].ToString();
                         simsobj.sims_admission_guardian_city = dr["sims_parent_guardian_city"].ToString();
                         simsobj.sims_admission_guardian_state = dr["sims_parent_guardian_state"].ToString();
                         simsobj.sims_admission_guardian_country_code = dr["sims_parent_guardian_country_code"].ToString();
                         simsobj.sims_admission_guardian_po_box = dr["sims_parent_guardian_po_box"].ToString();
                         simsobj.sims_admission_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                         simsobj.sims_admission_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                         simsobj.sims_admission_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                         simsobj.sims_admission_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                         simsobj.sims_admission_guardian_email = dr["sims_parent_guardian_email"].ToString();
                         simsobj.sims_admission_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                         simsobj.sims_parent_guardian_occupation_local_language = dr["sims_parent_guardian_occupation_local_language"].ToString();
                         simsobj.sims_parent_guardian_occupation_location_local_language = dr["sims_parent_guardian_occupation_location_local_language"].ToString();
                         simsobj.sims_admission_guardian_company = dr["sims_parent_guardian_company"].ToString();
                         simsobj.sims_parent_guardian_relationship_code = dr["sims_parent_guardian_relationship_code"].ToString();
                         simsobj.sims_admission_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                         simsobj.sims_admisison_guardian_image = dr["sims_parent_guardian_img"].ToString();
                         simsobj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].Equals("1") ? true : false;
                         simsobj.sims_parent_is_employement_comp_code = dr["sims_parent_is_employement_comp_code"].ToString();
                         simsobj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                         simsobj.sims_parent_father_passport_issue_date = dr["sims_parent_father_passport_issue_date"].ToString();
                         simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();
                         simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                         simsobj.sims_parent_father_national_id_issue_date = dr["sims_parent_father_national_id_issue_date"].ToString();
                         simsobj.sims_parent_father_national_id_expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                         simsobj.sims_parent_mother_passport_issue_date = dr["sims_parent_mother_passport_issue_date"].ToString();
                         simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                         simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                         simsobj.sims_parent_mother_national_id_issue_date = dr["sims_parent_mother_national_id_issue_date"].ToString();
                         simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();
                         simsobj.sims_parent_guardian_passport_issue_date = dr["sims_parent_guardian_passport_issue_date"].ToString();
                         simsobj.sims_parent_guardian_passport_expiry_date = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                         simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                         simsobj.sims_parent_guardian_national_id_issue_date = dr["sims_parent_guardian_national_id_issue_date"].ToString();
                         simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();

                         parent_details.Add(simsobj);

                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, parent_details);

             }
             catch (Exception x)
             {

                 return Request.CreateResponse(HttpStatusCode.OK, parent_details);

             }

         }

         [Route("getSubjectDetails")]
         public HttpResponseMessage getSubjectDetails(string enroll_no, string grade_cd, string section_cd)
         {

             List<Sims042> goaltarget_list = new List<Sims042>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "M"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                            new SqlParameter("@sims_grade_code",grade_cd),
                             new SqlParameter("@sims_section_code",section_cd),
                           
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {

                             Sims042 simsobj = new Sims042();
                             simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                             simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                             simsobj.student_enroll_no = dr["sims_enroll_number"].ToString();
                             simsobj.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                             simsobj.teacherName = dr["TeacherName"].ToString();
                             simsobj.em_login_code = dr["em_login_code"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getStaffchilddetail")]
         public HttpResponseMessage getStaffchilddetail(string enroll_no)
         {

             List<Sims042> goaltarget_list = new List<Sims042>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "L"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                            
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {

                             Sims042 simsobj = new Sims042();
                             simsobj.comp_code = dr["comp_code"].ToString();
                             simsobj.comp_name = dr["comp_name"].ToString();
                             simsobj.em_login_code = dr["em_login_code"].ToString();
                             simsobj.employeeName = dr["EmployeeName"].ToString();
                             simsobj.codp_dept_no = dr["codp_dept_no"].ToString();
                             simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                             simsobj.codp_short_name = dr["codp_short_name"].ToString();
                             simsobj.dg_code = dr["dg_code"].ToString();
                             simsobj.dg_desc = dr["dg_desc"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getStudentList")]
         public HttpResponseMessage GetStudentList(string parent_no)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentList(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "PP", "PARENT"));

             List<parentClass> parentStudents = new List<parentClass>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentName",
                         new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),
                             //new SqlParameter("@username", HttpContext.Current.User.Identity.Name)
                              new SqlParameter("@username", parent_no)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             parentClass pc = new parentClass();

                             if (!string.IsNullOrEmpty(dr["StudentName"].ToString()))
                             {
                                 pc.sims_student_passport_fullname = dr["StudentName"].ToString();
                             }

                             else if (string.IsNullOrEmpty(dr["sims_student_nickname"].ToString()) == false && string.IsNullOrWhiteSpace(dr["sims_student_nickname"].ToString()) == false)
                             {
                                 pc.sims_student_passport_fullname = dr["sims_student_nickname"].ToString();
                             }
                             pc.sims_section_name_en = dr["sims_section_name_en"].ToString();

                             try
                             {
                                 pc.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                             }
                             catch (Exception ex)
                             {

                             }

                             if (!string.IsNullOrEmpty(pc.sims_student_passport_fullname))
                             {
                                 pc.sims_student_passport_fullname = pc.sims_student_passport_fullname.Split(' ')[0].ToString();
                             }
                             pc.sims_section_code = dr["sims_section_code"].ToString();

                             pc.sims_grade_code = dr["sims_grade_code"].ToString();
                             pc.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             pc.sims_student_img = dr["sims_student_img"].ToString();
                             pc.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                             pc.sims_acad_yr = dr["sims_academic_year"].ToString();
                             pc.sims_cur_code = dr["sims_cur_code"].ToString();
                             parentStudents.Add(pc);
                         }
                     }
                 }
             }
             catch (Exception ex)
             {
                 Log.Debug(ex);
                 Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
             }
             return Request.CreateResponse(HttpStatusCode.OK, parentStudents);
         }

         [Route("getLeaveApplicationDetails")]
         public HttpResponseMessage getLeaveApplicationDetails(string enroll_no)
         {

             List<LeaveApplication> goaltarget_list = new List<LeaveApplication>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_studet_profile_proc]",

                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "N"),
                           new SqlParameter("@sims_student_enroll_number",enroll_no),
                            
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {

                             LeaveApplication simsobj = new LeaveApplication();
                             simsobj.sims_leave_number = dr["sims_leave_number"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                             simsobj.sims_parent_id = dr["sims_parent_id"].ToString();
                             simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             simsobj.sims_leave_code = dr["sims_leave_code"].ToString();
                             simsobj.cl_desc = dr["cl_desc"].ToString();
                             if (!string.IsNullOrEmpty(dr["sims_leave_start_date"].ToString()))
                                 simsobj.sims_leave_start_date = dr["sims_leave_start_date"].ToString();
                             if (!string.IsNullOrEmpty(dr["sims_leave_end_date"].ToString()))
                                 simsobj.sims_leave_end_date = dr["sims_leave_end_date"].ToString();
                             simsobj.sims_leave_reason = dr["sims_leave_reason"].ToString();
                             if (!string.IsNullOrEmpty(dr["sims_leave_application_date"].ToString()))
                                 simsobj.sims_leave_application_date = dr["sims_leave_application_date"].ToString();
                             simsobj.sims_leave_status = dr["sims_leave_status"].ToString();
                             simsobj.sims_leave_assigned_to = dr["sims_leave_assigned_to"].ToString();
                             simsobj.sims_leave_status_changed_by = dr["sims_leave_status_changed_by"].ToString();
                             goaltarget_list.Add(simsobj);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

    }
}