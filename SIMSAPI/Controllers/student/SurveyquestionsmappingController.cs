﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
using System.Linq;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/surveyquestionmapping")]
    public class SurveyquestionsmappingController : ApiController
    {
        [Route("Get_survey_type")]
        public HttpResponseMessage Get_survey_type()
        {
            List<Survey> com_list = new List<Survey>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey FinnObj = new Survey();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_desc_en = dr["sims_survey_desc_en"].ToString();
                            FinnObj.sims_survey_desc_en = dr["sims_survey_desc_en"].ToString();

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_questionbank_type")]
        public HttpResponseMessage Get_questionbank_type()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_desc_en = dr["sims_questionbank_desc_en"].ToString();

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getquestionbankQuestionType")]
        public HttpResponseMessage getquestionbankQuestionType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyQuestionType()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<QuestionBank> mod_list = new List<QuestionBank>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[questionbankQuestionMappingProc]",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "L"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank simsobj = new QuestionBank();
                            simsobj.sims_question_question_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_question_question_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Get_questionbank_section")]
        public HttpResponseMessage Get_questionbank_section()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'B')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_questionbank_subsection")]
        public HttpResponseMessage Get_questionbank_subsection()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'C')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
                            FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_questionbank_skill")]
        public HttpResponseMessage Get_questionbank_skill()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'A')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
                            FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_survey_details")]
        public HttpResponseMessage Get_survey_details(string survey_code)
        {
            List<Survey> com_list = new List<Survey>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "SS"),
                              new SqlParameter("@sims_survey_code", survey_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey FinnObj = new Survey();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_subject = dr["sims_survey_subject"].ToString();
                            FinnObj.sims_survey_desc_en = dr["sims_survey_desc_en"].ToString();
                            FinnObj.sims_survey_start_date = dr["sims_survey_start_date"].ToString();
                            FinnObj.sims_survey_end_date = dr["sims_survey_end_date"].ToString();
                            FinnObj.sims_survey_user_group_code = dr["sims_survey_user_group_code"].ToString();
                            FinnObj.sims_survey_dept_no = dr["sims_survey_dept_no"].ToString();
                            FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();
                            FinnObj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            FinnObj.sims_survey_no_of_question_available = int.Parse(dr["sims_survey_no_of_question_available"].ToString());
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_questionbank_details")]
        public HttpResponseMessage Get_questionbank_details(string questionbank_code)
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "SS"),
                              new SqlParameter("@sims_questionbank_code", questionbank_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
                            FinnObj.sims_questionbank_desc_en = dr["sims_questionbank_desc_en"].ToString();
                            FinnObj.sims_questionbank_start_date = dr["sims_questionbank_start_date"].ToString();
                            FinnObj.sims_questionbank_end_date = dr["sims_questionbank_end_date"].ToString();
                            FinnObj.sims_questionbank_user_group_code = dr["sims_questionbank_user_group_code"].ToString();
                            FinnObj.sims_questionbank_dept_no = dr["sims_questionbank_dept_no"].ToString();
                            FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();
                            FinnObj.comn_user_group_name = dr["comn_user_group_name"].ToString();

                            FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
                            FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();
                            FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();
                            FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
                            FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();
                            FinnObj.sims_questionbank_no_of_question_available = int.Parse(dr["sims_questionbank_no_of_question_available"].ToString());

                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        //[Route("Get_questionbank_details")]
        //public HttpResponseMessage Get_questionbank_details(string sims_questionbank_code)
        //{
        //    List<QuestionBank> com_list = new List<QuestionBank>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
        //                new List<SqlParameter>()
        //                 {
        //                      new SqlParameter("@opr", "SS"),
        //                      new SqlParameter("@sims_questionbank_code", sims_questionbank_code)

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    QuestionBank FinnObj = new QuestionBank();

        //                    FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
        //                    FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
        //                    FinnObj.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
        //                    FinnObj.sims_questionbank_desc_en = dr["sims_questionbank_desc_en"].ToString();
        //                    FinnObj.sims_questionbank_start_date = dr["sims_questionbank_start_date"].ToString();
        //                    FinnObj.sims_questionbank_end_date = dr["sims_questionbank_end_date"].ToString();
        //                    FinnObj.sims_questionbank_user_group_code = dr["sims_questionbank_user_group_code"].ToString();
        //                    FinnObj.sims_questionbank_dept_no = dr["sims_questionbank_dept_no"].ToString();
        //                    FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();
        //                    FinnObj.comn_user_group_name = dr["comn_user_group_name"].ToString();

        //                    FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
        //                    FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();
        //                    FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
        //                    FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();
        //                    FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
        //                    FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();
        //                    FinnObj.sims_questionbank_no_of_question_available = int.Parse(dr["sims_questionbank_no_of_question_available"].ToString());

        //                    FinnObj.que_ans = new List<ques_answerN>();

        //                    ques_answerN qs = new ques_answerN();
        //                    qs.sims_questionbank_question_answer_desc_en = dr["sims_questionbank_question_answer_desc_en"].ToString();
        //                    qs.sims_questionbank_question_answer_code = dr["sims_questionbank_question_answer_code"].ToString();
        //                    qs.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
        //                    qs.sims_questionbank_question_answer_image = dr["sims_questionbank_question_answer_image"].ToString();

        //                    string str = dr["sims_questionbank_question_code"].ToString();

        //                    var v = (from p in com_list where p.sims_questionbank_question_code == str select p);
        //                    if (v.Count() == 0)
        //                    {
        //                        FinnObj.que_ans.Add(qs);
        //                        com_list.Add(FinnObj);

        //                    }
        //                    else
        //                    {
        //                        v.ElementAt(0).que_ans.Add(qs);
        //                    }


        //                };
        //            }
        //        }


        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, com_list);
        //}



        [Route("Get_Survey_Questions_Details")]
        public HttpResponseMessage Get_Survey_Questions_Details(string sims_survey_code)
        {
            List<Sims711> com_list = new List<Sims711>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QD"),
                               new SqlParameter("@sims_survey_code",sims_survey_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 FinnObj = new Sims711();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            FinnObj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                           // FinnObj.sims_survey_question_desc_Arabic = dr["sims_survey_question_desc_Arabic"].ToString();
                            FinnObj.sims_survey_question_desc_ot = dr["sims_survey_question_desc_ot"].ToString();
                            FinnObj.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                            FinnObj.sims_survey_question_weightage = dr["sims_survey_question_weightage"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_difficulty_level = dr["sims_survey_question_difficulty_level"].ToString();
                            FinnObj.sims_survey_question_rating_group_code = dr["sims_survey_question_rating_group_code"].ToString();
                            FinnObj.sims_survey_question_skip_flag = dr["sims_survey_question_skip_flag"].ToString().Equals("Y") ? true : false;
                            FinnObj.sims_survey_question_status = dr["sims_survey_question_status"].ToString().Equals("A") ? true : false;
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("Get_Survey_Questions_DetailsNew")]
        public HttpResponseMessage Get_Survey_Questions_DetailsNew(string sims_survey_code)
        {
            List<Sims711> com_list = new List<Sims711>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QA"),
                               new SqlParameter("@sims_survey_code",sims_survey_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 FinnObj = new Sims711();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            FinnObj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                            FinnObj.sims_survey_question_desc_ot = dr["sims_survey_question_desc_ot"].ToString();
                            FinnObj.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                            FinnObj.sims_survey_question_weightage = dr["sims_survey_question_weightage"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();

                            //try
                            //{
                            //    FinnObj.sims_survey_question_display_order_sort = int.Parse(new String(FinnObj.sims_survey_question_display_order.Where(Char.IsDigit).ToArray()));
                            //}
                            //catch (Exception ex) { }

                            FinnObj.sims_survey_question_difficulty_level = dr["sims_survey_question_difficulty_level"].ToString();
                            FinnObj.sims_survey_question_rating_group_code = dr["sims_survey_question_rating_group_code"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_status = dr["sims_survey_question_status"].ToString().Equals("A") ? true : false;
                          //  FinnObj.sims_survey_skill_code = dr["sims_survey_skill_code"].ToString();
                            //FinnObj.sims_survey_skill_name = dr["sims_survey_skill_name"].ToString();
                            //FinnObj.sims_survey_section_code = dr["sims_survey_section_code"].ToString();
                            //FinnObj.sims_survey_section_name = dr["sims_survey_section_name"].ToString();
                            //FinnObj.sims_survey_subsection_code = dr["sims_survey_subsection_code"].ToString();
                            //FinnObj.sims_survey_subsection_name = dr["sims_survey_subsection_name"].ToString();
                            //FinnObj.sims_survey_question_answer_explanation = dr["sims_survey_question_answer_explanation"].ToString();
                            //FinnObj.sims_survey_question_header_code = dr["sims_survey_question_header_code"].ToString();
                            //FinnObj.sims_survey_question_header_code_description = dr["sims_survey_question_header_code_description"].ToString();
                            //FinnObj.sims_survey_question_image_path = dr["sims_survey_question_image_path"].ToString();

                            FinnObj.que_ans = new List<ques_answer>();

                            ques_answer qs = new ques_answer();
                            qs.sims_survey_question_answer_desc_en = dr["sims_survey_question_answer_desc_en"].ToString();
                            qs.sims_survey_question_answer_code = dr["sims_survey_question_answer_code"].ToString();
                            qs.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                           // qs.sims_survey_question_answer_image = dr["sims_survey_question_answer_image"].ToString();
                            qs.sims_survey_question_correct_answer_flag = dr["sims_survey_question_correct_answer_flag"].ToString().Equals("1") ? true : false;

                            string str = dr["sims_survey_question_code"].ToString();

                            var v = (from p in com_list where p.sims_survey_question_code == str select p);
                            if (v.Count() == 0)
                            {
                                FinnObj.que_ans.Add(qs);
                                com_list.Add(FinnObj);

                            }
                            else
                            {
                                v.ElementAt(0).que_ans.Add(qs);
                            }
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }




        [Route("Get_questionbank_Questions_Details")]
        public HttpResponseMessage Get_questionbank_Questions_Details(string sims_questionbank_code)
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QD"),
                               new SqlParameter("@sims_questionbank_code",sims_questionbank_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_question_code = dr["sims_questionbank_question_code"].ToString();
                            FinnObj.sims_questionbank_question_desc_en = dr["sims_questionbank_question_desc_en"].ToString();
                            FinnObj.sims_questionbank_question_desc_ot = dr["sims_questionbank_question_desc_ot"].ToString();
                            FinnObj.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            FinnObj.sims_questionbank_question_weightage = dr["sims_questionbank_question_weightage"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();

                            try
                            {
                                FinnObj.sims_questionbank_question_display_order_sort = int.Parse(new String(FinnObj.sims_questionbank_question_display_order.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            FinnObj.sims_questionbank_question_difficulty_level = dr["sims_questionbank_question_difficulty_level"].ToString();
                            FinnObj.sims_questionbank_question_rating_group_code = dr["sims_questionbank_question_rating_group_code"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_status = dr["sims_questionbank_question_status"].ToString().Equals("A") ? true : false;
                            FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
                            FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();
                            FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();
                            FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
                            FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();
                            FinnObj.sims_questionbank_question_answer_explanation = dr["sims_questionbank_question_answer_explanation"].ToString();
                            FinnObj.sims_questionbank_question_header_code = dr["sims_questionbank_question_header_code"].ToString();
                            FinnObj.sims_questionbank_question_header_code_description = dr["sims_questionbank_question_header_code_description"].ToString();
                            FinnObj.sims_questionbank_question_image_path = dr["sims_questionbank_question_image_path"].ToString();

                            FinnObj.que_ans = new List<ques_answerN>();

                            ques_answerN qs = new ques_answerN();
                            qs.sims_questionbank_question_answer_desc_en = dr["sims_questionbank_question_answer_desc_en"].ToString();
                            qs.sims_questionbank_question_answer_code = dr["sims_questionbank_question_answer_code"].ToString();
                            qs.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            qs.sims_questionbank_question_answer_image = dr["sims_questionbank_question_answer_image"].ToString();
                            qs.sims_questionbank_question_correct_answer_flag = dr["sims_questionbank_question_correct_answer_flag"].ToString().Equals("T") ? true : false;

                            string str = dr["sims_questionbank_question_code"].ToString();

                            var v = (from p in com_list where p.sims_questionbank_question_code == str select p);
                            if (v.Count() == 0)
                            {
                                FinnObj.que_ans.Add(qs);
                                com_list.Add(FinnObj);

                            }
                            else
                            {
                                v.ElementAt(0).que_ans.Add(qs);
                            }
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getSurveyQuestionOntype")]
        public HttpResponseMessage getSurveyQuestionOntype(string sims_survey_question_type ,string sims_survey_question_difficulty_level)
        {
            List<Sims711> com_list = new List<Sims711>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QT"),
                               new SqlParameter("@sims_survey_question_type",sims_survey_question_type),
                               new SqlParameter("@sims_survey_question_difficulty_level",sims_survey_question_difficulty_level)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 FinnObj = new Sims711();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            FinnObj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                         //   FinnObj.sims_survey_question_desc_Arabic = dr["sims_survey_question_desc_Arabic"].ToString();
                            FinnObj.sims_survey_question_desc_ot = dr["sims_survey_question_desc_ot"].ToString();
                            FinnObj.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                            FinnObj.sims_survey_question_weightage = dr["sims_survey_question_weightage"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_difficulty_level = dr["sims_survey_question_difficulty_level"].ToString();
                            FinnObj.sims_survey_question_rating_group_code = dr["sims_survey_question_rating_group_code"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_status = dr["sims_survey_question_status"].ToString().Equals("A") ? true : false;
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        //[Route("getquestionbankQuestionOntype")]
        //public HttpResponseMessage getquestionbankQuestionOntype(string sims_questionbank_question_type, string sims_questionbank_question_difficulty_level)
        //{
        //    List<QuestionBank> com_list = new List<QuestionBank>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
        //                new List<SqlParameter>()
        //                 {
        //                      new SqlParameter("@opr", "QT"),
        //                       new SqlParameter("@sims_questionbank_question_type",sims_questionbank_question_type),
        //                       new SqlParameter("@sims_questionbank_question_difficulty_level",sims_questionbank_question_difficulty_level)

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    QuestionBank FinnObj = new QuestionBank();
        //                    FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
        //                    FinnObj.sims_questionbank_question_code = dr["sims_questionbank_question_code"].ToString();
        //                    FinnObj.sims_questionbank_question_desc_en = dr["sims_questionbank_question_desc_en"].ToString();
        //                    FinnObj.sims_questionbank_question_desc_ot = dr["sims_questionbank_question_desc_ot"].ToString();
        //                    FinnObj.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
        //                    FinnObj.sims_questionbank_question_weightage = dr["sims_questionbank_question_weightage"].ToString();
        //                    FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
        //                    FinnObj.sims_questionbank_question_difficulty_level = dr["sims_questionbank_question_difficulty_level"].ToString();
        //                    FinnObj.sims_questionbank_question_rating_group_code = dr["sims_questionbank_question_rating_group_code"].ToString();
        //                    FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
        //                    FinnObj.sims_questionbank_question_status = dr["sims_questionbank_question_status"].ToString().Equals("A") ? true : false;
        //                    com_list.Add(FinnObj);
        //                };
        //            }
        //        }


        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, com_list);
        //}

        [Route("getquestionbankQuestionOntype")]
        public HttpResponseMessage getquestionbankQuestionOntype(string sims_questionbank_question_type, string sims_questionbank_question_difficulty_level,string sims_questionbank_code,string sims_questionbank_skill_code)
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "QT"),
                               new SqlParameter("@sims_questionbank_question_type", sims_questionbank_question_type),
                               new SqlParameter("@sims_questionbank_question_difficulty_level", sims_questionbank_question_difficulty_level),
                               new SqlParameter("@sims_questionbank_code", sims_questionbank_code),
                               new SqlParameter("@sims_questionbank_skill_code", sims_questionbank_skill_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();

                            //FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            //FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            //FinnObj.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
                            //FinnObj.sims_questionbank_desc_en = dr["sims_questionbank_desc_en"].ToString();
                            //FinnObj.sims_questionbank_start_date = dr["sims_questionbank_start_date"].ToString();
                            //FinnObj.sims_questionbank_end_date = dr["sims_questionbank_end_date"].ToString();
                            //FinnObj.sims_questionbank_user_group_code = dr["sims_questionbank_user_group_code"].ToString();
                            //FinnObj.sims_questionbank_dept_no = dr["sims_questionbank_dept_no"].ToString();
                            //FinnObj.codp_dept_name = dr["codp_dept_name"].ToString();
                            //FinnObj.comn_user_group_name = dr["comn_user_group_name"].ToString();

                            //FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
                            //FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();
                            //FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            //FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();
                            //FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
                            //FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();
                            //FinnObj.sims_questionbank_no_of_question_available = int.Parse(dr["sims_questionbank_no_of_question_available"].ToString());

                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_question_code = dr["sims_questionbank_question_code"].ToString();
                            FinnObj.sims_questionbank_question_desc_en = dr["sims_questionbank_question_desc_en"].ToString();
                            FinnObj.sims_questionbank_question_desc_ot = dr["sims_questionbank_question_desc_ot"].ToString();
                            FinnObj.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            FinnObj.sims_questionbank_question_weightage = dr["sims_questionbank_question_weightage"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();

                            try
                            {
                                FinnObj.sims_questionbank_question_display_order_sort = int.Parse(new String(FinnObj.sims_questionbank_question_display_order.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            FinnObj.sims_questionbank_question_difficulty_level = dr["sims_questionbank_question_difficulty_level"].ToString();
                            FinnObj.sims_questionbank_question_rating_group_code = dr["sims_questionbank_question_rating_group_code"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_status = dr["sims_questionbank_question_status"].ToString().Equals("A") ? true : false;
                            FinnObj.sims_questionbank_skill_code = dr["sims_questionbank_skill_code"].ToString();
                            FinnObj.sims_questionbank_skill_name = dr["sims_questionbank_skill_name"].ToString();
                            FinnObj.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            FinnObj.sims_questionbank_section_name = dr["sims_questionbank_section_name"].ToString();
                            FinnObj.sims_questionbank_subsection_code = dr["sims_questionbank_subsection_code"].ToString();
                            FinnObj.sims_questionbank_subsection_name = dr["sims_questionbank_subsection_name"].ToString();
                            FinnObj.sims_questionbank_question_answer_explanation = dr["sims_questionbank_question_answer_explanation"].ToString();
                            FinnObj.sims_questionbank_question_header_code = dr["sims_questionbank_question_header_code"].ToString();
                            FinnObj.sims_questionbank_question_header_code_description = dr["sims_questionbank_question_header_code_description"].ToString();
                            FinnObj.sims_questionbank_question_image_path = dr["sims_questionbank_question_image_path"].ToString();

                            FinnObj.que_ans = new List<ques_answerN>();

                            ques_answerN qs = new ques_answerN();
                            qs.sims_questionbank_question_answer_desc_en = dr["sims_questionbank_question_answer_desc_en"].ToString();
                            qs.sims_questionbank_question_answer_code = dr["sims_questionbank_question_answer_code"].ToString();
                            qs.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            qs.sims_questionbank_question_answer_image = dr["sims_questionbank_question_answer_image"].ToString();
                            qs.sims_questionbank_question_correct_answer_flag = dr["sims_questionbank_question_correct_answer_flag"].ToString().Equals("T") ? true : false;
                            // qs.sims_question_answer_flag_value = dr["sims_questionbank_question_correct_answer_flag"].ToString().Equals("T") ? true : false;

                            string str = dr["sims_questionbank_question_code"].ToString();

                            var v = (from p in com_list where p.sims_questionbank_question_code == str select p);
                            if (v.Count() == 0)
                            {
                                FinnObj.que_ans.Add(qs);
                                com_list.Add(FinnObj);

                            }
                            else
                            {
                                v.ElementAt(0).que_ans.Add(qs);
                            }


                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getParagraph")]
        public HttpResponseMessage getParagraph()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_question_code = dr["sims_questionbank_question_code"].ToString();
                            FinnObj.sims_questionbank_question_desc_en = dr["sims_questionbank_question_desc_en"].ToString();
                            FinnObj.sims_questionbank_question_desc_ot = dr["sims_questionbank_question_desc_ot"].ToString();
                            FinnObj.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            FinnObj.sims_questionbank_question_weightage = dr["sims_questionbank_question_weightage"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_difficulty_level = dr["sims_questionbank_question_difficulty_level"].ToString();
                            FinnObj.sims_questionbank_question_rating_group_code = dr["sims_questionbank_question_rating_group_code"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_status = dr["sims_questionbank_question_status"].ToString().Equals("A") ? true : false;
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("CUDAddnewQuestionstoQuestionnaire")]
        public HttpResponseMessage CUDAddnewQuestionstoQuestionnaire(List<Sims711> sims_survey_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 obj in sims_survey_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "IQ"),
                                new SqlParameter("@sims_survey_code",obj.sims_survey_code),
                                new SqlParameter("@sims_survey_question_code", obj.sims_survey_question_code),
                                new SqlParameter("@sims_survey_question_display_order", obj.sims_survey_question_display_order),
                                new SqlParameter("@sims_survey_question_skip_flag", obj.sims_survey_question_skip_flag==true?"Y":"N"),
                                new SqlParameter("@sims_survey_question_status", obj.sims_survey_question_status==true?"A":"I")
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDAddnewQuestionstoQuestionnaireQuestoinBank")]
        public HttpResponseMessage CUDAddnewQuestionstoQuestionnaireQuestoinBank(List<QuestionBank> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "IQ"),
                                new SqlParameter("@sims_questionbank_code",obj.sims_questionbank_code),
                                new SqlParameter("@sims_questionbank_question_code", obj.sims_questionbank_question_code),
                                new SqlParameter("@sims_questionbank_question_display_order", obj.sims_questionbank_question_display_order),
                                new SqlParameter("@sims_questionbank_question_skip_flag", obj.sims_questionbank_question_skip_flag==true?"Y":"N"),
                                new SqlParameter("@sims_questionbank_question_status", obj.sims_questionbank_question_status==true?"A":"I")
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDQuestionBank")]
        public HttpResponseMessage CUDQuestionBank(List<QuestionBank> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "UU"),
                                new SqlParameter("@sims_questionbank_question_code", obj.sims_questionbank_question_code),
                                new SqlParameter("@sims_questionbank_question_type", obj.sims_questionbank_question_type),
                                new SqlParameter("@sims_questionbank_question_desc_en", obj.sims_questionbank_question_desc_en),
                                new SqlParameter("@sims_questionbank_question_desc_ot", obj.sims_questionbank_question_desc_ot),
                                new SqlParameter("@sims_questionbank_question_weightage", obj.sims_questionbank_question_weightage),
                                new SqlParameter("@sims_questionbank_question_display_order", obj.sims_questionbank_question_display_order),
                                new SqlParameter("@sims_questionbank_question_difficulty_level", obj.sims_questionbank_question_difficulty_level),
                                new SqlParameter("@sims_questionbank_section_code", obj.sims_questionbank_section_code),
                                new SqlParameter("@sims_questionbank_skill_code", obj.sims_questionbank_skill_code),
                                new SqlParameter("@sims_questionbank_subsection_code", obj.sims_questionbank_subsection_code),
                                new SqlParameter("@sims_questionbank_question_answer_explanation", obj.sims_questionbank_question_answer_explanation),
                                new SqlParameter("@sims_questionbank_question_header_code", obj.sims_questionbank_question_header_code),

                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDQuestionBankANS")]
        public HttpResponseMessage CUDQuestionBankANS(List<QuestionBank> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "UA"),
                                new SqlParameter("@sims_questionbank_question_code", obj.sims_questionbank_question_code),
                                new SqlParameter("@sims_questionbank_question_answer_code", obj.sims_questionbank_question_answer_code),
                                new SqlParameter("@sims_questionbank_question_answer_desc_en", obj.sims_questionbank_question_answer_desc_en),
                                new SqlParameter("@sims_questionbank_question_correct_answer_flag", obj.sims_questionbank_question_correct_answer_flag),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDSurveyQuestionAnswerforsurveyquestionmapping")]
        public HttpResponseMessage CUDSurveyQuestionAnswerforsurveyquestionmapping(List<Sims711> data)
        {
            string insert = "";
            List<Sims711> com_list = new List<Sims711>();
            int cnt = 1;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 simsobj in data)
                    {
                        SqlDataReader ins = db.ExecuteStoreProcedure("[sims].[SurveyQuestionMappingProc]",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", 'I'),
                            //new SqlParameter("@sims_survey_question_code",	            simsobj.sims_survey_question_code),	
                            new SqlParameter("@sims_survey_question_desc_en",           simsobj.sims_survey_question_desc_en),
                           // new SqlParameter("@sims_survey_question_desc_Arabic",           simsobj.sims_survey_question_desc_Arabic),
                            new SqlParameter("@sims_survey_question_desc_ot",           simsobj.sims_survey_question_desc_ot),
                            new SqlParameter("@sims_survey_question_type",              simsobj.sims_survey_question_type),
                            new SqlParameter("@sims_survey_question_weightage",         simsobj.sims_survey_question_weightage),
                            new SqlParameter("@sims_survey_question_display_order",     simsobj.sims_survey_question_display_order),
                            new SqlParameter("@sims_survey_question_difficulty_level",  simsobj.sims_survey_question_difficulty_level),
                            new SqlParameter("@sims_survey_question_rating_group_code", simsobj.sims_survey_question_rating_group_code),
                            new SqlParameter("@sims_survey_question_status",            simsobj.sims_survey_question_status),

                           //new SqlParameter("@sims_survey_question_code",                 simsobj.sims_survey_question_code),
                           //new SqlParameter("@sims_survey_question_answer_code",          simsobj.sims_survey_question_answer_code),
                            new SqlParameter("@sims_survey_question_answer_desc_en",       simsobj.sims_survey_question_answer_desc_en),
                            new SqlParameter("@sims_survey_question_answer_desc_ot",       simsobj.sims_survey_question_answer_desc_ot),
                            new SqlParameter("@sims_survey_question_answer_weightage",     simsobj.sims_survey_question_answer_weightage),
                            new SqlParameter("@sims_survey_question_answer_display_order", simsobj.sims_survey_question_answer_display_order),
                            new SqlParameter("@sims_survey_question_answer_status",        simsobj.sims_survey_question_answer_status),
                            new SqlParameter("@sims_survey_question_correct_answer_flag",  simsobj.sims_survey_question_correct_answer_flag==true?"T":"F"),
                            new SqlParameter("@count",cnt)
                     });
                        if (ins.Read())
                        {
                            insert = ins["sims_survey_question_code"].ToString();
                        }

                        cnt = cnt + 1;
                        ins.Close();
                    }



                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QN"),
                               new SqlParameter("@sims_survey_question_code",insert)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 FinnObj = new Sims711();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            FinnObj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                           // FinnObj.sims_survey_question_desc_Arabic = dr["sims_survey_question_desc_Arabic"].ToString();                            
                            FinnObj.sims_survey_question_desc_ot = dr["sims_survey_question_desc_ot"].ToString();
                            FinnObj.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                            FinnObj.sims_survey_question_weightage = dr["sims_survey_question_weightage"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_difficulty_level = dr["sims_survey_question_difficulty_level"].ToString();
                            FinnObj.sims_survey_question_rating_group_code = dr["sims_survey_question_rating_group_code"].ToString();
                            FinnObj.sims_survey_question_display_order = dr["sims_survey_question_display_order"].ToString();
                            FinnObj.sims_survey_question_status = dr["sims_survey_question_status"].ToString().Equals("A") ? true : false;
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("CUDquestionbankQuestionAnswerforquestionbankquestionmapping")]
        public HttpResponseMessage CUDquestionbankQuestionAnswerforquestionbankquestionmapping(List<QuestionBank> data)
        {
            string insert = "";
            List<QuestionBank> com_list = new List<QuestionBank>();
            int cnt = 1;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank simsobj in data)
                    {
                        SqlDataReader ins = db.ExecuteStoreProcedure("[sims].[questionbankQuestionMappingProc]",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", 'I'),
                            //new SqlParameter("@sims_questionbank_question_code",	            simsobj.sims_questionbank_question_code),	
                            new SqlParameter("@sims_questionbank_question_desc_en",           simsobj.sims_questionbank_question_desc_en),
                            new SqlParameter("@sims_questionbank_question_desc_ot",           simsobj.sims_questionbank_question_desc_ot),
                            new SqlParameter("@sims_questionbank_question_type",              simsobj.sims_questionbank_question_type),
                            new SqlParameter("@sims_questionbank_question_weightage",         simsobj.sims_questionbank_question_weightage),
                            new SqlParameter("@sims_questionbank_question_display_order",     simsobj.sims_questionbank_question_display_order),
                            new SqlParameter("@sims_questionbank_question_difficulty_level",  simsobj.sims_questionbank_question_difficulty_level),
                            new SqlParameter("@sims_questionbank_question_rating_group_code", simsobj.sims_questionbank_question_rating_group_code),
                            new SqlParameter("@sims_questionbank_question_status",            simsobj.sims_questionbank_question_status),
                            new SqlParameter("@sims_questionbank_question_section",            simsobj.sims_questionbank_question_section),
                            new SqlParameter("@sims_questionbank_question_subsection",            simsobj.sims_questionbank_question_subsection),
                            new SqlParameter("@sims_questionbank_question_skill",            simsobj.sims_questionbank_question_skill),
                            new SqlParameter("@sims_questionbank_question_header_code",            simsobj.sims_questionbank_question_header_code),
                           //new SqlParameter("@sims_questionbank_question_code",                 simsobj.sims_questionbank_question_code),
                           //new SqlParameter("@sims_questionbank_question_answer_code",          simsobj.sims_questionbank_question_answer_code),
                            new SqlParameter("@sims_questionbank_question_answer_desc_en",       simsobj.sims_questionbank_question_answer_desc_en),
                            new SqlParameter("@sims_questionbank_question_answer_desc_ot",       simsobj.sims_questionbank_question_answer_desc_ot),
                            new SqlParameter("@sims_questionbank_question_answer_weightage",     simsobj.sims_questionbank_question_answer_weightage),
                            new SqlParameter("@sims_questionbank_question_answer_display_order", simsobj.sims_questionbank_question_answer_display_order),
                            new SqlParameter("@sims_questionbank_question_answer_status",        simsobj.sims_questionbank_question_answer_status),
                            new SqlParameter("@sims_questionbank_question_correct_answer_flag",  simsobj.sims_questionbank_question_correct_answer_flag==true?"T":"F"),
                            new SqlParameter("@count",cnt)
                     });
                        if (ins.Read())
                        {
                            insert = ins["sims_questionbank_question_code"].ToString();
                        }

                        cnt = cnt + 1;
                        ins.Close();
                    }



                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "QN"),
                               new SqlParameter("@sims_questionbank_question_code",insert)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank FinnObj = new QuestionBank();
                            FinnObj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            FinnObj.sims_questionbank_question_code = dr["sims_questionbank_question_code"].ToString();
                            FinnObj.sims_questionbank_question_desc_en = dr["sims_questionbank_question_desc_en"].ToString();
                            FinnObj.sims_questionbank_question_desc_ot = dr["sims_questionbank_question_desc_ot"].ToString();
                            FinnObj.sims_questionbank_question_type = dr["sims_questionbank_question_type"].ToString();
                            FinnObj.sims_questionbank_question_weightage = dr["sims_questionbank_question_weightage"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_difficulty_level = dr["sims_questionbank_question_difficulty_level"].ToString();
                            FinnObj.sims_questionbank_question_rating_group_code = dr["sims_questionbank_question_rating_group_code"].ToString();
                            FinnObj.sims_questionbank_question_display_order = dr["sims_questionbank_question_display_order"].ToString();
                            FinnObj.sims_questionbank_question_status = dr["sims_questionbank_question_status"].ToString().Equals("A") ? true : false;
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("Get_question_difficalty")]
        public HttpResponseMessage Get_question_difficalty()
        {
            List<Sims711> com_list = new List<Sims711>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "DL")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 obj = new Sims711();
                            obj.sims_survey_question_difficulty_level = dr["sims_appl_parameter"].ToString();
                            obj.sims_survey_question_difficulty_level_name = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_question_difficaltyQuestionBank")]
        public HttpResponseMessage Get_question_difficaltyQuestionBank()
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "DL")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank obj = new QuestionBank();
                            obj.sims_questionbank_question_difficulty_level = dr["sims_appl_parameter"].ToString();
                            obj.sims_questionbank_question_difficulty_level_name = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("CUDUpdatePicsF")]
        public HttpResponseMessage CUDUpdatePicsF(QuestionBank simsobj)//string opr, string data
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[questionbankQuestionMappingProc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_questionbank_question_code", simsobj.sims_questionbank_question_code),
                            new SqlParameter("@sims_questionbank_question_image_path", simsobj.sims_questionbank_question_image_path),
                            new SqlParameter("@sims_questionbank_question_answer_code", simsobj.sims_questionbank_question_answer_code),

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            insert = true;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, insert);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    insert = false;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getTestSubmist")]
        public HttpResponseMessage getTestSubmist(string sims_questionbank_code,string sims_questionbank_user_response_user_code,string attempt)
        {
            List<QuestionBank> com_list = new List<QuestionBank>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_test_submit_test",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "S"),
                               new SqlParameter("@sims_questionbank_code", sims_questionbank_code),
                               new SqlParameter("@sims_questionbank_user_response_user_code", sims_questionbank_user_response_user_code),
                               new SqlParameter("@attempt", attempt)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank obj = new QuestionBank();
                            obj.total_questionbank_question = dr["total_questionbank_question"].ToString();
                            obj.total_question_attempted = dr["total_question_attempted"].ToString();
                            obj.left_question = dr["left_question"].ToString();
                            obj.total_correct_answer = dr["total_correct_answer"].ToString();
                            obj.total_wrong_answer = dr["total_wrong_answer"].ToString();
                            obj.maximum_marks = dr["maximum_marks"].ToString();
                            obj.correct_marks = dr["correct_marks"].ToString();
                            obj.wrong_marks = dr["wrong_marks"].ToString();
                            obj.total_marks = dr["total_marks"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("CUDQuestionparagraph")]
        public HttpResponseMessage CUDQuestionparagraph(List<QuestionBank> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "UP"),
                                new SqlParameter("@sims_questionbank_question_code", obj.sims_questionbank_question_code),
                                new SqlParameter("@sims_questionbank_question_desc_en", obj.sims_questionbank_question_desc_en),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDADDQuestionparagraph")]
        public HttpResponseMessage CUDADDQuestionparagraph(List<QuestionBank> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (QuestionBank obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].questionbankQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "IP"),
                                new SqlParameter("@sims_questionbank_question_type", obj.sims_questionbank_question_type),
                                new SqlParameter("@sims_questionbank_question_desc_en", obj.sims_questionbank_question_desc_en),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }





        [Route("CUDSurveyNew")]
        public HttpResponseMessage CUDSurveyNew(List<Sims711> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "UU"),
                                new SqlParameter("@sims_survey_question_code", obj.sims_survey_question_code),
                                new SqlParameter("@sims_survey_question_type", obj.sims_survey_question_type),
                                new SqlParameter("@sims_survey_question_desc_en", obj.sims_survey_question_desc_en),
                                new SqlParameter("@sims_survey_question_desc_ot", obj.sims_survey_question_desc_ot),
                                new SqlParameter("@sims_survey_question_weightage", obj.sims_survey_question_weightage),
                                new SqlParameter("@sims_survey_question_display_order", obj.sims_survey_question_display_order),
                                new SqlParameter("@sims_survey_question_difficulty_level", obj.sims_survey_question_difficulty_level),
                                //new SqlParameter("@sims_survey_section_code", obj.sims_questionbank_section_code),
                                //new SqlParameter("@sims_survey_skill_code", obj.sims_questionbank_skill_code),
                                //new SqlParameter("@sims_survey_subsection_code", obj.sims_questionbank_subsection_code),
                                //new SqlParameter("@sims_survey_question_answer_explanation", obj.sims_questionbank_question_answer_explanation),
                                //new SqlParameter("@sims_survey_question_header_code", obj.sims_questionbank_question_header_code),

                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDSurveyANSNew")]
        public HttpResponseMessage CUDSurveyANSNew(List<Sims711> sims_questionbank_question_type)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 obj in sims_questionbank_question_type)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveyQuestionMappingProc",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "UA"),
                                new SqlParameter("@sims_survey_question_code", obj.sims_survey_question_code),
                                new SqlParameter("@sims_survey_question_answer_code", obj.sims_survey_question_answer_code),
                                new SqlParameter("@sims_survey_question_answer_desc_en", obj.sims_survey_question_answer_desc_en),
                                new SqlParameter("@sims_survey_question_correct_answer_flag", obj.sims_survey_question_correct_answer_flag),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}