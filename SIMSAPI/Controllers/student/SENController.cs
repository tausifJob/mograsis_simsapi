﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.student
{
    [RoutePrefix("api/student")]
    [BasicAuthentication]
    public class SENController : ApiController
    {


        [Route("getStudSearch")]
        public HttpResponseMessage getStudSearch(string sims_cur_code, string sims_academic_year, string sims_grade_code, string value)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim987> stud_list = new List<Sim987>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'), 
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            //new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@value",value),
                            
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sim987 simsobj = new Sim987();
                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();
                           // simsobj.sims_admission_date = dr["sims_admission_date"].ToString();
                            simsobj.name = dr["name"].ToString();
                            simsobj.sims_admission_sen_approve_status = dr["sims_admission_sen_approve_status"].ToString().Equals("1") ? true : false;
                           simsobj.sims_admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_sen_status_remark = dr["sims_sen_status_remark"].ToString();
                            simsobj.sims_admission_sr_no = dr["sims_admission_sr_no"].ToString();
                            simsobj.sims_admission_sen_approve_status = dr["sims_admission_sen_approve_status"].ToString().Equals("1") ? true : false;
                            simsobj.sims_admission_special_education_status = dr["sims_admission_special_education_status"].ToString().Equals("1") ? true : false;
                            stud_list.Add(simsobj);

                           
                        }
                    }
                }
            }
            catch (Exception x)
            {
              //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }


        [Route("CUDStudentSen")]
        public HttpResponseMessage CUDStudentSen(List<Sim987> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sim987 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                                new SqlParameter("@sims_sen_status_remark", simsobj.sims_sen_status_remark),
                                new SqlParameter("@sims_admission_special_education_status", simsobj.sims_admission_special_education_status==true?"1":"0"),
                                   
                                new SqlParameter("@value", simsobj.value),
                                new SqlParameter("@user_name", simsobj.user_name),
                                new SqlParameter("@sims_admission_sr_no", simsobj.sims_admission_sr_no),
                                

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    message.strMessage = st;
                        // inserted = false;
                        //}
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CIImgUpl")]
        public HttpResponseMessage CIImgUpl(List<Sim987> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sim987 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'J'),
                                new SqlParameter("@sims_sen_document_transaction_number", simsobj.sims_sen_document_transaction_number),
                                new SqlParameter("@sims_sen_document_admission_number", simsobj.sims_sen_document_admission_number),
                                new SqlParameter("@sims_sen_document_remark", simsobj.sims_sen_document_remark),
                                new SqlParameter("@sims_sen_document_name", simsobj.sims_sen_document_name),
                                new SqlParameter("@sims_sen_document_created_by", simsobj.sims_sen_document_created_by),
                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    message.strMessage = st;
                        // inserted = false;
                        //}
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getDoclist")]
        public HttpResponseMessage getDoclist(string sims_sen_document_transaction_number, string sims_sen_document_admission_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim987> stud_list = new List<Sim987>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'), 
                            new SqlParameter("@sims_sen_document_transaction_number",sims_sen_document_transaction_number),
                            new SqlParameter("@sims_sen_document_admission_number",sims_sen_document_admission_number),                    
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim987 simsobj = new Sim987();
                            simsobj.sims_sen_document_transaction_number = dr["sims_sen_document_transaction_number"].ToString();
                            simsobj.sims_sen_document_admission_number = dr["sims_sen_document_admission_number"].ToString();
                            simsobj.sims_sen_document_number = dr["sims_sen_document_number"].ToString();
                            simsobj.sims_sen_document_name = dr["sims_sen_document_name"].ToString();
                            simsobj.sims_sen_document_remark = dr["sims_sen_document_remark"].ToString();
                            simsobj.sims_sen_document_created_by = dr["sims_sen_document_created_by"].ToString();
                            simsobj.sims_sen_document_created_date = dr["sims_sen_document_created_date"].ToString();
                            stud_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }
    }
}