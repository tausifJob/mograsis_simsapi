﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.student
{
    [RoutePrefix("api/admissionQuotaUser")]

    public class admissionQuotaUserController : ApiController
    {

        public string section_term_section_code { get; set; }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAdmissionQuotaFromCur")]
        public HttpResponseMessage getAdmissionQuotaFromCur(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAdmissionQuotaFromCur(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAdmissionQuotaFromCur", cur_code, ac_year));

            List<sims_admission_quota_master> admission_quota_master = new List<sims_admission_quota_master>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[getAdmissionQuotaUser_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'P'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_quota_master objNew = new sims_admission_quota_master();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_quota_desc = dr["sims_quota_desc"].ToString();
                            objNew.sims_q_strength = dr["sims_q_strength"].ToString();
                            objNew.sims_q_status = dr["sims_q_status"].ToString();
                            objNew.sims_quota_id = dr["sims_quota_id"].ToString();
                            admission_quota_master.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, admission_quota_master);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, admission_quota_master);
            }
        }

        [Route("getAllAdmissionQuotaUser")]
        public HttpResponseMessage getAllAdmissionQuotaUser()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAdmissionQuotaUser(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getAllAdmissionQuotaUser"));

            List<sims_admission_quota_user> quota_list = new List<sims_admission_quota_user>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[getAdmissionQuotaUser_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_quota_user objNew = new sims_admission_quota_user();
                            objNew.sims_q_cur_code = dr["sims_q_cur_code"].ToString();
                            objNew.sims_q_academic_year = dr["sims_q_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_q_grade_code = dr["sims_q_grade_code"].ToString();
                            objNew.sims_quota_id = dr["sims_quota_id"].ToString();
                            objNew.sims_q_user_name = dr["sims_q_user_name"].ToString();
                            //objNew.sims_q_user_alias = dr["user_alias_name"].ToString();
                            objNew.sims_q_strength = int.Parse(dr["sims_q_strength"].ToString());
                            objNew.sims_q_status = dr["sims_q_status"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_quota_desc = dr["sims_quota_desc"].ToString();
                            objNew.mq_strength = dr["mq_strength"].ToString();
                            quota_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, quota_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, quota_list);
            }
        }

        [Route("addAdmissionQuotaForUser")]
        public HttpResponseMessage addAdmissionQuotaForUser(sims_admission_quota_user adminUserObj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : addAdmissionQuotaForUser(),PARAMETERS ::adminUserObj{2}";
            Log.Debug(string.Format(debug, "STUDENT", "addAdmissionQuotaForUser", adminUserObj));

            //sims_admission_quota_user from = Newtonsoft.Json.JsonConvert.DeserializeObject<sims_admission_quota_user>(adminUserObj);
            Message message = new Message();
            try
            {
                if (adminUserObj != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[getAdmissionQuotaUser_proc]", new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_cur_code", adminUserObj.sims_q_cur_code),
                                new SqlParameter("@sims_academic_year", adminUserObj.sims_q_academic_year),
                                new SqlParameter("@sims_q_grade_code", adminUserObj.sims_q_grade_code),
                                new SqlParameter("@sims_quota_id", adminUserObj.sims_quota_id),
                                new SqlParameter("@sims_q_user_name", adminUserObj.sims_q_user_name),
                                new SqlParameter("@sims_quota_sterngth", adminUserObj.sims_q_strength),
                                new SqlParameter("@sims_quota_status", adminUserObj.sims_q_status.Equals("true")?"A":"I")
                        });
                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "Admission User Quota Added Successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "User Name Already Exists";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;

                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                //return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("updateAdmissionQuotaForUser")]
        public HttpResponseMessage updateAdmissionQuotaForUser(sims_admission_quota_user adminUserObj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : updateAdmissionQuotaForUser(),PARAMETERS ::adminUserObj{2}";
            Log.Debug(string.Format(debug, "STUDENT", "updateAdmissionQuotaForUser", adminUserObj));

            //sims_admission_quota_user from = Newtonsoft.Json.JsonConvert.DeserializeObject<sims_admission_quota_user>(adminUserObj);

            Message message = new Message();
            try
            {
                if (adminUserObj != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[getAdmissionQuotaUser_proc]", new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_cur_code", adminUserObj.sims_q_cur_code),
                                new SqlParameter("@sims_academic_year", adminUserObj.sims_q_academic_year),
                                new SqlParameter("@sims_q_grade_code", adminUserObj.sims_q_grade_code),
                                new SqlParameter("@sims_quota_id", adminUserObj.sims_quota_id),
                                new SqlParameter("@sims_q_user_name", adminUserObj.sims_q_user_name),
                                new SqlParameter("@sims_quota_sterngth", adminUserObj.sims_q_strength),
                                new SqlParameter("@sims_quota_status", adminUserObj.sims_q_status.Equals("true")?"A":"I")
                        });

                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "Admission User Quota Updated Successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Not Updated";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                //return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        // ALL Grade with params
        [Route("AllGrades_p")]
        public HttpResponseMessage AllGrades_p(Sims028 pa)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AllGrades_p(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "AllGrades_p"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[getAdmissionQuotaUser_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'G'),
                                new SqlParameter("@sims_cur_code", pa.sims_cur_code),
                                new SqlParameter("@sims_academic_year", pa.sims_academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }
    }
}

