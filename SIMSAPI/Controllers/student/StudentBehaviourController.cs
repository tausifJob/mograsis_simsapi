﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using Telerik.Reporting.Processing;
using System.Web;
using System.IO;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.FEE;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudentBehaviour")]
    public class StudentBehaviourController : ApiController
    {
        
        [Route("getAllStudentBehaviour")]
        public HttpResponseMessage getAllStudentBehaviour(string cur, string a_year, string grades, string section) //, string advanced_year
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllStudentBehaviour(),PARAMETERS ::";
            int i = 0;
            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_student_behaviour]",
                        new List<SqlParameter>()
                         {
                              //new SqlParameter("@opr", "B"),
                              new SqlParameter("@opr", "P"),
                              new SqlParameter("@sims_cur_code", cur),
                              new SqlParameter("@sims_academic_year", a_year),
                              new SqlParameter("@sims_sims_grades", grades),
                              new SqlParameter("@sims_sims_section",section),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            i = i + 1;
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.sims_behaviour_date = db.UIDDMMYYYYformat(dr["sims_behaviour_date"].ToString());
                            simsobj.sims_behaviour_code = dr["sims_behaviour_code"].ToString();
                            simsobj.sims_behaviour_note = dr["sims_behaviour_note"].ToString();                            
                            simsobj.datastatus = i.ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getBehaviourComment")]
        public HttpResponseMessage getBehaviourComment(string cur, string a_year, string grades) //, string section
        {
            List<StudentImg> mod_list = new List<StudentImg>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_student_behaviour]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "V"),
                         new SqlParameter("@sims_cur_code", cur),
                         new SqlParameter("@sims_academic_year", a_year),
                         new SqlParameter("@sims_sims_grades", grades),
                       //  new SqlParameter("@sims_sims_section",section),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StudentImg simsobj = new StudentImg();
                            simsobj.sims_behaviour_code = dr["sims_behaviour_code"].ToString();
                            simsobj.sims_behaviour_description = dr["sims_behaviour_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CUDInsertStudentBehaviour")]
        public HttpResponseMessage CUDInsertStudentBehaviour(List<Sims190> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims190 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_student_behaviour]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_behaviour_enroll_number",simsobj.sims_behaviour_enroll_number),                           
                            new SqlParameter("@sims_behaviour_code", simsobj.sims_behaviour_code),
                            new SqlParameter("@sims_behaviour_note",simsobj.sims_behaviour_note),
                            new SqlParameter("@sims_behaviour_date", db.DBYYYYMMDDformat(simsobj.sims_behaviour_date))                            
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
      

    }
}