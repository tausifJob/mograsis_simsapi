﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudentAssessmentSchedule")]
    public class StudentAssessmentScheduleController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Student Assessment Schedule

        [Route("GetAssesmentTeacher")]
        public HttpResponseMessage GetAssesmentTeacher()
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_assesment_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "A"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetEmployeeApplicant")]
        public HttpResponseMessage GetEmployeeApplicant()
        {
            List<EmpAss> mod_list = new List<EmpAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_schedule_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", "E"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpAss simsobj = new EmpAss();
                            simsobj.em_applicant_id = dr["em_applicant_id"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetAssesmentSchedule")]
        public HttpResponseMessage GetAssesmentSchedule(string cur_code, string acad_yr, string grade_code)
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_assesment_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_acad_yr",acad_yr),
                                new SqlParameter("@sims_grade_code",grade_code),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_pros_no = dr["sims_admission_number"].ToString();
                            simsobj.sims_student_name = dr["stud_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_acad_yr = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_appl_date = dr["sims_pros_date_created"].ToString();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            simsobj.sims_assesment_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_assesment_teacher_name = dr["teacher_name"].ToString();
                            simsobj.sims_assesment_time = dr["sims_assesment_time"].ToString();
                            simsobj.sims_admission_father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.sims_admission_father_mobile = dr["sims_admission_father_mobile"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CUDInsertAssessmentScheduleDetails")]
        public HttpResponseMessage CUDInsertAssessmentScheduleDetails(List<studAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_assesment_schedule_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_yr", simsobj.sims_acad_yr),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_assesment_time",simsobj.sims_assesment_time),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //GetAssesmentScheduleObaCente
        [Route("GetAssesmentScheduleObaCenter")]
        public HttpResponseMessage GetAssesmentScheduleObaCenter(string cur_code, string acad_yr, string grade_code)
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_assesment_schedule_oba_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_acad_yr",acad_yr),
                                new SqlParameter("@sims_grade_code",grade_code),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_pros_no = dr["sims_admission_number"].ToString();
                            simsobj.sims_student_name = dr["stud_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_acad_yr = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_appl_date = dr["sims_pros_date_created"].ToString();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            simsobj.sims_assesment_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_assesment_teacher_name = dr["teacher_name"].ToString();
                            simsobj.sims_assesment_time = dr["sims_assesment_time"].ToString();
                            simsobj.sims_admission_father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.sims_admission_father_mobile = dr["sims_admission_father_mobile"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CUDInsertAssessmentScheduleDetailsObaCenter")]
        public HttpResponseMessage CUDInsertAssessmentScheduleDetailsObaCenter(List<studAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_assesment_schedule_oba_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_yr", simsobj.sims_acad_yr),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_assesment_time",simsobj.sims_assesment_time),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user),
                            new SqlParameter("@i",simsobj.sims_assesment_std),
                            new SqlParameter("@min",simsobj.sims_assesment_interval_min),
                            new SqlParameter("@hour",simsobj.sims_assesment_interval_hr),


                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("CUDsend_email")]
        public HttpResponseMessage CUDsend_email(List<studAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_assesment_schedule_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_yr", simsobj.sims_acad_yr),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_assesment_time",simsobj.sims_assesment_time),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region Employee Assessment Schedule


        [Route("GetEmployeeRound")]
        public HttpResponseMessage GetEmployeeRound()
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "B"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetEmployeeAssesmentSchedule")]
        public HttpResponseMessage GetEmployeeAssesmentSchedule(string from_date, string to_date, string round, string em_applicant_id)
        {
            List<EmpAss> mod_list = new List<EmpAss>();
            if (from_date == "undefined" || from_date == "")
            {
                from_date = null;
            }
            if (to_date == "undefined" || to_date == "")
            {
                to_date = null;
            }
            if (round == "undefined" || round == "")
            {
                round = null;
            }
            if (em_applicant_id == "undefined" || em_applicant_id == "")
            {
                em_applicant_id = null;
            }


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                                new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                                new SqlParameter("@em_round",round),
                                new SqlParameter("@em_applicant_id",em_applicant_id),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpAss simsobj = new EmpAss();
                            simsobj.sims_pros_no = dr["em_applicant_id"].ToString();
                            simsobj.sims_student_name = dr["emp_name"].ToString();
                            simsobj.em_sr_no = dr["em_sr_no"].ToString();
                            simsobj.sims_appl_date = dr["em_application_date"].ToString();
                            simsobj.sims_assesment_date = dr["em_interview_date"].ToString();
                            simsobj.sims_assesment_time = dr["em_interview_time"].ToString();

                            simsobj.em_employee_id = dr["em_employee_id"].ToString();
                            simsobj.sims_assesment_teacher_name = dr["teacher_name"].ToString();

                            simsobj.sims_admission_father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.sims_admission_father_mobile = dr["sims_admission_father_mobile"].ToString();
                            try
                            {
                                simsobj.em_round = dr["em_round"].ToString();
                                simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            }
                            catch(Exception e) { }            

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CUDInsertEmployeeScheduleDetails")]
        public HttpResponseMessage CUDInsertEmployeeScheduleDetails(List<EmpAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (EmpAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_employee_assesment_schedule_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_assesment_time",simsobj.sims_assesment_time),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user),
                            new SqlParameter("@em_round",simsobj.em_round)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetEmployeeProfileData")]//search
        public HttpResponseMessage GetEmployeeProfileData(string sims_pros_no)
        {
            List<EmpAss> code_list = new List<EmpAss>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_schedule_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'X'),
                           new SqlParameter("@em_applicant_id",sims_pros_no),

                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpAss obj = new EmpAss();
                            obj.em_applicant_id = dr["em_applicant_id"].ToString();

                            obj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            obj.em_desg_code = dr["em_desg_code"].ToString();
                            obj.dg_desc = dr["dg_desc"].ToString();
                            obj.em_nation_code = dr["em_nation_code"].ToString();
                            obj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            obj.em_dept_code = dr["em_dept_code"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();
                            obj.ename = dr["ename"].ToString();
                            obj.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            obj.em_sex = dr["em_sex"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();
                            obj.em_email = dr["em_email"].ToString();
                            obj.em_application_date = dr["em_application_date"].ToString();
                            obj.em_company_code = dr["em_company_code"].ToString();
                            obj.em_salutation = dr["em_salutation"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_middle_name = dr["em_middle_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.em_family_name = dr["em_family_name"].ToString();
                            obj.em_name_ot = dr["em_name_ot"].ToString();
                            obj.em_marital_status = dr["em_marital_status"].ToString();
                            obj.em_religion_code = dr["em_religion_code"].ToString();
                            obj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            obj.em_appartment_number = dr["em_appartment_number"].ToString();
                            obj.em_building_number = dr["em_building_number"].ToString();
                            obj.em_street_number = dr["em_street_number"].ToString();
                            obj.em_area_number = dr["em_area_number"].ToString();
                            obj.em_summary_address = dr["em_summary_address"].ToString();
                            obj.em_city = dr["em_city"].ToString();
                            obj.em_state = dr["em_state"].ToString();
                            obj.em_country_code = dr["em_country_code"].ToString();
                            obj.em_phone = dr["em_phone"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();

                            obj.em_email = dr["em_email"].ToString();
                            obj.em_fax = dr["em_fax"].ToString();
                            obj.em_po_box = dr["em_po_box"].ToString();
                            obj.em_passport_number = dr["em_passport_number"].ToString();
                            obj.em_passport_issue_date = dr["em_passport_issue_date"].ToString();
                            obj.em_passport_expiry_date = dr["em_passport_expiry_date"].ToString();
                            obj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            obj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            obj.em_visa_number = dr["em_visa_number"].ToString();
                            obj.em_visa_issue_date = dr["em_visa_issue_date"].ToString();
                            obj.em_visa_expiry_date = dr["em_visa_expiry_date"].ToString();
                            obj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            obj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            obj.em_visa_type = dr["em_visa_type"].ToString();
                            obj.em_national_id = dr["em_national_id"].ToString();
                            obj.em_national_id_issue_date = dr["em_national_id_issue_date"].ToString();
                            obj.em_national_id_expiry_date = dr["em_national_id_expiry_date"].ToString();
                            obj.em_pan_no = dr["em_pan_no"].ToString();
                            obj.en_labour_card_no = dr["en_labour_card_no"].ToString();
                            obj.em_img = dr["em_img"].ToString();
                            obj.em_social_address = dr["em_social_address"].ToString();
                            obj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            obj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            obj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            obj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            obj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            obj.em_expected_date_of_join = dr["em_expected_date_of_join"].ToString();
                            obj.em_joining_ref = dr["em_joining_ref"].ToString();

                            obj.em_handicap_status = dr["em_handicap_status"].ToString();
                            obj.em_blood_group_code = dr["em_blood_group_code"].ToString();
                            obj.em_doc = dr["em_doc"].ToString();
                            obj.em_application_status = dr["em_application_status"].ToString();
                            obj.em_password = dr["em_password"].ToString();
                            obj.em_application_date = dr["em_application_date"].ToString();
                            obj.em_user_id = dr["em_user_id"].ToString();


                            obj.em_doc_path = dr["em_doc_path"].ToString();
                            obj.em_dest_code = dr["em_dest_code"].ToString();
                            obj.em_staff_type = dr["em_staff_type"].ToString();
                            obj.em_grade_code = dr["em_grade_code"].ToString();
                            obj.em_agreement = dr["em_agreement"].ToString();
                            obj.em_agreement_start_date = dr["em_agreement_start_date"].ToString();
                            obj.em_agreement_exp_date = dr["em_agreement_exp_date"].ToString();
                            obj.offer_letter_doc = dr["offer_letter_doc"].ToString();

                            obj.em_offer_letter_accepted_flag = dr["em_offer_letter_accepted_flag"].ToString();
                            obj.em_offer_letter_show_flag = dr["em_offer_letter_show_flag"].ToString();

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }


        }

        #endregion
    }
}