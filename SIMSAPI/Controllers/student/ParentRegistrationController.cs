﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/ParentRegistration")]
    public class ParentRegistrationController : ApiController
    {


        [Route("getAll_Parent_Details")]
        public HttpResponseMessage getSearchEmp(string sims_parent_number)
        {

            List<Sims010_Edit> code_list = new List<Sims010_Edit>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parent_registration_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@sims_parent_number",sims_parent_number),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            //Father Details
                            obj.sims_parent_number = dr["sims_parent_number"].ToString();
                            obj.sims_parent_login_code = dr["sims_parent_login_code"].ToString();
                            obj.sims_parent_father_salutation_code = dr["sims_parent_father_salutation_code"].ToString();
                            obj.sims_parent_father_salutation_code1 = dr["sims_parent_father_salutation_code1"].ToString();
                            obj.sims_parent_father_first_name = dr["sims_parent_father_first_name"].ToString();
                            obj.sims_parent_father_middle_name = dr["sims_parent_father_middle_name"].ToString();
                            obj.sims_parent_father_last_name = dr["sims_parent_father_last_name"].ToString();
                            obj.sims_parent_father_family_name = dr["sims_parent_father_family_name"].ToString();
                            obj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            obj.sims_parent_father_nationality1_code = dr["sims_parent_father_nationality1_code"].ToString();
                            obj.sims_parent_father_nationality1_code1 = dr["sims_parent_father_nationality1_code1"].ToString();
                            obj.sims_parent_father_nationality2_code = dr["sims_parent_father_nationality2_code"].ToString();
                            obj.sims_parent_father_nationality2_code1 = dr["sims_parent_father_nationality2_code1"].ToString();
                            obj.sims_parent_father_appartment_number = dr["sims_parent_father_appartment_number"].ToString();
                            obj.sims_parent_father_building_number = dr["sims_parent_father_building_number"].ToString();
                            obj.sims_parent_father_street_number = dr["sims_parent_father_street_number"].ToString();
                            obj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                            obj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            obj.sims_parent_father_city1 = dr["sims_parent_father_city1"].ToString();
                            obj.sims_parent_father_state1 = dr["sims_parent_father_state1"].ToString();
                            obj.sims_parent_father_country_code1 = dr["sims_parent_father_country_code1"].ToString();
                            obj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            obj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            obj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            obj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            obj.sims_parent_father_po_box = dr["sims_parent_father_po_box"].ToString();
                            obj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            obj.sims_parent_father_occupation_local_language = dr["sims_parent_father_occupation_local_language"].ToString();
                            obj.sims_parent_father_occupation_location_local_language = dr["sims_parent_father_occupation_location_local_language"].ToString();
                            obj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            obj.sims_parent_father_img = dr["sims_parent_father_img"].ToString();

                            //Mother Details
                            obj.sims_parent_mother_salutation_code = dr["sims_parent_mother_salutation_code"].ToString();
                            obj.sims_parent_mother_salutation_code1 = dr["sims_parent_mother_salutation_code1"].ToString(); 
                            obj.sims_parent_mother_first_name = dr["sims_parent_mother_first_name"].ToString();
                            obj.sims_parent_mother_middle_name = dr["sims_parent_mother_middle_name"].ToString();
                            obj.sims_parent_mother_last_name = dr["sims_parent_mother_last_name"].ToString();
                            obj.sims_parent_mother_family_name = dr["sims_parent_mother_family_name"].ToString();
                            obj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            obj.sims_parent_mother_nationality1_code = dr["sims_parent_mother_nationality1_code"].ToString();
                            obj.sims_parent_mother_nationality1_code1 = dr["sims_parent_mother_nationality1_code1"].ToString();
                            obj.sims_parent_mother_nationality2_code = dr["sims_parent_mother_nationality2_code"].ToString();
                            obj.sims_parent_mother_nationality2_code1 = dr["sims_parent_mother_nationality2_code1"].ToString();
                            obj.sims_parent_mother_appartment_number = dr["sims_parent_mother_appartment_number"].ToString();
                            obj.sims_parent_mother_building_number = dr["sims_parent_mother_building_number"].ToString();
                            obj.sims_parent_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                            obj.sims_parent_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                            obj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            obj.sims_parent_mother_city1 = dr["sims_parent_mother_city1"].ToString();
                            obj.sims_parent_mother_state1 = dr["sims_parent_mother_state1"].ToString();
                            obj.sims_parent_mother_country_code1 = dr["sims_parent_mother_country_code1"].ToString();
                            obj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();
                            obj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            obj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            obj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            obj.sims_parent_mother_po_box = dr["sims_parent_mother_po_box"].ToString();
                            obj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            obj.sims_parent_mother_occupation_local_language = dr["sims_parent_mother_occupation_local_language"].ToString();
                            obj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            obj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            obj.sims_parent_mother_img = dr["sims_parent_mother_img"].ToString();

                            //Gaurdidan Details
                            obj.sims_parent_guardian_salutation_code = dr["sims_parent_guardian_salutation_code"].ToString();
                            obj.sims_parent_guardian_salutation_code1 = dr["sims_parent_guardian_salutation_code1"].ToString();
                            obj.sims_parent_guardian_first_name = dr["sims_parent_guardian_first_name"].ToString();
                            obj.sims_parent_guardian_middle_name = dr["sims_parent_guardian_middle_name"].ToString();
                            obj.sims_parent_guardian_last_name = dr["sims_parent_guardian_last_name"].ToString();
                            obj.sims_parent_guardian_family_name = dr["sims_parent_guardian_family_name"].ToString();
                            obj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            obj.sims_parent_guardian_nationality1_code = dr["sims_parent_guardian_nationality1_code"].ToString();
                            obj.sims_parent_guardian_nationality1_code1 = dr["sims_parent_guardian_nationality1_code1"].ToString();
                            obj.sims_parent_guardian_nationality2_code = dr["sims_parent_guardian_nationality2_code"].ToString();
                            obj.sims_parent_guardian_nationality2_code1 = dr["sims_parent_guardian_nationality2_code1"].ToString();
                            obj.sims_parent_guardian_appartment_number = dr["sims_parent_guardian_appartment_number"].ToString();
                            obj.sims_parent_guardian_building_number = dr["sims_parent_guardian_building_number"].ToString();
                            obj.sims_parent_guardian_street_number = dr["sims_parent_guardian_street_number"].ToString();
                            obj.sims_parent_guardian_area_number = dr["sims_parent_guardian_area_number"].ToString();
                            obj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            obj.sims_parent_guardian_city1 = dr["sims_parent_guardian_city1"].ToString();
                            obj.sims_parent_guardian_state1 = dr["sims_parent_guardian_state1"].ToString();
                            obj.sims_parent_guardian_country_code1 = dr["sims_parent_guardian_country_code1"].ToString();
                            obj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            obj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            obj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            obj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            obj.sims_parent_guardian_po_box = dr["sims_parent_guardian_po_box"].ToString();
                            obj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            obj.sims_parent_guardian_occupation_local_language = dr["sims_parent_guardian_occupation_local_language"].ToString();
                            obj.sims_parent_guardian_occupation_location_local_language = dr["sims_parent_guardian_occupation_location_local_language"].ToString();
                            obj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            obj.sims_parent_guardian_relationship_code1 = dr["sims_parent_guardian_relationship_code1"].ToString();
                            obj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            obj.sims_parent_guardian_img = dr["sims_parent_guardian_img"].ToString();

                            obj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].ToString();
                            obj.sims_parent_is_employement_comp_code1 = dr["sims_parent_is_employement_comp_code1"].ToString();
                            obj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                            obj.sims_parent_father_passport_issue_date =  db.UIDDMMYYYYformat(dr["sims_parent_father_passport_issue_date"].ToString());
                            obj.sims_parent_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_passport_expiry_date"].ToString());
                            obj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            obj.sims_parent_father_national_id_issue_date =   db.UIDDMMYYYYformat(dr["sims_parent_father_national_id_issue_date"].ToString());
                            obj.sims_parent_father_national_id__expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_national_id__expiry_date"].ToString());
                            obj.sims_parent_mother_passport_issue_date =  db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_issue_date"].ToString());
                            obj.sims_parent_mother_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_expiry_date"].ToString());
                            obj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            obj.sims_parent_mother_national_id_issue_date =  db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_issue_date"].ToString());
                            obj.sims_parent_mother_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_expiry_date"].ToString());
                            obj.sims_parent_guardian_passport_issue_date =   db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_issue_date"].ToString());
                            obj.sims_parent_guardian_passport_expiry_date2 = db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_expiry_date2"].ToString());
                            obj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            obj.sims_parent_guardian_national_id_issue_date =  db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_issue_date"].ToString());
                            obj.sims_parent_guardian_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_expiry_date"].ToString());


                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);

            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
        }


        [Route("CUDParent")]
        public HttpResponseMessage CUDParent(List<Sims010_Edit> data)
        {
            string parentcode = string.Empty;
            Message message = new Message();

            List<Sims010_Edit> lst = new List<Sims010_Edit>();

            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parent_registration_proc]",
                                new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",simsobj.opr),
                            //new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            #region
                            new SqlParameter("@USER_CODE", simsobj.parent_id),
                            new SqlParameter("@sims_admission_father_salutation_code", simsobj.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", simsobj.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", simsobj.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", simsobj.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", simsobj.father_family_name),
                            new SqlParameter("@sims_admission_father_name_ot", simsobj.father_family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", simsobj.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", simsobj.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", simsobj.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", simsobj.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", simsobj.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", simsobj.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", simsobj.father_city),
                            new SqlParameter("@sims_admission_father_state_name", simsobj.father_state),
                            new SqlParameter("@sims_admission_father_country_code", simsobj.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", simsobj.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", simsobj.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", simsobj.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", simsobj.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", simsobj.father_fax),
                            new SqlParameter("@sims_admission_father_email", simsobj.father_email),
                            new SqlParameter("@sims_admission_father_occupation", simsobj.father_occupation),
                            new SqlParameter("@sims_admission_father_company", simsobj.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", simsobj.father_passport_number),


                            new SqlParameter("@sims_admission_guardian_salutation_code", simsobj.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", simsobj.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", simsobj.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", simsobj.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", simsobj.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", simsobj.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", simsobj.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", simsobj.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", simsobj.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", simsobj.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", simsobj.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", simsobj.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", simsobj.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", simsobj.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", simsobj.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", simsobj.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", simsobj.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", simsobj.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", simsobj.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", simsobj.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", simsobj.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", simsobj.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", simsobj.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", simsobj.passport_num),


                            new SqlParameter("@sims_admission_mother_salutation_code", simsobj.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", simsobj.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", simsobj.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", simsobj.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", simsobj.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", simsobj.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", simsobj.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", simsobj.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", simsobj.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", simsobj.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", simsobj.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", simsobj.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", simsobj.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", simsobj.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", simsobj.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", simsobj.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", simsobj.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", simsobj.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", simsobj.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", simsobj.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", simsobj.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", simsobj.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", simsobj.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", simsobj.mother_passport_number),
                            new SqlParameter("@sims_parent_father_img", simsobj.sims_parent_father_img),
                            new SqlParameter("@sims_parent_mother_img",simsobj.sims_parent_mother_img),
                            new SqlParameter("@sims_parent_guardian_img", simsobj.sims_parent_guardian_img),

                            new SqlParameter("@sims_parent_father_passport_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_issue_date)),
                            new SqlParameter("@sims_parent_father_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_expiry_date)),
                            new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                            new SqlParameter("@sims_parent_father_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_issue_date)),
                            new SqlParameter("@sims_parent_father_national_id_expiry_date",db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_expiry_date)),

                            new SqlParameter("@sims_parent_mother_passport_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_issue_date)),
                            new SqlParameter("@sims_parent_mother_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_expiry_date)),
                            new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                            new SqlParameter("@sims_parent_mother_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_issue_date)),
                            new SqlParameter("@sims_parent_mother_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_expiry_date)),

                            new SqlParameter("@sims_parent_gua_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_gua_passport_issue_date)),
                            new SqlParameter("@sims_parent_gua_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.sims_parent_gua_passport_expiry_date)),
                            new SqlParameter("@sims_parent_gua_national_id", simsobj.sims_parent_gua_national_id),
                            new SqlParameter("@sims_parent_gua_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_parent_gua_national_id_issue_date)),
                            new SqlParameter("@sims_parent_gua_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_gua_national_id_expiry_date)),





                          
                                 #endregion
                         });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Sims010_Edit obj = new Sims010_Edit();
                                    obj.sims_parent_father_img = dr["sims_parent_father_img"].ToString();
                                    obj.user_code = dr["user_code"].ToString();
                                    obj.sims_parent_mother_img = dr["sims_parent_mother_img"].ToString();
                                    obj.sims_parent_guardian_img = dr["sims_parent_guardian_img"].ToString();
                                    lst.Add(obj);
                                }


                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }



        [Route("getAdmissionList")]
        public HttpResponseMessage getAdmissionList()
        {

            List<Sims010_Edit> listall = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[GetAdmissionData_Temp_proc]",
                        new List<SqlParameter>()
                        {
                            // new SqlParameter("@opr", 'S'),[sims].[GetAdmissionData_Temp_proc]

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit getlist = new Sims010_Edit();
                            {
                                getlist.school_code = dr["lic_school_code"].ToString();
                                getlist.school_name = dr["lic_school_name"].ToString();
                                //getlist.sibling_school_code = getlist.school_code;
                                //getlist.sibling_school_name = getlist.school_name;
                                //getlist.employee_school_code = getlist.school_code;
                                //getlist.employee_school_name = getlist.school_name;
                            }
                            {
                                getlist.curr_code = dr["sims_cur_code"].ToString();
                                getlist.curr_name = dr["sims_cur_full_name_en"].ToString();
                            }
                            {
                                getlist.birth_country_code = dr["sims_country_code"].ToString();
                                getlist.birth_country = dr["sims_country_name_en"].ToString();
                                getlist.father_country_code = getlist.birth_country_code;
                                getlist.father_country = getlist.birth_country;
                                getlist.mother_country_code = getlist.birth_country_code;
                                getlist.mother_country = getlist.birth_country;
                                getlist.guardian_country_code = getlist.birth_country_code;
                                getlist.guardian_country = getlist.birth_country;
                                getlist.current_school_country_code = getlist.birth_country_code;
                                getlist.current_school_country = getlist.birth_country;
                            }
                            {

                                getlist.nationality_code = dr["sims_nationality_code"].ToString();
                                getlist.nationality = dr["sims_nationality_name_en"].ToString();
                                getlist.father_nationality1_code = getlist.nationality_code;
                                getlist.father_nationality1_desc = getlist.nationality;
                                getlist.father_nationality2_code = getlist.nationality_code;
                                getlist.father_nationality2_desc = getlist.nationality;

                                getlist.mother_nationality1_code = getlist.nationality_code;
                                getlist.mother_nationality1_desc = getlist.nationality;
                                getlist.mother_nationality2_code = getlist.nationality_code;
                                getlist.mother_nationality2_desc = getlist.nationality;

                                getlist.guardian_nationality1_code = getlist.nationality_code;
                                getlist.guardian_nationality1_desc = getlist.nationality;
                                getlist.guardian_nationality2_code = getlist.nationality_code;
                                getlist.guardian_nationality2_desc = getlist.nationality;
                            }

                            {
                                getlist.main_language_code = dr["sims_language_code"].ToString();
                                getlist.main_language_desc = dr["sims_language_name_en"].ToString();
                                getlist.other_language_code = getlist.main_language_code;
                                getlist.other_language = getlist.main_language_desc;
                            }
                            //if (string.IsNullOrEmpty(dr[12].ToString()) == false)
                            {
                                getlist.ethinicity_code = dr["sims_ethnicity_code"].ToString();
                                getlist.ethinicity = dr["sims_ethnicity_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[13].ToString()) == false)
                            {
                                getlist.religion_code = dr["sims_religion_code"].ToString();
                                getlist.religion_desc = dr["sims_religion_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[15].ToString()) == false)
                            {
                                getlist.gender_code = dr["sims_appl_parameter_gender"].ToString();
                                getlist.gender_desc = dr["sims_appl_form_field_value1_gender"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[17].ToString()) == false)
                            {
                                getlist.legal_custody_code = dr["sims_appl_parameter_legal"].ToString();
                                getlist.legal_custody_name = dr["sims_appl_form_field_value1_legal"].ToString();
                                getlist.fee_payment_contact_pref_code = getlist.legal_custody_code;
                                getlist.fee_payment_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_code = getlist.legal_custody_code;
                            }
                            //if (string.IsNullOrEmpty(dr[19].ToString()) == false)
                            {
                                getlist.blood_group_code = dr["sims_appl_parameter_blood"].ToString();
                                getlist.blood_group_desc = dr["sims_appl_form_field_value1_blood"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[21].ToString()) == false) 
                            //Salutation Code
                            {
                                getlist.mother_salutation_code = dr["sims_appl_parameter_salut"].ToString();
                                getlist.mother_salutation_desc = dr["sims_appl_form_field_value1_salut"].ToString();
                                getlist.father_salutation_code = getlist.mother_salutation_code;
                                getlist.father_salutation_desc = getlist.mother_salutation_desc;
                                getlist.guardian_salutation_code = getlist.mother_salutation_code;
                                getlist.guardian_salutation_desc = getlist.mother_salutation_desc;
                            }
                            {
                                getlist.academic_year = dr["sims_acadmic_year"].ToString();
                                getlist.academic_year_desc = dr["sims_acadmic_year_desc"].ToString();
                                getlist.Aycur = dr["AyCur"].ToString();
                            }
                            {
                                getlist.grade_code = dr["sims_grade_code"].ToString();
                                getlist.grade_name = dr["sims_grade_name"].ToString();
                                getlist.GrCur = dr["GrCur"].ToString();
                                getlist.GrAca = dr["GrAca"].ToString();
                            }
                            {
                                getlist.section_code = dr["sims_section_code"].ToString();
                                getlist.section_name = dr["sims_section_name"].ToString();
                                getlist.SecAcy = dr["secAca"].ToString();
                                getlist.SecCur = dr["SecCur"].ToString();
                                getlist.SecGr = dr["SecGr"].ToString();
                                getlist.secgender = dr["SecGender"].ToString();
                            }
                            {
                                getlist.term_code = dr["sims_term_code"].ToString();
                                getlist.term_name = dr["sims_term_desc"].ToString();
                                getlist.termCur = dr["termCur"].ToString();
                                getlist.termAy = dr["TermAy"].ToString();
                            }
                            {
                                getlist.fee_category_code = dr["sims_fee_category_code"].ToString();
                                getlist.fee_category_desc = dr["sims_fee_category_desc"].ToString();
                            }
                            {
                                getlist.main_language_r_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_r = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_w_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_w = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_s_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_s = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                            }
                            {
                                getlist.visa_type_code = dr["sims_appl_parameter_Visa_code"].ToString();
                                getlist.visa_type_desc = dr["sims_appl_form_field_value1_Visa_type"].ToString();
                                getlist.quota_code = dr["quota_code"].ToString();
                                getlist.sims_admission_quota_code = dr["sims_quota_desc"].ToString();
                            }

                            listall.Add(getlist);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, listall);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }


        [Route("getParentID")]
        public HttpResponseMessage getParentID()
        {
           
            List<Sims010_Edit> prno_list = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parent_registration_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@Opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.user_code = dr["USER_CODE"].ToString();
                            prno_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {                
                return Request.CreateResponse(HttpStatusCode.OK, prno_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, prno_list);
        }

    }
}