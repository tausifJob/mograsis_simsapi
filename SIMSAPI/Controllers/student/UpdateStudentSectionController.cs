﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/student/UpdateStudentSection")]
    [BasicAuthentication]
    public class UpdateStudentSectionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            List<Sims044> lstCuriculum = new List<Sims044>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur_level_grade",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr","P"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur_level_grade",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","Q"),
                            new SqlParameter("@sims_cur_name",curCode)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSectionDetails")]
        public HttpResponseMessage getSectionDetails(string curName, string Acdmyr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","S"),
                          new SqlParameter("@opr_mem","S"),
                           new SqlParameter("@sims_academic_year",Acdmyr),
                            new SqlParameter("@sims_cur_name",curName)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.count = dr["count"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();

                            try
                            {
                                sequence.sims_section_stregth = dr["sims_section_stregth"].ToString();
                            }
                            catch (Exception ex)
                            {

                                throw;
                            }
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStudentDetailsSectionWise")]
        public HttpResponseMessage getStudentDetailsSectionWise(string jsonobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","S"),
                          new SqlParameter("@opr_mem","L"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                             new SqlParameter("@sims_section_name",obj.sims_section_code),
                            new SqlParameter("@sims_grade_name",obj.sims_grade_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.Student_name = dr["Student_name"].ToString();

                            try
                            {
                                sequence.sims_allocation_status = dr["sims_allocation_status"].ToString();
                            }
                            catch (Exception x) { }
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getSectionSubject")]
        public HttpResponseMessage getSectionSubject(string jsonobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","G"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                             new SqlParameter("@sims_section_name",obj.sims_section_code),
                            new SqlParameter("@sims_grade_name",obj.sims_grade_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sequence.sims_subject_type_code = dr["sims_subject_type_code"].ToString();
                            sequence.sims_subject_group_code = dr["sims_subject_group_code"].ToString();
                            sequence.sims_subject_code = dr["sims_subject_code"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getSectionFee")]
        public HttpResponseMessage getSectionFee(string jsonobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","H"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                             new SqlParameter("@sims_section_name",obj.sims_section_code),
                            new SqlParameter("@sims_grade_name",obj.sims_grade_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims044 sequence = new Sims044();
                            sequence.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            sequence.sims_fee_code=dr["sims_fee_code"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("StudentSectionUpdate")]
        public HttpResponseMessage StudentSectionUpdate(List<Sims044> obj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            //Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            // List<Sims044> lstModules = new List<Sims044>();
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims044 obj in obj1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                            new List<SqlParameter>()  
                         { 
                           new SqlParameter("@opr","U"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                           new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                           new SqlParameter("@sims_section_name",obj.sims_section_code_new),
                           new SqlParameter("@sims_grade_name",obj.sims_grade_code_new),
                           new SqlParameter("@sims_enroll_number",obj.sims_student_enroll_number),
                           new SqlParameter("@effect_from_date",db.DBYYYYMMDDformat(obj.effect_from_date)),
                             new SqlParameter("@fee_code_list","")
                         }
                             );
                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("StudentSectionUpdateNew")]
        public HttpResponseMessage StudentSectionUpdateNew(List<Sims044> obj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            //Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            // List<Sims044> lstModules = new List<Sims044>();
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims044 obj in obj1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_section_New_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr","U"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                           new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                           new SqlParameter("@sims_section_name",obj.sims_section_code_new),
                           new SqlParameter("@sims_grade_name",obj.sims_grade_code_new),
                           new SqlParameter("@sims_enroll_number",obj.sims_student_enroll_number),
                           new SqlParameter("@effect_from_date",db.DBYYYYMMDDformat(obj.effect_from_date)),
                             new SqlParameter("@fee_code_list","")
                         }
                             );
                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("classwiseStudentSectionUpdate")]
        public HttpResponseMessage classwiseStudentSectionUpdate(Sims044 obj)
        {
            List<classwisesectionstudent> lstModules = new List<classwisesectionstudent>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                           new SqlParameter("@opr","P"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                           new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                           new SqlParameter("@sims_grade_name",obj.sims_grade_code),
                           new SqlParameter("@sims_section_name",obj.sims_section_code)
                         
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            classwisesectionstudent obj1 = new classwisesectionstudent();
                            string str = dr["sims_enroll_number"].ToString();
                            obj1.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj1.sims_student_Name = dr["student_name"].ToString();
                            obj1.section_list = new List<classwisestudentsection>();

                            classwisestudentsection obj2 = new classwisestudentsection();

                            obj2.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj2.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj2.sims_grade_code_new = dr["sims_grade_code"].ToString();
                            obj2.sims_section_code_new = dr["sims_section_code"].ToString();
                            obj2.sims_student_enroll_number = dr["sims_enroll_number"].ToString();
                            obj2.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj2.section_status = dr["status"].ToString().Equals("1") ? true : false;

                            var v = (from p in lstModules where p.sims_enroll_number == str select p);
                            if (v.Count() == 0)
                            {
                                obj1.section_list.Add(obj2);
                                lstModules.Add(obj1);
                            }
                            else
                            {
                                v.ElementAt(0).section_list.Add(obj2);
                            }


                        }

                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
      
        [Route("getSectionFeecheck")]
        public HttpResponseMessage getSectionFeecheck(string jsonobj)
        {
            bool insert = false;
            Sims044 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims044>(jsonobj);

            List<Sims044> lstModules = new List<Sims044>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_proc",
                        new List<SqlParameter>()  
                         { 
                          new SqlParameter("@opr","A"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                             new SqlParameter("@sims_section_name",obj.sims_section_code),
                            new SqlParameter("@sims_grade_name",obj.sims_grade_code),
                           new SqlParameter("@sims_enroll_number",obj.sims_student_enroll_number)

                         }
                         );
                    if (dr.HasRows)
                    {
                        insert = false;
                    }
                    else
                    {
                        insert = true;
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
        

    }
}