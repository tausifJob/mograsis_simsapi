﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/Report/SubjectTeacherList")]
    public class SubjectTeacherListRptController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSubjctTeacherList_Rpt")]
        public HttpResponseMessage getSubjctTeacherList_Rpt(string cur_code, string academic_year, string grade_code, string section_code, string lecture_count)
        {
            List<SubjectTeacherListRpt> lstCuriculum = new List<SubjectTeacherListRpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[SubjectTeacherList_Rpt]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                            // new SqlParameter("@user_number", lecture_count),
                             new SqlParameter("@value", String.IsNullOrEmpty(lecture_count)?null:lecture_count)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubjectTeacherListRpt sequence = new SubjectTeacherListRpt();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            sequence.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            sequence.sims_bell_lecture_per_week = int.Parse(dr["sims_bell_lecture_per_week"].ToString());
                            sequence.Student_cnt = int.Parse(dr["Student_cnt"].ToString());
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        //attendance Rule API

        [Route("getAttendanceRule")]
        public HttpResponseMessage getAttendanceRule()
        {
            List<AR_R20> lstCuriculum = new List<AR_R20>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[AttendanceRuleReport_Rpt]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "T"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AR_R20 sequence = new AR_R20();
                            sequence.sims_attendance_rule_code = dr["sims_attendance_rule_code"].ToString();

                            sequence.sims_attendance_rule_name = dr["sims_attendance_rule_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }



        [Route("getAttendanceRule_Rpt")]
        public HttpResponseMessage getAttendanceRule_Rpt(string cur_code, string academic_year, string grade_code, string section_code, string rule_Code)
        {
            List<AR_R20> lstCuriculum = new List<AR_R20>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[AttendanceRuleReport_Rpt]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                            new SqlParameter("@rule_Code",rule_Code )
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AR_R20 sequence = new AR_R20();
                            sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class_name = dr["class_name"].ToString();
                            sequence.start_occ_date = dr["start_occ_date"].ToString();
                            sequence.end_occ_date = dr["end_occ_date"].ToString();
                            sequence.no_of_occ = int.Parse(dr["no_of_occ"].ToString());
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }
    }
}