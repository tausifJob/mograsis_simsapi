﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.student
{
   
    [RoutePrefix("api/StudentDevice")]
    public class StudentDeviceController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Seelct
        [Route("getStudentDevice")]
        public HttpResponseMessage getStudentDevice()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentDevice(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getStudentDevice"));

            List<Sim201> goaltarget_list = new List<Sim201>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_device_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim201 simsobj = new Sim201();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_device_type = dr["sims_device_type"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.TypeName = dr["TypeName"].ToString();
                            simsobj.sims_device_value = dr["sims_device_value"].ToString();
                           // simsobj.sims_device_status = dr["sims_device_status"].ToString();
                            simsobj.sims_device_status = dr["sims_device_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //GetLedgerNumber
        [Route("GetDeviceType")]
        public HttpResponseMessage GetDeviceType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDeviceType()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetLedgerNumber"));

            List<Sim201> doc_list = new List<Sim201>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_device_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim201 simsobj = new Sim201();
                            simsobj.sims_device_type = dr["sims_device_type"].ToString();
                            simsobj.sims_device_value = dr["sims_device_value"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }



        [Route("CUDStudentDevice")]
        public HttpResponseMessage CUDStudentDevice(List<Sim201> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim201 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_device_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_enroll_number",simsobj.enroll_number),
                                new SqlParameter("@sims_device_type", simsobj.sims_device_type),
                                new SqlParameter("@sims_device_value", simsobj.sims_device_value),
                                new SqlParameter("@sims_device_status", simsobj.sims_device_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



    }
}