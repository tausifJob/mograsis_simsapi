﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using Telerik.Reporting.Processing;
using System.Web;
using System.IO;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.FEE;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
using System.Collections;
using System.Text.RegularExpressions;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudSubWiseAttendNotifiController")]
    public class StudSubWiseAttendNotifiController : ApiController
    {

        [Route("getSubjectAbsentStudentList")]
        public HttpResponseMessage getSubjectAbsentStudentList(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string select_date, string att_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims561> mod_list = new List<Sims561>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "")
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "")
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "")
                        sec_code = null;
                    if (select_date == "undefined" || select_date == "")
                        select_date = null;
                    if (att_code == "undefined" || att_code == "")
                        att_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_subject_absent_notification_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_enroll_number", student_en),
                            new SqlParameter("@today_date", db.DBYYYYMMDDformat(select_date)),
                            //new SqlParameter("@grade_List", grd_code),
                            //new SqlParameter("@section_List", sec_code),
                            new SqlParameter("@sims_attendance_code", att_code),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_full_name = dr["sims_student_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                            simsobj.sims_email_id = dr["EmailID"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_contact_number = dr["sims_contact_number"].ToString();
                            simsobj.sims_contact_person = dr["sims_student_primary_contact_pref"].ToString();
                            simsobj.sims_attendance_day_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();
                            simsobj.doc_path_available_flag = dr["comn_alert_message"].ToString();
                            try
                            {
                                simsobj.sims_absent_notification_flag_sms = dr["sims_sms_status"].ToString();
                                simsobj.sims_absent_notification_flag_alert = dr["comn_alert_read_status"].ToString();
                                //simsobj.sims_absent_notification_flag_unread_alert = dr["comn_alert_unread_status"].ToString();
                                simsobj.sims_absent_notification_flag_email = dr["sims_email_status"].ToString();
                                simsobj.sims_absent_notification_sms = dr["Sms_Count"].ToString();
                                simsobj.sims_absent_notification_alert = dr["Alert_Count"].ToString();
                                //simsobj.sims_absent_notification_unread_alert = dr["Alert_Unread_Count"].ToString();
                                simsobj.sims_absent_notification_email = dr["Email_Count"].ToString();
                                simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            }
                            catch (Exception e)
                            {
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDAbsentStudentList")]
        public HttpResponseMessage CUDAbsentStudentList(string data1, List<Sims561> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            Sims561 smobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims561>(data1);

            #region Parent Email Id update...

            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_student_subject_absent_notification_proc]",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","N"),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                            new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr1.Read())
                        {

                        }
                        db1.Dispose();
                        dr1.Close();
                    }
                }
            }
            catch (Exception f)
            {

            }

            #endregion

            #region Save Sending data...

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_subject_absent_notification_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_name", simsobj.sims_section_name),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_student_name", simsobj.student_full_name),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                            new SqlParameter("@sims_contact_number", simsobj.sims_contact_number),
                            new SqlParameter("@today_date",  db.DBYYYYMMDDformat(simsobj.sims_from_date)),
                            new SqlParameter("@comn_alert_message", simsobj.doc_path_available_flag),
                            new SqlParameter("@sims_contact_person", simsobj.sims_contact_person),
                            new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                    #region send absent student list to principle
                    try
                    {
                        SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[sims_student_subject_absent_notification_proc]",
                        new List<SqlParameter>()
                        {
                                  new SqlParameter("@opr",'E'),
                                  new SqlParameter("@stud_name", smobj.stud_name),
                                  new SqlParameter("@attendance_type", smobj.attendance_type),
                                  new SqlParameter("@today_date",  db.DBYYYYMMDDformat(smobj.sims_from_date))
                        });
                        if (dr2.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                        dr2.Close();
                    }
                    catch (Exception ex)
                    { }
                    #endregion
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            #endregion

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

    }
}

 