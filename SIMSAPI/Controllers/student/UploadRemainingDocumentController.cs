﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

using SIMSAPI.Models.ParentClass;
using System.Web;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.COMMON;
using System.IO;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/student/UploadRemainingDocument")]
    public class UploadRemainingDocumentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getStudentsDetails")]
        public HttpResponseMessage getStudentsDetails(string curcode, string academicyear, string gradecode, string section, string studenroll)
        {

            if (gradecode == "undefined")
            { gradecode = null; }
            if (section == "undefined")
            { section = null; }
            if (studenroll == "undefined")
            { studenroll = null; }

            List<Sims526> Studetail = new List<Sims526>();
            string location = "", root = "";
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";
            root = HttpContext.Current.Server.MapPath(path);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Z"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@sims_academic_year", academicyear),
                             new SqlParameter("@sims_grade_code", gradecode),
                             new SqlParameter("@sims_section_code", section),
                             new SqlParameter("@sims_student_enroll_number",studenroll)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims526 sequence = new Sims526();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            //sequence.sims_admission_doc_admission_number = dr["sims_admission_doc_admission_number"].ToString();
                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            sequence.sims_student_passport_full_name_en = dr["sims_student_passport_first_name_en"].ToString() + ' ' + dr["sims_student_passport_last_name_en"];
                            sequence.sims_student_image = dr["sims_student_img"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name = dr["sims_grade_name"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name = dr["sims_section_name"].ToString();
                            Studetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Studetail);
        }

        [Route("getGrade")]
        public HttpResponseMessage getGrade(string curCode, string academicYear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGrade()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            List<Sims526> lstgrades = new List<Sims526>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_grade_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@tbl_name", "sims.sims_grade"),
                            new SqlParameter("@tbl_col_name1", "*"),
                            new SqlParameter("@tbl_cond", "sims_cur_code='"+curCode+"' AND sims_academic_year='"+academicYear+"'")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims526 cur = new Sims526();
                            cur.grade_code = dr["sims_grade_code"].ToString();
                            cur.grade_name_en = dr["sims_grade_name_en"].ToString();
                            lstgrades.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        }

        [Route("getStudentDetailsSectionWise")]
        public HttpResponseMessage getStudentDetailsSectionWise(string jsonobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Section"));

            Sims526 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims526>(jsonobj);

            List<Sims526> lstModules = new List<Sims526>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_section",
                        new List<SqlParameter>()
                         {
                          new SqlParameter("@opr","S"),
                          new SqlParameter("@opr_mem","L"),
                           new SqlParameter("@sims_academic_year",obj.sims_academic_year),
                            new SqlParameter("@sims_cur_name",obj.sims_cur_code),
                             new SqlParameter("@sims_section_name",obj.sims_section_code),
                            new SqlParameter("@sims_grade_name",obj.sims_grade_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims526 sequence = new Sims526();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.Student_name = dr["Student_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("UpdateStudentDocumentCUD")]
        public HttpResponseMessage UpdateStudentDocumentCUD(Sims526 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_student_enroll_number", simsobj.sims_student_enroll_number),
                                 new SqlParameter("@sims_student_img", simsobj.sims_student_image),

                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetCriteriaName")]
        public HttpResponseMessage GetCriteriaName(string student_roll_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCriteriaName(),PARAMETERS :: admission_num{2}";
            Log.Debug(string.Format(debug, "Student", "GetCriteriaName", student_roll_number));

            List<Uccw229> type_list = new List<Uccw229>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'M'),
                            new SqlParameter("@sims_student_enroll_number", student_roll_number)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw229 simsobj = new Uccw229();
                            simsobj.admis_num = student_roll_number;
                            simsobj.sims_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            simsobj.sims_criteria_type = dr["sims_criteria_type"].ToString();
                            simsobj.count = int.Parse(dr["count"].ToString());

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                               new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'W'),
                                    new SqlParameter("@sims_student_enroll_number", student_roll_number),
                                    new SqlParameter("@sims_admission_criteria_code", simsobj.sims_criteria_code)
                                });
                                if (dr1.HasRows)
                                {
                                    string sims_admission_doc_path = "";
                                    bool status = false, verify = false;
                                    while (dr1.Read())
                                    {
                                        Uccw229 simsobj1 = new Uccw229();
                                        simsobj1.sims_admission_criteria_code = simsobj.sims_criteria_code + "";
                                        simsobj1.sims_admission_doc_path = dr1["sims_admission_doc_path"].ToString();
                                        sims_admission_doc_path = sims_admission_doc_path + ',' + simsobj1.sims_admission_doc_path;

                                        simsobj1.if_uploaded = !string.IsNullOrEmpty(simsobj1.sims_admission_doc_path);

                                        simsobj1.sims_admission_doc_status = dr1["sims_admission_doc_status"].Equals("1") ? true : false;
                                        simsobj1.sims_admission_doc_verify = dr1["sims_admission_doc_verify"].Equals("1") ? true : false;

                                        //if (dr1["sims_admission_doc_status"].ToString() == "0")
                                        //    simsobj1.sims_admission_doc_status = false;
                                        //else if (dr1["sims_admission_doc_status"].ToString() == "1")
                                        //    simsobj1.sims_admission_doc_status = true;
                                        //if (dr1["sims_admission_doc_verify"].ToString() == "0")
                                        //    simsobj1.sims_admission_doc_verify = false;
                                        //else if (dr1["sims_admission_doc_verify"].ToString() == "1")
                                        //    simsobj1.sims_admission_doc_verify = true;

                                        simsobj1.if_status = simsobj1.if_uploaded;//for isenable
                                        simsobj1.if_verify = (simsobj1.if_status && simsobj1.if_uploaded);
                                        status = simsobj1.sims_admission_doc_status;
                                        verify = simsobj1.sims_admission_doc_verify;

                                    }
                                    simsobj.sims_admission_doc_path = sims_admission_doc_path.Substring(1);
                                    simsobj.sims_admission_doc_status = status;
                                    simsobj.sims_admission_doc_verify = verify;
                                }
                            }
                            //for isenable
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUD_Update_Admission_DocList")]
        public HttpResponseMessage CUD_Update_Admission_DocList(List<Uccw229> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            Array str;
            string str2;
            try
            {
                if (lstsimsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw229 simsobj in lstsimsobj)
                        {
                            if (!(string.IsNullOrEmpty(simsobj.sims_admission_doc_path)))
                            {
                                str2 = simsobj.sims_admission_doc_path;
                                str = str2.Split(',');

                                foreach (string k in str)
                                {
                                    string sims_admission_doc_path = k;
                                    //}
                                    int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                                        new List<SqlParameter>()
                                    {
                                        new SqlParameter("@opr","MMM"),
                                        new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                                        new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                                        // new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                                        // new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                                        //new SqlParameter("@sims_admission_doc_path_old", sims_admission_doc_path),
                                        new SqlParameter("@sims_admission_doc_path", simsobj.sims_admission_doc_path),
                                        new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                                        new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                                    });
                                    if (ins > 0)
                                    {
                                        updated = true;
                                    }
                                    else
                                    {
                                        updated = false;
                                    }
                                }
                            }
                            //else
                            //{
                            //    int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                            //        new List<SqlParameter>()
                            //    {
                            //            new SqlParameter("@opr","PPP"),
                            //            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            //            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                            //            // new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                            //            // new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                            //            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            //            new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                            //    });
                            //    if (ins > 0)
                            //    {
                            //        updated = true;
                            //    }
                            //    else
                            //    {
                            //        updated = false;
                            //    }
                            //}
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("CUD_Insert_Admission_Doc")]
        public HttpResponseMessage CUD_Insert_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Insert_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Insert_Admission_Doc"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("insert_admission_doc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                            new SqlParameter("@sims_admission_doc_path", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify", simsobj.sims_admission_doc_verify== true?"1":"0")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUD_Update_Admission_Doc")]
        public HttpResponseMessage CUD_Update_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", simsobj.opr),

                            new SqlParameter("@sims_admission_doc_admission_number",simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code",simsobj.sims_criteria_code),
                            new SqlParameter("@sims_admission_doc_path_new",simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0"),
                            new SqlParameter("@sims_admission_enroll_number",simsobj.sims_admission_enroll_number),
                            new SqlParameter("@sims_admission_number       ",simsobj.admis_num),
                            new SqlParameter("@sims_admission_cur_code     ",simsobj.sims_admission_cur_code),
                            new SqlParameter("@sims_admission_academic_year",simsobj.sims_admission_academic_year),
                            new SqlParameter("@sims_admission_grade_code   ",simsobj.sims_admission_grade_code),
                            new SqlParameter("@sims_admission_section_code ",simsobj.sims_admission_section_code),
                            new SqlParameter("@sims_admission_criteria_code",simsobj.sims_admission_criteria_code),
                            //new SqlParameter("@sims_admission_doc_path_new ",simsobj.sims_admission_doc_path_new),
                            //new SqlParameter("@sims_admission_marks",       simsobj. sims_admission_marks),
                            //new SqlParameter("@sims_admission_rating",      simsobj. sims_admission_rating),
                            //new SqlParameter("@sims_admission_user_code",   simsobj. sims_admission_user_code),
                           

                         });
                        if (ins > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [HttpPost(), Route("uploadDocument")]
        public string uploadDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + location + "/";
            //string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);

            //Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));


                        type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + type;
                        hpf.SaveAs((root + filename + "." + type));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;


                        //file = s;
                        //hpf.SaveAs((root + s));
                        ////fname = Path.GetFileName(hpf.FileName);
                        //iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [Route("CUD_Delete_Admission_Doc")]
        public HttpResponseMessage CUD_Delete_Admission_Doc(string adm_no, string criteria_code, string doc_path)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Delete_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Delete_Admission_Doc"));
            // bool deleted = false;
            Message message = new Message();
            try
            {
                // if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","E"),
                            new SqlParameter("@sims_admission_number", adm_no),
                            new SqlParameter("@sims_admission_criteria_code",criteria_code),
                            new SqlParameter("@sims_admission_doc_path", doc_path),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Document Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

    }


}