﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using Telerik.Reporting.Processing;
using System.Web;
using System.IO;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.FEE;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
using System.Collections;
using System.Text.RegularExpressions;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudentReport")]
    public class StudentReportController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region  API For Student Report...

        [Route("getStudentReport")]
        public HttpResponseMessage getStudentReport(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_active, string sims_inactive)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_active == "undefined" || sims_active == "" || sims_active == null)
                        sims_active = null;
                    if (sims_inactive == "undefined" || sims_inactive == "" || sims_inactive == null)
                        sims_inactive = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() { 

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_active",sims_active),
                            new SqlParameter("@sims_inactive",sims_inactive),
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_blood_group = dr["sims_blood_group"].ToString();
                            simsobj.cur_shrt_name = dr["cur_shrt_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            simsobj.sims_student_main_language_name = dr["sims_student_main_language_name"].ToString();
                            simsobj.sims_health_detail = dr["health_detail"].ToString();

                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_bus_name = dr["sims_transport_bus_name"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                           
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            try
                            {
                                simsobj.sims_student_enroll_number1 = int.Parse(new String(simsobj.sims_student_enroll_number.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            try {
                                simsobj.stu_roll = dr["sims_roll_number"].ToString();

                                simsobj.stu_roll1 = int.Parse(new String(simsobj.stu_roll.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.student_name = simsobj.sims_student_passport_first_name_en + " " + simsobj.sims_student_passport_middle_name_en + " " + simsobj.sims_student_passport_last_name_en;
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_en_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_en_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.student_name_ = dr["student_name_"].ToString();
                            simsobj.sims_student_gender = dr["gender"].ToString();
                            simsobj.sims_student_date = dr["sims_student_date"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_name"].ToString();
                            simsobj.sims_student_admission_academic_year = dr["sims_student_admission_academic_year"].ToString();
                            simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                            simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                            simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                            simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                            simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                            simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                            simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simsobj.sims_student_national_id_issue_date = dr["sims_student_national_id_issue_date"].ToString();
                            simsobj.sims_student_national_id_expiry_date = dr["sims_student_national_id_expiry_date"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                            simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            simsobj.sims_student_ea_transfer_status = dr["sims_student_ea_transfer_status"].ToString();
                            simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                            simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                            simsobj.sims_student_age = dr["sims_student_age"].ToString();
                            simsobj.sims_sibling_student_number = dr["sims_sibling_student_number"].ToString();
                            simsobj.sims_student_passport_issue_date = dr["sims_student_passport_issue_date"].ToString();
                            simsobj.sims_student_passport_expiry_date = dr["sims_student_passport_expiry_date"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();

                            simsobj.Student_Admission_Status = dr["Student_Admission_Status"].ToString();
                            simsobj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();
                           
                            try
                            {

                                simsobj.comn_user_app_status = dr["comn_user_app_status"].ToString();
                                simsobj.comn_user_device_status = dr["comn_user_device_status"].ToString();
                                simsobj.comn_user_last_login = dr["comn_user_last_login"].ToString();
                                simsobj.student_birth_country = dr["student_birth_country"].ToString();
                                simsobj.sims_student_admitted_in_class = dr["sims_student_admitted_in_class"].ToString();
                                simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                                simsobj.sims_student_dob1 = dr["sims_student_dob1"].ToString();

                               
                            }
                            catch (Exception e)
                            {
                            }

                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.father_nationality1 = dr["father_nationality1"].ToString();
                            simsobj.father_nationality2 = dr["father_nationality2"].ToString();
                            simsobj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            simsobj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            simsobj.sims_parent_father_company = dr["sims_parent_father_company"].ToString();
                            simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();

                            simsobj.sims_parent_father_street_number = dr["sims_parent_father_street_number"].ToString();
                            simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                            simsobj.sims_parent_father_state = dr["sims_parent_father_state"].ToString();
                            simsobj.sims_parent_father_city = dr["sims_parent_father_city"].ToString();


                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            simsobj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.sims_parent_father_national_id__expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();

                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.mother_nationality1 = dr["mother_nationality1"].ToString();
                            simsobj.mother_nationality2 = dr["mother_nationality2"].ToString();
                            simsobj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            simsobj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            simsobj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            simsobj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();

                            simsobj.sims_parent_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                            simsobj.sims_parent_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                            simsobj.sims_parent_mother_state = dr["sims_parent_mother_state"].ToString();
                            simsobj.sims_parent_mother_city = dr["sims_parent_mother_city"].ToString();


                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            simsobj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();

                            simsobj.guardian_name = dr["guardian_name"].ToString();
                            simsobj.guardian_nationality1 = dr["guardian_nationality1"].ToString();
                            simsobj.guardian_nationality2 = dr["guardian_nationality2"].ToString();
                            simsobj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            simsobj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            simsobj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            simsobj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            simsobj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            simsobj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            simsobj.sims_parent_guardian_passport_expiry_date2 = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();
                            try
                            {
                                simsobj.sims_student_mother_language_name = dr["sims_student_mother_language_name"].ToString();
                            }
                            catch (Exception exx)
                            {
                            }
                            try
                            {
                                simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                                simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                                simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                                simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                                simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                                simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                                simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                                simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                                simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                                simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                            }
                            catch (Exception ex)
                            {
                                
                            }

                            try
                            {
                                simsobj.sims_student_email = dr["sims_student_email"].ToString();
                                simsobj.sims_student_last_name_reverse = dr["sims_student_last_name_reverse"].ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getStudentReport_ahgs")]
        public HttpResponseMessage getStudentReport_ahgs(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_active, string sims_inactive)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_active == "undefined" || sims_active == "" || sims_active == null)
                        sims_active = null;
                    if (sims_inactive == "undefined" || sims_inactive == "" || sims_inactive == null)
                        sims_inactive = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_active",sims_active),
                            new SqlParameter("@sims_inactive",sims_inactive),
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_blood_group = dr["sims_blood_group"].ToString();
                            simsobj.cur_shrt_name = dr["cur_shrt_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            simsobj.sims_student_main_language_name = dr["sims_student_main_language_name"].ToString();
                            simsobj.sims_health_detail = dr["health_detail"].ToString();

                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_bus_name = dr["sims_transport_bus_name"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.student_name = simsobj.sims_student_passport_first_name_en + " " + simsobj.sims_student_passport_middle_name_en + " " + simsobj.sims_student_passport_last_name_en;
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_en_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_en_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.student_name_ = dr["student_name_"].ToString();
                            simsobj.sims_student_gender = dr["gender"].ToString();
                            simsobj.sims_student_date = dr["sims_student_date"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_name"].ToString();
                            simsobj.sims_student_admission_academic_year = dr["sims_student_admission_academic_year"].ToString();
                            simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                            simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                            simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                            simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                            simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                            simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                            simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simsobj.sims_student_national_id_issue_date = dr["sims_student_national_id_issue_date"].ToString();
                            simsobj.sims_student_national_id_expiry_date = dr["sims_student_national_id_expiry_date"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                            simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            simsobj.sims_student_ea_transfer_status = dr["sims_student_ea_transfer_status"].ToString();
                            simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                            simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                            simsobj.sims_student_age = dr["sims_student_age"].ToString();
                            simsobj.sims_sibling_student_number = dr["sims_sibling_student_number"].ToString();
                            simsobj.sims_student_passport_issue_date = dr["sims_student_passport_issue_date"].ToString();
                            simsobj.sims_student_passport_expiry_date = dr["sims_student_passport_expiry_date"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();

                            simsobj.Student_Admission_Status = dr["Student_Admission_Status"].ToString();
                            simsobj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();

                            try
                            {
                                simsobj.sims_mai_status = dr["comn_user_app_status"].ToString();
                                simsobj.sims_aids_status = dr["comn_user_device_status"].ToString();
                                simsobj.sims_llma_status = dr["comn_user_last_login"].ToString();
                            }
                            catch (Exception e)
                            {
                            }
                            try
                            {
                                string data_direc= dr["sims_transport_route_direction"].ToString();
                                if (!string.IsNullOrEmpty(data_direc))
                                {
                                    try
                                    {
                                        string[] spilit_data_dir = dr["sims_transport_route_direction"].ToString().Split(',');
                                        string finalDirection = string.Empty;
                                        if (spilit_data_dir[0].Contains("01"))
                                        {
                                            finalDirection = "Pickup";
                                        }
                                        else if (spilit_data_dir[1].Contains("02"))
                                        {
                                            finalDirection = finalDirection + "," + "Drop";
                                        }
                                        if (spilit_data_dir[0].Contains("03"))
                                        {
                                            finalDirection = "Both";
                                        }
                                        simsobj.sims_transport_route_direction = finalDirection;
                                    }
                                    catch (Exception ex)
                                    {
                                        simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                    }
                                }
                                //simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                string data_route = dr["sims_transport_bus_name"].ToString();
                                if (!string.IsNullOrEmpty(data_route))
                                {
                                    string[] spilit_data = dr["sims_transport_bus_name"].ToString().Split(',');
                                    if (spilit_data.Length > 2)
                                    {
                                        simsobj.sims_transport_Pickup_bus_name = spilit_data[0];
                                        simsobj.sims_transport_Drop_bus_name = spilit_data[1];
                                    }
                                    else
                                    {
                                        simsobj.sims_transport_Pickup_bus_name = dr["sims_transport_bus_name"].ToString();
                                        simsobj.sims_transport_Drop_bus_name = dr["sims_transport_bus_name"].ToString();
                                    }

                                }                               
                                simsobj.sims_transport_route_student_status = dr["sims_transport_route_student_status"].ToString();
                                simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            }
                            catch (Exception e)
                            {
                                
                            }
                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.father_nationality1 = dr["father_nationality1"].ToString();
                            simsobj.father_nationality2 = dr["father_nationality2"].ToString();
                            simsobj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            simsobj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            simsobj.sims_parent_father_company = dr["sims_parent_father_company"].ToString();
                            simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();

                            simsobj.sims_parent_father_street_number = dr["sims_parent_father_street_number"].ToString();
                            simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                            simsobj.sims_parent_father_state = dr["sims_parent_father_state"].ToString();
                            simsobj.sims_parent_father_city = dr["sims_parent_father_city"].ToString();


                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            simsobj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.sims_parent_father_national_id__expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();

                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.mother_nationality1 = dr["mother_nationality1"].ToString();
                            simsobj.mother_nationality2 = dr["mother_nationality2"].ToString();
                            simsobj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            simsobj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            simsobj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            simsobj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();

                            simsobj.sims_parent_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                            simsobj.sims_parent_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                            simsobj.sims_parent_mother_state = dr["sims_parent_mother_state"].ToString();
                            simsobj.sims_parent_mother_city = dr["sims_parent_mother_city"].ToString();


                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            simsobj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();

                            simsobj.guardian_name = dr["guardian_name"].ToString();
                            simsobj.guardian_nationality1 = dr["guardian_nationality1"].ToString();
                            simsobj.guardian_nationality2 = dr["guardian_nationality2"].ToString();
                            simsobj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            simsobj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            simsobj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            simsobj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            simsobj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            simsobj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            simsobj.sims_parent_guardian_passport_expiry_date2 = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();
                            try
                            {
                                simsobj.sims_student_mother_language_name = dr["sims_student_mother_language_name"].ToString();
                            }
                            catch (Exception exx)
                            {
                            }
                            try
                            {
                                simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                                simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                                simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                                simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                                simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                                simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                                simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                                simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                                simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                                simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims561> mod_list = new List<Sims561>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getStudentReportAdisw")]
        public HttpResponseMessage getStudentReportAdisw(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_active, string sims_inactive)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_active == "undefined" || sims_active == "" || sims_active == null)
                        sims_active = null;
                    if (sims_inactive == "undefined" || sims_inactive == "" || sims_inactive == null)
                        sims_inactive = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_active",sims_active),
                            new SqlParameter("@sims_inactive",sims_inactive),
                        });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.cur_shrt_name = dr["cur_shrt_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            simsobj.sims_student_main_language_name = dr["sims_student_main_language_name"].ToString();
                            simsobj.sims_health_detail = dr["health_detail"].ToString();

                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_bus_name = dr["sims_transport_bus_name"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.student_name = simsobj.sims_student_passport_first_name_en + " " + simsobj.sims_student_passport_middle_name_en + " " + simsobj.sims_student_passport_last_name_en;
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_en_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_en_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.student_name_ = dr["student_name_"].ToString();
                            simsobj.sims_student_gender = dr["gender"].ToString();
                            simsobj.sims_student_date = dr["sims_student_date"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_name"].ToString();
                            simsobj.sims_student_admission_academic_year = dr["sims_student_admission_academic_year"].ToString();
                            simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                            simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                            simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                            simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                            simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                            simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                            simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simsobj.sims_student_national_id_issue_date = dr["sims_student_national_id_issue_date"].ToString();
                            simsobj.sims_student_national_id_expiry_date = dr["sims_student_national_id_expiry_date"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                            simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            simsobj.sims_student_ea_transfer_status = dr["sims_student_ea_transfer_status"].ToString();
                            simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                            simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                            simsobj.sims_student_age = dr["sims_student_age"].ToString();
                            simsobj.sims_student_passport_issue_date = dr["sims_student_passport_issue_date"].ToString();
                            simsobj.sims_student_passport_expiry_date = dr["sims_student_passport_expiry_date"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.Student_Admission_Status = dr["Student_Admission_Status"].ToString();
                            simsobj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();
                            simsobj.sims_active_member = dr["active_member"].ToString();
                            simsobj.sims_contact_name = dr["contact_name"].ToString();
                            simsobj.sims_contact_number = dr["contact_number"].ToString();
                            simsobj.sims_fee_contact_person = dr["fee_contact_person"].ToString();
                            simsobj.sims_fee_Pref = dr["fee_Pref"].ToString();
                            simsobj.sims_MOE_Number = dr["MOE_Number"].ToString();
                            simsobj.sims_primary_contact_email_id = dr["primary_contact_email_id"].ToString();
                            simsobj.sims_fee_Payment_Contact_email_id = dr["fee_Payment_Contact_email_id"].ToString();
                            simsobj.sims_fee_Payment_Contact_Mobile_Number = dr["fee_Payment_Contact_Mobile_Number"].ToString();
                            simsobj.sims_fee_stream = dr["stream"].ToString();
                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.father_nationality1 = dr["father_nationality1"].ToString();
                            simsobj.father_nationality2 = dr["father_nationality2"].ToString();
                            simsobj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            simsobj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            simsobj.sims_parent_father_company = dr["sims_parent_father_company"].ToString();
                            simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            simsobj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.sims_parent_father_national_id__expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();

                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.mother_nationality1 = dr["mother_nationality1"].ToString();
                            simsobj.mother_nationality2 = dr["mother_nationality2"].ToString();
                            simsobj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            simsobj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            simsobj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            simsobj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();
                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            simsobj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();

                            simsobj.guardian_name = dr["guardian_name"].ToString();
                            simsobj.guardian_nationality1 = dr["guardian_nationality1"].ToString();
                            simsobj.guardian_nationality2 = dr["guardian_nationality2"].ToString();
                            simsobj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            simsobj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            simsobj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            simsobj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            simsobj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            simsobj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            simsobj.sims_parent_guardian_passport_expiry_date2 = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();
                          
                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        /*
        [Route("getStudentReportDpsmis")]
        public HttpResponseMessage getStudentReportDpsmis(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_active, string sims_inactive)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_active == "undefined" || sims_active == "" || sims_active == null)
                        sims_active = null;
                    if (sims_inactive == "undefined" || sims_inactive == "" || sims_inactive == null)
                        sims_inactive = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_active",sims_active),
                            new SqlParameter("@sims_inactive",sims_inactive),
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_blood_group = dr["sims_blood_group"].ToString();
                            simsobj.cur_shrt_name = dr["cur_shrt_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            simsobj.sims_student_main_language_name = dr["sims_student_main_language_name"].ToString();
                            simsobj.sims_health_detail = dr["health_detail"].ToString();

                            simsobj.sims_house_name = dr["sims_house_name"].ToString();

                            simsobj.sims_transport_route_in = dr["sims_transport_route_in"].ToString();
                            simsobj.sims_transport_bus_in = dr["sims_transport_bus_in"].ToString();
                            simsobj.sims_transport_route_out = dr["sims_transport_route_out"].ToString();
                            simsobj.sims_transport_bus_out = dr["sims_transport_bus_out"].ToString();


                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.student_name = simsobj.sims_student_passport_first_name_en + " " + simsobj.sims_student_passport_middle_name_en + " " + simsobj.sims_student_passport_last_name_en;
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_en_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_en_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.student_name_ = dr["student_name_"].ToString();
                            simsobj.sims_student_gender = dr["gender"].ToString();
                            simsobj.sims_student_date = dr["sims_student_date"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_name"].ToString();
                            simsobj.sims_student_admission_academic_year = dr["sims_student_admission_academic_year"].ToString();
                            simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                            simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                            simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                            simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                            simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                            simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                            simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simsobj.sims_student_national_id_issue_date = dr["sims_student_national_id_issue_date"].ToString();
                            simsobj.sims_student_national_id_expiry_date = dr["sims_student_national_id_expiry_date"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                            simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            simsobj.sims_student_ea_transfer_status = dr["sims_student_ea_transfer_status"].ToString();
                            simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                            simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                            simsobj.sims_student_age = dr["sims_student_age"].ToString();
                            simsobj.sims_sibling_student_number = dr["sims_sibling_student_number"].ToString();
                            simsobj.sims_student_passport_issue_date = dr["sims_student_passport_issue_date"].ToString();
                            simsobj.sims_student_passport_expiry_date = dr["sims_student_passport_expiry_date"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();

                            simsobj.Student_Admission_Status = dr["Student_Admission_Status"].ToString();
                            simsobj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();

                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.father_nationality1 = dr["father_nationality1"].ToString();
                            simsobj.father_nationality2 = dr["father_nationality2"].ToString();
                            simsobj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            simsobj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            simsobj.sims_parent_father_company = dr["sims_parent_father_company"].ToString();
                            simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            simsobj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.sims_parent_father_national_id__expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();

                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.mother_nationality1 = dr["mother_nationality1"].ToString();
                            simsobj.mother_nationality2 = dr["mother_nationality2"].ToString();
                            simsobj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            simsobj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            simsobj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            simsobj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();
                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            simsobj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();

                            simsobj.guardian_name = dr["guardian_name"].ToString();
                            simsobj.guardian_nationality1 = dr["guardian_nationality1"].ToString();
                            simsobj.guardian_nationality2 = dr["guardian_nationality2"].ToString();
                            simsobj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            simsobj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            simsobj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            simsobj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            simsobj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            simsobj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            simsobj.sims_parent_guardian_passport_expiry_date2 = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();

                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        */

        [Route("getStudentReportDpsmis")]
        public HttpResponseMessage getStudentReportDpsmis(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_active, string sims_inactive)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_active == "undefined" || sims_active == "" || sims_active == null)
                        sims_active = null;
                    if (sims_inactive == "undefined" || sims_inactive == "" || sims_inactive == null)
                        sims_inactive = null;

                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_active",sims_active),
                            new SqlParameter("@sims_inactive",sims_inactive),
                        });

                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);

        }


        #endregion

        #region  API For Admision Report...

        //[Route("getAllAdmissionStudentReport")]
        //public HttpResponseMessage getAllAdmissionStudentReport(string cur_name, string academic_year, string grd_code, string from, string to,string admission_status)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
        //    List<admission_report> mod_list = new List<admission_report>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            if (grd_code == "undefined" || grd_code == "" || grd_code == null)
        //                grd_code = null;
        //            if (from == "undefined" || from == "")
        //                from = null;
        //            if (to == "undefined" || to == "")
        //                to = null;

        //            if (admission_status == "undefined" || admission_status == "" || admission_status == null)
        //                admission_status = null;

        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_report_proc]",
        //                new List<SqlParameter>() { 

        //                    new SqlParameter("@opr", "K"),
        //                    new SqlParameter("@sims_cur_code", cur_name),
        //                    new SqlParameter("@sims_academic_year", academic_year),
        //                    new SqlParameter("@sims_grade_code", grd_code),
        //                    new SqlParameter("@start_date", db.DBYYYYMMDDformat(from)),
        //                    new SqlParameter("@end_date", db.DBYYYYMMDDformat(to)),
        //                    new SqlParameter("@sims_admission_status", admission_status)
        //                });

        //            if (dr.HasRows)
        //            {
        //                int i = 0;
        //                while (dr.Read())
        //                {
        //                    i = i + 1;

        //                    #region

        //                    admission_report simsobj = new admission_report();
        //                    simsobj.sims_admission_number = dr["sims_admission_number"].ToString();

        //                    try
        //                    {
        //                        simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
        //                        simsobj.fy_institution_name = dr["fy_sims_institute_name"].ToString();
        //                        simsobj.fy_institution_place = dr["fy_sims_institute_place"].ToString();
        //                        simsobj.fy_board_name = dr["fy_sims_board_name"].ToString();
        //                        simsobj.fy_year_pasing = dr["fy_sims_yearmonth_passing"].ToString();
        //                        simsobj.fy_seat_no = dr["fy_sims_seat_no"].ToString();
        //                        simsobj.fy_no_of_attempts_I = dr["fy_sims_no_attempts_sem1"].ToString();
        //                        simsobj.fy_no_of_obtained_I = dr["fy_sims_marks_obtained_sem1"].ToString();
        //                        simsobj.fy_marks_out_I = dr["fy_sims_marks_outof_sem1"].ToString();
        //                        simsobj.fy_percentage_I = dr["fy_sims_percentage_sem1"].ToString();
        //                        simsobj.fy_no_of_attempts_II = dr["fy_sims_no_attempts_sem2"].ToString();
        //                        simsobj.fy_no_of_obtained_II = dr["fy_sims_marks_obtained_sem2"].ToString();
        //                        simsobj.fy_marks_out_II = dr["fy_sims_marks_outeof_sem2"].ToString();
        //                        simsobj.fy_percentage_II = dr["fy_sims_percentage_sem2"].ToString();

        //                        simsobj.sy_institution_name = dr["sy_sims_institute_name"].ToString();
        //                        simsobj.sy_institution_place = dr["sy_sims_institute_place"].ToString();
        //                        simsobj.sy_board_name = dr["sy_sims_board_name"].ToString();
        //                        simsobj.sy_year_pasing = dr["sy_sims_yearmonth_passing"].ToString();
        //                        simsobj.sy_seat_no = dr["sy_sims_seat_no"].ToString();
        //                        simsobj.sy_no_of_attempts_III = dr["sy_sims_no_attempts_sem1"].ToString();
        //                        simsobj.sy_no_of_obtained_III = dr["sy_sims_marks_obtained_sem1"].ToString();
        //                        simsobj.sy_marks_out_III = dr["sy_sims_marks_outof_sem1"].ToString();
        //                        simsobj.sy_percentage_III = dr["sy_sims_percentage_sem1"].ToString();
        //                        simsobj.sy_no_of_attempts_IV = dr["sy_sims_no_attempts_sem2"].ToString();
        //                        simsobj.sy_no_of_obtained_IV = dr["sy_sims_marks_obtained_sem2"].ToString();
        //                        simsobj.sy_marks_out_IV= dr["sy_sims_marks_outeof_sem2"].ToString();
        //                        simsobj.sy_percentage_IV = dr["sy_sims_percentage_sem2"].ToString();
        //                        simsobj.hsc_institution_name = dr["hsc_sims_institute_name"].ToString();
        //                        simsobj.hsc_institution_place = dr["hsc_sims_institute_place"].ToString();
        //                        simsobj.board_name = dr["hsc_sims_board_name"].ToString();
        //                        simsobj.year_pasing = dr["hsc_sims_yearmonth_passing"].ToString();
        //                        simsobj.seat_no = dr["hsc_sims_seat_no"].ToString();
        //                        simsobj.no_of_attempts = dr["hsc_sims_no_attempts_sem1"].ToString();
        //                        simsobj.ssc_seat_no = dr["hsc_sims_seat_no"].ToString();
        //                        simsobj.ssc_no_of_attempts = dr["hsc_sims_no_attempts_sem1"].ToString();
        //                        simsobj.hsc_marks = dr["hsc_sims_marks_obtained_sem1"].ToString();
        //                        simsobj.marks_out = dr["hsc_sims_marks_outof_sem1"].ToString();
        //                        simsobj.hsc_percentage = dr["hsc_sims_percentage_sem1"].ToString();
        //                        simsobj.hsc_percentage_maths = dr["ssc_math"].ToString();
        //                        simsobj.hsc_percentage_science = dr["ssc_science"].ToString();
        //                        simsobj.XI_percentage_maths = dr["fy_sims_no_attempts_sem2"].ToString();
        //                        simsobj.XI_percentage_science = dr["fy_sims_marks_obtained_sem2"].ToString();
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                    }
        //                    try
        //                    {
        //                        simsobj.sims_admission_student_email= dr["sims_admission_student_email"].ToString();
        //                        simsobj.sims_admission_student_mobile = dr["sims_admission_student_mobile"].ToString();
        //                        simsobj.sims_admission_is_physically_handicapped = dr["sims_admission_is_physically_handicapped"].ToString();
        //                        simsobj.sims_admission_marital_status = dr["sims_admission_marital_status"].ToString();
        //                        simsobj.sims_student_category = dr["sims_student_category"].ToString();
        //                        simsobj.sims_admission_blood_group_code = dr["sims_admission_blood_group_code"].ToString();
        //                        simsobj.sims_admission_is_freedom_fighter = dr["sims_admission_is_freedom_fighter"].ToString();
        //                        simsobj.sims_admission_birthplace = dr["sims_admission_birthplace"].ToString();
        //                    }
        //                    catch (Exception e)
        //                    {                            }
        //                    simsobj.sims_admission_school_code = dr["sims_admission_school_code"].ToString();
        //                    simsobj.sims_admission_cur_code = dr["sims_admission_cur_code"].ToString();
        //                    simsobj.sims_admission_academic_year = dr["sims_admission_academic_year"].ToString();
        //                    simsobj.sims_admission_grade_code = dr["sims_admission_grade_code"].ToString();
        //                    simsobj.sims_admission_passport_number = dr["sims_admission_passport_number"].ToString();
        //                    simsobj.sims_admission_passport_issuing_authority = dr["sims_admission_passport_issuing_authority"].ToString();
        //                    simsobj.sims_admission_passport_issue_place = dr["sims_admission_passport_issue_place"].ToString();
        //                    simsobj.sims_admission_passport_first_name_en = dr["sims_admission_passport_first_name_en"].ToString();
        //                    simsobj.sims_admission_family_name_en = dr["sims_admission_family_name_en"].ToString();
        //                    simsobj.sims_admission_nickname = dr["sims_admission_nickname"].ToString();
        //                    simsobj.sims_admission_gender = dr["sims_admission_gender"].ToString();
        //                    simsobj.sims_admission_religion_code = dr["sims_admission_religion_code"].ToString();
        //                    simsobj.sims_admission_birth_country_code = dr["sims_admission_birth_country_code"].ToString();
        //                    simsobj.sims_admission_nationality_code = dr["sims_admission_nationality_code"].ToString();
        //                    simsobj.sims_admission_visa_number = dr["sims_admission_visa_number"].ToString();
        //                    simsobj.sims_admission_visa_issuing_place = dr["sims_admission_visa_issuing_place"].ToString();
        //                    simsobj.sims_admission_visa_issuing_authority = dr["sims_admission_visa_issuing_authority"].ToString();
        //                    simsobj.sims_admission_visa_type = dr["sims_admission_visa_type"].ToString();
        //                    simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
        //                    simsobj.sims_admission_sibling_name = dr["sims_admission_sibling_name"].ToString();
        //                    simsobj.sims_admission_sibling_status = dr["sims_admission_sibling_status"].ToString();
        //                    simsobj.sims_admission_sibling_enroll_number = dr["sims_admission_sibling_enroll_number"].ToString();
        //                    simsobj.sims_admission_parent_id = dr["sims_admission_parent_id"].ToString();
        //                    simsobj.sims_admission_primary_contact_pref = dr["sims_admission_primary_contact_pref"].ToString();
        //                    simsobj.sims_admission_transport_status = dr["sims_admission_transport_status"].ToString();
        //                    try
        //                    {
        //                        simsobj.sims_admission_transport_desc = dr["sims_admission_transport_desc"].ToString();
        //                    }
        //                    catch (Exception)
        //                    {

        //                    }
        //                    simsobj.sims_admission_marketing_code = dr["sims_admission_marketing_code"].ToString();
        //                    simsobj.sims_admission_marketing_description = dr["sims_admission_marketing_description"].ToString();
        //                    simsobj.sims_admission_legal_custody = dr["sims_admission_legal_custody"].ToString();
        //                    simsobj.sims_admission_current_school_name = dr["sims_admission_current_school_name"].ToString();
        //                    simsobj.sims_admission_current_school_head_teacher = dr["sims_admission_current_school_head_teacher"].ToString();
        //                    simsobj.sims_admission_current_school_enroll_number = dr["sims_admission_current_school_enroll_number"].ToString();
        //                    simsobj.sims_admission_current_school_grade = dr["sims_admission_current_school_grade"].ToString();
        //                    simsobj.sims_admission_current_school_cur = dr["sims_admission_current_school_cur"].ToString();
        //                    simsobj.sims_admission_current_school_language = dr["sims_admission_current_school_language"].ToString();
        //                    simsobj.sims_admission_current_school_address = dr["sims_admission_current_school_address"].ToString();
        //                    simsobj.sims_admission_current_school_city = dr["sims_admission_current_school_city"].ToString();
        //                    simsobj.sims_admission_current_school_country_code = dr["sims_admission_current_school_country_code"].ToString();
        //                    simsobj.sims_admission_current_school_phone = dr["sims_admission_current_school_phone"].ToString();
        //                    simsobj.sims_admission_current_school_fax = dr["sims_admission_current_school_fax"].ToString();
        //                    simsobj.sims_admission_declaration_status = dr["sims_admission_declaration_status"].ToString();
        //                    simsobj.sims_admission_fees_paid_status = dr["sims_admission_fees_paid_status"].ToString();
        //                    simsobj.sims_admission_status = dr["sims_admission_status"].ToString();
        //                    simsobj.sims_admission_user_code = dr["sims_admission_user_code"].ToString();
        //                    simsobj.sims_admission_section_code = dr["sims_admission_section_code"].ToString();
        //                    simsobj.sims_admission_fee_category = dr["sims_admission_fee_category"].ToString();
        //                    simsobj.sims_student_organizational_skills = dr["sims_student_organizational_skills"].ToString();
        //                    simsobj.sims_student_social_skills = dr["sims_student_social_skills"].ToString();
        //                    simsobj.sims_student_special_education = dr["sims_student_special_education"].ToString();

        //                    simsobj.sims_admission_tentative_joining_date = db.UIDDMMYYYYformat(dr["sims_admission_tentative_joining_date"].ToString());
        //                    simsobj.sims_admission_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
        //                    simsobj.sims_admission_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_expiry_date"].ToString());
        //                    simsobj.sims_admission_dob = db.UIDDMMYYYYformat(dr["sims_admission_dob"].ToString());
        //                    simsobj.sims_admission_commencement_date = db.UIDDMMYYYYformat(dr["sims_admission_commencement_date"].ToString());
        //                    simsobj.sims_admission_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_issue_date"].ToString());
        //                    simsobj.sims_admission_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_expiry_date"].ToString());
        //                    simsobj.sims_admission_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_issue_date"].ToString());
        //                    simsobj.sims_admission_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_expiry_date"].ToString());
        //                    simsobj.sims_admission_sibling_dob = db.UIDDMMYYYYformat(dr["sims_admission_sibling_dob"].ToString());
        //                    simsobj.sims_admission_current_school_from_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_from_date"].ToString());
        //                    simsobj.sims_admission_current_school_to_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_to_date"].ToString());
        //                    simsobj.sims_admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());

        //                    simsobj.sims_admission_father_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_father_national_id_expiry_date"].ToString());
        //                    simsobj.sims_admission_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());

        //                    simsobj.sims_admission_mother_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_mother_national_id_expiry_date"].ToString());
        //                    simsobj.sims_admission_mother_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());

        //                    simsobj.sims_admission_guardian_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
        //                    simsobj.sims_admission_guardian_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_guardian_national_id_expiry_date"].ToString());

        //                    simsobj.sims_admission_father_national_id = dr["sims_admission_father_national_id"].ToString();
        //                    simsobj.sims_admission_father_company_address = dr["sims_admission_father_company_address"].ToString();
        //                    simsobj.sims_admission_father_company_phone = dr["sims_admission_father_company_phone"].ToString();
        //                    simsobj.sims_admission_father_qualification = dr["sims_admission_father_qualification"].ToString();
        //                    simsobj.sims_admission_father_salutation_code = dr["sims_admission_father_salutation_code"].ToString();
        //                    simsobj.sims_admission_father_first_name = dr["sims_admission_father_first_name"].ToString();
        //                    simsobj.sims_admission_father_family_name = dr["sims_admission_father_family_name"].ToString();
        //                    simsobj.sims_admission_father_nationality1_code = dr["sims_admission_father_nationality1_code"].ToString();
        //                    simsobj.sims_admission_father_nationality2_code = dr["sims_admission_father_nationality2_code"].ToString();
        //                    simsobj.sims_admission_father_appartment_number = dr["sims_admission_father_appartment_number"].ToString();
        //                    simsobj.sims_admission_father_building_number = dr["sims_admission_father_building_number"].ToString();
        //                    simsobj.sims_admission_father_street_number = dr["sims_admission_father_street_number"].ToString();
        //                    simsobj.sims_admission_father_area_number = dr["sims_admission_father_area_number"].ToString();
        //                    simsobj.sims_admission_father_summary_address = dr["sims_admission_father_summary_address"].ToString();
        //                    simsobj.sims_admission_father_city = dr["sims_admission_father_city"].ToString();
        //                    simsobj.sims_admission_father_state = dr["sims_admission_father_state"].ToString();
        //                    simsobj.sims_admission_father_country_code = dr["sims_admission_father_country_code"].ToString();
        //                    simsobj.sims_admission_father_phone = dr["sims_admission_father_phone"].ToString();
        //                    simsobj.sims_admission_father_mobile = dr["sims_admission_father_mobile"].ToString();
        //                    simsobj.sims_admission_father_email = dr["sims_admission_father_email"].ToString();
        //                    simsobj.sims_admission_father_fax = dr["sims_admission_father_fax"].ToString();
        //                    simsobj.sims_admission_father_po_box = dr["sims_admission_father_po_box"].ToString();
        //                    simsobj.sims_admission_father_occupation = dr["sims_admission_father_occupation"].ToString();
        //                    simsobj.sims_admission_father_company = dr["sims_admission_father_company"].ToString();
        //                    simsobj.sims_admission_father_passport_number = dr["sims_admission_father_passport_number"].ToString();
        //                    simsobj.sims_admission_father_company = dr["sims_admission_father_company"].ToString();

        //                    simsobj.sims_admission_mother_salutation_code = dr["sims_admission_mother_salutation_code"].ToString();
        //                    simsobj.sims_admission_mother_first_name = dr["sims_admission_mother_first_name"].ToString();
        //                    simsobj.sims_admission_mother_family_name = dr["sims_admission_mother_family_name"].ToString();
        //                    simsobj.sims_admission_mother_name_ot = dr["sims_admission_mother_name_ot"].ToString();
        //                    simsobj.sims_admission_mother_nationality1_code = dr["sims_admission_mother_nationality1_code"].ToString();
        //                    simsobj.sims_admission_mother_nationality2_code = dr["sims_admission_mother_nationality2_code"].ToString();
        //                    simsobj.sims_admission_mother_appartment_number = dr["sims_admission_mother_appartment_number"].ToString();
        //                    simsobj.sims_admission_mother_building_number = dr["sims_admission_mother_building_number"].ToString();
        //                    simsobj.sims_admission_mother_street_number = dr["sims_admission_mother_street_number"].ToString();
        //                    simsobj.sims_admission_mother_area_number = dr["sims_admission_mother_area_number"].ToString();
        //                    simsobj.sims_admission_mother_summary_address = dr["sims_admission_mother_summary_address"].ToString();
        //                    simsobj.sims_admission_mother_city = dr["sims_admission_mother_city"].ToString();
        //                    simsobj.sims_admission_mother_state = dr["sims_admission_mother_state"].ToString();
        //                    simsobj.sims_admission_mother_country_code = dr["sims_admission_mother_country_code"].ToString();
        //                    simsobj.sims_admission_mother_phone = dr["sims_admission_mother_phone"].ToString();
        //                    simsobj.sims_admission_mother_mobile = dr["sims_admission_mother_mobile"].ToString();
        //                    simsobj.sims_admission_mother_email = dr["sims_admission_mother_email"].ToString();
        //                    simsobj.sims_admission_mother_fax = dr["sims_admission_mother_fax"].ToString();
        //                    simsobj.sims_admission_mother_po_box = dr["sims_admission_mother_po_box"].ToString();
        //                    simsobj.sims_admission_mother_occupation = dr["sims_admission_mother_occupation"].ToString();
        //                    simsobj.sims_admission_mother_company = dr["sims_admission_mother_company"].ToString();
        //                    simsobj.sims_admission_mother_passport_number = dr["sims_admission_mother_passport_number"].ToString();
        //                    simsobj.sims_admission_mother_national_id = dr["sims_admission_mother_national_id"].ToString();
        //                    simsobj.sims_admission_mother_company = dr["sims_admission_mother_company"].ToString();
        //                    simsobj.sims_admission_mother_company_address = dr["sims_admission_mother_company_address"].ToString();
        //                    simsobj.sims_admission_mother_company_phone = dr["sims_admission_mother_company_phone"].ToString();
        //                    simsobj.sims_admission_mother_qualification = dr["sims_admission_mother_qualification"].ToString();

        //                    simsobj.sims_admission_guardian_salutation_code = dr["sims_admission_guardian_salutation_code"].ToString();
        //                    simsobj.sims_admission_guardian_first_name = dr["sims_admission_guardian_first_name"].ToString();
        //                    simsobj.sims_admission_guardian_family_name = dr["sims_admission_guardian_family_name"].ToString();
        //                    simsobj.sims_admission_guardian_name_ot = dr["sims_admission_guardian_name_ot"].ToString();
        //                    simsobj.sims_admission_guardian_nationality1_code = dr["sims_admission_guardian_nationality1_code"].ToString();
        //                    simsobj.sims_admission_guardian_nationality2_code = dr["sims_admission_guardian_nationality2_code"].ToString();
        //                    simsobj.sims_admission_guardian_appartment_number = dr["sims_admission_guardian_appartment_number"].ToString();
        //                    simsobj.sims_admission_guardian_building_number = dr["sims_admission_guardian_building_number"].ToString();
        //                    simsobj.sims_admission_guardian_street_number = dr["sims_admission_guardian_street_number"].ToString();
        //                    simsobj.sims_admission_guardian_area_number = dr["sims_admission_guardian_area_number"].ToString();
        //                    simsobj.sims_admission_guardian_summary_address = dr["sims_admission_guardian_summary_address"].ToString();
        //                    simsobj.sims_admission_guardian_city = dr["sims_admission_guardian_city"].ToString();
        //                    simsobj.sims_admission_guardian_state = dr["sims_admission_guardian_state"].ToString();
        //                    simsobj.sims_admission_guardian_country_code = dr["sims_admission_guardian_country_code"].ToString();
        //                    simsobj.sims_admission_guardian_phone = dr["sims_admission_guardian_phone"].ToString();
        //                    simsobj.sims_admission_guardian_mobile = dr["sims_admission_guardian_mobile"].ToString();
        //                    simsobj.sims_admission_guardian_email = dr["sims_admission_guardian_email"].ToString();
        //                    simsobj.sims_admission_guardian_fax = dr["sims_admission_guardian_fax"].ToString();
        //                    simsobj.sims_admission_guardian_po_box = dr["sims_admission_guardian_po_box"].ToString();
        //                    simsobj.sims_admission_guardian_occupation = dr["sims_admission_guardian_occupation"].ToString();
        //                    simsobj.sims_admission_guardian_company = dr["sims_admission_guardian_company"].ToString();
        //                    simsobj.sims_admission_guardian_relationship_code = dr["sims_admission_guardian_relationship_code"].ToString();
        //                    simsobj.sims_admission_guardian_passport_number = dr["sims_admission_guardian_passport_number"].ToString();
        //                    simsobj.sims_admission_guardian_national_id = dr["sims_admission_guardian_national_id"].ToString();
        //                    simsobj.sims_admission_guardian_company = dr["sims_admission_guardian_company"].ToString();


        //                    simsobj.Sr_no = i.ToString();

        //                    mod_list.Add(simsobj);

        //                    #endregion

        //                }
        //            }
        //        }
        //    }

        //    catch (Exception x)
        //    {
        //        // Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        [Route("getAllAdmissionStudentReport")]
        public HttpResponseMessage getAllAdmissionStudentReport(string cur_name, string academic_year, string grd_code, string from, string to, string admission_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAllAdmissionStudentReport()PARAMETERS ::cur_name,academic_year,grd_code,from,to,admission_status";
            List<admission_report> mod_list = new List<admission_report>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (from == "undefined" || from == "")
                        from = null;
                    if (to == "undefined" || to == "")
                        to = null;

                    if (admission_status == "undefined" || admission_status == "" || admission_status == null)
                        admission_status = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_report_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "K"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@start_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@end_date", db.DBYYYYMMDDformat(to)),
                            new SqlParameter("@sims_admission_status", admission_status)
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;

                            #region

                            admission_report simsobj = new admission_report();
                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();

                            try
                            {
                                simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                simsobj.fy_institution_name = dr["fy_sims_institute_name"].ToString();
                                simsobj.fy_institution_place = dr["fy_sims_institute_place"].ToString();
                                simsobj.fy_board_name = dr["fy_sims_board_name"].ToString();
                                simsobj.fy_year_pasing = dr["fy_sims_yearmonth_passing"].ToString();
                                simsobj.fy_seat_no = dr["fy_sims_seat_no"].ToString();
                                simsobj.fy_no_of_attempts_I = dr["fy_sims_no_attempts_sem1"].ToString();
                                simsobj.fy_no_of_obtained_I = dr["fy_sims_marks_obtained_sem1"].ToString();
                                simsobj.fy_marks_out_I = dr["fy_sims_marks_outof_sem1"].ToString();
                                simsobj.fy_percentage_I = dr["fy_sims_percentage_sem1"].ToString();
                                simsobj.fy_no_of_attempts_II = dr["fy_sims_no_attempts_sem2"].ToString();
                                simsobj.fy_no_of_obtained_II = dr["fy_sims_marks_obtained_sem2"].ToString();
                                simsobj.fy_marks_out_II = dr["fy_sims_marks_outeof_sem2"].ToString();
                                simsobj.fy_percentage_II = dr["fy_sims_percentage_sem2"].ToString();

                                simsobj.sy_institution_name = dr["sy_sims_institute_name"].ToString();
                                simsobj.sy_institution_place = dr["sy_sims_institute_place"].ToString();
                                simsobj.sy_board_name = dr["sy_sims_board_name"].ToString();
                                simsobj.sy_year_pasing = dr["sy_sims_yearmonth_passing"].ToString();
                                simsobj.sy_seat_no = dr["sy_sims_seat_no"].ToString();
                                simsobj.sy_no_of_attempts_III = dr["sy_sims_no_attempts_sem1"].ToString();
                                simsobj.sy_no_of_obtained_III = dr["sy_sims_marks_obtained_sem1"].ToString();
                                simsobj.sy_marks_out_III = dr["sy_sims_marks_outof_sem1"].ToString();
                                simsobj.sy_percentage_III = dr["sy_sims_percentage_sem1"].ToString();
                                simsobj.sy_no_of_attempts_IV = dr["sy_sims_no_attempts_sem2"].ToString();
                                simsobj.sy_no_of_obtained_IV = dr["sy_sims_marks_obtained_sem2"].ToString();
                                simsobj.sy_marks_out_IV = dr["sy_sims_marks_outeof_sem2"].ToString();
                                simsobj.sy_percentage_IV = dr["sy_sims_percentage_sem2"].ToString();

                                simsobj.hsc_institution_name = dr["hsc_sims_institute_name"].ToString();
                                simsobj.hsc_institution_place = dr["hsc_sims_institute_place"].ToString();
                                simsobj.board_name = dr["hsc_sims_board_name"].ToString();
                                simsobj.year_pasing = dr["hsc_sims_yearmonth_passing"].ToString();
                                simsobj.seat_no = dr["hsc_sims_seat_no"].ToString();
                                simsobj.no_of_attempts = dr["hsc_sims_no_attempts_sem1"].ToString();
                                simsobj.hsc_marks = dr["hsc_sims_marks_obtained_sem1"].ToString();
                                simsobj.marks_out = dr["hsc_sims_marks_outof_sem1"].ToString();
                                simsobj.hsc_percentage = dr["hsc_sims_percentage_sem1"].ToString();
                                try
                                {
                                    simsobj.ssc_math = dr["ssc_math"].ToString();
                                    simsobj.ssc_science = dr["ssc_science"].ToString();
                                    
                                }
                                catch (Exception ex)
                                {
                                    
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            try
                            {
                                simsobj.sims_admission_student_email = dr["sims_admission_student_email"].ToString();
                                simsobj.sims_admission_student_mobile = dr["sims_admission_student_mobile"].ToString();
                                simsobj.sims_admission_is_physically_handicapped = dr["sims_admission_is_physically_handicapped"].ToString();
                                simsobj.sims_admission_marital_status = dr["sims_admission_marital_status"].ToString();
                                simsobj.sims_student_category = dr["sims_student_category"].ToString();
                                simsobj.sims_admission_blood_group_code = dr["sims_admission_blood_group_code"].ToString();
                                simsobj.sims_admission_is_freedom_fighter = dr["sims_admission_is_freedom_fighter"].ToString();
                                simsobj.sims_admission_birthplace = dr["sims_admission_birthplace"].ToString();
                            }
                            catch (Exception e)
                            { }
                            simsobj.sims_admission_school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.sims_admission_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_admission_academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_admission_grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.sims_admission_passport_number = dr["sims_admission_passport_number"].ToString();
                            simsobj.sims_admission_passport_issuing_authority = dr["sims_admission_passport_issuing_authority"].ToString();
                            simsobj.sims_admission_passport_issue_place = dr["sims_admission_passport_issue_place"].ToString();
                            simsobj.sims_admission_passport_first_name_en = dr["sims_admission_passport_first_name_en"].ToString();
                            simsobj.sims_admission_family_name_en = dr["sims_admission_family_name_en"].ToString();
                            simsobj.sims_admission_nickname = dr["sims_admission_nickname"].ToString();
                            simsobj.sims_admission_gender = dr["sims_admission_gender"].ToString();
                            simsobj.sims_admission_religion_code = dr["sims_admission_religion_code"].ToString();
                            simsobj.sims_admission_birth_country_code = dr["sims_admission_birth_country_code"].ToString();
                            simsobj.sims_admission_nationality_code = dr["sims_admission_nationality_code"].ToString();
                            simsobj.sims_admission_visa_number = dr["sims_admission_visa_number"].ToString();
                            simsobj.sims_admission_visa_issuing_place = dr["sims_admission_visa_issuing_place"].ToString();
                            simsobj.sims_admission_visa_issuing_authority = dr["sims_admission_visa_issuing_authority"].ToString();
                            simsobj.sims_admission_visa_type = dr["sims_admission_visa_type"].ToString();
                            simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                            simsobj.sims_admission_sibling_name = dr["sims_admission_sibling_name"].ToString();
                            simsobj.sims_admission_sibling_status = dr["sims_admission_sibling_status"].ToString();
                            simsobj.sims_admission_sibling_enroll_number = dr["sims_admission_sibling_enroll_number"].ToString();
                            simsobj.sims_admission_parent_id = dr["sims_admission_parent_id"].ToString();
                            simsobj.sims_admission_primary_contact_pref = dr["sims_admission_primary_contact_pref"].ToString();
                            simsobj.sims_admission_transport_status = dr["sims_admission_transport_status"].ToString();
                            try
                            {
                                simsobj.sims_admission_transport_desc = dr["sims_admission_transport_desc"].ToString();
                            }
                            catch (Exception)
                            {

                            }
                            simsobj.sims_admission_marketing_code = dr["sims_admission_marketing_code"].ToString();
                            simsobj.sims_admission_marketing_description = dr["sims_admission_marketing_description"].ToString();
                            simsobj.sims_admission_legal_custody = dr["sims_admission_legal_custody"].ToString();
                            simsobj.sims_admission_current_school_name = dr["sims_admission_current_school_name"].ToString();
                            simsobj.sims_admission_current_school_head_teacher = dr["sims_admission_current_school_head_teacher"].ToString();
                            simsobj.sims_admission_current_school_enroll_number = dr["sims_admission_current_school_enroll_number"].ToString();
                            simsobj.sims_admission_current_school_grade = dr["sims_admission_current_school_grade"].ToString();
                            simsobj.sims_admission_current_school_cur = dr["sims_admission_current_school_cur"].ToString();
                            simsobj.sims_admission_current_school_language = dr["sims_admission_current_school_language"].ToString();
                            simsobj.sims_admission_current_school_address = dr["sims_admission_current_school_address"].ToString();
                            simsobj.sims_admission_current_school_city = dr["sims_admission_current_school_city"].ToString();
                            simsobj.sims_admission_current_school_country_code = dr["sims_admission_current_school_country_code"].ToString();
                            simsobj.sims_admission_current_school_phone = dr["sims_admission_current_school_phone"].ToString();
                            simsobj.sims_admission_current_school_fax = dr["sims_admission_current_school_fax"].ToString();
                            simsobj.sims_admission_declaration_status = dr["sims_admission_declaration_status"].ToString();
                            simsobj.sims_admission_fees_paid_status = dr["sims_admission_fees_paid_status"].ToString();
                            simsobj.sims_admission_status = dr["sims_admission_status"].ToString();
                            simsobj.sims_admission_user_code = dr["sims_admission_user_code"].ToString();
                            simsobj.sims_admission_section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.sims_admission_fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.sims_student_organizational_skills = dr["sims_student_organizational_skills"].ToString();
                            simsobj.sims_student_social_skills = dr["sims_student_social_skills"].ToString();
                            simsobj.sims_student_special_education = dr["sims_student_special_education"].ToString();

                            simsobj.sims_admission_tentative_joining_date = db.UIDDMMYYYYformat(dr["sims_admission_tentative_joining_date"].ToString());
                            simsobj.sims_admission_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
                            simsobj.sims_admission_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_expiry_date"].ToString());
                            simsobj.sims_admission_dob = db.UIDDMMYYYYformat(dr["sims_admission_dob"].ToString());
                            simsobj.sims_admission_commencement_date = db.UIDDMMYYYYformat(dr["sims_admission_commencement_date"].ToString());
                            simsobj.sims_admission_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_issue_date"].ToString());
                            simsobj.sims_admission_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_expiry_date"].ToString());
                            simsobj.sims_admission_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_issue_date"].ToString());
                            simsobj.sims_admission_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_expiry_date"].ToString());
                            simsobj.sims_admission_sibling_dob = db.UIDDMMYYYYformat(dr["sims_admission_sibling_dob"].ToString());
                            simsobj.sims_admission_current_school_from_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_from_date"].ToString());
                            simsobj.sims_admission_current_school_to_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_to_date"].ToString());
                            simsobj.sims_admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());

                            simsobj.sims_admission_father_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_father_national_id_expiry_date"].ToString());
                            simsobj.sims_admission_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());

                            simsobj.sims_admission_mother_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_mother_national_id_expiry_date"].ToString());
                            simsobj.sims_admission_mother_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());

                            simsobj.sims_admission_guardian_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
                            simsobj.sims_admission_guardian_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_guardian_national_id_expiry_date"].ToString());

                            simsobj.sims_admission_father_national_id = dr["sims_admission_father_national_id"].ToString();
                            simsobj.sims_admission_father_company_address = dr["sims_admission_father_company_address"].ToString();
                            simsobj.sims_admission_father_company_phone = dr["sims_admission_father_company_phone"].ToString();
                            simsobj.sims_admission_father_qualification = dr["sims_admission_father_qualification"].ToString();
                            simsobj.sims_admission_father_salutation_code = dr["sims_admission_father_salutation_code"].ToString();
                            simsobj.sims_admission_father_first_name = dr["sims_admission_father_first_name"].ToString();
                            simsobj.sims_admission_father_family_name = dr["sims_admission_father_family_name"].ToString();
                            simsobj.sims_admission_father_nationality1_code = dr["sims_admission_father_nationality1_code"].ToString();
                            simsobj.sims_admission_father_nationality2_code = dr["sims_admission_father_nationality2_code"].ToString();
                            simsobj.sims_admission_father_appartment_number = dr["sims_admission_father_appartment_number"].ToString();
                            simsobj.sims_admission_father_building_number = dr["sims_admission_father_building_number"].ToString();
                            simsobj.sims_admission_father_street_number = dr["sims_admission_father_street_number"].ToString();
                            simsobj.sims_admission_father_area_number = dr["sims_admission_father_area_number"].ToString();
                            simsobj.sims_admission_father_summary_address = dr["sims_admission_father_summary_address"].ToString();
                            simsobj.sims_admission_father_city = dr["sims_admission_father_city"].ToString();
                            simsobj.sims_admission_father_state = dr["sims_admission_father_state"].ToString();
                            simsobj.sims_admission_father_country_code = dr["sims_admission_father_country_code"].ToString();
                            simsobj.sims_admission_father_phone = dr["sims_admission_father_phone"].ToString();
                            simsobj.sims_admission_father_mobile = dr["sims_admission_father_mobile"].ToString();
                            simsobj.sims_admission_father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.sims_admission_father_fax = dr["sims_admission_father_fax"].ToString();
                            simsobj.sims_admission_father_po_box = dr["sims_admission_father_po_box"].ToString();
                            simsobj.sims_admission_father_occupation = dr["sims_admission_father_occupation"].ToString();
                            simsobj.sims_admission_father_company = dr["sims_admission_father_company"].ToString();
                            simsobj.sims_admission_father_passport_number = dr["sims_admission_father_passport_number"].ToString();
                            simsobj.sims_admission_father_company = dr["sims_admission_father_company"].ToString();

                            simsobj.sims_admission_mother_salutation_code = dr["sims_admission_mother_salutation_code"].ToString();
                            simsobj.sims_admission_mother_first_name = dr["sims_admission_mother_first_name"].ToString();
                            simsobj.sims_admission_mother_family_name = dr["sims_admission_mother_family_name"].ToString();
                            simsobj.sims_admission_mother_name_ot = dr["sims_admission_mother_name_ot"].ToString();
                            simsobj.sims_admission_mother_nationality1_code = dr["sims_admission_mother_nationality1_code"].ToString();
                            simsobj.sims_admission_mother_nationality2_code = dr["sims_admission_mother_nationality2_code"].ToString();
                            simsobj.sims_admission_mother_appartment_number = dr["sims_admission_mother_appartment_number"].ToString();
                            simsobj.sims_admission_mother_building_number = dr["sims_admission_mother_building_number"].ToString();
                            simsobj.sims_admission_mother_street_number = dr["sims_admission_mother_street_number"].ToString();
                            simsobj.sims_admission_mother_area_number = dr["sims_admission_mother_area_number"].ToString();
                            simsobj.sims_admission_mother_summary_address = dr["sims_admission_mother_summary_address"].ToString();
                            simsobj.sims_admission_mother_city = dr["sims_admission_mother_city"].ToString();
                            simsobj.sims_admission_mother_state = dr["sims_admission_mother_state"].ToString();
                            simsobj.sims_admission_mother_country_code = dr["sims_admission_mother_country_code"].ToString();
                            simsobj.sims_admission_mother_phone = dr["sims_admission_mother_phone"].ToString();
                            simsobj.sims_admission_mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            simsobj.sims_admission_mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.sims_admission_mother_fax = dr["sims_admission_mother_fax"].ToString();
                            simsobj.sims_admission_mother_po_box = dr["sims_admission_mother_po_box"].ToString();
                            simsobj.sims_admission_mother_occupation = dr["sims_admission_mother_occupation"].ToString();
                            simsobj.sims_admission_mother_company = dr["sims_admission_mother_company"].ToString();
                            simsobj.sims_admission_mother_passport_number = dr["sims_admission_mother_passport_number"].ToString();
                            simsobj.sims_admission_mother_national_id = dr["sims_admission_mother_national_id"].ToString();
                            simsobj.sims_admission_mother_company = dr["sims_admission_mother_company"].ToString();
                            simsobj.sims_admission_mother_company_address = dr["sims_admission_mother_company_address"].ToString();
                            simsobj.sims_admission_mother_company_phone = dr["sims_admission_mother_company_phone"].ToString();
                            simsobj.sims_admission_mother_qualification = dr["sims_admission_mother_qualification"].ToString();

                            simsobj.sims_admission_guardian_salutation_code = dr["sims_admission_guardian_salutation_code"].ToString();
                            simsobj.sims_admission_guardian_first_name = dr["sims_admission_guardian_first_name"].ToString();
                            simsobj.sims_admission_guardian_family_name = dr["sims_admission_guardian_family_name"].ToString();
                            simsobj.sims_admission_guardian_name_ot = dr["sims_admission_guardian_name_ot"].ToString();
                            simsobj.sims_admission_guardian_nationality1_code = dr["sims_admission_guardian_nationality1_code"].ToString();
                            simsobj.sims_admission_guardian_nationality2_code = dr["sims_admission_guardian_nationality2_code"].ToString();
                            simsobj.sims_admission_guardian_appartment_number = dr["sims_admission_guardian_appartment_number"].ToString();
                            simsobj.sims_admission_guardian_building_number = dr["sims_admission_guardian_building_number"].ToString();
                            simsobj.sims_admission_guardian_street_number = dr["sims_admission_guardian_street_number"].ToString();
                            simsobj.sims_admission_guardian_area_number = dr["sims_admission_guardian_area_number"].ToString();
                            simsobj.sims_admission_guardian_summary_address = dr["sims_admission_guardian_summary_address"].ToString();
                            simsobj.sims_admission_guardian_city = dr["sims_admission_guardian_city"].ToString();
                            simsobj.sims_admission_guardian_state = dr["sims_admission_guardian_state"].ToString();
                            simsobj.sims_admission_guardian_country_code = dr["sims_admission_guardian_country_code"].ToString();
                            simsobj.sims_admission_guardian_phone = dr["sims_admission_guardian_phone"].ToString();
                            simsobj.sims_admission_guardian_mobile = dr["sims_admission_guardian_mobile"].ToString();
                            simsobj.sims_admission_guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.sims_admission_guardian_fax = dr["sims_admission_guardian_fax"].ToString();
                            simsobj.sims_admission_guardian_po_box = dr["sims_admission_guardian_po_box"].ToString();
                            simsobj.sims_admission_guardian_occupation = dr["sims_admission_guardian_occupation"].ToString();
                            simsobj.sims_admission_guardian_company = dr["sims_admission_guardian_company"].ToString();
                            simsobj.sims_admission_guardian_relationship_code = dr["sims_admission_guardian_relationship_code"].ToString();
                            simsobj.sims_admission_guardian_passport_number = dr["sims_admission_guardian_passport_number"].ToString();
                            simsobj.sims_admission_guardian_national_id = dr["sims_admission_guardian_national_id"].ToString();
                            simsobj.sims_admission_guardian_company = dr["sims_admission_guardian_company"].ToString();


                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);

                            #endregion

                        }
                    }
                }
            }

            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        #endregion

        #region API For Admision Email History...

        [Route("getAdmissionTemplateFromGrade")]
        public HttpResponseMessage getAdmissionTemplateFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";

            List<email> mod_list = new List<email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_email_history_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email simsobj = new email();
                            simsobj.sims_msg_subject = dr["sims_msg_subject"].ToString();
                            simsobj.sims_msg_type = dr["sims_msg_type"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getAdmissionEmailSchedule")]
        public HttpResponseMessage getAdmissionEmailSchedule(string cur_name, string academic_year, string grd_code, string email_code, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (email_code == "undefined" || email_code == "")
                        email_code = null;
                    if (from_date == "undefined" || from_date == "")
                        from_date = null;
                    if (to_date == "undefined" || to_date == "")
                        to_date = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_email_history_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_msg_subject", email_code),
                            new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@sims_to_date ",db.DBYYYYMMDDformat(to_date)),
                            
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.Sr_no = i.ToString();
                            simsobj.student_name_ = dr["sims_student_name"].ToString();
                            simsobj.father_name = dr["sims_parent_name"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_email_date"].ToString());
                            simsobj.sims_parent_father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.sims_transport_bus_name = dr["module"].ToString();
                            simsobj.sims_MOE_Number = dr["sims_email_subject"].ToString();
                            simsobj.sims_student_attribute1 = dr["sims_admission_parent_id"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_admission_student_id"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_sender_id"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_email_message"].ToString();
                            simsobj.sims_student_attribute5 = dr["sims_recepient_id"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDAdmissionEmailSchedule")]
        public HttpResponseMessage CUDAdmissionEmailSchedule(List<Sims176> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            string emailmasssage = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims176 simsobj in data)
                    {
                        try
                        {
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_admission_email_history_proc]",
                                    new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_email_number", simsobj.sims_student_attribute4),
                           
                         });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        emailmasssage = dr1["sims_msg_subject"].ToString();
                                    }
                                }
                                db1.Dispose();
                                dr1.Close();
                            }
                        }
                        catch (Exception e)
                        {
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_email_history_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","C"),
                            new SqlParameter("@sims_cur_code", simsobj.cur_shrt_name),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_parent_number", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_sender_id", simsobj.sims_student_attribute3),
                            new SqlParameter("@sims_recepient_id", simsobj.sims_student_attribute5),
                            new SqlParameter("@sims_msg_subject", simsobj.sims_MOE_Number),
                            //new SqlParameter("@sims_email_message", simsobj.sims_student_attribute4),
                            new SqlParameter("@sims_email_message", emailmasssage),
                            new SqlParameter("@today_date",  db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_expiry_date)),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        #endregion

        //#region API For Student Documment List...

        //[Route("getStudentDocumentList")]
        //public HttpResponseMessage getStudentDocumentList(string cur_name, string academic_year, string grd_code, string sec_code, string student_en)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
        //    List<Sims561> mod_list = new List<Sims561>();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            if (student_en == "undefined" || student_en == "" || student_en == null)
        //                student_en = null;
        //            if (grd_code == "undefined" || grd_code == "" || grd_code == null)
        //                grd_code = null;
        //            if (sec_code == "undefined" || sec_code == "" || sec_code == null)
        //                sec_code = null;

        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_document_details_proc]",
        //                new List<SqlParameter>() { 

        //                    new SqlParameter("@opr", "S"),
        //                    new SqlParameter("@sims_cur_code", cur_name),
        //                    new SqlParameter("@sims_academic_year", academic_year),
        //                    new SqlParameter("@sims_grade_code", grd_code),
        //                    new SqlParameter("@sims_section_code", sec_code),
        //                    new SqlParameter("@sims_student_enroll_number", student_en),
        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims561 simsobj = new Sims561();
        //                    simsobj.sims_enroll_number = dr["sims_admission_enroll_number"].ToString();
        //                    simsobj.student_full_name = dr["sims_student_name"].ToString();
        //                    simsobj.sims_cur_code = dr["sims_admission_cur_code"].ToString();
        //                    simsobj.sims_academic_year = dr["sims_admission_academic_year"].ToString();
        //                    simsobj.sims_grade_code = dr["sims_admission_grade_code"].ToString();
        //                    simsobj.sims_grade_name = dr["Garde_Name"].ToString();
        //                    simsobj.sims_section_code = dr["sims_admission_section_code"].ToString();
        //                    simsobj.sims_section_name = dr["Section_Name"].ToString();
        //                    simsobj.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
        //                    simsobj.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
        //                    simsobj.sims_admission_date_created = dr["sims_admission_date_created"].ToString();
        //                    mod_list.Add(simsobj);
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception x)
        //    {
        //        // Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        //#endregion

        #region API For Student Documment List...

        [Route("getStudentDocumentList")]
        public HttpResponseMessage getStudentDocumentList(string cur_name, string academic_year, string grd_code, string sec_code, string student_en)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims561> mod_list = new List<Sims561>();
            string str = "";

            List<document> doc_list = new List<document>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_document_details_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                           simsobj.doc_list = new List<document>();
                            document doc = new document();

                            //sims_admission_doc_admission_number,
                            //sims_admission_doc_criteria_code,

                            str = dr["sims_student_enroll_number"].ToString();

                            simsobj.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
                            doc.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
                            doc.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
                            doc.sims_admission_doc_verify = dr["sims_admission_doc_verify"].ToString().Equals("1") ? true : false;
                            doc.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            doc.doc_path_available_flag = dr["doc_path_available_flag"].ToString().Equals("1") ? true : false;

                            var v = from p in mod_list where p.sims_enroll_number == str select p;

                            if (v.Count() > 0)
                            {

                                v.ElementAt(0).doc_list.Add(doc);
                            }
                            else
                            {
                                mod_list.Add(simsobj);
                                simsobj.doc_list.Add(doc);
                            }

                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        [Route("getStudentDocumentListNew")]
        public HttpResponseMessage getStudentDocumentListNew(string cur_name, string academic_year, string grd_code, string sec_code, string student_en)
        {
            string str = "";
            string sub = "";
            List<Sims561> mod_list = new List<Sims561>();
            if (student_en == "undefined" || student_en == "" || student_en == null)
                student_en = null;
            if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                grd_code = null;
            if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                sec_code = null;

            //List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_document_details_proc]",
                         new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["sims_student_enroll_number"].ToString();
                            sub = dr["sims_criteria_name_en"].ToString();

                            var v = (from p in mod_list where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                Sims561 ob = new Sims561();
                                ob.doc_list = new List<document>();

                                ob.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
                                ob.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
                                ob.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
                                ob.sims_admission_doc_verify = dr["sims_admission_doc_verify"].ToString().Equals("1") ? true : false;
                                ob.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            //    ob.doc_path_available_flag = dr["doc_path_available_flag"].ToString().Equals("1") ? true : false;
                                ob.doc_path_available_flag = dr["doc_path_available_flag"].ToString();

                                document doc = new document();
                                doc.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
                                doc.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
                                doc.sims_admission_doc_verify = dr["sims_admission_doc_verify"].ToString().Equals("1") ? true : false;
                                doc.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                                doc.doc_path_available_flag = dr["doc_path_available_flag"].ToString().Equals("1") ? true : false;

                                ob.doc_list.Add(doc);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                document doc = new document();
                               // sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                                doc.sims_admission_doc_path = dr["sims_admission_doc_path"].ToString();
                                doc.sims_admission_doc_status = dr["sims_admission_doc_status"].ToString();
                                doc.sims_admission_doc_verify = dr["sims_admission_doc_verify"].ToString().Equals("1") ? true : false;
                                doc.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                                doc.doc_path_available_flag = dr["doc_path_available_flag"].ToString().Equals("1") ? true : false;


                                //sb.sims_status = dr["Status"].Equals(1) ? true : false;
                                v.ElementAt(0).doc_list.Add(doc);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        #endregion

        #region API For Email Invoice...

        [Route("getStudentEmailInvoice")]
        public HttpResponseMessage getStudentEmailInvoice(string cur_name, string academic_year, string grd_code, string sec_code, string user_code, string start_date, string end_date, string term_code)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<sims541> mod_list = new List<sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (user_code == "undefined" || user_code == "" || user_code == null)
                        user_code = null;
                    if (start_date == "undefined" || start_date == "" || start_date == null)
                        start_date = null;
                    if (end_date == "undefined" || end_date == "" || end_date == null)
                        end_date = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_invoice_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", cur_name),
                            new SqlParameter("@academic_year", academic_year),
                            new SqlParameter("@grade_code", grd_code),
                            new SqlParameter("@section_code", sec_code),
                            new SqlParameter("@start_date", db.DBYYYYMMDDformat(start_date)),
                            new SqlParameter("@end_date", db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@Search", user_code),
                            new SqlParameter("@term_code", term_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 simsobj = new sims541();
                            simsobj.in_no = dr["in_no"].ToString();
                            try {
                                simsobj.count_n = dr["count_n"].ToString();
                            }
                            catch (Exception ex) { }
                            //simsobj.in_date = DateTime.Parse(dr["in_date"].ToString()).ToShortDateString();
                            simsobj.in_date = db.UIDDMMYYYYformat(dr["in_date"].ToString());
                            simsobj.in_status = dr["in_status"].ToString();
                            simsobj.in_status_code = dr["InStatus"].ToString();
                            simsobj.sims_invoice_amount = dr["in_total_amount"].ToString();
                            simsobj.sims_enroll_number = dr["enroll_number"].ToString();
                            simsobj.sims_student_name = string.Format("{0}({1})", dr["StudentName"].ToString(), simsobj.sims_enroll_number);
                            simsobj.sims_parent_name = dr["Father_Name"].ToString();
                            simsobj.sims_academic_year = dr["in_academic_year"].ToString(); // obj.sims_academic_year;
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString(); //obj.sims_cur_code;
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString(); //obj.sims_grade_code;
                            simsobj.sims_section_code = dr["sims_section_code"].ToString(); //obj.sims_section_code;
                            simsobj.sims_period_code = dr["in_reference_no"].ToString();
                            simsobj.sims_invoce_mode = dr["Invoice_mode"].ToString();
                            //simsobj.sims_student_class = dr["Class"].ToString();

                            string f_email = dr["sims_parent_father_email"].ToString();
                            string m_email = dr["sims_parent_mother_email"].ToString();

                            if (!string.IsNullOrEmpty(dr["sims_parent_father_email"].ToString()) && dr["sims_parent_father_email"].ToString() != "-")
                                f_email = dr["sims_parent_father_email"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_parent_mother_email"].ToString()) && dr["sims_parent_mother_email"].ToString() != "-")
                                m_email = dr["sims_parent_mother_email"].ToString();

                            if (!string.IsNullOrEmpty(f_email) && !string.IsNullOrEmpty(m_email))
                                simsobj.father_email = f_email + "," + m_email;
                            if (!string.IsNullOrEmpty(f_email) && string.IsNullOrEmpty(m_email))
                                simsobj.father_email = f_email;
                            if (string.IsNullOrEmpty(f_email) && string.IsNullOrEmpty(m_email))
                                simsobj.father_email = m_email;
                            if (string.IsNullOrEmpty(f_email) && string.IsNullOrEmpty(m_email))
                                simsobj.father_email = ""; ;

                            simsobj.mother_email = dr["sims_parent_mother_email"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("getStudentEmailInvoiceDetails")]
        public HttpResponseMessage getStudentEmailInvoiceDetails(string invo)
        {
           
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<sims541> mod_list = new List<sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                 

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_invoice_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "B"),
                           
                            new SqlParameter("@Search", invo),
                         
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 simsobj = new sims541();
                            simsobj.in_no = dr["sims_invoice_number"].ToString();

                            simsobj.in_date = db.UIDDMMYYYYformat(dr["sims_send_date"].ToString());
                           
                            //simsobj.sims_student_class = dr["Class"].ToString();

                            simsobj.father_email = dr["sims_parent_email"].ToString();
                           

                          

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        
        [Route("CUDsendemailinvoicenew")]
        public HttpResponseMessage CUDsendemailinvoicenew(List<sims541> data)
        {
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                {
                    st = string.Empty;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (sims541 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_invoice_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","I"),
                            new SqlParameter("@enrollno",simsobj.sims_enroll_number),
                            new SqlParameter("@sims_parent_email_id", simsobj.father_email),
                            new SqlParameter("@sims_invoice_number", simsobj.in_no),

                            //new SqlParameter("@sims_complaint_registration_date", db.DBYYYYMMDDformat(simsobj.sims_complaint_registration_date)),
                        });

                            if (dr.Read())
                            {
                                st = dr[0].ToString();
                                msg.strMessage = st;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();

                            sendemailinvoice(simsobj.sims_cur_code, simsobj.sims_academic_year, simsobj.sims_grade_code, simsobj.sims_section_code, simsobj.sims_enroll_number, simsobj.in_no, simsobj.sims_invoce_mode, simsobj.sims_period_code, simsobj.father_email, simsobj.term_code);
                        }
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        //[Route("getemailinvoice")]
        public void sendemailinvoice(string cur_code, string academic_year, string grade_code, string section_code, string enroll, string invoice_no, string invoice_mode, string invoice_period, string emailids, string term_code)
        {
            //SimsReports.Sims.SIMR60ETA bn = new SimsReports.Sims.SIMR60ETA();
     //       SimsReports.Fees.SIMR60CSD rfile = new SimsReports.Fees.SIMR60CSD();
      //      SaveReport(rfile, cur_code, academic_year, grade_code, section_code, enroll, invoice_no, invoice_mode, invoice_period, emailids, term_code);
        }



        void SaveReport(Telerik.Reporting.Report report, string cur_code, string academic_year, string grade_code, string section_code, string enroll, string invoice_no, string invoice_mode, string invoice_period, string emailids, string term_code)
        {
            try
            {
                ReportProcessor reportProcessor = new ReportProcessor();

                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

                instanceReportSource.ReportDocument = report;
                report.ReportParameters["cur_code"].Value = cur_code;
                report.ReportParameters["acad_year"].Value = academic_year;
                report.ReportParameters["grade_code"].Value = grade_code;
                report.ReportParameters["section_code"].Value = section_code;
                report.ReportParameters["enroll"].Value = enroll;
                report.ReportParameters["term_code"].Value = term_code;
                report.ReportParameters["in_no"].Value = invoice_no;
                //report.ReportParameters["invmode"].Value = invoice_mode;
                //report.ReportParameters["invperiod"].Value = invoice_period;
                string emailid = emailids;

                string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Docs/" + enroll + "_" + invoice_no + ".pdf";

                string fileName = HttpContext.Current.Server.MapPath(path);
                //string fileName1 = enroll + "_" + invoice_no + ".pdf";
                string fileName1 = enroll + "_" + invoice_no + ".pdf";

                RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                {
                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    fs.Close();
                    fs.Dispose();
                }

                report.Dispose();
                reportProcessor = null;
                result = null;

                // string EmailSubject = "Invoice Details";
                // //string Email_Body = "Dear Parent,<p>Please find attached the copy of Invoice for academic year " + academic_year + ".</p><br><p>Regards,<br>Star International School</p>";
                // string Email_Body = "<style type='text/css'>p { margin-top: 0px;margin-bottom: 12px;line-height: 1.15; } body { font-family: 'Segoe UI';font-style: Normal;font-weight: normal;font-size: 12px; } " +
                // ".Normal { telerik-style-type: paragraph;telerik-style-name: Normal;border-collapse: collapse; } .TableNormal { telerik-style-type: table;telerik-style-name: TableNormal;border-collapse: collapse; }" +
                // "</style><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>Dear Parents, </span></p><p class='Normal'>" +
                // "<span style='font-family: 'Calibri';font-style: Normal;font-weight: bold;font-size: 12px;text-decoration: underline;'>SCHOOL FEES:</span>" +
                // "<span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>" +
                // "The School fees for your child has to be paid before 1st September 2016. If you would like to make the payment now, the Accountant is available in the School from 8.30am to 1pm from Sunday to Thursday" +
                // "(till 30th June 2016 and from 15th August onwards). The misc fees are included in the First term fees. Please see your child&rsquo;s invoice(attached) for the breakup. " +
                // "</span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>The sibling discount is applicable ONLY if Term I is fully paid and two post dated cheques are submitted for Term II and III on or before" +
                // "1st Sep 2016.</span><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>*Term I includes the misc fees</span></p><p class='Normal'>" +
                // "<span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>1) Due dates for payment of fees: First term 1st September 2016, Second term 15th December 2016; Third term 15th&nbsp; March 2017. Cheques should be made in favor of " +
                // "</span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>Star International School." +
                //"</span><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>2) T</span>" +
                // "<span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>Two Post-dated cheques for the 2nd and 3rd installments MUST be paid to the school along with the cash/cheque for the 1st term</span>" +
                // "<span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>. If you would like to make cash payments for Term II and III, the cheque will be kept as security deposits which the parents can collect it before the cheque due date. " +
                // "These cheques will be deposited on the due date.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;text-decoration: underline;'>" +
                // "Optional Fees: </span></p><p class='Normal'><span style='font-family: 'Calibri';'>Fine for returned Cheque-AED 100</span></p><p class='Normal'>" +
                // "<span style='font-family: 'Calibri';'>Fine for bounced cheque-AED 250</span></p><p class='Normal'><span style='font-family: 'Calibri';'>Postponing the Cheque for 1 week(5 working days) will be 100Dhs and any subsequent working days after that will be charged @25Dhs per day. (Subject to the Principal&rsquo;s approval)</span></p><p class='Normal'> </p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;'>Please see the School Policies regarding the Discount, Late Admission and Tuition Refund. It will be effective from 1st September 2016.</span> <span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>DISCOUNT POLICY</span><span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>Sibling Discount </span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>If more than one child takes admission from a family of the same parents, then the second child gets 10% discount, third child onwards will get 20%. Discount will be applicable for the lowest fees with following conditions and will be implemented strictly starting September 2016.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>&nbsp; </span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>1.&nbsp; &nbsp;  </span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>The sibling discount is applicable only if Term I is fully paid and two PDC is submitted on or before 1st Sep 2016.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'> 2.&nbsp; &nbsp; &nbsp;  The discount is to be reflected on the last installment payment. If any of the payments are late, sibling discount will be revoked.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'> 3.&nbsp; &nbsp; &nbsp;  If the cheque is dishonored due to insufficient fund, the discount will be revoked.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>4.&nbsp; &nbsp; &nbsp;  The discount will not be eligible for late admission students and it will be applicable only from the beginning of ensuing academic year.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>5.&nbsp; &nbsp; &nbsp;  The student will not be entitled to avail more than one discount.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>6.&nbsp; &nbsp; &nbsp;  We will NOT be offering any special discount to any organization (like UOD, Emirates etc.,)</span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>TUITION REFUND POLICY</span><span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>As per KHDA fee framework.</span></p><p class='Normal'><span>&nbsp;</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Best Regards</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Aleemunnisa</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>STAR INTERNATIONAL SCHOOL-ALTWAR 2</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>P.O. Box 51008 - Dubai, UAE </span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Tel: 04-263 8999 Fax: 04-263 8998</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Email: admin.altwar@starintl.ae&nbsp; </span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Website: http://starintlschoolaltwar.com</span></p>";
                List<comn_email_attachments> lstatt = new List<comn_email_attachments>();
                comn_email_attachments att = new comn_email_attachments();
                att.attFilename = fileName1;
                lstatt.Add(att);

                //Get Parent Email ID                

                List<Comn_email> email = new List<Comn_email>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    try
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_invoice_proc]",
                            new List<SqlParameter>() {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_parent_email_id", emailids),
                            new SqlParameter("@cfile1", fileName1),
                            new SqlParameter("@enrollno", enroll),
                            new SqlParameter("@term_code", term_code),
                            new SqlParameter("@academic_year", academic_year),


                        });
                        if (dr.HasRows)
                        {
                            //while (dr.Read())
                            //{
                            //    if (!string.IsNullOrEmpty(dr["sims_parent_father_email"].ToString()) && dr["sims_parent_father_email"].ToString() != "-")
                            //    {
                            //        Comn_email em = new Comn_email();
                            //        em.emailsendto = dr["sims_parent_father_email"].ToString();
                            //        em.body = Email_Body;
                            //        em.subject = EmailSubject;
                            //        email.Add(em);
                            //    }
                            //    if (!string.IsNullOrEmpty(dr["sims_parent_mother_email"].ToString()) && dr["sims_parent_mother_email"].ToString() != "-")
                            //    {
                            //        Comn_email em1 = new Comn_email();
                            //        em1.emailsendto = dr["sims_parent_mother_email"].ToString();
                            //        em1.body = Email_Body;
                            //        em1.subject = EmailSubject;
                            //        email.Add(em1);
                            //    }
                            //}
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                // ScheduleMails(email, lstatt);
            }
            catch (Exception de)
            {

            }
        }

        #endregion

        #region API For Absent Student

        [Route("getAbsentStudentList")]
        public HttpResponseMessage getAbsentStudentList(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string select_date, string att_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims561> mod_list = new List<Sims561>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "")
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "")
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "")
                        sec_code = null;
                    if (select_date == "undefined" || select_date == "")
                        select_date = null;
                    if (att_code == "undefined" || att_code == "")
                        att_code = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_enroll_number", student_en),
                            new SqlParameter("@today_date", db.DBYYYYMMDDformat(select_date)),
                            //new SqlParameter("@grade_List", grd_code),
                            //new SqlParameter("@section_List", sec_code),
                            new SqlParameter("@sims_attendance_code", att_code),
                            
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims561 simsobj = new Sims561();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_full_name = dr["sims_student_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                           simsobj.sims_email_id = dr["EmailID"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_contact_number = dr["sims_contact_number"].ToString();
                            simsobj.sims_contact_person = dr["sims_student_primary_contact_pref"].ToString();
                            simsobj.sims_attendance_day_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();
                            simsobj.doc_path_available_flag = dr["comn_alert_message"].ToString();
                            try
                            {
                                simsobj.sims_absent_notification_flag_sms = dr["sims_sms_status"].ToString();
                                simsobj.sims_absent_notification_flag_alert = dr["comn_alert_read_status"].ToString();
                                //simsobj.sims_absent_notification_flag_unread_alert = dr["comn_alert_unread_status"].ToString();
                                simsobj.sims_absent_notification_flag_email = dr["sims_email_status"].ToString();
                                simsobj.sims_absent_notification_sms = dr["Sms_Count"].ToString();
                                simsobj.sims_absent_notification_alert = dr["Alert_Count"].ToString();
                                //simsobj.sims_absent_notification_unread_alert = dr["Alert_Unread_Count"].ToString();
                                simsobj.sims_absent_notification_email = dr["Email_Count"].ToString();
                                simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            }
                            catch (Exception e)
                            {
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

       [Route("CUDAbsentStudentList")]
        public HttpResponseMessage CUDAbsentStudentList(string data1,List<Sims561> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            Sims561 smobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims561>(data1);

            #region Parent Email Id update...

            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","N"),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                           new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr1.Read())
                        {

                        }
                        db1.Dispose();
                        dr1.Close();
                    }
                }
            }
            catch (Exception f)
            {

            }
            
            #endregion

            #region Save Sending data...

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_name", simsobj.sims_section_name),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_student_name", simsobj.student_full_name),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                            new SqlParameter("@sims_contact_number", simsobj.sims_contact_number),
                            new SqlParameter("@today_date",  db.DBYYYYMMDDformat(simsobj.sims_from_date)),
                            new SqlParameter("@comn_alert_message", simsobj.doc_path_available_flag),
                            new SqlParameter("@sims_contact_person", simsobj.sims_contact_person),
                           new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                    #region send absent student list to principle
                        try
                        {
                            SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                            new List<SqlParameter>()
                            {
                                  new SqlParameter("@opr",'E'),
                                  new SqlParameter("@stud_name", smobj.stud_name),
                                  new SqlParameter("@attendance_type", smobj.attendance_type),
                                  new SqlParameter("@today_date",  db.DBYYYYMMDDformat(smobj.sims_from_date))
                            });
                            if (dr2.RecordsAffected > 0)
                            {
                                inserted = true;
                            }

                            dr2.Close();
                        }
                        catch (Exception ex)
                        { }
                    #endregion
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            #endregion

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        
        }


        [Route("CUDAbsentStudentList")]
        public HttpResponseMessage CUDAbsentStudentList(List<Sims561> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            #region Parent Email Id update...

            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","N"),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                            new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr1.Read())
                        {

                        }
                        db1.Dispose();
                        dr1.Close();
                    }
                }
            }
            catch (Exception f)
            {

            }

            #endregion

            #region Save Sending data...

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_name", simsobj.sims_grade_name),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_name", simsobj.sims_section_name),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_student_name", simsobj.student_full_name),
                            new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                           new SqlParameter("@sims_contact_number", simsobj.sims_contact_number),
                            new SqlParameter("@today_date",  db.DBYYYYMMDDformat(simsobj.sims_from_date)),
                            new SqlParameter("@comn_alert_message", simsobj.doc_path_available_flag),
                            new SqlParameter("@sims_contact_person", simsobj.sims_contact_person),
                           new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            #endregion

            return Request.CreateResponse(HttpStatusCode.OK, msg);

        }



        [Route("getAttendanceRule")]
        public HttpResponseMessage getAttendanceRule(string curCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sim156> mod_list = new List<Sim156>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_absent_notification_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@sims_cur_code", curCode),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim156 simsobj = new Sim156();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_description = dr["sims_attendance_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        #endregion

        #region API For Absent Employee

        [Route("getAbsentEmployeeList")]
        public HttpResponseMessage getAbsentEmployeeList(string cur_name, string com_code, string academic_year, string dep_code, string desg_code, string employee_en, string select_date, string att_code, string opr_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims561> mod_list = new List<Sims561>();
            if (opr_code == "S")
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        if (employee_en == "undefined" || employee_en == "")
                            employee_en = null;
                        if (dep_code == "undefined" || dep_code == "")
                            dep_code = null;
                        if (desg_code == "undefined" || desg_code == "")
                            desg_code = null;
                        if (select_date == "undefined" || select_date == "")
                            select_date = null;
                        if (att_code == "undefined" || att_code == "")
                            att_code = null;

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                            new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@pays_comp_code", com_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@em_dept_code", dep_code),
                            new SqlParameter("@em_desg_code", desg_code),
                            new SqlParameter("@em_login_code", employee_en),
                            new SqlParameter("@today_date", db.DBYYYYMMDDformat(select_date)),
                            new SqlParameter("@pays_attendance_code", att_code),
                            
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims561 simsobj = new Sims561();
                                simsobj.sims_enroll_number = dr["em_login_code"].ToString();
                                simsobj.student_full_name = dr["sims_student_name"].ToString();
                                simsobj.sims_grade_code = dr["em_dept_code"].ToString();
                                simsobj.sims_grade_name = dr["em_desg_code"].ToString();
                                //simsobj.sims_section_code = dr["sims_section_code"].ToString();
                                //simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                                simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                                simsobj.sims_email_id = dr["EmailID"].ToString();
                                //simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                                simsobj.sims_contact_number = dr["sims_contact_number"].ToString();
                                //simsobj.sims_contact_person = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_attendance_day_attendance_code = dr["att_absent_flag"].ToString();
                                simsobj.doc_path_available_flag = dr["comn_alert_message"].ToString();
                                try
                                {
                                    simsobj.sims_absent_notification_flag_sms = dr["sims_sms_status"].ToString();
                                    simsobj.sims_absent_notification_flag_alert = dr["comn_alert_read_status"].ToString();
                                    simsobj.sims_absent_notification_flag_email = dr["sims_email_status"].ToString();
                                    simsobj.sims_absent_notification_sms = dr["Sms_Count"].ToString();
                                    simsobj.sims_absent_notification_alert = dr["Alert_Count"].ToString();
                                    simsobj.sims_absent_notification_email = dr["Email_Count"].ToString();
                                }
                                catch (Exception e)
                                {
                                }
                                mod_list.Add(simsobj);
                            }
                        }
                        dr.Close();
                        db.Dispose();
                    }
                    

                }
                catch (Exception x)
                {
                    // Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            if (opr_code == "A")
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        if (employee_en == "undefined" || employee_en == "")
                            employee_en = null;
                        if (dep_code == "undefined" || dep_code == "")
                            dep_code = null;
                        if (desg_code == "undefined" || desg_code == "")
                            desg_code = null;
                        if (select_date == "undefined" || select_date == "")
                            select_date = null;
                        if (att_code == "undefined" || att_code == "")
                            att_code = null;

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                            new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@pays_comp_code", com_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@em_dept_code", dep_code),
                            new SqlParameter("@em_desg_code", desg_code),
                            new SqlParameter("@em_login_code", employee_en),
                            new SqlParameter("@today_date", db.DBYYYYMMDDformat(select_date)),
                            new SqlParameter("@pays_attendance_code", att_code),
                            
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims561 simsobj = new Sims561();
                                simsobj.sims_enroll_number = dr["em_login_code"].ToString();
                                simsobj.student_full_name = dr["sims_student_name"].ToString();
                                simsobj.sims_grade_code = dr["em_dept_code"].ToString();
                                simsobj.sims_grade_name = dr["em_desg_code"].ToString();
                                //simsobj.sims_section_code = dr["sims_section_code"].ToString();
                                //simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                                simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                                simsobj.sims_email_id = dr["EmailID"].ToString();
                                //simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                                simsobj.sims_contact_number = dr["sims_contact_number"].ToString();
                                //simsobj.sims_contact_person = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_attendance_day_attendance_code = dr["att_absent_flag"].ToString();
                                simsobj.doc_path_available_flag = dr["comn_alert_message"].ToString();
                                try
                                {
                                    simsobj.sims_absent_notification_flag_sms = dr["sims_sms_status"].ToString();
                                    simsobj.sims_absent_notification_flag_alert = dr["comn_alert_read_status"].ToString();
                                    simsobj.sims_absent_notification_flag_email = dr["sims_email_status"].ToString();
                                    simsobj.sims_absent_notification_sms = dr["Sms_Count"].ToString();
                                    simsobj.sims_absent_notification_alert = dr["Alert_Count"].ToString();
                                    simsobj.sims_absent_notification_email = dr["Email_Count"].ToString();
                                }
                                catch (Exception e)
                                {
                                }
                                mod_list.Add(simsobj);
                            }
                        }
                        dr.Close();
                        db.Dispose();
                    }

                }
                catch (Exception x)
                {
                    // Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            if (opr_code == "B")
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        if (employee_en == "undefined" || employee_en == "")
                            employee_en = null;
                        if (dep_code == "undefined" || dep_code == "")
                            dep_code = null;
                        if (desg_code == "undefined" || desg_code == "")
                            desg_code = null;
                        if (select_date == "undefined" || select_date == "")
                            select_date = null;
                        if (att_code == "undefined" || att_code == "")
                            att_code = null;

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                            new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@pays_comp_code", com_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@em_dept_code", dep_code),
                            new SqlParameter("@em_desg_code", desg_code),
                            new SqlParameter("@em_login_code", employee_en),
                            new SqlParameter("@today_date", db.DBYYYYMMDDformat(select_date)),
                            new SqlParameter("@pays_attendance_code", att_code),
                            
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims561 simsobj = new Sims561();
                                simsobj.sims_enroll_number = dr["em_login_code"].ToString();
                                simsobj.student_full_name = dr["sims_student_name"].ToString();
                                simsobj.sims_grade_code = dr["em_dept_code"].ToString();
                                simsobj.sims_grade_name = dr["em_desg_code"].ToString();
                                //simsobj.sims_section_code = dr["sims_section_code"].ToString();
                                //simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                                simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                                simsobj.sims_email_id = dr["EmailID"].ToString();
                                //simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                                simsobj.sims_contact_number = dr["sims_contact_number"].ToString();
                                //simsobj.sims_contact_person = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_attendance_day_attendance_code = dr["att_absent_flag"].ToString();
                                simsobj.doc_path_available_flag = dr["comn_alert_message"].ToString();
                                try
                                {
                                    simsobj.sims_absent_notification_flag_sms = dr["sims_sms_status"].ToString();
                                    simsobj.sims_absent_notification_flag_alert = dr["comn_alert_read_status"].ToString();
                                    simsobj.sims_absent_notification_flag_email = dr["sims_email_status"].ToString();
                                    simsobj.sims_absent_notification_sms = dr["Sms_Count"].ToString();
                                    simsobj.sims_absent_notification_alert = dr["Alert_Count"].ToString();
                                    simsobj.sims_absent_notification_email = dr["Email_Count"].ToString();
                                }
                                catch (Exception e)
                                {
                                }
                                mod_list.Add(simsobj);
                            }
                        }
                        dr.Close();
                        db.Dispose();
                    }

                }
                catch (Exception x)
                {
                    // Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }


            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDEmployeeAbsentStudentList")]
        public HttpResponseMessage CUDEmployeeAbsentStudentList(string data1,List<Sims561> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            Sims561 empobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims561>(data1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims561 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@pays_comp_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@em_dept_code", simsobj.sims_grade_name),
                            new SqlParameter("@em_desg_code", simsobj.sims_grade_code),
                            new SqlParameter("@em_login_code", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_student_name", simsobj.student_full_name),
                            //new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                            new SqlParameter("@sims_contact_number", simsobj.sims_contact_number),
                            new SqlParameter("@today_date",  db.DBYYYYMMDDformat(simsobj.sims_from_date)),
                            new SqlParameter("@comn_alert_message", simsobj.doc_path_available_flag),
                            //new SqlParameter("@sims_contact_person", simsobj.sims_contact_person),
                            new SqlParameter("@sims_email_id", simsobj.sims_email_id),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                    try { 
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                        new List<SqlParameter>()
                        {
                              new SqlParameter("@opr",'E'),
                              new SqlParameter("@emp_name", empobj.emp_name),
                              new SqlParameter("@attendance_type", empobj.attendance_type),
                              new SqlParameter("@today_date",  db.DBYYYYMMDDformat(empobj.sims_from_date))
                        });
                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                 
                        dr1.Close();
                    }
                    catch(Exception ex)
                    {}
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        [Route("getEmployeeAttendanceRule")]
        public HttpResponseMessage getEmployeeAttendanceRule(string comCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sim156> mod_list = new List<Sim156>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_absent_notification_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@pays_comp_code", comCode),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim156 simsobj = new Sim156();
                            simsobj.sims_attendance_code = dr["pays_attendance_code"].ToString();
                            simsobj.sims_attendance_description = dr["pays_attendance_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        #endregion

        #region Sims668 Tc Certificate Requiest

        [Route("getTcCertificateRequiest")]
        public HttpResponseMessage getTcCertificateRequiest()//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            //new SqlParameter("@academic_year", academic_year),
                            //new SqlParameter("@grade_code", grd_code),
                            //new SqlParameter("@section_code", sec_code),
                            //new SqlParameter("@start_date", start_date),
                            //new SqlParameter("@end_date", end_date),
                            //new SqlParameter("@Search", user_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_certificate_request_number = dr["sims_tc_certificate_request_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_request_date"].ToString()))
                                simsobj.sims_tc_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_request_date"].ToString());
                            simsobj.sims_tc_certificate_requested_by = dr["sims_tc_certificate_requested_by"].ToString();
                            simsobj.sims_tc_certificate_reason = dr["sims_tc_certificate_reason"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_expected_date"].ToString()))
                                simsobj.sims_tc_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_expected_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_tc_certificate_enroll_number"].ToString();
                            //simsobj.sims_student_name = string.Format("{0}({1})", dr["StudentName"].ToString(), simsobj.sims_enroll_number);
                            //simsobj.sims_tc_certificate_issued_by = dr["sims_tc_certificate_issued_by"].ToString();
                            simsobj.sims_tc_certificate_request_status = dr["sims_tc_certificate_request_status"].ToString(); // obj.sims_academic_year;
                            simsobj.sims_tc_certificate_cur_code = dr["sims_tc_certificate_cur_code"].ToString();
                            simsobj.sims_tc_certificate_academic_year = dr["sims_tc_certificate_academic_year"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        //Sagar For Statudents Wise
        [Route("getTcCertificateRequieststatus")]
        public HttpResponseMessage getTcCertificateRequieststatus(string status)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_tc_certificate_request_status", status),
                            //new SqlParameter("@academic_year", academic_year),
                            //new SqlParameter("@grade_code", grd_code),
                            //new SqlParameter("@section_code", sec_code),
                            //new SqlParameter("@start_date", start_date),
                            //new SqlParameter("@end_date", end_date),
                            //new SqlParameter("@Search", user_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_certificate_request_number = dr["sims_tc_certificate_request_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_request_date"].ToString()))
                                simsobj.sims_tc_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_request_date"].ToString());
                            simsobj.sims_tc_certificate_requested_by = dr["sims_tc_certificate_requested_by"].ToString();
                            simsobj.sims_tc_certificate_reason = dr["sims_tc_certificate_reason"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_expected_date"].ToString()))
                                simsobj.sims_tc_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_expected_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_tc_certificate_enroll_number"].ToString();
                            //simsobj.sims_student_name = string.Format("{0}({1})", dr["StudentName"].ToString(), simsobj.sims_enroll_number);
                            //simsobj.sims_tc_certificate_issued_by = dr["sims_tc_certificate_issued_by"].ToString();
                            simsobj.sims_tc_certificate_request_status = dr["sims_tc_certificate_request_status"].ToString(); // obj.sims_academic_year;
                            simsobj.sims_tc_certificate_cur_code = dr["sims_tc_certificate_cur_code"].ToString();
                            simsobj.sims_tc_certificate_academic_year = dr["sims_tc_certificate_academic_year"].ToString();
                            simsobj.sims_tc_certificate_academic_year_desc = dr["sims_tc_certificate_academic_year_desc"].ToString();
                            simsobj.sims_appl_parameter = dr["sims_tc_type"].ToString();
                            simsobj.sims_appl_parameter_desc = dr["sims_appl_parameter_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        //Direct Approve Status
        [Route("getTcDirectApproveStatus")]
        public HttpResponseMessage getTcDirectApproveStatus()//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getTcDirectApproveStatus()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "M"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_direct_approve_status_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_tc_direct_approve_status = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getTcCertificateRequiestDate")]
        public HttpResponseMessage getTcCertificateRequiestDate(string date_wisesearch)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_tc_certificate_date", db.DBYYYYMMDDformat(date_wisesearch)),
                            //new SqlParameter("@academic_year", academic_year),
                            //new SqlParameter("@grade_code", grd_code),
                            //new SqlParameter("@section_code", sec_code),
                            //new SqlParameter("@start_date", start_date),
                            //new SqlParameter("@end_date", end_date),
                            //new SqlParameter("@Search", user_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_tc_certificate_request_number = dr["sims_tc_certificate_request_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_request_date"].ToString()))
                                simsobj.sims_tc_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_request_date"].ToString());
                            simsobj.sims_tc_certificate_requested_by = dr["sims_tc_certificate_requested_by"].ToString();
                            simsobj.sims_tc_certificate_reason = dr["sims_tc_certificate_reason"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_tc_certificate_expected_date"].ToString()))
                                simsobj.sims_tc_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_tc_certificate_expected_date"].ToString());
                            simsobj.sims_enroll_number = dr["sims_tc_certificate_enroll_number"].ToString();
                            //simsobj.sims_student_name = string.Format("{0}({1})", dr["StudentName"].ToString(), simsobj.sims_enroll_number);
                            //simsobj.sims_tc_certificate_issued_by = dr["sims_tc_certificate_issued_by"].ToString();
                            simsobj.sims_tc_certificate_cur_code = dr["sims_tc_certificate_cur_code"].ToString();
                            simsobj.sims_tc_certificate_academic_year_desc = dr["sims_tc_certificate_academic_year_desc"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_tc_certificate_request_status = dr["sims_tc_certificate_request_status"].ToString(); // obj.sims_academic_year;
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetStatusforStudent")]
        public HttpResponseMessage GetStatusforStudent(string enroll_no)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : GetStatusforStudent()PARAMETERS ::NA";
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@sims_enroll_number", enroll_no ),                            
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            //simsobj.sims_sr_no =dr["sims_sr_no"].ToString();
                            //simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.dept = dr["dept"].ToString();
                            simsobj.status = dr["status"].ToString();
                            if (!string.IsNullOrEmpty(dr["date"].ToString()))
                                simsobj.date = db.UIDDMMYYYYformat(dr["date"].ToString());
                            simsobj.clear_by = dr["clear_by"].ToString();
                            simsobj.remark = dr["remark"].ToString();
                            //simsobj.sims_finn_clr_by = dr["sims_finn_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_finn_clr_date"].ToString()))
                            //    simsobj.sims_finn_clr_date = DateTime.Parse(dr["sims_finn_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_finn_clr_remark = dr["sims_finn_clr_remark"].ToString();
                            //simsobj.sims_inv_clr_status = dr["sims_inv_clr_status"].ToString();
                            //simsobj.sims_inv_clr_by = dr["sims_inv_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_inv_clr_date"].ToString()))
                            //    simsobj.sims_inv_clr_date = DateTime.Parse(dr["sims_inv_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_inv_clr_remark = dr["sims_inv_clr_remark"].ToString();
                            //simsobj.sims_inci_clr_status = dr["sims_inci_clr_status"].ToString();
                            //simsobj.sims_inci_clr_by = dr["sims_inci_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_inci_clr_date"].ToString()))
                            //    simsobj.sims_inci_clr_date = DateTime.Parse(dr["sims_inci_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_inci_clr_remark = dr["sims_inci_clr_remark"].ToString();
                            //simsobj.sims_lib_clr_status = dr["sims_lib_clr_status"].ToString();
                            //simsobj.sims_lib_clr_by = dr["sims_lib_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_lib_clr_date"].ToString()))
                            //    simsobj.sims_lib_clr_date = DateTime.Parse(dr["sims_lib_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_lib_clr_remark = dr["sims_lib_clr_remark"].ToString();
                            //simsobj.sims_trans_clr_status = dr["sims_trans_clr_status"].ToString();
                            //simsobj.sims_trans_clr_by = dr["sims_trans_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_trans_clr_date"].ToString()))
                            //    simsobj.sims_trans_clr_date = DateTime.Parse(dr["sims_trans_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_trans_clr_remark = dr["sims_trans_clr_remark"].ToString();
                            //simsobj.sims_acad_clr_status = dr["sims_acad_clr_status"].ToString();
                            //simsobj.sims_acad_clr_by = dr["sims_acad_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_acad_clr_date"].ToString()))
                            //    simsobj.sims_acad_clr_date = DateTime.Parse(dr["sims_acad_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_acad_clr_remark = dr["sims_acad_clr_remark"].ToString();
                            //simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].ToString();
                            //simsobj.sims_admin_clr_by = dr["sims_admin_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_acad_clr_date"].ToString()))
                            //    simsobj.sims_admin_clr_date = DateTime.Parse(dr["sims_admin_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_admin_clr_remark = dr["sims_admin_clr_remark"].ToString();
                            //simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].ToString();
                            //simsobj.sims_other1_clr_by = dr["sims_other1_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other1_clr_date"].ToString()))
                            //    simsobj.sims_other1_clr_date = DateTime.Parse(dr["sims_other1_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other1_clr_remark = dr["sims_other1_clr_remark"].ToString();
                            //simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].ToString();
                            //simsobj.sims_other2_clr_by = dr["sims_other2_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other2_clr_date"].ToString()))
                            //    simsobj.sims_other2_clr_date = DateTime.Parse(dr["sims_other2_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other2_clr_remark = dr["sims_other2_clr_remark"].ToString();
                            //simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].ToString();
                            //simsobj.sims_other3_clr_by = dr["sims_other3_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other3_clr_date"].ToString()))
                            //    simsobj.sims_other2_clr_date = DateTime.Parse(dr["sims_other3_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other3_clr_remark = dr["sims_other3_clr_remark"].ToString();
                            //simsobj.sims_clr_status = dr["sims_clr_status"].Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getdatemonth")]
        public HttpResponseMessage getdatemonth(string enroll, string year)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : GetStatusforStudent()PARAMETERS ::NA";
            // List<Sim670> mod_list = new List<Sim670>();
            string due_date = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_request_tc_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "E"),
                            new SqlParameter("@sims_enroll_number", enroll ),
                            new SqlParameter("@sims_tc_certificate_academic_year", year ),
                            

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            due_date = dr["due_date"].ToString();


                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, due_date);
            }

            return Request.CreateResponse(HttpStatusCode.OK, due_date);

        }

        [Route("getResionsupdate")]
        public HttpResponseMessage getResionsupdate(string enroll, string year, string duedate)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : GetStatusforStudent()PARAMETERS ::NA";
            // List<Sim670> mod_list = new List<Sim670>();
            string due_date = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_request_tc_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@sims_enroll_number", enroll ),
                            new SqlParameter("@sims_tc_certificate_academic_year", year ),
                             new SqlParameter("@sims_tc_certificate_reason", duedate ),


                        });
                    if (dr.HasRows)
                    {

                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, due_date);
            }

            return Request.CreateResponse(HttpStatusCode.OK, due_date);

        }

        [Route("approvePendingStatus")]
        public HttpResponseMessage approvePendingStatus(List<Sims668> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : approvePendingStatus(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims668 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),                           
                            //new SqlParameter("@sims_tc_certificate_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_tc_certificate_request_number", simsobj.sims_tc_certificate_request_number),
                            new SqlParameter("@sims_tc_certificate_request_status", simsobj.sims_tc_certificate_request_status)
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetStatusforStudentNew")]
        public HttpResponseMessage GetStatusforStudentNew(string enroll_no)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : GetStatusforStudent()PARAMETERS ::NA";
            List<Sim670> mod_list = new List<Sim670>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@sims_enroll_number", enroll_no ),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim670 simsobj = new Sim670();
                            //simsobj.sims_sr_no =dr["sims_sr_no"].ToString();
                            //simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.dept = dr["dept"].ToString();
                            simsobj.status = dr["status"].ToString();
                            if (!string.IsNullOrEmpty(dr["date"].ToString()))
                                simsobj.date = db.UIDDMMYYYYformat(dr["date"].ToString());
                            simsobj.clear_by = dr["clear_by"].ToString();
                            simsobj.remark = dr["remark"].ToString();
                            //simsobj.sims_finn_clr_by = dr["sims_finn_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_finn_clr_date"].ToString()))
                            //    simsobj.sims_finn_clr_date = DateTime.Parse(dr["sims_finn_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_finn_clr_remark = dr["sims_finn_clr_remark"].ToString();
                            //simsobj.sims_inv_clr_status = dr["sims_inv_clr_status"].ToString();
                            //simsobj.sims_inv_clr_by = dr["sims_inv_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_inv_clr_date"].ToString()))
                            //    simsobj.sims_inv_clr_date = DateTime.Parse(dr["sims_inv_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_inv_clr_remark = dr["sims_inv_clr_remark"].ToString();
                            //simsobj.sims_inci_clr_status = dr["sims_inci_clr_status"].ToString();
                            //simsobj.sims_inci_clr_by = dr["sims_inci_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_inci_clr_date"].ToString()))
                            //    simsobj.sims_inci_clr_date = DateTime.Parse(dr["sims_inci_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_inci_clr_remark = dr["sims_inci_clr_remark"].ToString();
                            //simsobj.sims_lib_clr_status = dr["sims_lib_clr_status"].ToString();
                            //simsobj.sims_lib_clr_by = dr["sims_lib_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_lib_clr_date"].ToString()))
                            //    simsobj.sims_lib_clr_date = DateTime.Parse(dr["sims_lib_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_lib_clr_remark = dr["sims_lib_clr_remark"].ToString();
                            //simsobj.sims_trans_clr_status = dr["sims_trans_clr_status"].ToString();
                            //simsobj.sims_trans_clr_by = dr["sims_trans_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_trans_clr_date"].ToString()))
                            //    simsobj.sims_trans_clr_date = DateTime.Parse(dr["sims_trans_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_trans_clr_remark = dr["sims_trans_clr_remark"].ToString();
                            //simsobj.sims_acad_clr_status = dr["sims_acad_clr_status"].ToString();
                            //simsobj.sims_acad_clr_by = dr["sims_acad_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_acad_clr_date"].ToString()))
                            //    simsobj.sims_acad_clr_date = DateTime.Parse(dr["sims_acad_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_acad_clr_remark = dr["sims_acad_clr_remark"].ToString();
                            //simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].ToString();
                            //simsobj.sims_admin_clr_by = dr["sims_admin_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_acad_clr_date"].ToString()))
                            //    simsobj.sims_admin_clr_date = DateTime.Parse(dr["sims_admin_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_admin_clr_remark = dr["sims_admin_clr_remark"].ToString();
                            //simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].ToString();
                            //simsobj.sims_other1_clr_by = dr["sims_other1_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other1_clr_date"].ToString()))
                            //    simsobj.sims_other1_clr_date = DateTime.Parse(dr["sims_other1_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other1_clr_remark = dr["sims_other1_clr_remark"].ToString();
                            //simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].ToString();
                            //simsobj.sims_other2_clr_by = dr["sims_other2_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other2_clr_date"].ToString()))
                            //    simsobj.sims_other2_clr_date = DateTime.Parse(dr["sims_other2_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other2_clr_remark = dr["sims_other2_clr_remark"].ToString();
                            //simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].ToString();
                            //simsobj.sims_other3_clr_by = dr["sims_other3_clr_by"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_other3_clr_date"].ToString()))
                            //    simsobj.sims_other2_clr_date = DateTime.Parse(dr["sims_other3_clr_date"].ToString()).ToShortDateString();
                            //simsobj.sims_other3_clr_remark = dr["sims_other3_clr_remark"].ToString();
                            //simsobj.sims_clr_status = dr["sims_clr_status"].Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getTcTypes")]
        public HttpResponseMessage getTcTypes()
        {
            List<Sims668> mod_list = new List<Sims668>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'T'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims668 simsobj = new Sims668();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetTCappliedStatus")]
        public HttpResponseMessage GetTCappliedStatus(string enroll_no, string curr)//, string student_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : GetTCappliedStatus()PARAMETERS ::NA";
            List<Sim670> mod_list = new List<Sim670>();
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_enroll_number", enroll_no),    
                            new SqlParameter("@sims_tc_certificate_cur_code", curr),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            flag = true;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDTccertificaterequiest")]
        public HttpResponseMessage CUDTccertificaterequiest(List<Sims668> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims668 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_tc_certificate_reason", simsobj.sims_tc_certificate_reason),
                            new SqlParameter("@sims_tc_certificate_requested_by", simsobj.sims_tc_certificate_requested_by),
                            new SqlParameter("@sims_tc_certificate_request_date",db.DBYYYYMMDDformat(simsobj.sims_tc_certificate_request_date)),
                            new SqlParameter("@sims_tc_certificate_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_tc_certificate_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_tc_certificate_request_number", simsobj.sims_tc_certificate_request_number),
                            new SqlParameter("@sims_appl_parameter", simsobj.sims_appl_parameter)
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDTccertificaterequiestUpdate")]
        public HttpResponseMessage CUDTccertificaterequiestUpdate(List<Sims668> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims668 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_certificate_request_tc_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@sims_tc_certificate_reason", simsobj.sims_tc_certificate_reason),
                            new SqlParameter("@sims_tc_certificate_requested_by", simsobj.sims_tc_certificate_requested_by),
                            new SqlParameter("@sims_tc_certificate_request_date",db.DBYYYYMMDDformat(simsobj.sims_tc_certificate_request_date)),
                            new SqlParameter("@sims_tc_certificate_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_tc_certificate_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_tc_certificate_request_number", simsobj.sims_tc_certificate_request_number),

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region API For Vacancy Master...

        [Route("getCompanyDetails")]
        public HttpResponseMessage getCompanyDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "A"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_vacancy_company_code = dr["co_company_code"].ToString();
                            persobj.pays_vacancy_company_nm = dr["co_desc"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getDepartmentDetails")]
        public HttpResponseMessage getDepartmentDetails(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@pays_vacancy_company_code", comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_vacancy_dept_code = dr["codp_dept_no"].ToString();
                            persobj.pays_vacancy_dept_nm = dr["codp_dept_name"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getDesignationDetails")]
        public HttpResponseMessage getDesignationDetails(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@pays_vacancy_company_code", comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_vacancy_profile_desg_code = dr["dg_code"].ToString();
                            persobj.pays_vacancy_profile_desg_name = dr["dg_desc"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getVacancytypeDetails")]
        public HttpResponseMessage getVacancytypeDetails(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "E"),
                            new SqlParameter("@pays_vacancy_company_code", comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_vacancy_type = dr["pays_appl_parameter"].ToString();
                            persobj.pays_vacancy_type_nm = dr["pays_appl_form_field_value1"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getVacancyIdDetails")]
        public HttpResponseMessage getVacancyIdDetails(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@pays_vacancy_company_code", comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_sr_no = dr["seq"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getVacancyLevelDetails")]
        public HttpResponseMessage getVacancyLevelDetails(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@pays_vacancy_company_code", comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 persobj = new Pers319();
                            persobj.pays_vacancy_level = dr["pays_appl_parameter"].ToString();
                            persobj.pays_vacancy_level_nm = dr["pays_appl_form_field_value1"].ToString();
                            my_list.Add(persobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("getVacancyDetails")]
        public HttpResponseMessage getVacancyDetails(string desg, string exp, string date, string todate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Pers319> my_list = new List<Pers319>();
            try
            {

                if (desg == "undefined" || desg == "" || desg == null)
                    desg = null;
                if (exp == "undefined" || exp == "" || exp == null)
                    exp = null;
                if (date == "undefined" || date == "" || date == null)
                    date = null;
                if (todate == "undefined" || todate == "" || todate == null)
                    todate = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@pays_vacancy_profile_desg_code", desg),
                            new SqlParameter("@pays_vacancy_min_experience", exp),
                            new SqlParameter("@pays_vacancy_posting_date", date),
                            new SqlParameter("@pays_vacancy_expiry_date",todate),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers319 Finnobj = new Pers319();

                            Finnobj.pays_sr_no = dr["pays_sr_no"].ToString();
                            Finnobj.pays_vacancy_id = dr["pays_vacancy_id"].ToString();
                            Finnobj.pays_vacancy_profile_desg_code = dr["pays_vacancy_profile_desg_code"].ToString();

                            if (dr["pays_vacancy_status"].ToString().Equals("A"))
                            {
                                Finnobj.pays_vacancy_status = true;
                            }
                            else
                            {
                                Finnobj.pays_vacancy_status = false;
                            }

                            Finnobj.pays_vacancy_desc = dr["pays_vacancy_desc"].ToString();
                            Finnobj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            Finnobj.pays_vacancy_level = dr["pays_vacancy_level"].ToString();
                            Finnobj.pays_vacancy_type = dr["pays_vacancy_type"].ToString();
                            Finnobj.pays_vacancy_min_experience = dr["pays_vacancy_min_experience"].ToString();
                            Finnobj.pays_vacancy_posting_date = dr["pays_vacancy_posting_date"].ToString();
                            Finnobj.pays_vacancy_expiry_date = dr["pays_vacancy_expiry_date"].ToString();
                            Finnobj.pays_vacancy_doc = dr["pays_vacancy_doc"].ToString();
                            Finnobj.pays_vacancy_company_code = dr["pays_vacancy_company_code"].ToString();
                            Finnobj.pays_vacancy_dept_code = dr["pays_vacancy_dept_code"].ToString();

                            Finnobj.pays_vacancy_company_nm = dr["company_name"].ToString();
                            Finnobj.pays_vacancy_dept_nm = dr["dept_name"].ToString();
                            Finnobj.pays_vacancy_profile_desg_nm = dr["designation_name"].ToString();
                            Finnobj.pays_vacancy_type_nm = dr["pays_vacancy_type_nm"].ToString();
                            Finnobj.pays_vacancy_level_nm = dr["pays_vacancy_level_nm"].ToString();

                            my_list.Add(Finnobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, my_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, my_list);

        }

        [Route("CUDVacancyDetails")]
        public HttpResponseMessage CUDVacancyDetails(List<Pers319> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers319 finsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("invs.pays_vacancy_master_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",finsobj.opr),
                            new SqlParameter("@pays_vacancy_id", finsobj.pays_vacancy_id),
                            new SqlParameter("@pays_vacancy_profile_desg_code", finsobj.pays_vacancy_profile_desg_code),
                            new SqlParameter("@pays_vacancy_status",finsobj.pays_vacancy_status==true?"A":"I"),
                            new SqlParameter("@pays_vacancy_desc", finsobj.pays_vacancy_desc),
                            new SqlParameter("@pays_vacancy_roles", finsobj.pays_vacancy_roles),
                            new SqlParameter("@pays_vacancy_level", finsobj.pays_vacancy_level),
                            new SqlParameter("@pays_vacancy_type", finsobj.pays_vacancy_type),
                            new SqlParameter("@pays_vacancy_min_experience", finsobj.pays_vacancy_min_experience),
                            new SqlParameter("@pays_vacancy_posting_date", db.DBYYYYMMDDformat(finsobj.pays_vacancy_posting_date)),
                            new SqlParameter("@pays_vacancy_expiry_date", db.DBYYYYMMDDformat(finsobj.pays_vacancy_expiry_date)),
                            new SqlParameter("@pays_vacancy_doc", finsobj.pays_vacancy_doc),

                            //new SqlParameter("@pays_vacancy_doc", di.Extension),
                            new SqlParameter("@pays_vacancy_company_code", finsobj.pays_vacancy_company_code),
                            new SqlParameter("@pays_vacancy_dept_code", finsobj.pays_vacancy_dept_code),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            string sPath = ""; string fname = ""; string file = ""; string root = "";
            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Docs/Vacancy/";
            root = HttpContext.Current.Server.MapPath(path);
            Directory.CreateDirectory(root);
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;
                    hpf.SaveAs((root + filename));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                }
            }

            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        #endregion

        #region API Print Vouchers...

        [Route("getPrintVouchers")]
        public HttpResponseMessage getPrintVouchers(string com_code, string search, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<Finn226> mod_list = new List<Finn226>();
            try
            {

                if (search == "undefined" || search == "" || search == null)
                    search = null;
                if (from_date == "undefined" || from_date == "" || from_date == null)
                    from_date = null;
                if (to_date == "undefined" || to_date == "" || to_date == null)
                    to_date = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins.fins_print_vouchers_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@comp_code", com_code),
                             new SqlParameter("@search", search),
                             new SqlParameter("@from_date", from_date),
                             new SqlParameter("@to_date", to_date)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn226 finsobj = new Finn226();
                            finsobj.gltd_doc_code = dr["gltd_doc_code"].ToString();
                            finsobj.gltd_final_doc_no = dr["gltd_final_doc_no"].ToString();
                            finsobj.gltd_post_date = dr["gltd_post_date"].ToString();
                            finsobj.pty_name = dr["pty_name"].ToString();
                            finsobj.voucher_amt = dr["voucher_amt"].ToString();
                            mod_list.Add(finsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }


        #endregion

        #region Api For Transport Vehicle Expense...

        [Route("getVehicleName")]
        public HttpResponseMessage getVehicleName()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.fvex_vehicle_name = dr["sims_transport_vehicle_name_plate"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("getVehicleExpanseDetails")]
        public HttpResponseMessage getVehicleExpanseDetails()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_transaction_number = dr["fvex_transaction_number"].ToString();
                            obj.fvex_vehicle_code = dr["fvex_vehicle_code"].ToString();
                            obj.fvex_expense_type = dr["fvex_expense_type"].ToString();
                            obj.fvex_expense_amount = dr["fvex_expense_amount"].ToString();
                            obj.fvex_vechile_expense_date = dr["fvex_vechile_expense_date"].ToString();
                            obj.fvex_dd_cheque_bank_name = dr["fvex_dd_cheque_bank_name"].ToString();
                            obj.fvex_expense_request_no = dr["fvex_expense_request_no"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("getBankName")]
        public HttpResponseMessage getBankName()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_dd_cheque_bank_code = dr["pb_bank_code"].ToString();
                            obj.fvex_dd_cheque_bank_name = dr["pb_bank_name"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("CUDVehicleExpense")]
        public HttpResponseMessage CUDVehicleExpense(List<Fins234> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fins234 simobj in data)
                    {
                        if (simobj.fvex_dd_cheque_number == "undefined")
                        {
                            simobj.fvex_dd_cheque_number = null;
                        }
                        if (simobj.fvex_dd_cheque_due_date == "undefined")
                        {
                            simobj.fvex_dd_cheque_due_date = null;
                        }
                        if (simobj.fvex_dd_cheque_bank_code == "undefined")
                        {
                            simobj.fvex_dd_cheque_bank_code = null;
                        }
                        if (simobj.fvex_cc_number == "undefined")
                        {
                            simobj.fvex_cc_number = null;
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                             new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr",simobj.opr),
                                 new SqlParameter("@fvex_vechile_expense_date",simobj.fvex_vechile_expense_date),
                                 new SqlParameter("@fvex_vehicle_code", simobj.fvex_vehicle_code),
                                 new SqlParameter("@fvex_expense_type",simobj.fvex_expense_type),
                                 new SqlParameter("@fvex_expense_amount", simobj.fvex_expense_amount),
                                 new SqlParameter("@fvex_expense_remark",simobj.fvex_expense_remark),
                                 new SqlParameter("@fvex_expense_creation_user", simobj.fvex_expense_creation_user),
                                 new SqlParameter("@fvex_dd_cheque_number",simobj.fvex_dd_cheque_number),
                                 new SqlParameter("@fvex_dd_cheque_due_date",simobj.fvex_dd_cheque_due_date),
                                 new SqlParameter("@fvex_dd_cheque_bank_code",simobj.fvex_dd_cheque_bank_code),
                                 new SqlParameter("@fvex_cc_number",simobj.fvex_cc_number),

                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        #endregion

        #region Api For Student Promotion Expense...

        [Route("getAllTermDetailspromotion")]
        public HttpResponseMessage getAllTermDetailspromotion(string cur, string a_year, string grades, string section, string advanced_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSusceptibilityDesc(),PARAMETERS ::";


            int i = 0;

            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promotion_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", cur),
                              new SqlParameter("@sims_academic_year", a_year),
                              new SqlParameter("@sims_sims_grades", grades),
                              new SqlParameter("@sims_sims_section",section),
                              new SqlParameter("@sims_advanced_academic_year",advanced_year),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            i = i + 1;
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_name = dr["std_name"].ToString();
                            simsobj.parent_name = dr["parent_name"].ToString();
                            simsobj.pramoted_grade = dr["pramoted_grade"].ToString();
                            simsobj.promote_code = dr["sims_section_promote_code"].ToString();
                            simsobj.grade_names = dr["grade_names"].ToString();
                            simsobj.pramoted_code = dr["pramoted_code"].ToString();

                            string promot_grade = grades + "," + dr["pramoted_grade"].ToString();

                            simsobj.P_class = promot_code(advanced_year, promot_grade, cur);
                            simsobj.datastatus = i.ToString();
                            try
                            {
                                simsobj.adv_flag = dr["adv_flag"].ToString();
                            }catch(Exception e){}
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        public List<pormote_class> promot_code(string avd_year, string grade, string cur)
        {
            List<pormote_class> mod_list = new List<pormote_class>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promotion_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@sims_advanced_academic_year",avd_year),
                              new SqlParameter("@sims_sims_grades",grade),
                              new SqlParameter("@sims_cur_code",cur),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            pormote_class simsobj = new pormote_class();

                            simsobj.promote_code = dr["grade_code"].ToString();
                            simsobj.promote_name = dr["grade_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return mod_list;

            }
            return mod_list;
        }



        [Route("getAcademicYearActive")]
        public HttpResponseMessage getAcademicYearActive()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYearActive(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promotion_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "H"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            simsobj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            simsobj.sims_academic_years = dr["sims_academic_years"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getgradesectionCombo")]
        public HttpResponseMessage getgradesectionCombo(string cur_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYearActive(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promotion_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@sims_academic_year",cur_year),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.grade_code = dr["grade_code"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDPromoteDetails")]
        public HttpResponseMessage CUDPromoteDetails(List<Sims101> data)
        {
            bool inserted = false;
            Message msg = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims101 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_promotion_proc]",
                        new List<SqlParameter>()
                     {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_sims_grades", simsobj.sims_sims_grades),
                             new SqlParameter("@sims_enrollment_number", simsobj.sims_enrollment_number),
                             new SqlParameter("@sims_academic_year_old",simsobj.sims_academic_year_old),
                             new SqlParameter("@user_name",simsobj.user_name),
                     });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        #endregion

        #region API Student Behaviour Target Details...

        [Route("gettargetcode")]
        public HttpResponseMessage gettargetcode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimSTD> mod_list = new List<SimSTD>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimSTD simssobj = new SimSTD();
                            simssobj.sims_target_code = dr["sims_student_behaviour_target_no"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("gettargetcodes")]
        public HttpResponseMessage gettargetcodes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimSTD> mod_list = new List<SimSTD>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimSTD simssobj = new SimSTD();
                            simssobj.sims_target_code = dr["sims_student_behaviour_target_no"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("gettargetcodesdetails")]
        public HttpResponseMessage gettargetcodesdetails(string targetcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimSTD> mod_list = new List<SimSTD>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@sims_student_behaviour_target_no", targetcode)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimSTD simsobj = new SimSTD();
                            simsobj.sims_target_code = dr["sims_student_behaviour_target_no"].ToString();
                            simsobj.sims_target_code_line_no = dr["sims_student_behaviour_target_line_no"].ToString();
                            simsobj.sims_target_desc = dr["sims_student_behaviour_target_desc"].ToString();
                            if (dr["sims_student_behaviour_target_status"].ToString().Equals("A"))
                                simsobj.sims_student_behaviour_target_status = true;
                            else
                                simsobj.sims_student_behaviour_target_status = false;
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDTtargetdetails")]
        public HttpResponseMessage CUDTtargetdetails(string targetcode, List<SimSTD> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                if (!string.IsNullOrEmpty(targetcode))
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'D'),
                            new SqlParameter("@sims_student_behaviour_target_no", targetcode),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }
                        dr.Close();
                        db.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(st) || targetcode == null)
                {
                    st = string.Empty;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (SimSTD simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_student_behaviour_target_no", simsobj.sims_target_code),
                            new SqlParameter("@sims_student_behaviour_target_line_no", simsobj.sims_target_code_line_no),
                            new SqlParameter("@sims_student_behaviour_target_desc", simsobj.sims_target_desc),
                            new SqlParameter("@sims_student_behaviour_target_status", simsobj.sims_student_behaviour_target_status==true?"A":"I"),
                        });

                            if (dr.Read())
                            {
                                st = dr[0].ToString();
                                msg.strMessage = st;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        #endregion

        #region API Student Behaviour Targe...

        [Route("CUDTtargetdetailstudent")]
        public HttpResponseMessage CUDTtargetdetailstudent(string targetcode, string enroll_number, List<SimSTD> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                if (!string.IsNullOrEmpty(targetcode))
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'F'),
                            new SqlParameter("@sims_student_behaviour_target_no", targetcode),
                            new SqlParameter("@sims_student_behaviour_enroll_number", enroll_number),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }
                        dr.Close();
                        db.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(st) || targetcode == null)
                {
                    st = string.Empty;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (SimSTD simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_behaviour_target_details_proc",
                                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_student_behaviour_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_student_behaviour_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_student_behaviour_enroll_number", simsobj.sims_student_enroll_number),
                            new SqlParameter("@sims_student_behaviour_target_no", simsobj.sims_target_code),
                            new SqlParameter("@sims_student_behaviour_target_line_no", simsobj.sims_target_code_line_no),
                            new SqlParameter("@sims_student_behaviour_week_start_date", db.DBYYYYMMDDformat(simsobj.sims_from_date)),
                            new SqlParameter("@sims_student_behaviour_week_end_date", db.DBYYYYMMDDformat(simsobj.sims_upto_date)),
                            new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_target_status==true?"A":"I"),
                            });

                            if (dr.Read())
                            {
                                st = dr[0].ToString();
                                msg.strMessage = st;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("Targetcodesdetailstudent")]
        public HttpResponseMessage Targetcodesdetailstudent(SimSTD obj)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                //SimSTD sf = Newtonsoft.Json.JsonConvert.DeserializeObject<SimSTD>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_student_behaviour_target_details_proc",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@sims_student_behaviour_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_student_behaviour_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_student_behaviour_enroll_number", obj.sims_student_enroll_number),
                                new SqlParameter("@sims_student_behaviour_target_no", obj.sims_target_code),
                                new SqlParameter("@sims_student_behaviour_target_line_no", obj.sims_target_code_line_no),
                                new SqlParameter("@sims_student_behaviour_week_start_date", db.DBYYYYMMDDformat(obj.sims_from_date)),
                                new SqlParameter("@sims_student_behaviour_week_end_date", db.DBYYYYMMDDformat(obj.sims_upto_date)),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        #region Marks Entry


        [Route("getgradesectionComboMarkEntry")]
        public HttpResponseMessage getgradesectionComboMarkEntry(string curCode, string accYear, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYearActive(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_mark_entry_view]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@sims_cur_code",curCode),
                               new SqlParameter("@bell_academic_year",accYear),
                               new SqlParameter("@user_name",user),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getsectionComboMarkEntry")]

        public HttpResponseMessage getsectionComboMarkEntry(string curCode, string accYear, string grade_code, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : sectionComboMarkEntry(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_mark_entry_view]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "T"),
                              new SqlParameter("@sims_cur_code",curCode),
                               new SqlParameter("@bell_academic_year",accYear),
                               new SqlParameter("@user_name",user),
                               new SqlParameter("@bell_grade_code",grade_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();

                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getsubjectComboMarkEntry")]

        public HttpResponseMessage getsubjectComboMarkEntry(string curCode, string accYear, string grade_code, string term, string section)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getsubjectComboMarkEntry(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_mark_entry_view]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "W"),
                              new SqlParameter("@sims_cur_code",curCode),
                               new SqlParameter("@bell_academic_year",accYear),
                               new SqlParameter("@term_code",term),
                               new SqlParameter("@bell_grade_code",grade_code),
                               new SqlParameter("@bell_section_code",section),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims101 simsobj = new Sims101();
                            simsobj.sims_gb_number = dr["sims_gb_number"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getCategoryComboMarkEntry")]
        public HttpResponseMessage getCategoryComboMarkEntry(string curCode, string accYear, string grade_code, string section, string gbnumber)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getsubjectComboMarkEntry(),PARAMETERS ::";


            List<Sims101> mod_list = new List<Sims101>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_mark_entry_view]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "K"),
                              new SqlParameter("@sims_cur_code",curCode),
                               new SqlParameter("@bell_academic_year",accYear),
                               new SqlParameter("@bell_grade_code",grade_code),
                               new SqlParameter("@bell_section_code",section),
                               new SqlParameter("@gb_number",gbnumber),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims101 simsobj = new Sims101();
                            simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        #endregion

        #region API For Complaint Registration...

        [Route("getcomplaintdetails")]
        public HttpResponseMessage getcomplaintdetails(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@user_code", username)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simsobj = new SimCMF();
                            simsobj.sims_complaint_srl_no = dr["sims_complaint_srl_no"].ToString();
                            simsobj.sims_complaint_subject = dr["sims_complaint_subject"].ToString();
                            simsobj.sims_complaint_desc = dr["sims_complaint_desc"].ToString();
                            //if (dr["sims_complaint_status"].ToString().Equals("A"))
                            //    simsobj.sims_complaint_status = true;
                            //else
                            //    simsobj.sims_complaint_status = false;
                            simsobj.sims_complaint_registration_date = db.UIDDMMYYYYformat(dr["sims_complaint_registration_date"].ToString());
                            simsobj.sims_complaint_register_user_code = dr["sims_complaint_register_user_code"].ToString();
                            simsobj.sims_complaint_assigned_to_user_code = dr["sims_complaint_assigned_to_user_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getcomplaintcode")]
        public HttpResponseMessage getcomplaintcode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_complaint_srl_no = dr["sims_complaint_srl_no"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDcomplainttails")]
        public HttpResponseMessage CUDcomplainttails(string complaintcode, List<SimCMF> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                //if (!string.IsNullOrEmpty(targetcode))
                //{
                //    using (DBConnection db = new DBConnection())
                //    {
                //        db.Open();

                //        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                //                new List<SqlParameter>()
                //        {
                //            new SqlParameter("@opr",'D'),
                //            new SqlParameter("@sims_complaint_srl_no", complaintcode),
                //        });

                //        if (dr.Read())
                //        {
                //            st = dr[0].ToString();
                //            msg.strMessage = st;
                //        }
                //        dr.Close();
                //        db.Dispose();
                //    }
                //}

                //if (!string.IsNullOrEmpty(st) || complaintcode == null)
                {
                    st = string.Empty;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (SimCMF simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_complaint_srl_no", simsobj.sims_complaint_srl_no),
                            new SqlParameter("@sims_complaint_subject", simsobj.sims_complaint_subject),
                            new SqlParameter("@sims_complaint_desc", simsobj.sims_complaint_desc),
                            //new SqlParameter("@sims_complaint_status", simsobj.sims_complaint_status==true?"A":"I"),
                            new SqlParameter("@sims_complaint_registration_date", db.DBYYYYMMDDformat(simsobj.sims_complaint_registration_date)),
                            new SqlParameter("@sims_complaint_register_user_code", simsobj.sims_complaint_register_user_code),
                            new SqlParameter("@sims_complaint_assigned_to_user_code", simsobj.sims_complaint_assigned_to_user_code),
                        });

                            if (dr.Read())
                            {
                                st = dr[0].ToString();
                                msg.strMessage = st;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        [Route("getcomplaintdetailsassign")]
        public HttpResponseMessage getcomplaintdetailsassign(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@user_code", username)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simsobj = new SimCMF();
                            simsobj.sims_complaint_srl_no = dr["sims_complaint_srl_no"].ToString();
                            simsobj.sims_complaint_subject = dr["sims_complaint_subject"].ToString();
                            simsobj.sims_complaint_desc = dr["sims_complaint_desc"].ToString();
                            //if (dr["sims_complaint_status"].ToString().Equals("A"))
                            //    simsobj.sims_complaint_status = true;
                            //else
                            //    simsobj.sims_complaint_status = false;
                            simsobj.sims_complaint_registration_date = db.UIDDMMYYYYformat(dr["sims_complaint_registration_date"].ToString());
                            simsobj.sims_complaint_register_user_code = dr["sims_complaint_register_user_code"].ToString();
                            simsobj.sims_complaint_assigned_to_user_code = dr["sims_complaint_assigned_to_user_code"].ToString();
                            simsobj.sims_complaint_status = dr["sims_complaint_status"].ToString();
                            //new SqlParameter("@sims_complaint_status", simsobj.sims_complaint_status==true?"A":"I"),
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }


        [Route("getstatus")]
        public HttpResponseMessage getstatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            List<SimCMF> mod_list = new List<SimCMF>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimCMF simssobj = new SimCMF();
                            simssobj.sims_action_desc = dr["sims_appl_form_field_value1"].ToString();
                            simssobj.sims_action_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simssobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDcomplainttassign")]
        public HttpResponseMessage CUDcomplainttassign(string complaintcode, List<SimCMF> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {

                {
                    st = string.Empty;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (SimCMF simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_complaint_book_proc]",
                                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_complaint_srl_no", simsobj.sims_complaint_srl_no),
                            new SqlParameter("@sims_complaint_subject", simsobj.sims_complaint_subject),
                            new SqlParameter("@sims_complaint_desc", simsobj.sims_complaint_desc),
                            new SqlParameter("@sims_complaint_status", simsobj.sims_action_code),
                            new SqlParameter("@sims_complaint_registration_date",db.DBYYYYMMDDformat( simsobj.sims_complaint_registration_date)),
                            new SqlParameter("@sims_complaint_register_user_code", simsobj.sims_complaint_register_user_code),
                            new SqlParameter("@sims_complaint_assigned_to_user_code", simsobj.sims_complaint_assigned_to_user_code),
                        });

                            if (dr.Read())
                            {
                                st = dr[0].ToString();
                                msg.strMessage = st;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        #endregion

        #region API Student Attendence View...

        [Route("getStudentattendanceview")]
        public HttpResponseMessage getStudentattendanceview(string cur_name, string academic_year, string grd_code, string sec_code, string from_date, string upto_date)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;

            List<view_student_attendance> mod_list = new List<view_student_attendance>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (upto_date == "undefined" || upto_date == "" || upto_date == null)
                        upto_date = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_view_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_start_date",  db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@sims_end_date", db.DBYYYYMMDDformat(upto_date)),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            view_student_attendance obj = new view_student_attendance();
                            string str = dr["sims_enrollment_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.section = dr["section"].ToString();
                            obj.studentName = dr["studentName"].ToString();
                            obj.att_details = new List<attendance_details>();

                            attendance_details obj1 = new attendance_details();
                            obj1.present = dr["present"].ToString();
                            obj1.absent = dr["absent"].ToString();
                            obj1.absent_excused = dr["absent_excused"].ToString();
                            obj1.Tardy = dr["Tardy"].ToString();
                            obj1.Unmarked = dr["Unmarked"].ToString();
                            obj1.total = dr["total"].ToString();
                            obj1.TotalAttendanceDays = dr["TotalAttendanceDays"].ToString();
                            obj1.Percentage = dr["Percentage"].ToString();
                            obj1.Month_No = dr["Month_No"].ToString();
                            obj1.month_name = dr["month_name"].ToString();

                            var v = from p in mod_list where p.sims_enrollment_number == str select p;
                            if (v.Count() > 0)
                            {
                                v.ElementAt(0).att_details.Add(obj1);
                            }
                            else
                            {
                                mod_list.Add(obj);
                                obj.att_details.Add(obj1);
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        #endregion

        #region API For Admission Promote Remark...

        [Route("getAdmRemarkStudList")]
        public HttpResponseMessage getAdmRemarkStudList(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Sims176 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims176>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_student_promote_remark_proc]",
                    new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@sims_cur_code", sf.cur_shrt_name),
                                new SqlParameter("@sims_academic_year", sf.sims_academic_year),
                                new SqlParameter("@sims_grade_code", sf.sims_grade_code),
                                new SqlParameter("@sims_section_code", sf.sims_section_code),
                                new SqlParameter("@sims_enroll_number",  sf.sims_student_enroll_number),

                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);

        }

        [Route("getAdmRemarkStudList_ABQIS")]
        public HttpResponseMessage getAdmRemarkStudList_ABQIS(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Sims176 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims176>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_student_promote_remark_proc]",
                    new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "SS"),
                                new SqlParameter("@sims_cur_code", sf.cur_shrt_name),
                                new SqlParameter("@sims_academic_year", sf.sims_academic_year),
                                new SqlParameter("@sims_enroll_number",  sf.sims_student_enroll_number),

                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getRegistrationStatus")]
        public HttpResponseMessage getRegistrationStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promote_remark_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_student_attribute1 = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getFeedescStatus")]
        public HttpResponseMessage getFeedescStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promote_remark_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "F"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_student_attribute3 = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDAdmissionstudentpro")]
        public HttpResponseMessage CUDAdmissionstudentpro(List<Sims176> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims176 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_promote_remark_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'U'),
                            new SqlParameter("@sims_cur_code", simsobj.cur_shrt_name),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_student_enroll_number),
                            new SqlParameter("@sims_remark", simsobj.sims_student_attribute5),
                            new SqlParameter("@sims_status", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_fee_paid", simsobj.sims_student_attribute3),

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("CUDAdmissionstudentpro_ABQIS")]
        public HttpResponseMessage CUDAdmissionstudentpro_ABQIS(List<Sims176> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims176 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_promote_remark_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","UU"),
                            new SqlParameter("@sims_cur_code", simsobj.cur_shrt_name),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_student_enroll_number),
                            new SqlParameter("@sims_remark", simsobj.sims_student_attribute5),
                            new SqlParameter("@sims_status", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_fee_paid", simsobj.sims_student_attribute3),

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #endregion

        #region API For Transport Invoice...

        [Route("GetAllCurName")]
        public HttpResponseMessage GetAllCurName()
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllAcademicYears")]
        public HttpResponseMessage GetAllAcademicYears(string cur_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_academic_year_start_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            objNew.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            objNew.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllGrades")]
        public HttpResponseMessage GetAllGrades(string cur_code, string ac_year)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllSections")]
        public HttpResponseMessage GetAllSections(string cur_code, string ac_year, string g_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                                new SqlParameter("@sims_g_code", g_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("Getsims541_InvoiceReportUrl")]
        public HttpResponseMessage Getsims541_InvoiceReportUrl()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            string url = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "U")
                            });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            url = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, url);

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, url); }
        }

        [Route("Getsims541_InvoiceMode")]
        public HttpResponseMessage Getsims541_InvoiceMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            List<sims541> coll = new List<sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "M")
                            });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_invoce_mode = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_invoce_mode_code = dr["sims_appl_parameter"].ToString();
                            coll.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, coll);

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
        }

        [Route("Getsims541_FrequencyValue")]
        public HttpResponseMessage Getsims541_FrequencyValue(string cur_code, string academic_year, string freq_flag)
        {
            List<sims541> coll = new List<sims541>();
            try
            {
                // if (freq_flag.ToUpper() == "T")//Get Terms
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                            new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "T"),
                                   new SqlParameter("@cur_code", cur_code),
                                   new SqlParameter("@academic_year", academic_year)
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                sims541 obj = new sims541();
                                obj.sims_invoce_frq_name = dr["sims_fee_term_desc_en"].ToString();
                                obj.sims_invoce_frq_name_value = dr["sims_fee_term_code"].ToString();
                                coll.Add(obj);
                            }
                        }
                    }

                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("Getsims541_FeeTypes")]
        public HttpResponseMessage Getsims541_FeeTypes(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                        new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "F"),
                                   new SqlParameter("@cur_code", cur_code),
                                   new SqlParameter("@academic_year", academic_year),
                                   new SqlParameter("@grade_code", grade_code),
                                   new SqlParameter("@section_code", section_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_fee_code = dr["sims_fee_code"].ToString();
                            obj.sims_fee_code_desc = dr["sims_fee_code_description"].ToString();
                            coll.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("Getsims541_InvoiceStudents")]
        public HttpResponseMessage Getsims541_InvoiceStudents(string cur_code, string academic_year, string grade_code, string section_code, string term_code, string search, string fee_code_search, string isPayingAgent = null, string acc_ctl = null, string acc_no = null)
        {
            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                        new List<SqlParameter>() 
                            { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@academic_year", academic_year),
                            new SqlParameter("@grade_code",string.IsNullOrEmpty(grade_code)?null:grade_code ),
                            new SqlParameter("@section_code",string.IsNullOrEmpty(section_code)?null:section_code),
                            new SqlParameter("@isPayingAgent", isPayingAgent),
                            new SqlParameter("@slma_ldgrctl_code", acc_ctl),
                            new SqlParameter("@slma_acno", acc_no),
                            new SqlParameter("@term_code", term_code),
                            new SqlParameter("@search", string.IsNullOrEmpty(search)?null:search) ,
                            new SqlParameter("@fee_code_search", string.IsNullOrEmpty(fee_code_search)?null:fee_code_search),
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_name = dr["StudentName"].ToString();
                            obj.sims_parent_name = dr["ParentName"].ToString();
                            obj.sims_student_class = dr["Class"].ToString();
                            obj.sims_paid_amount = "0";
                            obj.sims_invoce_mode = dr["invoice_mode"].ToString();
                            obj.sims_period_code = dr["period_code"].ToString();
                            obj.sims_invoice_status = false;
                            if (dr["Invoice_Status"].ToString() == "Y")
                                obj.sims_invoice_student_status = true;
                            else
                                obj.sims_invoice_student_status = false;
                            coll.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("GenerateInvoiceForStudents")]
        public HttpResponseMessage GenerateInvoiceForStudents(string term_code, string invoice_mode, string payingAgentFlag, string fee_code_search, List<sims541> temp)
        {
            var obj = (from x in temp where x.sims_invoice_status == true && x.sims_invoice_student_status == false select x).ToList();
            for (int i = 0; i < obj.Count; i++)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                                new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", payingAgentFlag.ToUpper() == "Y"?"X":"I"),
                                new SqlParameter("@cur_code", obj[i].sims_cur_code),
                                new SqlParameter("@academic_year", obj[i].sims_academic_year),
                                new SqlParameter("@grade_code", obj[i].sims_grade_code),
                                new SqlParameter("@section_code", obj[i].sims_section_code),
                                new SqlParameter("@isPayingAgent", payingAgentFlag),
                                new SqlParameter("@term_code", term_code),
                                new SqlParameter("@invoice_mode", invoice_mode),
                                new SqlParameter("@enroll_number", obj[i].sims_enroll_number),
                                new SqlParameter("@fee_code_search", string.IsNullOrEmpty(fee_code_search)?null:fee_code_search)
                            });

                        int r = dr.RecordsAffected;
                    }
                }
                catch (Exception x)
                {
                    { return Request.CreateResponse(HttpStatusCode.OK, false); }

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("SearchInvoice")]
        public HttpResponseMessage SearchInvoice(string search_code, string cur_code, string academic_year, string grade_code, string section_code, string fee_code_search)// sims541 obj)
        {
            search_code = string.IsNullOrEmpty(search_code) ? null : search_code;
            cur_code = string.IsNullOrEmpty(cur_code) ? null : cur_code;
            academic_year = string.IsNullOrEmpty(academic_year) ? null : academic_year;
            grade_code = string.IsNullOrEmpty(grade_code) ? null : grade_code;
            section_code = string.IsNullOrEmpty(section_code) ? null : section_code;

            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_details_proc",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "S"),
                                    new SqlParameter("@Search", search_code),
                                    new SqlParameter("@start_date", null),
                                    new SqlParameter("@end_date", null),
                                    new SqlParameter("@cur_code", cur_code),
                                    new SqlParameter("@academic_year", academic_year),
                                    new SqlParameter("@grade_code", grade_code),
                                    new SqlParameter("@section_code", section_code),
                                    new SqlParameter("@fee_code_search", string.IsNullOrEmpty(fee_code_search)?null:fee_code_search)
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 t_obj = new sims541();
                            t_obj.in_no = dr["in_no"].ToString();
                            //t_obj.in_date = DateTime.Parse(dr["in_date"].ToString()).ToShortDateString();
                            t_obj.in_date = db.UIDDMMYYYYformat(dr["in_date"].ToString());
                            t_obj.in_status = dr["in_status"].ToString();
                            t_obj.in_status_code = dr["InStatus"].ToString();
                            t_obj.sims_invoice_amount = dr["in_total_amount"].ToString();
                            t_obj.sims_enroll_number = dr["enroll_number"].ToString();
                            t_obj.sims_student_name = string.Format("{0}-{1}", t_obj.sims_enroll_number, dr["StudentName"].ToString());
                            t_obj.sims_parent_name = dr["Father_Name"].ToString();
                            t_obj.sims_academic_year = dr["in_academic_year"].ToString(); // obj.sims_academic_year;
                            t_obj.sims_cur_code = dr["sims_cur_code"].ToString(); //obj.sims_cur_code;
                            t_obj.sims_grade_code = dr["sims_grade_code"].ToString(); //obj.sims_grade_code;
                            t_obj.sims_section_code = dr["sims_section_code"].ToString(); //obj.sims_section_code;
                            t_obj.sims_period_code = dr["in_reference_no"].ToString();
                            t_obj.sims_invoce_mode = dr["Invoice_mode"].ToString();
                            t_obj.sims_student_class = dr["Class"].ToString();
                            t_obj.sims_invoice_status = false;
                            coll.Add(t_obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("updateInvoices")]
        public HttpResponseMessage updateInvoices(List<string> invoice_nos)
        {
            string invoicenos = string.Empty;
            if (invoice_nos.Count > 0)
                invoicenos = string.Join(",", invoice_nos);

            try
            {
                foreach (string x in invoice_nos)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_details_proc",
                                new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "U"),
                                    new SqlParameter("@Search", "5"),
                                    new SqlParameter("@inv_no", x.ToString())
                            });
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.NotModified, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        #region

        /*
        [Route("getStudent_Fees")]
        public HttpResponseMessage GetAllStudentFee(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            simsobj.is_parent_employee = dr["is_parent_employee"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_fee_defined = dr["is_adv_fee_defined"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_section_defined = dr["is_adv_section_defined"].ToString().Equals("A") ? true : false;
                            simsobj.adv_fee_year = dr["adv_fee_year"].ToString();
                            simsobj.is_adv_grade = dr["is_adv_grade"].ToString();
                            simsobj.is_adv_section = dr["is_adv_section"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        */

        /*
        //Get Student Fee - Transport Fee //
        [Route("getStudentFeesTransport")]
        public HttpResponseMessage getStudentFeesTransport(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_transport",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            simsobj.is_parent_employee = dr["is_parent_employee"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_fee_defined = dr["is_adv_fee_defined"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_section_defined = dr["is_adv_section_defined"].ToString().Equals("A") ? true : false;
                            simsobj.adv_fee_year = dr["adv_fee_year"].ToString();
                            simsobj.is_adv_grade = dr["is_adv_grade"].ToString();
                            simsobj.is_adv_section = dr["is_adv_section"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        */

        /*
        [Route("DefineAdvFee")]
        public HttpResponseMessage DefineAdvFee(string cur_code, string ay, string grade, string section, string enroll)
        {
            string result = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "AF"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@search_text", enroll),
                                new SqlParameter("@grade_code", grade),
                                new SqlParameter("@section_code", section)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = dr["adv_status"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }
        */

        /*
        //Transport Fees//
        [Route("getStudent_Transport_Fees")]
        public HttpResponseMessage GetAllStudentTransportFee(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_transport",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        */

        /*
        [Route("GetPreviousReceipts")]
        public HttpResponseMessage GetPreviousReceipts(string ay, string enroll)
        {
            ArrayList coll = new ArrayList();
            List<sims043_previous> mod_list = new List<sims043_previous>();
            List<sims043_previous> dist_docs = new List<sims043_previous>();
            coll.Add(mod_list);
            coll.Add(dist_docs);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_preview_fee_receipt_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@enroll", enroll)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.Receipt_Type = dr["Receipt_Type"].ToString();
                            simsobj.doc_status_code = dr["doc_status_code"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.dd_fee_amount = dr["dd_fee_amount"].ToString();
                            simsobj.total_amount = dr["total_amount"].ToString();
                            //simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            //simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());

                            simsobj.creation_date = (dr["creation_date"].ToString());
                            simsobj.doc_date = (dr["doc_date"].ToString());
                            simsobj.creation_user = dr["creation_user"].ToString();
                            if (dr["doc_status_code"].Equals("3"))
                            {
                                simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                            }
                            else
                            {
                                simsobj.dd_fee_amount_discounted = dr["dd_fee_amount_discounted"].ToString();
                                simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            }
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.ChequeStatus_Desc = dr["ChequeStatus_Desc"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;


                            mod_list.Add(simsobj);
                        }
                        foreach (sims043_previous a in mod_list)
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = a.doc_no;
                            simsobj.Receipt_Type = a.Receipt_Type;
                            simsobj.doc_status_code = a.doc_status_code;
                            simsobj.doc_status = a.doc_status;
                            //simsobj.creation_date = db.UIDDMMYYYYformat(a.creation_date.ToString());
                            //simsobj.doc_date = db.UIDDMMYYYYformat(a.doc_date.ToString());

                            simsobj.creation_date = (a.creation_date.ToString());
                            simsobj.doc_date = (a.doc_date.ToString());
                            //  simsobj.total_amount = mod_list.Where(xx => xx.doc_no == a.doc_no).Sum(xx => decimal.Parse(xx.total_amount)).ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;
                            int cnt = dist_docs.Where(xx => xx.doc_no == simsobj.doc_no).Count();
                            if (cnt == 0)
                                dist_docs.Add(simsobj);
                        }
                        foreach (sims043_previous simsobj in dist_docs)
                        {
                            simsobj.total_amount = mod_list.Where(xx => xx.doc_no == simsobj.doc_no).Sum(xx => decimal.Parse(xx.dd_fee_amount_final)).ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, coll);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
        }
        */

        /*
        //GetPreviousDetailsFor Transport//
        [Route("GetTransportPreviousReceipts")]
        public HttpResponseMessage GetTransportPreviousReceipts(string ay, string enroll)
        {
            ArrayList coll = new ArrayList();
            List<sims043_previous> mod_list = new List<sims043_previous>();
            List<sims043_previous> dist_docs = new List<sims043_previous>();
            coll.Add(mod_list);
            coll.Add(dist_docs);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_transport_fee_TRA1",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "PD"),
                                new SqlParameter("@ACADEMIC", ay),
                                new SqlParameter("@ENROLL", enroll)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.Receipt_Type = dr["Receipt_Type"].ToString();
                            simsobj.doc_status_code = dr["doc_status_code"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.dd_fee_amount = dr["dd_fee_amount"].ToString();
                            simsobj.total_amount = dr["total_amount"].ToString();
                            simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());

                            simsobj.creation_user = dr["creation_user"].ToString();
                            if (dr["doc_status_code"].Equals("3"))
                            {
                                simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                            }
                            else
                            {
                                simsobj.dd_fee_amount_discounted = dr["dd_fee_amount_discounted"].ToString();
                                simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            }
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.ChequeStatus_Desc = dr["ChequeStatus_Desc"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;


                            mod_list.Add(simsobj);
                        }
                        foreach (sims043_previous a in mod_list)
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = a.doc_no;
                            simsobj.Receipt_Type = a.Receipt_Type;
                            simsobj.doc_status_code = a.doc_status_code;
                            simsobj.doc_status = a.doc_status;
                            simsobj.creation_date = db.UIDDMMYYYYformat(a.creation_date.ToString());
                            simsobj.doc_date = db.UIDDMMYYYYformat(a.doc_date.ToString());
                            //  simsobj.total_amount = mod_list.Where(xx => xx.doc_no == a.doc_no).Sum(xx => decimal.Parse(xx.total_amount)).ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;

                            int cnt = dist_docs.Where(xx => xx.doc_no == simsobj.doc_no).Count();
                            if (cnt == 0)
                                dist_docs.Add(simsobj);
                        }
                        foreach (sims043_previous simsobj in dist_docs)
                        {
                            simsobj.total_amount = mod_list.Where(xx => xx.doc_no == simsobj.doc_no).Sum(xx => decimal.Parse(xx.dd_fee_amount_final)).ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, coll);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
        }
        */

        /*
        [Route("getStudent_FeesPA")]
        public HttpResponseMessage getStudent_FeesPA(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_PA",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Acative";
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        */

        /*
        [Route("getSeachOptions")]
        public HttpResponseMessage getSeachOptions()
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'O'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 objNew = new sims043();
                            objNew.sims_search_code = dr["sims_appl_parameter"].ToString();
                            objNew.sims_search_code_name = dr["sims_appl_form_field_value1"].ToString();
                            objNew.sims_search_code_status = dr["sims_appl_form_field_value3"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        */


        /*
        [Route("Getsims541_CreationOnFee")]
        public HttpResponseMessage Getsims541_CreationOnFee()
        {
            string data = string.Empty;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_invoice_document_proc",
                        new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "C")
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        data = dr["sims_appl_parameter"].ToString();
                    }
                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, data); }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        */

        #endregion

        #endregion

        #region API For Student Result Upload...

        [Route("getReprotcardlevel")]
        public HttpResponseMessage getReprotcardlevel(string cur_name, string academic_year, string grd_code, string sec_code)
        {
            List<Sim508> mod_list = new List<Sim508>();
            try
            {
                if (cur_name == "undefined" || cur_name == "")
                    cur_name = null;
                if (academic_year == "undefined" || academic_year == "")
                    academic_year = null;
                if (grd_code == "undefined" || grd_code == "")
                    grd_code = null;
                if (sec_code == "undefined" || sec_code == "")
                    sec_code = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_pdf_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@sims_report_cur_code", cur_name),
                            new SqlParameter("@sims_report_academic_year", academic_year),
                            new SqlParameter("@sims_report_grade_code", grd_code),
                            new SqlParameter("@sims_report_section_code",sec_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 simsobj = new Sim508();
                            simsobj.sims_assignment_term_code = dr["sims_level_code"].ToString();
                            simsobj.sims_assignment_title = dr["sims_report_card_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getReprotcardcode")]
        public HttpResponseMessage getReprotcardcode(string cur_name, string academic_year, string grd_code, string sec_code, string report_levelcode)
        {
            List<Sim508> mod_list = new List<Sim508>();
            try
            {
                if (cur_name == "undefined" || cur_name == "")
                    cur_name = null;
                if (academic_year == "undefined" || academic_year == "")
                    academic_year = null;
                if (grd_code == "undefined" || grd_code == "")
                    grd_code = null;
                if (sec_code == "undefined" || sec_code == "")
                    sec_code = null;
                if (report_levelcode == "undefined" || report_levelcode == "")
                    report_levelcode = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_pdf_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@sims_report_cur_code", cur_name),
                            new SqlParameter("@sims_report_academic_year", academic_year),
                            new SqlParameter("@sims_report_grade_code", grd_code),
                            new SqlParameter("@sims_report_section_code",sec_code),
                            new SqlParameter("@sims_report_level_code",report_levelcode),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 simsobj = new Sim508();
                            simsobj.sims_gb_cat_code = dr["sims_report_card_code"].ToString();
                            simsobj.sims_gb_number = dr["sims_report_card_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getStudentReportCard")]
        public HttpResponseMessage getStudentReportCard(string cur_name, string academic_year, string grd_code, string sec_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sim508> mod_list = new List<Sim508>();
            try
            {
                if (cur_name == "undefined" || cur_name == "")
                    cur_name = null;
                if (academic_year == "undefined" || academic_year == "")
                    academic_year = null;
                if (grd_code == "undefined" || grd_code == "")
                    grd_code = null;
                if (sec_code == "undefined" || sec_code == "")
                    sec_code = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_pdf_proc",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_report_cur_code", cur_name),
                            new SqlParameter("@sims_report_academic_year", academic_year),
                            new SqlParameter("@sims_report_grade_code", grd_code),
                            new SqlParameter("@sims_report_section_code",sec_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 simsobj = new Sim508();

                            if (dr["sims_report_status"].ToString().Equals("A"))
                            {
                                simsobj.sims_assignment_status = true;
                            }
                            else
                            {
                                simsobj.sims_assignment_status = false;
                            }

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_student_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["student_full_Name"].ToString();
                            simsobj.sims_assignment_doc_path_name = dr["sims_report_filename"].ToString();
                            simsobj.sims_assignment_desc = dr["sims_report_level_code"].ToString();
                            simsobj.sims_assignment_doc_line_no = dr["sims_report_card_code"].ToString();
                            simsobj.sims_assignment_number = dr["sims_report_number"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDResultUpload")]
        public HttpResponseMessage CUDResultUpload(List<Sim508> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim508 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_report_pdf_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_report_number", simsobj.sims_assignment_number),
                            new SqlParameter("@sims_report_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_report_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_report_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_report_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_report_level_code", simsobj.sims_assignment_term_code),
                            new SqlParameter("@sims_report_card_code", simsobj.sims_gb_cat_code),
                            new SqlParameter("@sims_report_enroll_number", simsobj.sims_student_enroll_number),
                            new SqlParameter("@sims_report_filename", simsobj.sims_assignment_doc_path),
                            new SqlParameter("@sims_report_status",simsobj.sims_assignment_status==true?"A":"I"),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [HttpPost(), Route("upload712")]
        public string UploadFiles712(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/ResultUpload/";

            root = HttpContext.Current.Server.MapPath(path);
            //$("#pdfAttendace").attr('src', 'https://api.mograsys.com/kindoapi/Content/'+$http.defaults.headers.common['schoolId']+'/ResultUpload/' + res.data);

            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        #endregion


        #region AdmissionStatus
        [Route("AdmissionStatus")]
        public HttpResponseMessage AdmissionStatus()
        {
            List<admission_report> lstCuriculum = new List<admission_report>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Z"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admission_report sequence = new admission_report();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }
        #endregion
        [Route("getPendingAmount")]
        public HttpResponseMessage getPendingAmount(string sims_cur_code, string sims_academic_year, string sims_grade_code, string sims_section_code, string sims_enroll_number)
        {
           
            string return_message = string.Empty;
            Message message = new Message();
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_promotion_proc]",
                            new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "A"),
                              new SqlParameter("@sims_cur_code", sims_cur_code),
                              new SqlParameter("@sims_academic_year", sims_academic_year),
                              new SqlParameter("@sims_sims_grades", sims_grade_code),
                              new SqlParameter("@sims_sims_section",sims_section_code),
                              new SqlParameter("@sims_enrollment_number",sims_enroll_number)
                         });

                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                return_message = dr["return_message"].ToString();
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, return_message);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, return_message);
                        }
                    }
                
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, return_message);

            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, return_message);
        }


        [Route("getstudentStatus")]
        public HttpResponseMessage getstudentStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getstudentStatus(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<stdrpt> mod_list = new List<stdrpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "R")
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            stdrpt simsobj = new stdrpt();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }


        [Route("getStudentReportStatus")]
        public HttpResponseMessage getStudentReportStatus(string cur_name, string academic_year, string grd_code, string sec_code, string student_en, string sims_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            List<Sims176> mod_list = new List<Sims176>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (student_en == "undefined" || student_en == "" || student_en == null)
                        student_en = null;
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (sec_code == "undefined" || sec_code == "" || sec_code == null)
                        sec_code = null;
                    if (sims_status == "undefined" || sims_status == "" || sims_status == null)
                        sims_status = null;
                     

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_report_forall_school_proc]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@sims_cur_code", cur_name),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grd_code),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_student_enroll_number", student_en),
                            new SqlParameter("@sims_status",sims_status),
                             
                        });

                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Sims176 simsobj = new Sims176();
                            simsobj.sims_blood_group = dr["sims_blood_group"].ToString();
                            simsobj.cur_shrt_name = dr["cur_shrt_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();

                            simsobj.sims_student_main_language_name = dr["sims_student_main_language_name"].ToString();
                            simsobj.sims_health_detail = dr["health_detail"].ToString();

                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_bus_name = dr["sims_transport_bus_name"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_student_commence_date = dr["sims_student_commence_date"].ToString();

                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();

                            try
                            {
                                simsobj.sims_student_status = dr["sims_student_status"].ToString();
                            }
                            catch (Exception x)
                            {
                            }
                            try
                            {
                                simsobj.sims_student_enroll_number1 = int.Parse(new String(simsobj.sims_student_enroll_number.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            try
                            {
                                simsobj.stu_roll = dr["sims_roll_number"].ToString();

                                simsobj.stu_roll1 = int.Parse(new String(simsobj.stu_roll.Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex) { }

                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.student_name = simsobj.sims_student_passport_first_name_en + " " + simsobj.sims_student_passport_middle_name_en + " " + simsobj.sims_student_passport_last_name_en;
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_en_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_en_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.student_name_ = dr["student_name_"].ToString();
                            simsobj.sims_student_gender = dr["gender"].ToString();
                            simsobj.sims_student_date = dr["sims_student_date"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_name"].ToString();
                            simsobj.sims_student_admission_academic_year = dr["sims_student_admission_academic_year"].ToString();
                            simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                            simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                            simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                            simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                            simsobj.sims_student_visa_issue_date = dr["sims_student_visa_issue_date"].ToString();
                            simsobj.sims_student_visa_expiry_date = dr["sims_student_visa_expiry_date"].ToString();
                            simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simsobj.sims_student_national_id_issue_date = dr["sims_student_national_id_issue_date"].ToString();
                            simsobj.sims_student_national_id_expiry_date = dr["sims_student_national_id_expiry_date"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_student_ea_registration_date = dr["sims_student_ea_registration_date"].ToString();
                            simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            simsobj.sims_student_ea_transfer_status = dr["sims_student_ea_transfer_status"].ToString();
                            simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                            simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                            simsobj.sims_student_age = dr["sims_student_age"].ToString();
                            simsobj.sims_sibling_student_number = dr["sims_sibling_student_number"].ToString();
                            simsobj.sims_student_passport_issue_date = dr["sims_student_passport_issue_date"].ToString();
                            simsobj.sims_student_passport_expiry_date = dr["sims_student_passport_expiry_date"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();

                            simsobj.Student_Admission_Status = dr["Student_Admission_Status"].ToString();
                            simsobj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();

                            try
                            {

                                simsobj.comn_user_app_status = dr["comn_user_app_status"].ToString();
                                simsobj.comn_user_device_status = dr["comn_user_device_status"].ToString();
                                simsobj.comn_user_last_login = dr["comn_user_last_login"].ToString();
                                simsobj.student_birth_country = dr["student_birth_country"].ToString();
                                simsobj.sims_student_admitted_in_class = dr["sims_student_admitted_in_class"].ToString();
                                simsobj.sims_roll_number = dr["sims_roll_number"].ToString();
                                simsobj.sims_student_dob1 = dr["sims_student_dob1"].ToString();


                            }
                            catch (Exception e)
                            {
                            }

                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.father_nationality1 = dr["father_nationality1"].ToString();
                            simsobj.father_nationality2 = dr["father_nationality2"].ToString();
                            simsobj.sims_parent_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                            simsobj.sims_parent_father_occupation = dr["sims_parent_father_occupation"].ToString();
                            simsobj.sims_parent_father_company = dr["sims_parent_father_company"].ToString();
                            simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_parent_father_phone = dr["sims_parent_father_phone"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();

                            simsobj.sims_parent_father_street_number = dr["sims_parent_father_street_number"].ToString();
                            simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                            simsobj.sims_parent_father_state = dr["sims_parent_father_state"].ToString();
                            simsobj.sims_parent_father_city = dr["sims_parent_father_city"].ToString();


                            simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.sims_parent_father_fax = dr["sims_parent_father_fax"].ToString();
                            simsobj.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.sims_parent_father_national_id__expiry_date = dr["sims_parent_father_national_id__expiry_date"].ToString();
                            simsobj.sims_parent_father_passport_expiry_date = dr["sims_parent_father_passport_expiry_date"].ToString();

                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.mother_nationality1 = dr["mother_nationality1"].ToString();
                            simsobj.mother_nationality2 = dr["mother_nationality2"].ToString();
                            simsobj.sims_parent_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                            simsobj.sims_parent_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                            simsobj.sims_parent_mother_company = dr["sims_parent_mother_company"].ToString();
                            simsobj.sims_parent_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_phone = dr["sims_parent_mother_phone"].ToString();

                            simsobj.sims_parent_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                            simsobj.sims_parent_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                            simsobj.sims_parent_mother_state = dr["sims_parent_mother_state"].ToString();
                            simsobj.sims_parent_mother_city = dr["sims_parent_mother_city"].ToString();


                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.sims_parent_mother_fax = dr["sims_parent_mother_fax"].ToString();
                            simsobj.sims_parent_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                            simsobj.sims_parent_mother_passport_expiry_date = dr["sims_parent_mother_passport_expiry_date"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.sims_parent_mother_national_id_expiry_date = dr["sims_parent_mother_national_id_expiry_date"].ToString();

                            simsobj.guardian_name = dr["guardian_name"].ToString();
                            simsobj.guardian_nationality1 = dr["guardian_nationality1"].ToString();
                            simsobj.guardian_nationality2 = dr["guardian_nationality2"].ToString();
                            simsobj.sims_parent_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                            simsobj.sims_parent_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                            simsobj.sims_parent_guardian_company = dr["sims_parent_guardian_company"].ToString();
                            simsobj.sims_parent_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                            simsobj.sims_parent_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.sims_parent_guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.sims_parent_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                            simsobj.sims_parent_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                            simsobj.sims_parent_guardian_passport_expiry_date2 = dr["sims_parent_guardian_passport_expiry_date2"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.sims_parent_guardian_national_id_expiry_date = dr["sims_parent_guardian_national_id_expiry_date"].ToString();
                            try
                            {
                                simsobj.sims_student_mother_language_name = dr["sims_student_mother_language_name"].ToString();
                            }
                            catch (Exception exx)
                            {
                            }
                            try
                            {
                                simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                                simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                                simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                                simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                                simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                                simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                                simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                                simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                                simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                                simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                                simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            simsobj.Sr_no = i.ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDContactUpdate")]
        public HttpResponseMessage CUDContactUpdate(List<Sims176> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
           
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims176 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_promote_remark_proc",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","T"),
                            new SqlParameter("@sims_father_mobile_no", simsobj.sims_parent_father_mobile),
                             new SqlParameter("@sims_mother_mobile_no", simsobj.sims_parent_mother_mobile),
                             new SqlParameter("@sims_guardian_mobile_no", simsobj.sims_parent_guardian_mobile),
                             new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
    }
}