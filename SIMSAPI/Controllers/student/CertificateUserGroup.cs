﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/CertificateUserGroup")]
    public class CertificateUserGroupController : ApiController
    {


        public string section_term_section_code { get; set; }private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getCurriculumName")]
        public HttpResponseMessage getCurriculumName()
        {
            List<Sim110> group_list = new List<Sim110>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_user_group_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim110 invsobj = new Sim110();
                            invsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            invsobj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            group_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        [Route("getCertificateName")]
        public HttpResponseMessage getCertificateName()
        {
            List<Sim110> group_list = new List<Sim110>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_user_group_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "B"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim110 invsobj = new Sim110();
                            invsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            invsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            group_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        [Route("getUserGroupName")]
        public HttpResponseMessage getUserGroupName()
        {
            List<Sim110> group_list = new List<Sim110>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_user_group_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim110 invsobj = new Sim110();
                            invsobj.sims_user_group_code = dr["comn_user_group_code"].ToString();
                            invsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            group_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }




        [Route("getCertificateUserGroup")]
        public HttpResponseMessage getCertificateUserGroup()
        {
            List<Sim110> group_list = new List<Sim110>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_user_group_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim110 simsCrti = new Sim110();

                            simsCrti.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsCrti.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            simsCrti.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsCrti.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsCrti.sims_user_group_code = dr["sims_user_group_code"].ToString();
                            simsCrti.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            simsCrti.sims_user_group_enable = dr["sims_user_group_enable"].ToString().Equals("A") ? true : false;

                            group_list.Add(simsCrti);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        // This is used for the CRUD operetions WebApi

        [Route("CUDCertificateUserGroup")]
        public HttpResponseMessage CUDCertificateUserGroup(List<Sim110> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim110 insert_invs in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_user_group_proc]",
                            new List<SqlParameter>()
                         {

                           new SqlParameter("@opr", insert_invs.opr),

                           new SqlParameter("@sims_cur_code", insert_invs.sims_cur_code),
                           new SqlParameter("@sims_certificate_number", insert_invs.sims_certificate_number),
                           new SqlParameter("@sims_user_group_code", insert_invs.sims_user_group_code),
                           new SqlParameter("@sims_user_group_enable", insert_invs.sims_user_group_enable==true?"A":"I")
                         
                           //simsobj.sims_level_status ? "A" : "I")

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}

