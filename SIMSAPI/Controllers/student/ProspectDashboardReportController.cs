﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/ProspectDashboardReportController")]
    public class ProspectDashboardReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [HttpGet]
        [Route("getProspecDashboardReport")]
        public HttpResponseMessage getProspecDashboardReport(string sims_pros_number,string from_date,string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProductCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getProductCode"));

            List<prospect> goaltarget_list = new List<prospect>();
            if(string.IsNullOrEmpty(sims_pros_number)|| sims_pros_number == "undefined")
            {
                sims_pros_number = null;
            }
            if (string.IsNullOrEmpty(from_date) || from_date == "undefined")
            {
                from_date = null;
            }
            if (string.IsNullOrEmpty(to_date) || to_date == "undefined")
            {
                to_date = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Prospect_Dashborad_Report_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_pros_number",sims_pros_number ),
                           new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            prospect simsobj = new prospect();
                            simsobj.sims_pros_number = dr["sims_pros_number"].ToString();
                            simsobj.sims_pros_sibling_enroll_number = dr["sims_pros_sibling_enroll_number"].ToString();
                            simsobj.full_student_name = dr["full_student_name"].ToString();
                            simsobj.school_remark = dr["school_remark"].ToString();
                            simsobj.parent_remark = dr["parent_remark"].ToString();
                            simsobj.sims_created_by = dr["sims_created_by"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_pros_status = dr["sims_pros_status"].ToString();
                            simsobj.sims_created_date =db.UIDDMMYYYYformat( dr["sims_created_date"].ToString());
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

    }
}