﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/CertificateTCApprove")]
    [BasicAuthentication]
    public class CertificateTCApprove_PearlController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_certificate_no")]
        public HttpResponseMessage Get_certificate_no()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_certificate_number = dr["sims_appl_parameter"].ToString();
                            obj.sims_certificate_name = dr["sims_appl_form_field_value1"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_Stream")]
        public HttpResponseMessage Get_Stream()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.stream = dr["sims_appl_form_field_value1"].ToString();
                            obj.stream_code = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_grade_data")]
        public HttpResponseMessage Get_grade_data()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_promotion_status")]
        public HttpResponseMessage Get_promotion_status()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_certificate_qualified_for_promotion_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_qualified_for_promotion = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_academic_status")]
        public HttpResponseMessage Get_academic_status()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'O'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_certificate_field3_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_field3 = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_student_caste")]
        public HttpResponseMessage Get_student_caste()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'K'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_certificate_student_caste_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_student_caste = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_failed_status")]
        public HttpResponseMessage Get_failed_status()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'M'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.sims_certificate_student_fail_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_student_fail = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Check_Tc_details")]
        public HttpResponseMessage Check_Tc_details(string userid)
        {
            List<sims684> lstcertificate = new List<sims684>();
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),
                           new SqlParameter("@sims_certificate_enroll_number", userid),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            flag = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        [Route("GetTCDetails")]
        public HttpResponseMessage GetTCDetails()
        {
            List<sims684> lstcertificate = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 obj = new sims684();
                            obj.stream = dr["sims_appl_form_field_value1"].ToString();
                            obj.stream_code = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("sims684_Get_Stud_proted_class")]
        public sims684 sims684_Get_Stud_proted_class(string enroll_No)
        {
            List<sims684> lstcertificate = new List<sims684>();
            sims684 simobj = new sims684();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'X'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //sims684 obj = new sims684();
                            simobj.promoted_class = dr["promoted_class"].ToString();
                            simobj.promoted_acad_yr = dr["promote_acad_yr"].ToString();

                        }
                    }
                    return simobj;
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return simobj;
            }

        }

        [Route("Get_Stud_subject")]
        public HttpResponseMessage Get_Stud_subject(string enroll_No)
        {
            List<sims684> simobj = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Y'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 temp = new sims684();
                            temp.sims_certificate_subject_studied = dr["sims_subject_code"].ToString();
                            temp.sims_certificate_subject_studied_ar = dr["sims_subject_name_en"].ToString();
                            simobj.Add(temp);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, simobj);
        }

        [Route("sims684_Get_Stream")]
        public sims684 sims684_Get_Stream(string enroll_No)
        {
            // List<sims684> simobj = new List<sims684>();
            sims684 simobj = new sims684();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'z'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simobj.stream = dr["sims_appl_form_field_value1"].ToString();
                            //simobj.Add(temp);
                        }
                    }
                    return simobj;
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return simobj;
                // return Request.CreateResponse(HttpStatusCode.OK, simobj);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, simobj);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "PZ"),
                            //new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("Get_Student_details")]
        public HttpResponseMessage Get_Student_details(string enroll_No, string opr)
        {
            List<sims684> group_list = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 simobj = new sims684();
                            simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simobj.stud_name = dr["student"].ToString();
                            simobj.religion = dr["religion"].ToString();
                            simobj.nationality = dr["Nationality"].ToString();
                            string date = dr["date"].ToString();
                            if (!string.IsNullOrEmpty(date))
                                simobj.date_of_admission = date;
                            string date1 = dr["sims_student_dob"].ToString();
                            if (!string.IsNullOrEmpty(date1))
                                simobj.date_of_birth = date1;
                            simobj.dobw = dr["DOBW"].ToString();
                            simobj.father_name = dr["parentName"].ToString();
                            simobj.mother_name = dr["motherName"].ToString();
                            simobj.sims_grade_name = dr["admitted_grade"].ToString();
                            simobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simobj.present_class = dr["Present_grade"].ToString();
                            simobj.present_sec = dr["Present_section"].ToString();
                            simobj.present_days = dr["TotalPresentDays"].ToString();
                            simobj.sims_certificate_academic_year = dr["academic_year"].ToString();
                            simobj.total_days = dr["TotalDays"].ToString();
                            simobj.registrar = dr["register"].ToString();
                            simobj.principle = dr["principle"].ToString();
                            simobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simobj.leave_date = dr["leave_date"].ToString();

                            simobj.sims_student_fees_paid_lastmonth = dr["doc_date"].ToString();
                            simobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simobj.bus_name = dr["bus_name"].ToString();
                            group_list.Add(simobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("Get_Student_details_new")]
        public HttpResponseMessage Get_Student_details_new(string enroll_No, string opr, string academic_year)
        {
            List<sims684> group_list = new List<sims684>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "N"),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                           new SqlParameter("@sims_certificate_academic_year", academic_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims684 simobj = new sims684();
                            simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simobj.stud_name = dr["student"].ToString();
                            simobj.religion = dr["religion"].ToString();
                            simobj.nationality = dr["Nationality"].ToString();
                            string date = db.UIDDMMYYYYformat(dr["date"].ToString());
                            if (!string.IsNullOrEmpty(date))
                                simobj.date_of_admission = date;
                            string date1 = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            if (!string.IsNullOrEmpty(date1))
                                simobj.date_of_birth = date1;
                            simobj.dobw = db.UIDDMMYYYYformat(dr["DOBW"].ToString());
                            simobj.father_name = dr["parentName"].ToString();
                            simobj.mother_name = dr["motherName"].ToString();
                            simobj.sims_grade_name = dr["admitted_grade"].ToString();
                            simobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simobj.present_class = dr["Present_grade"].ToString();
                            simobj.present_sec = dr["Present_section"].ToString();
                            simobj.present_days = dr["TotalPresentDays"].ToString();
                            simobj.sims_certificate_academic_year = dr["academic_year"].ToString();
                            simobj.sims_certificate_academic_year_desc = dr["sims_academic_year"].ToString();
                            simobj.total_days = dr["TotalDays"].ToString();
                            simobj.registrar = dr["register"].ToString();
                            simobj.principle = dr["principle"].ToString();

                            simobj.leave_date = db.UIDDMMYYYYformat(dr["leave_date"].ToString());

                            simobj.sims_student_fees_paid_lastmonth = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            simobj.bus_name = dr["bus_name"].ToString();
                            group_list.Add(simobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }
        
        [Route("Get_Search_Tc_details")]
        public HttpResponseMessage Get_Search_Tc_details(string enroll_no)
        {
            List<sims684> group_list = new List<sims684>();
            if (enroll_no == "undefined" || enroll_no == "")
            {
                enroll_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc]",
                   new List<SqlParameter>()
                   {
                       new SqlParameter ("@opr", 'S'),
                       new SqlParameter("@sims_certificate_enroll_number",enroll_no)
                   });

                    while (dr.Read())
                    {
                        sims684 simobj = new sims684();
                        simobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                        simobj.sims_certificate_name = dr["certificate_name"].ToString();
                        simobj.sims_certificate_enroll_number = dr["sims_certificate_enroll_number"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_leaving"].ToString()))
                            //simobj.tC_Date = DateTime.Parse(dr["sims_certificate_date_of_leaving"].ToString()).ToShortDateString();
                            simobj.tC_Date = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_leaving"].ToString());
                        simobj.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                        simobj.sims_certificate_general_conduct = dr["sims_certificate_general_conduct"].ToString();
                        simobj.sims_certificate_academic_progress = dr["sims_certificate_academic_progress"].ToString();
                        simobj.sims_certificate_subject_studied = dr["sims_certificate_subject_studied"].ToString();
                        simobj.sims_subject_code = dr["sims_subject_code"].ToString();

                        simobj.sims_certificate_subject_studied_ar = dr["sims_subject_name_en"].ToString();
                        simobj.sims_certificate_remark = dr["sims_certificate_remark"].ToString();
                        simobj.sims_certificate_attendance_remark = dr["sims_certificate_attendance_remark"].ToString();
                        simobj.sims_certificate_qualified_for_promotion = dr["sims_certificate_qualified_for_promotion"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_issue"].ToString()))
                            simobj.sims_certificate_date_of_issue = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_issue"].ToString());
                        simobj.sims_certificate_fee_paid = dr["sims_certificate_fee_paid"].ToString();
                        simobj.sims_certificate_sc_st_status = dr["sims_certificate_sc_st_status"].Equals("A") ? true : false;
                        simobj.sims_certificate_field1 = dr["sims_certificate_field1"].ToString();
                        simobj.sims_grade_code = dr["sims_certificate_field2"].ToString();
                        simobj.sims_certificate_field3 = dr["sims_certificate_field3"].ToString();
                        simobj.sims_certificate_field4 = dr["sims_certificate_field4"].ToString();
                        simobj.sims_certificate_field5 = dr["sims_certificate_field5"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_register_no"].ToString()))
                            simobj.sims_certificate_registration_register_no = int.Parse(dr["sims_certificate_registration_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_serial_no"].ToString()))
                            simobj.sims_certificate_registration_serial_no = int.Parse(dr["sims_certificate_registration_serial_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_register_no"].ToString()))
                            simobj.sims_certificate_result_register_no = int.Parse(dr["sims_certificate_result_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_serial_no"].ToString()))
                            simobj.sims_certificate_result_serial_no = int.Parse(dr["sims_certificate_result_serial_no"].ToString());
                        simobj.stud_name = dr["student"].ToString();
                        simobj.religion = dr["religion"].ToString();
                        simobj.nationality = dr["Nationality"].ToString();
                        string date = db.UIDDMMYYYYformat(dr["date"].ToString());
                        if (!string.IsNullOrEmpty(date))
                            simobj.date_of_admission = date;
                        string date1 = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        if (!string.IsNullOrEmpty(date1))
                            simobj.date_of_birth = date1;
                        simobj.father_name = dr["parentName"].ToString();
                        simobj.mother_name = dr["motherName"].ToString();
                        simobj.Credits = dr["sims_certificate_credit"].ToString();
                        simobj.stream = dr["stream"].ToString();
                        if (!string.IsNullOrEmpty(dr["DOBW"].ToString()))
                            simobj.dobw = dr["DOBW"].ToString();
                        //simobj.mother_name = dr["mother_name"].ToString();
                        simobj.admitted_class = dr["admitted_grade"].ToString();
                        simobj.present_class = dr["Present_grade"].ToString();
                        simobj.present_sec = dr["Present_section"].ToString();
                        simobj.present_days = dr["TotalPresentDays"].ToString();
                        simobj.total_days = dr["TotalDays"].ToString();
                        simobj.registrar = dr["register"].ToString();
                        simobj.principle = dr["principle"].ToString();
                        simobj.sims_certificate_academic_year = dr["sims_certificate_academic_year"].ToString();
                        //simobj.sims_certificate_student_caste = dr["sims_certificate_student_caste"].ToString();
                        //simobj.sims_certificate_student_caste_name = dr["caste"].ToString();
                        simobj.sims_certificate_student_fail = dr["sims_certificate_student_fail"].ToString();
                        simobj.sims_certificate_student_fail_name = dr["failed"].ToString();
                        simobj.sims_student_fee_discount_reason = dr["sims_student_fee_discount_reason"].ToString();
                        simobj.sims_student_exam_result = dr["sims_student_exam_result"].ToString();
                        simobj.sims_student_promoted_to = dr["sims_student_promoted_to"].ToString();
                        simobj.sims_certificate_qualified_for_promotion = dr["sims_certificate_qualified_for_promotion"].ToString();
                        simobj.sims_certificate_qualified_for_promotion_name = dr["promotion"].ToString();
                        simobj.sims_student_fees_paid_lastmonth = dr["sims_student_fees_paid_lastmonth"].ToString();
                        //simobj.sims_certificate_req_date = dr["sims_certificate_req_date"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_req_date"].ToString()))
                            //simobj.sims_certificate_req_date = DateTime.Parse(dr["sims_certificate_req_date"].ToString()).ToShortDateString();
                            simobj.sims_certificate_req_date = db.UIDDMMYYYYformat(dr["sims_certificate_req_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_leaving"].ToString()))
                            //simobj.sims_certificate_date_of_leaving = DateTime.Parse(dr["sims_certificate_date_of_leaving"].ToString()).ToShortDateString();
                            simobj.sims_certificate_date_of_leaving = db.DBYYYYMMDDformat(dr["sims_certificate_date_of_leaving"].ToString());

                        simobj.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                        //simobj.sims_certificate_date_of_leaving = dr["sims_certificate_date_of_leaving"].ToString();
                        simobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                        simobj.contact1 = dr["contact1"].ToString();
                        simobj.contact2 = dr["contact2"].ToString();
                        simobj.email = dr["email"].ToString();
                        simobj.refund_acc1 = dr["refund_acc1"].ToString();
                        simobj.refund_acc2 = dr["refund_acc2"].ToString();
                        simobj.sims_certificate_req_status = dr["tc_status"].ToString();
                        simobj.sims_certificate_student_games = dr["sims_certificate_student_games"].ToString();
                        simobj.sims_student_admission_number = dr["admission_number"].ToString();
                        group_list.Add(simobj);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("CUDCertificate_Tc_Parameter")]
        public HttpResponseMessage CUDCertificate_Tc_Parameter(List<sims684> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims684 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_certificate_tc_parameter_details_proc]",
                        new List<SqlParameter>()
                     {
                        new SqlParameter("@opr", simsobj.opr),
                        new SqlParameter("@sims_certificate_name", simsobj.sims_certificate_number),
                        new SqlParameter("@sims_certificate_enroll_number", simsobj.sims_certificate_enroll_number),
                        new SqlParameter("@sims_certificate_date_of_leaving", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_leaving)),
                        new SqlParameter("@sims_certificate_reason_of_leaving", simsobj.sims_certificate_reason_of_leaving),
                        new SqlParameter("@sims_certificate_general_conduct", simsobj.sims_certificate_general_conduct),
                        new SqlParameter("@sims_certificate_academic_progress", simsobj.sims_certificate_academic_progress),
                        new SqlParameter("@sims_certificate_subject_studied", simsobj.sims_certificate_subject_studied),
                        new SqlParameter("@sims_certificate_remark", simsobj.sims_certificate_remark),
                        new SqlParameter("@sims_certificate_attendance_remark", simsobj.sims_certificate_attendance_remark),
                        new SqlParameter("@sims_certificate_qualified_for_promotion", simsobj.sims_certificate_qualified_for_promotion),
                        new SqlParameter("@sims_certificate_date_of_issue", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_issue)),                      
                        new SqlParameter("@sims_certificate_fee_paid",simsobj.sims_certificate_fee_paid),                    
                        new SqlParameter("@sims_certificate_sc_st_status", simsobj.sims_certificate_sc_st_status == true? "A":"I"),                        
                        new SqlParameter("@sims_certificate_field1", simsobj.sims_certificate_field1),
                        new SqlParameter("@sims_certificate_field2", simsobj.sims_certificate_field2),
                        new SqlParameter("@sims_certificate_field3", simsobj.sims_certificate_field3),
                        new SqlParameter("@sims_certificate_field4", simsobj.sims_certificate_field4),
                        new SqlParameter("@sims_certificate_field5", simsobj.sims_certificate_field5),
                        new SqlParameter("@sims_certificate_registration_register_no", simsobj.sims_certificate_registration_register_no),
                        new SqlParameter("@sims_certificate_registration_serial_no", simsobj.sims_certificate_registration_serial_no),
                        new SqlParameter("@sims_certificate_result_register_no", simsobj.sims_certificate_result_register_no),
                        new SqlParameter("@sims_certificate_result_serial_no", simsobj.sims_certificate_result_serial_no),
                        new SqlParameter("@sims_certificate_stream_name", simsobj.stream),
                        new SqlParameter("@sims_certificate_credit", simsobj.Credits),     
                        new SqlParameter("@sims_certificate_academic_year", simsobj.sims_certificate_academic_year),
                        new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                        new SqlParameter("@contact1", simsobj.contact1),
                        new SqlParameter("@contact2", simsobj.contact2),
                        new SqlParameter("@sims_transport_vehicle_name_plate", simsobj.bus_name),
                        new SqlParameter("@email", simsobj.email),
                        new SqlParameter("@refund_acc1", simsobj.refund_acc1),
                        new SqlParameter("@refund_acc2", simsobj.refund_acc2),
                        new SqlParameter("@last_attendance_day", simsobj.last_attendance_day),		  
                       // new SqlParameter("@sims_certificate_student_caste", simsobj.sims_certificate_student_caste),
                        new SqlParameter("@sims_certificate_student_fail", simsobj.sims_certificate_student_fail),
                        new SqlParameter("@sims_student_fee_discount_reason", simsobj.sims_student_fee_discount_reason),
                        new SqlParameter("@sims_student_exam_result", simsobj.sims_student_exam_result),                        
                        new SqlParameter("@sims_student_fees_paid_lastmonth", simsobj.sims_student_fees_paid_lastmonth),
                        //new SqlParameter("@sims_certificate_qualified_for_promotion", simsobj.sims_certificate_qualified_for_promotion),
                        new SqlParameter("@sims_certificate_req_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_req_date)),
                        new SqlParameter("@sims_certificate_student_games",simsobj.sims_certificate_student_games),
                        new SqlParameter("@sims_certificate_req_status", simsobj.sims_certificate_req_status),
                        new SqlParameter("@sims_student_admission_number",simsobj.sims_student_admission_number),
                        new SqlParameter("@sims_student_promoted_to", simsobj.sims_student_promoted_to),
                        //new SqlParameter("@sims_certificate_admission_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_admission_date)),
                        //new SqlParameter("@sims_certificate_joined_class", simsobj.sims_certificate_joined_class),

                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}