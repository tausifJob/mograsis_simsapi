﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.DocumentDetailsController
{
    [RoutePrefix("api/DocumentDetail")]
    [BasicAuthentication]
    public class DocumentDetailsController : ApiController
    {
       
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getAllDocument")]
        public HttpResponseMessage getAllDocument()
        {
           
            List<Sim019> doc_list = new List<Sim019>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_doc_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim019 simsobj = new Sim019();
                            simsobj.sims_doc_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_doc_cur_code = dr["sims_doc_cur_code"].ToString();
                            simsobj.sims_doc_mod_code = dr["sims_doc_mod_code"].ToString();
                            simsobj.sims_doc_mod_name = dr["comn_mod_name_en"].ToString();
                            simsobj.sims_doc_code = dr["sims_doc_code"].ToString();
                            simsobj.sims_doc_name = dr["sims_doc_master_desc"].ToString();
                            simsobj.sims_doc_srno = dr["sims_doc_srno"].ToString();
                            simsobj.sims_doc_desc = dr["sims_doc_desc"].ToString();
                            simsobj.sims_doc_status = dr["sims_doc_status"].Equals("A") ? true : false;
                            doc_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, doc_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_list);//.Skip(skip).Take(PageSize).ToList()
        }

        [Route("getModuleName")]
        public HttpResponseMessage getModuleName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModuleName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getModuleName"));

            List<Sim019> mod_list = new List<Sim019>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_doc_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim019 simsobj = new Sim019();
                            simsobj.sims_doc_mod_code = dr["sims_doc_mod_code"].ToString();
                            simsobj.sims_doc_mod_name = dr["comn_mod_name_en"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDocumentName")]
        public HttpResponseMessage getDocumentName(string modcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModuleName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getModuleName"));

            List<Sim019> mod_list = new List<Sim019>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_doc_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "N"),
                            new SqlParameter("@sims_doc_mod_code",modcode)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim019 simsobj = new Sim019();
                            simsobj.sims_doc_code = dr["sims_doc_code"].ToString();
                            simsobj.sims_doc_name = dr["sims_doc_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDDocumentDetails")]
        public HttpResponseMessage CUDDocumentDetails(List<Sim019> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDocumentDetails(),PARAMETERS";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim019 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_doc_details_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_doc_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_doc_mod_code", simsobj.sims_doc_mod_code),
                                new SqlParameter("@sims_doc_code", simsobj.sims_doc_code),
                                new SqlParameter("@sims_doc_srno", simsobj.sims_doc_srno),
                                new SqlParameter("@sims_doc_desc", simsobj.sims_doc_desc),
                                new SqlParameter("@sims_doc_status",simsobj.sims_doc_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
