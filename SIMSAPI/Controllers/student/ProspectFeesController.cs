﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{
     [RoutePrefix("api/ProspectFees")]
    public class ProspectFeesController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("getProspectFees")]
         public HttpResponseMessage getProspectFees(string cur_code, string academic_year, string grade_code, string prospect_no_name)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProspectFees(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "Common", "getProspectFees"));

             if (cur_code == "undefined")
                 cur_code = null;
             if (academic_year == "undefined")
                 academic_year = null;
             if (grade_code == "undefined")
                 grade_code = null;
             if (prospect_no_name == "undefined")
                 prospect_no_name = null;
             List<Sims565> goaltarget_list = new List<Sims565>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_prospect_fee_document_proc]",
                         //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_prospect_no", prospect_no_name),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims565 objNew = new Sims565();
                             objNew.sims_academic_year = dr["sims_pros_academic_year"].ToString();
                             objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                             objNew.sims_pros_number = dr["sims_pros_number"].ToString();
                             objNew.sims_cur_code = dr["sims_pros_cur_code"].ToString();
                             objNew.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                             objNew.sims_grade_code = dr["sims_pros_grade_code"].ToString();
                             objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             objNew.sims_grade_fee_amount = dr["sims_grade_fee_amount"].ToString();
                             objNew.sims_student_full_name = dr["student_full_name"].ToString();
                             objNew.sims_parent_full_name = dr["parent_full_name"].ToString();
                             objNew.sims_fee_amount = dr["paid_amount"].ToString();
                             objNew.sims_doc_status = dr["sims_doc_status"].ToString();
                             objNew.sims_doc_status_desc = dr["sims_doc_status_desc"].ToString();
                             objNew.sims_doc_status_content = dr["sims_doc_status_content"].ToString();
                             objNew.sims_doc_no = dr["sims_doc_no"].ToString();
                             goaltarget_list.Add(objNew);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getProspectGradeFees")]
         public HttpResponseMessage getProspectGradeFees(string cur_code, string academic_year, string grade_code, string prospect_no_name)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProspectGradeFees(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "Common", "getAdmissionGradeFees"));

             if (cur_code == "undefined")
                 cur_code = null;
             if (academic_year == "undefined")
                 academic_year = null;
             if (grade_code == "undefined")
                 grade_code = null;
             if (prospect_no_name == "undefined")
                 prospect_no_name = null;
             List<Sims565> goaltarget_list = new List<Sims565>();
             try
             {

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_prospect_fee_document_proc]",
                         //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                         new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_prospect_no", prospect_no_name),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims565 objNew = new Sims565();
                             objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                             objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                             objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                             objNew.sims_grade_fee_amount = dr["sims_fee_amount"].ToString();
                             objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                             objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                             objNew.sims_rev_acc_no = dr["sims_rev_acc_no"].ToString();
                             objNew.sims_cash_acc_no = dr["sims_cash_acc_no"].ToString();
                             objNew.sims_fee_status = dr["sims_fee_status"].ToString();



                             goaltarget_list.Add(objNew);

                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("insertProspectFees")]
         public HttpResponseMessage insertProspectFees(List<Sims565> data)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertProspectFees(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "Common", "insertProspectFees"));

             Message message = new Message();
             int cnt = 1;
             bool insert = false;
             string ref_no = string.Empty;

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (Sims565 simsobj in data)
                     {
                         // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                         SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_prospect_fee_document_proc]",
                         new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_prospect_no", simsobj.sims_pros_number),
                                new SqlParameter("@sims_doc_no", simsobj.sims_doc_no),
                                new SqlParameter("@sims_doc_date", simsobj.sims_doc_date),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                                new SqlParameter("@sims_fee_amount", simsobj.sims_grade_fee_amount),
                                new SqlParameter("@sims_user_name", simsobj.sims_user_name),
                                new SqlParameter("@sims_doc_status", simsobj.sims_doc_status),                               
                                new SqlParameter("@sr_no", cnt),


                                new SqlParameter("@sims_fee_payment_mode", simsobj.sims_fee_payment_mode),
                                new SqlParameter("@sims_fee_cheque_bank_code", simsobj.sims_fee_cheque_bank_code),
                                new SqlParameter("@sims_fee_cheque_number", simsobj.sims_fee_cheque_number),
                                new SqlParameter("@sims_fee_cheque_date", simsobj.sims_fee_cheque_date),

                               
                     });
                         if (dr.HasRows)
                         {
                             if (dr.Read())
                             {
                                 if (!string.IsNullOrEmpty(dr["doc_no"].ToString()))
                                     ref_no = dr["doc_no"].ToString();
                             }
                         }
                         cnt = cnt + 1;
                         dr.Close();
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, ref_no);
                 }

             }

             catch (Exception x)
             {

                 message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, ref_no);
         }

         [Route("promotetoadmission")]
         public HttpResponseMessage insertProspectFees(string prospectno)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertProspectFees(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "Common", "insertProspectFees"));

             Message message = new Message();

             bool insert = false;
             string ref_no = string.Empty;

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();

                     // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                     int ins = db.ExecuteStoreProcedureforInsert("[sims].[Sims_admission_prospects_proc]",
                       new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_admission_number", prospectno),
                               

                               
                     });
                     if (ins > 0)
                     {
                         insert = true;
                     }


                     else
                     {
                         insert = false;
                     }

                 }
                 return Request.CreateResponse(HttpStatusCode.OK, insert);
             }



             catch (Exception x)
             {

                 message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;

             }
             return Request.CreateResponse(HttpStatusCode.OK, insert);
         }

         [Route("getprospectReceipt")]
         public HttpResponseMessage getprospectReceipt()
         {
             List<SqlParameter> lst = new List<SqlParameter>();
             string reportName = "Sims.SIMR51Prospect";
             lst.Add(new SqlParameter("@OPR", "R"));
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_prospect_fee_document_proc]", lst);
                     if (dr.Read())
                     {
                         reportName = dr[0].ToString();
                     }
                 }

             }
             catch (Exception ex)
             {
             }
             return Request.CreateResponse(HttpStatusCode.OK, reportName);
         }


    }
}