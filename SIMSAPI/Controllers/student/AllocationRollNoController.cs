﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data;
using Telerik.Reporting.Interfaces;
using SIMSAPI.Attributes;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/AllocationRollNo")]
    public class AllocationRollNoController : ApiController
    {

        public object message { get; set; }
        [Route("getStudentName")]
        public HttpResponseMessage getStudentName(string curcode, string academicyear, string gradecode, string sectioncode, string orderByGender, string orderByName, string sims_allocation_status,string sims_subject_code)
        {
            List<alloc_roll_no> mod_list = new List<alloc_roll_no>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[student_roll_number_allocation]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","S"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", sectioncode),
                new SqlParameter("@orderByGender", orderByGender),
                new SqlParameter("@orderByName", orderByName),
                new SqlParameter("@sims_allocation_status", sims_allocation_status),
                new SqlParameter("@sims_subject_code", sims_subject_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alloc_roll_no newobj = new alloc_roll_no();
                            newobj.sims_student_enroll_number = dr["sims_enroll_number"].ToString();
                            newobj.sims_first_name = dr["first_name"].ToString();
                            newobj.sims_middle_name = dr["middle_name"].ToString();
                            newobj.sims_last_name = dr["last_name"].ToString();
                            newobj.sims_roll_number = dr["sims_roll_number"].ToString();
                            newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            newobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            newobj.sims_section_code = dr["sims_section_code"].ToString();
                            newobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            mod_list.Add(newobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("updateRollNo")]
        public HttpResponseMessage updateRollNo(List<alloc_roll_no> lst)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (alloc_roll_no obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[student_roll_number_allocation]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", 'U'),
                    new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                new SqlParameter("@sims_section_code", obj.sims_section_code),
                    //new SqlParameter("@sims_student_enroll_number", obj.sims_student_enroll_number),
                    //new SqlParameter("@sims_roll_number", obj.sims_roll_number),
                    new SqlParameter("@sims_enroll_roll", obj.sims_enroll_roll),

                    
                      });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();

                        
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

               


        //Seelct
        [Route("getAllStageDescription")]
        public HttpResponseMessage getAllStageDescription()
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllStageDescription(),PARAMETERS :: NO";
          //  Log.Debug(string.Format(debug, "Common", "getAllCreditEditDivision"));

            List<SAR011> goaltarget_list = new List<SAR011>();
            //int total = 0, skip = 0;
            try 
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[grade_stagedesc_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                          new SqlParameter("@sims_stage_sr_no",sims_stage_sr_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SAR011 simsobj = new SAR011();
                            simsobj.sims_stage_sr_no = dr["sims_stage_sr_no"].ToString();
                            simsobj.sims_stage_description = dr["sims_stage_description"].ToString();
                            //simsobj.sims_stage_status = dr["sims_stage_status"].ToString();
                            if (dr["sims_stage_status"].ToString().Equals("A"))
                                simsobj.sims_stage_status = true;
                            else
                                simsobj.sims_stage_status = false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
              //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDStageDescription")]
        public HttpResponseMessage CUDStageDescription(List<SAR011> data)
        {
            bool insert = false;
          //  Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SAR011 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[grade_stagedesc_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_stage_sr_no", simsobj.sims_stage_sr_no),
                                new SqlParameter("@sims_stage_description", simsobj.sims_stage_description),
                                new SqlParameter("@sims_stage_status",simsobj.sims_stage_status==true?"A":"I")


                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }
            // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("allocationrollnoCommon")]
        public HttpResponseMessage allocationrollnoCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[dbo].[student_roll_number_allocation]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        public object sims_stage_sr_no { get; set; }
    }
}

        
    

