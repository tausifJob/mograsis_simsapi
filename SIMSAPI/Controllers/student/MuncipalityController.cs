﻿using log4net;
using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System.Net;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/Municipality")]
    public class MunicipalityController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getMunicipalityDetails")]
        public HttpResponseMessage getMunicipalityDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMunicipalityDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getMunicipalityDetails"));

            List<municipality> municipality_list = new List<municipality>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_municipality_proc]",
                        new List<SqlParameter>()
                    {
                        new SqlParameter("@opr",'S'),
                    
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            municipality simsobj = new municipality();
                            simsobj.municipality_code = dr["sims_municipality_code"].ToString();
                            simsobj.english_name = dr["sims_municipality_name_en"].ToString();
                            simsobj.arabic_name = dr["sims_municipality_name_ar"].ToString();
                            simsobj.other_name = dr["sims_municipality_name_ot"].ToString();
                            simsobj.municipality_status = dr["sims_municipality_status"].Equals("A") ? true : false;
                            municipality_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, municipality_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, municipality_list);


        }

        [Route("CUDMunicipalityDetails")]
        public HttpResponseMessage InsertMunicipalityDetails(List<municipality> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (municipality simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_municipality_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_municipality_code",simsobj.municipality_code),
                                new SqlParameter("@sims_municipality_name_en",simsobj.english_name),
                                new SqlParameter("@sims_municipality_name_ar", simsobj.arabic_name),
                                new SqlParameter("@sims_municipality_name_ot", simsobj.other_name),
                                new SqlParameter("@sims_municipality_status", simsobj.municipality_status==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
    }
}
