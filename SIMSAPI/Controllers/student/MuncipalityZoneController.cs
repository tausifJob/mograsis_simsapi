﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/MunicipalityZone")]
    public class MuncipalityZoneController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetMunicipalityName")]
        public HttpResponseMessage GetMunicipalityName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMunicipalityName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetMunicipalityName"));

            List<municipalityZone> goaltarget_list = new List<municipalityZone>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_municipality_zone_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            municipalityZone simsobj = new municipalityZone();


                            simsobj.municipality_code = dr["sims_municipality_code"].ToString();
                            simsobj.municipality_name = dr["sims_municipality_name_en"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getMunicipalityZoneDetails")]
        public HttpResponseMessage getMunicipalityDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMunicipalityDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getMunicipalityDetails"));

            List<municipalityZone> municipality_list = new List<municipalityZone>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_municipality_zone_proc]",
                        new List<SqlParameter>()
                    {
                        new SqlParameter("@opr",'S'),
                    
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            municipalityZone simsobj = new municipalityZone();
                            simsobj.municipalityZone_code = dr["sims_zone_code"].ToString();
                            simsobj.municipality_code = dr["sims_municipality_code"].ToString();                            
                            simsobj.municipality_name = dr["sims_municipality_name_en"].ToString();
                            simsobj.english_name = dr["sims_zone_name_en"].ToString();
                            simsobj.arabic_name = dr["sims_zone_name_ar"].ToString();
                            simsobj.other_name = dr["sims_zone_name_ot"].ToString();
                            simsobj.municipalityZone_status = dr["sims_zone_status"].Equals("A") ? true : false;
                            municipality_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, municipality_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, municipality_list);


        }

        [Route("CUDMunicipalityZoneDetails")]
        public HttpResponseMessage InsertMunicipalityDetails(List<municipalityZone> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (municipalityZone simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_municipality_zone_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_municipality_code",simsobj.municipality_code),
                                new SqlParameter("@sims_zone_code",simsobj.municipalityZone_code),
                                new SqlParameter("@sims_zone_name_en",simsobj.english_name),
                                new SqlParameter("@sims_zone_name_ar", simsobj.arabic_name),
                                new SqlParameter("@sims_zone_name_ot", simsobj.other_name),
                                new SqlParameter("@sims_zone_status", simsobj.municipalityZone_status==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
    }
}