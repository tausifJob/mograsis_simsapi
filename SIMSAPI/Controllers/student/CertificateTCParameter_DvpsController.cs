﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using System.Data;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/CertificateTCParameter_Dvps")]
    [BasicAuthentication]
    public class CertificateTCParameter_DvpsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_certificate_no")]
        public HttpResponseMessage Get_certificate_no()
        {

            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.sims_certificate_number = dr["sims_appl_parameter"].ToString();
                            obj.sims_certificate_name = dr["sims_appl_form_field_value1"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_Stream")]
        public HttpResponseMessage Get_Stream()
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.stream = dr["sims_appl_form_field_value1"].ToString();
                            obj.stream_code = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Check_Tc_details")]
        public HttpResponseMessage Check_Tc_details(string userid)
        {
            List<sims505> lstcertificate = new List<sims505>();
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),
                           new SqlParameter("@sims_certificate_enroll_number", userid),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            flag = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        [Route("GetTCDetails")]
        public HttpResponseMessage GetTCDetails()
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.stream = dr["sims_appl_form_field_value1"].ToString();
                            obj.stream_code = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("sims505_Get_Stud_proted_class")]
        public sims505 sims505_Get_Stud_proted_class(string enroll_No)
        {
            List<sims505> lstcertificate = new List<sims505>();
            sims505 simobj = new sims505();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'X'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //sims505 obj = new sims505();
                            simobj.promoted_class = dr["promoted_class"].ToString();
                            simobj.promoted_acad_yr = dr["promote_acad_yr"].ToString();

                        }
                    }
                    return simobj;
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return simobj;
            }

        }

        [Route("Get_Stud_subject")]
        public HttpResponseMessage Get_Stud_subject(string enroll_No)
        {
            List<sims505> simobj = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Y'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 temp = new sims505();
                            temp.sims_certificate_subject_studied = dr["sims_subject_code"].ToString();
                            temp.sims_certificate_subject_studied_ar = dr["sims_subject_name_en"].ToString();
                            simobj.Add(temp);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, simobj);
        }

        [Route("Get_status_code")]
        public HttpResponseMessage Get_status_code()
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'K'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.sims_certificate_field3_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_field3 = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_Student_details")]
        public HttpResponseMessage Get_Student_details(string enroll_No, string opr)
        {
            List<sims505> group_list = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 simobj = new sims505();
                            simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simobj.stud_name = dr["student"].ToString();
                            simobj.religion = dr["religion"].ToString();
                            simobj.nationality = dr["Nationality"].ToString();
                            string date = dr["date"].ToString();
                            if (!string.IsNullOrEmpty(date))
                                //    simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
                                simobj.date_of_admission = date;
                            string date1 = dr["sims_student_dob"].ToString();
                            if (!string.IsNullOrEmpty(date1))
                                //simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
                                simobj.date_of_birth = date1;
                            simobj.dobw = dr["DOBW"].ToString();
                            simobj.father_name = dr["parentName"].ToString();
                            simobj.mother_name = dr["motherName"].ToString();
                            simobj.sims_birth_place = dr["sims_birth_place"].ToString();
                            simobj.admitted_class = dr["admitted_grade"].ToString();
                            simobj.present_class = dr["Present_grade"].ToString();
                            simobj.present_sec = dr["Present_section"].ToString();
                            simobj.present_days = dr["TotalPresentDays"].ToString();
                            simobj.total_days = dr["TotalDays"].ToString();
                            simobj.registrar = dr["register"].ToString();
                            simobj.principle = dr["principle"].ToString();
                            simobj.curr_code = dr["sims_student_cur_code"].ToString();
                            simobj.academic_year = dr["academic_year"].ToString();
                            simobj.sims_certificate_academic_year = dr["sims_academic_year"].ToString();
                            simobj.curr_name = dr["curr_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["last_day"].ToString()))
                                //simobj.sims_certificate_date_of_leaving = DateTime.Parse(dr["last_day"].ToString()).ToShortDateString();
                                simobj.sims_certificate_date_of_leaving = dr["last_day"].ToString();
                            simobj.sims_student_fees_paid_lastmonth = dr["doc_date"].ToString();
                            simobj.promoted_acad_yr = dr["academic_year"].ToString();
                            simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simobj.sims_certificate_exam_year = dr["exam_year"].ToString();
                            simobj.sims_manually_attendance = dr["sims_manually_attendance"].ToString();
                            try
                            {
                                simobj.sims_certificate_field4 = dr["concession"].ToString();
                                simobj.sims_student_exam_appeared = dr["sims_student_exam_appeared"].ToString();
                                simobj.sims_certificate_general_conduct = dr["sims_certificate_general_conduct"].ToString();
                                simobj.sims_certificate_remark = dr["sims_certificate_remark"].ToString();
                                simobj.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                                
                            }
                            catch (Exception ex) { }
                            group_list.Add(simobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("Get_Student_details")]
        public HttpResponseMessage Get_Student_details(string enroll_No, string opr, string academic_year)
        {
            List<sims505> group_list = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                           new SqlParameter("@sims_certificate_academic_year", academic_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 simobj = new sims505();
                            simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simobj.stud_name = dr["student"].ToString();
                            simobj.religion = dr["religion"].ToString();
                            simobj.nationality = dr["Nationality"].ToString();
                            string date = dr["date"].ToString();
                            if (!string.IsNullOrEmpty(date))
                                //simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
                                simobj.date_of_admission = date;
                            string date1 = dr["sims_student_dob"].ToString();
                            if (!string.IsNullOrEmpty(date1))
                                //simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
                            simobj.date_of_birth = date1;
                            simobj.sims_birth_place = dr["sims_birth_place"].ToString();
                            simobj.dobw = dr["DOBW"].ToString();
                            simobj.father_name = dr["parentName"].ToString();
                            simobj.mother_name = dr["motherName"].ToString();
                            simobj.admitted_class = dr["admitted_grade"].ToString();
                            simobj.present_class = dr["Present_grade"].ToString();
                            simobj.present_sec = dr["Present_section"].ToString();
                            simobj.present_days = dr["TotalPresentDays"].ToString();
                            simobj.total_days = dr["TotalDays"].ToString();
                            simobj.registrar = dr["register"].ToString();
                            simobj.principle = dr["principle"].ToString();
                            simobj.curr_code = dr["sims_student_cur_code"].ToString();
                            simobj.academic_year = dr["academic_year"].ToString();
                            simobj.sims_certificate_academic_year = dr["sims_academic_year"].ToString();
                            simobj.curr_name = dr["curr_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["last_day"].ToString()))
                                //simobj.sims_certificate_date_of_leaving = DateTime.Parse(dr["last_day"].ToString()).ToShortDateString();
                                simobj.sims_certificate_date_of_leaving = dr["last_day"].ToString();
                            simobj.sims_student_fees_paid_lastmonth = dr["doc_date"].ToString();
                            simobj.promoted_acad_yr = dr["academic_year"].ToString();
                            simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simobj.sims_certificate_exam_year = dr["exam_year"].ToString();
                            try
                            {
                                simobj.sims_certificate_field4 = dr["concession"].ToString();

                            }
                            catch (Exception ex) { }
                            group_list.Add(simobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }



        //[Route("Get_Student_details_sisqatar")]
        //public HttpResponseMessage Get_Student_details_sisqatar(string enroll_No, string opr)
        //{
        //    List<sims505> group_list = new List<sims505>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
        //                new List<SqlParameter>()
        //                 {
        //                   new SqlParameter("@opr", opr),
        //                   new SqlParameter("@sims_certificate_enroll_number", enroll_No),
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    sims505 simobj = new sims505();
        //                    simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
        //                    simobj.stud_name = dr["student"].ToString();
        //                    simobj.religion = dr["religion"].ToString();
        //                    simobj.nationality = dr["Nationality"].ToString();
        //                    simobj.date_of_admission = db.UIDDMMYYYYformat(dr["date"].ToString());
        //                    //if (!string.IsNullOrEmpty(date))
        //                    //    simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
        //                    simobj.date_of_birth = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
        //                    //if (!string.IsNullOrEmpty(date1))
        //                    //    simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
        //                    simobj.dobw = dr["DOBW"].ToString();
        //                    simobj.father_name = dr["parentName"].ToString();
        //                    simobj.mother_name = dr["motherName"].ToString();
        //                    simobj.admitted_class = dr["admitted_grade"].ToString();
        //                    simobj.present_class = dr["Present_grade"].ToString();
        //                    simobj.present_sec = dr["Present_section"].ToString();
        //                    simobj.present_days = dr["TotalPresentDays"].ToString();
        //                    simobj.total_days = dr["TotalDays"].ToString();
        //                    simobj.registrar = dr["register"].ToString();
        //                    simobj.principle = dr["principle"].ToString();
        //                    simobj.curr_code = dr["sims_student_cur_code"].ToString();
        //                    simobj.academic_year = dr["academic_year"].ToString();
        //                    simobj.sims_certificate_academic_year = dr["sims_academic_year"].ToString();
        //                    simobj.curr_name = dr["curr_name"].ToString();
        //                    simobj.sims_certificate_date_of_leaving = db.UIDDMMYYYYformat(dr["last_day"].ToString());
        //                    simobj.sims_student_fees_paid_lastmonth = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
        //                    simobj.promoted_acad_yr = dr["academic_year"].ToString();
        //                    simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
        //                    simobj.sims_student_gender = dr["sims_student_gender"].ToString();
        //                    simobj.sims_certificate_exam_year = dr["exam_year"].ToString();
        //                    simobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
        //                    try
        //                    {
        //                        simobj.sims_certificate_field4 = dr["concession"].ToString();

        //                    }
        //                    catch (Exception ex) { }
        //                    group_list.Add(simobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, group_list);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, group_list);
        //}
        //Get
        [Route("Get_Student_details_sisqatar")]
        public HttpResponseMessage Get_Student_details_sisqatar(string enroll_No, string opr, string academic_year)
        {
            List<sims505> group_list = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", opr),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                           new SqlParameter("@sims_certificate_academic_year", academic_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 simobj = new sims505();
                            simobj.sims_certificate_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simobj.stud_name = dr["student"].ToString();
                            simobj.religion = dr["religion"].ToString();
                            simobj.nationality = dr["Nationality"].ToString();
                            simobj.date_of_admission = dr["date"].ToString();
                            //if (!string.IsNullOrEmpty(date))
                            //    simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
                            simobj.date_of_birth = dr["sims_student_dob"].ToString();
                            //if (!string.IsNullOrEmpty(date1))
                            //    simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
                            simobj.dobw = dr["DOBW"].ToString();
                            simobj.father_name = dr["parentName"].ToString();
                            simobj.mother_name = dr["motherName"].ToString();
                            simobj.admitted_class = dr["admitted_grade"].ToString();
                            simobj.present_class = dr["Present_grade"].ToString();
                            simobj.present_sec = dr["Present_section"].ToString();
                            simobj.present_days = dr["TotalPresentDays"].ToString();
                            simobj.total_days = dr["TotalDays"].ToString();
                            simobj.registrar = dr["register"].ToString();
                            simobj.principle = dr["principle"].ToString();
                            simobj.curr_code = dr["sims_student_cur_code"].ToString();
                            simobj.academic_year = dr["academic_year"].ToString();
                            simobj.sims_certificate_academic_year = dr["sims_academic_year"].ToString();
                            simobj.curr_name = dr["curr_name"].ToString();
                            // if (!string.IsNullOrEmpty(dr["last_day"].ToString()))
                            //   simobj.sims_certificate_date_of_leaving = DateTime.Parse(dr["last_day"].ToString()).ToShortDateString();
                            simobj.sims_certificate_date_of_leaving = dr["last_day"].ToString();

                            simobj.sims_student_fees_paid_lastmonth = dr["doc_date"].ToString();
                            simobj.promoted_acad_yr = dr["academic_year"].ToString();
                            simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simobj.sims_certificate_exam_year = dr["exam_year"].ToString();
                            simobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                            try
                            {
                                simobj.sims_certificate_field4 = dr["concession"].ToString();

                            }
                            catch (Exception ex) { }
                            group_list.Add(simobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("Get_Search_Tc_details")]
        public HttpResponseMessage Get_Search_Tc_details(string enroll_no)
        {
            List<sims505> group_list = new List<sims505>();
            if (enroll_no == "undefined" || enroll_no == "")
            {
                enroll_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                   new List<SqlParameter>()
                   {
                       new SqlParameter ("@opr", 'S'),
                       new SqlParameter ("@sims_certificate_enroll_number", enroll_no),
                   });

                    while (dr.Read())
                    {
                        sims505 simobj = new sims505();
                        simobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                        simobj.sims_certificate_name = dr["certificate_name"].ToString();
                        simobj.sims_certificate_enroll_number = dr["sims_certificate_enroll_number"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_leaving"].ToString()))
                        simobj.sims_certificate_date_of_leaving = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_leaving"].ToString());
                        simobj.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                        simobj.sims_certificate_general_conduct = dr["sims_certificate_general_conduct"].ToString();
                        simobj.sims_certificate_academic_progress = dr["sims_certificate_academic_progress"].ToString();
                        simobj.sims_certificate_subject_studied = dr["sims_certificate_subject_studied"].ToString();
                        simobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_subject_name_en"].ToString()))
                            simobj.sims_certificate_subject_studied_ar = dr["sims_subject_name_en"].ToString().Remove(dr["sims_subject_name_en"].ToString().Length - 1);
                        simobj.sims_certificate_remark = dr["sims_certificate_remark"].ToString();
                        simobj.sims_certificate_attendance_remark = dr["sims_certificate_attendance_remark"].ToString();
                        simobj.sims_certificate_qualified_for_promotion = dr["sims_certificate_qualified_for_promotion"].Equals("A") ? true : false;
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_issue"].ToString()))
                            simobj.sims_certificate_date_of_issue = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_issue"].ToString());
                        simobj.sims_certificate_fee_paid = dr["sims_certificate_fee_paid"].Equals("A") ? true : false;
                        simobj.sims_certificate_sc_st_status = dr["sims_certificate_sc_st_status"].Equals("A") ? true : false;
                        simobj.sims_certificate_field1 = dr["sims_certificate_field1"].ToString();
                        simobj.sims_certificate_field2 = dr["sims_certificate_field2"].ToString();
                        simobj.sims_certificate_field3 = dr["sims_certificate_field3"].ToString();
                        //simobj.sims_certificate_field3_name = dr["sims_certificate_field3_name"].ToString();
                        simobj.sims_certificate_field4 = dr["sims_certificate_field4"].ToString();
                        simobj.sims_certificate_field5 = dr["sims_certificate_field5"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_register_no"].ToString()))
                            simobj.sims_certificate_registration_register_no = int.Parse(dr["sims_certificate_registration_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_serial_no"].ToString()))
                            simobj.sims_certificate_registration_serial_no = int.Parse(dr["sims_certificate_registration_serial_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_register_no"].ToString()))
                            simobj.sims_certificate_result_register_no = int.Parse(dr["sims_certificate_result_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_serial_no"].ToString()))
                            simobj.sims_certificate_result_serial_no = int.Parse(dr["sims_certificate_result_serial_no"].ToString());
                        simobj.stud_name = dr["student"].ToString();
                        simobj.religion = dr["religion"].ToString();
                        simobj.nationality = dr["Nationality"].ToString();
                        //string date = dr["date"].ToString();
                        string date = db.UIDDMMYYYYformat(dr["date"].ToString());
                        if (!string.IsNullOrEmpty(date))
                            //simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
                            simobj.date_of_admission = date;
                        string date1 = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        if (!string.IsNullOrEmpty(date1))
                            //simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
                            simobj.date_of_birth = date1;
                        simobj.father_name = dr["parentName"].ToString();
                        simobj.Credits = dr["sims_certificate_credit"].ToString();
                        simobj.stream = dr["stream"].ToString();
                        if (!string.IsNullOrEmpty(dr["DOBW"].ToString()))
                            simobj.dobw = dr["DOBW"].ToString();
                        simobj.father_name = dr["parentName"].ToString();
                        simobj.mother_name = dr["motherName"].ToString();
                        simobj.sims_birth_place = dr["sims_birth_place"].ToString();
                        simobj.admitted_class = dr["admitted_grade"].ToString();
                        simobj.present_class = dr["Present_grade"].ToString();
                        simobj.present_sec = dr["Present_section"].ToString();
                        simobj.present_days = dr["TotalPresentDays"].ToString();
                        simobj.total_days = dr["TotalDays"].ToString();
                        simobj.registrar = dr["register"].ToString();
                        simobj.principle = dr["principle"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_req_date"].ToString()))
                            //simobj.sims_certificate_req_date = DateTime.Parse(dr["sims_certificate_req_date"].ToString()).ToShortDateString();
                            simobj.sims_certificate_req_date = dr["sims_certificate_req_date"].ToString();
                        simobj.sims_certificate_req_status = dr["tc_status"].ToString();
                        if (!string.IsNullOrEmpty(dr["last_day"].ToString()))
                            //simobj.sims_certificate_date_of_leaving = DateTime.Parse(dr["last_day"].ToString()).ToShortDateString();
                            simobj.sims_certificate_date_of_leaving = db.UIDDMMYYYYformat(dr["last_day"].ToString());
                        simobj.sims_certificate_academic_year = dr["sims_certificate_academic_year"].ToString();
                        simobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                        simobj.sims_student_exam_result = dr["sims_student_exam_result"].ToString();
                        simobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simobj.sims_student_exam_appeared = dr["sims_student_exam_appeared"].ToString();
                        simobj.sims_student_promoted_to = dr["sims_student_promoted_to"].ToString();
                        simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                        simobj.sims_certificate_exam_year = dr["sims_certificate_exam_year"].ToString();
                        simobj.sims_student_fees_paid_lastmonth = dr["sims_student_fees_paid_lastmonth"].ToString();
                        //simobj.sims_manually_attendance = dr["sims_manually_attendance"].ToString();
                        group_list.Add(simobj);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("Get_Search_Tc_details_sisqatar")]
        public HttpResponseMessage Get_Search_Tc_details_sisqatar(string enroll_no)
        {
            List<sims505> group_list = new List<sims505>();
            if (enroll_no == "undefined" || enroll_no == "")
            {
                enroll_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                   new List<SqlParameter>()
                   {
                       new SqlParameter ("@opr", 'S'),
                       new SqlParameter ("@sims_certificate_enroll_number", enroll_no),
                   });

                    while (dr.Read())
                    {
                        sims505 simobj = new sims505();
                        simobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                        simobj.sims_certificate_name = dr["certificate_name"].ToString();
                        simobj.sims_certificate_enroll_number = dr["sims_certificate_enroll_number"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_leaving"].ToString()))
                            simobj.sims_certificate_date_of_leaving = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_leaving"].ToString());
                        simobj.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                        simobj.sims_certificate_general_conduct = dr["sims_certificate_general_conduct"].ToString();
                        simobj.sims_certificate_academic_progress = dr["sims_certificate_academic_progress"].ToString();
                        simobj.sims_certificate_subject_studied = dr["sims_certificate_subject_studied"].ToString();
                        simobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_subject_name_en"].ToString()))
                            simobj.sims_certificate_subject_studied_ar = dr["sims_subject_name_en"].ToString().Remove(dr["sims_subject_name_en"].ToString().Length - 1);
                        simobj.sims_certificate_remark = dr["sims_certificate_remark"].ToString();
                        simobj.sims_certificate_attendance_remark = dr["sims_certificate_attendance_remark"].ToString();
                        simobj.sims_certificate_qualified_for_promotion = dr["sims_certificate_qualified_for_promotion"].Equals("A") ? true : false;
                        if (!string.IsNullOrEmpty(dr["sims_certificate_date_of_issue"].ToString()))
                            simobj.sims_certificate_date_of_issue = db.UIDDMMYYYYformat(dr["sims_certificate_date_of_issue"].ToString());
                        //simobj.sims_certificate_date_of_issue = DateTime.Parse(dr["sims_certificate_date_of_issue"].ToString()).ToShortDateString();
                        simobj.sims_certificate_fee_paid = dr["sims_certificate_fee_paid"].Equals("A") ? true : false;
                        simobj.sims_certificate_sc_st_status = dr["sims_certificate_sc_st_status"].Equals("A") ? true : false;
                        simobj.sims_certificate_field1 = dr["sims_certificate_field1"].ToString();
                        simobj.sims_certificate_field2 = dr["sims_certificate_field2"].ToString();
                        simobj.sims_certificate_field3 = dr["sims_certificate_field3"].ToString();
                        //simobj.sims_certificate_field3_name = dr["sims_certificate_field3_name"].ToString();
                        simobj.sims_certificate_field4 = dr["sims_certificate_field4"].ToString();
                        simobj.sims_certificate_field5 = dr["sims_certificate_field5"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_register_no"].ToString()))
                            simobj.sims_certificate_registration_register_no = int.Parse(dr["sims_certificate_registration_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_registration_serial_no"].ToString()))
                            simobj.sims_certificate_registration_serial_no = int.Parse(dr["sims_certificate_registration_serial_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_register_no"].ToString()))
                            simobj.sims_certificate_result_register_no = int.Parse(dr["sims_certificate_result_register_no"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_certificate_result_serial_no"].ToString()))
                            simobj.sims_certificate_result_serial_no = int.Parse(dr["sims_certificate_result_serial_no"].ToString());
                        simobj.stud_name = dr["student"].ToString();
                        simobj.religion = dr["religion"].ToString();
                        simobj.nationality = dr["Nationality"].ToString();
                        simobj.date_of_admission = dr["date"].ToString();
                        //if (!string.IsNullOrEmpty(date))
                        //    simobj.date_of_admission = date.Substring(0, date.IndexOf(' '));
                        simobj.date_of_birth = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        //if (!string.IsNullOrEmpty(date1))
                        //    simobj.date_of_birth = date1.Substring(0, date1.IndexOf(' '));
                        simobj.father_name = dr["parentName"].ToString();
                        simobj.Credits = dr["sims_certificate_credit"].ToString();
                        simobj.stream = dr["stream"].ToString();
                        if (!string.IsNullOrEmpty(dr["DOBW"].ToString()))
                            simobj.dobw = dr["DOBW"].ToString();
                        simobj.father_name = dr["parentName"].ToString();
                        simobj.mother_name = dr["motherName"].ToString();
                        simobj.admitted_class = dr["admitted_grade"].ToString();
                        simobj.present_class = dr["Present_grade"].ToString();
                        simobj.present_sec = dr["Present_section"].ToString();
                        simobj.present_days = dr["TotalPresentDays"].ToString();
                        simobj.total_days = dr["TotalDays"].ToString();
                        simobj.registrar = dr["register"].ToString();
                        simobj.principle = dr["principle"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_certificate_req_date"].ToString()))
                            //simobj.sims_certificate_req_date = DateTime.Parse(dr["sims_certificate_req_date"].ToString()).ToShortDateString();
                            simobj.sims_certificate_req_date = db.UIDDMMYYYYformat(dr["sims_certificate_req_date"].ToString());
                        simobj.sims_certificate_req_status = dr["tc_status"].ToString();
                        // if (!string.IsNullOrEmpty(dr["last_day"].ToString()))
                        //   simobj.sims_certificate_date_of_leaving = db.UIDDMMYYYYformat(dr["last_day"].ToString());
                        simobj.sims_certificate_academic_year = dr["sims_certificate_academic_year"].ToString();
                        simobj.sims_student_exam_result = dr["sims_student_exam_result"].ToString();
                        simobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simobj.sims_student_exam_appeared = dr["sims_student_exam_appeared"].ToString();
                        simobj.sims_student_promoted_to = dr["sims_student_promoted_to"].ToString();
                        simobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                        simobj.sims_certificate_exam_year = dr["sims_certificate_exam_year"].ToString();
                        simobj.sims_student_fees_paid_lastmonth = dr["sims_student_fees_paid_lastmonth"].ToString();
                        group_list.Add(simobj);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        [Route("CUDCertificate_Tc_Parameter")]
        public HttpResponseMessage CUDCertificate_Tc_Parameter(List<sims505> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims505 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                     {
                        new SqlParameter("@opr", simsobj.opr),
                        new SqlParameter("@sims_certificate_name", simsobj.sims_certificate_number),
                        new SqlParameter("@sims_certificate_enroll_number", simsobj.sims_certificate_enroll_number),
                        new SqlParameter("@sims_certificate_date_of_leaving", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_leaving)),
                        new SqlParameter("@sims_certificate_reason_of_leaving", simsobj.sims_certificate_reason_of_leaving),
                        new SqlParameter("@sims_certificate_general_conduct", simsobj.sims_certificate_general_conduct),
                        new SqlParameter("@sims_certificate_academic_progress", simsobj.sims_certificate_academic_progress),
                        new SqlParameter("@sims_certificate_subject_studied", simsobj.sims_certificate_subject_studied),
                        new SqlParameter("@sims_certificate_remark", simsobj.sims_certificate_remark),
                        new SqlParameter("@sims_certificate_attendance_remark", simsobj.sims_certificate_attendance_remark),
                        new SqlParameter("@sims_certificate_qualified_for_promotion", simsobj.sims_certificate_qualified_for_promotion==true?"A":"I"),
                        new SqlParameter("@sims_certificate_date_of_issue", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_issue)),                      
                        new SqlParameter("@sims_certificate_fee_paid",simsobj.sims_certificate_fee_paid==true? "A":"I"),                    
                        new SqlParameter("@sims_certificate_sc_st_status", simsobj.sims_certificate_sc_st_status == true? "A":"I"),                        
                        new SqlParameter("@sims_certificate_field1", simsobj.sims_certificate_field1),
                        new SqlParameter("@sims_certificate_field2", simsobj.sims_certificate_field2),
                        new SqlParameter("@sims_certificate_field3", simsobj.sims_certificate_field3),
                        new SqlParameter("@sims_certificate_field4", simsobj.sims_certificate_field4),
                        new SqlParameter("@sims_certificate_field5", simsobj.sims_certificate_field5),
                        new SqlParameter("@sims_certificate_registration_register_no", simsobj.sims_certificate_registration_register_no),
                        new SqlParameter("@sims_certificate_registration_serial_no", simsobj.sims_certificate_registration_serial_no),
                        new SqlParameter("@sims_certificate_result_register_no", simsobj.sims_certificate_result_register_no),
                        new SqlParameter("@sims_certificate_result_serial_no", simsobj.sims_certificate_result_serial_no),
                        new SqlParameter("@sims_certificate_stream_name", simsobj.stream),
                        new SqlParameter("@sims_certificate_credit", simsobj.Credits),   
                        new SqlParameter("@sims_certificate_academic_year", simsobj.sims_certificate_academic_year),
                        new SqlParameter("@sims_certificate_exam_year",simsobj.sims_certificate_exam_year),
                        new SqlParameter("@sims_student_promoted_to", simsobj.sims_student_promoted_to),
                        new SqlParameter("@sims_student_fees_paid_lastmonth",simsobj.sims_student_fees_paid_lastmonth),
                        new SqlParameter("@sims_student_exam_result",simsobj.sims_student_exam_result),
                        new SqlParameter("@sims_student_exam_appeared",simsobj.sims_student_exam_appeared),                        
                        new SqlParameter("@sims_certificate_req_status",simsobj.sims_certificate_req_status),
                        new SqlParameter("@sims_birth_place",simsobj.sims_birth_place),
                        new SqlParameter("@sims_admitted_class",simsobj.admitted_class),
                        new SqlParameter("@sims_manually_attendance",simsobj.present_days),
                        new SqlParameter("@sims_working_days",simsobj.total_days),

                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Get_doc_details")]
        public HttpResponseMessage Get_doc_details(string enroll_No)
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@sims_certificate_enroll_number", enroll_No),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.sims_certificate_number = dr["doc_name"].ToString();
                            obj.sims_certificate_name = dr["sims_certificate_doc_year"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_failed_status")]
        public HttpResponseMessage Get_failed_status()
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'M'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.sims_certificate_student_fail_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_student_fail = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("Get_cast_status")]
        public HttpResponseMessage Get_cast_status()
        {
            List<sims505> lstcertificate = new List<sims505>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims505 obj = new sims505();
                            obj.sims_certificate_student_fail_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_certificate_student_fail = dr["sims_appl_parameter"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("CUDCertificate_Tc_Parameter_Saveonly")]
        public HttpResponseMessage CUDCertificate_Tc_Parameter_Saveonly(List<sims505> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims505 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_certificate_tc_parameter_details_proc_dvps]",
                        new List<SqlParameter>()
                     {
                         //Tejas
                        new SqlParameter("@opr", simsobj.opr),
                        new SqlParameter("@sims_certificate_name", simsobj.sims_certificate_number),
                        new SqlParameter("@sims_certificate_enroll_number", simsobj.sims_certificate_enroll_number),
                        new SqlParameter("@sims_certificate_date_of_leaving", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_leaving)),
                        new SqlParameter("@sims_certificate_reason_of_leaving", simsobj.sims_certificate_reason_of_leaving),
                        new SqlParameter("@sims_certificate_general_conduct", simsobj.sims_certificate_general_conduct),
                        new SqlParameter("@sims_certificate_academic_progress", simsobj.sims_certificate_academic_progress),
                        new SqlParameter("@sims_certificate_subject_studied", simsobj.sims_certificate_subject_studied),
                        new SqlParameter("@sims_certificate_remark", simsobj.sims_certificate_remark),
                        new SqlParameter("@sims_certificate_attendance_remark", simsobj.sims_certificate_attendance_remark),
                        new SqlParameter("@sims_certificate_qualified_for_promotion", simsobj.sims_certificate_qualified_for_promotion==true?"A":"I"),
                        new SqlParameter("@sims_certificate_date_of_issue", db.DBYYYYMMDDformat(simsobj.sims_certificate_date_of_issue)),                      
                        new SqlParameter("@sims_certificate_fee_paid",simsobj.sims_certificate_fee_paid==true? "A":"I"),                    
                        new SqlParameter("@sims_certificate_sc_st_status", simsobj.sims_certificate_sc_st_status == true? "A":"I"),                        
                        new SqlParameter("@sims_certificate_field1", simsobj.sims_certificate_field1),
                        new SqlParameter("@sims_certificate_field2", simsobj.sims_certificate_field2),
                        new SqlParameter("@sims_certificate_field3", simsobj.sims_certificate_field3),
                        new SqlParameter("@sims_certificate_field4", simsobj.sims_certificate_field4),
                        new SqlParameter("@sims_certificate_field5", simsobj.sims_certificate_field5),
                        new SqlParameter("@sims_certificate_registration_register_no", simsobj.sims_certificate_registration_register_no),
                        new SqlParameter("@sims_certificate_registration_serial_no", simsobj.sims_certificate_registration_serial_no),
                        new SqlParameter("@sims_certificate_result_register_no", simsobj.sims_certificate_result_register_no),
                        new SqlParameter("@sims_certificate_result_serial_no", simsobj.sims_certificate_result_serial_no),
                        new SqlParameter("@sims_certificate_stream_name", simsobj.stream),
                        new SqlParameter("@sims_certificate_credit", simsobj.Credits),   
                        new SqlParameter("@sims_certificate_academic_year", simsobj.sims_certificate_academic_year),
                        new SqlParameter("@sims_certificate_exam_year",simsobj.sims_certificate_exam_year),
                        new SqlParameter("@sims_student_promoted_to", simsobj.sims_student_promoted_to),
                        new SqlParameter("@sims_student_fees_paid_lastmonth",simsobj.sims_student_fees_paid_lastmonth),
                        new SqlParameter("@sims_student_exam_result",simsobj.sims_student_exam_result),
                        new SqlParameter("@sims_student_exam_appeared",simsobj.sims_student_exam_appeared),                        
                        new SqlParameter("@sims_certificate_req_status",simsobj.sims_certificate_req_status),
                        new SqlParameter("@sims_admitted_class",simsobj.admitted_class),
                        new SqlParameter("@sims_birth_place",simsobj.sims_birth_place),
                        new SqlParameter("@sims_manually_attendance",simsobj.sims_manually_attendance),
                        new SqlParameter("@sims_working_days",simsobj.sims_working_days),

                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}