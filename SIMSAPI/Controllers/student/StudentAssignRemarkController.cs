﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudentAssignRemark")]
    public class StudentAssignRemarkController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Student Assign Remark

        [Route("Get_Section_CodebyCuriculum")]
        public HttpResponseMessage Get_Section_CodebyCuriculum(string cur_code, string acad_yr,string grade_sec)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Section_CodebyCuriculum"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_assign_Remark_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_acad_yr", acad_yr),
                             new SqlParameter("@sections", grade_sec)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            simsobj.grade_section_name = dr["section"].ToString();
                            simsobj.grade_section_code = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

       

        [Route("GetStudentAssignRemark")]
        public HttpResponseMessage GetStudentAssignRemark(string cur_code, string acad_yr, string grade_sec)
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_assign_Remark_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_acad_yr",acad_yr),
                                new SqlParameter("@sections",grade_sec),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_enroll_no = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_name = dr["sims_student_name"].ToString();
                            simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CUDInsertAssignRemarkDetails")]
        public HttpResponseMessage CUDInsertAssignRemarkDetails(List<studAss> data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_assign_Remark_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                            new SqlParameter("@student_enrolllist", simsobj.sims_enroll_no),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("CUDsend_email")]
        public HttpResponseMessage CUDsend_email(List<studAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_assesment_schedule_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_yr", simsobj.sims_acad_yr),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_assesment_time",simsobj.sims_assesment_time),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

       

    }
}