﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/ConsistancyCheck1")]
    [BasicAuthentication]
    public class ConsitancyCheckController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getconsistancycheck1")]
        public HttpResponseMessage getconsistancycheck1()
        {

            List<Sims022> consistcheck = new List<Sims022>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_consistency_check_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims022 simsobj = new Sims022();
                            simsobj.sims_scriptid = int.Parse(dr["sims_scriptid"].ToString());
                            simsobj.sims_scriptname = dr["sims_scriptname"].ToString();
                            simsobj.sims_startdate = db.UIDDMMYYYYformat(dr["sims_startdate"].ToString());
                            simsobj.sims_nextdate = db.UIDDMMYYYYformat(dr["sims_nextdate"].ToString());
                            simsobj.sims_alertdays = dr["sims_alertdays"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_control_field = dr["sims_control_field"].ToString();
                            simsobj.sims_alert_name = dr["sims_alert_name"].ToString();
                            simsobj.sims_alert_desc = dr["sims_alert_desc"].ToString();
                            simsobj.sims_alert_time = dr["sims_alert_time"].ToString();
                            simsobj.sims_alert_frequncy = dr["sims_alert_frequncy"].ToString();
                            simsobj.sims_alert_frequncy_time = dr["sims_alert_frequncy_time"].ToString();
                           // simsobj.sims_alert_frequncy_update_count = dr["sims_alert_frequncy_update_count"].ToString();
                           // simsobj.sims_alert_frequncy_time_to_add = dr["sims_alert_frequncy_time_to_add"].ToString();
                            simsobj.sims_alert_due_days = dr["sims_alert_due_days"].ToString();

                            consistcheck.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, consistcheck);
            }
            return Request.CreateResponse(HttpStatusCode.OK, consistcheck);
        }

        [Route("consistancycheck1in")]
        public HttpResponseMessage consistancycheck1in(List<Sims022> data)
        {
          bool inserted = false;
              try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                     foreach (Sims022 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_consistency_check_proc]",
                            new List<SqlParameter>()
                            {
                               new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_scriptid", simsobj.sims_scriptid),
                                new SqlParameter("@sims_scriptname", simsobj.sims_scriptname),
                                new SqlParameter("@sims_startdate", db.DBYYYYMMDDformat(simsobj.sims_startdate)),
                                new SqlParameter("@sims_nextdate", db.DBYYYYMMDDformat(simsobj.sims_nextdate)),
                                new SqlParameter("@sims_alertdays", simsobj.sims_alertdays),
                                new SqlParameter("@sims_status", simsobj.sims_status==true?"Y":"N"),
                                new SqlParameter("@sims_control_field",simsobj.sims_control_field),
                                new SqlParameter("@sims_alert_name", simsobj.sims_alert_name),
                                new SqlParameter("@sims_alert_desc", simsobj.sims_alert_desc),
                                //new SqlParameter("@sims_alert_name", simsobj.sims_alert_name),
                                new SqlParameter("@sims_alert_time", simsobj.sims_alert_time),
                                new SqlParameter("@sims_alert_frequncy", simsobj.sims_alert_frequncy),
                                new SqlParameter("@sims_alert_frequncy_time", simsobj.sims_alert_frequncy_time),
                              //  new SqlParameter("@sims_alert_frequncy_update_count", simsobj.sims_alert_frequncy_update_count),
                              //  new SqlParameter("@sims_alert_frequncy_time_to_add", simsobj.@sims_alert_frequncy_time_to_add),
                                new SqlParameter("@sims_alert_due_days", simsobj.@sims_alert_due_days),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getScriptId")]
        public HttpResponseMessage getScriptId(List<Sims022> data)
        {
            Sims022 obj = new Sims022();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_consistency_check_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                      });
                    while (dr.Read())
                    {
                        obj.sims_scriptid = int.Parse(dr["sims_scriptid"].ToString());
                    
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, obj.sims_scriptid);

            }
            return Request.CreateResponse(HttpStatusCode.OK, obj.sims_scriptid);
        }

        [Route("getalertfrequency")]
        public HttpResponseMessage getalertfrequency(List<Sims022> data)
        {
            
               List<Sims022> doc_list = new List<Sims022>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_consistency_check_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter ("@opr", "F"),


                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims022 simsobj = new Sims022();
                        
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    }
            }
            catch (Exception x)
            {
               
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_list);

        }



    }
}