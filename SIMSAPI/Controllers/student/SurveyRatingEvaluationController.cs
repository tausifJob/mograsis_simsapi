﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Web;
using System.IO;
namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/SurveyRatingEval")]
    public class SurveyRatingEvaluationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

       
        [Route("SurveyRatingDetails")]
        public HttpResponseMessage SurveyRatingDetails(string sur_code,string sdate,string edate)
        {
            if (sdate == "undefined" || sdate == "\"\"")
            {
                sdate = null;
            }
            if (edate == "undefined" || edate == "\"\"")
            {
                edate = null;
            }
            HttpStatusCode s = HttpStatusCode.OK;
            List<survey_eval> type_list = new List<survey_eval>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'M'),
                       new SqlParameter ("@sims_survey_code", sur_code),
                       new SqlParameter ("@start_date", db.DBYYYYMMDDformat(sdate)),
                       new SqlParameter ("@end_date", db.DBYYYYMMDDformat(edate))
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                survey_eval simsobj = new survey_eval();
                                simsobj.sims_survey_user_response_srl_no = dr["sims_survey_user_response_srl_no"].ToString();
                                simsobj.sims_survey_user_response_user_code = dr["user_code"].ToString();
                                simsobj.sims_survey_code = dr["sims_survey_code"].ToString();
                                simsobj.sims_survey_question_code= dr["sims_survey_question_code"].ToString();
                                simsobj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                                simsobj.sims_survey_user_response_answer_code = dr["sims_survey_user_response_answer_code"].ToString();
                                simsobj.sims_survey_rating_desc= dr["sims_survey_rating_desc"].ToString();
                                simsobj.sims_survey_rating_point = dr["sims_survey_rating_point"].ToString();
                                simsobj.sims_survey_rating_img_path = dr["sims_survey_rating_img_path"].ToString();
                                simsobj.sims_survey_student_enroll_number = dr["enroll_number"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_survey_user_response_start_time"].ToString()))
                                    simsobj.sims_survey_user_response_start_time = dr["sims_survey_user_response_start_time"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_survey_user_response_end_time"].ToString()))
                                    simsobj.sims_survey_user_response_end_time = dr["sims_survey_user_response_end_time"].ToString();
                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }
    }
}