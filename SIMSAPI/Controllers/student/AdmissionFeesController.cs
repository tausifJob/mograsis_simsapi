﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
//using SIMSAPI.Models.ERP.messageClass;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using SIMSAPI.Attributes;
//using SIMSAPI.Helper;
//using System.Data.SqlClient;
//using System.Data;
//using log4net;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/AdmissionFees")]
    public class AdmissionFeesController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getAdmissionFees")]
        public HttpResponseMessage getAdmissionFees(string cur_code, string academic_year, string grade_code, string admission_no_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAdmissionFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAdmissionFees"));

            if (cur_code == "undefined")
                cur_code = null;
            if (academic_year == "undefined")
                academic_year = null;
            if (grade_code == "undefined")
                grade_code = null;
            if (admission_no_name == "undefined")
                admission_no_name = null;
            List<Sims521> goaltarget_list = new List<Sims521>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                       new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_admission_no", admission_no_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims521 objNew = new Sims521();
                            objNew.sims_academic_year = dr["sims_admission_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_admission_no = dr["sims_admission_number"].ToString();
                            objNew.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            objNew.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            objNew.sims_grade_code = dr["sims_admission_grade_code"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_grade_fee_amount = dr["sims_grade_fee_amount"].ToString();
                            objNew.sims_student_full_name = dr["student_full_name"].ToString();
                            objNew.sims_parent_full_name = dr["parent_full_name"].ToString();
                            objNew.sims_fee_amount = dr["paid_amount"].ToString();
                            objNew.sims_doc_status = dr["sims_doc_status"].ToString();
                            objNew.sims_doc_status_desc = dr["sims_doc_status_desc"].ToString();
                            objNew.sims_doc_status_content = dr["sims_doc_status_content"].ToString();
                            objNew.sims_doc_no = dr["sims_doc_no"].ToString();
                            objNew.sims_doc_remark = dr["sims_doc_remark"].ToString();
                            goaltarget_list.Add(objNew);


                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAdmissionGradeFees")]
        public HttpResponseMessage getAdmissionGradeFees(string cur_code, string academic_year, string grade_code, string admission_no_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAdmissionGradeFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAdmissionGradeFees"));

            if (cur_code == "undefined")
                cur_code = null;
            if (academic_year == "undefined")
                academic_year = null;
            if (grade_code == "undefined")
                grade_code = null;
            if (admission_no_name == "undefined")
                admission_no_name = null;
            List<Sims521> goaltarget_list = new List<Sims521>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_admission_no", admission_no_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims521 objNew = new Sims521();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_fee_amount = dr["sims_fee_amount"].ToString();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            objNew.sims_rev_acc_no = dr["sims_rev_acc_no"].ToString();
                            objNew.sims_cash_acc_no = dr["sims_cash_acc_no"].ToString();
                            objNew.sims_fee_status = dr["sims_fee_status"].ToString();



                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getBankDeatails")]
        public HttpResponseMessage getBankDeatails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBankDeatails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getBankDeatails"));

            List<Sims521> goaltarget_list = new List<Sims521>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims521 objNew = new Sims521();
                            objNew.slma_pty_bank_id = dr["pb_bank_code"].ToString();
                            objNew.slma_pty_bank_name = dr["pb_bank_code"].ToString() + "-" + dr["pb_bank_name"].ToString();
                            objNew.pb_gl_acno = dr["pb_gl_acno"].ToString();
                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("insertAdmissionFees")]
        public HttpResponseMessage insertAdmissionFees(List<Sims521> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertAdmissionFee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "insertAdmissionFee"));

            Message message = new Message();
            int cnt = 1;
            bool insert = false;
            string ref_no = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims521 simsobj in data)
                    {
                        // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_admission_no", simsobj.sims_admission_no),
                                new SqlParameter("@sims_doc_no", simsobj.sims_doc_no),
                                new SqlParameter("@sims_doc_date", db.DBYYYYMMDDformat(simsobj.sims_doc_date)),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                                new SqlParameter("@sims_fee_amount", simsobj.sims_grade_fee_amount),
                                new SqlParameter("@sims_user_name", simsobj.sims_user_name),
                                new SqlParameter("@sims_doc_status", simsobj.sims_doc_status),                               
                                new SqlParameter("@sr_no", cnt),


                                new SqlParameter("@sims_fee_payment_mode", simsobj.sims_fee_payment_mode),
                                new SqlParameter("@sims_fee_cheque_bank_code", simsobj.sims_fee_cheque_bank_code),
                                new SqlParameter("@sims_fee_cheque_number", simsobj.sims_fee_cheque_number),
                                new SqlParameter("@sims_fee_cheque_date", db.DBYYYYMMDDformat(simsobj.sims_fee_cheque_date)),
                                new SqlParameter("@sims_doc_remark", simsobj.sims_doc_remark),
                               
                     });
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["doc_no"].ToString()))
                                    ref_no = dr["doc_no"].ToString();
                            }
                        }
                        cnt = cnt + 1;
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ref_no);
                }

            }

            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, ref_no);
        }


        [Route("getAdmissionGradeFeesCancel")]
        public HttpResponseMessage getAdmissionGradeFeesCancel(string cur_code, string academic_year, string grade_code, string admission_no_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAdmissionGradeFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAdmissionGradeFees"));

            if (cur_code == "undefined")
                cur_code = null;
            if (academic_year == "undefined")
                academic_year = null;
            if (grade_code == "undefined")
                grade_code = null;
            if (admission_no_name == "undefined")
                admission_no_name = null;
            List<Sims521> goaltarget_list = new List<Sims521>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_admission_no", admission_no_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims521 objNew = new Sims521();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_fee_amount = dr["sims_fee_amount"].ToString();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            //objNew.sims_rev_acc_no = dr["sims_rev_acc_no"].ToString();
                            //objNew.sims_cash_acc_no = dr["sims_cash_acc_no"].ToString();
                            //objNew.sims_fee_status = dr["sims_fee_status"].ToString();

                            objNew.sr_no = dr["sr_no"].ToString();

                            objNew.sims_doc_no = dr["sims_doc_no"].ToString();

                            objNew.sims_doc_date = dr["sims_doc_date"].ToString();
                            objNew.sims_admission_no = dr["sims_admission_no"].ToString();
                            objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            objNew.sims_fee_payment_mode = dr["sims_fee_payment_mode"].ToString();
                            objNew.sims_fee_payment_desc = dr["sims_fee_payment_desc"].ToString();

                            objNew.sims_fee_cheque_bank_code = dr["sims_fee_cheque_bank_code"].ToString();
                            objNew.sims_fee_cheque_number = dr["sims_fee_cheque_number"].ToString();
                            objNew.sims_fee_cheque_date = dr["sims_fee_cheque_date"].ToString();

                            objNew.sims_user_name = dr["sims_user_name"].ToString();
                           
                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("insertAdmissionFeesCancel")]
        public HttpResponseMessage insertAdmissionFeesCancel(List<Sims521> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertAdmissionFee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "insertAdmissionFee"));

            Message message = new Message();
            int cnt = 1;
            bool insert = false;
            string ref_no = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims521 simsobj in data)
                    {
                        // SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_admission_no", simsobj.sims_admission_no),
                                new SqlParameter("@sims_doc_no", simsobj.sims_doc_no),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                               new SqlParameter("@sims_user_name", simsobj.sims_user_name),
                               new SqlParameter("@sr_no", cnt),
                                new SqlParameter("@sims_doc_status", simsobj.sims_doc_status),                               

                                new SqlParameter("@sims_fee_amount", simsobj.sims_fee_amount),
                                new SqlParameter("@sims_fee_payment_mode", simsobj.sims_fee_payment_mode),
                                new SqlParameter("@sims_fee_cheque_bank_code", simsobj.sims_fee_cheque_bank_code),
                                new SqlParameter("@sims_doc_date", db.DBYYYYMMDDformat(simsobj.sims_doc_date)),
                                 new SqlParameter("@sims_fee_cheque_number", simsobj.sims_fee_cheque_number),
                                new SqlParameter("@sims_fee_cheque_date", db.DBYYYYMMDDformat(simsobj.sims_fee_cheque_date)),

                              
                               
                     });
                        int r = dr.RecordsAffected;
                        insert = r > 0 ? true : false;

                        cnt = cnt + 1;
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }

            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, ref_no);
        }


        [Route("getAdmissionFeesGeis")]
        public HttpResponseMessage getAdmissionFeesGeis(string cur_code, string academic_year, string grade_code, string admission_no_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAdmissionFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAdmissionFees"));

            if (cur_code == "undefined")
                cur_code = null;
            if (academic_year == "undefined")
                academic_year = null;
            if (grade_code == "undefined")
                grade_code = null;
            if (admission_no_name == "undefined")
                admission_no_name = null;
            List<Sims521> goaltarget_list = new List<Sims521>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_document_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_fee_document",
                       new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_cur_code",cur_code),
                           new SqlParameter("@sims_academic_year",academic_year),
                           new SqlParameter("@sims_grade_code", grade_code),
                           new SqlParameter("@sims_admission_no", admission_no_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims521 objNew = new Sims521();
                            objNew.sims_academic_year = dr["sims_admission_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_admission_no = dr["sims_admission_number"].ToString();
                            objNew.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            objNew.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            objNew.sims_grade_code = dr["sims_admission_grade_code"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_grade_fee_amount = dr["sims_grade_fee_amount"].ToString();
                            objNew.sims_student_full_name = dr["student_full_name"].ToString();
                            objNew.sims_parent_full_name = dr["parent_full_name"].ToString();
                            objNew.sims_fee_amount = dr["paid_amount"].ToString();
                            objNew.sims_doc_status = dr["sims_doc_status"].ToString();
                            objNew.sims_doc_status_desc = dr["sims_doc_status_desc"].ToString();
                            objNew.sims_doc_status_content = dr["sims_doc_status_content"].ToString();
                            objNew.sims_doc_no = dr["sims_doc_no"].ToString();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();

                            objNew.sims_doc_no = dr["sims_doc_no"].ToString();
                            objNew.sims_doc_date = dr["sims_doc_date"].ToString();
                            objNew.sims_user_name = dr["sims_user_name"].ToString();
                            goaltarget_list.Add(objNew);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

    }
}