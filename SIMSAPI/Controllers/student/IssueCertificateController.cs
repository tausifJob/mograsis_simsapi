﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/IssueCertificate")]
    public class IssueCertificateController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        [Route("getIssueCertificate")]
        public HttpResponseMessage getIssueCertificate(string opr1,string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getIssueCertificate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getIssueCertificate"));

            List<Sims105> goaltarget_list = new List<Sims105>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_issue_proc]",
                    //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                       new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@opr_select", 'A'),
                           new SqlParameter("@opr1", opr1),
                           new SqlParameter("@sims_certificate_issue_number", ""),
                           new SqlParameter("@sims_certificate_issue_date", ""),
                           new SqlParameter("@sims_certificate_number", ""),
                           new SqlParameter("@sims_certificate_issue_person_number", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_certificate_issue_number = dr["sims_certificate_issue_number"].ToString();
                           if (!string.IsNullOrEmpty(dr["sims_certificate_issue_date"].ToString()))
                               //simsobj.sims_certificate_issue_date = ((DateTime)dr["sims_certificate_issue_date"]).ToShortDateString();

                           simsobj.sims_certificate_issue_date = dr["sims_certificate_issue_date"].ToString();

                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_issue_person_number = dr["sims_certificate_issue_person_number"].ToString();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_name_display = dr["sims_certificate_request_number"].ToString();
                            if (dr["sims_certificate_print_flag"].ToString().Equals("A"))
                                simsobj.sims_certificate_print_flag = true;
                            else
                                simsobj.sims_certificate_print_flag = false;
                            simsobj.sims_certificate_print_count = dr["sims_certificate_print_count"].ToString();
                            simsobj.sims_certificate_doc_path = dr["sims_certificate_doc_path"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDIssueCertificate")]
        public HttpResponseMessage CUDIssueCertificate(List<Sims105> data)
        {
            bool insert = false;
            string status;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims105 obj in data)
                    {
                        if (obj.sims_certificate_print_flag == true)
                            status = "A";
                        else
                            status = "I";
                        

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_certificate_issue_proc]",
                        //int ins = db.ExecuteStoreProcedureforInsert("sims_certificate_issue",
                        new List<SqlParameter>()
                     {

                               
                             new SqlParameter("@opr1",obj.opr1),
                             new SqlParameter("@opr", obj.opr),
                             new SqlParameter("@opr_select", ""),
                             new SqlParameter("@sims_certificate_issue_number", obj.sims_certificate_issue_number),
                           //if (!string.IsNullOrEmpty(obj.sims_certificate_issue_date))
                              new SqlParameter("@sims_certificate_issue_date", db.DBYYYYMMDDformat(obj.sims_certificate_issue_date)),
                             new SqlParameter("@sims_certificate_number", obj.sims_certificate_number),
                             new SqlParameter("@sims_certificate_issue_person_number", obj.sims_certificate_issue_person_number),
                             new SqlParameter("@sims_certificate_request_number", obj.sims_certificate_request_number),
                             new SqlParameter("@sims_certificate_print_count", obj.sims_certificate_print_count),
                             new SqlParameter("@sims_certificate_doc_path", obj.sims_certificate_doc_path),
                             //new SqlParameter("@comn_sequence_code", comn_sequence_code),
                             new SqlParameter("@sims_certificate_print_flag", status),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getUserType")]
        public HttpResponseMessage getUserType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getUserType"));

            List<Sims105> goaltarget_list = new List<Sims105>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_issue_proc]",
                    //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr1", "S"),
                            new SqlParameter("@opr", "S"),
                           new SqlParameter("@opr_select", "S"),
                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.user_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.user_type = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);
                           
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getIssueNo")]
        public HttpResponseMessage getIssueNo(string user_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getIssueNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getIssueNo"));

            List<Sims105> goaltarget_list = new List<Sims105>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_issue_proc]",
                    //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", user_name),
                           new SqlParameter("@opr_select", ""),
                           new SqlParameter("@opr1", "R"),
                           new SqlParameter("@sims_certificate_issue_number", ""),
                           new SqlParameter("@sims_certificate_issue_date", ""),
                            new SqlParameter("@sims_certificate_number", ""),
                            
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_certificate_name_display = dr["sims_certificate_requested_name"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_issue_person_number = dr["sims_certificate_requested_by"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAllRequestCertificateNo")]
        public HttpResponseMessage getAllRequestCertificateNo(string user_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRequestCertificateNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllRequestCertificateNo"));

            List<Sims105> goaltarget_list = new List<Sims105>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_issue_proc]",
                    //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr1", "R"),
                            new SqlParameter("@opr", user_name),
                            new SqlParameter("@opr_select", ""),
                            new SqlParameter("@sims_certificate_issue_number", ""),
                            new SqlParameter("@sims_certificate_issue_date", ""),
                            new SqlParameter("@sims_certificate_number", ""),
                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_certificate_name_display = dr["sims_certificate_requested_name"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_issue_person_number = dr["sims_certificate_requested_by"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

    }
}