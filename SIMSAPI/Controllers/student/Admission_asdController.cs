﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/Admission_asd")]
    public class Admission_asdController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region admission_report
        [Route("GetadmCurName")]
        public HttpResponseMessage GetadmCurName()
        {
            List<sims_admission_status_rpt> mod_list = new List<sims_admission_status_rpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_status_rpt simsobj = new sims_admission_status_rpt();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetadmAcademicYears")]
        public HttpResponseMessage GetadmAcademicYears(string cur_code)
        {
            List<sims_admission_status_rpt> mod_list = new List<sims_admission_status_rpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_status_rpt objNew = new sims_admission_status_rpt();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetadmGrades")]
        public HttpResponseMessage GetadmGrades(string cur_code, string ac_year)
        {
            List<sims_admission_status_rpt> mod_list = new List<sims_admission_status_rpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_status_rpt objNew = new sims_admission_status_rpt();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("Getadmstatus")]
        public HttpResponseMessage Getadmstatus()
        {
            List<sims_admission_status_rpt> mod_list = new List<sims_admission_status_rpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_status_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'P')
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_status_rpt objNew = new sims_admission_status_rpt();
                            objNew.sims_application_status_code = dr["sims_appl_form_field_value1"].ToString();
                            objNew.sims_application_status_name = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("AdmissionData")]
        public HttpResponseMessage AdmissionData(sims_admission_status_rpt_param obj)
        {
            List<sims_admission_status_rpt> mod_list = new List<sims_admission_status_rpt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_status_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@acad_year", obj.sims_academic_year),
                                new SqlParameter("@grade_code", obj.sims_grade_code),
                                new SqlParameter("@admn_status", obj.sims_application_status_code),
                                new SqlParameter("@st_date", db.DBYYYYMMDDformat(obj.sims_start_date)),
                                new SqlParameter("@end_date", db.DBYYYYMMDDformat(obj.sims_end_date)),
                                new SqlParameter("@admn_no", obj.admission_no)

                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_status_rpt o = new sims_admission_status_rpt();
                            o.admission_no = dr["sims_admission_number"].ToString();
                            //o.admission_date = DateTime.Parse(dr["sims_admission_date"].ToString()).ToShortDateString();
                            o.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            o.student_enrollno = dr["sims_student_enroll_number"].ToString();
                            o.student_name = dr["student_name_en"].ToString();
                            o.admission_grade = dr["admission_grade_name"].ToString();
                            o.admission_section = dr["admission_section_name"].ToString();
                            o.gender = dr["gender"].ToString();
                            o.father_name = dr["father_name"].ToString();
                            o.mother_name = dr["mother_name"].ToString();
                            o.parent_email = dr["parent_email"].ToString();
                            o.student_dob = dr["sims_admission_dob"].ToString();
                            o.religion = dr["sims_religion_name_en"].ToString();
                            o.nationality = dr["sims_nationality_name_en"].ToString();
                            o.sibling_name = dr["sims_admission_sibling_name"].ToString();
                            o.sibling_grade = dr["sibling_grade_name"].ToString();
                            o.sibling_section = dr["sibling_section_name"].ToString();

                            mod_list.Add(o);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        #endregion

        [Route("getAdmissionList")]
        public HttpResponseMessage getAdmissionList()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "ATTENDANCECRITERIA"));
            List<Sims010_Edit> listall = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[GetAdmissionData_Temp_proc]",
                        new List<SqlParameter>()
                        {
                            // new SqlParameter("@opr", 'S'),[sims].[GetAdmissionData_Temp_proc]

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit getlist = new Sims010_Edit();
                            {
                                getlist.school_code = dr["lic_school_code"].ToString();
                                getlist.school_name = dr["lic_school_name"].ToString();
                                //getlist.sibling_school_code = getlist.school_code;
                                //getlist.sibling_school_name = getlist.school_name;
                                //getlist.employee_school_code = getlist.school_code;
                                //getlist.employee_school_name = getlist.school_name;
                            }
                            {
                                getlist.curr_code = dr["sims_cur_code"].ToString();
                                getlist.curr_name = dr["sims_cur_full_name_en"].ToString();
                            }
                            {
                                getlist.birth_country_code = dr["sims_country_code"].ToString();
                                getlist.birth_country = dr["sims_country_name_en"].ToString();
                                getlist.father_country_code = getlist.birth_country_code;
                                getlist.father_country = getlist.birth_country;
                                getlist.mother_country_code = getlist.birth_country_code;
                                getlist.mother_country = getlist.birth_country;
                                getlist.guardian_country_code = getlist.birth_country_code;
                                getlist.guardian_country = getlist.birth_country;
                                getlist.current_school_country_code = getlist.birth_country_code;
                                getlist.current_school_country = getlist.birth_country;
                            }
                            {

                                getlist.nationality_code = dr["sims_nationality_code"].ToString();
                                getlist.nationality = dr["sims_nationality_name_en"].ToString();
                                getlist.father_nationality1_code = getlist.nationality_code;
                                getlist.father_nationality1_desc = getlist.nationality;
                                getlist.father_nationality2_code = getlist.nationality_code;
                                getlist.father_nationality2_desc = getlist.nationality;

                                getlist.mother_nationality1_code = getlist.nationality_code;
                                getlist.mother_nationality1_desc = getlist.nationality;
                                getlist.mother_nationality2_code = getlist.nationality_code;
                                getlist.mother_nationality2_desc = getlist.nationality;

                                getlist.guardian_nationality1_code = getlist.nationality_code;
                                getlist.guardian_nationality1_desc = getlist.nationality;
                                getlist.guardian_nationality2_code = getlist.nationality_code;
                                getlist.guardian_nationality2_desc = getlist.nationality;
                            }

                            {
                                getlist.main_language_code = dr["sims_language_code"].ToString();
                                getlist.main_language_desc = dr["sims_language_name_en"].ToString();
                                getlist.other_language_code = getlist.main_language_code;
                                getlist.other_language = getlist.main_language_desc;
                            }
                            //if (string.IsNullOrEmpty(dr[12].ToString()) == false)
                            {
                                getlist.ethinicity_code = dr["sims_ethnicity_code"].ToString();
                                getlist.ethinicity = dr["sims_ethnicity_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[13].ToString()) == false)
                            {
                                getlist.religion_code = dr["sims_religion_code"].ToString();
                                getlist.religion_desc = dr["sims_religion_name_en"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[15].ToString()) == false)
                            {
                                getlist.gender_code = dr["sims_appl_parameter_gender"].ToString();
                                getlist.gender_desc = dr["sims_appl_form_field_value1_gender"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[17].ToString()) == false)
                            {
                                getlist.legal_custody_code = dr["sims_appl_parameter_legal"].ToString();
                                getlist.legal_custody_name = dr["sims_appl_form_field_value1_legal"].ToString();
                                getlist.fee_payment_contact_pref_code = getlist.legal_custody_code;
                                getlist.fee_payment_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_desc = getlist.legal_custody_name;
                                getlist.primary_contact_pref_code = getlist.legal_custody_code;
                            }
                            //if (string.IsNullOrEmpty(dr[19].ToString()) == false)
                            {
                                getlist.blood_group_code = dr["sims_appl_parameter_blood"].ToString();
                                getlist.blood_group_desc = dr["sims_appl_form_field_value1_blood"].ToString();
                            }
                            //if (string.IsNullOrEmpty(dr[21].ToString()) == false) 
                            //Salutation Code
                            {
                                getlist.mother_salutation_code = dr["sims_appl_parameter_salut"].ToString();
                                getlist.mother_salutation_desc = dr["sims_appl_form_field_value1_salut"].ToString();
                                getlist.father_salutation_code = getlist.mother_salutation_code;
                                getlist.father_salutation_desc = getlist.mother_salutation_desc;
                                getlist.guardian_salutation_code = getlist.mother_salutation_code;
                                getlist.guardian_salutation_desc = getlist.mother_salutation_desc;
                            }
                            {
                                getlist.academic_year = dr["sims_acadmic_year"].ToString();
                                getlist.academic_year_desc = dr["sims_acadmic_year_desc"].ToString();
                                getlist.acdemic_year_status = dr["sims_acadmic_year_status"].ToString();
                                getlist.Aycur = dr["AyCur"].ToString();
                            }
                            {
                                getlist.grade_code = dr["sims_grade_code"].ToString();
                                getlist.grade_name = dr["sims_grade_name"].ToString();
                                getlist.GrCur = dr["GrCur"].ToString();
                                getlist.GrAca = dr["GrAca"].ToString();
                            }
                            {
                                getlist.section_code = dr["sims_section_code"].ToString();
                                getlist.section_name = dr["sims_section_name"].ToString();
                                getlist.SecAcy = dr["secAca"].ToString();
                                getlist.SecCur = dr["SecCur"].ToString();
                                getlist.SecGr = dr["SecGr"].ToString();
                                getlist.secgender = dr["SecGender"].ToString();
                            }
                            {
                                getlist.term_code = dr["sims_term_code"].ToString();
                                getlist.term_name = dr["sims_term_desc"].ToString();
                                getlist.termCur = dr["termCur"].ToString();
                                getlist.termAy = dr["TermAy"].ToString();
                            }
                            {
                                getlist.fee_category_code = dr["sims_fee_category_code"].ToString();
                                getlist.fee_category_desc = dr["sims_fee_category_desc"].ToString();
                            }
                            {
                                getlist.main_language_r_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_r = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_w_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_w = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                                getlist.main_language_s_code = dr["sims_appl_parameter_Lang_Prof"].ToString();
                                getlist.main_language_s = dr["sims_appl_form_field_value1_Lang_Prof"].ToString();
                            }
                            {
                                getlist.visa_type_code = dr["sims_appl_parameter_Visa_code"].ToString();
                                getlist.visa_type_desc = dr["sims_appl_form_field_value1_Visa_type"].ToString();
                                getlist.quota_code = dr["quota_code"].ToString();
                                getlist.sims_admission_quota_code = dr["sims_quota_desc"].ToString();
                                getlist.sims_fee_month_code = dr["sims_fee_month_code"].ToString();
                                getlist.sims_fee_month_name = dr["sims_fee_month_name"].ToString();

                                getlist.sims_national_id_code = dr["sims_national_id_code"].ToString();
                                getlist.sims_national_id_name = dr["sims_national_id_name"].ToString();
                            }
                            {
                                getlist.houseCur = dr["house_cur"].ToString();
                                getlist.houseAca = dr["house_aca"].ToString();
                                getlist.sims_house_code = dr["sims_house_code"].ToString();
                                getlist.sims_house_name = dr["sims_house_name"].ToString();
                            }

                            //try
                            //{
                            //    getlist.comn_seq = dr["comn_seq"].ToString();
                            //}
                            //catch (Exception ex1)
                            //{
                            //}


                            listall.Add(getlist);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, listall);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("CheckParentCode")]
        public HttpResponseMessage CheckParentCode(string parent_id, string enroll_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckTypeCode(),PARAMETERS :: parent_id{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckParentCode", parent_id));

            bool ifexists = false;
            List<Sims010_Edit> parent = new List<Sims010_Edit>();
            Sims010_Edit simsobj = new Sims010_Edit();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'X'),
                                 new SqlParameter("@sims_parent_number", parent_id),
                                  new SqlParameter("@sims_enroll_number", enroll_no)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;
                        while (dr.Read())
                        {
                            simsobj.parent_id = dr["sims_parent_number"].ToString();
                            simsobj.father_email = dr["sims_parent_father_email"].ToString();
                            simsobj.father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.father_first_name = dr["sims_parent_father_first_name"].ToString();
                            simsobj.father_middle_name = dr["sims_parent_father_middle_name"].ToString();
                            simsobj.father_last_name = dr["sims_parent_father_last_name"].ToString();
                            simsobj.father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sibling_name = dr["student_name"].ToString();
                            simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                            simsobj.father_nationality1_code = dr["sims_parent_father_nationality1_code"].ToString();
                            simsobj.father_nationality1_desc = dr["father_nationality"].ToString();

                            simsobj.mother_email = dr["sims_parent_mother_email"].ToString();
                            simsobj.mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.mother_first_name = dr["sims_parent_mother_first_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_parent_mother_middle_name"].ToString();
                            simsobj.mother_last_name = dr["sims_parent_mother_last_name"].ToString();
                            simsobj.mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                            simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_parent_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality1_desc = dr["mother_nationality"].ToString();

                            simsobj.guardian_email = dr["sims_parent_guardian_email"].ToString();
                            simsobj.guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                            simsobj.guardian_first_name = dr["sims_parent_guardian_first_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_parent_guardian_middle_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_parent_guardian_last_name"].ToString();
                            simsobj.guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                            simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_parent_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality1_desc = dr["guardian_nationality"].ToString();
                        }

                    }
                }

                if (ifexists)
                {

                    simsobj.chkstatus = true;
                    // simsobj.father_email=dr[].t
                    //simsobj.strMessage = "Parent Already Exists";
                    parent.Add(simsobj);
                }
                else
                {
                    simsobj.chkstatus = false;
                    simsobj.strMessage = "No Records Found";
                    parent.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj);
            }
        }

        [Route("CUDInsertAdmission")]
        public HttpResponseMessage CUDInsertAdmission(List<Sims010_Edit> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDInsertAdmission"));
            Sims010_Edit res = new Sims010_Edit();

            // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            new SqlParameter("@sims_admission_application_number", simsobj.appl_num),
                            new SqlParameter("@sims_admission_pros_number", simsobj.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", simsobj.pros_appl_num),
                            new SqlParameter("@sims_admission_date",db.DBYYYYMMDDformat(simsobj.admission_date)),
                            new SqlParameter("@sims_student_ea_number",simsobj.sims_student_ea_number),
                            #region
                            new SqlParameter("@sims_admission_school_code", simsobj.school_code),
                            new SqlParameter("@sims_admission_cur_code", simsobj.curr_code),
                            new SqlParameter("@sims_admission_academic_year", simsobj.academic_year),
                            new SqlParameter("@sims_admission_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_admission_section_code", simsobj.section_code),
                            new SqlParameter("@sims_admission_term_code", simsobj.term_code),
                            new SqlParameter("@sims_fee_month_code",simsobj.sims_fee_month_code),
                            new SqlParameter("@sims_house_code",simsobj.sims_house_code),
                            new SqlParameter("@sims_admission_tentative_joining_date",db.DBYYYYMMDDformat(simsobj.tent_join_date)),
                            new SqlParameter("@sims_student_emergency_contact_number1",simsobj.sims_student_emergency_contact_number1),
                            new SqlParameter("@sims_student_emergency_contact_name1",simsobj.sims_student_emergency_contact_name1),
                            //try
                            //{
                            //    new SqlParameter("@sims_admission_tentative_joining_date", DateTime.Parse(simsobj.tent_join_date, dateFormat.FormatProvider)),
                            //}
                            //catch (Exception ex)
                            //{
                            //    new SqlParameter("@sims_admission_tentative_joining_date", ""),
                            //}
                            new SqlParameter("@sims_admission_passport_first_name_en", simsobj.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", simsobj.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", simsobj.last_name),
                            new SqlParameter("@sims_admission_family_name_en", simsobj.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", simsobj.first_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", simsobj.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", simsobj.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_nickname", simsobj.nicke_name),
                            new SqlParameter("@sims_admission_dob",db.DBYYYYMMDDformat(simsobj.birth_date)),
                            new SqlParameter("@sims_admission_commencement_date",db.DBYYYYMMDDformat(simsobj.comm_date)),
                            //try
                            //{
                            //    new SqlParameter("@sims_admission_dob",simsobj.birth_date),
                            //}
                            //catch (Exception ex)
                            //{
                            //    new SqlParameter("@sims_admission_dob", ""),
                            //}
                            //try
                            //{
                            //    new SqlParameter("@sims_admission_commencement_date",simsobj.comm_date),

                            //}
                            //catch (Exception ex)
                            //{
                            //    new SqlParameter("@sims_admission_commencement_date", ""),

                            //}
                            new SqlParameter("@sims_admission_birth_country_code", simsobj.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", simsobj.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", simsobj.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", simsobj.gender_code),
                            new SqlParameter("@sims_admission_religion_code", simsobj.religion_code),
                            new SqlParameter("@sims_admission_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date",db.DBYYYYMMDDformat(simsobj.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.passport_expiry)),
                            //try
                            //{
                            //    new SqlParameter("@sims_admission_passport_issue_date",simsobj.passport_issue_date),
                            //}
                            //catch (Exception ex)
                            //{
                            //    new SqlParameter("@sims_admission_passport_issue_date", ""),
                            //}
                            //try
                            //{
                            //    new SqlParameter("@sims_admission_passport_expiry_date",simsobj.passport_expiry),
                            //}
                            //catch (Exception ex)
                            //{
                            //    new SqlParameter("@sims_admission_passport_expiry_date", ""),
                            //}
                            new SqlParameter("@sims_admission_passport_issuing_authority", simsobj.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", simsobj.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", simsobj.visa_number),
                            new SqlParameter("@sims_admission_visa_type", simsobj.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", simsobj.visa_issuing_authority),
                             new SqlParameter("@sims_admission_visa_issue_date",db.DBYYYYMMDDformat(simsobj.visa_issue_date)),
                             new SqlParameter("@sims_admission_visa_expiry_date",db.DBYYYYMMDDformat(simsobj.visa_expiry_date)),
                            
                            new SqlParameter("@sims_admission_visa_issuing_place", simsobj.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", simsobj.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date",db.DBYYYYMMDDformat(simsobj.national_id_issue_date)),
                            new SqlParameter("@sims_admission_national_id_expiry_date",db.DBYYYYMMDDformat(simsobj.national_id_expiry_date)),
                            
                            new SqlParameter("@sims_admission_sibling_status", simsobj.sibling_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_parent_id", simsobj.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", simsobj.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", simsobj.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",simsobj.sibling_dob),
                            
                            new SqlParameter("@sims_admission_sibling_school_code", simsobj.sibling_school_code),
                            new SqlParameter("@sims_admission_employee_type", simsobj.employee_type==true?"1":"0"),
                            new SqlParameter("@sims_admission_employee_code", simsobj.employee_code),
                            new SqlParameter("@sims_admission_employee_school_code", simsobj.employee_school_code),
                            new SqlParameter("@sims_admission_main_language_m", simsobj.motherTounge_language_code),
                            new SqlParameter("@sims_admission_main_language_code", simsobj.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", simsobj.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", simsobj.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", simsobj.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", simsobj.primary_contact_code==true?"1":"0"),
                            new SqlParameter("@sims_admission_primary_contact_pref", simsobj.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", simsobj.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", simsobj.transport_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_transport_desc", simsobj.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", simsobj.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", simsobj.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", simsobj.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", simsobj.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", simsobj.father_family_name),
                            new SqlParameter("@sims_admission_father_name_ot", simsobj.father_family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", simsobj.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", simsobj.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", simsobj.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", simsobj.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", simsobj.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", simsobj.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", simsobj.father_city),
                            new SqlParameter("@sims_admission_father_state_name", simsobj.father_state),
                            new SqlParameter("@sims_admission_father_country_code", simsobj.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", simsobj.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", simsobj.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", simsobj.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", simsobj.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", simsobj.father_fax),
                            new SqlParameter("@sims_admission_father_email", simsobj.father_email),
                            new SqlParameter("@sims_admission_father_occupation", simsobj.father_occupation),
                            new SqlParameter("@sims_admission_father_company", simsobj.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", simsobj.father_passport_number),
                            new SqlParameter("@sims_admission_father_national_id", simsobj.sims_admission_father_national_id),
                            new SqlParameter("@sims_admission_mother_national_id", simsobj.sims_admission_mother_national_id),
                            new SqlParameter("@sims_admission_guardian_national_id", simsobj.sims_admission_guardian_national_id),
                          
                            new SqlParameter("@sims_admission_guardian_salutation_code", simsobj.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", simsobj.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", simsobj.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", simsobj.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", simsobj.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", simsobj.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", simsobj.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", simsobj.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", simsobj.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", simsobj.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", simsobj.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", simsobj.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", simsobj.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", simsobj.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", simsobj.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", simsobj.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", simsobj.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", simsobj.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", simsobj.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", simsobj.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", simsobj.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", simsobj.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", simsobj.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", simsobj.guardian_passport_number),
                            new SqlParameter("@sims_admission_guardian_relationship_code",simsobj.guardian_relationship_code),
                            new SqlParameter("@sims_admission_mother_salutation_code", simsobj.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", simsobj.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", simsobj.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", simsobj.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", simsobj.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", simsobj.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", simsobj.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", simsobj.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", simsobj.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", simsobj.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", simsobj.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", simsobj.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", simsobj.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", simsobj.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", simsobj.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", simsobj.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", simsobj.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", simsobj.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", simsobj.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", simsobj.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", simsobj.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", simsobj.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", simsobj.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", simsobj.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", simsobj.current_school_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_current_school_name", simsobj.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", simsobj.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", simsobj.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", simsobj.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date",db.DBYYYYMMDDformat(simsobj.current_school_from_date)),
                            new SqlParameter("@sims_admission_current_school_to_date",db.DBYYYYMMDDformat(simsobj.current_school_to_date)),
                            
                            new SqlParameter("@sims_admission_current_school_language", simsobj.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", simsobj.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", simsobj.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", simsobj.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", simsobj.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", simsobj.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", simsobj.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", simsobj.marketing_code==true?"1":"0"),
                            new SqlParameter("@sims_admission_marketing_description", simsobj.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", simsobj.parent_status_code==true?"1":"0"),
                            new SqlParameter("@sims_admission_legal_custody", simsobj.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", simsobj.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date",db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date",db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),

                            
                            new SqlParameter("@sims_admission_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", simsobj.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", simsobj.medication_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_medication_desc", simsobj.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", simsobj.disability_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_disability_desc", simsobj.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", simsobj.behaviour_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_behaviour_desc", simsobj.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", simsobj.health_restriction_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_health_restriction_desc", simsobj.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", simsobj.health_hearing_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_health_hearing_desc", simsobj.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", simsobj.health_vision_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_health_vision_desc", simsobj.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", simsobj.health_other_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_health_other_desc", simsobj.health_other_desc),

                            new SqlParameter("@sims_admission_gifted_status", simsobj.gifted_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_gifted_desc", simsobj.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", simsobj.music_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_music_desc", simsobj.music_desc),
                            new SqlParameter("@sims_admission_sports_status", simsobj.sports_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_sports_desc", simsobj.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", simsobj.language_support_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_language_support_desc", simsobj.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", simsobj.declaration_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_fees_paid_status", simsobj.fees_paid_status==true?"1":"0"),
                            new SqlParameter("@sims_admission_fee_category_code", simsobj.fee_category_code),
                            new SqlParameter("@sims_admission_ip", simsobj.ip),
                            new SqlParameter("@sims_admission_dns", simsobj.dns),
                            new SqlParameter("@sims_admission_user_code", simsobj.user_code),
                            new SqlParameter("@sims_admission_status", simsobj.status),

                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),

                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),

                            new SqlParameter("@sims_student_attribute1", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_student_attribute2", simsobj.sims_student_attribute2),
                            new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3),
                            new SqlParameter("@sims_student_attribute4", simsobj.sims_student_attribute4),

                            new SqlParameter("@sims_student_health_respiratory_status", simsobj.sims_student_health_respiratory_status),
                            new SqlParameter("@sims_student_health_respiratory_desc", simsobj.sims_student_health_respiratory_desc),
                            new SqlParameter("@sims_student_health_hay_fever_status", simsobj.sims_student_health_hay_fever_status),
                            new SqlParameter("@sims_student_health_hay_fever_desc", simsobj.sims_student_health_hay_fever_desc),
                            new SqlParameter("@sims_student_health_epilepsy_status", simsobj.sims_student_health_epilepsy_status),
                            new SqlParameter("@sims_student_health_epilepsy_desc", simsobj.sims_student_health_epilepsy_desc),
                            new SqlParameter("@sims_student_health_skin_status", simsobj.sims_student_health_skin_status),
                            new SqlParameter("@sims_student_health_skin_desc", simsobj.sims_student_health_skin_desc),
                            new SqlParameter("@sims_student_health_diabetes_status", simsobj.sims_student_health_diabetes_status),
                            new SqlParameter("@sims_student_health_diabetes_desc", simsobj.sims_student_health_diabetes_desc),
                            new SqlParameter("@sims_student_health_surgery_status", simsobj.sims_student_health_surgery_status),
                            new SqlParameter("@sims_student_health_surgery_desc", simsobj.sims_student_health_surgery_desc),
                            new SqlParameter("@sims_admission_recommendation",simsobj.sims_admission_recommendation),
                            new SqlParameter("@sims_admission_parent_REG_id",simsobj.sims_admission_parent_REG_id),


                            

                            #endregion
                         });
                            if (dr.Read())
                            {
                                res.admission_number = dr["ADMISSION_NUMBER"].ToString();
                                if (!string.IsNullOrEmpty(res.admission_number))
                                {
                                    res.stud_full_name = dr["STUD_NAME"].ToString();
                                    res.family_name = dr["FATHER_NAME"].ToString();
                                }
                                else
                                    res.status = "Error";
                            }
                        }
                        // if (dr.RecordsAffected > 0)
                        //{
                        //if (simsobj.opr.Equals("U"))
                        //    message.strMessage = "Fee Type Information Updated Sucessfully!!";
                        //else if (simsobj.opr.Equals("I"))
                        //    message.strMessage = "Fee Type Information Added Sucessfully!!";
                        //else if (simsobj.opr.Equals("D"))
                        //    message.strMessage = "Fee Type Information Deleted Sucessfully!!";
                        //message.systemMessage = string.Empty;
                        //message.messageType = MessageType.Success;
                        // }
                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //if (simsobj.opr.Equals("I"))
                //    message.strMessage = "Error In Adding Admission Information";
                //message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("CUDUpdateAdmission")]
        public HttpResponseMessage CUDUpdateAdmission(List<Sims010_Edit> simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateAdmission"));
            Sims010_Edit res = new Sims010_Edit();

            // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit data in simsobj)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                                new List<SqlParameter>() 
                         { 
                           
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@sims_admission_number", data.admission_number),
                            new SqlParameter("@sims_admission_main_language_m", data.motherTounge_language_code),
                            new SqlParameter("@sims_admission_application_number", data.appl_num),
                            new SqlParameter("@sims_admission_pros_number", data.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", data.pros_appl_num),
                            new SqlParameter("@sims_admission_date",db.DBYYYYMMDDformat(data.admission_date)),
                            new SqlParameter("@sims_student_ea_number",data.sims_student_ea_number),
                         
                            #region
                            new SqlParameter("@sims_admission_school_code", data.school_code),
                            new SqlParameter("@sims_admission_cur_code", data.curr_code),
                            new SqlParameter("@sims_admission_academic_year", data.academic_year),
                            new SqlParameter("@sims_admission_grade_code", data.grade_code),
                            new SqlParameter("@sims_admission_section_code", data.section_code),
                            new SqlParameter("@sims_admission_term_code", data.term_code),
                            new SqlParameter("@sims_fee_month_code",data.sims_fee_month_code),
                              new SqlParameter("@sims_house_code",data.sims_house_code),
                            new SqlParameter("@sims_admission_tentative_joining_date",db.DBYYYYMMDDformat(data.tent_join_date)),
                
                            new SqlParameter("@sims_admission_passport_first_name_en", data.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", data.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", data.last_name),
                            new SqlParameter("@sims_admission_family_name_en", data.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", data.first_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", data.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", data.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", data.family_name_ot),
                            new SqlParameter("@sims_admission_nickname", data.nicke_name),
                            new SqlParameter("@sims_admission_dob",db.DBYYYYMMDDformat(data.birth_date)),
                            new SqlParameter("@sims_admission_commencement_date",db.DBYYYYMMDDformat(data.comm_date)),
               
                            new SqlParameter("@sims_admission_birth_country_code", data.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", data.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", data.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", data.gender_code),
                            new SqlParameter("@sims_admission_religion_code", data.religion_code),
                            new SqlParameter("@sims_admission_passport_number", data.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date",db.DBYYYYMMDDformat(data.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date",db.DBYYYYMMDDformat(data.passport_expiry)),
                
               
                            new SqlParameter("@sims_admission_passport_issuing_authority", data.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", data.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", data.visa_number),
                            new SqlParameter("@sims_admission_visa_type", data.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", data.visa_issuing_authority),
                            new SqlParameter("@sims_admission_visa_issue_date",db.DBYYYYMMDDformat(data.visa_issue_date)),
                            new SqlParameter("@sims_admission_visa_expiry_date",db.DBYYYYMMDDformat(data.visa_expiry_date)),
                
                
                            new SqlParameter("@sims_admission_visa_issuing_place", data.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", data.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date",db.DBYYYYMMDDformat(data.national_id_issue_date)),
                            new SqlParameter("@sims_admission_national_id_expiry_date",db.DBYYYYMMDDformat(data.national_id_expiry_date)),

                            new SqlParameter("@sims_admission_sibling_status", data.sibling_status),
                            new SqlParameter("@sims_admission_parent_id", data.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", data.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", data.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",data.sibling_dob),

                            new SqlParameter("@sims_admission_sibling_school_code", data.sibling_school_code),
                            new SqlParameter("@sims_admission_employee_type", data.employee_type),
                            new SqlParameter("@sims_admission_employee_code", data.employee_code),
                            new SqlParameter("@sims_admission_employee_school_code", data.employee_school_code),
                            new SqlParameter("@sims_admission_main_language_code", data.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", data.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", data.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", data.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", data.primary_contact_code),
                            new SqlParameter("@sims_admission_primary_contact_pref", data.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", data.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", data.transport_status),
                            new SqlParameter("@sims_admission_transport_desc", data.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", data.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", data.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", data.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", data.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", data.family_name),
                            new SqlParameter("@sims_admission_father_name_ot", data.family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", data.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", data.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", data.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", data.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", data.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", data.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", data.father_city),
                            new SqlParameter("@sims_admission_father_state_name", data.father_state),
                            new SqlParameter("@sims_admission_father_country_code", data.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", data.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", data.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", data.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", data.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", data.father_fax),
                            new SqlParameter("@sims_admission_father_email", data.father_email),
                            new SqlParameter("@sims_admission_father_occupation", data.father_occupation),
                            new SqlParameter("@sims_admission_father_company", data.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", data.father_passport_number),
                            new SqlParameter("@sims_admission_guardian_salutation_code", data.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", data.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", data.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", data.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", data.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", data.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", data.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", data.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", data.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", data.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", data.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", data.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", data.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", data.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", data.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", data.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", data.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", data.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", data.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", data.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", data.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", data.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", data.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", data.passport_num),
                            new SqlParameter("@sims_admission_mother_salutation_code", data.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", data.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", data.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", data.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", data.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", data.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", data.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", data.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", data.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", data.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", data.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", data.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", data.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", data.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", data.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", data.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", data.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", data.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", data.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", data.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", data.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", data.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", data.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", data.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", data.current_school_status),
                            new SqlParameter("@sims_admission_current_school_name", data.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", data.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", data.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", data.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date",db.DBYYYYMMDDformat(data.current_school_from_date)),
                            new SqlParameter("@sims_admission_current_school_to_date",db.DBYYYYMMDDformat(data.current_school_to_date)),

                            new SqlParameter("@sims_admission_current_school_language", data.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", data.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", data.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", data.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", data.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", data.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", data.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", data.marketing_code),
                            new SqlParameter("@sims_admission_marketing_description", data.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", data.parent_status_code),
                            new SqlParameter("@sims_admission_legal_custody", data.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", data.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date",db.DBYYYYMMDDformat(data.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date",db.DBYYYYMMDDformat(data.health_card_expiry_date)),

                            new SqlParameter("@sims_admission_health_card_issuing_authority", data.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", data.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", data.medication_status),
                            new SqlParameter("@sims_admission_medication_desc", data.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", data.disability_status),
                            new SqlParameter("@sims_admission_disability_desc", data.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", data.behaviour_status),
                            new SqlParameter("@sims_admission_behaviour_desc", data.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", data.health_restriction_status),
                            new SqlParameter("@sims_admission_health_restriction_desc", data.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", data.health_hearing_status),
                            new SqlParameter("@sims_admission_health_hearing_desc", data.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", data.health_vision_status),
                            new SqlParameter("@sims_admission_health_vision_desc", data.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", data.health_other_status),
                            new SqlParameter("@sims_admission_health_other_desc", data.health_other_desc),

                            new SqlParameter("@sims_admission_gifted_status", data.gifted_status),
                            new SqlParameter("@sims_admission_gifted_desc", data.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", data.music_status),
                            new SqlParameter("@sims_admission_music_desc", data.music_desc),
                            new SqlParameter("@sims_admission_sports_status", data.sports_status),
                            new SqlParameter("@sims_admission_sports_desc", data.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", data.language_support_status),
                            new SqlParameter("@sims_admission_language_support_desc", data.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", data.declaration_status),
                            new SqlParameter("@sims_admission_fees_paid_status", data.fees_paid_status),
                            new SqlParameter("@sims_admission_fee_category_code", data.fee_category_code),
                            new SqlParameter("@sims_admission_ip", data.ip),
                            new SqlParameter("@sims_admission_dns", data.dns),
                            new SqlParameter("@sims_admission_user_code", data.user_code),
                            new SqlParameter("@sims_admission_status", ""),

                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),

                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),
                            new SqlParameter("@sims_admission_father_national_id", data.sims_admission_father_national_id),
                            new SqlParameter("@sims_admission_mother_national_id", data.sims_admission_mother_national_id),
                            new SqlParameter("@sims_admission_guardian_national_id", data.sims_admission_guardian_national_id),
                            new SqlParameter("@sims_admission_recommendation",data.sims_admission_recommendation),
                            new SqlParameter("@sims_admission_parent_REG_id",data.sims_admission_parent_REG_id),
                            new SqlParameter("@sims_admission_father_salary", data.father_salary),
                            new SqlParameter("@sims_admission_mother_salary", data.mother_salary),

                             new SqlParameter("@sims_student_attribute1", data.sims_student_attribute1),
                            new SqlParameter("@sims_student_attribute2", data.sims_student_attribute2),
                            new SqlParameter("@sims_student_attribute3", data.sims_student_attribute3),
                            new SqlParameter("@sims_student_attribute4", data.sims_student_attribute4),
                             new SqlParameter("@sims_student_attribute5", data.sims_student_attribute5),
                            new SqlParameter("@sims_student_attribute6", data.sims_student_attribute6),
                            new SqlParameter("@sims_student_attribute7", data.sims_student_attribute7),
                            new SqlParameter("@sims_student_attribute8", data.sims_student_attribute8),
                             new SqlParameter("@sims_student_attribute9", data.sims_student_attribute9),
                              new SqlParameter("@sims_student_attribute10", data.sims_student_attribute10),
                               new SqlParameter("@sims_student_attribute11", data.sims_student_attribute11),
                                new SqlParameter("@sims_student_attribute12", data.sims_student_attribute12),

                                new SqlParameter("@sims_admission_learning_therapy_status", data.sims_admission_learning_therapy_status),
                                new SqlParameter("@sims_admission_learning_therapy_desc", data.sims_admission_learning_therapy_desc),
                                new SqlParameter("@sims_admission_special_education_status", data.sims_admission_special_education_status),
                                new SqlParameter("@sims_admission_special_education_desc", data.sims_admission_special_education_desc),
                                new SqlParameter("@sims_admission_falled_grade_status", data.sims_admission_falled_grade_status),
                                
                                  new SqlParameter("@sims_admission_falled_grade_desc", data.sims_admission_falled_grade_desc),
                                new SqlParameter("@sims_admission_communication_status", data.sims_admission_communication_status),
                                new SqlParameter("@sims_admission_communication_desc", data.sims_admission_communication_desc),
                                new SqlParameter("@sims_admission_specialAct_status", data.sims_admission_specialAct_status),
                                new SqlParameter("@sims_admission_specialAct_desc", data.sims_admission_specialAct_desc),

                              

                               // new SqlParameter("@sims_admission_second_lang_code",data.sims_admission_second_lang_code)
                            #endregion
                         });

                            if (dr.RecordsAffected > 0)
                            {
                                if (data.opr.Equals("U"))
                                    message.strMessage = "Admission Data Updated Sucessfully";
                                message.systemMessage = string.Empty;
                                message.status = true;
                                message.messageType = MessageType.Success;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //if (data.opr.Equals("U"))
                //    message.strMessage = "Error In Updating Admission Information";
                //message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("ApproveStudent")]
        public HttpResponseMessage ApproveStudent(List<Sims010_Edit> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ApproveStudent()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveStudent"));

            //Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            List<Sims010_Edit> lst = new List<Sims010_Edit>();
            // List<Comn_email> lst = new List<Comn_email>();
            Message message = new Message();
            //string enroll="", parent_id="", student_name="";
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                                new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'A'),
                            new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            #region
                            new SqlParameter("@sims_admission_application_number", simsobj.appl_num),
                            new SqlParameter("@sims_admission_pros_number", simsobj.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", simsobj.pros_appl_num),

                            new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                            new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),

                            new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                            new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                            new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                            new SqlParameter("@sims_admission_date", db.DBYYYYMMDDformat(simsobj.admission_date)),
                            new SqlParameter("@sims_student_ea_number",simsobj.sims_student_ea_number),
                            
                            new SqlParameter("@sims_admission_school_code", simsobj.school_code),
                            new SqlParameter("@sims_admission_cur_code", simsobj.curr_code),
                            new SqlParameter("@sims_admission_academic_year", simsobj.academic_year),
                            new SqlParameter("@sims_admission_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_admission_section_code", simsobj.section_code),
                            new SqlParameter("@sims_admission_term_code", simsobj.term_code),
                            new SqlParameter("@sims_fee_month_code",simsobj.sims_fee_month_code),
                            new SqlParameter("@sims_house_code",simsobj.sims_house_code),
                            new SqlParameter("@sims_admission_tentative_joining_date", db.DBYYYYMMDDformat(simsobj.tent_join_date)),
                           
                            new SqlParameter("@sims_admission_passport_first_name_en", simsobj.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", simsobj.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", simsobj.last_name),
                            new SqlParameter("@sims_admission_family_name_en", simsobj.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", simsobj.first_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", simsobj.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", simsobj.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_nickname", simsobj.nicke_name),
                            new SqlParameter("@sims_admission_dob", db.DBYYYYMMDDformat(simsobj.birth_date)),
                            new SqlParameter("@sims_admission_commencement_date", db.DBYYYYMMDDformat(simsobj.comm_date)),
                           
                            new SqlParameter("@sims_admission_birth_country_code", simsobj.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", simsobj.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", simsobj.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", simsobj.gender_code),
                            new SqlParameter("@sims_admission_religion_code", simsobj.religion_code),
                            new SqlParameter("@sims_admission_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date", db.DBYYYYMMDDformat(simsobj.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.passport_expiry)),
                           
                            new SqlParameter("@sims_admission_passport_issuing_authority", simsobj.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", simsobj.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", simsobj.visa_number),
                            new SqlParameter("@sims_admission_visa_type", simsobj.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", simsobj.visa_issuing_authority),
                            new SqlParameter("@sims_admission_visa_issue_date", db.DBYYYYMMDDformat(simsobj.visa_issue_date)),
                            new SqlParameter("@sims_admission_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.visa_expiry_date)),
                           
                            new SqlParameter("@sims_admission_visa_issuing_place", simsobj.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", simsobj.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.national_id_issue_date)),
                                new SqlParameter("@sims_admission_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.national_id_expiry_date)),
                
                        
                            new SqlParameter("@sims_admission_sibling_status", simsobj.sibling_status),
                            new SqlParameter("@sims_admission_parent_id", simsobj.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", simsobj.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", simsobj.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",simsobj.sibling_dob),
                        
                            new SqlParameter("@sims_admission_sibling_school_code", simsobj.sibling_school_code),
                            new SqlParameter("@sims_admission_employee_type", simsobj.employee_type),
                            new SqlParameter("@sims_admission_employee_code", simsobj.employee_code),
                            new SqlParameter("@sims_admission_employee_school_code", simsobj.employee_school_code),


                            new SqlParameter("@sims_admission_main_language_m", simsobj.motherTounge_language_code),


                            new SqlParameter("@sims_admission_main_language_code", simsobj.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", simsobj.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", simsobj.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", simsobj.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", simsobj.primary_contact_code),
                            new SqlParameter("@sims_admission_primary_contact_pref", simsobj.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", simsobj.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", simsobj.transport_status),
                            new SqlParameter("@sims_admission_transport_desc", simsobj.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", simsobj.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", simsobj.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", simsobj.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", simsobj.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", simsobj.family_name),
                            new SqlParameter("@sims_admission_father_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", simsobj.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", simsobj.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", simsobj.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", simsobj.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", simsobj.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", simsobj.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", simsobj.father_city),
                            new SqlParameter("@sims_admission_father_state_name", simsobj.father_state),
                            new SqlParameter("@sims_admission_father_country_code", simsobj.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", simsobj.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", simsobj.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", simsobj.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", simsobj.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", simsobj.father_fax),
                            new SqlParameter("@sims_admission_father_email", simsobj.father_email),
                            new SqlParameter("@sims_admission_father_occupation", simsobj.father_occupation),
                            new SqlParameter("@sims_admission_father_company", simsobj.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", simsobj.father_passport_number),
                            new SqlParameter("@sims_admission_guardian_salutation_code", simsobj.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", simsobj.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", simsobj.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", simsobj.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", simsobj.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", simsobj.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", simsobj.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", simsobj.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", simsobj.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", simsobj.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", simsobj.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", simsobj.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", simsobj.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", simsobj.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", simsobj.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", simsobj.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", simsobj.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", simsobj.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", simsobj.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", simsobj.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", simsobj.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", simsobj.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", simsobj.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_mother_salutation_code", simsobj.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", simsobj.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", simsobj.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", simsobj.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", simsobj.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", simsobj.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", simsobj.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", simsobj.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", simsobj.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", simsobj.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", simsobj.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", simsobj.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", simsobj.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", simsobj.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", simsobj.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", simsobj.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", simsobj.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", simsobj.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", simsobj.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", simsobj.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", simsobj.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", simsobj.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", simsobj.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", simsobj.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", simsobj.current_school_status),
                            new SqlParameter("@sims_admission_current_school_name", simsobj.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", simsobj.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", simsobj.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", simsobj.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date", db.DBYYYYMMDDformat(simsobj.current_school_from_date)),
                            new SqlParameter("@sims_admission_current_school_to_date", db.DBYYYYMMDDformat(simsobj.current_school_to_date)),
                            
                            new SqlParameter("@sims_admission_current_school_language", simsobj.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", simsobj.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", simsobj.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", simsobj.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", simsobj.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", simsobj.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", simsobj.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", simsobj.marketing_code),
                            new SqlParameter("@sims_admission_marketing_description", simsobj.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", simsobj.parent_status_code),
                            new SqlParameter("@sims_admission_legal_custody", simsobj.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", simsobj.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date", db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date", db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),
                           
                            new SqlParameter("@sims_admission_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", simsobj.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", simsobj.medication_status),
                            new SqlParameter("@sims_admission_medication_desc", simsobj.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", simsobj.disability_status),
                            new SqlParameter("@sims_admission_disability_desc", simsobj.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", simsobj.behaviour_status),
                            new SqlParameter("@sims_admission_behaviour_desc", simsobj.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", simsobj.health_restriction_status),
                            new SqlParameter("@sims_admission_health_restriction_desc", simsobj.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", simsobj.health_hearing_status),
                            new SqlParameter("@sims_admission_health_hearing_desc", simsobj.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", simsobj.health_vision_status),
                            new SqlParameter("@sims_admission_health_vision_desc", simsobj.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", simsobj.health_other_status),
                            new SqlParameter("@sims_admission_health_other_desc", simsobj.health_other_desc),

                            new SqlParameter("@sims_admission_gifted_status", simsobj.gifted_status),
                            new SqlParameter("@sims_admission_gifted_desc", simsobj.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", simsobj.music_status),
                            new SqlParameter("@sims_admission_music_desc", simsobj.music_desc),
                            new SqlParameter("@sims_admission_sports_status", simsobj.sports_status),
                            new SqlParameter("@sims_admission_sports_desc", simsobj.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", simsobj.language_support_status),
                            new SqlParameter("@sims_admission_language_support_desc", simsobj.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", simsobj.declaration_status),
                            new SqlParameter("@sims_admission_fees_paid_status", simsobj.fees_paid_status),
                            new SqlParameter("@sims_admission_fee_category_code", simsobj.fee_category_code),
                            new SqlParameter("@sims_admission_ip", simsobj.ip),
                            new SqlParameter("@sims_admission_dns", simsobj.dns),
                            new SqlParameter("@sims_admission_user_code", simsobj.user_code),
                            new SqlParameter("@sims_admission_status", ""),

                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),

                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),


                            new SqlParameter("@sims_student_attribute1", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_student_attribute2", simsobj.sims_student_attribute2),
                            new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3),
                            new SqlParameter("@sims_student_attribute4", simsobj.sims_student_attribute4),

                            new SqlParameter("@sims_student_health_respiratory_status", simsobj.sims_student_health_respiratory_status),
                            new SqlParameter("@sims_student_health_respiratory_desc", simsobj.sims_student_health_respiratory_desc),
                            new SqlParameter("@sims_student_health_hay_fever_status", simsobj.sims_student_health_hay_fever_status),
                            new SqlParameter("@sims_student_health_hay_fever_desc", simsobj.sims_student_health_hay_fever_desc),
                            new SqlParameter("@sims_student_health_epilepsy_status", simsobj.sims_student_health_epilepsy_status),
                            new SqlParameter("@sims_student_health_epilepsy_desc", simsobj.sims_student_health_epilepsy_desc),
                            new SqlParameter("@sims_student_health_skin_status", simsobj.sims_student_health_skin_status),
                            new SqlParameter("@sims_student_health_skin_desc", simsobj.sims_student_health_skin_desc),
                            new SqlParameter("@sims_student_health_diabetes_status", simsobj.sims_student_health_diabetes_status),
                            new SqlParameter("@sims_student_health_diabetes_desc", simsobj.sims_student_health_diabetes_desc),
                            new SqlParameter("@sims_student_health_surgery_status", simsobj.sims_student_health_surgery_status),
                            new SqlParameter("@sims_student_health_surgery_desc", simsobj.sims_student_health_surgery_desc),
                            new SqlParameter("@sims_mod_code","003"),
                            new SqlParameter("@sims_appl_code","SDash"),
                            new SqlParameter("@sims_admission_recommendation",simsobj.sims_admission_recommendation),
                            new SqlParameter("@sims_admission_parent_REG_id",simsobj.sims_admission_parent_REG_id),
                             new SqlParameter("@sims_subject_code",simsobj.sims_subject_code),
                               new SqlParameter("@sims_admission_father_salary", simsobj.father_salary),
                               new SqlParameter("@sims_admission_mother_salary", simsobj.mother_salary),

                                new SqlParameter("@sims_student_attribute5", simsobj.sims_student_attribute5),
                            new SqlParameter("@sims_student_attribute6", simsobj.sims_student_attribute6),
                            new SqlParameter("@sims_student_attribute7", simsobj.sims_student_attribute7),
                            new SqlParameter("@sims_student_attribute8", simsobj.sims_student_attribute8),
                             new SqlParameter("@sims_student_attribute9", simsobj.sims_student_attribute9),
                              new SqlParameter("@sims_student_attribute10", simsobj.sims_student_attribute10),
                               new SqlParameter("@sims_student_attribute11", simsobj.sims_student_attribute11),
                                new SqlParameter("@sims_student_attribute12", simsobj.sims_student_attribute12),

                                new SqlParameter("@sims_admission_learning_therapy_status", simsobj.sims_admission_learning_therapy_status),
                                new SqlParameter("@sims_admission_learning_therapy_desc", simsobj.sims_admission_learning_therapy_desc),
                                new SqlParameter("@sims_admission_special_education_status", simsobj.sims_admission_special_education_status),
                                new SqlParameter("@sims_admission_special_education_desc", simsobj.sims_admission_special_education_desc),
                                new SqlParameter("@sims_admission_falled_grade_status", simsobj.sims_admission_falled_grade_status),
                               new SqlParameter("@sims_admission_falled_grade_desc", simsobj.sims_admission_falled_grade_desc),
                                new SqlParameter("@sims_admission_communication_status", simsobj.sims_admission_communication_status),
                                new SqlParameter("@sims_admission_communication_desc", simsobj.sims_admission_communication_desc),
                                new SqlParameter("@sims_admission_specialAct_status", simsobj.sims_admission_specialAct_status),
                                new SqlParameter("@sims_admission_specialAct_desc", simsobj.sims_admission_specialAct_desc),
                               //  new SqlParameter("@sims_admission_second_lang_code",simsobj.sims_admission_second_lang_code)
                                 #endregion
                         });
                            while (dr.Read())
                            {
                                Sims010_Edit e = new Sims010_Edit();
                                // Comn_email e = new Comn_email();
                                //e.admission_date = DateTime.Parse(dr["Admis_date"].ToString()).ToShortDateString();
                                e.admission_date = db.UIDDMMYYYYformat(dr["Admis_date"].ToString());
                                e.enroll = dr["ENROLL"].ToString();
                                //enroll = e.enroll;
                                e.parent_id = dr["PARENT_ID"].ToString();
                                // parent_id = e.parent_id;
                                e.stud_full_name = dr["STUD_NAME"].ToString();
                                // student_name = e.stud_full_name;
                                //e.tent_join_date = DateTime.Parse(dr["Tent_Join_Date"].ToString()).ToShortDateString();
                                e.tent_join_date = db.UIDDMMYYYYformat(dr["Tent_Join_Date"].ToString());
                                e.status = dr["STATUS"].ToString();
                                e.getstatus = dr["STATUS"].ToString().Split('\\').ToList();
                                e.family_name = dr["ParentName"].ToString();//////////for parent full name
                                e.student_pass = dr["AdmissionStatus"].ToString();/////////for admission status Description
                                e.sims_msg_body = dr["Body"].ToString();
                                e.sims_msg_signature = dr["SIGN"].ToString();
                                e.sims_msg_subject = dr["Subject"].ToString();

                                lst.Add(e);

                                // GetcheckEmailProfileForAdmission();
                                // ScheduleMails(null,lst);
                                #region
                                //try
                                //{
                                //    List<Comn_email> lstmail = new List<Comn_email>();
                                //    //e.sims_msg_body += "Student Name: " + e.stud_full_name + "<p></p>";
                                //    //e.sims_msg_body += "Father Name: " + e.family_name + "<p></p>";
                                //    //e.sims_msg_body += "Enroll No.: " + e.enroll + "<p></p>";
                                //    //e.sims_msg_body += "Parent Id: " + e.parent_id + "<p></p>";
                                //    //e.sims_msg_body += "Class: " + data.grade_name +"-"+ data.section_name+"<p></p>";
                                //    //e.sims_msg_body += "Admission Status: " + e.student_pass + "<p></p>";
                                //    //e.sims_msg_body += e.sims_msg_signature ;
                                //    if (string.IsNullOrEmpty(data.father_email) == false)
                                //    {
                                //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                                //        m.body = e.sims_msg_body;
                                //        m.subject = e.sims_msg_subject;
                                //        m.emailsendto = data.father_email;
                                //        lstmail.Add(m);
                                //    }
                                //    if (string.IsNullOrEmpty(data.mother_email) == false)
                                //    {
                                //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                                //        m.body = e.sims_msg_body;
                                //        m.subject = e.sims_msg_subject;
                                //        m.emailsendto = data.mother_email;
                                //        lstmail.Add(m);
                                //    }
                                //    if (string.IsNullOrEmpty(data.guardian_email) == false)
                                //    {
                                //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                                //        m.body = e.sims_msg_body;
                                //        m.subject = e.sims_msg_subject;
                                //        m.emailsendto = data.guardian_email;
                                //        lstmail.Add(m);
                                //    }
                                //    if (lstmail.Count > 0)
                                //    {
                                //        sisService.commonService.Service_Email_Master em = new sisService.commonService.Service_Email_Master();
                                //        em.SendEmail(lstmail);
                                //    }
                                //}
                                //catch (Exception ex)
                                //{
                                //}
                                #endregion
                            }


                            if (dr.RecordsAffected > 0)
                            {

                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
        }

        private List<Sims010_Edit> ApproveStudent1(Sims010_Edit simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ApproveStudent()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveStudent"));

            // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            List<Sims010_Edit> lst = new List<Sims010_Edit>();
            Message message = new Message();
            //string enroll="", parent_id="", student_name="";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                            new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'A'),
                            new SqlParameter("@sims_admission_number", simsobj.admission_number),
                            #region
                            new SqlParameter("@sims_admission_application_number", simsobj.appl_num),
                            new SqlParameter("@sims_admission_pros_number", simsobj.pros_num),
                            new SqlParameter("@sims_admission_pros_application_number", simsobj.pros_appl_num),
                            new SqlParameter("@sims_student_ea_number",simsobj.sims_student_ea_number),

                            new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                            new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),

                            new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                            new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                            new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                            new SqlParameter("@sims_admission_date", db.DBYYYYMMDDformat(simsobj.admission_date)),

                            
                            new SqlParameter("@sims_admission_school_code", simsobj.school_code),
                            new SqlParameter("@sims_admission_cur_code", simsobj.curr_code),
                            new SqlParameter("@sims_admission_academic_year", simsobj.academic_year),
                            new SqlParameter("@sims_admission_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_admission_section_code", simsobj.section_code),
                            new SqlParameter("@sims_admission_term_code", simsobj.term_code),
                            new SqlParameter("@sims_fee_month_code",simsobj.sims_fee_month_code),
                            new SqlParameter("@sims_house_code",simsobj.sims_house_code),
                            new SqlParameter("@sims_admission_tentative_joining_date",db.DBYYYYMMDDformat(simsobj.tent_join_date)),
                           
                            new SqlParameter("@sims_admission_passport_first_name_en", simsobj.first_name),
                            new SqlParameter("@sims_admission_passport_middle_name_en", simsobj.middle_name),
                            new SqlParameter("@sims_admission_passport_last_name_en", simsobj.last_name),
                            new SqlParameter("@sims_admission_family_name_en", simsobj.family_name),
                            new SqlParameter("@sims_admission_passport_first_name_ot", simsobj.first_name_ot),
                            new SqlParameter("@sims_admission_passport_middle_name_ot", simsobj.midd_name_ot),
                            new SqlParameter("@sims_admission_passport_last_name_ot", simsobj.last_name_ot),
                            new SqlParameter("@sims_admission_family_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_nickname", simsobj.nicke_name),
                            new SqlParameter("@sims_admission_dob",db.DBYYYYMMDDformat(simsobj.birth_date)),
                            new SqlParameter("@sims_admission_commencement_date",db.DBYYYYMMDDformat(simsobj.comm_date)),
                           
                            new SqlParameter("@sims_admission_birth_country_code", simsobj.birth_country_code),
                            new SqlParameter("@sims_admission_nationality_code", simsobj.nationality_code),
                            new SqlParameter("@sims_admission_ethnicity_code", simsobj.ethinicity_code),
                            new SqlParameter("@sims_admission_gender_code", simsobj.gender_code),
                            new SqlParameter("@sims_admission_religion_code", simsobj.religion_code),
                            new SqlParameter("@sims_admission_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_passport_issue_date",db.DBYYYYMMDDformat(simsobj.passport_issue_date)),
                            new SqlParameter("@sims_admission_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.passport_expiry)),
                           
                            new SqlParameter("@sims_admission_passport_issuing_authority", simsobj.passport_issue_auth),
                            new SqlParameter("@sims_admission_passport_issue_place", simsobj.passport_issue_place),
                            new SqlParameter("@sims_admission_visa_number", simsobj.visa_number),
                            new SqlParameter("@sims_admission_visa_type", simsobj.visa_type),
                            new SqlParameter("@sims_admission_visa_issuing_authority", simsobj.visa_issuing_authority),
                            new SqlParameter("@sims_admission_visa_issue_date",db.DBYYYYMMDDformat(simsobj.visa_issue_date)),
                            new SqlParameter("@sims_admission_visa_expiry_date",db.DBYYYYMMDDformat(simsobj.visa_expiry_date)),
                           
                            new SqlParameter("@sims_admission_visa_issuing_place", simsobj.visa_issuing_place),
                            new SqlParameter("@sims_admission_national_id", simsobj.national_id),
                            new SqlParameter("@sims_admission_national_id_issue_date",db.DBYYYYMMDDformat(simsobj.national_id_issue_date)),
                                new SqlParameter("@sims_admission_national_id_expiry_date",db.DBYYYYMMDDformat(simsobj.national_id_expiry_date)),
                
                        
                            new SqlParameter("@sims_admission_sibling_status", simsobj.sibling_status),
                            new SqlParameter("@sims_admission_parent_id", simsobj.parent_id),
                            new SqlParameter("@sims_admission_sibling_enroll_number", simsobj.sibling_enroll),
                            new SqlParameter("@sims_admission_sibling_name", simsobj.sibling_name),
                            new SqlParameter("@sims_admission_sibling_dob",db.DBYYYYMMDDformat(simsobj.sibling_dob)),
                        
                            new SqlParameter("@sims_admission_sibling_school_code", simsobj.sibling_school_code),
                            new SqlParameter("@sims_admission_employee_type", simsobj.employee_type),
                            new SqlParameter("@sims_admission_employee_code", simsobj.employee_code),
                            new SqlParameter("@sims_admission_employee_school_code", simsobj.employee_school_code),


                            new SqlParameter("@sims_admission_main_language_m", simsobj.motherTounge_language_code),


                            new SqlParameter("@sims_admission_main_language_code", simsobj.main_language_code),
                            new SqlParameter("@sims_admission_main_language_r", simsobj.main_language_r_code),
                            new SqlParameter("@sims_admission_main_language_w", simsobj.main_language_w_code),
                            new SqlParameter("@sims_admission_main_language_s", simsobj.main_language_s_code),
                            new SqlParameter("@sims_admission_primary_contact_code", simsobj.primary_contact_code),
                            new SqlParameter("@sims_admission_primary_contact_pref", simsobj.primary_contact_pref_code),
                            new SqlParameter("@sims_admission_fee_payment_contact_pref", simsobj.fee_payment_contact_pref_code),
                            new SqlParameter("@sims_admission_transport_status", simsobj.transport_status),
                            new SqlParameter("@sims_admission_transport_desc", simsobj.transport_desc),
                            new SqlParameter("@sims_admission_father_salutation_code", simsobj.father_salutation_code),
                            new SqlParameter("@sims_admission_father_first_name", simsobj.father_first_name),
                            new SqlParameter("@sims_admission_father_middle_name", simsobj.father_middle_name),
                            new SqlParameter("@sims_admission_father_last_name", simsobj.father_last_name),
                            new SqlParameter("@sims_admission_father_family_name", simsobj.family_name),
                            new SqlParameter("@sims_admission_father_name_ot", simsobj.family_name_ot),
                            new SqlParameter("@sims_admission_father_nationality1_code", simsobj.father_nationality1_code),
                            new SqlParameter("@sims_admission_father_nationality2_code", simsobj.father_nationality2_code),
                            new SqlParameter("@sims_admission_father_appartment_number", simsobj.father_appartment_number),
                            new SqlParameter("@sims_admission_father_building_number", simsobj.father_building_number),
                            new SqlParameter("@sims_admission_father_street_number", simsobj.father_street_number),
                            new SqlParameter("@sims_admission_father_area_number", simsobj.father_area_number),
                            new SqlParameter("@sims_admission_father_city_name", simsobj.father_city),
                            new SqlParameter("@sims_admission_father_state_name", simsobj.father_state),
                            new SqlParameter("@sims_admission_father_country_code", simsobj.father_country_code),
                            new SqlParameter("@sims_admission_father_summary_address", simsobj.father_summary_address),
                            new SqlParameter("@sims_admission_father_po_box", simsobj.father_po_box),
                            new SqlParameter("@sims_admission_father_phone", simsobj.father_phone),
                            new SqlParameter("@sims_admission_father_mobile", simsobj.father_mobile),
                            new SqlParameter("@sims_admission_father_fax", simsobj.father_fax),
                            new SqlParameter("@sims_admission_father_email", simsobj.father_email),
                            new SqlParameter("@sims_admission_father_occupation", simsobj.father_occupation),
                            new SqlParameter("@sims_admission_father_company", simsobj.father_company),
                            new SqlParameter("@sims_admission_father_passport_number", simsobj.father_passport_number),
                            new SqlParameter("@sims_admission_guardian_salutation_code", simsobj.guardian_salutation_code),
                            new SqlParameter("@sims_admission_guardian_first_name", simsobj.guardian_first_name),
                            new SqlParameter("@sims_admission_guardian_middle_name", simsobj.guardian_middle_name),
                            new SqlParameter("@sims_admission_guardian_last_name", simsobj.guardian_last_name),
                            new SqlParameter("@sims_admission_guardian_family_name", simsobj.guardian_family_name),
                            new SqlParameter("@sims_admission_guardian_name_ot", simsobj.guardian_name_ot),
                            new SqlParameter("@sims_admission_guardian_nationality1_code", simsobj.guardian_nationality1_code),
                            new SqlParameter("@sims_admission_guardian_nationality2_code", simsobj.guardian_nationality2_code),
                            new SqlParameter("@sims_admission_guardian_appartment_number", simsobj.guardian_appartment_number),
                            new SqlParameter("@sims_admission_guardian_building_number", simsobj.guardian_building_number),
                            new SqlParameter("@sims_admission_guardian_street_number", simsobj.guardian_street_number),
                            new SqlParameter("@sims_admission_guardian_area_number", simsobj.guardian_area_number),
                            new SqlParameter("@sims_admission_guardian_city_name", simsobj.guardian_city),
                            new SqlParameter("@sims_admission_guardian_state_name", simsobj.guardian_state),
                            new SqlParameter("@sims_admission_guardian_country_code", simsobj.guardian_country_code),
                            new SqlParameter("@sims_admission_guardian_summary_address", simsobj.guardian_summary_address),
                            new SqlParameter("@sims_admission_guardian_po_box", simsobj.guardian_po_box),
                            new SqlParameter("@sims_admission_guardian_phone", simsobj.guardian_phone),
                            new SqlParameter("@sims_admission_guardian_mobile", simsobj.guardian_mobile),
                            new SqlParameter("@sims_admission_guardian_fax", simsobj.guardian_fax),
                            new SqlParameter("@sims_admission_guardian_email", simsobj.guardian_email),
                            new SqlParameter("@sims_admission_guardian_occupation", simsobj.guardian_occupation),
                            new SqlParameter("@sims_admission_guardian_company", simsobj.guardian_company),
                            new SqlParameter("@sims_admission_guardian_passport_number", simsobj.passport_num),
                            new SqlParameter("@sims_admission_mother_salutation_code", simsobj.mother_salutation_code),
                            new SqlParameter("@sims_admission_mother_first_name", simsobj.mother_first_name),
                            new SqlParameter("@sims_admission_mother_middle_name", simsobj.mother_middle_name),
                            new SqlParameter("@sims_admission_mother_last_name", simsobj.mother_last_name),
                            new SqlParameter("@sims_admission_mother_family_name", simsobj.mother_family_name),
                            new SqlParameter("@sims_admission_mother_name_ot", simsobj.mother_name_ot),
                            new SqlParameter("@sims_admission_mother_nationality1_code", simsobj.mother_nationality1_code),
                            new SqlParameter("@sims_admission_mother_nationality2_code", simsobj.mother_nationality2_code),
                            new SqlParameter("@sims_admission_mother_appartment_number", simsobj.mother_appartment_number),
                            new SqlParameter("@sims_admission_mother_building_number", simsobj.mother_building_number),
                            new SqlParameter("@sims_admission_mother_street_number", simsobj.mother_street_number),
                            new SqlParameter("@sims_admission_mother_area_number", simsobj.mother_area_number),
                            new SqlParameter("@sims_admission_mother_city_name", simsobj.mother_city),
                            new SqlParameter("@sims_admission_mother_state_name", simsobj.mother_state),
                            new SqlParameter("@sims_admission_mother_country_code", simsobj.mother_country_code),
                            new SqlParameter("@sims_admission_mother_summary_address", simsobj.mother_summary_address),
                            new SqlParameter("@sims_admission_mother_po_box", simsobj.mother_po_box),
                            new SqlParameter("@sims_admission_mother_phone", simsobj.mother_phone),
                            new SqlParameter("@sims_admission_mother_mobile", simsobj.mother_mobile),
                            new SqlParameter("@sims_admission_mother_fax", simsobj.mother_fax),
                            new SqlParameter("@sims_admission_mother_email", simsobj.mother_email),
                            new SqlParameter("@sims_admission_mother_occupation", simsobj.mother_occupation),
                            new SqlParameter("@sims_admission_mother_company", simsobj.mother_company),
                            new SqlParameter("@sims_admission_mother_passport_number", simsobj.mother_passport_number),
                            new SqlParameter("@sims_admission_current_school_status", simsobj.current_school_status),
                            new SqlParameter("@sims_admission_current_school_name", simsobj.current_school_name),
                            new SqlParameter("@sims_admission_current_school_enroll_number", simsobj.current_school_enroll_number),
                            new SqlParameter("@sims_admission_current_school_grade", simsobj.current_school_grade),
                            new SqlParameter("@sims_admission_current_school_cur", simsobj.current_school_cur),
                            new SqlParameter("@sims_admission_current_school_from_date",db.DBYYYYMMDDformat(simsobj.current_school_from_date)),
                            new SqlParameter("@sims_admission_current_school_to_date",db.DBYYYYMMDDformat(simsobj.current_school_to_date)),
                            
                            new SqlParameter("@sims_admission_current_school_language", simsobj.current_school_language),
                            new SqlParameter("@sims_admission_current_school_head_teacher", simsobj.current_school_head_teacher),
                            new SqlParameter("@sims_admission_current_school_phone", simsobj.current_school_phone),
                            new SqlParameter("@sims_admission_current_school_fax", simsobj.current_school_fax),
                            new SqlParameter("@sims_admission_current_school_city_name", simsobj.current_school_city),
                            new SqlParameter("@sims_admission_current_school_country_code", simsobj.current_school_country_code),
                            new SqlParameter("@sims_admission_current_school_address", simsobj.current_school_address),
                            new SqlParameter("@sims_admission_marketing_code", simsobj.marketing_code),
                            new SqlParameter("@sims_admission_marketing_description", simsobj.marketing_description),
                            new SqlParameter("@sims_admission_parent_status_code", simsobj.parent_status_code),
                            new SqlParameter("@sims_admission_legal_custody", simsobj.legal_custody_code),
                            new SqlParameter("@sims_admission_health_card_number", simsobj.health_card_number),
                            new SqlParameter("@sims_admission_health_card_issue_date",db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                            new SqlParameter("@sims_admission_health_card_expiry_date",db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),
                           
                            new SqlParameter("@sims_admission_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                            new SqlParameter("@sims_admission_blood_group_code", simsobj.blood_group_code),//blood group name
                            new SqlParameter("@sims_admission_medication_status", simsobj.medication_status),
                            new SqlParameter("@sims_admission_medication_desc", simsobj.medication_desc),
                            new SqlParameter("@sims_admission_disability_status", simsobj.disability_status),
                            new SqlParameter("@sims_admission_disability_desc", simsobj.disability_desc),
                            new SqlParameter("@sims_admission_behaviour_status", simsobj.behaviour_status),
                            new SqlParameter("@sims_admission_behaviour_desc", simsobj.behaviour_desc),
                            new SqlParameter("@sims_admission_health_restriction_status", simsobj.health_restriction_status),
                            new SqlParameter("@sims_admission_health_restriction_desc", simsobj.health_restriction_desc),
                            new SqlParameter("@sims_admission_health_hearing_status", simsobj.health_hearing_status),
                            new SqlParameter("@sims_admission_health_hearing_desc", simsobj.health_hearing_desc),
                            new SqlParameter("@sims_admission_health_vision_status", simsobj.health_vision_status),
                            new SqlParameter("@sims_admission_health_vision_desc", simsobj.health_vision_desc),
                            new SqlParameter("@sims_admission_health_other_status", simsobj.health_other_status),
                            new SqlParameter("@sims_admission_health_other_desc", simsobj.health_other_desc),

                            new SqlParameter("@sims_admission_gifted_status", simsobj.gifted_status),
                            new SqlParameter("@sims_admission_gifted_desc", simsobj.gifted_desc),
                            new SqlParameter("@sims_admission_music_status", simsobj.music_status),
                            new SqlParameter("@sims_admission_music_desc", simsobj.music_desc),
                            new SqlParameter("@sims_admission_sports_status", simsobj.sports_status),
                            new SqlParameter("@sims_admission_sports_desc", simsobj.sports_desc),
                            new SqlParameter("@sims_admission_language_support_status", simsobj.language_support_status),
                            new SqlParameter("@sims_admission_language_support_desc", simsobj.language_support_desc),
                            new SqlParameter("@sims_admission_declaration_status", simsobj.declaration_status),
                            new SqlParameter("@sims_admission_fees_paid_status", simsobj.fees_paid_status),
                            new SqlParameter("@sims_admission_fee_category_code", simsobj.fee_category_code),
                            new SqlParameter("@sims_admission_ip", simsobj.ip),
                            new SqlParameter("@sims_admission_dns", simsobj.dns),
                            new SqlParameter("@sims_admission_user_code", simsobj.user_code),
                            new SqlParameter("@sims_admission_status", ""),

                            new SqlParameter("@student_user_code", ""),
                            new SqlParameter("@sims_enroll_number", ""),
                            new SqlParameter("@student_user_name", ""),
                            new SqlParameter("@student_user_password", ""),
                            new SqlParameter("@student_user_group_code", ""),
                            new SqlParameter("@student_user_date_created", "01/01/2014"),
                            new SqlParameter("@sims_parent_number", ""),
                            new SqlParameter("@parent_user_code", ""),
                            new SqlParameter("@parent_user_password", ""),
                            new SqlParameter("@parent_user_group_code", ""),
                            new SqlParameter("@parent_user_date_created", "01/01/2014"),

                            new SqlParameter("@sims_student_img", ""),
                            new SqlParameter("@sims_parent_father_img", ""),
                            new SqlParameter("@sims_parent_mother_img", ""),
                            new SqlParameter("@sims_parent_guardian_img", ""),


                            new SqlParameter("@sims_student_attribute1", simsobj.sims_student_attribute1),
                            new SqlParameter("@sims_student_attribute2", simsobj.sims_student_attribute2),
                            new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3),
                            new SqlParameter("@sims_student_attribute4", simsobj.sims_student_attribute4),

                            new SqlParameter("@sims_student_health_respiratory_status", simsobj.sims_student_health_respiratory_status),
                            new SqlParameter("@sims_student_health_respiratory_desc", simsobj.sims_student_health_respiratory_desc),
                            new SqlParameter("@sims_student_health_hay_fever_status", simsobj.sims_student_health_hay_fever_status),
                            new SqlParameter("@sims_student_health_hay_fever_desc", simsobj.sims_student_health_hay_fever_desc),
                            new SqlParameter("@sims_student_health_epilepsy_status", simsobj.sims_student_health_epilepsy_status),
                            new SqlParameter("@sims_student_health_epilepsy_desc", simsobj.sims_student_health_epilepsy_desc),
                            new SqlParameter("@sims_student_health_skin_status", simsobj.sims_student_health_skin_status),
                            new SqlParameter("@sims_student_health_skin_desc", simsobj.sims_student_health_skin_desc),
                            new SqlParameter("@sims_student_health_diabetes_status", simsobj.sims_student_health_diabetes_status),
                            new SqlParameter("@sims_student_health_diabetes_desc", simsobj.sims_student_health_diabetes_desc),
                            new SqlParameter("@sims_student_health_surgery_status", simsobj.sims_student_health_surgery_status),
                            new SqlParameter("@sims_student_health_surgery_desc", simsobj.sims_student_health_surgery_desc),
                               
                              new SqlParameter("@sims_subject_code",simsobj.sims_subject_code),
                            new SqlParameter("@sims_admission_father_salary", simsobj.father_salary),
                               new SqlParameter("@sims_admission_mother_salary", simsobj.mother_salary),

                               //new
                                new SqlParameter("@sims_student_attribute5", simsobj.sims_student_attribute5),
                            new SqlParameter("@sims_student_attribute6", simsobj.sims_student_attribute6),
                            new SqlParameter("@sims_student_attribute7", simsobj.sims_student_attribute7),
                            new SqlParameter("@sims_student_attribute8", simsobj.sims_student_attribute8),
                             new SqlParameter("@sims_student_attribute9", simsobj.sims_student_attribute9),
                              new SqlParameter("@sims_student_attribute10", simsobj.sims_student_attribute10),
                               new SqlParameter("@sims_student_attribute11", simsobj.sims_student_attribute11),
                                new SqlParameter("@sims_student_attribute12", simsobj.sims_student_attribute12),


                                //for asd
                                new SqlParameter("@sims_admission_learning_therapy_status", simsobj.sims_admission_learning_therapy_status),
                                new SqlParameter("@sims_admission_learning_therapy_desc", simsobj.sims_admission_learning_therapy_desc),
                                new SqlParameter("@sims_admission_special_education_status", simsobj.sims_admission_special_education_status),
                                new SqlParameter("@sims_admission_special_education_desc", simsobj.sims_admission_special_education_desc),
                                new SqlParameter("@sims_admission_falled_grade_status", simsobj.sims_admission_falled_grade_status),
                               new SqlParameter("@sims_admission_falled_grade_desc", simsobj.sims_admission_falled_grade_desc),
                                new SqlParameter("@sims_admission_communication_status", simsobj.sims_admission_communication_status),
                                new SqlParameter("@sims_admission_communication_desc", simsobj.sims_admission_communication_desc),
                                new SqlParameter("@sims_admission_specialAct_status", simsobj.sims_admission_specialAct_status),
                                new SqlParameter("@sims_admission_specialAct_desc", simsobj.sims_admission_specialAct_desc),
                                // new SqlParameter("@sims_admission_second_lang_code",simsobj.sims_admission_second_lang_code)
                                 #endregion
                         });
                        while (dr.Read())
                        {
                            Sims010_Edit e = new Sims010_Edit();
                            //e.admission_date = DateTime.Parse(dr["Admis_date"].ToString()).ToShortDateString();
                            e.admission_date = db.UIDDMMYYYYformat(dr["Admis_date"].ToString());
                            e.enroll = dr["ENROLL"].ToString();
                            //enroll = e.enroll;
                            e.parent_id = dr["PARENT_ID"].ToString();
                            // parent_id = e.parent_id;
                            e.stud_full_name = dr["STUD_NAME"].ToString();
                            // student_name = e.stud_full_name;
                            //e.tent_join_date = DateTime.Parse(dr["Tent_Join_Date"].ToString()).ToShortDateString();
                            e.tent_join_date = db.UIDDMMYYYYformat(dr["Tent_Join_Date"].ToString());
                            e.status = dr["STATUS"].ToString();
                            e.getstatus = dr["STATUS"].ToString().Split('\\').ToList();
                            e.family_name = dr["ParentName"].ToString();//////////for parent full name
                            e.student_pass = dr["AdmissionStatus"].ToString();/////////for admission status Description
                            e.sims_msg_body = dr["Body"].ToString();
                            e.sims_msg_signature = dr["SIGN"].ToString();
                            e.sims_msg_subject = dr["Subject"].ToString();
                            lst.Add(e);

                            #region
                            //try
                            //{
                            //    List<Comn_email> lstmail = new List<Comn_email>();
                            //    //e.sims_msg_body += "Student Name: " + e.stud_full_name + "<p></p>";
                            //    //e.sims_msg_body += "Father Name: " + e.family_name + "<p></p>";
                            //    //e.sims_msg_body += "Enroll No.: " + e.enroll + "<p></p>";
                            //    //e.sims_msg_body += "Parent Id: " + e.parent_id + "<p></p>";
                            //    //e.sims_msg_body += "Class: " + data.grade_name +"-"+ data.section_name+"<p></p>";
                            //    //e.sims_msg_body += "Admission Status: " + e.student_pass + "<p></p>";
                            //    //e.sims_msg_body += e.sims_msg_signature ;
                            //    if (string.IsNullOrEmpty(data.father_email) == false)
                            //    {
                            //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                            //        m.body = e.sims_msg_body;
                            //        m.subject = e.sims_msg_subject;
                            //        m.emailsendto = data.father_email;
                            //        lstmail.Add(m);
                            //    }
                            //    if (string.IsNullOrEmpty(data.mother_email) == false)
                            //    {
                            //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                            //        m.body = e.sims_msg_body;
                            //        m.subject = e.sims_msg_subject;
                            //        m.emailsendto = data.mother_email;
                            //        lstmail.Add(m);
                            //    }
                            //    if (string.IsNullOrEmpty(data.guardian_email) == false)
                            //    {
                            //        sims.Entity.sisClass.commonClass.Comn_email m = new sims.Entity.sisClass.commonClass.Comn_email();
                            //        m.body = e.sims_msg_body;
                            //        m.subject = e.sims_msg_subject;
                            //        m.emailsendto = data.guardian_email;
                            //        lstmail.Add(m);
                            //    }
                            //    if (lstmail.Count > 0)
                            //    {
                            //        sisService.commonService.Service_Email_Master em = new sisService.commonService.Service_Email_Master();
                            //        em.SendEmail(lstmail);
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //}
                            #endregion
                        }

                        if (dr.RecordsAffected > 0)
                        {

                        }
                        return lst;
                    }
                }
                return lst;
            }
            catch (Exception x)
            {
                return lst;
            }
        }

        private Sims010_Edit GetTabStudentData(string admission_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTabStudentData()PARAMETERS ::admission_number{2}";
            Log.Debug(string.Format(debug, "ERP/Admission/", "GetTabStudentData", admission_number));

            Sims010_Edit simsobj = new Sims010_Edit();
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@CURR_CODE", ""),
                            new SqlParameter("@ACA_YEAR", ""),
                            new SqlParameter("@GRADE_CODE", ""),
                            new SqlParameter("@ADMISSION_NUM", admission_number)
                          
                         });
                        while (dr.Read())
                        {
                            #region
                            simsobj.sims_admission_father_national_id = dr["sims_admission_father_national_id"].ToString();
                            simsobj.sims_admission_mother_national_id = dr["sims_admission_mother_national_id"].ToString();
                            simsobj.sims_admission_guardian_national_id = dr["sims_admission_guardian_national_id"].ToString();
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.appl_num = dr["sims_admission_application_number"].ToString();
                            simsobj.behaviour_desc = dr["sims_admission_behaviour_desc"].ToString();
                            if (dr["sims_admission_behaviour_status"].ToString() == "1")
                            {
                                simsobj.behaviour_status = true;
                            }
                            else
                            {
                                simsobj.behaviour_status = false;
                            }
                            simsobj.birth_country_code = dr["sims_admission_birth_country_code"].ToString();
                            simsobj.blood_group_code = dr["sims_admission_blood_group_code"].ToString();
                            try
                            {
                                simsobj.comm_date = db.UIDDMMYYYYformat(dr["sims_admission_commencement_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.comm_date = "";
                            }
                            simsobj.current_school_address = dr["sims_admission_current_school_address"].ToString();
                            simsobj.current_school_city = dr["sims_admission_current_school_city"].ToString();
                            simsobj.current_school_cur = dr["sims_admission_current_school_cur"].ToString();
                            simsobj.current_school_enroll_number = dr["sims_admission_current_school_enroll_number"].ToString();
                            simsobj.current_school_fax = dr["sims_admission_current_school_fax"].ToString();
                            try
                            {
                                simsobj.current_school_from_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_from_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.current_school_from_date = "";
                            }
                            simsobj.current_school_grade = dr["sims_admission_current_school_grade"].ToString();
                            simsobj.current_school_head_teacher = dr["sims_admission_current_school_head_teacher"].ToString();
                            simsobj.current_school_language = dr["sims_admission_current_school_language"].ToString();

                            simsobj.current_school_name = dr["sims_admission_current_school_name"].ToString();
                            simsobj.current_school_phone = dr["sims_admission_current_school_phone"].ToString();
                            if (dr["sims_admission_current_school_status"].ToString() == "1")
                            {
                                simsobj.current_school_status = true;
                            }
                            else
                            {
                                simsobj.current_school_status = false;
                            }

                            simsobj.current_school_to_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_to_date"].ToString());
                            simsobj.current_school_country = dr["current_school_country"].ToString();
                            simsobj.current_school_country_code = dr["sims_admission_current_school_country_code"].ToString();
                            if (dr["sims_admission_declaration_status"].ToString() == "1")
                            {
                                simsobj.declaration_status = true;
                            }
                            else
                            {
                                simsobj.declaration_status = false;
                            }
                            simsobj.disability_desc = dr["sims_admission_disability_desc"].ToString();
                            if (dr["sims_admission_disability_status"].ToString() == "1")
                            {
                                simsobj.disability_status = true;
                            }
                            else
                            {
                                simsobj.disability_status = false;
                            }
                            simsobj.dns = dr["sims_admission_dns"].ToString();
                            simsobj.employee_comp_code = dr["sims_admission_employee_comp_code"].ToString();
                            simsobj.employee_school_code = dr["sims_admission_employee_school_code"].ToString();
                            if (dr["sims_admission_employee_type"].ToString() == "1")
                            {
                                simsobj.employee_type = true;
                            }
                            else
                            {
                                simsobj.employee_type = false;
                            }
                            simsobj.family_name_ot = dr["sims_admission_family_name_ot"].ToString();
                            simsobj.ethinicity_code = dr["sims_admission_ethnicity_code"].ToString();
                            simsobj.family_name = dr["sims_admission_family_name_en"].ToString();
                            simsobj.father_appartment_number = dr["sims_admission_father_appartment_number"].ToString();
                            simsobj.father_area_number = dr["sims_admission_father_area_number"].ToString();
                            simsobj.father_building_number = dr["sims_admission_father_building_number"].ToString();
                            simsobj.father_city = dr["sims_admission_father_city"].ToString();
                            simsobj.father_company = dr["sims_admission_father_company"].ToString();
                            simsobj.father_country_code = dr["sims_admission_father_country_code"].ToString();
                            simsobj.father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.father_family_name = dr["sims_admission_father_family_name"].ToString();
                            simsobj.father_fax = dr["sims_admission_father_fax"].ToString();
                            simsobj.father_first_name = dr["sims_admission_father_first_name"].ToString();
                            simsobj.father_last_name = dr["sims_admission_father_last_name"].ToString();
                            simsobj.father_middle_name = dr["sims_admission_father_middle_name"].ToString();
                            simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                            simsobj.father_name_ot = dr["sims_admission_father_name_ot"].ToString();
                            simsobj.father_nationality1_code = dr["sims_admission_father_nationality1_code"].ToString();
                            simsobj.father_nationality2_code = dr["sims_admission_father_nationality2_code"].ToString();
                            simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();
                            simsobj.father_passport_number = dr["sims_admission_father_passport_number"].ToString();
                            simsobj.father_phone = dr["sims_admission_father_phone"].ToString();
                            simsobj.father_po_box = dr["sims_admission_father_po_box"].ToString();
                            simsobj.father_salutation_code = dr["sims_admission_father_salutation_code"].ToString();
                            simsobj.father_state = dr["sims_admission_father_state"].ToString();
                            simsobj.father_street_number = dr["sims_admission_father_street_number"].ToString();
                            simsobj.father_summary_address = dr["sims_admission_father_summary_address"].ToString();
                            simsobj.fee_payment_contact_pref_code = dr["sims_admission_fee_payment_contact_pref"].ToString();
                            if (dr["sims_admission_fees_paid_status"].ToString() == "1")
                            {
                                simsobj.fees_paid_status = true;
                            }
                            else
                            {
                                simsobj.fees_paid_status = false;
                            }
                            simsobj.first_name = dr["sims_admission_passport_first_name_en"].ToString();
                            simsobj.first_name_ot = dr["sims_admission_passport_first_name_ot"].ToString();
                            simsobj.gifted_desc = dr["sims_admission_gifted_desc"].ToString();
                            if (dr["sims_admission_gifted_status"].ToString() == "1")
                            {
                                simsobj.gifted_status = true;
                            }
                            else
                            {
                                simsobj.gifted_status = false;
                            }
                            simsobj.guardian_appartment_number = dr["sims_admission_guardian_appartment_number"].ToString();
                            simsobj.guardian_area_number = dr["sims_admission_guardian_area_number"].ToString();
                            simsobj.guardian_building_number = dr["sims_admission_guardian_building_number"].ToString();
                            simsobj.guardian_city = dr["sims_admission_guardian_city"].ToString();
                            simsobj.guardian_company = dr["sims_admission_guardian_company"].ToString();
                            simsobj.guardian_country_code = dr["sims_admission_guardian_country_code"].ToString();
                            simsobj.guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.guardian_family_name = dr["sims_admission_guardian_family_name"].ToString();
                            simsobj.guardian_fax = dr["sims_admission_guardian_fax"].ToString();
                            simsobj.guardian_first_name = dr["sims_admission_guardian_first_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_admission_guardian_last_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_admission_guardian_middle_name"].ToString();
                            simsobj.guardian_mobile = dr["sims_admission_guardian_mobile"].ToString();
                            simsobj.guardian_name_ot = dr["sims_admission_guardian_name_ot"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_admission_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality2_code = dr["sims_admission_guardian_nationality2_code"].ToString();
                            simsobj.guardian_occupation = dr["sims_admission_guardian_occupation"].ToString();
                            simsobj.guardian_passport_number = dr["sims_admission_guardian_passport_number"].ToString();
                            simsobj.guardian_phone = dr["sims_admission_guardian_phone"].ToString();
                            simsobj.guardian_po_box = dr["sims_admission_guardian_po_box"].ToString();
                            simsobj.guardian_salutation_code = dr["sims_admission_guardian_salutation_code"].ToString();
                            simsobj.guardian_state = dr["sims_admission_guardian_state"].ToString();
                            simsobj.guardian_street_number = dr["sims_admission_guardian_street_number"].ToString();
                            simsobj.guardian_summary_address = dr["sims_admission_guardian_summary_address"].ToString();
                            try
                            {
                                simsobj.health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_expiry_date = "";
                            }
                            try
                            {
                                simsobj.health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_issue_date = "";
                            }
                            simsobj.health_card_issuing_authority = dr["sims_admission_health_card_issuing_authority"].ToString();
                            simsobj.health_card_number = dr["sims_admission_health_card_number"].ToString();
                            simsobj.health_hearing_desc = dr["sims_admission_health_hearing_desc"].ToString();
                            if (dr["sims_admission_health_hearing_status"].ToString() == "1")
                            {
                                simsobj.health_hearing_status = true;
                            }
                            else
                            {
                                simsobj.health_hearing_status = false;
                            }
                            simsobj.health_other_desc = dr["sims_admission_health_other_desc"].ToString();
                            if (dr["sims_admission_health_other_status"].ToString() == "1")
                            {
                                simsobj.health_other_status = true;
                            }
                            else
                            {
                                simsobj.health_other_status = false;
                            }
                            simsobj.health_restriction_desc = dr["sims_admission_health_restriction_desc"].ToString();
                            if (dr["sims_admission_health_restriction_status"].ToString() == "1")
                            {
                                simsobj.health_restriction_status = true;
                            }
                            else
                            {
                                simsobj.health_restriction_status = false;
                            }
                            simsobj.health_vision_desc = dr["sims_admission_health_vision_desc"].ToString();
                            if (dr["sims_admission_health_vision_status"].ToString() == "1")
                            {
                                simsobj.health_vision_status = true;
                            }
                            else
                            {
                                simsobj.health_vision_status = false;
                            }
                            simsobj.last_name = dr["sims_admission_passport_last_name_en"].ToString();
                            simsobj.last_name_ot = dr["sims_admission_passport_last_name_ot"].ToString();
                            simsobj.language_support_desc = dr["sims_admission_language_support_desc"].ToString();
                            if (dr["sims_admission_language_support_status"].ToString() == "1")
                            {
                                simsobj.language_support_status = true;
                            }
                            else
                            {
                                simsobj.language_support_status = false;
                            }
                            simsobj.legal_custody_code = dr["sims_admission_legal_custody"].ToString();
                            simsobj.main_language_code = dr["sims_admission_main_language_code"].ToString();
                            simsobj.main_language_r_code = dr["sims_admission_main_language_r"].ToString();
                            simsobj.main_language_s_code = dr["sims_admission_main_language_s"].ToString();
                            simsobj.main_language_w_code = dr["sims_admission_main_language_w"].ToString();
                            simsobj.other_language_code = dr["sims_admission_other_language"].ToString();
                            if (dr["sims_admission_marketing_code"].ToString() == "1")
                            {
                                simsobj.marketing_code = true;
                            }
                            else
                            {
                                simsobj.marketing_code = false;
                            }
                            simsobj.marketing_description = dr["sims_admission_marketing_description"].ToString();
                            simsobj.medication_desc = dr["sims_admission_medication_desc"].ToString();
                            if (dr["sims_admission_medication_status"].ToString() == "1")
                            {
                                simsobj.medication_status = true;
                            }
                            else
                            {
                                simsobj.medication_status = false;
                            }
                            simsobj.middle_name = dr["sims_admission_passport_middle_name_en"].ToString();
                            simsobj.midd_name_ot = dr["sims_admission_passport_middle_name_ot"].ToString();
                            simsobj.motherTounge_language_code = dr["sims_admission_main_language_m"].ToString();
                            simsobj.mother_appartment_number = dr["sims_admission_mother_appartment_number"].ToString();
                            simsobj.mother_area_number = dr["sims_admission_mother_area_number"].ToString();
                            simsobj.mother_building_number = dr["sims_admission_mother_building_number"].ToString();
                            simsobj.mother_city = dr["sims_admission_mother_city"].ToString();
                            simsobj.mother_company = dr["sims_admission_mother_company"].ToString();
                            simsobj.mother_country_code = dr["sims_admission_mother_country_code"].ToString();
                            simsobj.mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.mother_family_name = dr["sims_admission_mother_family_name"].ToString();
                            simsobj.mother_fax = dr["sims_admission_mother_fax"].ToString();
                            simsobj.mother_first_name = dr["sims_admission_mother_first_name"].ToString();
                            simsobj.mother_last_name = dr["sims_admission_mother_last_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_admission_mother_middle_name"].ToString();
                            simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            simsobj.mother_name_ot = dr["sims_admission_mother_name_ot"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_admission_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality2_code = dr["sims_admission_mother_nationality2_code"].ToString();
                            simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();
                            simsobj.mother_passport_number = dr["sims_admission_mother_passport_number"].ToString();
                            simsobj.mother_phone = dr["sims_admission_mother_phone"].ToString();
                            simsobj.mother_po_box = dr["sims_admission_mother_po_box"].ToString();
                            simsobj.mother_salutation_code = dr["sims_admission_mother_salutation_code"].ToString();
                            simsobj.mother_state = dr["sims_admission_mother_state"].ToString();
                            simsobj.mother_street_number = dr["sims_admission_mother_street_number"].ToString();
                            simsobj.mother_summary_address = dr["sims_admission_mother_summary_address"].ToString();
                            simsobj.music_desc = dr["sims_admission_music_desc"].ToString();
                            if (dr["sims_admission_music_status"].ToString() == "1")
                            {
                                simsobj.music_status = true;
                            }
                            else
                            {
                                simsobj.music_status = false;
                            }
                            simsobj.national_id = dr["sims_admission_national_id"].ToString();
                            try
                            {
                                simsobj.national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_issue_date = "";
                            }

                            try
                            {
                                simsobj.national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_expiry_date = "";
                            }
                            simsobj.nicke_name = dr["sims_admission_nickname"].ToString();
                            simsobj.parent_id = dr["sims_admission_parent_id"].ToString();
                            if (dr["sims_admission_parent_status_code"].ToString() == "1")
                            {
                                simsobj.parent_status_code = true;
                            }
                            else
                            {
                                simsobj.parent_status_code = false;
                            }
                            try
                            {
                                simsobj.passport_expiry = db.UIDDMMYYYYformat(dr["sims_admission_passport_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_expiry = "";
                            }
                            simsobj.passport_issue_auth = dr["sims_admission_passport_issuing_authority"].ToString();
                            try
                            {
                                simsobj.passport_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_issue_date = "";
                            }
                            simsobj.passport_issue_place = dr["sims_admission_passport_issue_place"].ToString();
                            simsobj.passport_num = dr["sims_admission_passport_number"].ToString();
                            simsobj.primary_contact_pref_code = dr["sims_admission_primary_contact_pref"].ToString();
                            if (dr["sims_admission_primary_contact_code"].ToString() == "1")
                            {
                                simsobj.primary_contact_code = true;
                            }
                            else
                            {
                                simsobj.primary_contact_code = false;
                            }
                            simsobj.primary_contact_pref_desc = dr["primary_contact_pref_desc"].ToString();
                            simsobj.pros_appl_num = dr["sims_admission_pros_application_number"].ToString();
                            simsobj.religion_code = dr["sims_admission_religion_code"].ToString();
                            simsobj.sibling_dob = dr["sims_admission_sibling_dob"].ToString();
                            simsobj.sibling_name = dr["sims_admission_sibling_name"].ToString();
                            simsobj.sibling_school_code = dr["sims_admission_sibling_school_code"].ToString();
                            simsobj.sibling_school_name = dr["sibSchoolName"].ToString();
                            simsobj.sports_desc = dr["sims_admission_sports_desc"].ToString();
                            if (dr["sims_admission_sports_status"].ToString() == "1")
                            {
                                simsobj.sports_status = true;
                            }
                            else
                            {
                                simsobj.sports_status = false;
                            }
                            simsobj.status = dr["sims_admission_status"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.user_code = dr["sims_admission_user_code"].ToString();
                            try
                            {
                                simsobj.visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_expiry_date = "";
                            }

                            try
                            {
                                simsobj.visa_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_issue_date = "";
                            }
                            simsobj.visa_issuing_authority = dr["sims_admission_visa_issuing_authority"].ToString();
                            simsobj.visa_issuing_place = dr["sims_admission_visa_issuing_place"].ToString();
                            simsobj.visa_number = dr["sims_admission_visa_number"].ToString();
                            simsobj.visa_type_desc = dr["sims_admission_visa_type_desc"].ToString();
                            simsobj.visa_type = dr["sims_admission_visa_type"].ToString();
                            if (dr["sims_admission_transport_status"].ToString() == "1")
                            {
                                simsobj.transport_status = true;
                            }
                            else
                            {
                                simsobj.transport_status = false;
                            }
                            simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();
                            simsobj.ip = dr["sims_admission_ip"].ToString();
                            #endregion

                            simsobj.sims_student_attribute1 = dr["sims_student_attribute1"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_student_attribute4"].ToString();
                            simsobj.academic_year_desc = dr["academic_year_desc"].ToString();
                            simsobj.sims_student_health_respiratory_status = dr["sims_student_health_respiratory_status"].ToString();
                            simsobj.sims_student_health_respiratory_desc = dr["sims_student_health_respiratory_desc"].ToString();
                            simsobj.sims_student_health_hay_fever_status = dr["sims_student_health_hay_fever_status"].ToString();
                            simsobj.sims_student_health_hay_fever_desc = dr["sims_student_health_hay_fever_desc"].ToString();
                            simsobj.sims_student_health_epilepsy_status = dr["sims_student_health_epilepsy_status"].ToString();
                            simsobj.sims_student_health_epilepsy_desc = dr["sims_student_health_epilepsy_desc"].ToString();
                            simsobj.sims_student_health_skin_status = dr["sims_student_health_skin_status"].ToString();
                            simsobj.sims_student_health_skin_desc = dr["sims_student_health_skin_desc"].ToString();
                            simsobj.sims_student_health_diabetes_status = dr["sims_student_health_diabetes_status"].ToString();
                            simsobj.sims_student_health_diabetes_desc = dr["sims_student_health_diabetes_desc"].ToString();
                            simsobj.sims_student_health_surgery_status = dr["sims_student_health_surgery_status"].ToString();
                            simsobj.sims_student_health_surgery_desc = dr["sims_student_health_surgery_desc"].ToString();


                            simsobj.admission_number = admission_number;
                            simsobj.stud_full_name = simsobj.first_name + " " + simsobj.middle_name + " " + simsobj.last_name;// o.stud_full_name;
                            simsobj.fee_category_code = dr["sims_admission_fee_category"].ToString();
                            try
                            {
                                simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());//o.admission_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.admission_date = "";
                            }
                            simsobj.pros_num = dr["pros_no"].ToString();//o.pros_no;
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.term_code = dr["sims_admission_term_code"].ToString();
                            try
                            {
                                simsobj.tent_join_date = db.UIDDMMYYYYformat(dr["tent_join_date"].ToString());//o.tent_join_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.tent_join_date = "";
                            }
                            simsobj.gender_desc = dr["gender"].ToString();// o.gender;
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            try
                            {
                                simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());//o.birth_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.birth_date = "";
                            }
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.nationality_code = dr["sims_admission_nationality_code"].ToString();//o.nation;
                            if (dr["sibling_status"].ToString() == "1")
                            {
                                simsobj.sibling_status = true;
                            }
                            else
                            {
                                simsobj.sibling_status = false;
                            }
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            // simsobj.sibling_enroll = (simsobj.sibling_status.Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.employee_code = dr["emp_code"].ToString();

                           

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();

                                simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();
                                simsobj.sims_student_attribute5 = dr["sims_student_attribute5"].ToString();
                                simsobj.sims_student_attribute6 = dr["sims_student_attribute6"].ToString();
                                simsobj.sims_student_attribute7 = dr["sims_student_attribute7"].ToString();
                                simsobj.sims_student_attribute8 = dr["sims_student_attribute8"].ToString();
                                simsobj.sims_student_attribute9 = dr["sims_student_attribute9"].ToString();
                                simsobj.sims_student_attribute10 = dr["sims_student_attribute10"].ToString();
                                simsobj.sims_student_attribute11 = dr["sims_student_attribute11"].ToString();
                                simsobj.sims_student_attribute12 = dr["sims_student_attribute12"].ToString();

                              //  simsobj.sims_admission_second_lang_code = dr["sims_admission_second_lang_code"].ToString();
                              //  simsobj.sims_admission_second_lang_name = dr["sims_admission_second_lang_name"].ToString();
                            }
                            catch (Exception ee)
                            {
                            }


                            try
                            {
                                if (dr["sims_admission_learning_therapy_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_learning_therapy_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_learning_therapy_status = false;
                                }

                                simsobj.sims_admission_learning_therapy_desc = dr["sims_admission_learning_therapy_desc"].ToString();

                                if (dr["sims_admission_special_education_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_special_education_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_special_education_status = false;
                                }

                                simsobj.sims_admission_special_education_desc = dr["sims_admission_special_education_desc"].ToString();

                                if (dr["sims_admission_falled_grade_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_falled_grade_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_falled_grade_status = false;
                                }

                                simsobj.sims_admission_falled_grade_desc = dr["sims_admission_falled_grade_desc"].ToString();

                                if (dr["sims_admission_communication_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_communication_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_communication_status = false;
                                }
                                simsobj.sims_admission_communication_desc = dr["sims_admission_communication_desc"].ToString();
                                if (dr["sims_admission_specialAct_status"].ToString() == "1")
                                {
                                    simsobj.sims_admission_specialAct_status = true;
                                }
                                else
                                {
                                    simsobj.sims_admission_specialAct_status = false;
                                }
                                simsobj.sims_admission_specialAct_desc = dr["sims_admission_specialAct_desc"].ToString();

                            }
                            catch (Exception et)
                            {
                            }
                        }

                        return simsobj;
                    }
                }
                return simsobj;
            }
            catch (Exception x)
            {
                return simsobj;
            }
        }

        [Route("ApproveMultiple")]
        public HttpResponseMessage ApproveMultiple(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAdmPromoteData()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "ApproveMultiple", "admission_number"));

            List<string> lst = new List<string>();
            lst = obj.admission_number.Substring(0, obj.admission_number.Length - 1).Split(',').ToList();
            Message message = new Message();

            try
            {
                if (lst != null)
                {
                    foreach (string admno in lst)
                    {
                        ApproveStudent1(GetTabStudentData(admno));
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, message);

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("AdmissionStatusOpr")]
        public HttpResponseMessage AdmissionStatusOpr(Sims010_Edit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AdmissionStatusOpr(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "AdmissionStatus"));
            int cnt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr",obj.opr),
                             new SqlParameter("@sims_admission_numberColl", obj.admission_number),
                             new SqlParameter("@sims_admission_status", obj.status),

                        });
                    // if (dr.HasRows)
                    // {
                    if (dr.RecordsAffected > 0)
                    {
                        cnt = 1;
                    }

                    //  }
                    //else
                    //    return Request.CreateResponse(HttpStatusCode.OK, cnt);
                }
                return Request.CreateResponse(HttpStatusCode.OK, cnt);
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, cnt);
            }

        }

        [Route("getsectionList")]
        public HttpResponseMessage getsectionList(string cur_code, string acad_yr, string grade_code, string section_code)
        {

            List<Sims010_Edit> listall = new List<Sims010_Edit>();
            try
            {
                if (section_code == "undefined")
                {
                    section_code = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", 'M'),
                             new SqlParameter("@sims_admission_cur_code",cur_code),
                             new SqlParameter("@sims_admission_academic_year",acad_yr),
                             new SqlParameter("@sims_admission_grade_code",grade_code),
                             new SqlParameter("@sims_admission_section_code",section_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit getlist = new Sims010_Edit();
                            getlist.section_code = dr["sims_section_code"].ToString();
                            getlist.section_name = dr["sims_section_name_en"].ToString();
                            getlist.section_strength = dr["sims_section_stregth"].ToString();
                            getlist.female_strength = dr["female"].ToString();
                            getlist.male_strength = dr["male"].ToString();
                            getlist.current_strength = dr["Current_cnt"].ToString();
                            getlist.gender_code = dr["sims_section_gender"].ToString();
                            listall.Add(getlist);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, listall);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, listall);
            }

        }

        private string GetcheckEmailProfileForAdmission()
        {
            Comn_email em = new Comn_email();
            List<Comn_email> lstEmails = new List<Comn_email>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'F')
                            
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            em.username = dr["sims_smtp_username"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // return em.username;
            }
            return em.username;
        }

        private bool ScheduleMails(string filenames, List<Comn_email> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool flag = true;

            List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    string attachments = string.Empty;
                    foreach (comn_email_attachments att in data)
                    {
                        attachments = attachments + ',' + att.attFilename;
                    }
                    if (!string.IsNullOrEmpty(attachments))
                    {
                        attachments = attachments.Substring(1);
                    }


                    foreach (Comn_email email in emailList)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                        });
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return flag;
        }

        [Route("getsection_ajb")]
        public HttpResponseMessage getsection_ajb(string cur_code, string acad_yr, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getsection_ajb(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Attendance", "AttendanceRule"));

            List<Sims010_Edit> list = new List<Sims010_Edit>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "PM"),
                            new SqlParameter("@CURR_CODE",cur_code),
                            new SqlParameter("@ACA_YEAR",acad_yr),
                            new SqlParameter("@GRADE_CODE",grade_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit simsobj = new Sims010_Edit();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sec_count = dr["sec_count"].ToString();
                            list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("CUDUpdateAdmissionDash")]
        public HttpResponseMessage CUDUpdateAdmissionDash(List<Sims010_Edit> simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateAdmission"));
            Sims010_Edit res = new Sims010_Edit();
            bool inserted = false;
            // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims010_Edit data in simsobj)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[Sims_admission_proc]",
                                new List<SqlParameter>() 
                         { 
                           
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@sims_admission_number", data.admission_number),
                            new SqlParameter("@sims_admission_recommendation",data.sims_admission_recommendation)
                           
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
               
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
        }

        [Route("subjectDetails")]
        public HttpResponseMessage subjectDetails(param p)
        {

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "AB"),
                            new SqlParameter("@CURR_CODE", p.curr_code),
                            new SqlParameter("@ACA_YEAR", p.AcadmicYear),
                            new SqlParameter("@GRADE_CODE", p.gradeCode),
                            new SqlParameter("@SECTION_CODE", p.sectionCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.subject_code = dr["sims_subject_code"].ToString();
                            simsobj.subject_name = dr["subject_name"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetStreamPrefferd")]
        public HttpResponseMessage GetStreamPrefferd()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStreamPrefferd(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "GetStreamPrefferd"));
            List<Sims010_Edit> route = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","AB"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.stream_code = dr["sims_appl_parameter"].ToString();
                            obj.stream_desc = dr["sims_appl_form_field_value1"].ToString();
                            route.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, route);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, route);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, route);
            }
        }

        [Route("GetSubject")]
        public HttpResponseMessage GetSubject(string stream)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSubject(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "GetSubject"));
            List<Sims010_Edit> subject = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","CD"),
                            new SqlParameter("@sims_subject_stream",stream)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.subject_code = dr["sims_appl_parameter"].ToString();
                            obj.subject_name = dr["subject_name"].ToString();
                            subject.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject);
            }
        }

        [Route("Get_secondlangDetails")]
        public HttpResponseMessage Get_secondlangDetails(string cur_code, string acad_yr, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","W"),
                            new SqlParameter("@CURR_CODE",cur_code),
                             new SqlParameter("@ACA_YEAR",acad_yr),
                              new SqlParameter("@GRADE_CODE",grade_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.sims_subject_second_lang_code = dr["sims_subject_second_lang_code"].ToString();
                            obj.sims_subject_third_lang_code = dr["sims_subject_third_lang_code"].ToString();
                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        [Route("Get_thirdlangDetails")]
        public HttpResponseMessage Get_thirdlangDetails(string cur_code, string acad_yr, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_langDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "language", "AdmissionNoDetails"));
            List<Sims010_Edit> subject_lst = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","BC"),
                            new SqlParameter("@CURR_CODE",cur_code),
                             new SqlParameter("@ACA_YEAR",acad_yr),
                              new SqlParameter("@GRADE_CODE",grade_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit obj = new Sims010_Edit();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.sims_subject_second_lang_code = dr["sims_subject_second_lang_code"].ToString();
                            obj.sims_subject_third_lang_code = dr["sims_subject_third_lang_code"].ToString();
                            subject_lst.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_lst);
            }
        }

        [Route("CheckEMPCode")]
        public HttpResponseMessage CheckEMPCode(string emp_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckEMPCode(),PARAMETERS :: emp_id{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckEMPCode", emp_id));

            bool ifexists = false;
            List<Sims010_Edit> parent = new List<Sims010_Edit>();
            Sims010_Edit simsobj = new Sims010_Edit();

            int employee_code = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'L'),
                                 new SqlParameter("@sims_admission_employee_code", emp_id)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;
                        while (dr.Read())
                        {
                            employee_code = int.Parse(dr["emp_cnt"].ToString());
                        }
                    }
                }

                if (employee_code > 0)
                {
                    simsobj.chkstatus = true;
                    parent.Add(simsobj);
                }
                else
                {
                    simsobj.chkstatus = false;
                    simsobj.strMessage = "No Records Found";
                    parent.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj);
            }
        }

       

    }
}