﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/attendanceDashAnalysis")]
    public class AttendanceAnalysisDashboardReportController : ApiController
    {

        [Route("getAttendanceAnalysis")]
        public HttpResponseMessage getAttendanceAnalysis()
        {
            List<atdr01> dailystatus = new List<atdr01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 sequence = new atdr01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.capacity = dr["capacity"].ToString();
                            sequence.intake_student = dr["intake_student"].ToString();
                            sequence.vacancy = dr["vacancy"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.present = dr["present"].ToString();
                            sequence.absent = dr["absent"].ToString();
                            sequence.others = dr["others"].ToString();
                            sequence.unmark = dr["unmark"].ToString();
                            sequence.registration = dr["registration"].ToString();
                            
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }



        [Route("getAttendanceAnalysis_total")]
        public HttpResponseMessage getdailystatus_total()
        {
            List<atdr01> dailystatus = new List<atdr01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "W"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 sequence = new atdr01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.capacity = dr["capacity"].ToString();
                            sequence.intake_student = dr["intake_student"].ToString();
                            sequence.vacancy = dr["vacancy"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.present = dr["present"].ToString();
                            sequence.absent = dr["absent"].ToString();
                            sequence.unmark = dr["unmark"].ToString();
                            sequence.others = dr["others"].ToString();
                            sequence.registration = dr["registration"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getAbsentStudentList")]
        public HttpResponseMessage getAbsentStudentList()
        {
            List<atdr01> dailystatus = new List<atdr01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 sequence = new atdr01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.flag = dr["flag"].ToString();                          
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getStudentAnalysisGraph1")]
        public HttpResponseMessage getStudentAnalysisGraph1(string curCode, string acaYear,string month)
        {
            List<atdr01> list = new List<atdr01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "B"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                           new SqlParameter("@month", month),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 simsobj = new atdr01();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.att_month = dr["att_month"].ToString();
                            simsobj.sims_attendance_date = dr["sims_attendance_date"].ToString();
                            simsobj.present = dr["present"].ToString();
                            simsobj.absent = dr["absent"].ToString();
                            simsobj.others = dr["others"].ToString();
                            simsobj.unmark = dr["unmark"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

         [Route("getmonth")]
        public HttpResponseMessage getmonth()
        {
            List<atdr01> dailystatus = new List<atdr01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 sequence = new atdr01();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.month_status = dr["month_status"].ToString();                            
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }


        [Route("getStudentAnalysisGraph1class")]
        public HttpResponseMessage getStudentAnalysisGraph1class(string curCode, string acaYear, string grade)
        {
            List<atdr01> list = new List<atdr01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "D"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                           new SqlParameter("@grade", grade),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 simsobj = new atdr01();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.att_month = dr["att_month"].ToString();
                            simsobj.sims_attendance_date = dr["sims_attendance_date"].ToString();
                            simsobj.present = dr["present"].ToString();
                            simsobj.absent = dr["absent"].ToString();
                            simsobj.others = dr["others"].ToString();
                            simsobj.unmark = dr["unmark"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getClass")]
        public HttpResponseMessage getClass(string curCode, string acaYear)
        {
            List<atdr01> dailystatus = new List<atdr01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "E"),
                              new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            atdr01 sequence = new atdr01();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();                          
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getAttendanceSnapshotReport")]
        public HttpResponseMessage getAttendanceSnapshotReport()
        {
            // List<atdr01> dailystatus = new List<atdr01>();
            string rpt_name = string.Empty;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_dashboard_analysis]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Z"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            // atdr01 sequence = new atdr01();
                            rpt_name = dr["attendanca_snapshot_report"].ToString();
                            //dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, rpt_name);
        }
    }
}