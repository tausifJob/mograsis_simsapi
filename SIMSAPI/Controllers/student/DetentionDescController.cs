﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/Student")]
    [BasicAuthentication]
    public class DetentionDescController : ApiController
    {
        [Route("getDetentionDesc")]
        public HttpResponseMessage getDetentionDesc()
        {
            List<DetDes> list = new List<DetDes>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_detention_desc_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            DetDes obj = new DetDes();
                           
                            obj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            obj.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                            obj.sims_detention_description = dr["sims_detention_description"].ToString();
                            obj.sims_detention_description_ot = dr["sims_detention_description_ot"].ToString();
                            obj.sims_display_order = dr["sims_display_order"].ToString();
                            obj.sims_detention_desc_created_date = db.UIDDMMYYYYformat(dr["sims_detention_desc_created_date"].ToString());
                            obj.sims_detention_desc_created_user_code = dr["sims_detention_desc_created_user_code"].ToString();
                            obj.sims_detention_name = dr["sims_detention_name"].ToString();
                            if (dr["sims_detention_desc_status"].ToString() == "A")
                                obj.sims_detention_desc_status = true;
                            else
                                obj.sims_detention_desc_status = false;
                          
                            list.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateDeleteDetentionDesc")]
        public HttpResponseMessage InsertUpdateDeleteReportCard(List<DetDes> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var simsobj in data)
                    {
                     
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_detention_desc_proc]",
                        new List<SqlParameter>()
                            {

                                  new SqlParameter("@opr", simsobj.opr),
                                  new SqlParameter("@sims_detention_level_code", simsobj.sims_detention_level_code),
                                  new SqlParameter("@sims_detention_desc_code", simsobj.sims_detention_desc_code),
                                  new SqlParameter("@sims_detention_description", simsobj.sims_detention_description),
                                  new SqlParameter("@sims_detention_description_ot", simsobj.sims_detention_description_ot),
                                  new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                                  new SqlParameter("@sims_detention_desc_created_date",db.DBYYYYMMDDformat(simsobj.sims_detention_desc_created_date)),
                                  new SqlParameter("@sims_detention_desc_created_user_code", simsobj.sims_detention_desc_created_user_code),
                                  new SqlParameter("@sims_detention_desc_status",simsobj.sims_detention_desc_status),
                              
                                  
                               
                            });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                    }



                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getDetentionLevelName")]
        public HttpResponseMessage getDetentionLevelName()
        {
            List<DetDes> list = new List<DetDes>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_detention_desc_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DetDes simsobj = new DetDes();
                            simsobj.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            simsobj.sims_detention_name = dr["sims_detention_name"].ToString();
                            list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

       
    }
}