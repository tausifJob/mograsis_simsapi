﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/AgendaConfiguration")]
    public class AgendaConfigurationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("GetAgendatype")]
        public HttpResponseMessage GetAgendatype()
        {
            List<AgendaConfig> mod_list = new List<AgendaConfig>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AgendaConfig simsobj = new AgendaConfig();
                            simsobj.sims_agenda_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_agenda_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        //Supervisor
        [Route("getSupervisorGradeSectionSubject")]
        public HttpResponseMessage getSupervisorGradeSectionSubject(string data)
        {
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            List<AgendaConfig> mod_list = new List<AgendaConfig>();
            List<supervisor> op = new List<supervisor>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),                          
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@aca_year", comnobj.sims_academic_year),    
                           // new SqlParameter("@sims_supervisor_code", em_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AgendaConfig simsobj = new AgendaConfig();
                            simsobj.sup_grade = new List<supervisor_grade_section_subject>();
                            simsobj.sims_supervisor_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_supervisor_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            try
                            {
                                if (dr["cnt"].ToString() == "1")
                                    simsobj.isSelected = true;

                                if (dr["scnt"].ToString() == "1")
                                    simsobj.isSelected_sup = true;
                            }
                            catch (Exception ex) { }

                            mod_list.Add(simsobj);
                        }
                        foreach (AgendaConfig x in mod_list)
                        {
                            supervisor s = new supervisor();
                            s.sims_supervisor_code = x.sims_supervisor_code;
                            s.sims_supervisor_name = x.sims_supervisor_name;
                            s.isSelected = x.isSelected_sup;
                            s.tid = "0";
                            s.sup_grade = new List<supervisor_grade>();
                            var v = from p in op where p.sims_supervisor_code == s.sims_supervisor_code select p;
                            if (v.Count() == 0)
                            {
                                s.sup_grade = getSGrade(mod_list, s);
                                op.Add(s);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<supervisor_grade> getSGrade(List<AgendaConfig> mod_list, supervisor s)
        {
            List<supervisor_grade> op = new List<supervisor_grade>();
            var i = 0;
            foreach (var item in mod_list)
            {
                if (item.sims_supervisor_code == s.sims_supervisor_code)
                {
                    supervisor_grade g = new supervisor_grade();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_grade_name = item.sims_grade_name;
                    g.sims_supervisor_code = item.sims_supervisor_code;
                    g.isSelected = item.isSelected;

                    g.gid = i + "";
                    i++;
                    g.sup_sect = new List<supervisor_section>();
                    var v = from p in op where p.sims_grade_code == g.sims_grade_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_sect = getSSec(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_section> getSSec(List<AgendaConfig> mod_list, supervisor_grade s)
        {
            List<supervisor_section> op = new List<supervisor_section>();
            var i = 0;
            foreach (var item in mod_list)
            {
                if (item.sims_supervisor_code == s.sims_supervisor_code && item.sims_grade_code == s.sims_grade_code)
                {
                    supervisor_section g = new supervisor_section();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_section_name = item.sims_section_name;
                    g.sims_supervisor_code = item.sims_supervisor_code;
                    g.isSelected = item.isSelected;


                    g.sid = i + "";
                    i++;
                    g.sup_subject = new List<supervisor_subject>();
                    var v = from p in op where p.sims_section_code == g.sims_section_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_subject = getSSub(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_subject> getSSub(List<AgendaConfig> mod_list, supervisor_section s)
        {
            List<supervisor_subject> op = new List<supervisor_subject>();
            foreach (var item in mod_list)
            {
                if (item.sims_supervisor_code == s.sims_supervisor_code && item.sims_grade_code == s.sims_grade_code && item.sims_section_code == s.sims_section_code)
                {
                    supervisor_subject g = new supervisor_subject();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_subject_name = item.sims_subject_name;
                    g.sims_subject_code = item.sims_subject_code;
                    g.sims_supervisor_code = item.sims_supervisor_code;
                    g.isSelected = item.isSelected;

                    var v = from p in op where p.sims_subject_code == g.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        //HOD
        [Route("getHODGradeSectionSubject")]
        public HttpResponseMessage getHODGradeSectionSubject(string data)
        {
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            List<AgendaConfig> mod_list = new List<AgendaConfig>();
            List<supervisor> op = new List<supervisor>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),                          
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@aca_year", comnobj.sims_academic_year),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AgendaConfig simsobj = new AgendaConfig();
                            simsobj.sup_grade = new List<supervisor_grade_section_subject>();
                            simsobj.sims_head_teacher_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_head_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            try
                            {
                                if (dr["cnt"].ToString() == "1")
                                    simsobj.isSelected = true;

                                if (dr["scnt"].ToString() == "1")
                                    simsobj.isSelected_sup = true;
                            }
                            catch (Exception ex) { }

                            mod_list.Add(simsobj);
                        }
                        foreach (AgendaConfig x in mod_list)
                        {
                            supervisor s = new supervisor();
                            s.sims_head_teacher_code = x.sims_head_teacher_code;
                            s.sims_head_teacher_name = x.sims_head_teacher_name;
                            s.isSelected = x.isSelected_sup;
                            s.sup_grade = new List<supervisor_grade>();
                            var v = from p in op where p.sims_head_teacher_code == s.sims_head_teacher_code select p;
                            if (v.Count() == 0)
                            {
                                s.sup_grade = getHGrade(mod_list, s);
                                op.Add(s);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<supervisor_grade> getHGrade(List<AgendaConfig> mod_list, supervisor s)
        {
            List<supervisor_grade> op = new List<supervisor_grade>();
            foreach (var item in mod_list)
            {
                if (item.sims_head_teacher_code == s.sims_head_teacher_code)
                {
                    supervisor_grade g = new supervisor_grade();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_grade_name = item.sims_grade_name;
                    g.sims_head_teacher_code = item.sims_head_teacher_code;
                    g.isSelected = item.isSelected;

                    g.sup_sect = new List<supervisor_section>();
                    var v = from p in op where p.sims_grade_code == g.sims_grade_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_sect = getHSec(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_section> getHSec(List<AgendaConfig> mod_list, supervisor_grade s)
        {
            List<supervisor_section> op = new List<supervisor_section>();
            foreach (var item in mod_list)
            {
                if (item.sims_head_teacher_code == s.sims_head_teacher_code && item.sims_grade_code == s.sims_grade_code)
                {
                    supervisor_section g = new supervisor_section();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_section_name = item.sims_section_name;
                    g.sims_head_teacher_code = item.sims_head_teacher_code;
                    g.isSelected = item.isSelected;

                    g.sup_subject = new List<supervisor_subject>();
                    var v = from p in op where p.sims_section_code == g.sims_section_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_subject = getHSub(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_subject> getHSub(List<AgendaConfig> mod_list, supervisor_section s)
        {
            List<supervisor_subject> op = new List<supervisor_subject>();
            foreach (var item in mod_list)
            {
                if (item.sims_head_teacher_code == s.sims_head_teacher_code && item.sims_grade_code == s.sims_grade_code && item.sims_section_code == s.sims_section_code)
                {
                    supervisor_subject g = new supervisor_subject();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_subject_name = item.sims_subject_name;
                    g.sims_subject_code = item.sims_subject_code;
                    g.sims_head_teacher_code = item.sims_head_teacher_code;
                    g.isSelected = item.isSelected;

                    var v = from p in op where p.sims_subject_code == g.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        //Subject Teacher
        [Route("getSubjectTeacherGradeSectionSubject")]
        public HttpResponseMessage getSubjectTeacherGradeSectionSubject(string data)
        {
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            List<AgendaConfig> mod_list = new List<AgendaConfig>();
            List<supervisor> op = new List<supervisor>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),                          
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@aca_year", comnobj.sims_academic_year),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AgendaConfig simsobj = new AgendaConfig();
                            //simsobj.tid = dr["tid"].ToString();
                            //simsobj.gid = dr["gid"].ToString();
                            //simsobj.sid = dr["sid"].ToString();
                            simsobj.sup_grade = new List<supervisor_grade_section_subject>();
                            simsobj.sims_bell_teacher_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_bell_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                        //var i = 0;
                        foreach (AgendaConfig x in mod_list)
                        {
                            supervisor s = new supervisor();
                            s.sims_bell_teacher_code = x.sims_bell_teacher_code;
                            s.sims_bell_teacher_name = x.sims_bell_teacher_name;
                            s.tid = "0";
                            s.sup_grade = new List<supervisor_grade>();
                            var v = from p in op where p.sims_bell_teacher_code == s.sims_bell_teacher_code select p;
                            if (v.Count() == 0)
                            {
                                s.sup_grade = getCGrade(mod_list, s);
                                op.Add(s);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<supervisor_grade> getCGrade(List<AgendaConfig> mod_list, supervisor s)
        {
            List<supervisor_grade> op = new List<supervisor_grade>();
            var i = 0;
            foreach (var item in mod_list)
            {
                if (item.sims_bell_teacher_code == s.sims_bell_teacher_code)
                {
                    supervisor_grade g = new supervisor_grade();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_grade_name = item.sims_grade_name;
                    g.sims_bell_teacher_code = item.sims_bell_teacher_code;
                    g.gid = i + "";
                    i++;
                    g.sup_sect = new List<supervisor_section>();
                    var v = from p in op where p.sims_grade_code == g.sims_grade_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_sect = getCSec(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_section> getCSec(List<AgendaConfig> mod_list, supervisor_grade s)
        {
            List<supervisor_section> op = new List<supervisor_section>();
            var i = 0;
            foreach (var item in mod_list)
            {
                if (item.sims_bell_teacher_code == s.sims_bell_teacher_code && item.sims_grade_code == s.sims_grade_code)
                {
                    supervisor_section g = new supervisor_section();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_section_name = item.sims_section_name;
                    g.sims_bell_teacher_code = item.sims_bell_teacher_code;
                    g.sid = i + "";
                    i++;
                    g.sup_subject = new List<supervisor_subject>();
                    var v = from p in op where p.sims_section_code == g.sims_section_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_subject = getCSub(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_subject> getCSub(List<AgendaConfig> mod_list, supervisor_section s)
        {
            List<supervisor_subject> op = new List<supervisor_subject>();
            foreach (var item in mod_list)
            {
                if (item.sims_bell_teacher_code == s.sims_bell_teacher_code && item.sims_grade_code == s.sims_grade_code && item.sims_section_code == s.sims_section_code)
                {
                    supervisor_subject g = new supervisor_subject();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_subject_name = item.sims_subject_name;
                    g.sims_subject_code = item.sims_subject_code;
                    g.sims_bell_teacher_code = item.sims_bell_teacher_code;
                    var v = from p in op where p.sims_subject_code == g.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        //Class Teacher
        [Route("getClassTeacherGradeSectionSubject")]
        public HttpResponseMessage getClassTeacherGradeSectionSubject(string data)
        {
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            List<AgendaConfig> mod_list = new List<AgendaConfig>();
            List<supervisor> op = new List<supervisor>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),                          
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@aca_year", comnobj.sims_academic_year),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AgendaConfig simsobj = new AgendaConfig();
                            simsobj.sup_grade = new List<supervisor_grade_section_subject>();
                            simsobj.sims_class_teacher_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_class_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                        foreach (AgendaConfig x in mod_list)
                        {
                            supervisor s = new supervisor();
                            s.sims_class_teacher_code = x.sims_class_teacher_code;
                            s.sims_class_teacher_name = x.sims_class_teacher_name;
                            s.sup_grade = new List<supervisor_grade>();
                            var v = from p in op where p.sims_class_teacher_code == s.sims_class_teacher_code select p;
                            if (v.Count() == 0)
                            {
                                s.sup_grade = getTGrade(mod_list, s);
                                op.Add(s);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<supervisor_grade> getTGrade(List<AgendaConfig> mod_list, supervisor s)
        {
            List<supervisor_grade> op = new List<supervisor_grade>();
            foreach (var item in mod_list)
            {
                if (item.sims_class_teacher_code == s.sims_class_teacher_code)
                {
                    supervisor_grade g = new supervisor_grade();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_grade_name = item.sims_grade_name;
                    g.sims_class_teacher_code = item.sims_class_teacher_code;
                    g.sup_sect = new List<supervisor_section>();
                    var v = from p in op where p.sims_grade_code == g.sims_grade_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_sect = getTSec(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_section> getTSec(List<AgendaConfig> mod_list, supervisor_grade s)
        {
            List<supervisor_section> op = new List<supervisor_section>();
            foreach (var item in mod_list)
            {
                if (item.sims_class_teacher_code == s.sims_class_teacher_code && item.sims_grade_code == s.sims_grade_code)
                {
                    supervisor_section g = new supervisor_section();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_section_name = item.sims_section_name;
                    g.sims_class_teacher_code = item.sims_class_teacher_code;
                    g.sup_subject = new List<supervisor_subject>();
                    var v = from p in op where p.sims_section_code == g.sims_section_code select p;
                    if (v.Count() == 0)
                    {
                        g.sup_subject = getTSub(mod_list, g);
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        private List<supervisor_subject> getTSub(List<AgendaConfig> mod_list, supervisor_section s)
        {
            List<supervisor_subject> op = new List<supervisor_subject>();
            foreach (var item in mod_list)
            {
                if (item.sims_class_teacher_code == s.sims_class_teacher_code && item.sims_grade_code == s.sims_grade_code && item.sims_section_code == s.sims_section_code)
                {
                    supervisor_subject g = new supervisor_subject();
                    g.sims_academic_year = item.sims_academic_year;
                    g.sims_cur_code = item.sims_cur_code;
                    g.sims_grade_code = item.sims_grade_code;
                    g.sims_section_code = item.sims_section_code;
                    g.sims_subject_name = item.sims_subject_name;
                    g.sims_subject_code = item.sims_subject_code;
                    g.sims_class_teacher_code = item.sims_class_teacher_code;
                    var v = from p in op where p.sims_subject_code == g.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        op.Add(g);
                    }
                }
            }
            return op;
        }

        //Save Records
        [Route("InsertAgendaAccess")]
        public HttpResponseMessage InsertAgendaAccess(List<AgendaConfig> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (AgendaConfig simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_agenda_configuration_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'I'),                                
                                new SqlParameter("@sims_sr_no", simsobj.sims_sr_no),                                 
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),                                 
                                new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),                                 
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),                                 
                                new SqlParameter("@sims_employee_code",simsobj.sims_employee_code),
                                new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),                                 
                                new SqlParameter("@sims_agenda_assign_type", simsobj.sims_agenda_assign_type),                                 
                                new SqlParameter("@sims_employee_type",simsobj.sims_employee_type),
                                new SqlParameter("@sims_status",simsobj.sims_status),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}