﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/QuestionBankProduct")]
    public class QuestionBankProductController : ApiController
    {

        [Route("getQuestionBankDetails")]
        public HttpResponseMessage getQuestionBankDetails(string grade_code)
        {

            List<que_bank_product> lst = new List<que_bank_product>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_question_bank_product_proc]",
                    new List<SqlParameter>()
                    {
                          new SqlParameter("@opr", 'B'),
                          new SqlParameter("@grade_code",grade_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            que_bank_product simsobj = new que_bank_product();

                            simsobj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            simsobj.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();                       
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getQuestionBankProductDetails")]
        public HttpResponseMessage getQuestionBankProductDetails()
        {
            List<que_bank_product> lst = new List<que_bank_product>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_question_bank_product_proc]",
                    new List<SqlParameter>()
                    {
                          new SqlParameter("@opr", 'S')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            que_bank_product simsobj = new que_bank_product();

                            simsobj.sims_questionbank_set_no = dr["sims_questionbank_set_no"].ToString();
                            simsobj.sims_questionbank_set_name = dr["sims_questionbank_set_name"].ToString();
                            simsobj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            simsobj.sims_questionbank_set_cost = dr["sims_questionbank_set_cost"].ToString();
                            simsobj.sims_questionbank_set_status = dr["sims_questionbank_set_status"].ToString().Equals("A") ? true : false; 
                            simsobj.sims_questionbank_set_created_by = dr["sims_questionbank_set_created_by"].ToString();
                            simsobj.sims_questionbank_set_created_date = db.UIDDMMYYYYformat(dr["sims_questionbank_set_created_date"].ToString());
                            simsobj.sims_questionbank_set_free_status = dr["sims_questionbank_set_free_status"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_questionbank_set_logo_path = dr["sims_questionbank_set_logo_path"].ToString();
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("CUDQuestionBankProductSet")]
        public HttpResponseMessage CUDQuestionBankProductSet(List<que_bank_product> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {                 
                    db.Open();
                    foreach (que_bank_product simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_question_bank_product_proc]",
                        new List<SqlParameter>()
                        {
                              new SqlParameter("@opr", simsobj.opr),
                              new SqlParameter("@sims_questionbank_set_no", simsobj.sims_questionbank_set_no),
                              new SqlParameter("@sims_questionbank_set_name", simsobj.sims_questionbank_set_name),
                              new SqlParameter("@sims_questionbank_code", simsobj.sims_questionbank_code),
                              new SqlParameter("@sims_questionbank_set_cost", simsobj.sims_questionbank_set_cost),
                              new SqlParameter("@sims_questionbank_set_status", simsobj.sims_questionbank_set_status==true?"A":"I"),
                              new SqlParameter("@sims_questionbank_set_created_by", simsobj.sims_questionbank_set_created_by),
                              new SqlParameter("@sims_questionbank_set_free_status", simsobj.sims_questionbank_set_free_status==true?"Y":"N"),
                              new SqlParameter("@sims_questionbank_set_logo_path", simsobj.sims_questionbank_set_logo_path)
                        });
                        
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        
                        else
                        {
                            insert = false;
                        }                      
                    }                    

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,x.Message);
            }
           
        }
        
        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades()
        {           
            List<que_bank_product> grade_list = new List<que_bank_product>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_question_bank_product_proc]",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "C")                             
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            que_bank_product simsobj = new que_bank_product();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
           {                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }


        [Route("getCurrencyDetails")]
        public HttpResponseMessage getCurrencyDetails()
        {
            List<que_bank_product> lst = new List<que_bank_product>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_question_bank_product_proc]",
                    new List<SqlParameter>()
                    {
                          new SqlParameter("@opr", 'E')                          
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            que_bank_product simsobj = new que_bank_product();

                            simsobj.comp_curcy_code = dr["comp_curcy_code"].ToString();
                            simsobj.comp_curcy_dec = dr["comp_curcy_dec"].ToString();
                            lst.Add(simsobj);
                        }   
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


    }
}