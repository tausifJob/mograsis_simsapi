﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Web;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/student/studentImage")]
    public class StudentImageViewController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getStudentsDetails")]
        public HttpResponseMessage getStudentsDetails(string curcode, string academicyear, string gradecode, string section)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<Sims175> lstBoard = new List<Sims175>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),
                             new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@sims_academic_year", academicyear),
                             new SqlParameter("@sims_grade_code", gradecode),
                             new SqlParameter("@sims_section_code", section),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims175 sequence = new Sims175();

                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();

                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();

                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_last_name_en"].ToString();

                            sequence.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();

                            sequence.sims_student_passport_full_name_en = dr["sims_student_passport_first_name_en"].ToString() + ' ' + dr["sims_student_passport_middle_name_en"].ToString() +' '+ dr["sims_student_passport_last_name_en"];

                            sequence.sims_student_image = dr["sims_student_img"].ToString();

                            sequence.sims_parent_login_code = dr["sims_parent_login_code"].ToString();

                            sequence.parent_Father_name = dr["parent_Father_name"].ToString();

                            sequence.sims_parent_father_img = dr["sims_parent_father_img"].ToString();

                            sequence.parent_Mother_name = dr["parent_Mother_name"].ToString();

                            sequence.sims_parent_mother_img = dr["sims_parent_mother_img"].ToString();

                             




                            lstBoard.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstBoard);
        }

        [Route("CUDStudentsDetails")]
        public HttpResponseMessage CUDStudentsDetails(Sims175 simsobj)
        {
            Message msg = new Message();
            bool deleted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",simsobj.opr),
                             new SqlParameter("@sims_student_enroll_number", simsobj.sims_student_enroll_number),
                               new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                                new SqlParameter("@sims_parent_login_code", simsobj.sims_parent_login_code),
                          
                            
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        deleted = true;
                    }
                    if (deleted == true)
                    {
                        msg.strMessage = "Image Deleted Successfully";
                    }
                }

            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, deleted);
        }

        [Route("CUDUpdatePics")]
        public HttpResponseMessage CUDUpdatePics(Sims175 simsobj)//string opr, string data
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_student_enroll_number", simsobj.sims_student_enroll_number),
                            new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                         
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            insert = true;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, insert);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    insert = false;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDUpdatePicsF")]
        public HttpResponseMessage CUDUpdatePicsF(Sims175 simsobj)//string opr, string data
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_parent_login_code", simsobj.sims_parent_login_code),
                            new SqlParameter("@sims_parent_father_img", simsobj.sims_parent_father_img),
                         
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            insert = true;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, insert);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    insert = false;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDUpdatePicsM")]
        public HttpResponseMessage CUDUpdatePicsM(Sims175 simsobj)//string opr, string data
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_parent_login_code", simsobj.sims_parent_login_code),
                            new SqlParameter("@sims_parent_mother_img", simsobj.sims_parent_mother_img),
                         
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            insert = true;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, insert);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    insert = false;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
        [Route("UpdatePicsCUD")]
        public HttpResponseMessage EmpGradeCUD(Sims175 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("pays_employee_grade",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_student_enroll_number", simsobj.sims_student_enroll_number),
                                 new SqlParameter("@sims_student_img", simsobj.sims_student_img),
                             

                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        //[Route("DStuImg")]
        //public HttpResponseMessage DStuImg(string objlist)//string opr, string data
        //{

        //    List<string> simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(objlist);

        //    Message message = new Message();
        //    try
        //    {
        //        if (simsobj != null)
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                SqlDataReader dr;
        //                db.Open();
        //                foreach (string deleteobj in simsobj)
        //                {
        //                    dr = db.ExecuteStoreProcedure("sims_student",
        //                       new List<SqlParameter>()
        //                 {
        //                   new SqlParameter("@opr", "D"),
        //                    new SqlParameter("@sims_student_img",deleteobj),

        //                 });
        //                    if (dr.RecordsAffected > 0)
        //                    {
        //                        dr.Close();
        //                        message.strMessage = "1";
        //                        message.systemMessage = string.Empty;
        //                        message.messageType = MessageType.Success;
        //                    }
        //                    else
        //                    {
        //                        dr.Close();
        //                    }
        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, message);
        //            }
        //        }
        //        else
        //        {
        //            message.strMessage = "Error In Parsing Information!!";
        //            message.messageType = MessageType.Error;
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        message.strMessage = "Error In Deleting image Information!!";
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, message);
        //}

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims175> grade_list = new List<Sims175>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@tbl_name", "[sims].[sims_grade]"),
                                new SqlParameter("@tbl_col_name1", "[sims_grade_code],[sims_grade_name_en]"),
                                new SqlParameter("@tbl_cond", "[sims_cur_code]='"+cur_code+"' and [sims_academic_year]=(Select  [sims_academic_year] from [sims].[sims_academic_year] where [sims_cur_code]='"+cur_code+"' and [sims_academic_year_status]='C')")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims175 simsobj = new Sims175();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims175> lstModules = new List<Sims175>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "R"),
                            new SqlParameter("@sims_cur_code", curCode)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims175 sequence = new Sims175();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
    }
}