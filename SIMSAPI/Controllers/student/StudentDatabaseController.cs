﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.FINNANCE;

using System.IO;
using System.Web.Configuration;
using System.Web;
namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/studentdatabase")]
    public class StudentDatabaseController : ApiController
    {
        static string root = "http://localhost/SIMSAPI/Content/sjs/Images/StudentImages/";
       
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        #region Student_Information_Update

        //Prospect
        [Route("getcountry")]
        public HttpResponseMessage getcountry()
        {
            List<Sims042> country = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_country_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "T")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_birth_country_code = dr["sims_country_name_en"].ToString();
                            obj.sims_student_birth_country = dr["sims_country_code"].ToString();
                            country.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, country);
            }


            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, country);

        }

        [Route("getHouse")]
        public HttpResponseMessage getHouse()
        {
            List<Sims042> house = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_house_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_house = dr["sims_house_name"].ToString();
                            obj.student_house = dr["sims_house_code"].ToString();
                            house.Add(obj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, house);
                }
            }

            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);

        }

        [Route("getLanguage")]
        public HttpResponseMessage getLanguage()
        {
            List<Sims042> Language = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_language_proc]",
                        new List<SqlParameter>() 
               {
               new SqlParameter("@opr", "S")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();
                            //read
                            obj.sims_student_main_language_r_code = dr["sims_language_code"].ToString();
                            obj.sims_student_main_language_r = dr["sims_language_name_en"].ToString();
                            //write
                            obj.sims_student_main_language_w_code = dr["sims_language_code"].ToString();
                            obj.sims_student_main_language_w = dr["sims_language_name_en"].ToString();
                            //gaurdian
                            obj.sims_student_main_language_s_code = dr["sims_language_code"].ToString();
                            obj.sims_student_main_language_s = dr["sims_language_name_en"].ToString();

                            obj.sims_student_main_language_code = dr["sims_language_code"].ToString();
                            obj.sims_student_main_language = dr["sims_language_name_en"].ToString();

                            obj.sims_student_main_language_m = dr["sims_language_name_en"].ToString();

                            Language.Add(obj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Language);

                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Language);


        }

        [Route("getSchoolName")]
        public HttpResponseMessage getSchoolName()
        {
            List<Sims042> school = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Mogra_License_details", new List<SqlParameter>() { new SqlParameter("@opr", "S") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();
                            //read
                            obj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                            obj.student_current_school_code = dr["lic_school_code"].ToString();

                            school.Add(obj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, school);

                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, school);


        }

        [Route("getgender")]
        public HttpResponseMessage getgender()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "G") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_gender = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_gender_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("get_legalCustudy_Preference")]
        public HttpResponseMessage get_legalCustudy_Preference()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "L") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_legal_custody = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_fee_payment_contact_pref = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_primary_contact_pref = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_primary_contact_code = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_parameter_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getVisaType")]
        public HttpResponseMessage getVisaType()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "T") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_visa_type = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_visa_type_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getLangProfieciency")]
        public HttpResponseMessage getLangProfieciency()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "P") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_main_language_r = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_main_language_w = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_main_language_s = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_main_language_r_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_student_main_language_w_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_student_main_language_s_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getSecretQuestion")]
        public HttpResponseMessage getSecretQuestion()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "Q") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_secret_question_code = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_secret_question = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr.Close();
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getNationality")]
        public HttpResponseMessage getNationality()
        {
            List<Sims042> Nationality = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_nationality_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_nationality_name = dr["sims_nationality_name_en"].ToString();
                            obj.sims_student_nationality_code = dr["sims_nationality_code"].ToString();
                            obj.sims_admission_mother_nationality1_name = dr["sims_nationality_name_en"].ToString();
                            obj.sims_admission_mother_nationality2_code = dr["sims_nationality_code"].ToString();
                            obj.sims_admission_guardian_nationality1_name = dr["sims_nationality_name_en"].ToString();
                            obj.sims_admission_guardian_nationality2_code = dr["sims_nationality_code"].ToString();
                            obj.sims_admission_father_nationality1_name = dr["sims_nationality_name_en"].ToString();
                            obj.sims_admission_father_nationality2_code = dr["sims_nationality_code"].ToString();

                            Nationality.Add(obj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Nationality);

                }
            }
            catch (Exception)
            {

                return Request.CreateResponse(HttpStatusCode.OK, Nationality);

            }

        }

        [Route("getEthinicity")]
        public HttpResponseMessage getEthinicity()
        {
            List<Sims042> ethnicity = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_ethnicity_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                            obj.sims_student_ethnicity = dr["sims_ethnicity_code"].ToString();
                            ethnicity.Add(obj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ethnicity);

                }

            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, ethnicity);

        }

        [Route("getAllReligion")]
        public HttpResponseMessage getAllReligion()
        {
            List<Sims042> religion = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_religion_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                            obj.sims_student_religion = dr["sims_religion_code"].ToString();
                            religion.Add(obj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, religion);

                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, religion);

        }

        [Route("getParent_Status")]
        public HttpResponseMessage getParent_Status()
        {
            List<Sims042> mod_list = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "R") });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_parent_status_code = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_parent_status = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr.Close();
                    }


                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "A") });


                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_academic_status = dr1["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_academic_status_code = dr1["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr1.Close();
                    }

                    SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "F") });



                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_financial_status = dr2["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_financial_status_code = dr2["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr2.Close();
                    }
                    SqlDataReader dr3 = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "RS") });


                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_ea_status = dr3["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_ea_status_code = dr3["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr3.Close();
                    }
                    SqlDataReader dr4 = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "TS") });


                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            Sims042 simsobj = new Sims042();
                            simsobj.sims_student_ea_transfer = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_student_ea_transfer_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }
                        dr4.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("get_Sims_Sibling_Details")]
        public HttpResponseMessage get_Sims_Sibling_Details(string enroll_no)
        {
            List<Sims042> sims_student_sibling = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "B"), 
                    new SqlParameter("@sims_student_enroll_number", enroll_no)

                });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.sims_sibling_enroll_number = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_sibling_name = dr["StudentName"].ToString();
                        simsobj.sims_sibling_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_sibling_academic_year = dr["sims_academic_year"].ToString();
                        simsobj.sims_sibling_grade_name_en = dr["sims_grade_name_en"].ToString();
                        simsobj.sims_sibling_section_name_en = dr["sims_section_name_en"].ToString();
                       // simsobj.sims_sibling_student_img_path = string.Format("{0}/Images/StudentImage/{1}", CommonStaticClass.imgpath, dr["sims_student_img"].ToString());
                        simsobj.sims_sibling_student_img_path = dr["sims_student_img"].ToString();
                        simsobj.sims_parent_number = dr["sims_sibling_parent_number"].ToString();
                        simsobj.sims_student_house = dr["sims_house_name"].ToString();
                        
                        sims_student_sibling.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_sibling);

            }
            catch (Exception e)
            {



            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_sibling);

        }
        //get Student Database
        [Route("get_Sims_Student_Details")]
        public HttpResponseMessage get_Sims_Student_Details(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"), 
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        //simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                        simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();
                        simsobj.sims_student_passport_full_name_ar = dr["sims_student_passport_full_name_ar"].ToString();

                       
                            simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                            simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                       
                            simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());
                        
                            simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();
                        
                            simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());
                      
                            simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();
                       
                        //simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_student_commence_date"].ToString()))
                            simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        else
                            simsobj.sims_student_commence_date = simsobj.sims_student_date;
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                       
                            simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());
                       
                            simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                        simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                        try
                        {
                            simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                            simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                            simsobj.sims_student_birth_place = dr["sims_student_birth_place"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_code"].ToString();
                            simsobj.comn_user_email = dr["comn_user_email"].ToString();
                            simsobj.sims_admission_family_monthly_income = dr["sims_admission_family_monthly_income"].ToString();

                        }
                        catch (Exception ex)
                        { }
                        sims_student_details.Add(simsobj);
                    }
             }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);
 }

        [Route("get_Sims_Student_DetailsForAELC")]
        public HttpResponseMessage get_Sims_Student_DetailsForAELC(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_aelc_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                    //   simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();

                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                        simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                        simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                        simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                        simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                        simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                        //simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());

                        simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                        simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                        simsobj.sims_student_attribute5 = dr["sims_student_attribute5"].ToString();

                        sims_student_details.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

        }
        //SISO,ABQIS and ASIS
        [Route("get_Sims_Student_DetailsForSISO")]
        public HttpResponseMessage get_Sims_Student_DetailsForSISO(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"), 
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        //simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                        simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();
                        simsobj.sims_student_passport_full_name_ar = dr["sims_student_passport_full_name_ar"].ToString();


                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                        simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                        simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                        simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                        simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                        simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                        //simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                        if (!string.IsNullOrEmpty(dr["sims_student_commence_date"].ToString()))
                            simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        else
                            simsobj.sims_student_commence_date = simsobj.sims_student_date;
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                        simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                        simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                        try
                        {
                            simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                            simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                            simsobj.sims_student_birth_place = dr["sims_student_birth_place"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_code"].ToString();

                        }
                        catch (Exception ex)
                        { }
                        sims_student_details.Add(simsobj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);
        }
        //SISO, ABQIS and ASIS 
         [Route("get_Sims_Sibling_DetailsForSISO")]
        public HttpResponseMessage get_Sims_Sibling_DetailsForSISO(string enroll_no)
        {
            List<Sims042> sims_student_sibling = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "B"), 
                    new SqlParameter("@sims_student_enroll_number", enroll_no)

                });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.sims_sibling_enroll_number = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_sibling_name = dr["StudentName"].ToString();
                        simsobj.sims_sibling_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_sibling_academic_year = dr["sims_academic_year"].ToString();
                        simsobj.sims_sibling_grade_name_en = dr["sims_grade_name_en"].ToString();
                        simsobj.sims_sibling_section_name_en = dr["sims_section_name_en"].ToString();
                       // simsobj.sims_sibling_student_img_path = string.Format("{0}/Images/StudentImage/{1}", CommonStaticClass.imgpath, dr["sims_student_img"].ToString());
                        simsobj.sims_sibling_student_img_path = dr["sims_student_img"].ToString();
                        simsobj.sims_parent_number = dr["sims_sibling_parent_number"].ToString();
                        simsobj.sims_student_house = dr["sims_house_name"].ToString();
                        
                        sims_student_sibling.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_sibling);

            }
            catch (Exception e)
            {



            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_sibling);

        }

        //leams
         [Route("get_Sims_Student_DetailsLeams")]
         public HttpResponseMessage get_Sims_Student_DetailsLeams(string enroll_no)
         {
             List<Sims042> sims_student_details = new List<Sims042>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"), 
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                     while (dr.Read())
                     {
                         Sims042 simsobj = new Sims042();
                         simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                         simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                         //simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                         simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                         simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                         simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                         simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                         simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                         simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                         simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                         simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                         simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                         simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                         simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                         simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();
                         simsobj.sims_student_passport_full_name_ar = dr["sims_student_passport_full_name_ar"].ToString();


                         simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                         simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                         simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                         simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                         simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                         simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                         simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                         simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                         simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                         simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                         simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                         simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                         simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                         simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                         simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                         simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                         simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                         simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                         simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                         simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                         simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                         simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                         simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                         simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                         simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                         simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                         simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                         simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                         simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                         simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                         simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                         simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                         simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                         simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                         simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                         simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                         simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                         simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                         simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                         simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                         //simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                         simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());
                         if (!string.IsNullOrEmpty(dr["sims_student_commence_date"].ToString()))
                             simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                         else
                             simsobj.sims_student_commence_date = simsobj.sims_student_date;
                         simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                         simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                         simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                         simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                         simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                         simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                         simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                         simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                         simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                         simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                         simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                         simsobj.sims_student_house = dr["sims_student_house"].ToString();
                         simsobj.sims_student_image = dr["sims_student_img"].ToString();
                         simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                         simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                         simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                         simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                         simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                         simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                         simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                         simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                         simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                         simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                         simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                         simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                         simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                         try
                         {
                             simsobj.sims_student_attribute5 = dr["sims_student_attribute5"].ToString();
                             simsobj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                             simsobj.sims_student_attribute1 = dr["sims_student_attribute1"].ToString();
                             simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                             simsobj.sims_student_special_education_status = dr["sims_student_special_education_status"].Equals("Y") ? true : false;
                             simsobj.sims_student_special_education_desc = dr["sims_student_special_education_desc"].ToString();


                             simsobj.sims_student_pan_no = dr["sims_student_pan_no"].ToString();
                             simsobj.sims_student_voter_id = dr["sims_student_voter_id"].ToString();
                             simsobj.sims_student_birth_place = dr["sims_student_birth_place"].ToString();
                         }
                         catch (Exception ex)
                         { }
                         try
                         {
                             simsobj.sims_student_admission_grade_code = dr["sims_student_admission_grade_code"].ToString();

                         }
                         catch (Exception ex)
                         { }
                         sims_student_details.Add(simsobj);
                     }
                 }
                 return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

             }
             catch (Exception e)
             {
             }
             return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);
         }



        //ASD
        [Route("get_Sims_Student_DetailsForASD")]
        public HttpResponseMessage get_Sims_Student_DetailsForASD(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_asd_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                        //simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();

                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        simsobj.sims_country_code = dr["sims_student_birth_country_code"].ToString();
                        simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                        simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                        simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                        simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                        simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                        simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                       // simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());

                        simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                        simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();

                        simsobj.sims_student_learning_therapy_status = dr["sims_student_learning_therapy_status"].Equals("1") ? true : false;
                        simsobj.sims_student_learning_therapy_desc = dr["sims_student_learning_therapy_desc"].ToString();

                        simsobj.sims_student_special_education_status = dr["sims_student_special_education_status"].Equals("1") ? true : false;
                        simsobj.sims_student_special_education_desc = dr["sims_student_special_education_desc"].ToString();

                        if (dr["sims_student_language_support_status"].ToString() == "1")
                        {
                            simsobj.language_support_status = true;
                        }
                        else
                        {
                            simsobj.language_support_status = false;
                        }
                        simsobj.language_support_desc = dr["sims_student_language_support_desc"].ToString();

                        simsobj.sims_student_falled_grade_status = dr["sims_student_falled_grade_status"].Equals("1") ? true : false;
                        simsobj.sims_student_falled_grade_desc = dr["sims_student_falled_grade_desc"].ToString();
                        
                        if (dr["sims_student_behaviour_status"].ToString() == "1")
                        {
                            simsobj.behaviour_status = true;
                        }
                        else
                        {
                            simsobj.behaviour_status = false;
                        }
                        simsobj.behaviour_desc = dr["sims_student_behaviour_desc"].ToString();

                        simsobj.sims_student_communication_status = dr["sims_student_communication_status"].Equals("1") ? true : false;
                        simsobj.sims_student_communication_desc = dr["sims_student_communication_desc"].ToString();
                        simsobj.sims_student_specialAct_status = dr["sims_student_specialAct_status"].Equals("1") ? true : false;
                        simsobj.sims_student_specialAct_desc = dr["sims_student_specialAct_desc"].ToString();                        
                        sims_student_details.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

        }

        /* Get Student Details For DPSMIS*/
        [Route("get_Sims_Student_DetailsForDPSMIS")]
        public HttpResponseMessage get_Sims_Student_DetailsForDPSMIS(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_dpsmis_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"),
                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                    new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                    new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                    new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                    new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                    new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                    new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                    new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                    new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                    new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                    new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                    new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                    new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                    new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                       // simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();

                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                        simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                        simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                        simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                        simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                        simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                       // simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());


                        simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                        simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                        simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();


                        simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();

                        simsobj.sims_student_home_appartment_number = dr["sims_student_home_appartment_number"].ToString();
                        simsobj.sims_student_home_building_number = dr["sims_student_home_building_number"].ToString();
                        simsobj.sims_student_home_street_number = dr["sims_student_home_street_number"].ToString();
                        simsobj.sims_student_home_area_number = dr["sims_student_home_area_number"].ToString();
                        simsobj.sims_student_home_landmark = dr["sims_student_home_landmark"].ToString();
                        simsobj.sims_student_home_country_code = dr["sims_student_home_country_code"].ToString();
                        simsobj.sims_student_home_state = dr["sims_student_home_state"].ToString();
                        simsobj.sims_student_home_city = dr["sims_student_home_city"].ToString();
                        simsobj.sims_student_home_phone = dr["sims_student_home_phone"].ToString();
                        simsobj.sims_student_home_po_box = dr["sims_student_home_po_box"].ToString();
                        simsobj.sims_student_home_summary_address = dr["sims_student_home_summary_address"].ToString();

                        //Prevoius School Details
                        simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                        simsobj.sims_previous_school_cur_name = dr["sims_previous_school_cur_name"].ToString();
                        simsobj.sims_previous_school_academic_year = dr["sims_previous_school_academic_year"].ToString();
                        simsobj.sims_previous_school_class = dr["sims_previous_school_class"].ToString();
                        simsobj.sims_previous_school_country = dr["sims_previous_school_country"].ToString();
                        simsobj.sims_previous_school_address = dr["sims_previous_school_address"].ToString();

                        sims_student_details.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

        }

        [Route("get_Sims_Student_DetailsForNISS")]
        public HttpResponseMessage get_Sims_Student_DetailsForNISS(string enroll_no)
        {
            List<Sims042> sims_student_details = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_niss_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", enroll_no)

                    });
                    while (dr.Read())
                    {
                        Sims042 simsobj = new Sims042();
                        simsobj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                        simsobj.sims_student_cur_code = dr["sims_cur_full_name_en"].ToString();
                        simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                        simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                        simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                        simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                        simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                        simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                        simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                        simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                        simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                        simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                        simsobj.sims_student_religion_code = dr["sims_religion_name_en"].ToString();
                        //simsobj.sims_student_passport_full_name_en = dr["sims_student_passport_full_name_en"].ToString();

                        simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                        simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        simsobj.sims_nationality_code = dr["sims_student_nationality_code"].ToString();
                        simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                        simsobj.sims_student_ethnicity_code = dr["sims_ethnicity_name_en"].ToString();
                        simsobj.sims_student_visa_number = dr["sims_student_visa_number"].ToString();

                        simsobj.sims_student_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_student_visa_issue_date"].ToString());

                        simsobj.sims_student_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_visa_expiry_date"].ToString());
                        simsobj.sims_student_visa_issuing_place = dr["sims_student_visa_issuing_place"].ToString();
                        simsobj.sims_student_visa_issuing_authority = dr["sims_student_visa_issuing_authority"].ToString();
                        simsobj.sims_student_visa_type = dr["sims_student_visa_type"].ToString();
                        simsobj.sims_student_national_id = dr["sims_student_national_id"].ToString();

                        simsobj.sims_student_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_issue_date"].ToString());

                        simsobj.sims_student_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_national_id_expiry_date"].ToString());
                        simsobj.sims_student_main_language_code = dr["sims_student_main_language_code"].ToString();
                        simsobj.sims_student_main_language_r = dr["sims_student_main_language_r"].ToString();
                        simsobj.sims_student_main_language_w = dr["sims_student_main_language_w"].ToString();
                        simsobj.sims_student_main_language_s = dr["sims_student_main_language_s"].ToString();
                        simsobj.sims_student_main_language_m = dr["sims_student_main_language_m"].ToString();
                        simsobj.sims_student_other_language = dr["sims_student_other_language"].ToString();
                        simsobj.sims_student_primary_contact_code = dr["sims_student_primary_contact_code"].ToString();
                        simsobj.sims_student_primary_contact_pref = dr["sims_student_primary_contact_pref"].ToString();
                        simsobj.sims_student_fee_payment_contact_pref = dr["sims_student_fee_payment_contact_pref"].ToString();
                        simsobj.sims_student_transport_status = dr["sims_student_transport_status"].Equals("A") ? true : false;
                        simsobj.sims_student_parent_status_code = dr["sims_student_parent_status_code"].ToString();
                        simsobj.sims_student_legal_custody = dr["sims_student_legal_custody"].ToString();
                        simsobj.sims_student_emergency_contact_name1 = dr["sims_student_emergency_contact_name1"].ToString();
                        simsobj.sims_student_emergency_contact_name2 = dr["sims_student_emergency_contact_name2"].ToString();
                        simsobj.sims_student_emergency_contact_number1 = dr["sims_student_emergency_contact_number1"].ToString();
                        simsobj.sims_student_emergency_contact_number2 = dr["sims_student_emergency_contact_number2"].ToString();
                        simsobj.sims_student_language_support_status = dr["sims_student_language_support_status"].Equals("1") ? true : false;
                        simsobj.sims_student_language_support_desc = dr["sims_student_language_support_desc"].ToString();
                        simsobj.sims_student_behaviour_status = dr["sims_student_behaviour_status"].Equals("1") ? true : false;
                        simsobj.sims_student_behaviour_desc = dr["sims_student_behaviour_desc"].ToString();
                        simsobj.sims_student_gifted_status = dr["sims_student_gifted_status"].Equals("1") ? true : false;
                        simsobj.sims_student_gifted_desc = dr["sims_student_gifted_desc"].ToString();
                        simsobj.sims_student_music_status = dr["sims_student_music_status"].Equals("1") ? true : false;
                        simsobj.sims_student_music_desc = dr["sims_student_music_desc"].ToString();
                        simsobj.sims_student_sports_status = dr["sims_student_sports_status"].Equals("1") ? true : false;
                        simsobj.sims_student_sports_desc = dr["sims_student_sports_desc"].ToString();

                        //simsobj.sims_student_date = dr["sims_student_date"].ToString().ToString();
                        simsobj.sims_student_date = db.UIDDMMYYYYformat(dr["sims_student_date"].ToString());

                        simsobj.sims_student_commence_date = db.UIDDMMYYYYformat(dr["sims_student_commence_date"].ToString());
                        simsobj.sims_student_remark = dr["sims_student_remark"].ToString();
                        simsobj.sims_student_login_id = dr["sims_student_login_id"].ToString();
                        simsobj.student_current_school_code = dr["student_current_school_code"].ToString();
                        simsobj.sims_student_current_school_code = dr["lic_school_name"].ToString();
                        simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                        simsobj.sims_student_employee_code = dr["sims_student_employee_code"].ToString();
                        simsobj.sims_student_last_login = db.UIDDMMYYYYformat(dr["sims_student_last_login"].ToString());
                        simsobj.sims_student_secret_question_code = dr["sims_student_secret_question_code"].ToString();
                        simsobj.sims_student_secret_answer = dr["sims_student_secret_answer"].ToString();
                        simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                        simsobj.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                        simsobj.sims_student_house = dr["sims_student_house"].ToString();
                        simsobj.sims_student_image = dr["sims_student_img"].ToString();
                        simsobj.sims_student_class_rank = dr["sims_student_class_rank"].ToString();
                        simsobj.sims_student_honour_roll = dr["sims_student_honour_roll"].ToString();
                        simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                        simsobj.sims_student_ea_registration_date = db.UIDDMMYYYYformat(dr["sims_student_ea_registration_date"].ToString());
                        simsobj.sims_student_ea_transfer = dr["sims_student_ea_transfer"].ToString();
                        simsobj.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                        simsobj.sims_student_passport_number = dr["sims_student_passport_number"].ToString();

                        simsobj.sims_student_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_student_passport_issue_date"].ToString());

                        simsobj.sims_student_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_student_passport_expiry_date"].ToString());
                        simsobj.sims_student_passport_issuing_authority = dr["sims_student_passport_issuing_authority"].ToString();
                        simsobj.sims_student_passport_issue_place = dr["sims_student_passport_issue_place"].ToString();
                        simsobj.sims_student_section_strength = dr["sims_section_stregth"].ToString();
                        simsobj.sims_student_prev_school = dr["sims_student_prev_school"].ToString();
                        simsobj.sims_student_category = dr["sims_student_category"].ToString();
                        sims_student_details.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student_details);

        }

        [Route("UpdateSims_Student_Details")]
        public HttpResponseMessage UpdateSims_Student_Details(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
               // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),
               // new SqlParameter("@sims_student_passport_full_name_ar", simsobj.sims_student_passport_full_name_ar),
               
                new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school)
                
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("UpdateSims_Student_DetailsSOK")]
        public HttpResponseMessage UpdateSims_Student_DetailsSOK(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),
                new SqlParameter("@sims_student_passport_full_name_ar", simsobj.sims_student_passport_full_name_ar),
               
                new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school)
                
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }



        [Route("UpdateSims_Student_DetailsLeams")]
        public HttpResponseMessage UpdateSims_Student_DetailsLeams(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
               // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),
               // new SqlParameter("@sims_student_passport_full_name_ar", simsobj.sims_student_passport_full_name_ar),
               
                new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
               
 
                new SqlParameter("@sims_student_special_education_desc", simsobj.sims_student_special_education_desc),
                new SqlParameter("@sims_student_special_education_status",simsobj.sims_student_special_education_status.Equals(true)?"Y":"N") ,
                new SqlParameter("@sims_student_attribute5", simsobj.sims_student_attribute5),
                new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3),
                new SqlParameter("@sims_student_attribute1", simsobj.sims_student_attribute1),
                new SqlParameter("@sims_student_attribute2", simsobj.sims_student_attribute2),

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("UpdateSims_Student_DetailsLeamsNew")]
        public HttpResponseMessage UpdateSims_Student_DetailsLeamsNew(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_DetailsLeamsNew", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),
                new SqlParameter("@sims_student_passport_full_name_ar", simsobj.sims_student_passport_full_name_ar),
               
                new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
               
 
                new SqlParameter("@sims_student_special_education_desc", simsobj.sims_student_special_education_desc),
                new SqlParameter("@sims_student_special_education_status",simsobj.sims_student_special_education_status.Equals(true)?"Y":"N") ,
                new SqlParameter("@sims_student_attribute5", simsobj.sims_student_attribute5),
                new SqlParameter("@sims_student_attribute3", simsobj.sims_student_attribute3)

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
       // Update for DMC
        [Route("UpdateSims_Student_DetailsForDMC")]
        public HttpResponseMessage UpdateSims_Student_DetailsForDMC(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_DetailsForDMC()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_DetailsForDMC", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                    new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                    new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                    new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                    new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                    new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                    new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                    new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                    new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                    new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                    new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                    new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                    new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                    new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                    new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                    new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                    new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                    new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                    new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                    new SqlParameter("@sims_student_admission_grade_code", simsobj.sims_student_admission_grade_code),

                    new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                    new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                    new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                    new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                    new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                    new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                    new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                   //new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                        new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                    new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                    new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                    new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                    new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                       new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                    new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                    new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                    new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                    new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                    new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                    new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                    new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                    new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                    new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                    new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                    new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                    new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                    new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                    new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                    new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                    new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                    new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                    new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                    new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                    new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                    new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                    new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                    new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                    new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                    new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                    new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                    new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                    new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                        new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                        new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                    new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                    new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                    new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                    new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                    new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                    new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                    new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                    new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                    new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                    new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                    new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                    new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                    new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                    new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                    new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                    new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                    new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                    new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                    new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                    new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                    new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                    new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                    new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
                    new SqlParameter("@sims_student_pan_no",simsobj.sims_student_pan_no),
                    new SqlParameter("@sims_student_voter_id",simsobj.sims_student_voter_id),
                    new SqlParameter("@sims_student_birth_place",simsobj.sims_student_birth_place),

                    //comn user email
                    new SqlParameter("@comn_user_email",simsobj.comn_user_email),
                     new SqlParameter("@sims_admission_fee_month_code",simsobj.sims_admission_family_monthly_income)
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
        [Route("UpdateSims_Student_DetailsForDVHS")]
        public HttpResponseMessage UpdateSims_Student_DetailsForDVHS(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_DetailsForDVHS()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_DetailsForDVHS", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                    new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                    new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                    new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                    new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                    new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                    new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                    new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                    new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                    new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                    new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                    new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                    new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                    new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                    new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                    new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                    new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                    new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                    new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                    new SqlParameter("@sims_student_admission_grade_code", simsobj.sims_student_admission_grade_code),

                    new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                    new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                    new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                    new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                    new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                    new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                    new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                   //new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                        new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                    new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                    new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                    new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                    new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                       new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                    new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                    new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                    new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                    new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                    new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                    new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                    new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                    new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                    new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                    new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                    new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                    new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                    new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                    new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                    new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                    new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                    new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                    new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                    new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                    new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                    new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                    new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                    new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                    new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                    new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                    new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                    new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                    new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                        new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                        new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                    new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                    new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                    new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                    new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                    new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                    new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                    new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                    new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                    new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                    new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                    new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                    new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                    new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                    new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                    new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                    new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                    new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                    new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                    new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                    new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                    new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                    new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                    new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
                    new SqlParameter("@sims_student_pan_no",simsobj.sims_student_pan_no),
                    new SqlParameter("@sims_student_voter_id",simsobj.sims_student_voter_id),
                    new SqlParameter("@sims_student_birth_place",simsobj.sims_student_birth_place),

                    //comn user email
                    new SqlParameter("@comn_user_email",simsobj.comn_user_email),
                   //  new SqlParameter("@sims_admission_family_monthly_income",simsobj.sims_admission_family_monthly_income)
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        /* Update Student For AELC */
        [Route("UpdateSims_Student_DetailsForAELC")]
        public HttpResponseMessage UpdateSims_Student_DetailsForAELC(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_aelc_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
              // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                    new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_attribute5", simsobj.sims_student_attribute5)

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        /* Update Student For ASD */
        [Route("UpdateSims_Student_DetailsForASD")]
        public HttpResponseMessage UpdateSims_Student_DetailsForASD(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_asd_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
               // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                    new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_code),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                //new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                //new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                //new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                //new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),

                new SqlParameter("@sims_student_learning_therapy_status", simsobj.sims_student_learning_therapy_status),
                new SqlParameter("@sims_student_learning_therapy_desc", simsobj.sims_student_learning_therapy_desc),
                new SqlParameter("@sims_student_special_education_status", simsobj.sims_student_special_education_status),
                new SqlParameter("@sims_student_special_education_desc", simsobj.sims_student_special_education_desc),
                new SqlParameter("@language_support_status", simsobj.language_support_status),
                new SqlParameter("@language_support_desc", simsobj.language_support_desc),
                new SqlParameter("@sims_student_falled_grade_status", simsobj.sims_student_falled_grade_status),
                new SqlParameter("@sims_student_falled_grade_desc", simsobj.sims_student_falled_grade_desc),
                new SqlParameter("@behaviour_status", simsobj.behaviour_status),
                new SqlParameter("@behaviour_desc", simsobj.behaviour_desc),
                new SqlParameter("@sims_student_communication_status", simsobj.sims_student_communication_status),
                new SqlParameter("@sims_student_communication_desc",simsobj.sims_student_communication_desc),
                new SqlParameter("@sims_student_specialAct_status", simsobj.sims_student_specialAct_status),
                new SqlParameter("@sims_student_specialAct_desc", simsobj.sims_student_specialAct_desc)
                
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #endregion
        /* Update Student For DPSMIS */

        [Route("UpdateSims_Student_DetailsForDPSMIS")]
        public HttpResponseMessage UpdateSims_Student_DetailsForDPSMIS(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_DetailsForDPSMIS()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_DetailsForDPSMIS", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_dpsmis_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                    new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                    new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                    new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                    new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                    new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                    new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                    new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                    new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                    new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                    new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                    new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                    new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                    new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                    new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                    new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                    new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                    new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                    new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                    new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                    new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                    new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                    new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                    new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                    new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                    new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                  // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                    new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                    new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                    new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                    new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                    new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                    new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                    new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                    new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                    new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                    new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                    new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                    new SqlParameter("@sims_student_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                    new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                    new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                    new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                    new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                    new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                    new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                    new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                    new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                    new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                    new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                    new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                    new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                    new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                    new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                    new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                    new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                    new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                    new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                    new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                    new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                    new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                    new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                    new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                    new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                    new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                    new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                    new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                    new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                    new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                    new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                    new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                    new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                    new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                    new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                    new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                    new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                    new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                    new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                    new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                    new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                    new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                    new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                    new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                    new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),

                    new SqlParameter("@sims_student_home_appartment_number", simsobj.sims_student_home_appartment_number),
                    new SqlParameter("@sims_student_home_building_number", simsobj.sims_student_home_building_number),
                    new SqlParameter("@sims_student_home_street_number", simsobj.sims_student_home_street_number),
                    new SqlParameter("@sims_student_home_area_number", simsobj.sims_student_home_area_number),
                    new SqlParameter("@sims_student_home_landmark", simsobj.sims_student_home_landmark),
                    new SqlParameter("@sims_student_home_country_code", simsobj.sims_student_home_country_code),
                    new SqlParameter("@sims_student_home_state", simsobj.sims_student_home_state),
                    new SqlParameter("@sims_student_home_city", simsobj.sims_student_home_city),
                    new SqlParameter("@sims_student_home_phone", simsobj.sims_student_home_phone),
                    new SqlParameter("@sims_student_home_po_box", simsobj.sims_student_home_po_box),
                    new SqlParameter("@sims_student_home_summary_address", simsobj.sims_student_home_summary_address),
                    // Previous School Details
                    new SqlParameter("@sopr", simsobj.sopr),
                    new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
                    new SqlParameter("@sims_previous_school_cur_name", simsobj.sims_previous_school_cur_name),
                    new SqlParameter("@sims_previous_school_academic_year", simsobj.sims_previous_school_academic_year),
                    new SqlParameter("@sims_previous_school_class", simsobj.sims_previous_school_class),
                    new SqlParameter("@sims_previous_school_country", simsobj.sims_previous_school_country),
                    new SqlParameter("@sims_previous_school_address", simsobj.sims_previous_school_address)


                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        /* Update Student For NISS */
        [Route("UpdateSims_Student_DetailsForNISS")]
        public HttpResponseMessage UpdateSims_Student_DetailsForNISS(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_Details", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_niss_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                    new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                    new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                    new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                    new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                    new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                    new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                    new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                    new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                    new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                    new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                    new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                    new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                    new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                    new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                    new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                    new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                    new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                    new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                    new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                    new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                    new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                    new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                    new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                    new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                    new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                  // new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),

                        new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                    new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                    new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                    new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                    new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                       new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                    new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                    new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                    new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                    new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                    new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                    new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                    new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                    new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                    new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                    new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                    new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                    new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                    new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                    new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                    new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                    new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                    new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                    new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                    new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                    new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                    new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                    new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                    new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                    new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                    new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                    new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                    new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                    new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                    new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                        new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                        new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                    new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                    new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                    new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                    new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                    new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                    new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                    new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                    new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                    new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                    new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                    new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                    new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                    new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                    new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                    new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                    new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                    new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                    new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                    new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                    new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                    new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                    new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                    new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                    new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school),
                    new SqlParameter("@sims_student_category", simsobj.sims_student_category)

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #region Parent_Information_update

        [Route("getSalutation")]
        public HttpResponseMessage getSalutation()
        {
            List<admissionClass> list = new List<admissionClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parameter_proc]", 
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "M") 
                        });

                    while (dr.Read())
                    {
                        admissionClass simsobj = new admissionClass();
                        simsobj.sims_admission_father_salutation_code = dr["sims_appl_form_field_value1"].ToString();
                        simsobj.sims_admission_mother_salutation_code = dr["sims_appl_form_field_value1"].ToString();
                        simsobj.sims_admission_guardian_salutation_code = dr["sims_appl_form_field_value1"].ToString();
                        list.Add(simsobj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            catch (Exception e)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getRelationship")]
        public HttpResponseMessage getRelationship()
        {
            List<admissionClass> list = new List<admissionClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parameter_proc", new List<SqlParameter>() { new SqlParameter("@opr", "RL") });

                    while (dr.Read())
                    {
                        admissionClass simsobj = new admissionClass();
                        simsobj.sims_parent_guardian_relationship_code = dr["sims_appl_form_field_value1"].ToString();

                        list.Add(simsobj);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }
        //get getmunicipalityZone for select Area Box
       [Route("getmunicipalityZone")]
        public HttpResponseMessage getmunicipalityZone()
        {
          
            List<municipalityZone> mod_list = new List<municipalityZone>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_municipality_zone_proc]", new List<SqlParameter>() { new SqlParameter("@opr", "L") });

                    while (dr.Read())
                    {
                        municipalityZone objNew = new municipalityZone();
                        objNew.sims_municipality_code = dr["sims_municipality_code"].ToString();
                        objNew.sims_municipality_name_en = dr["sims_municipality_name_en"].ToString();
                         
                        mod_list.Add(objNew);

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        //SISO ,  ABQIS and ASIS
        [Route("UpdateSims_Student_DetailsForSISO")]
       public HttpResponseMessage UpdateSims_Student_DetailsForSISO(Sims042 simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Student_DetailsForSISO()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Student_DetailsForSISO", simsobj));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_proc", new List<SqlParameter>() { new SqlParameter("@opr", "U"),

                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_appl_code2", CommonStaticClass.Sims042_appl_code2),
                new SqlParameter("@sims_gender_appl_form_field", CommonStaticClass.Sims042_appl_form_field),
                new SqlParameter("@sims_visa_type_appl_form_field", CommonStaticClass.Sims042_appl_form_field1),
                new SqlParameter("@sims_secret_question_appl_form_field", CommonStaticClass.Sims042_appl_form_field2),
                new SqlParameter("@sims_legal_custudy_appl_form_field", CommonStaticClass.Sims042_appl_form_field3),
                new SqlParameter("@sims_parent_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field4),
                new SqlParameter("@sims_academic_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field5),
                new SqlParameter("@sims_financial_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field6),
                new SqlParameter("@sims_register_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field7),
                new SqlParameter("@sims_transfer_status_appl_form_field", CommonStaticClass.Sims042_appl_form_field8),
                new SqlParameter("@sims_language_proficiency_appl_form_field", CommonStaticClass.Sims042_appl_form_field_1),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_student_cur_code", simsobj.sims_student_cur_code),
                new SqlParameter("@sims_student_passport_first_name_en", simsobj.sims_student_passport_first_name_en),
                new SqlParameter("@sims_student_passport_middle_name_en", simsobj.sims_student_passport_middle_name_en),
                new SqlParameter("@sims_student_passport_last_name_en", simsobj.sims_student_passport_last_name_en),
                new SqlParameter("@sims_student_family_name_en", simsobj.sims_student_family_name_en),
                new SqlParameter("@sims_student_nickname", simsobj.sims_student_nickname),
                new SqlParameter("@sims_student_passport_first_name_ot", simsobj.sims_student_passport_first_name_ot),
                new SqlParameter("@sims_student_passport_middle_name_ot", simsobj.sims_student_passport_middle_name_ot),
                new SqlParameter("@sims_student_passport_last_name_ot", simsobj.sims_student_passport_last_name_ot),
                new SqlParameter("@sims_student_family_name_ot", simsobj.sims_student_family_name_ot),
                new SqlParameter("@sims_student_gender", simsobj.sims_student_gender),
                new SqlParameter("@sims_student_religion_code", simsobj.sims_student_religion_code),
                new SqlParameter("@sims_student_passport_full_name_en", simsobj.sims_student_passport_full_name_en),
                new SqlParameter("@sims_student_passport_full_name_ar", simsobj.sims_student_passport_full_name_ar),
               
                new SqlParameter("@sims_student_dob", db.DBYYYYMMDDformat(simsobj.sims_student_dob)),
                new SqlParameter("@sims_student_birth_country_code", simsobj.sims_country_name_en),
                new SqlParameter("@sims_student_nationality_code", simsobj.sims_nationality_code),
                new SqlParameter("@sims_student_ethnicity_code", simsobj.sims_student_ethnicity_code),
                new SqlParameter("@sims_student_visa_number", simsobj.sims_student_visa_number),

                   new SqlParameter("@sims_student_visa_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_issue_date)),

                new SqlParameter("@sims_student_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_visa_expiry_date)),
                new SqlParameter("@sims_student_visa_issuing_place", simsobj.sims_student_visa_issuing_place),
                new SqlParameter("@sims_student_visa_issuing_authority", simsobj.sims_student_visa_issuing_authority),
                new SqlParameter("@sims_student_visa_type", simsobj.sims_student_visa_type),
                new SqlParameter("@sims_student_national_id", simsobj.sims_student_national_id),
                new SqlParameter("@sims_student_national_id_issue_date",  db.DBYYYYMMDDformat(simsobj.sims_student_national_id_issue_date)),


                new SqlParameter("@sims_student_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_national_id_expiry_date)),
                new SqlParameter("@sims_student_main_language_code", simsobj.sims_student_main_language_code),

                new SqlParameter("@sims_student_main_language_r", simsobj.sims_student_main_language_r),
                new SqlParameter("@sims_student_main_language_w", simsobj.sims_student_main_language_w),
                new SqlParameter("@sims_student_main_language_s", simsobj.sims_student_main_language_s),
                new SqlParameter("@sims_student_main_language_m", simsobj.sims_student_main_language_m),
                new SqlParameter("@sims_student_other_language", simsobj.sims_student_other_language),
                new SqlParameter("@sims_student_primary_contact_code", simsobj.sims_student_primary_contact_code),
                new SqlParameter("@sims_student_primary_contact_pref", simsobj.sims_student_primary_contact_pref),
                new SqlParameter("@sims_student_fee_payment_contact_pref", simsobj.sims_student_fee_payment_contact_pref),
                new SqlParameter("@sims_student_transport_status",simsobj.sims_student_transport_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_student_parent_status_code", simsobj.sims_student_parent_status_code),
                new SqlParameter("@sims_student_legal_custody", simsobj.sims_student_legal_custody),
                new SqlParameter("@sims_student_emergency_contact_name1", simsobj.sims_student_emergency_contact_name1),
                new SqlParameter("@sims_student_emergency_contact_name2", simsobj.sims_student_emergency_contact_name2),
                new SqlParameter("@sims_student_emergency_contact_number1", simsobj.sims_student_emergency_contact_number1),
                new SqlParameter("@sims_student_emergency_contact_number2", simsobj.sims_student_emergency_contact_number2),
                new SqlParameter("@sims_student_language_support_status", simsobj.sims_student_language_support_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_language_support_desc", simsobj.sims_student_language_support_desc),
                new SqlParameter("@sims_student_behaviour_status", simsobj.sims_student_behaviour_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_behaviour_desc", simsobj.sims_student_behaviour_desc),
                new SqlParameter("@sims_student_gifted_status", simsobj.sims_student_gifted_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_gifted_desc", simsobj.sims_student_gifted_desc),
                new SqlParameter("@sims_student_music_status", simsobj.sims_student_music_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_music_desc", simsobj.sims_student_music_desc),
                new SqlParameter("@sims_student_sports_status", simsobj.sims_student_sports_status.Equals(true)?1:0),
                new SqlParameter("@sims_student_sports_desc", simsobj.sims_student_sports_desc),
                    new SqlParameter("@sims_student_date", db.DBYYYYMMDDformat(simsobj.sims_student_date)),

                    new SqlParameter("@sims_student_commence_date", db.DBYYYYMMDDformat(simsobj.sims_student_commence_date)),
                new SqlParameter("@sims_student_remark", simsobj.sims_student_remark),
                new SqlParameter("@sims_student_login_id", simsobj.sims_student_login_id),
                new SqlParameter("@sims_student_current_school_name", simsobj.sims_student_current_school_code),
                new SqlParameter("@sims_student_employee_comp_code", simsobj.sims_student_employee_comp_code),
                new SqlParameter("@sims_student_employee_code", simsobj.sims_student_employee_code),

                new SqlParameter("@sims_student_last_login", db.DBYYYYMMDDformat(simsobj.sims_student_last_login)),
                new SqlParameter("@sims_student_secret_question_code", simsobj.sims_student_secret_question_code),
                new SqlParameter("@sims_student_secret_answer", simsobj.sims_student_secret_answer),
                new SqlParameter("@sims_student_academic_status", simsobj.sims_student_academic_status),
                new SqlParameter("@sims_student_financial_status", simsobj.sims_student_financial_status),
                new SqlParameter("@sims_student_house", simsobj.sims_student_house),
                new SqlParameter("@sims_student_img", simsobj.sims_student_image),
                new SqlParameter("@sims_student_class_rank", simsobj.sims_student_class_rank),
                new SqlParameter("@sims_student_honour_roll", simsobj.sims_student_honour_roll),
                new SqlParameter("@sims_student_ea_number", simsobj.sims_student_ea_number),
                new SqlParameter("@sims_student_ea_registration_date", db.DBYYYYMMDDformat(simsobj.sims_student_ea_registration_date)),
                new SqlParameter("@sims_student_ea_transfer", simsobj.sims_student_ea_transfer),
                new SqlParameter("@sims_student_ea_status", simsobj.sims_student_ea_status),
                new SqlParameter("@sims_student_passport_number", simsobj.sims_student_passport_number),
                new SqlParameter("@sims_student_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_issue_date)),
                new SqlParameter("@sims_student_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_student_passport_expiry_date)),
                new SqlParameter("@sims_student_passport_issuing_authority", simsobj.sims_student_passport_issuing_authority),
                new SqlParameter("@sims_student_passport_issue_place", simsobj.sims_student_passport_issue_place),
                new SqlParameter("@sims_student_prev_school", simsobj.sims_student_prev_school)
                
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated Sucessfully";
                        inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
        // SISO ABQIS and ASIS
        [Route("UpdateSims_Parent_DetailsForSISO")]
        public HttpResponseMessage UpdateSims_Parent_DetailsForSISO(admissionClass simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Parent_DetailsForSISO()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Parent_DetailsForSISO", simsobj));

            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_proc", new List<SqlParameter>() { 

                new SqlParameter("@opr", 'E'),
                new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                new SqlParameter("@sims_parent_father_img", simsobj.sims_admisison_father_image),
                new SqlParameter("@sims_parent_mother_img", simsobj.sims_admisison_mother_image),
                new SqlParameter("@sims_parent_guardian_img", simsobj.sims_admisison_guardian_image),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1),
                new SqlParameter("@sims_parent_login_code", simsobj.sims_admission_login_name),
                new SqlParameter("@sims_parent_father_salutation_code", simsobj.sims_admission_father_salutation_code),
                new SqlParameter("@sims_parent_father_first_name", simsobj.sims_admission_father_first_name),
                new SqlParameter("@sims_parent_father_middle_name", simsobj.sims_admission_father_middle_name),
                new SqlParameter("@sims_parent_father_last_name", simsobj.sims_admission_father_last_name),
                new SqlParameter("@sims_parent_father_family_name", simsobj.sims_admission_family_name_en),
                new SqlParameter("@sims_parent_father_name_ot", simsobj.sims_admission_father_name_ot),
                new SqlParameter("@sims_parent_father_nationality1_code", simsobj.sims_admission_father_nationality1_code),
                new SqlParameter("@sims_parent_father_nationality2_code", simsobj.sims_admission_father_nationality2_code),
                new SqlParameter("@sims_parent_father_appartment_number", simsobj.sims_admission_father_appartment_number),
                new SqlParameter("@sims_parent_father_building_number", simsobj.sims_admission_father_building_number),
                new SqlParameter("@sims_parent_father_street_number", simsobj.sims_admission_father_street_number),
                new SqlParameter("@sims_parent_father_area_number", simsobj.sims_admission_father_area_number),
                new SqlParameter("@sims_parent_father_summary_address", simsobj.sims_admission_father_summary_address),
                new SqlParameter("@sims_parent_father_city", simsobj.sims_admission_father_city),
                new SqlParameter("@sims_parent_father_state", simsobj.sims_admission_father_state),
                new SqlParameter("@sims_parent_father_country_code", simsobj.sims_admission_father_country_code),
                new SqlParameter("@sims_parent_father_phone", simsobj.sims_admission_father_phone),
                new SqlParameter("@sims_parent_father_mobile", simsobj.sims_admission_father_mobile),
                new SqlParameter("@sims_parent_father_email", simsobj.sims_admission_father_email),
                new SqlParameter("@sims_parent_father_fax", simsobj.sims_admission_father_fax),
                new SqlParameter("@sims_parent_father_po_box", simsobj.sims_admission_father_po_box),
                new SqlParameter("@sims_parent_father_occupation", simsobj.sims_admission_father_occupation),
                new SqlParameter("@sims_parent_father_occupation_local_language", simsobj.sims_parent_father_occupation_local_language),
                new SqlParameter("@sims_parent_father_occupation_location_local_language", simsobj.sims_parent_father_occupation_location_local_language),
                new SqlParameter("@sims_parent_father_company", simsobj.sims_admission_father_company),
                new SqlParameter("@sims_parent_father_passport_number", simsobj.sims_admission_father_passport_number),
                new SqlParameter("@sims_parent_mother_salutation_code", simsobj.sims_admission_mother_salutation_code),
                new SqlParameter("@sims_parent_mother_first_name", simsobj.sims_admission_mother_first_name),
                new SqlParameter("@sims_parent_mother_middle_name", simsobj.sims_admission_mother_middle_name),
                new SqlParameter("@sims_parent_mother_last_name", simsobj.sims_admission_mother_last_name),
                new SqlParameter("@sims_parent_mother_family_name", simsobj.sims_admission_mother_family_name_en),
                new SqlParameter("@sims_parent_mother_name_ot", simsobj.sims_admission_mother_name_ot),
                new SqlParameter("@sims_parent_mother_nationality1_code", simsobj.sims_admission_mother_nationality1_code),
                new SqlParameter("@sims_parent_mother_nationality2_code", simsobj.sims_admission_mother_nationality2_code),
                new SqlParameter("@sims_parent_mother_appartment_number", simsobj.sims_admission_mother_appartment_number),
                new SqlParameter("@sims_parent_mother_building_number", simsobj.sims_admission_mother_building_number),
                new SqlParameter("@sims_parent_mother_street_number", simsobj.sims_admission_mother_street_number),
                new SqlParameter("@sims_parent_mother_area_number", simsobj.sims_admission_mother_area_number),
                new SqlParameter("@sims_parent_mother_summary_address", simsobj.sims_admission_mother_summary_address),
                new SqlParameter("@sims_parent_mother_city", simsobj.sims_admission_mother_city),
                new SqlParameter("@sims_parent_mother_state", simsobj.sims_admission_mother_state),
                new SqlParameter("@sims_parent_mother_country_code", simsobj.sims_admission_mother_country_code),
                new SqlParameter("@sims_parent_mother_phone", simsobj.sims_admission_mother_phone),
                new SqlParameter("@sims_parent_mother_mobile", simsobj.sims_admission_mother_mobile),
                new SqlParameter("@sims_parent_mother_email", simsobj.sims_admission_mother_email),
                new SqlParameter("@sims_parent_mother_fax", simsobj.sims_admission_mother_fax),
                new SqlParameter("@sims_parent_mother_po_box", simsobj.sims_admission_mother_po_box),
                new SqlParameter("@sims_parent_mother_occupation", simsobj.sims_admission_mother_occupation),
                new SqlParameter("@sims_parent_mother_occupation_local_language", simsobj.sims_parent_mother_occupation_local_language),
                new SqlParameter("@sims_parent_mother_company", simsobj.sims_admission_mother_company),
                new SqlParameter("@sims_parent_mother_passport_number", simsobj.sims_admission_mother_passport_number),
                new SqlParameter("@sims_parent_guardian_salutation_code", simsobj.sims_admission_guardian_salutation_code),
                new SqlParameter("@sims_parent_guardian_first_name", simsobj.sims_admission_guardian_first_name),
                new SqlParameter("@sims_parent_guardian_middle_name", simsobj.sims_admission_guardian_middle_name),
                new SqlParameter("@sims_parent_guardian_last_name", simsobj.sims_admission_guardian_last_name),
                new SqlParameter("@sims_parent_guardian_family_name", simsobj.sims_admission_guardian_family_name_en),
                new SqlParameter("@sims_parent_guardian_name_ot", simsobj.sims_admission_guardian_name_ot),
                new SqlParameter("@sims_parent_guardian_nationality1_code", simsobj.sims_admission_guardian_nationality1_code),
                new SqlParameter("@sims_parent_guardian_nationality2_code", simsobj.sims_admission_guardian_nationality2_code),
                new SqlParameter("@sims_parent_guardian_appartment_number", simsobj.sims_admission_guardian_appartment_number),
                new SqlParameter("@sims_parent_guardian_building_number", simsobj.sims_admission_guardian_building_number),
                new SqlParameter("@sims_parent_guardian_street_number", simsobj.sims_admission_guardian_street_number),
                new SqlParameter("@sims_parent_guardian_area_number", simsobj.sims_admission_guardian_area_number),
                new SqlParameter("@sims_parent_guardian_summary_address", simsobj.sims_admission_guardian_summary_address),
                new SqlParameter("@sims_parent_guardian_city", simsobj.sims_admission_guardian_city),
                new SqlParameter("@sims_parent_guardian_state", simsobj.sims_admission_guardian_state),
                new SqlParameter("@sims_parent_guardian_country_code", simsobj.sims_admission_guardian_country_code),
                new SqlParameter("@sims_parent_guardian_phone", simsobj.sims_admission_guardian_phone),
                new SqlParameter("@sims_parent_guardian_mobile", simsobj.sims_admission_guardian_mobile),
                new SqlParameter("@sims_parent_guardian_email", simsobj.sims_admission_guardian_email),
                new SqlParameter("@sims_parent_guardian_fax", simsobj.sims_admission_guardian_fax),
                new SqlParameter("@sims_parent_guardian_po_box", simsobj.sims_admission_guardian_po_box),
                new SqlParameter("@sims_parent_guardian_occupation", simsobj.sims_admission_guardian_occupation),
                new SqlParameter("@sims_parent_guardian_occupation_local_language", simsobj.sims_parent_guardian_occupation_local_language),
                new SqlParameter("@sims_parent_guardian_occupation_location_local_language", simsobj.sims_parent_guardian_occupation_location_local_language),
                new SqlParameter("@sims_parent_guardian_company", simsobj.sims_admission_guardian_company),
                new SqlParameter("@sims_parent_guardian_relationship_code", simsobj.sims_parent_guardian_relationship_code),
                new SqlParameter("@sims_parent_guardian_passport_number", simsobj.sims_admission_guardian_passport_number),
                new SqlParameter("@sims_parent_is_employment_status", simsobj.sims_parent_is_employment_status.Equals(true)?"1":"0"),
                new SqlParameter("@sims_parent_is_employement_comp_code", simsobj.sims_parent_is_employement_comp_code),
                new SqlParameter("@sims_parent_is_employment_number", simsobj.sims_parent_is_employment_number),
                new SqlParameter("@sims_parent_father_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_issue_date)),
                new SqlParameter("@sims_parent_father_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_expiry_date)),
                new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                new SqlParameter("@sims_parent_father_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_issue_date)),
                new SqlParameter("@sims_parent_father_national_id__expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_expiry_date)),
                new SqlParameter("@sims_parent_mother_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_issue_date)),
                new SqlParameter("@sims_parent_mother_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_expiry_date)),
                new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                new SqlParameter("@sims_parent_mother_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_issue_date)),
                new SqlParameter("@sims_parent_mother_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_expiry_date)),
                new SqlParameter("@sims_parent_guardian_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_issue_date)),
                new SqlParameter("@sims_parent_guardian_passport_expiry_date2", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_expiry_date)),
                new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                new SqlParameter("@sims_parent_guardian_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_national_id_issue_date)),
                new SqlParameter("@sims_parent_guardian_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.@sims_parent_guardian_national_id_expiry_date))
                    });


                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated successfully";
                        inserted = true;
                    }
                    else
                    {

                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }


            catch (Exception e)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

       // SISO ABQIS and ASIS
        #endregion
        #region Sims098(medical_student_details)
        [Route("update_Student_Medical_DetailsForSISO")]
        public HttpResponseMessage update_Student_Medical_DetailsForSISO(Sims098 simsobj)
        {
            Message message = new Message();
            bool inserted = false;
            List<Sims098> sims_student = new List<Sims098>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_medical_student_proc", new List<SqlParameter>() { 
                
                new SqlParameter("@opr", 'U'),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_blood_group_appl_form_field", CommonStaticClass.Sims042_appl_form_field9),
                new SqlParameter("@sims_health_card_number", simsobj.health_card_number),
                new SqlParameter("@sims_health_card_issue_date", db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                new SqlParameter("@sims_health_card_expiry_date", db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),
                new SqlParameter("@sims_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                new SqlParameter("@sims_blood_group_code", simsobj.student_blood_group_code),
                new SqlParameter("@sims_health_restriction_status", simsobj.student_health_restriction_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_restriction_desc", simsobj.sims_health_restriction_desc),
                new SqlParameter("@sims_disability_status", simsobj.sims_disability_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_disability_desc", simsobj.sims_disability_desc),
                new SqlParameter("@sims_medication_status", simsobj.sims_medication_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_medication_desc", simsobj.sims_medication_desc),
                new SqlParameter("@sims_health_other_status",simsobj.sims_health_other_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_other_desc", simsobj.sims_health_other_desc),
                new SqlParameter("@sims_health_hearing_status", simsobj.sims_health_hearing_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_hearing_desc", simsobj.sims_health_hearing_desc),
                new SqlParameter("@sims_health_vision_status", simsobj.sims_health_vision_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_vision_desc", simsobj.sims_health_vision_desc),
                new SqlParameter("@sims_regular_doctor_name", simsobj.regular_doctor_name),
                new SqlParameter("@sims_regular_doctor_phone", simsobj.regular_doctor_phone),
                new SqlParameter("@sims_regular_hospital_name", simsobj.regular_hospital_name),
                new SqlParameter("@sims_regular_hospital_phone", simsobj.regular_hospital_phone),
                new SqlParameter("@sims_height", simsobj.student_height),
                new SqlParameter("@sims_wieght", simsobj.student_wieght),
                new SqlParameter("@sims_teeth", simsobj.student_teeth),
                new SqlParameter("@min_val", ""),
                new SqlParameter("@max_val", ""),

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Medical Information Updated Sucessfully";
                      inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Medical Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #endregion

        // SISO , ABQIS and ASIS
        [Route("getParentDetailsForSISO")]
        public HttpResponseMessage getParentDetailsForSISO(string enroll_no)
        {

            string parent_number = "";

            List<admissionClass> parent_details = new List<admissionClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_sibling_proc", new List<SqlParameter>() { 
                  new SqlParameter("@opr", "A"),
                  new SqlParameter("@sims_sibling_student_enroll_number", enroll_no),
                    });
                    if (dr1.HasRows)
                    {

                        while (dr1.Read())
                        {
                            parent_number = dr1["sims_sibling_parent_number"].ToString();

                        }
                    }
                    dr1.Dispose();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_proc", new List<SqlParameter>() { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_parent_number", parent_number),
                new SqlParameter("@sims_parent_father_img", ""),
                new SqlParameter("@sims_parent_mother_img", ""),
                new SqlParameter("@sims_parent_guardian_img", ""),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1)

                    });
                    while (dr.Read())
                    {
                        admissionClass simsobj = new admissionClass();
                        simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                        simsobj.sims_admission_login_name = dr["sims_parent_login_code"].ToString();
                        simsobj.sims_admission_father_salutation_code = dr["sims_parent_father_salutation_code"].ToString();
                        simsobj.sims_admission_father_first_name = dr["sims_parent_father_first_name"].ToString();
                        simsobj.sims_admission_father_middle_name = dr["sims_parent_father_middle_name"].ToString();
                        simsobj.sims_admission_father_last_name = dr["sims_parent_father_last_name"].ToString();
                        simsobj.sims_admission_family_name_en = dr["sims_parent_father_family_name"].ToString();
                        simsobj.sims_admission_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                        simsobj.sims_admission_father_nationality1_code = dr["sims_parent_father_nationality1_code"].ToString();
                        simsobj.sims_admission_father_nationality2_code = dr["sims_parent_father_nationality2_code"].ToString();
                        simsobj.sims_admission_father_appartment_number = dr["sims_parent_father_appartment_number"].ToString();
                        simsobj.sims_admission_father_building_number = dr["sims_parent_father_building_number"].ToString();
                        simsobj.sims_admission_father_street_number = dr["sims_parent_father_street_number"].ToString();
                        simsobj.sims_admission_father_area_number = dr["sims_parent_father_area_number"].ToString();
                        simsobj.sims_admission_father_city = dr["sims_parent_father_city"].ToString();
                        simsobj.sims_admission_father_state = dr["sims_parent_father_state"].ToString();
                        simsobj.sims_admission_father_country_code = dr["sims_parent_father_country_code"].ToString();
                        simsobj.sims_admission_father_po_box = dr["sims_parent_father_po_box"].ToString();
                        simsobj.sims_admission_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                        simsobj.sims_admission_father_phone = dr["sims_parent_father_phone"].ToString();
                        simsobj.sims_admission_father_mobile = dr["sims_parent_father_mobile"].ToString();
                        simsobj.sims_admission_father_fax = dr["sims_parent_father_fax"].ToString();
                        simsobj.sims_admission_father_email = dr["sims_parent_father_email"].ToString();
                        simsobj.sims_admission_father_occupation = dr["sims_parent_father_occupation"].ToString();
                        simsobj.sims_parent_father_occupation_local_language = dr["sims_parent_father_occupation_local_language"].ToString();
                        simsobj.sims_parent_father_occupation_location_local_language = dr["sims_parent_father_occupation_location_local_language"].ToString();
                        simsobj.sims_admission_father_company = dr["sims_parent_father_company"].ToString();
                        simsobj.sims_admission_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                        simsobj.sims_admisison_father_image = dr["sims_parent_father_img"].ToString();
                        //mother
                        simsobj.sims_admission_mother_salutation_code = dr["sims_parent_mother_salutation_code"].ToString();
                        simsobj.sims_admission_mother_first_name = dr["sims_parent_mother_first_name"].ToString();
                        simsobj.sims_admission_mother_middle_name = dr["sims_parent_mother_middle_name"].ToString();
                        simsobj.sims_admission_mother_last_name = dr["sims_parent_mother_last_name"].ToString();
                        simsobj.sims_admission_mother_family_name_en = dr["sims_parent_mother_family_name"].ToString();
                        simsobj.sims_admission_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                        simsobj.sims_admission_mother_nationality1_code = dr["sims_parent_mother_nationality1_code"].ToString();
                        simsobj.sims_admission_mother_nationality2_code = dr["sims_parent_mother_nationality2_code"].ToString();
                        simsobj.sims_admission_mother_appartment_number = dr["sims_parent_mother_appartment_number"].ToString();
                        simsobj.sims_admission_mother_building_number = dr["sims_parent_mother_building_number"].ToString();
                        simsobj.sims_admission_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                        simsobj.sims_admission_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                        simsobj.sims_admission_mother_city = dr["sims_parent_mother_city"].ToString();
                        simsobj.sims_admission_mother_state = dr["sims_parent_mother_state"].ToString();
                        simsobj.sims_admission_mother_country_code = dr["sims_parent_mother_country_code"].ToString();
                        simsobj.sims_admission_mother_po_box = dr["sims_parent_mother_po_box"].ToString();
                        simsobj.sims_admission_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                        simsobj.sims_admission_mother_phone = dr["sims_parent_mother_phone"].ToString();
                        simsobj.sims_admission_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                        simsobj.sims_admission_mother_fax = dr["sims_parent_mother_fax"].ToString();
                        simsobj.sims_admission_mother_email = dr["sims_parent_mother_email"].ToString();
                        simsobj.sims_admission_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                        simsobj.sims_parent_mother_occupation_local_language = dr["sims_parent_mother_occupation_local_language"].ToString();
                        simsobj.sims_admission_mother_company = dr["sims_parent_mother_company"].ToString();
                        simsobj.sims_admission_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                        simsobj.sims_admisison_mother_image = dr["sims_parent_mother_img"].ToString();
                        //guardian
                        simsobj.sims_admission_guardian_salutation_code = dr["sims_parent_guardian_salutation_code"].ToString();
                        simsobj.sims_admission_guardian_first_name = dr["sims_parent_guardian_first_name"].ToString();
                        simsobj.sims_admission_guardian_middle_name = dr["sims_parent_guardian_middle_name"].ToString();
                        simsobj.sims_admission_guardian_last_name = dr["sims_parent_guardian_last_name"].ToString();
                        simsobj.sims_admission_guardian_family_name_en = dr["sims_parent_guardian_family_name"].ToString();
                        simsobj.sims_admission_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                        simsobj.sims_admission_guardian_nationality1_code = dr["sims_parent_guardian_nationality1_code"].ToString();
                        simsobj.sims_admission_guardian_nationality2_code = dr["sims_parent_guardian_nationality2_code"].ToString();
                        simsobj.sims_admission_guardian_appartment_number = dr["sims_parent_guardian_appartment_number"].ToString();
                        simsobj.sims_admission_guardian_building_number = dr["sims_parent_guardian_building_number"].ToString();
                        simsobj.sims_admission_guardian_street_number = dr["sims_parent_guardian_street_number"].ToString();
                        simsobj.sims_admission_guardian_area_number = dr["sims_parent_guardian_area_number"].ToString();
                        simsobj.sims_admission_guardian_city = dr["sims_parent_guardian_city"].ToString();
                        simsobj.sims_admission_guardian_state = dr["sims_parent_guardian_state"].ToString();
                        simsobj.sims_admission_guardian_country_code = dr["sims_parent_guardian_country_code"].ToString();
                        simsobj.sims_admission_guardian_po_box = dr["sims_parent_guardian_po_box"].ToString();
                        simsobj.sims_admission_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                        simsobj.sims_admission_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                        simsobj.sims_admission_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                        simsobj.sims_admission_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                        simsobj.sims_admission_guardian_email = dr["sims_parent_guardian_email"].ToString();
                        simsobj.sims_admission_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                        simsobj.sims_parent_guardian_occupation_local_language = dr["sims_parent_guardian_occupation_local_language"].ToString();
                        simsobj.sims_parent_guardian_occupation_location_local_language = dr["sims_parent_guardian_occupation_location_local_language"].ToString();
                        simsobj.sims_admission_guardian_company = dr["sims_parent_guardian_company"].ToString();
                        simsobj.sims_parent_guardian_relationship_code = dr["sims_parent_guardian_relationship_code"].ToString();
                        simsobj.sims_admission_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                        simsobj.sims_admisison_guardian_image = dr["sims_parent_guardian_img"].ToString();
                        simsobj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].Equals("1") ? true : false;
                        simsobj.sims_parent_is_employement_comp_code = dr["sims_parent_is_employement_comp_code"].ToString();
                        simsobj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                        simsobj.sims_parent_father_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_father_passport_issue_date"].ToString());
                        simsobj.sims_parent_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_passport_expiry_date"].ToString());
                        simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                        simsobj.sims_parent_father_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_father_national_id_issue_date"].ToString());
                        simsobj.sims_parent_father_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_national_id__expiry_date"].ToString());
                        simsobj.sims_parent_mother_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_issue_date"].ToString());
                        simsobj.sims_parent_mother_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_expiry_date"].ToString());
                        simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                        simsobj.sims_parent_mother_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_issue_date"].ToString());
                        simsobj.sims_parent_mother_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_expiry_date"].ToString());
                        simsobj.sims_parent_guardian_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_issue_date"].ToString());
                        simsobj.sims_parent_guardian_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_expiry_date2"].ToString());
                        simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                        simsobj.sims_parent_guardian_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_issue_date"].ToString());
                        simsobj.sims_parent_guardian_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_expiry_date"].ToString());

                        parent_details.Add(simsobj);

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, parent_details);

            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, parent_details);

            }

        }


        [Route("getStateName")]
        public HttpResponseMessage getStateName(string country_name, bool father, bool mother)
        {
            List<admissionClass> mod_list = new List<admissionClass>();

            string country_code = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_country_proc", new List<SqlParameter>() { new SqlParameter("@opr", "Z") ,

                new SqlParameter("@sims_country_name_en", country_name)
               });



                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            country_code = dr["sims_country_code"].ToString();
                        }
                    }
                    dr.Close();


                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_state_proc", new List<SqlParameter>() { new SqlParameter("@opr", "B") ,
                new SqlParameter("@country_code" ,country_code)
                });

                    if (mother)
                    {
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                admissionClass simsobj = new admissionClass();
                                simsobj.sims_admission_mother_state = dr1["sims_state_name_en"].ToString();
                                simsobj.sims_admission_mother_state_code = dr1["sims_state_code"].ToString();
                                mod_list.Add(simsobj);

                            }
                        }
                    }
                    else if (father)
                    {

                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                admissionClass simsobj = new admissionClass();
                                simsobj.sims_admission_father_state = dr1["sims_state_name_en"].ToString();
                                simsobj.sims_admission_father_state_code = dr1["sims_state_code"].ToString();
                                mod_list.Add(simsobj);
                            }
                        }
                    }

                    else
                    {
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                admissionClass simsobj = new admissionClass();
                                simsobj.sims_admission_guardian_state = dr1["sims_state_name_en"].ToString();
                                simsobj.sims_admission_guardian_state_code = dr1["sims_state_code"].ToString();
                                mod_list.Add(simsobj);
                            }
                        }

                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("getCountryName")]
        public HttpResponseMessage getCountryName()
        {
            List<admissionClass> mod_list = new List<admissionClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_country_proc", new List<SqlParameter>() { new SqlParameter("@opr", "T") });

                    while (dr.Read())
                    {
                        admissionClass objNew = new admissionClass();
                        objNew.sims_admission_father_country_code = dr["sims_country_name_en"].ToString();
                        objNew.sims_admission_mother_country_code = dr["sims_country_name_en"].ToString();
                        objNew.sims_admission_guardian_country_code = dr["sims_country_name_en"].ToString();
                        mod_list.Add(objNew);

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getCity")]
        public HttpResponseMessage getCity(string state_name, bool father, bool mother)
        {
            List<admissionClass> list = new List<admissionClass>();
          
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_city_proc", new List<SqlParameter>() { new SqlParameter("@opr", "P") ,
                new SqlParameter("@sims_state_name_en",state_name)
                                    });

                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            admissionClass simsobj = new admissionClass();
                            if (father == true && mother == false)
                            {
                                simsobj.sims_admission_father_city = dr1["sims_city_name_en"].ToString();
                                simsobj.sims_admission_father_city_code = dr1["sims_city_code"].ToString();
                            }
                            else if (father == false && mother == true)
                                simsobj.sims_admission_mother_city = dr1["sims_city_name_en"].ToString();
                            else
                                simsobj.sims_admission_guardian_city = dr1["sims_city_name_en"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getParentDetails")]
        public HttpResponseMessage getParentDetails(string enroll_no)
        {

            string parent_number = "";

            List<admissionClass> parent_details = new List<admissionClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_sibling_proc", new List<SqlParameter>() { 
                  new SqlParameter("@opr", "A"),
                  new SqlParameter("@sims_sibling_student_enroll_number", enroll_no),
                    });
                    if (dr1.HasRows)
                    {

                        while (dr1.Read())
                        {
                            parent_number = dr1["sims_sibling_parent_number"].ToString();

                        }
                    }
                    dr1.Dispose();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_proc", new List<SqlParameter>() { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_parent_number", parent_number),
                new SqlParameter("@sims_parent_father_img", ""),
                new SqlParameter("@sims_parent_mother_img", ""),
                new SqlParameter("@sims_parent_guardian_img", ""),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1)

                    });
                    while (dr.Read())
                    {
                        admissionClass simsobj = new admissionClass();
                        simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                        simsobj.sims_admission_login_name = dr["sims_parent_login_code"].ToString();
                        simsobj.sims_admission_father_salutation_code = dr["sims_parent_father_salutation_code"].ToString();
                        simsobj.sims_admission_father_first_name = dr["sims_parent_father_first_name"].ToString();
                        simsobj.sims_admission_father_middle_name = dr["sims_parent_father_middle_name"].ToString();
                        simsobj.sims_admission_father_last_name = dr["sims_parent_father_last_name"].ToString();
                        simsobj.sims_admission_family_name_en = dr["sims_parent_father_family_name"].ToString();
                        simsobj.sims_admission_father_name_ot = dr["sims_parent_father_name_ot"].ToString();
                        simsobj.sims_admission_father_nationality1_code = dr["sims_parent_father_nationality1_code"].ToString();
                        simsobj.sims_admission_father_nationality2_code = dr["sims_parent_father_nationality2_code"].ToString();
                        simsobj.sims_admission_father_appartment_number = dr["sims_parent_father_appartment_number"].ToString();
                        simsobj.sims_admission_father_building_number = dr["sims_parent_father_building_number"].ToString();
                        simsobj.sims_admission_father_street_number = dr["sims_parent_father_street_number"].ToString();
                        simsobj.sims_admission_father_area_number = dr["sims_parent_father_area_number"].ToString();
                        simsobj.sims_admission_father_city = dr["sims_parent_father_city"].ToString();
                        simsobj.sims_admission_father_state = dr["sims_parent_father_state"].ToString();
                        simsobj.sims_admission_father_country_code = dr["sims_parent_father_country_code"].ToString();
                        simsobj.sims_admission_father_po_box = dr["sims_parent_father_po_box"].ToString();
                        simsobj.sims_admission_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                        simsobj.sims_admission_father_phone = dr["sims_parent_father_phone"].ToString();
                        simsobj.sims_admission_father_mobile = dr["sims_parent_father_mobile"].ToString();
                        simsobj.sims_admission_father_fax = dr["sims_parent_father_fax"].ToString();
                        simsobj.sims_admission_father_email = dr["sims_parent_father_email"].ToString();
                        simsobj.sims_admission_father_occupation = dr["sims_parent_father_occupation"].ToString();
                        simsobj.sims_parent_father_occupation_local_language = dr["sims_parent_father_occupation_local_language"].ToString();
                        simsobj.sims_parent_father_occupation_location_local_language = dr["sims_parent_father_occupation_location_local_language"].ToString();
                        simsobj.sims_admission_father_company = dr["sims_parent_father_company"].ToString();
                        simsobj.sims_admission_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                        simsobj.sims_admisison_father_image = dr["sims_parent_father_img"].ToString();
                        
                        //mother
                        simsobj.sims_admission_mother_salutation_code = dr["sims_parent_mother_salutation_code"].ToString();
                        simsobj.sims_admission_mother_first_name = dr["sims_parent_mother_first_name"].ToString();
                        simsobj.sims_admission_mother_middle_name = dr["sims_parent_mother_middle_name"].ToString();
                        simsobj.sims_admission_mother_last_name = dr["sims_parent_mother_last_name"].ToString();
                        simsobj.sims_admission_mother_family_name_en = dr["sims_parent_mother_family_name"].ToString();
                        simsobj.sims_admission_mother_name_ot = dr["sims_parent_mother_name_ot"].ToString();
                        simsobj.sims_admission_mother_nationality1_code = dr["sims_parent_mother_nationality1_code"].ToString();
                        simsobj.sims_admission_mother_nationality2_code = dr["sims_parent_mother_nationality2_code"].ToString();
                        simsobj.sims_admission_mother_appartment_number = dr["sims_parent_mother_appartment_number"].ToString();
                        simsobj.sims_admission_mother_building_number = dr["sims_parent_mother_building_number"].ToString();
                        simsobj.sims_admission_mother_street_number = dr["sims_parent_mother_street_number"].ToString();
                        simsobj.sims_admission_mother_area_number = dr["sims_parent_mother_area_number"].ToString();
                        simsobj.sims_admission_mother_city = dr["sims_parent_mother_city"].ToString();
                        simsobj.sims_admission_mother_state = dr["sims_parent_mother_state"].ToString();
                        simsobj.sims_admission_mother_country_code = dr["sims_parent_mother_country_code"].ToString();
                        simsobj.sims_admission_mother_po_box = dr["sims_parent_mother_po_box"].ToString();
                        simsobj.sims_admission_mother_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                        simsobj.sims_admission_mother_phone = dr["sims_parent_mother_phone"].ToString();
                        simsobj.sims_admission_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                        simsobj.sims_admission_mother_fax = dr["sims_parent_mother_fax"].ToString();
                        simsobj.sims_admission_mother_email = dr["sims_parent_mother_email"].ToString();
                        simsobj.sims_admission_mother_occupation = dr["sims_parent_mother_occupation"].ToString();
                        simsobj.sims_parent_mother_occupation_local_language = dr["sims_parent_mother_occupation_local_language"].ToString();
                        simsobj.sims_admission_mother_company = dr["sims_parent_mother_company"].ToString();
                        simsobj.sims_admission_mother_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                        simsobj.sims_admisison_mother_image = dr["sims_parent_mother_img"].ToString();
                        //guardian
                        simsobj.sims_admission_guardian_salutation_code = dr["sims_parent_guardian_salutation_code"].ToString();
                        simsobj.sims_admission_guardian_first_name = dr["sims_parent_guardian_first_name"].ToString();
                        simsobj.sims_admission_guardian_middle_name = dr["sims_parent_guardian_middle_name"].ToString();
                        simsobj.sims_admission_guardian_last_name = dr["sims_parent_guardian_last_name"].ToString();
                        simsobj.sims_admission_guardian_family_name_en = dr["sims_parent_guardian_family_name"].ToString();
                        simsobj.sims_admission_guardian_name_ot = dr["sims_parent_guardian_name_ot"].ToString();
                        simsobj.sims_admission_guardian_nationality1_code = dr["sims_parent_guardian_nationality1_code"].ToString();
                        simsobj.sims_admission_guardian_nationality2_code = dr["sims_parent_guardian_nationality2_code"].ToString();
                        simsobj.sims_admission_guardian_appartment_number = dr["sims_parent_guardian_appartment_number"].ToString();
                        simsobj.sims_admission_guardian_building_number = dr["sims_parent_guardian_building_number"].ToString();
                        simsobj.sims_admission_guardian_street_number = dr["sims_parent_guardian_street_number"].ToString();
                        simsobj.sims_admission_guardian_area_number = dr["sims_parent_guardian_area_number"].ToString();
                        simsobj.sims_admission_guardian_city = dr["sims_parent_guardian_city"].ToString();
                        simsobj.sims_admission_guardian_state = dr["sims_parent_guardian_state"].ToString();
                        simsobj.sims_admission_guardian_country_code = dr["sims_parent_guardian_country_code"].ToString();
                        simsobj.sims_admission_guardian_po_box = dr["sims_parent_guardian_po_box"].ToString();
                        simsobj.sims_admission_guardian_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                        simsobj.sims_admission_guardian_phone = dr["sims_parent_guardian_phone"].ToString();
                        simsobj.sims_admission_guardian_mobile = dr["sims_parent_guardian_mobile"].ToString();
                        simsobj.sims_admission_guardian_fax = dr["sims_parent_guardian_fax"].ToString();
                        simsobj.sims_admission_guardian_email = dr["sims_parent_guardian_email"].ToString();
                        simsobj.sims_admission_guardian_occupation = dr["sims_parent_guardian_occupation"].ToString();
                        simsobj.sims_parent_guardian_occupation_local_language = dr["sims_parent_guardian_occupation_local_language"].ToString();
                        simsobj.sims_parent_guardian_occupation_location_local_language = dr["sims_parent_guardian_occupation_location_local_language"].ToString();
                        simsobj.sims_admission_guardian_company = dr["sims_parent_guardian_company"].ToString();
                        simsobj.sims_parent_guardian_relationship_code = dr["sims_parent_guardian_relationship_code"].ToString();
                        simsobj.sims_admission_guardian_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                        simsobj.sims_admisison_guardian_image = dr["sims_parent_guardian_img"].ToString();
                        simsobj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].Equals("1") ? true : false;
                        simsobj.sims_parent_is_employement_comp_code = dr["sims_parent_is_employement_comp_code"].ToString();
                        simsobj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                        simsobj.sims_parent_father_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_father_passport_issue_date"].ToString());
                        simsobj.sims_parent_father_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_passport_expiry_date"].ToString());
                        simsobj.sims_parent_father_national_id = dr["sims_parent_father_national_id"].ToString();
                        simsobj.sims_parent_father_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_father_national_id_issue_date"].ToString());
                        simsobj.sims_parent_father_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_father_national_id__expiry_date"].ToString());
                        simsobj.sims_parent_mother_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_issue_date"].ToString());
                        simsobj.sims_parent_mother_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_passport_expiry_date"].ToString());
                        simsobj.sims_parent_mother_national_id = dr["sims_parent_mother_national_id"].ToString();
                        simsobj.sims_parent_mother_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_issue_date"].ToString());
                        simsobj.sims_parent_mother_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_mother_national_id_expiry_date"].ToString());
                        simsobj.sims_parent_guardian_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_issue_date"].ToString());
                        simsobj.sims_parent_guardian_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_passport_expiry_date2"].ToString());
                        simsobj.sims_parent_guardian_national_id = dr["sims_parent_guardian_national_id"].ToString();
                        simsobj.sims_parent_guardian_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_issue_date"].ToString());
                        simsobj.sims_parent_guardian_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_parent_guardian_national_id_expiry_date"].ToString());


                        try
                        {
                            simsobj.sims_parent_father_dob = db.UIDDMMYYYYformat(dr["sims_parent_father_dob"].ToString());
                            simsobj.sims_parent_mother_dob = db.UIDDMMYYYYformat(dr["sims_parent_mother_dob"].ToString());
                        }
                        catch (Exception ex){
                        }
                        parent_details.Add(simsobj);

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, parent_details);

            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, parent_details);

            }

        }

        [Route("UpdateSims_Parent_Details")]
        public HttpResponseMessage UpdateSims_Parent_Details(admissionClass simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Parent_Details()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Parent_Details", simsobj));

            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_proc", new List<SqlParameter>() { 

                new SqlParameter("@opr", 'E'),
                new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                new SqlParameter("@sims_parent_father_img", simsobj.sims_admisison_father_image),
                new SqlParameter("@sims_parent_mother_img", simsobj.sims_admisison_mother_image),
                new SqlParameter("@sims_parent_guardian_img", simsobj.sims_admisison_guardian_image),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1),
                new SqlParameter("@sims_parent_login_code", simsobj.sims_admission_login_name),
                new SqlParameter("@sims_parent_father_salutation_code", simsobj.sims_admission_father_salutation_code),
                new SqlParameter("@sims_parent_father_first_name", simsobj.sims_admission_father_first_name),
                new SqlParameter("@sims_parent_father_middle_name", simsobj.sims_admission_father_middle_name),
                new SqlParameter("@sims_parent_father_last_name", simsobj.sims_admission_father_last_name),
                new SqlParameter("@sims_parent_father_family_name", simsobj.sims_admission_family_name_en),
                new SqlParameter("@sims_parent_father_name_ot", simsobj.sims_admission_father_name_ot),
                new SqlParameter("@sims_parent_father_nationality1_code", simsobj.sims_admission_father_nationality1_code),
                new SqlParameter("@sims_parent_father_nationality2_code", simsobj.sims_admission_father_nationality2_code),
                new SqlParameter("@sims_parent_father_appartment_number", simsobj.sims_admission_father_appartment_number),
                new SqlParameter("@sims_parent_father_building_number", simsobj.sims_admission_father_building_number),
                new SqlParameter("@sims_parent_father_street_number", simsobj.sims_admission_father_street_number),
                new SqlParameter("@sims_parent_father_area_number", simsobj.sims_admission_father_area_number),
                new SqlParameter("@sims_parent_father_summary_address", simsobj.sims_admission_father_summary_address),
                new SqlParameter("@sims_parent_father_city", simsobj.sims_admission_father_city),
                new SqlParameter("@sims_parent_father_state", simsobj.sims_admission_father_state),
                new SqlParameter("@sims_parent_father_country_code", simsobj.sims_admission_father_country_code),
                new SqlParameter("@sims_parent_father_phone", simsobj.sims_admission_father_phone),
                new SqlParameter("@sims_parent_father_mobile", simsobj.sims_admission_father_mobile),
                new SqlParameter("@sims_parent_father_email", simsobj.sims_admission_father_email),
                new SqlParameter("@sims_parent_father_fax", simsobj.sims_admission_father_fax),
                new SqlParameter("@sims_parent_father_po_box", simsobj.sims_admission_father_po_box),
                new SqlParameter("@sims_parent_father_occupation", simsobj.sims_admission_father_occupation),
                new SqlParameter("@sims_parent_father_occupation_local_language", simsobj.sims_parent_father_occupation_local_language),
                new SqlParameter("@sims_parent_father_occupation_location_local_language", simsobj.sims_parent_father_occupation_location_local_language),
                new SqlParameter("@sims_parent_father_company", simsobj.sims_admission_father_company),
                new SqlParameter("@sims_parent_father_passport_number", simsobj.sims_admission_father_passport_number),
                new SqlParameter("@sims_parent_mother_salutation_code", simsobj.sims_admission_mother_salutation_code),
                new SqlParameter("@sims_parent_mother_first_name", simsobj.sims_admission_mother_first_name),
                new SqlParameter("@sims_parent_mother_middle_name", simsobj.sims_admission_mother_middle_name),
                new SqlParameter("@sims_parent_mother_last_name", simsobj.sims_admission_mother_last_name),
                new SqlParameter("@sims_parent_mother_family_name", simsobj.sims_admission_mother_family_name_en),
                new SqlParameter("@sims_parent_mother_name_ot", simsobj.sims_admission_mother_name_ot),
                new SqlParameter("@sims_parent_mother_nationality1_code", simsobj.sims_admission_mother_nationality1_code),
                new SqlParameter("@sims_parent_mother_nationality2_code", simsobj.sims_admission_mother_nationality2_code),
                new SqlParameter("@sims_parent_mother_appartment_number", simsobj.sims_admission_mother_appartment_number),
                new SqlParameter("@sims_parent_mother_building_number", simsobj.sims_admission_mother_building_number),
                new SqlParameter("@sims_parent_mother_street_number", simsobj.sims_admission_mother_street_number),
                new SqlParameter("@sims_parent_mother_area_number", simsobj.sims_admission_mother_area_number),
                new SqlParameter("@sims_parent_mother_summary_address", simsobj.sims_admission_mother_summary_address),
                new SqlParameter("@sims_parent_mother_city", simsobj.sims_admission_mother_city),
                new SqlParameter("@sims_parent_mother_state", simsobj.sims_admission_mother_state),
                new SqlParameter("@sims_parent_mother_country_code", simsobj.sims_admission_mother_country_code),
                new SqlParameter("@sims_parent_mother_phone", simsobj.sims_admission_mother_phone),
                new SqlParameter("@sims_parent_mother_mobile", simsobj.sims_admission_mother_mobile),
                new SqlParameter("@sims_parent_mother_email", simsobj.sims_admission_mother_email),
                new SqlParameter("@sims_parent_mother_fax", simsobj.sims_admission_mother_fax),
                new SqlParameter("@sims_parent_mother_po_box", simsobj.sims_admission_mother_po_box),
                new SqlParameter("@sims_parent_mother_occupation", simsobj.sims_admission_mother_occupation),
                new SqlParameter("@sims_parent_mother_occupation_local_language", simsobj.sims_parent_mother_occupation_local_language),
                new SqlParameter("@sims_parent_mother_company", simsobj.sims_admission_mother_company),
                new SqlParameter("@sims_parent_mother_passport_number", simsobj.sims_admission_mother_passport_number),
                new SqlParameter("@sims_parent_guardian_salutation_code", simsobj.sims_admission_guardian_salutation_code),
                new SqlParameter("@sims_parent_guardian_first_name", simsobj.sims_admission_guardian_first_name),
                new SqlParameter("@sims_parent_guardian_middle_name", simsobj.sims_admission_guardian_middle_name),
                new SqlParameter("@sims_parent_guardian_last_name", simsobj.sims_admission_guardian_last_name),
                new SqlParameter("@sims_parent_guardian_family_name", simsobj.sims_admission_guardian_family_name_en),
                new SqlParameter("@sims_parent_guardian_name_ot", simsobj.sims_admission_guardian_name_ot),
                new SqlParameter("@sims_parent_guardian_nationality1_code", simsobj.sims_admission_guardian_nationality1_code),
                new SqlParameter("@sims_parent_guardian_nationality2_code", simsobj.sims_admission_guardian_nationality2_code),
                new SqlParameter("@sims_parent_guardian_appartment_number", simsobj.sims_admission_guardian_appartment_number),
                new SqlParameter("@sims_parent_guardian_building_number", simsobj.sims_admission_guardian_building_number),
                new SqlParameter("@sims_parent_guardian_street_number", simsobj.sims_admission_guardian_street_number),
                new SqlParameter("@sims_parent_guardian_area_number", simsobj.sims_admission_guardian_area_number),
                new SqlParameter("@sims_parent_guardian_summary_address", simsobj.sims_admission_guardian_summary_address),
                new SqlParameter("@sims_parent_guardian_city", simsobj.sims_admission_guardian_city),
                new SqlParameter("@sims_parent_guardian_state", simsobj.sims_admission_guardian_state),
                new SqlParameter("@sims_parent_guardian_country_code", simsobj.sims_admission_guardian_country_code),
                new SqlParameter("@sims_parent_guardian_phone", simsobj.sims_admission_guardian_phone),
                new SqlParameter("@sims_parent_guardian_mobile", simsobj.sims_admission_guardian_mobile),
                new SqlParameter("@sims_parent_guardian_email", simsobj.sims_admission_guardian_email),
                new SqlParameter("@sims_parent_guardian_fax", simsobj.sims_admission_guardian_fax),
                new SqlParameter("@sims_parent_guardian_po_box", simsobj.sims_admission_guardian_po_box),
                new SqlParameter("@sims_parent_guardian_occupation", simsobj.sims_admission_guardian_occupation),
                new SqlParameter("@sims_parent_guardian_occupation_local_language", simsobj.sims_parent_guardian_occupation_local_language),
                new SqlParameter("@sims_parent_guardian_occupation_location_local_language", simsobj.sims_parent_guardian_occupation_location_local_language),
                new SqlParameter("@sims_parent_guardian_company", simsobj.sims_admission_guardian_company),
                new SqlParameter("@sims_parent_guardian_relationship_code", simsobj.sims_parent_guardian_relationship_code),
                new SqlParameter("@sims_parent_guardian_passport_number", simsobj.sims_admission_guardian_passport_number),
                new SqlParameter("@sims_parent_is_employment_status", simsobj.sims_parent_is_employment_status.Equals(true)?"1":"0"),
                new SqlParameter("@sims_parent_is_employement_comp_code", simsobj.sims_parent_is_employement_comp_code),
                new SqlParameter("@sims_parent_is_employment_number", simsobj.sims_parent_is_employment_number),
                new SqlParameter("@sims_parent_father_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_issue_date)),
                new SqlParameter("@sims_parent_father_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_expiry_date)),
                new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                new SqlParameter("@sims_parent_father_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_issue_date)),
                new SqlParameter("@sims_parent_father_national_id__expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_expiry_date)),
                new SqlParameter("@sims_parent_mother_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_issue_date)),
                new SqlParameter("@sims_parent_mother_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_expiry_date)),
                new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                new SqlParameter("@sims_parent_mother_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_issue_date)),
                new SqlParameter("@sims_parent_mother_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_expiry_date)),
                new SqlParameter("@sims_parent_guardian_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_issue_date)),
                new SqlParameter("@sims_parent_guardian_passport_expiry_date2", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_expiry_date)),
                new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                new SqlParameter("@sims_parent_guardian_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_national_id_issue_date)),
                new SqlParameter("@sims_parent_guardian_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_national_id_expiry_date)),

                new SqlParameter("@sims_parent_father_dob", db.DBYYYYMMDDformat(simsobj.sims_parent_father_dob)),
                new SqlParameter("@sims_parent_mother_dob", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_dob))

                    });


                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated successfully";
                        inserted = true;
                    }
                    else
                    {

                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }


            catch (Exception e)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("UpdateSims_Parent_Details_stag")]
        public HttpResponseMessage UpdateSims_Parent_Details_stag(admissionClass simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateSims_Parent_Details_stag()PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "api/studentdatabase", "UpdateSims_Parent_Details_stag", simsobj));

            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_parent_leams_proc", new List<SqlParameter>() {

                new SqlParameter("@opr", 'E'),
                new SqlParameter("@sims_parent_number", simsobj.sims_parent_number),
                new SqlParameter("@sims_parent_father_img", simsobj.sims_admisison_father_image),
                new SqlParameter("@sims_parent_mother_img", simsobj.sims_admisison_mother_image),
                new SqlParameter("@sims_parent_guardian_img", simsobj.sims_admisison_guardian_image),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims032_appl_code),
                new SqlParameter("@sims_appl_code1", CommonStaticClass.Sims032_appl_code1),
                new SqlParameter("@sims_salutation_appl_form_field", CommonStaticClass.Sims032_appl_form_field),
                new SqlParameter("@sims_relationship_appl_form_field", CommonStaticClass.Sims032_appl_form_field1),
                new SqlParameter("@sims_parent_login_code", simsobj.sims_admission_login_name),
                new SqlParameter("@sims_parent_father_salutation_code", simsobj.sims_admission_father_salutation_code),
                new SqlParameter("@sims_parent_father_first_name", simsobj.sims_admission_father_first_name),
                new SqlParameter("@sims_parent_father_middle_name", simsobj.sims_admission_father_middle_name),
                new SqlParameter("@sims_parent_father_last_name", simsobj.sims_admission_father_last_name),
                new SqlParameter("@sims_parent_father_family_name", simsobj.sims_admission_family_name_en),
                new SqlParameter("@sims_parent_father_name_ot", simsobj.sims_admission_father_name_ot),
                new SqlParameter("@sims_parent_father_nationality1_code", simsobj.sims_admission_father_nationality1_code),
                new SqlParameter("@sims_parent_father_nationality2_code", simsobj.sims_admission_father_nationality2_code),
                new SqlParameter("@sims_parent_father_appartment_number", simsobj.sims_admission_father_appartment_number),
                new SqlParameter("@sims_parent_father_building_number", simsobj.sims_admission_father_building_number),
                new SqlParameter("@sims_parent_father_street_number", simsobj.sims_admission_father_street_number),
                new SqlParameter("@sims_parent_father_area_number", simsobj.sims_admission_father_area_number),
                new SqlParameter("@sims_parent_father_summary_address", simsobj.sims_admission_father_summary_address),
                new SqlParameter("@sims_parent_father_city", simsobj.sims_admission_father_city),
                new SqlParameter("@sims_parent_father_state", simsobj.sims_admission_father_state),
                new SqlParameter("@sims_parent_father_country_code", simsobj.sims_admission_father_country_code),

                new SqlParameter("@sims_parent_father_mobile_isd_code", simsobj.sims_parent_father_mobile_isd_code),
                new SqlParameter("@sims_parent_father_phone", simsobj.sims_admission_father_phone),
                new SqlParameter("@sims_parent_father_mobile", simsobj.sims_admission_father_mobile),
                new SqlParameter("@sims_parent_father_email", simsobj.sims_admission_father_email),
                new SqlParameter("@sims_parent_father_fax", simsobj.sims_admission_father_fax),
                new SqlParameter("@sims_parent_father_po_box", simsobj.sims_admission_father_po_box),
                new SqlParameter("@sims_parent_father_occupation", simsobj.sims_admission_father_occupation),
                new SqlParameter("@sims_parent_father_occupation_local_language", simsobj.sims_parent_father_occupation_local_language),
                new SqlParameter("@sims_parent_father_occupation_location_local_language", simsobj.sims_parent_father_occupation_location_local_language),
                new SqlParameter("@sims_parent_father_company", simsobj.sims_admission_father_company),
                new SqlParameter("@sims_parent_father_passport_number", simsobj.sims_admission_father_passport_number),
                new SqlParameter("@sims_parent_mother_salutation_code", simsobj.sims_admission_mother_salutation_code),
                new SqlParameter("@sims_parent_mother_first_name", simsobj.sims_admission_mother_first_name),
                new SqlParameter("@sims_parent_mother_middle_name", simsobj.sims_admission_mother_middle_name),
                new SqlParameter("@sims_parent_mother_last_name", simsobj.sims_admission_mother_last_name),
                new SqlParameter("@sims_parent_mother_family_name", simsobj.sims_admission_mother_family_name_en),
                new SqlParameter("@sims_parent_mother_name_ot", simsobj.sims_admission_mother_name_ot),
                new SqlParameter("@sims_parent_mother_nationality1_code", simsobj.sims_admission_mother_nationality1_code),
                new SqlParameter("@sims_parent_mother_nationality2_code", simsobj.sims_admission_mother_nationality2_code),
                new SqlParameter("@sims_parent_mother_appartment_number", simsobj.sims_admission_mother_appartment_number),
                new SqlParameter("@sims_parent_mother_building_number", simsobj.sims_admission_mother_building_number),
                new SqlParameter("@sims_parent_mother_street_number", simsobj.sims_admission_mother_street_number),
                new SqlParameter("@sims_parent_mother_area_number", simsobj.sims_admission_mother_area_number),
                new SqlParameter("@sims_parent_mother_summary_address", simsobj.sims_admission_mother_summary_address),
                new SqlParameter("@sims_parent_mother_city", simsobj.sims_admission_mother_city),
                new SqlParameter("@sims_parent_mother_state", simsobj.sims_admission_mother_state),
                new SqlParameter("@sims_parent_mother_country_code", simsobj.sims_admission_mother_country_code),
                new SqlParameter("@sims_parent_mother_phone", simsobj.sims_admission_mother_phone),
                new SqlParameter("@sims_parent_mother_mobile", simsobj.sims_admission_mother_mobile),
                new SqlParameter("@sims_parent_mother_email", simsobj.sims_admission_mother_email),
                new SqlParameter("@sims_parent_mother_fax", simsobj.sims_admission_mother_fax),
                new SqlParameter("@sims_parent_mother_po_box", simsobj.sims_admission_mother_po_box),
                new SqlParameter("@sims_parent_mother_occupation", simsobj.sims_admission_mother_occupation),
                new SqlParameter("@sims_parent_mother_occupation_local_language", simsobj.sims_parent_mother_occupation_local_language),
                new SqlParameter("@sims_parent_mother_company", simsobj.sims_admission_mother_company),
                new SqlParameter("@sims_parent_mother_passport_number", simsobj.sims_admission_mother_passport_number),
                new SqlParameter("@sims_parent_guardian_salutation_code", simsobj.sims_admission_guardian_salutation_code),
                new SqlParameter("@sims_parent_guardian_first_name", simsobj.sims_admission_guardian_first_name),
                new SqlParameter("@sims_parent_guardian_middle_name", simsobj.sims_admission_guardian_middle_name),
                new SqlParameter("@sims_parent_guardian_last_name", simsobj.sims_admission_guardian_last_name),
                new SqlParameter("@sims_parent_guardian_family_name", simsobj.sims_admission_guardian_family_name_en),
                new SqlParameter("@sims_parent_guardian_name_ot", simsobj.sims_admission_guardian_name_ot),
                new SqlParameter("@sims_parent_guardian_nationality1_code", simsobj.sims_admission_guardian_nationality1_code),
                new SqlParameter("@sims_parent_guardian_nationality2_code", simsobj.sims_admission_guardian_nationality2_code),
                new SqlParameter("@sims_parent_guardian_appartment_number", simsobj.sims_admission_guardian_appartment_number),
                new SqlParameter("@sims_parent_guardian_building_number", simsobj.sims_admission_guardian_building_number),
                new SqlParameter("@sims_parent_guardian_street_number", simsobj.sims_admission_guardian_street_number),
                new SqlParameter("@sims_parent_guardian_area_number", simsobj.sims_admission_guardian_area_number),
                new SqlParameter("@sims_parent_guardian_summary_address", simsobj.sims_admission_guardian_summary_address),
                new SqlParameter("@sims_parent_guardian_city", simsobj.sims_admission_guardian_city),
                new SqlParameter("@sims_parent_guardian_state", simsobj.sims_admission_guardian_state),
                new SqlParameter("@sims_parent_guardian_country_code", simsobj.sims_admission_guardian_country_code),
                new SqlParameter("@sims_parent_guardian_phone", simsobj.sims_admission_guardian_phone),
                new SqlParameter("@sims_parent_guardian_mobile", simsobj.sims_admission_guardian_mobile),
                new SqlParameter("@sims_parent_guardian_email", simsobj.sims_admission_guardian_email),
                new SqlParameter("@sims_parent_guardian_fax", simsobj.sims_admission_guardian_fax),
                new SqlParameter("@sims_parent_guardian_po_box", simsobj.sims_admission_guardian_po_box),
                new SqlParameter("@sims_parent_guardian_occupation", simsobj.sims_admission_guardian_occupation),
                new SqlParameter("@sims_parent_guardian_occupation_local_language", simsobj.sims_parent_guardian_occupation_local_language),
                new SqlParameter("@sims_parent_guardian_occupation_location_local_language", simsobj.sims_parent_guardian_occupation_location_local_language),
                new SqlParameter("@sims_parent_guardian_company", simsobj.sims_admission_guardian_company),
                new SqlParameter("@sims_parent_guardian_relationship_code", simsobj.sims_parent_guardian_relationship_code),
                new SqlParameter("@sims_parent_guardian_passport_number", simsobj.sims_admission_guardian_passport_number),
                new SqlParameter("@sims_parent_is_employment_status", simsobj.sims_parent_is_employment_status.Equals(true)?"1":"0"),
                new SqlParameter("@sims_parent_is_employement_comp_code", simsobj.sims_parent_is_employement_comp_code),
                new SqlParameter("@sims_parent_is_employment_number", simsobj.sims_parent_is_employment_number),
                new SqlParameter("@sims_parent_father_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_issue_date)),
                new SqlParameter("@sims_parent_father_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_passport_expiry_date)),
                new SqlParameter("@sims_parent_father_national_id", simsobj.sims_parent_father_national_id),
                new SqlParameter("@sims_parent_father_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_issue_date)),
                new SqlParameter("@sims_parent_father_national_id__expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_father_national_id_expiry_date)),
                new SqlParameter("@sims_parent_mother_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_issue_date)),
                new SqlParameter("@sims_parent_mother_passport_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_passport_expiry_date)),
                new SqlParameter("@sims_parent_mother_national_id", simsobj.sims_parent_mother_national_id),
                new SqlParameter("@sims_parent_mother_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_issue_date)),
                new SqlParameter("@sims_parent_mother_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_parent_mother_national_id_expiry_date)),
                new SqlParameter("@sims_parent_guardian_passport_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_issue_date)),
                new SqlParameter("@sims_parent_guardian_passport_expiry_date2", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_passport_expiry_date)),
                new SqlParameter("@sims_parent_guardian_national_id", simsobj.sims_parent_guardian_national_id),
                new SqlParameter("@sims_parent_guardian_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.sims_parent_guardian_national_id_issue_date)),
                new SqlParameter("@sims_parent_guardian_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.@sims_parent_guardian_national_id_expiry_date))
                    });


                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Database Information Updated successfully";
                        inserted = true;
                    }
                    else
                    {

                        message.strMessage = "Student Database Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }


            catch (Exception e)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #region Sims098(medical_student_details)
        [Route("get_Student_Medical_Details")]
        public HttpResponseMessage get_Student_Medical_Details(string enroll_no)
        {
            List<Sims098> sims_student = new List<Sims098>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_medical_student_proc", new List<SqlParameter>() { 
                
                new SqlParameter("@opr", 'S'),
                new SqlParameter("@sims_student_enroll_number", enroll_no),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_blood_group_appl_form_field", CommonStaticClass.Sims042_appl_form_field9),
                new SqlParameter("@sims_health_card_number", ""),
                new SqlParameter("@sims_health_card_issue_date", ""),
                new SqlParameter("@sims_health_card_expiry_date", ""),
                new SqlParameter("@sims_health_card_issuing_authority", ""),
                new SqlParameter("@sims_blood_group_code", ""),
                new SqlParameter("@sims_health_restriction_status", "Y"),
                new SqlParameter("@sims_health_restriction_desc", ""),
                new SqlParameter("@sims_disability_status", ""),
                new SqlParameter("@sims_disability_desc", ""),
                new SqlParameter("@sims_medication_status", ""),
                new SqlParameter("@sims_medication_desc", ""),
                new SqlParameter("@sims_health_other_status", ""),
                new SqlParameter("@sims_health_other_desc", ""),
                new SqlParameter("@sims_health_hearing_status", ""),
                new SqlParameter("@sims_health_hearing_desc", ""),
                new SqlParameter("@sims_health_vision_status", ""),
                new SqlParameter("@sims_health_vision_desc", ""),
                new SqlParameter("@sims_regular_doctor_name", ""),
                new SqlParameter("@sims_regular_doctor_phone", ""),
                new SqlParameter("@sims_regular_hospital_name", ""),
                new SqlParameter("@sims_regular_hospital_phone", ""),
                new SqlParameter("@sims_height", ""),
                new SqlParameter("@sims_wieght", ""),
                new SqlParameter("@sims_teeth", ""),
                new SqlParameter("@min_val", ""),
                new SqlParameter("@max_val", ""),

                    });
                    while (dr.Read())
                    {
                        Sims098 simsobj = new Sims098();
                        simsobj.student_enroll_no = dr["sims_enrollment_number"].ToString();
                        simsobj.student_full_name = dr["sims_student_full_name"].ToString();
                        simsobj.health_card_number = dr["sims_health_card_number"].ToString();
                       
                            simsobj.health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_health_card_issue_date"].ToString());
                        
                            simsobj.health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_health_card_expiry_date"].ToString());
                        simsobj.health_card_issuing_authority = dr["sims_health_card_issuing_authority"].ToString();
                        simsobj.student_blood_group_code = dr["sims_blood_group_code"].ToString();
                       // simsobj.sims_health_restriction_status = (dr["sims_health_restriction_status"].ToString().Equals("Y") ? true : false);
                         simsobj.student_health_restriction_status = (dr["sims_health_restriction_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                        //simsobj.student_disability_status = dr["sims_disability_status"].ToString();
                        simsobj.sims_disability_status = (dr["sims_disability_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                        //simsobj.student_medication_status = dr["sims_medication_status"].ToString();
                        simsobj.sims_medication_status = (dr["sims_medication_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                        //simsobj.health_other_status = dr["sims_health_other_status"].ToString();
                        simsobj.sims_health_other_status = (dr["sims_health_other_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                        simsobj.sims_health_hearing_status = (dr["sims_health_hearing_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                        simsobj.sims_health_vision_status = (dr["sims_health_vision_status"].ToString().Equals("Y") ? true : false);
                        simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                        simsobj.regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                        simsobj.regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                        simsobj.regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                        simsobj.regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();
                        simsobj.student_height = dr["sims_height"].ToString();
                        simsobj.student_wieght = dr["sims_wieght"].ToString();
                        simsobj.student_teeth = dr["sims_teeth"].ToString();

                        sims_student.Add(simsobj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sims_student);

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_student);
        }

        #endregion

        #region Sims098(medical_student_details)
        [Route("update_Student_Medical_Details")]
        public HttpResponseMessage update_Student_Medical_Details(Sims098 simsobj)
        {
            Message message = new Message();
            bool inserted = false;
            List<Sims098> sims_student = new List<Sims098>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_medical_student_proc", new List<SqlParameter>() { 
                
                new SqlParameter("@opr", 'U'),
                new SqlParameter("@sims_student_enroll_number", simsobj.student_enroll_no),
                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims042_appl_code1),
                new SqlParameter("@sims_blood_group_appl_form_field", CommonStaticClass.Sims042_appl_form_field9),
                new SqlParameter("@sims_health_card_number", simsobj.health_card_number),
                new SqlParameter("@sims_health_card_issue_date", db.DBYYYYMMDDformat(simsobj.health_card_issue_date)),
                new SqlParameter("@sims_health_card_expiry_date", db.DBYYYYMMDDformat(simsobj.health_card_expiry_date)),
                new SqlParameter("@sims_health_card_issuing_authority", simsobj.health_card_issuing_authority),
                new SqlParameter("@sims_blood_group_code", simsobj.student_blood_group_code),
                new SqlParameter("@sims_health_restriction_status", simsobj.student_health_restriction_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_restriction_desc", simsobj.sims_health_restriction_desc),
                new SqlParameter("@sims_disability_status", simsobj.sims_disability_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_disability_desc", simsobj.sims_disability_desc),
                new SqlParameter("@sims_medication_status", simsobj.sims_medication_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_medication_desc", simsobj.sims_medication_desc),
                new SqlParameter("@sims_health_other_status",simsobj.sims_health_other_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_other_desc", simsobj.sims_health_other_desc),
                new SqlParameter("@sims_health_hearing_status", simsobj.sims_health_hearing_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_hearing_desc", simsobj.sims_health_hearing_desc),
                new SqlParameter("@sims_health_vision_status", simsobj.sims_health_vision_status.Equals(true)?"Y":"N"),
                new SqlParameter("@sims_health_vision_desc", simsobj.sims_health_vision_desc),
                new SqlParameter("@sims_regular_doctor_name", simsobj.regular_doctor_name),
                new SqlParameter("@sims_regular_doctor_phone", simsobj.regular_doctor_phone),
                new SqlParameter("@sims_regular_hospital_name", simsobj.regular_hospital_name),
                new SqlParameter("@sims_regular_hospital_phone", simsobj.regular_hospital_phone),
                new SqlParameter("@sims_height", simsobj.student_height),
                new SqlParameter("@sims_wieght", simsobj.student_wieght),
                new SqlParameter("@sims_teeth", simsobj.student_teeth),
                new SqlParameter("@min_val", ""),
                new SqlParameter("@max_val", ""),

                    });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Student Medical Information Updated Sucessfully";
                      inserted = true;
                    }
                    else
                    {
                        message.strMessage = "Student Medical Information Not Updated";
                        inserted = false;
                    }
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);

            }
            catch (Exception e)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        #endregion

        [Route("getlblnationalId")]     //Get National ID Label 
        public HttpResponseMessage getlblnationalId()
        {
            List<Sims042> country = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Q")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_lbl = dr["sims_appl_form_field_value1"].ToString();
                            //obj.sims_student_birth_country = dr["sims_country_code"].ToString();
                            country.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, country);
            }


            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, country);

        }

        [Route("getStudentCategory")]     
        public HttpResponseMessage getStudentCategory()
        {
            List<Sims042> category = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_niss_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "A")
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_student_category = dr["sims_appl_parameter"].ToString();
                            obj.student_category_name = dr["sims_appl_form_field_value"].ToString();

                            category.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, category);
            }
            
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            
        }

        [Route("GetPreviousSchool")]     
        public HttpResponseMessage GetPreviousSchool()
        {
            List<Sims042> category = new List<Sims042>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "K")
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            category.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, category);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

        }

        // Staging1,staging2

        [Route("getfathermobileisd")]
        public HttpResponseMessage getfathermobileisd(string country_code)
        {
            List<admissionClass> category = new List<admissionClass>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parent_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "K"),
                        new SqlParameter("@sims_parent_father_country_code", country_code)

                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionClass obj = new admissionClass();

                            obj.sims_parent_father_mobile_isd_code = dr["sims_country_isd_code"].ToString();
                             

                            category.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, category);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

        }


        [Route("getSimsStudentExportDetails")]
        public HttpResponseMessage getSimsStudentExportDetails(string cur_code, string academic_year, string grade_code, string section_code, string search_flag, string search_text)
        //public HttpResponseMessage getSimsStudentExportDetails(studDetails x)
        {
            List<Sims176> student_data = new List<Sims176>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_export_details",
                    new List<SqlParameter>()
                    {
                        //new SqlParameter("@opr", "S"),
                        //new SqlParameter("@cur_code", x.cur_code),
                        //new SqlParameter("@academic_year", x.academic_year),
                        //new SqlParameter("@grade_code", x.grade_code),
                        //new SqlParameter("@section_code", x.section_code),
                        //new SqlParameter("@search_flag", x.search_flag),
                        //new SqlParameter("@search_text", x.search_text)
                        new SqlParameter("@opr", 'S'),
                        new SqlParameter("@cur_code", cur_code),
                        new SqlParameter("@academic_year", academic_year),
                        new SqlParameter("@grade_code", grade_code),
                        new SqlParameter("@section_code", section_code),
                        new SqlParameter("@search_flag", search_flag),
                        new SqlParameter("@search_text", search_text)
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims176 obj = new Sims176();
                            obj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.father_name = dr["parent_name"].ToString();
                            obj.mother_name = dr["mother_name"].ToString();
                            obj.sims_parent_father_mobile = dr["parent_mobile_no"].ToString();
                            obj.sims_parent_guardian_email = dr["parent_email_id"].ToString();
                            obj.sims_academic_year = dr["sims_student_academic_status"].ToString();
                            student_data.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, student_data);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

        }

        [Route("getAreaDetails")]
        public HttpResponseMessage getAreaDetails()
        {
            List<Finn212> mod_list = new List<Finn212>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'L'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn212 simsobj = new Finn212();
                            simsobj.sims_municipality_name_en = dr["sims_municipality_name_en"].ToString();
                            mod_list.Add(simsobj);

                        }
                        //return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    //else
                        //return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAllIncome")]
        public HttpResponseMessage GetAllIncome()
        {
            List<Sims042> lstAge = new List<Sims042>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'L'),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 em = new Sims042();
                            em.sims_admission_family_monthly_income = dr["sims_appl_parameter"].ToString();
                            em.sims_admission_family_monthly_income_desc = dr["sims_appl_form_field_value1"].ToString();
                            lstAge.Add(em);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lstAge);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstAge);
        }
    }
}








