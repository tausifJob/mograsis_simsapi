﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/RequestCertificate")]
    [BasicAuthentication]
    public class RequestCertificateController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllCertificate")]
        public HttpResponseMessage GetAllCertificate()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllCertificate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllCertificate"));

            List<Sims108> goaltarget_list = new List<Sims108>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_request_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims108 simsobj = new Sims108();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetAllUserType")]
        public HttpResponseMessage GetAllUserType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllUserType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllUserType"));

            List<Sims108> goaltarget_list = new List<Sims108>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_request_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims108 simsobj = new Sims108();
                            simsobj.user_type = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetCertificateStatus")]
        public HttpResponseMessage GetCertificateStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCertificateStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetCertificateStatus"));

            List<Sims108> goaltarget_list = new List<Sims108>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_request_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims108 simsobj = new Sims108();
                            //simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_request_status = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetRequestCertificate")]
        public HttpResponseMessage GetRequestCertificate()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetRequestCertificate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetRequestCertificate"));

            List<Sims108> goaltarget_list = new List<Sims108>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_certificate_request_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims108 simsobj = new Sims108();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_certificate_request_date"].ToString()))
                            //    simsobj.sims_certificate_request_date = DateTime.Parse(dr["sims_certificate_request_date"].ToString()).ToShortDateString();
                            simsobj.sims_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_certificate_request_date"].ToString());
                            simsobj.sims_certificate_requested_by = dr["sims_certificate_requested_by"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_reason = dr["sims_certificate_reason"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_certificate_expected_date"].ToString()))
                            //    simsobj.sims_certificate_expected_date = DateTime.Parse(dr["sims_certificate_expected_date"].ToString()).ToShortDateString();
                            simsobj.sims_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_certificate_expected_date"].ToString());
                            simsobj.sims_certificate_enroll_number = dr["sims_certificate_enroll_number"].ToString();
                            simsobj.sims_certificate_issued_by = dr["sims_certificate_issued_by"].ToString();
                            simsobj.sims_certificate_request_status = dr["sims_certificate_request_status"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDRequestCertificate")]
        public HttpResponseMessage CUDRequestCertificate(List<Sims108> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims108 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_certificate_request_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_certificate_request_number", simsobj.sims_certificate_request_number),
                                new SqlParameter("@sims_certificate_request_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_request_date)),   
                                new SqlParameter("@sims_certificate_requested_by", simsobj.sims_certificate_requested_by),
                                new SqlParameter("@sims_certificate_number", simsobj.sims_certificate_number),   
                                new SqlParameter("@sims_certificate_reason", simsobj.sims_certificate_reason),
                                new SqlParameter("@sims_certificate_expected_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_expected_date)),   
                                new SqlParameter("@sims_certificate_enroll_number", simsobj.sims_certificate_enroll_number),
                                new SqlParameter("@sims_certificate_issued_by", simsobj.sims_certificate_issued_by),   
                                new SqlParameter("@sims_certificate_request_status", simsobj.sims_certificate_request_status),  
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}