﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
using System.Linq;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/surveyselection")]
    public class SurveySelectionController : ApiController
    {


        [Route("Get_survey_for_user")]
        public HttpResponseMessage Get_survey_for_user(string user_code)
        {
            List<Survey> com_list = new List<Survey>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveySelectionProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S'),
                              new SqlParameter("@comn_user_code", user_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey FinnObj = new Survey();
                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_subject = dr["sims_survey_subject"].ToString();
                            FinnObj.sims_survey_desc_en = dr["sims_survey_desc_en"].ToString();
                            FinnObj.sims_survey_end_date = dr["sims_survey_end_date"].ToString();
                            FinnObj.sims_survey_start_date = dr["sims_survey_start_date"].ToString();
                            FinnObj.sims_survey_user_group_code = dr["survey_user_group"].ToString();
                            FinnObj.sims_survey_type = dr["sims_survey_type"].ToString();
                            com_list.Add(FinnObj);
                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("get_survey_details_quetions")]
        public HttpResponseMessage get_survey_details_quetions(string sims_survey_code)
        {
            List<Sims711> com_list = new List<Sims711>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].SurveySelectionProc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "SS"),
                              new SqlParameter("@sims_survey_code", sims_survey_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 FinnObj = new Sims711();

                            FinnObj.sims_survey_code = dr["sims_survey_code"].ToString();
                            FinnObj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            FinnObj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                            FinnObj.sims_survey_type = dr["sims_survey_type"].ToString();
                            FinnObj.sims_survey_no_of_question_available = dr["sims_survey_no_of_question_available"].ToString();
                            FinnObj.sims_survey_no_of_question_to_attempt = dr["sims_survey_no_of_question_to_attempt"].ToString();

                            FinnObj.que_ans = new List<ques_answer>();

                            ques_answer qs = new ques_answer();
                            qs.sims_survey_question_answer_desc_en = dr["sims_survey_question_answer_desc_en"].ToString();
                            qs.sims_survey_question_answer_code = dr["sims_survey_question_answer_code"].ToString();
                            qs.sims_survey_question_type = dr["sims_survey_question_type"].ToString();

                            string str = dr["sims_survey_question_code"].ToString();

                            var v = (from p in com_list where p.sims_survey_question_code == str select p);
                            if (v.Count() == 0)
                            {
                                FinnObj.que_ans.Add(qs);
                                com_list.Add(FinnObj);

                            }
                            else
                            {
                                v.ElementAt(0).que_ans.Add(qs);
                            }


                        };
                    }
                }


            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }



        [Route("CUD_PP_Survey")]
        public HttpResponseMessage CUD_PP_Survey(List<Sims711> obj)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 simsobj in obj)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[SurveySelectionProc]",
                            new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_survey_user_response_user_code", simsobj.sims_survey_user_response_code),
                            new SqlParameter("@sims_survey_code", simsobj.sims_survey_code),
                            new SqlParameter("@sims_survey_question_code", simsobj.sims_survey_question_code),
                            new SqlParameter("@sims_survey_question_answer_code", simsobj.sims_survey_question_answer_code),
                            new SqlParameter("@sims_survey_question_answer_desc_en", simsobj.sims_survey_question_answer_desc_en),
                             });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("get_survey_details_check")]
        public HttpResponseMessage get_survey_details_check(string sims_survey_code, string sims_user_code)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[SurveySelectionProc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@sims_survey_code", sims_survey_code),
                            new SqlParameter("@comn_user_code", sims_user_code)
                        });
                    if (dr.Read())
                    {
                        inserted = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}