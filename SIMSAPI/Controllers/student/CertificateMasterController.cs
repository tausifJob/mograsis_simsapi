﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/CertificateMaster")]
    public class CertificateMasterController:ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getCertificate")]
        public HttpResponseMessage getCertificate()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCertificate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getCertificate"));

            List<Sims104> goaltarget_list = new List<Sims104>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                       new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sub_opr", "A"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims104 simsobj = new Sims104();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_code_name = dr["sims_cur_name"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_desc = dr["sims_certificate_desc"].ToString();
                            simsobj.sims_certificate_watermark = dr["sims_certificate_watermark"].ToString();

                            if (dr["sims_certificate_status"].ToString().Equals("A"))
                                simsobj.sims_certificate_status = true;
                            else
                                simsobj.sims_certificate_status = false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getCertificateByNo")]
        public HttpResponseMessage getCertificateByNo(string certificate_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCertificateByNo(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getCertificateByNo"));

            Sims104 simsobj = new Sims104();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_certificate_proc]",
                        //SqlDataReader dr = db.ExecuteStoreProcedure("sims_certificate_issue",
                       new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sub_opr", "B"),
                           new SqlParameter("@sims_certificate_number", certificate_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_code_name = dr["sims_cur_name"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_desc = dr["sims_certificate_desc"].ToString();
                            simsobj.sims_certificate_count_flag = dr["sims_certificate_count_flag"].ToString();
                            simsobj.sims_certificate_watermark = dr["sims_certificate_watermark"].ToString();
                            simsobj.sims_certificate_header1 = dr["sims_certificate_header1"].ToString();
                            simsobj.sims_certificate_header2 = dr["sims_certificate_header2"].ToString();
                            simsobj.sims_certificate_header3 = dr["sims_certificate_header3"].ToString();
                            simsobj.sims_certificate_header4 = dr["sims_certificate_header4"].ToString();
                            simsobj.sims_certificate_header5 = dr["sims_certificate_header5"].ToString();
                            simsobj.sims_certificate_body1 = dr["sims_certificate_body1"].ToString();
                            simsobj.sims_certificate_body2 = dr["sims_certificate_body2"].ToString();
                            simsobj.sims_certificate_body3 = dr["sims_certificate_body3"].ToString();
                            simsobj.sims_certificate_body4 = dr["sims_certificate_body4"].ToString();
                            simsobj.sims_certificate_body5 = dr["sims_certificate_body5"].ToString();
                            simsobj.sims_certificate_body6 = dr["sims_certificate_body6"].ToString();
                            simsobj.sims_certificate_body7 = dr["sims_certificate_body7"].ToString();
                            simsobj.sims_certificate_body8 = dr["sims_certificate_body8"].ToString();
                            simsobj.sims_certificate_body9 = dr["sims_certificate_body9"].ToString();
                            simsobj.sims_certificate_body10 = dr["sims_certificate_body10"].ToString();
                            simsobj.sims_certificate_body11 = dr["sims_certificate_body11"].ToString();
                            simsobj.sims_certificate_body12 = dr["sims_certificate_body12"].ToString();
                            simsobj.sims_certificate_body13 = dr["sims_certificate_body13"].ToString();
                            simsobj.sims_certificate_body14 = dr["sims_certificate_body14"].ToString();
                            simsobj.sims_certificate_body15 = dr["sims_certificate_body15"].ToString();
                            simsobj.sims_certificate_footer1 = dr["sims_certificate_footer1"].ToString();
                            simsobj.sims_certificate_footer2 = dr["sims_certificate_footer2"].ToString();
                            simsobj.sims_certificate_footer3 = dr["sims_certificate_footer3"].ToString();
                            simsobj.sims_certificate_footer4 = dr["sims_certificate_footer4"].ToString();
                            simsobj.sims_certificate_footer5 = dr["sims_certificate_footer5"].ToString();
                            simsobj.sims_certificate_signatory1 = dr["sims_certificate_signatory1"].ToString();
                            simsobj.sims_certificate_signatory2 = dr["sims_certificate_signatory2"].ToString();
                            simsobj.sims_certificate_signatory3 = dr["sims_certificate_signatory3"].ToString();
                            simsobj.sims_certificate_signatory4 = dr["sims_certificate_signatory4"].ToString();
                            simsobj.sims_certificate_signatory5 = dr["sims_certificate_signatory5"].ToString();
                            simsobj.sims_certificate_watermark = dr["sims_certificate_watermark"].ToString();

                            if (dr["sims_certificate_status"].ToString().Equals("A"))
                                simsobj.sims_certificate_status = true;
                            else
                                simsobj.sims_certificate_status = false;

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);

            }
            return Request.CreateResponse(HttpStatusCode.OK, simsobj);
        }

        [Route("CUDCertificate")]
        public HttpResponseMessage CUDCertificate(List<Sims104> data)
        {
            bool insert = false;
            string status;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims104 simsobj in data)
                    {
                        if (simsobj.sims_certificate_status == true)
                            status = "A";
                        else
                            status = "I";


                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_certificate_proc]",
                            //int ins = db.ExecuteStoreProcedureforInsert("sims_certificate_issue",
                        new List<SqlParameter>()
                     {

                               
                            
                             new SqlParameter("@opr", simsobj.opr),
                             
                             new SqlParameter("@sub_opr", ""),
                             new SqlParameter("@sims_certificate_number", simsobj.sims_certificate_number),                            
                             new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                             new SqlParameter("@sims_certificate_name", simsobj.sims_certificate_name),
                             new SqlParameter("@sims_certificate_desc", simsobj.sims_certificate_desc),
                             new SqlParameter("@sims_certificate_count_flag", simsobj.sims_certificate_count_flag),
                             new SqlParameter("@sims_certificate_header1", simsobj.sims_certificate_header1),
                             new SqlParameter("@sims_certificate_header2", simsobj.sims_certificate_header2),
                             new SqlParameter("@sims_certificate_header3", simsobj.sims_certificate_header3),
                             new SqlParameter("@sims_certificate_header4", simsobj.sims_certificate_header4),
                             new SqlParameter("@sims_certificate_header5", simsobj.sims_certificate_header5),
                             new SqlParameter("@sims_certificate_body1", simsobj.sims_certificate_body1),
                             new SqlParameter("@sims_certificate_body2", simsobj.sims_certificate_body2),
                             new SqlParameter("@sims_certificate_body3", simsobj.sims_certificate_body3),
                             new SqlParameter("@sims_certificate_body4", simsobj.sims_certificate_body4),
                             new SqlParameter("@sims_certificate_body5", simsobj.sims_certificate_body5),
                             new SqlParameter("@sims_certificate_body6", simsobj.sims_certificate_body6),
                             new SqlParameter("@sims_certificate_body7", simsobj.sims_certificate_body7),
                             new SqlParameter("@sims_certificate_body8", simsobj.sims_certificate_body8),
                             new SqlParameter("@sims_certificate_body9", simsobj.sims_certificate_body9),
                             new SqlParameter("@sims_certificate_body10", simsobj.sims_certificate_body10),
                             new SqlParameter("@sims_certificate_body11", simsobj.sims_certificate_body11),
                             new SqlParameter("@sims_certificate_body12", simsobj.sims_certificate_body12),
                             new SqlParameter("@sims_certificate_body13", simsobj.sims_certificate_body13),
                             new SqlParameter("@sims_certificate_body14", simsobj.sims_certificate_body14),
                             new SqlParameter("@sims_certificate_body15", simsobj.sims_certificate_body15),
                             new SqlParameter("@sims_certificate_footer1", simsobj.sims_certificate_footer1),
                             new SqlParameter("@sims_certificate_footer2", simsobj.sims_certificate_footer2),
                             new SqlParameter("@sims_certificate_footer3", simsobj.sims_certificate_footer3),
                             new SqlParameter("@sims_certificate_footer4", simsobj.sims_certificate_footer4),
                             new SqlParameter("@sims_certificate_footer5", simsobj.sims_certificate_footer5),
                             new SqlParameter("@sims_certificate_watermark", simsobj.sims_certificate_watermark),
                             new SqlParameter("@sims_certificate_signatory1", simsobj.sims_certificate_signatory1),
                             new SqlParameter("@sims_certificate_signatory2", simsobj.sims_certificate_signatory2),
                             new SqlParameter("@sims_certificate_signatory3", simsobj.sims_certificate_signatory3),
                             new SqlParameter("@sims_certificate_signatory4", simsobj.sims_certificate_signatory4),
                             new SqlParameter("@sims_certificate_signatory5", simsobj.sims_certificate_signatory5),
                             new SqlParameter("@sims_certificate_status", status),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}