﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/ERP/SurveyDetails")]

    public class SurveyDetailsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSurveyType")]
        public HttpResponseMessage getSurveyType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/SurveyDetails"));

            List<Survey> lstSurvey = new List<Survey>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey sequence = new Survey();
                            sequence.survey_type_code = dr["sims_appl_parameter"].ToString();
                            sequence.survey_type_name = dr["sims_appl_form_field_value1"].ToString();
                            lstSurvey.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstSurvey);
        }

        [Route("getSurveyGroupType")]
        public HttpResponseMessage getSurveyGroupType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyGroup()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/SurveyDetails"));

            List<Survey> lstSurvey = new List<Survey>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'E'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey sequence = new Survey();
                            sequence.user_grp_code = dr["sims_appl_parameter"].ToString();
                            sequence.user_grp_name = dr["sims_appl_form_field_value1"].ToString();
                            lstSurvey.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstSurvey);
        }

        [Route("getGender")]
        public HttpResponseMessage getGender()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims010_Edit> lstgender = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit gen = new Sims010_Edit();
                            gen.gender_code = dr["sims_appl_parameter"].ToString();
                            gen.gender_desc = dr["sims_appl_form_field_value1"].ToString();
                            lstgender.Add(gen);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
           
            List<Sims024> lstgrades = new List<Sims024>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims024 cur = new Sims024();
                            cur.grade_academic_year = dr["sims_academic_year"].ToString();
                            cur.grade_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstgrades.Add(cur);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        }

        [Route("getSurveyDetails")]
        public HttpResponseMessage getSurveyDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyDetails()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Survey"));
            //int total = 0, skip = 0, cnt = 1;
            int cnt = 1;
            List<Survey> lstgrades = new List<Survey>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Survey sur = new Survey();
                            sur.myid = cnt;
                            sur.sims_survey_srl_no = int.Parse(dr["sims_survey_srl_no"].ToString());
                            sur.sims_survey_code = dr["sims_survey_code"].ToString();
                            sur.sims_survey_type = dr["sims_survey_type"].ToString();
                            sur.survey_type_name = dr["survey_type_name"].ToString();
                            sur.sims_survey_subject = dr["sims_survey_subject"].ToString();
                            sur.sims_survey_subject_ot = dr["sims_survey_subject_ot"].ToString();
                            sur.sims_survey_desc_en = dr["sims_survey_desc_en"].ToString();
                            sur.sims_survey_desc_ot = dr["sims_survey_desc_ot"].ToString();
                            sur.sims_survey_start_date = db.DBYYYYMMDDformat(dr["sims_survey_start_date"].ToString());
                            sur.sims_survey_end_date = db.DBYYYYMMDDformat(dr["sims_survey_end_date"].ToString());
                            sur.sims_survey_annonimity_flag = dr["sims_survey_annonimity_flag"].Equals("A") ? true : false;
                            sur.sims_survey_ppn_flag = dr["sims_survey_ppn_flag"].Equals("A") ? true : false;
                            sur.sims_survey_erp_flag = dr["sims_survey_erp_flag"].Equals("A") ? true : false;
                            sur.sims_survey_multiple_attempt_flag = dr["sims_survey_multiple_attempt_flag"].Equals("A") ? true : false;
                            sur.sims_survey_end_of_survey_remark_flag = dr["sims_survey_end_of_survey_remark_flag"].Equals("A") ? true : false;
                            sur.sims_survey_end_of_survey_suggestion_flag = dr["sims_survey_end_of_survey_suggestion_flag"].Equals("A") ? true : false;
                            sur.sims_survey_end_of_survey_rating_flag = dr["sims_survey_end_of_survey_rating_flag"].Equals("A") ? true : false;
                            sur.sims_survey_max_attempt_count = int.Parse(dr["sims_survey_max_attempt_count"].ToString());
                            sur.sims_survey_max_allowed_time_in_minute = int.Parse(dr["sims_survey_max_allowed_time_in_minute"].ToString());
                            sur.sims_survey_no_of_question_to_attempt = int.Parse(dr["sims_survey_no_of_question_to_attempt"].ToString());
                            sur.sims_survey_no_of_question_available = int.Parse(dr["sims_survey_no_of_question_available"].ToString());
                            sur.sims_survey_created_by_user =dr["sims_survey_created_by_user"].ToString();
                            sur.sims_survey_creation_time = dr["sims_survey_creation_time"].ToString();
                            //sur.sims_survey_status = dr["sims_survey_status"].Equals("A") ? true : false;
                            sur.sims_survey_status = dr["sims_survey_status"].ToString();
                            sur.sims_survey_status_desc = dr["sims_survey_status_desc"].ToString();
                            sur.sims_survey_dept_no = dr["sims_survey_dept_no"].ToString();
                            sur.sims_survey_user_group_code = dr["sims_survey_user_group_code"].ToString();
                            sur.sims_survey_grade_code = dr["sims_survey_grade_code"].ToString();
                            sur.sims_survey_section_code = dr["sims_survey_section_code"].ToString();
                            sur.sims_survey_acad_year = dr["sims_survey_acad_year"].ToString();
                            sur.sims_survey_gender_code = dr["sims_survey_gender_code"].ToString();
                            sur.sims_survey_gradesection_code = dr["sims_survey_gradesection_code"].ToString();
                            lstgrades.Add(sur);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        }


        [Route("getQuestionBankDetails")]
        public HttpResponseMessage getQuestionBankDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyDetails()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "STUDENT", "ERP/Survey"));
            //int total = 0, skip = 0, cnt = 1;
            int cnt = 1;
            List<QuestionBank> lstgrades = new List<QuestionBank>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_questionbank_details_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            QuestionBank sur = new QuestionBank();
                            sur.sims_questionbank_srl_no = int.Parse(dr["sims_questionbank_srl_no"].ToString());
                            sur.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            sur.sims_questionbank_type = dr["sims_questionbank_type"].ToString();
                            sur.sims_questionbank_type_name = dr["sims_questionbank_type_name"].ToString();
                            sur.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
                            sur.sims_questionbank_subject_ot = dr["sims_questionbank_subject_ot"].ToString();
                            sur.sims_questionbank_desc_en = dr["sims_questionbank_desc_en"].ToString();
                            sur.sims_questionbank_desc_ot = dr["sims_questionbank_desc_ot"].ToString();
                            sur.sims_questionbank_start_date = db.DBYYYYMMDDformat(dr["sims_questionbank_start_date"].ToString());
                            sur.sims_questionbank_end_date = db.DBYYYYMMDDformat(dr["sims_questionbank_end_date"].ToString());
                            sur.sims_questionbank_annonimity_flag = dr["sims_questionbank_annonimity_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_ppn_flag = dr["sims_questionbank_ppn_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_erp_flag = dr["sims_questionbank_erp_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_multiple_attempt_flag = dr["sims_questionbank_multiple_attempt_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_end_of_questionbank_remark_flag = dr["sims_questionbank_end_of_questionbank_remark_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_end_of_questionbank_suggestion_flag = dr["sims_questionbank_end_of_questionbank_suggestion_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_end_of_questionbank_rating_flag = dr["sims_questionbank_end_of_questionbank_rating_flag"].Equals("A") ? true : false;
                            sur.sims_questionbank_max_attempt_count = int.Parse(dr["sims_questionbank_max_attempt_count"].ToString());
                            sur.sims_questionbank_max_allowed_time_in_minute = int.Parse(dr["sims_questionbank_max_allowed_time_in_minute"].ToString());
                            sur.sims_questionbank_no_of_question_to_attempt = int.Parse(dr["sims_questionbank_no_of_question_to_attempt"].ToString());
                            sur.sims_questionbank_no_of_question_available = int.Parse(dr["sims_questionbank_no_of_question_available"].ToString());
                            sur.sims_questionbank_created_by_user = dr["sims_questionbank_created_by_user"].ToString();
                            sur.sims_questionbank_creation_time = dr["sims_questionbank_creation_time"].ToString();
                            //sur.sims_questionbank_status = dr["sims_questionbank_status"].Equals("A") ? true : false;
                            sur.sims_survey_status = dr["sims_questionbank_status"].ToString();
                            sur.sims_questionbank_status_desc = dr["sims_questionbank_status_desc"].ToString();
                            sur.sims_questionbank_dept_no = dr["sims_questionbank_dept_no"].ToString();
                            sur.sims_questionbank_user_group_code = dr["sims_questionbank_user_group_code"].ToString();
                            sur.sims_questionbank_grade_code = dr["sims_questionbank_grade_code"].ToString();
                            sur.sims_questionbank_section_code = dr["sims_questionbank_section_code"].ToString();
                            sur.sims_questionbank_acad_year = dr["sims_questionbank_acad_year"].ToString();
                            sur.sims_questionbank_gender_code = dr["sims_questionbank_gender_code"].ToString();
                            sur.sims_questionbank_gradesection_code = dr["sims_questionbank_gradesection_code"].ToString();
                            lstgrades.Add(sur);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        }

        //[Route("getSurveyAllDetails")]
        //public HttpResponseMessage getSurveyAllDetails(string survey_code)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyAllDetails()PARAMETERS ::NA";
        //    Log.Debug(string.Format(debug, "STUDENT", "ERP/Survey"));
        //    //int total = 0, skip = 0, cnt = 1;
        //    int cnt = 1;
        //   // List<Survey> lstgrades = new List<Survey>();
        //    Survey sur = new Survey();
        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                     new SqlParameter("@opr", "F"),
        //                     new SqlParameter("@sims_survey_code",survey_code)
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {


        //                    sur.surveyDept = new List<Survey_department>();
        //                    while (dr.Read())
        //                    {
        //                        Survey_department fd = new Survey_department();
        //                        fd.sims_survey_dept_no = dr["sims_survey_dept_no"].ToString();
        //                        fd.survey_dept_name = dr["survey_dept_name"].ToString();
        //                        sur.surveyDept.Add(fd);
        //                    }
        //                    dr.NextResult();

        //                    sur.surveyUserGrp = new List<Survey_userGroup>();
        //                    while (dr.Read())
        //                    {
        //                        Survey_userGroup fd1 = new Survey_userGroup();
        //                        fd1.sims_survey_user_group_code = dr["sims_survey_user_group_code"].ToString();
        //                        fd1.survey_user_name = dr["survey_user_name"].ToString();
        //                        sur.surveyUserGrp.Add(fd1);
        //                    }
        //                    dr.NextResult();

        //                    sur.surveyGrade = new List<Survey_grade>();
        //                    while (dr.Read())
        //                    {
        //                        Survey_grade fd2 = new Survey_grade();
        //                        fd2.sims_survey_grade_code = dr["sims_survey_grade_code"].ToString();
        //                        fd2.survey_grade_name = dr["survey_grade_name"].ToString();
        //                        sur.surveyGrade.Add(fd2);
        //                    }
        //                    dr.NextResult();

        //                    sur.surveySection = new List<Survey_section>();
        //                    while (dr.Read())
        //                    {
        //                        Survey_section fd3 = new Survey_section();
        //                        fd3.sims_survey_section_code = dr["sims_survey_section_code"].ToString();
        //                        fd3.survey_section_name = dr["survey_section_name"].ToString();
        //                        sur.surveySection.Add(fd3);
        //                    }
        //                    dr.NextResult();

        //                    //sur.sims_survey_user_group_code = dr["sims_survey_user_group_code"].ToString();
        //                    //sur.survey_user_name = dr["survey_user_name"].ToString();
        //                    //sur.sims_survey_dept_no = dr["sims_survey_dept_no"].ToString();
        //                    //sur.survey_dept_name = dr["survey_dept_name"].ToString();
        //                    //sur.sims_survey_acad_year = dr["sims_survey_acad_year"].ToString();

        //                    //sur.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
        //                    //sur.sims_survey_grade_code = dr["sims_survey_grade_code"].ToString();
        //                    //sur.survey_grade_name = dr["survey_grade_name"].ToString();

        //                    //sur.sims_survey_section_code = dr["sims_survey_section_code"].ToString();
        //                    //sur.survey_section_name = dr["survey_section_name"].ToString();

        //                    //sur.sims_survey_gender_code = dr["sims_survey_gender_code"].ToString();
        //                    //sur.survey_gender_name = dr["survey_gender_name"].ToString();

        //                    //sur.sims_survey_age_group_code = dr["sims_survey_age_group_code"].ToString();


        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, sur);
        //            }
        //            else
        //                return Request.CreateResponse(HttpStatusCode.OK, sur);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, sur);
        //    }
        //    //return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        //}

        [Route("CUDInsertServiceDetails")]
        public HttpResponseMessage CUDInsertServiceDetails(List<Survey> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Survey simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                           // new SqlParameter("@sims_survey_code", simsobj.grade_cur_code),
                            new SqlParameter("@sims_survey_type", simsobj.sims_survey_type),
                            new SqlParameter("@sims_survey_subject", simsobj.sims_survey_subject),
                            new SqlParameter("@sims_survey_subject_ot", simsobj.sims_survey_subject_ot),
                            new SqlParameter("@sims_survey_desc_en", simsobj.sims_survey_desc_en),
                            new SqlParameter("@sims_survey_desc_ot", simsobj.sims_survey_desc_ot),
                            new SqlParameter("@sims_survey_start_date", db.DBYYYYMMDDformat(simsobj.sims_survey_start_date)),
                            new SqlParameter("@sims_survey_end_date",db.DBYYYYMMDDformat(simsobj.sims_survey_end_date)),
                            new SqlParameter("@sims_survey_annonimity_flag",simsobj.sims_survey_annonimity_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_ppn_flag",simsobj.sims_survey_ppn_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_erp_flag",simsobj.sims_survey_erp_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_multiple_attempt_flag",simsobj.sims_survey_multiple_attempt_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_remark_flag",simsobj.sims_survey_end_of_survey_remark_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_suggestion_flag",simsobj.sims_survey_end_of_survey_suggestion_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_rating_flag",simsobj.sims_survey_end_of_survey_rating_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_max_attempt_count",simsobj.sims_survey_max_attempt_count),
                            new SqlParameter("@sims_survey_max_allowed_time_in_minute",simsobj.sims_survey_max_allowed_time_in_minute),
                            new SqlParameter("@sims_survey_no_of_question_to_attempt",simsobj.sims_survey_no_of_question_to_attempt),
                            new SqlParameter("@sims_survey_no_of_question_available",simsobj.sims_survey_no_of_question_available),

                            new SqlParameter("@sims_survey_created_by_user",simsobj.sims_survey_created_by_user),
                          //  new SqlParameter("@sims_survey_creation_time",simsobj.sims_survey_creation_time),

                            //new SqlParameter("@sims_survey_status",simsobj.sims_survey_status.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_user_group_code",simsobj.sims_survey_user_group_code),
                            new SqlParameter("@sims_survey_dept_no",simsobj.sims_survey_dept_no),
                            new SqlParameter("@sims_survey_acad_year",simsobj.sims_survey_acad_year),
                            new SqlParameter("@sims_survey_grade_code",simsobj.sims_survey_grade_code),
                            new SqlParameter("@sims_survey_section_code",simsobj.sims_survey_section_code),
                            new SqlParameter("@sims_survey_gender_code",simsobj.sims_survey_gender_code),
                            new SqlParameter("@sims_survey_age_group_code",simsobj.sims_survey_age_group_code),

                            
                            

                         });
                            while (dr.Read())
                            {
                                string cnt = dr["service_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDInsertBankDetails")]
        public HttpResponseMessage CUDInsertBankDetails(List<QuestionBank> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (QuestionBank simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_questionbank_details_proc]",
                                new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", simsobj.opr),
                           // new SqlParameter("@sims_questionbank_code", simsobj.grade_cur_code),
                            new SqlParameter("@sims_questionbank_type", simsobj.sims_questionbank_type),
                            new SqlParameter("@sims_questionbank_subject", simsobj.sims_questionbank_subject),
                            new SqlParameter("@sims_questionbank_subject_ot", simsobj.sims_questionbank_subject_ot),
                            new SqlParameter("@sims_questionbank_desc_en", simsobj.sims_questionbank_desc_en),
                            new SqlParameter("@sims_questionbank_desc_ot", simsobj.sims_questionbank_desc_ot),
                            new SqlParameter("@sims_questionbank_start_date", db.DBYYYYMMDDformat(simsobj.sims_questionbank_start_date)),
                            new SqlParameter("@sims_questionbank_end_date",db.DBYYYYMMDDformat(simsobj.sims_questionbank_end_date)),
                            new SqlParameter("@sims_questionbank_annonimity_flag",simsobj.sims_questionbank_annonimity_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_ppn_flag",simsobj.sims_questionbank_ppn_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_erp_flag",simsobj.sims_questionbank_erp_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_multiple_attempt_flag",simsobj.sims_questionbank_multiple_attempt_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_remark_flag",simsobj.sims_questionbank_end_of_questionbank_remark_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_suggestion_flag",simsobj.sims_questionbank_end_of_questionbank_suggestion_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_rating_flag",simsobj.sims_questionbank_end_of_questionbank_rating_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_max_attempt_count",simsobj.sims_questionbank_max_attempt_count),
                            new SqlParameter("@sims_questionbank_max_allowed_time_in_minute",simsobj.sims_questionbank_max_allowed_time_in_minute),
                            new SqlParameter("@sims_questionbank_no_of_question_to_attempt",simsobj.sims_questionbank_no_of_question_to_attempt),
                            new SqlParameter("@sims_questionbank_no_of_question_available",simsobj.sims_questionbank_no_of_question_available),

                            new SqlParameter("@sims_questionbank_created_by_user",simsobj.sims_questionbank_created_by_user),
                          //  new SqlParameter("@sims_questionbank_creation_time",simsobj.sims_questionbank_creation_time),

                            //new SqlParameter("@sims_questionbank_status",simsobj.sims_questionbank_status.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_user_group_code",simsobj.sims_survey_user_group_code),
                            new SqlParameter("@sims_survey_dept_no",simsobj.sims_survey_dept_no),
                            new SqlParameter("@sims_survey_acad_year",simsobj.sims_survey_acad_year),
                            new SqlParameter("@sims_survey_grade_code",simsobj.sims_survey_grade_code),
                            new SqlParameter("@sims_survey_section_code",simsobj.sims_survey_section_code),
                            new SqlParameter("@sims_survey_gender_code",simsobj.sims_survey_gender_code),
                            new SqlParameter("@sims_survey_age_group_code",simsobj.sims_survey_age_group_code),




                         });
                            while (dr.Read())
                            {
                                string cnt = dr["service_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CUDSurveyDetailsUpdate")]
        public HttpResponseMessage CUDSurveyDetailsUpdate(List<Survey> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Survey simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_survey_details_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_survey_code", simsobj.sims_survey_code),
                           // new SqlParameter("@sims_survey_srl_no",simsobj.sims_survey_srl_no),
                            new SqlParameter("@sims_survey_type", simsobj.sims_survey_type),
                            new SqlParameter("@sims_survey_subject", simsobj.sims_survey_subject),
                            new SqlParameter("@sims_survey_subject_ot", simsobj.sims_survey_subject_ot),
                            new SqlParameter("@sims_survey_desc_en", simsobj.sims_survey_desc_en),
                            new SqlParameter("@sims_survey_desc_ot", simsobj.sims_survey_desc_ot),
                            new SqlParameter("@sims_survey_start_date", db.DBYYYYMMDDformat(simsobj.sims_survey_start_date)),
                            new SqlParameter("@sims_survey_end_date",db.DBYYYYMMDDformat(simsobj.sims_survey_end_date)),
                            new SqlParameter("@sims_survey_annonimity_flag",simsobj.sims_survey_annonimity_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_ppn_flag",simsobj.sims_survey_ppn_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_erp_flag",simsobj.sims_survey_erp_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_multiple_attempt_flag",simsobj.sims_survey_multiple_attempt_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_remark_flag",simsobj.sims_survey_end_of_survey_remark_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_suggestion_flag",simsobj.sims_survey_end_of_survey_suggestion_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_end_of_survey_rating_flag",simsobj.sims_survey_end_of_survey_rating_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_max_attempt_count",simsobj.sims_survey_max_attempt_count),
                            new SqlParameter("@sims_survey_max_allowed_time_in_minute",simsobj.sims_survey_max_allowed_time_in_minute),
                            new SqlParameter("@sims_survey_no_of_question_to_attempt",simsobj.sims_survey_no_of_question_to_attempt),
                            new SqlParameter("@sims_survey_no_of_question_available",simsobj.sims_survey_no_of_question_available),
                            new SqlParameter("@sims_survey_created_by_user",simsobj.sims_survey_created_by_user),
                           // new SqlParameter("@sims_survey_status",simsobj.sims_survey_status.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_user_group_code",simsobj.sims_survey_user_group_code),
                            new SqlParameter("@sims_survey_dept_no",simsobj.sims_survey_dept_no),
                            new SqlParameter("@sims_survey_acad_year",simsobj.sims_survey_acad_year),
                            new SqlParameter("@sims_survey_grade_code",simsobj.sims_survey_grade_code),
                            new SqlParameter("@sims_survey_section_code",simsobj.sims_survey_section_code),
                            new SqlParameter("@sims_survey_gender_code",simsobj.sims_survey_gender_code),
                            new SqlParameter("@sims_survey_age_group_code",simsobj.sims_survey_age_group_code),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDQuestionBankDetailsUpdate")]
        public HttpResponseMessage CUDQuestionBankDetailsUpdate(List<QuestionBank> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (QuestionBank simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_questionbank_details_proc]",
                                new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_questionbank_code", simsobj.sims_questionbank_code),
                           // new SqlParameter("@sims_questionbank_srl_no",simsobj.sims_questionbank_srl_no),
                            new SqlParameter("@sims_questionbank_type", simsobj.sims_questionbank_type),
                            new SqlParameter("@sims_questionbank_subject", simsobj.sims_questionbank_subject),
                            new SqlParameter("@sims_questionbank_subject_ot", simsobj.sims_questionbank_subject_ot),
                            new SqlParameter("@sims_questionbank_desc_en", simsobj.sims_questionbank_desc_en),
                            new SqlParameter("@sims_questionbank_desc_ot", simsobj.sims_questionbank_desc_ot),
                            new SqlParameter("@sims_questionbank_start_date", db.DBYYYYMMDDformat(simsobj.sims_questionbank_start_date)),
                            new SqlParameter("@sims_questionbank_end_date",db.DBYYYYMMDDformat(simsobj.sims_questionbank_end_date)),
                            new SqlParameter("@sims_questionbank_annonimity_flag",simsobj.sims_questionbank_annonimity_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_ppn_flag",simsobj.sims_questionbank_ppn_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_erp_flag",simsobj.sims_questionbank_erp_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_multiple_attempt_flag",simsobj.sims_questionbank_multiple_attempt_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_remark_flag",simsobj.sims_questionbank_end_of_questionbank_remark_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_suggestion_flag",simsobj.sims_questionbank_end_of_questionbank_suggestion_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_end_of_questionbank_rating_flag",simsobj.sims_questionbank_end_of_questionbank_rating_flag.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_questionbank_max_attempt_count",simsobj.sims_questionbank_max_attempt_count),
                            new SqlParameter("@sims_questionbank_max_allowed_time_in_minute",simsobj.sims_questionbank_max_allowed_time_in_minute),
                            new SqlParameter("@sims_questionbank_no_of_question_to_attempt",simsobj.sims_questionbank_no_of_question_to_attempt),
                            new SqlParameter("@sims_questionbank_no_of_question_available",simsobj.sims_questionbank_no_of_question_available),
                            new SqlParameter("@sims_questionbank_created_by_user",simsobj.sims_questionbank_created_by_user),
                           // new SqlParameter("@sims_survey_status",simsobj.sims_survey_status.Equals(true)?"A":"I"),
                            new SqlParameter("@sims_survey_user_group_code",simsobj.sims_survey_user_group_code),
                            new SqlParameter("@sims_survey_dept_no",simsobj.sims_survey_dept_no),
                            new SqlParameter("@sims_survey_acad_year",simsobj.sims_survey_acad_year),
                            new SqlParameter("@sims_survey_grade_code",simsobj.sims_survey_grade_code),
                            new SqlParameter("@sims_survey_section_code",simsobj.sims_survey_section_code),
                            new SqlParameter("@sims_survey_gender_code",simsobj.sims_survey_gender_code),
                            new SqlParameter("@sims_survey_age_group_code",simsobj.sims_survey_age_group_code),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Get_Grade_CodebyCuriculum")]
        public HttpResponseMessage Get_Grade_CodebyCuriculum(string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Grade_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Grade_Section_Code"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_survey_acad_year", acad_yr)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("Get_Section_CodebyCuriculum")]
        public HttpResponseMessage Get_Section_CodebyCuriculum(string acad_yr, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Section_CodebyCuriculum"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@sims_survey_acad_year", acad_yr),
                             new SqlParameter("@IGNORED_SECTION_LIST", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            simsobj.grade_section_name = dr["section"].ToString();
                            simsobj.grade_section_code = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("updateSurveyStatus")]
        public HttpResponseMessage updateSurveyStatus(string survey_code, string sims_survey_status)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (survey_code != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_survey_details_proc]",
                            new List<SqlParameter>() 
                         {                             
                                    new SqlParameter("@opr", 'H'),
                                    new SqlParameter("@sims_survey_code",survey_code),
                                    new SqlParameter("@sims_survey_status",sims_survey_status),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("updateQuestionBankStatus")]
        public HttpResponseMessage updateQuestionBankStatus(string survey_code, string sims_survey_status)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (survey_code != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_questionbank_details_proc]",
                            new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", 'H'),
                                    new SqlParameter("@sims_questionbank_code",survey_code),
                                    new SqlParameter("@sims_questionbank_status",sims_survey_status),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getSurveyStatus")]
        public HttpResponseMessage getSurveyStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyStatus()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims010_Edit> lstgender = new List<Sims010_Edit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_details_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Edit gen = new Sims010_Edit();
                            gen.survey_status_code = dr["sims_appl_parameter"].ToString();
                            gen.survey_status = dr["sims_appl_form_field_value1"].ToString();
                            lstgender.Add(gen);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }
   
    }
}