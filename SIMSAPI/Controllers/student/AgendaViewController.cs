﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.AgendaViewController
{
    [RoutePrefix("api/Agenda")]
    [BasicAuthentication]
    public class AgendaViewController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year, string emp_login_code)
        {

            List<Sim539> mod_list = new List<Sim539>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'M'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_acaedmic_year", academic_year),
                            new SqlParameter("@emp_login_code",emp_login_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim539 simsobj = new Sim539();
                            simsobj.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year, string emp_login_code)
        {

            List<Sim539> mod_list = new List<Sim539>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'N'),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_acaedmic_year", academic_year),
                             new SqlParameter("@sims_grade_code", grade_code),
                              new SqlParameter("@emp_login_code",emp_login_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim539 simsobj = new Sim539();
                            simsobj.sims_section_code = dr["sims_bell_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getSubject")]
        public HttpResponseMessage getSubject()
        {

            List<Sim539> nation_list = new List<Sim539>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),    
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim539 obj = new Sim539();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            nation_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, nation_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, nation_list);
        }


        [Route("getAll_Agenda_Details")]
        public HttpResponseMessage getAll_Agenda_Details()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim539> agenda_list = new List<Sim539>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr", 'S'),   
                                  
                                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim539 simsobj = new Sim539();
                            simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                            simsobj.sims_agenda_date = db.UIDDMMYYYYformat(dr["sims_agenda_date"].ToString());
                            simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                            simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                            simsobj.sims_agenda_doc = dr["sims_agenda_doc_name"].ToString();
                            simsobj.sims_agenda_visible_to_parent_portal = dr["sims_agenda_visible_to_parent_portal"].ToString();
                            simsobj.sims_agenda_status = dr["sims_agenda_status"].ToString();
                            agenda_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, agenda_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, agenda_list);
        }

        [Route("getAgendaDetails")]
        public HttpResponseMessage getAgendaDetails(string sims_acaedmic_year, string sdate, string edate, string sims_grade_code, string sims_section_code, string sims_subject_code, string em_login_code)
        {

            if (sims_subject_code == "undefined")
            {
                sims_subject_code = null;
            }
            if (sims_grade_code == "undefined")
            {
                sims_grade_code = null;
            }
            if (sims_section_code == "undefined")
            {
                sims_section_code = null;
            }
            if (em_login_code == "undefined")
            {
                em_login_code = null;
            }
            List<Sim539> agenda_list = new List<Sim539>();
            List<AgendaDate> op = new List<AgendaDate>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),   
                            new SqlParameter("@sims_acaedmic_year",sims_acaedmic_year),
                             new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_subject_code",sims_subject_code),
                            new SqlParameter("@sdate",db.DBYYYYMMDDformat(sdate)),
                            new SqlParameter("@edate",db.DBYYYYMMDDformat(edate)),
                            new SqlParameter("@emp_login_code",em_login_code)
                         });
                    while (dr.Read())
                    {
                        Sim539 simsobj = new Sim539();
                        simsobj.sims_isexpanded = "None";
                        simsobj.sims_icon = "fa fa-plus-circle";
                        simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.sims_grade = dr["sims_grade_name"].ToString();
                        simsobj.sims_section_code = dr["sims_section_code"].ToString();
                        simsobj.sims_section = dr["sims_section_name"].ToString();
                        simsobj.sims_subject_name = dr["subject"].ToString();
                        simsobj.sims_agenda_date = db.UIDDMMYYYYformat(dr["sims_agenda_date"].ToString());
                        simsobj.sims_agenda_day = dr["Day"].ToString();
                        //simsobj.sims_agenda_sdate = dr["sims_agenda_sdate"].ToString();
                        //simsobj.sims_agenda_edate = dr["sims_agenda_edate"].ToString();
                        simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                        simsobj.sims_agenda_doc = dr["sims_agenda_doc_name"].ToString();
                        simsobj.sims_agenda_doc_line_no = dr["sims_agenda_doc_line_no"].ToString();
                        simsobj.sims_agenda_doc_en = dr["sims_agenda_doc_name_en"].ToString();

                        try
                        {
                            simsobj.sims_agenda_teacher_code = dr["sims_agenda_teacher_code"].ToString();

                        }
                        catch (Exception e)
                        {
                        }
                        agenda_list.Add(simsobj);

                    }
                    foreach (var item in agenda_list)
                    {
                        AgendaDate a = new AgendaDate();
                        a.sims_agenda_date = item.sims_agenda_date;
                        a.sims_agenda_teacher_code = item.sims_agenda_teacher_code;
                        a.sims_agenda_list = new List<Agenda>();
                        var v = from p in op where p.sims_agenda_date == a.sims_agenda_date select p;
                        if (v.Count() == 0)
                        {
                            a.sims_agenda_list = getAgenda(agenda_list, a);
                            op.Add(a);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, op);

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        private List<Agenda> getAgenda(List<Sim539> modList, AgendaDate ad)
        {
            List<Agenda> op = new List<Agenda>();
            foreach (var item in modList)
            {
                if (item.sims_agenda_date == ad.sims_agenda_date)
                {
                    Agenda a = new Agenda();
                    a.sims_agenda_date = item.sims_agenda_date;
                    a.sims_agenda_desc = item.sims_agenda_desc;
                    a.sims_agenda_edate = item.sims_agenda_edate;
                    a.sims_agenda_name = item.sims_agenda_name;
                    a.sims_agenda_number = item.sims_agenda_number;
                    a.sims_grade_code = item.sims_grade_code;
                    a.sims_section_code = item.sims_section_code;
                    a.sims_grade = item.sims_grade;
                    a.sims_section = item.sims_section;

                    a.sims_agenda_stage = item.sims_agenda_stage;
                    a.sims_agenda_page_no = item.sims_agenda_page_no;
                    a.sims_agenda_teacher_code = item.sims_agenda_teacher_code;
                    a.sims_agenda_from_time = item.sims_agenda_from_time;
                    a.sims_agenda_to_time = item.sims_agenda_to_time;
                    a.sims_agenda_sub_list = new List<AgendaSubject>();
                    var v = from p in op where p.sims_agenda_number == a.sims_agenda_number && p.sims_agenda_date == a.sims_agenda_date select p;
                    if (v.Count() == 0)
                    {
                        a.sims_agenda_sub_list = getAgendaSubjects(modList, a);
                        op.Add(a);
                    }
                }
            }
            return op;
        }

        private List<AgendaSubject> getAgendaSubjects(List<Sim539> modList, Agenda a)
        {
            List<AgendaSubject> op = new List<AgendaSubject>();
            foreach (var item in modList)
            {
                if (item.sims_agenda_date == a.sims_agenda_date && item.sims_agenda_number == a.sims_agenda_number)
                {
                    AgendaSubject asb = new AgendaSubject();
                    asb.sims_agenda_date = item.sims_agenda_date;
                    asb.sims_agenda_number = item.sims_agenda_number;
                    asb.sims_subject_code = item.sims_subject_code;
                    asb.sims_subject_name = item.sims_subject_name;
                    asb.sims_agenda_doc_list = new List<AgendaDoc>();
                    var v = from p in op where p.sims_agenda_number == asb.sims_agenda_number && p.sims_agenda_date == asb.sims_agenda_date && p.sims_subject_code == asb.sims_subject_code select p;
                    if (v.Count() == 0)
                    {
                        asb.sims_agenda_doc_list = getAgendaDocs(modList, asb);
                        op.Add(asb);
                    }
                }
            }
            return op;
        }

        private List<AgendaDoc> getAgendaDocs(List<Sim539> modList, AgendaSubject a)
        {
            List<AgendaDoc> op = new List<AgendaDoc>();
            foreach (var item in modList)
            {
                if (item.sims_agenda_date == a.sims_agenda_date && item.sims_agenda_number == a.sims_agenda_number && item.sims_subject_code == a.sims_subject_code)
                {
                    AgendaDoc asb = new AgendaDoc();
                    asb.sims_agenda_date = item.sims_agenda_date;
                    asb.sims_agenda_number = item.sims_agenda_number;
                    asb.sims_subject_code = item.sims_subject_code;
                    asb.sims_agenda_doc = item.sims_agenda_doc;
                    asb.sims_agenda_doc_line_no = item.sims_agenda_doc_line_no;
                    asb.sims_agenda_doc_en = item.sims_agenda_doc_en;
                    op.Add(asb);
                }
            }
            return op;
        }


        [Route("getAgendaDesc")]
        public HttpResponseMessage getAgendaDesc(string sims_agenda_number, string sims_agenda_name)
        {

            List<Sim539> agenda_list = new List<Sim539>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr", 'Z'), 
                                    new SqlParameter("@sims_agenda_number",sims_agenda_number),   
                                    new SqlParameter("@sims_agenda_name",sims_agenda_name),   
                                  
                                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim539 simsobj = new Sim539();
                            simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                            simsobj.sims_agenda_date = db.UIDDMMYYYYformat(dr["sims_agenda_date"].ToString());
                            simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                            simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();
                            agenda_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, agenda_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, agenda_list);
        }


        [Route("getAgendaDetailsGeis")]
        public HttpResponseMessage getAgendaDetailsGeis(string sims_acaedmic_year, string sdate, string edate, string sims_grade_code, string sims_section_code, string sims_subject_code, string em_login_code)
        {

            if (sims_subject_code == "undefined")
            {
                sims_subject_code = null;
            }
            if (sims_grade_code == "undefined")
            {
                sims_grade_code = null;
            }
            if (sims_section_code == "undefined")
            {
                sims_section_code = null;
            }
            if (em_login_code == "undefined")
            {
                em_login_code = null;
            }
            List<Sim539> agenda_list = new List<Sim539>();
            List<AgendaDate> op = new List<AgendaDate>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),   
                            new SqlParameter("@sims_acaedmic_year",sims_acaedmic_year),
                             new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_subject_code",sims_subject_code),
                            new SqlParameter("@sdate",db.DBYYYYMMDDformat(sdate)),
                            new SqlParameter("@edate",db.DBYYYYMMDDformat(edate)),
                            new SqlParameter("@emp_login_code",em_login_code)
                         });
                    while (dr.Read())
                    {
                        Sim539 simsobj = new Sim539();
                        simsobj.sims_isexpanded = "None";
                        simsobj.sims_icon = "fa fa-plus-circle";
                        simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.sims_grade = dr["sims_grade_name"].ToString();
                        simsobj.sims_section_code = dr["sims_section_code"].ToString();
                        simsobj.sims_section = dr["sims_section_name"].ToString();
                        simsobj.sims_subject_name = dr["subject"].ToString();
                        simsobj.sims_agenda_date = db.UIDDMMYYYYformat(dr["sims_agenda_date"].ToString());
                        simsobj.sims_agenda_day = dr["Day"].ToString();
                        simsobj.sims_agenda_stage = dr["sims_agenda_stage"].ToString();
                        simsobj.sims_agenda_page_no = dr["sims_agenda_page_no"].ToString();
                        simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();

                        simsobj.sims_agenda_from_time = dr["sims_agenda_from_time"].ToString();
                        simsobj.sims_agenda_to_time = dr["sims_agenda_to_time"].ToString();

                        simsobj.sims_agenda_doc = dr["sims_agenda_doc_name"].ToString();
                        simsobj.sims_agenda_doc_line_no = dr["sims_agenda_doc_line_no"].ToString();
                        simsobj.sims_agenda_doc_en = dr["sims_agenda_doc_name_en"].ToString();

                        agenda_list.Add(simsobj);

                    }
                    foreach (var item in agenda_list)
                    {
                        AgendaDate a = new AgendaDate();
                        a.sims_agenda_date = item.sims_agenda_date;
                        a.sims_agenda_list = new List<Agenda>();
                        var v = from p in op where p.sims_agenda_date == a.sims_agenda_date select p;
                        if (v.Count() == 0)
                        {
                            a.sims_agenda_list = getAgenda(agenda_list, a);
                            op.Add(a);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, op);

            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        [Route("getAgendaDetailsGeisNew")]
        public HttpResponseMessage getAgendaDetailsGeisNew(string sims_acaedmic_year, string sdate, string edate, string sims_grade_code, string sims_section_code, string sims_subject_code, string em_login_code, string sims_cur_code)
        {

            if (sims_subject_code == "undefined")
            {
                sims_subject_code = null;
            }
            if (sims_grade_code == "undefined")
            {
                sims_grade_code = null;
            }
            if (sims_section_code == "undefined")
            {
                sims_section_code = null;
            }
            if (em_login_code == "undefined")
            {
                em_login_code = null;
            }
            List<Sim539> agenda_list = new List<Sim539>();
            //List<AgendaDate> op = new List<AgendaDate>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),   
                            new SqlParameter("@sims_acaedmic_year",sims_acaedmic_year),
                             new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_subject_code",sims_subject_code),
                            new SqlParameter("@sdate",db.DBYYYYMMDDformat(sdate)),
                            new SqlParameter("@edate",db.DBYYYYMMDDformat(edate)),
                            new SqlParameter("@emp_login_code",em_login_code),
                             new SqlParameter("@sims_cur_code",sims_cur_code)
                         });
                    while (dr.Read())
                    {
                        Sim539 simsobj = new Sim539();
                        simsobj.sims_isexpanded = "None";
                        simsobj.sims_icon = "fa fa-plus-circle";
                        simsobj.sims_agenda_number = dr["sims_agenda_number"].ToString();
                        simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                        simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.sims_grade = dr["sims_grade_name"].ToString();
                        simsobj.sims_section_code = dr["sims_section_code"].ToString();
                        simsobj.sims_section = dr["sims_section_name"].ToString();
                        simsobj.sims_subject_name = dr["subject"].ToString();
                        simsobj.sims_agenda_date = dr["sims_agenda_date"].ToString();
                        simsobj.sims_agenda_day = dr["Day"].ToString();
                        simsobj.sims_agenda_stage = dr["sims_agenda_stage"].ToString();
                        simsobj.sims_agenda_page_no = dr["sims_agenda_page_no"].ToString();
                        simsobj.sims_agenda_name = dr["sims_agenda_name"].ToString();
                        simsobj.sims_agenda_desc = dr["sims_agenda_desc"].ToString();

                        simsobj.sims_agenda_from_time = dr["sims_agenda_from_time"].ToString();
                        simsobj.sims_agenda_to_time = dr["sims_agenda_to_time"].ToString();

                        simsobj.sims_agenda_doc = dr["sims_agenda_doc_name"].ToString();
                        simsobj.sims_agenda_doc_line_no = dr["sims_agenda_doc_line_no"].ToString();
                        simsobj.sims_agenda_doc_en = dr["sims_agenda_doc_name_en"].ToString();
                        simsobj.sims_agenda_date = dr["sims_agenda_date"].ToString();

                        simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();

                        agenda_list.Add(simsobj);

                    }
                    
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, agenda_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, agenda_list);
        }

        [Route("getAgendaDocDetails")]
        public HttpResponseMessage getAgendaDocDetails()
        {
            List<Sims104> mod_list = new List<Sims104>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_agenda_view_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                               
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims104 simsobj = new Sims104();

                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);
                        }

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

    }
}




