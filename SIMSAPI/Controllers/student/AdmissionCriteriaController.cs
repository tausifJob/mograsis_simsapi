﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/AdmissionCriteria")]

    public class AdmissionCriteriaController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getAutoCriteriaCode")]
        public HttpResponseMessage getAutoCriteriaCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoCriteriaCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoCriteriaCode"));

            List<Sims012> code_list = new List<Sims012>();
            Sims012 obj = new Sims012();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_criteria_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", 'B')
                         });
                    // if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (dr["max"].ToString() != "")
                                obj.sims_criteria_code = int.Parse(dr["max"].ToString());
                            else
                                obj.sims_criteria_code = 1;
                            //code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj.sims_criteria_code);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, obj.sims_criteria_code);
            }
        }

        [Route("getCriteriaType")]
        public HttpResponseMessage getCriteriaType()
        {
            List<Sims012> criteria = new List<Sims012>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_criteria_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims012 crit = new Sims012();
                            crit.sims_criteria_type = dr["sims_appl_parameter"].ToString();
                            crit.sims_criteria_type_name = dr["sims_appl_form_field_value1"].ToString();
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

        [Route("getAdmissionCriteria")]
        public HttpResponseMessage getAdmissionCriteria()
        {
            List<Sims012> criteria = new List<Sims012>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_criteria_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims012 crit = new Sims012();
                            crit.sims_criteria_code = int.Parse(dr["sims_criteria_code"].ToString());
                            crit.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            crit.sims_criteria_name_ot = dr["sims_criteria_name_ot"].ToString();
                            if (dr["sims_criteria_mandatory"].ToString().Equals("Y"))
                                crit.sims_criteria_mandatory = true;
                            else
                                crit.sims_criteria_mandatory = false;
                            crit.sims_criteria_type = dr["sims_criteria_type"].ToString();
                            crit.sims_criteria_type_name = dr["sim_criteria_type_name"].ToString();
                            if (dr["sims_criteria_status"].ToString().Equals("A"))
                                crit.sims_criteria_status = true;
                            else
                                crit.sims_criteria_status = false;
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

        [Route("CUDAdmissionCriteria")]
        public HttpResponseMessage CUDAdmissionCriteria(List<Sims012> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDAdmissionCriteria()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/STUDENT/", "CUDAdmissionCriteria"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims012 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_criteria_proc]",
                                 new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_criteria_code", simsobj.sims_criteria_code),
                            new SqlParameter("@sims_criteria_name_en", simsobj.sims_criteria_name_en),
                            new SqlParameter("@sims_criteria_name_ot", simsobj.sims_criteria_name_ot),
                            new SqlParameter("@sims_criteria_mandatory", simsobj.sims_criteria_mandatory==true?"Y":"N"),
                            new SqlParameter("@sims_criteria_type", simsobj.sims_criteria_type),
                            new SqlParameter("@sims_criteria_status", simsobj.sims_criteria_status==true?"A":"I"),

                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


    }
}