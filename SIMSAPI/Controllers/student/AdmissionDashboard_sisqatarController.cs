﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/AdmissionDashboard_sisqatar")]

    public class AdmissionDashboard_sisqatarController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetStudents")]
        public HttpResponseMessage GetStudents(string curr_code, string AcadmicYear, string gradeCode, string admission_status)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                //if (admission_status="null")
                //{
                //    admission_status=null;
                //}
                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                    str = "G";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            //new SqlParameter("@sims_admission_employee_code",employee_code),
                            //new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            //new SqlParameter("@sims_admission_nationality_code",nationality_code),
                           // new SqlParameter("@sims_admission_gender",gender_code)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());
                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("GetStudents1")]
        public HttpResponseMessage GetStudents1(string curr_code, string AcadmicYear, string gradeCode, string admission_status, string gender_code, string nationality_code, string employee_code, string sibling_enroll,string adm_no, string adm_name)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", curr_code, AcadmicYear, gradeCode, admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (adm_no == "undefined" || adm_no == "null")
                {
                    adm_no = null;
                }
                if (adm_name == "undefined" || adm_name == "null")
                {
                    adm_name = null;
                }
                if (gender_code == "null")
                {
                    gender_code = null;
                }
                if (nationality_code == "null")
                {
                    nationality_code = null;
                }
                if (sibling_enroll == "null" || sibling_enroll == "undefined,")
                {
                    sibling_enroll = null;
                }
                if (employee_code == "null" || employee_code == "undefined," )
                {
                    employee_code = null;
                }
                if (admission_status == "null")
                {
                    admission_status = null;
                }

                if (string.IsNullOrEmpty(curr_code) || string.IsNullOrEmpty(AcadmicYear) || string.IsNullOrEmpty(gradeCode))
                    str = "G";
                else if (!string.IsNullOrEmpty(sibling_enroll))
                    str = "G1";
                else
                    str = "S";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@ADMISSION_NUM", admission_status),
                            new SqlParameter("@sims_admission_employee_code",employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",nationality_code),
                            new SqlParameter("@sims_admission_gender",gender_code),
                             new SqlParameter("@adm_nos",adm_no),
                            new SqlParameter("@First_name",adm_name),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            }
                            catch (Exception rx) { }

                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            //simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }
                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";


                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        
        }

        [Route("waitingPaidStudents")]
        public HttpResponseMessage waitingPaidStudents(param p)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", p.curr_code, p.AcadmicYear, p.gradeCode, p.admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (p.gradeCode == null)
                {
                    p.gradeCode = "undefined";
                }
                if (p.gender_code == "null")
                {
                    p.gender_code = null;
                }
                if (p.nationality_code == "null")
                {
                    p.nationality_code = null;
                }
                if (p.sibling_enroll == "null")
                {
                    p.sibling_enroll = null;
                }
                if (p.employee_code == "null")
                {
                    p.employee_code = null;
                }
                if (p.adm_no == "null")
                {
                    p.adm_no = null;
                }
                if (p.adm_name == "null")
                {
                    p.adm_name = null;
                }

                if (string.IsNullOrEmpty(p.curr_code) || string.IsNullOrEmpty(p.AcadmicYear) || string.IsNullOrEmpty(p.gradeCode))
                    str = "GP";
                else
                    str = "SP";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_paidUnpaid_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", p.curr_code),
                            new SqlParameter("@ACA_YEAR", p.AcadmicYear),
                            new SqlParameter("@GRADE_CODE", p.gradeCode),
                            new SqlParameter("@ADMISSION_NUM", p.admission_status),
                            new SqlParameter("@sims_admission_employee_code",p.employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",p.sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",p.nationality_code),
                            new SqlParameter("@sims_admission_gender",p.gender_code),
                            new SqlParameter("@adm_nos",p.adm_no),
                            new SqlParameter("@First_name",p.adm_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            }
                            catch (Exception ex) { }


                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                           // simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }
                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";

                           

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("waitingUnPaidStudents")]
        public HttpResponseMessage waitingUnPaidStudents(param p)//, Sims010_Auth nationality_code, Sims010_Auth gender_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetStudents", p.curr_code, p.AcadmicYear, p.gradeCode, p.admission_status));
            string cnt = "0", str = "";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (p.gradeCode == null)
                {
                    p.gradeCode = "undefined";
                }
                if (p.gender_code == "null")
                {
                    p.gender_code = null;
                }
                if (p.nationality_code == "null")
                {
                    p.nationality_code = null;
                }
                if (p.sibling_enroll == "null")
                {
                    p.sibling_enroll = null;
                }
                if (p.employee_code == "null")
                {
                    p.employee_code = null;
                }
                if (p.adm_no == "null")
                {
                    p.adm_no = null;
                }
                if (p.adm_name == "null")
                {
                    p.adm_name = null;
                }

                if (string.IsNullOrEmpty(p.curr_code) || string.IsNullOrEmpty(p.AcadmicYear)|| string.IsNullOrEmpty(p.gradeCode))
                    str = "GU";
                else
                    str = "SU";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_paidUnpaid_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", str),
                            new SqlParameter("@CURR_CODE", p.curr_code),
                            new SqlParameter("@ACA_YEAR", p.AcadmicYear),
                            new SqlParameter("@GRADE_CODE", p.gradeCode),
                            new SqlParameter("@ADMISSION_NUM", p.admission_status),
                            new SqlParameter("@sims_admission_employee_code",p.employee_code),
                            new SqlParameter("@sims_admission_sibling_enroll_number",p.sibling_enroll),
                            new SqlParameter("@sims_admission_nationality_code",p.nationality_code),
                            new SqlParameter("@sims_admission_gender",p.gender_code),
                            new SqlParameter("@adm_nos",p.adm_no),
                            new SqlParameter("@First_name",p.adm_name)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["Name"].ToString();
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.curr_name = dr["curr"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.grade_name = dr["grade"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.fee_category = dr["sims_admission_fee_category"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.fee_category_desc = dr["fee_category"].ToString();
                            simsobj.pros_num = dr["pros_no"].ToString();
                            simsobj.feepaid = dr["feepaid"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.term_code = dr["term_code"].ToString();
                            simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());

                            try
                            {
                                simsobj.sims_admission_dob = dr["sims_admission_dob"].ToString();
                                simsobj.sims_admission_national_id = dr["sims_admission_national_id"].ToString();
                                simsobj.communication_admi_date = db.UIDDMMYYYYformat(dr["communication_admi_date"].ToString());
                                simsobj.prospect_fee_status = dr["prospect_fee_status"].ToString();
                                simsobj.result = dr["result"].ToString();
                                simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();

                            }
                            catch (Exception ex) { }

                            simsobj.nation = dr["nationality"].ToString();
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                           // simsobj.sibling_enroll = (dr["sibling_status"].ToString().Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "New");
                            simsobj.empN_code = dr["empl_code"].ToString();
                            simsobj.empl_code = dr["emp_code"].ToString();
                            simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.Father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.Mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.Guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.FatherName = dr["FatherName"].ToString();
                            simsobj.criteria_cnt = dr["cnt_criteria"].ToString();
                            simsobj.path_cnt = dr["path_cnt"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simsobj.sims_admission_management_quota = dr["sims_admission_management_quota"].ToString();
                            simsobj.admission_status_code = dr["sims_admission_status"].ToString();
                            simsobj.admission_status = dr["admission_status"].ToString();
                            simsobj.remark = dr["remark"].ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_transport_desc"].ToString()))
                                    simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_father_occupation"].ToString()))
                                    simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_occupation"].ToString()))
                                    simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();

                                simsobj.enroll_no = dr["enroll_no"].ToString();
                            }
                            catch (Exception tx) { }

                            try
                            {
                                simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                                simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            }
                            catch (Exception tx1)
                            {

                            }

                            //cnt = dr["Doc_status"].ToString();
                            //simsobj.up_docsstatus = cnt == "0" ? "N" : "Y";
                            //cnt = dr["Mark_status"].ToString();
                            //simsobj.up_marksstatus = cnt == "0" ? "N" : "Y";
                            cnt = dr["Doc_status"].ToString();
                            if (cnt == "-1")
                                simsobj.up_docsstatus = "N";
                            else if (cnt == "0")
                                simsobj.up_docsstatus = "Y";
                            cnt = dr["Mark_status"].ToString();
                            if (cnt == "0")
                                simsobj.up_marksstatus = "Y";
                            else
                                simsobj.up_marksstatus = "N";

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("GetDashTilesDetails")]
        public HttpResponseMessage GetDashTilesDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetDashTilesDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                if (gradeCode == "undefined")
                {
                    gradeCode = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.appl_count = dr["appl_count"].ToString();
                            simsobj.Completed_cnt = dr["Completed_cnt"].ToString();
                            simsobj.rejected_cnt = dr["rejected_cnt"].ToString();
                            simsobj.waiting_cnt = dr["waiting_cnt"].ToString();
                            simsobj.selected_cnt = dr["selected_cnt"].ToString();
                            simsobj.waiting_paid = dr["waiting_paid"].ToString();
                            simsobj.waiting_unpaid = dr["waiting_unpaid"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetDashTilesViewDetails")]
        public HttpResponseMessage GetDashTilesViewDetails(string curr_code, string AcadmicYear, string gradeCode, string admission_status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4},admission_status{5}";
            Log.Debug(string.Format(debug, "Student", "GetDashTilesViewDetails", curr_code, AcadmicYear, gradeCode, admission_status));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.stud_full_name = dr["name"].ToString();
                            simsobj.sims_admission_status = dr["sims_admission_status"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetTabStudentData")]
        public HttpResponseMessage GetTabStudentData(string admission_number)
       {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTabStudentData()PARAMETERS ::admission_number{2}";
            Log.Debug(string.Format(debug, "ERP/Admission/", "GetTabStudentData", admission_number));

            Sims010_Edit simsobj = new Sims010_Edit();
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@CURR_CODE", ""),
                            new SqlParameter("@ACA_YEAR", ""),
                            new SqlParameter("@GRADE_CODE", ""),
                            new SqlParameter("@ADMISSION_NUM", admission_number)
                          
                         });
                        while (dr.Read())
                        {
                            #region
                            simsobj.admission_number = dr["sims_admission_number"].ToString();
                            simsobj.sims_admission_father_national_id = dr["sims_admission_father_national_id"].ToString();
                            simsobj.sims_admission_mother_national_id = dr["sims_admission_mother_national_id"].ToString();
                            simsobj.sims_admission_guardian_national_id = dr["sims_admission_guardian_national_id"].ToString();
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.school_name = dr["schoolName"].ToString();
                            simsobj.appl_num = dr["sims_admission_application_number"].ToString();
                            simsobj.behaviour_desc = dr["sims_admission_behaviour_desc"].ToString();
                            if (dr["sims_admission_behaviour_status"].ToString() == "1")
                            {
                                simsobj.behaviour_status = true;
                            }
                            else
                            {
                                simsobj.behaviour_status = false;
                            }
                            simsobj.birth_country_code = dr["sims_admission_birth_country_code"].ToString();
                            simsobj.blood_group_code = dr["sims_admission_blood_group_code"].ToString();
                            try
                            {
                                simsobj.comm_date = db.UIDDMMYYYYformat(dr["sims_admission_commencement_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.comm_date = "";
                            }
                            simsobj.current_school_address = dr["sims_admission_current_school_address"].ToString();
                            simsobj.current_school_city = dr["sims_admission_current_school_city"].ToString();
                            simsobj.current_school_cur = dr["sims_admission_current_school_cur"].ToString();
                            simsobj.current_school_enroll_number = dr["sims_admission_current_school_enroll_number"].ToString();
                            simsobj.current_school_fax = dr["sims_admission_current_school_fax"].ToString();
                            try
                            {
                                simsobj.current_school_from_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_from_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.current_school_from_date = "";
                            }
                            simsobj.current_school_grade = dr["sims_admission_current_school_grade"].ToString();

                            simsobj.current_school_head_teacher = dr["sims_admission_current_school_head_teacher"].ToString();
                            simsobj.current_school_language = dr["sims_admission_current_school_language"].ToString();
                            simsobj.current_school_name = dr["sims_admission_current_school_name"].ToString();
                            simsobj.current_school_phone = dr["sims_admission_current_school_phone"].ToString();
                            if (dr["sims_admission_current_school_status"].ToString() == "1")
                            {
                                simsobj.current_school_status = true;
                            }
                            else
                            {
                                simsobj.current_school_status = false;
                            }

                            simsobj.current_school_to_date = db.UIDDMMYYYYformat(dr["sims_admission_current_school_to_date"].ToString());
                            simsobj.current_school_country = dr["current_school_country"].ToString();
                            simsobj.current_school_country_code = dr["sims_admission_current_school_country_code"].ToString();
                            if (dr["sims_admission_declaration_status"].ToString() == "1")
                            {
                                simsobj.declaration_status = true;
                            }
                            else
                            {
                                simsobj.declaration_status = false;
                            }
                            simsobj.disability_desc = dr["sims_admission_disability_desc"].ToString();
                            if (dr["sims_admission_disability_status"].ToString() == "1")
                            {
                                simsobj.disability_status = true;
                            }
                            else
                            {
                                simsobj.disability_status = false;
                            }
                            simsobj.dns = dr["sims_admission_dns"].ToString();
                            simsobj.employee_comp_code = dr["sims_admission_employee_comp_code"].ToString();
                            simsobj.employee_school_code = dr["sims_admission_employee_school_code"].ToString();
                            if (dr["sims_admission_employee_type"].ToString() == "1")
                            {
                                simsobj.employee_type = true;
                            }
                            else
                            {
                                simsobj.employee_type = false;
                            }
                            simsobj.family_name_ot = dr["sims_admission_family_name_ot"].ToString();
                            simsobj.ethinicity_code = dr["sims_admission_ethnicity_code"].ToString();
                            simsobj.family_name = dr["sims_admission_family_name_en"].ToString();
                            simsobj.father_appartment_number = dr["sims_admission_father_appartment_number"].ToString();
                            simsobj.father_area_number = dr["sims_admission_father_area_number"].ToString();
                            simsobj.father_building_number = dr["sims_admission_father_building_number"].ToString();
                            simsobj.father_city = dr["sims_admission_father_city"].ToString();
                            simsobj.father_company = dr["sims_admission_father_company"].ToString();
                            simsobj.father_country_code = dr["sims_admission_father_country_code"].ToString();
                            simsobj.father_email = dr["sims_admission_father_email"].ToString();
                            simsobj.father_family_name = dr["sims_admission_father_family_name"].ToString();
                            simsobj.father_fax = dr["sims_admission_father_fax"].ToString();
                            simsobj.father_first_name = dr["sims_admission_father_first_name"].ToString();
                            simsobj.father_last_name = dr["sims_admission_father_last_name"].ToString();
                            simsobj.father_middle_name = dr["sims_admission_father_middle_name"].ToString();
                            simsobj.father_mobile = dr["sims_admission_father_mobile"].ToString();
                            simsobj.father_name_ot = dr["sims_admission_father_name_ot"].ToString();
                            simsobj.father_nationality1_code = dr["sims_admission_father_nationality1_code"].ToString();
                            simsobj.father_nationality2_code = dr["sims_admission_father_nationality2_code"].ToString();
                            simsobj.father_occupation = dr["sims_admission_father_occupation"].ToString();
                            simsobj.father_passport_number = dr["sims_admission_father_passport_number"].ToString();
                            simsobj.father_phone = dr["sims_admission_father_phone"].ToString();
                            simsobj.father_po_box = dr["sims_admission_father_po_box"].ToString();
                            simsobj.father_salutation_code = dr["sims_admission_father_salutation_code"].ToString();
                            simsobj.father_state = dr["sims_admission_father_state"].ToString();
                            simsobj.father_street_number = dr["sims_admission_father_street_number"].ToString();
                            simsobj.father_summary_address = dr["sims_admission_father_summary_address"].ToString();
                            simsobj.fee_payment_contact_pref_code = dr["sims_admission_fee_payment_contact_pref"].ToString();
                            if (dr["sims_admission_fees_paid_status"].ToString() == "1")
                            {
                                simsobj.fees_paid_status = true;
                            }
                            else
                            {
                                simsobj.fees_paid_status = false;
                            }
                            simsobj.first_name = dr["sims_admission_passport_first_name_en"].ToString();
                            simsobj.first_name_ot = dr["sims_admission_passport_first_name_ot"].ToString();
                            simsobj.gifted_desc = dr["sims_admission_gifted_desc"].ToString();
                            if (dr["sims_admission_gifted_status"].ToString() == "1")
                            {
                                simsobj.gifted_status = true;
                            }
                            else
                            {
                                simsobj.gifted_status = false;
                            }
                            simsobj.guardian_appartment_number = dr["sims_admission_guardian_appartment_number"].ToString();
                            simsobj.guardian_area_number = dr["sims_admission_guardian_area_number"].ToString();
                            simsobj.guardian_building_number = dr["sims_admission_guardian_building_number"].ToString();
                            simsobj.guardian_city = dr["sims_admission_guardian_city"].ToString();
                            simsobj.guardian_company = dr["sims_admission_guardian_company"].ToString();
                            simsobj.guardian_country_code = dr["sims_admission_guardian_country_code"].ToString();
                            simsobj.guardian_email = dr["sims_admission_guardian_email"].ToString();
                            simsobj.guardian_family_name = dr["sims_admission_guardian_family_name"].ToString();
                            simsobj.guardian_fax = dr["sims_admission_guardian_fax"].ToString();
                            simsobj.guardian_first_name = dr["sims_admission_guardian_first_name"].ToString();
                            simsobj.guardian_last_name = dr["sims_admission_guardian_last_name"].ToString();
                            simsobj.guardian_middle_name = dr["sims_admission_guardian_middle_name"].ToString();
                            simsobj.guardian_mobile = dr["sims_admission_guardian_mobile"].ToString();
                            simsobj.guardian_name_ot = dr["sims_admission_guardian_name_ot"].ToString();
                            simsobj.guardian_nationality1_code = dr["sims_admission_guardian_nationality1_code"].ToString();
                            simsobj.guardian_nationality2_code = dr["sims_admission_guardian_nationality2_code"].ToString();
                            simsobj.guardian_occupation = dr["sims_admission_guardian_occupation"].ToString();
                            simsobj.guardian_passport_number = dr["sims_admission_guardian_passport_number"].ToString();
                            simsobj.guardian_phone = dr["sims_admission_guardian_phone"].ToString();
                            simsobj.guardian_po_box = dr["sims_admission_guardian_po_box"].ToString();
                            simsobj.guardian_salutation_code = dr["sims_admission_guardian_salutation_code"].ToString();
                            simsobj.guardian_state = dr["sims_admission_guardian_state"].ToString();
                            simsobj.guardian_street_number = dr["sims_admission_guardian_street_number"].ToString();
                            simsobj.guardian_summary_address = dr["sims_admission_guardian_summary_address"].ToString();
                            try
                            {
                                simsobj.health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_expiry_date = "";
                            }
                            try
                            {
                                simsobj.health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_health_card_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.health_card_issue_date = "";
                            }
                            simsobj.health_card_issuing_authority = dr["sims_admission_health_card_issuing_authority"].ToString();
                            simsobj.health_card_number = dr["sims_admission_health_card_number"].ToString();
                            simsobj.health_hearing_desc = dr["sims_admission_health_hearing_desc"].ToString();
                            if (dr["sims_admission_health_hearing_status"].ToString() == "1")
                            {
                                simsobj.health_hearing_status = true;
                            }
                            else
                            {
                                simsobj.health_hearing_status = false;
                            }
                            simsobj.health_other_desc = dr["sims_admission_health_other_desc"].ToString();
                            if (dr["sims_admission_health_other_status"].ToString() == "1")
                            {
                                simsobj.health_other_status = true;
                            }
                            else
                            {
                                simsobj.health_other_status = false;
                            }
                            simsobj.health_restriction_desc = dr["sims_admission_health_restriction_desc"].ToString();
                            if (dr["sims_admission_health_restriction_status"].ToString() == "1")
                            {
                                simsobj.health_restriction_status = true;
                            }
                            else
                            {
                                simsobj.health_restriction_status = false;
                            }
                            simsobj.health_vision_desc = dr["sims_admission_health_vision_desc"].ToString();
                            if (dr["sims_admission_health_vision_status"].ToString() == "1")
                            {
                                simsobj.health_vision_status = true;
                            }
                            else
                            {
                                simsobj.health_vision_status = false;
                            }
                            simsobj.last_name = dr["sims_admission_passport_last_name_en"].ToString();
                            simsobj.last_name_ot = dr["sims_admission_passport_last_name_ot"].ToString();
                            simsobj.language_support_desc = dr["sims_admission_language_support_desc"].ToString();
                            if (dr["sims_admission_language_support_status"].ToString() == "1")
                            {
                                simsobj.language_support_status = true;
                            }
                            else
                            {
                                simsobj.language_support_status = false;
                            }
                            simsobj.legal_custody_code = dr["sims_admission_legal_custody"].ToString();
                            simsobj.main_language_code = dr["sims_admission_main_language_code"].ToString();
                            simsobj.main_language_r_code = dr["sims_admission_main_language_r"].ToString();
                            simsobj.main_language_s_code = dr["sims_admission_main_language_s"].ToString();
                            simsobj.main_language_w_code = dr["sims_admission_main_language_w"].ToString();
                            simsobj.other_language_code = dr["sims_admission_other_language"].ToString();
                            if (dr["sims_admission_marketing_code"].ToString() == "1")
                            {
                                simsobj.marketing_code = true;
                            }
                            else
                            {
                                simsobj.marketing_code = false;
                            }
                            simsobj.marketing_description = dr["sims_admission_marketing_description"].ToString();
                            simsobj.medication_desc = dr["sims_admission_medication_desc"].ToString();
                            if (dr["sims_admission_medication_status"].ToString() == "1")
                            {
                                simsobj.medication_status = true;
                            }
                            else
                            {
                                simsobj.medication_status = false;
                            }
                            simsobj.middle_name = dr["sims_admission_passport_middle_name_en"].ToString();
                            simsobj.midd_name_ot = dr["sims_admission_passport_middle_name_ot"].ToString();
                            simsobj.motherTounge_language_code = dr["sims_admission_main_language_m"].ToString();
                            simsobj.mother_appartment_number = dr["sims_admission_mother_appartment_number"].ToString();
                            simsobj.mother_area_number = dr["sims_admission_mother_area_number"].ToString();
                            simsobj.mother_building_number = dr["sims_admission_mother_building_number"].ToString();
                            simsobj.mother_city = dr["sims_admission_mother_city"].ToString();
                            simsobj.mother_company = dr["sims_admission_mother_company"].ToString();
                            simsobj.mother_country_code = dr["sims_admission_mother_country_code"].ToString();
                            simsobj.mother_email = dr["sims_admission_mother_email"].ToString();
                            simsobj.mother_family_name = dr["sims_admission_mother_family_name"].ToString();
                            simsobj.mother_fax = dr["sims_admission_mother_fax"].ToString();
                            simsobj.mother_first_name = dr["sims_admission_mother_first_name"].ToString();
                            simsobj.mother_last_name = dr["sims_admission_mother_last_name"].ToString();
                            simsobj.mother_middle_name = dr["sims_admission_mother_middle_name"].ToString();
                            simsobj.mother_mobile = dr["sims_admission_mother_mobile"].ToString();
                            simsobj.mother_name_ot = dr["sims_admission_mother_name_ot"].ToString();
                            simsobj.mother_nationality1_code = dr["sims_admission_mother_nationality1_code"].ToString();
                            simsobj.mother_nationality2_code = dr["sims_admission_mother_nationality2_code"].ToString();
                            simsobj.mother_occupation = dr["sims_admission_mother_occupation"].ToString();
                            simsobj.mother_passport_number = dr["sims_admission_mother_passport_number"].ToString();
                            simsobj.mother_phone = dr["sims_admission_mother_phone"].ToString();
                            simsobj.mother_po_box = dr["sims_admission_mother_po_box"].ToString();
                            simsobj.mother_salutation_code = dr["sims_admission_mother_salutation_code"].ToString();
                            simsobj.mother_state = dr["sims_admission_mother_state"].ToString();
                            simsobj.mother_street_number = dr["sims_admission_mother_street_number"].ToString();
                            simsobj.mother_summary_address = dr["sims_admission_mother_summary_address"].ToString();
                            simsobj.music_desc = dr["sims_admission_music_desc"].ToString();
                            if (dr["sims_admission_music_status"].ToString() == "1")
                            {
                                simsobj.music_status = true;
                            }
                            else
                            {
                                simsobj.music_status = false;
                            }
                            simsobj.national_id = dr["sims_admission_national_id"].ToString();
                            try
                            {
                                simsobj.national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_issue_date = "";
                            }

                            try
                            {
                                simsobj.national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_national_id_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.national_id_expiry_date = "";
                            }
                            simsobj.nicke_name = dr["sims_admission_nickname"].ToString();
                            simsobj.parent_id = dr["sims_admission_parent_id"].ToString();
                            if (dr["sims_admission_parent_status_code"].ToString() == "1")
                            {
                                simsobj.parent_status_code = true;
                            }
                            else
                            {
                                simsobj.parent_status_code = false;
                            }
                            try
                            {
                                simsobj.passport_expiry = db.UIDDMMYYYYformat(dr["sims_admission_passport_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_expiry = "";
                            }
                            simsobj.passport_issue_auth = dr["sims_admission_passport_issuing_authority"].ToString();
                            try
                            {
                                simsobj.passport_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_passport_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.passport_issue_date = "";
                            }
                            simsobj.passport_issue_place = dr["sims_admission_passport_issue_place"].ToString();
                            simsobj.passport_num = dr["sims_admission_passport_number"].ToString();
                            simsobj.primary_contact_pref_code = dr["sims_admission_primary_contact_pref"].ToString();
                            if (dr["sims_admission_primary_contact_code"].ToString() == "1")
                            {
                                simsobj.primary_contact_code = true;
                            }
                            else
                            {
                                simsobj.primary_contact_code = false;
                            }
                            simsobj.primary_contact_pref_desc = dr["primary_contact_pref_desc"].ToString();
                            simsobj.pros_appl_num = dr["sims_admission_pros_application_number"].ToString();
                            simsobj.religion_code = dr["sims_admission_religion_code"].ToString();
                            simsobj.sibling_dob = dr["sims_admission_sibling_dob"].ToString();
                            simsobj.sibling_name = dr["sims_admission_sibling_name"].ToString();
                            simsobj.sibling_school_code = dr["sims_admission_sibling_school_code"].ToString();
                            simsobj.sibling_school_name = dr["sibSchoolName"].ToString();
                            simsobj.sports_desc = dr["sims_admission_sports_desc"].ToString();
                            if (dr["sims_admission_sports_status"].ToString() == "1")
                            {
                                simsobj.sports_status = true;
                            }
                            else
                            {
                                simsobj.sports_status = false;
                            }
                            simsobj.status = dr["sims_admission_status"].ToString();
                            simsobj.section_code = dr["sims_admission_section_code"].ToString();
                            simsobj.user_code = dr["sims_admission_user_code"].ToString();
                            try
                            {
                                simsobj.visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_expiry_date = "";
                            }

                            try
                            {
                                simsobj.visa_issue_date = db.UIDDMMYYYYformat(dr["sims_admission_visa_issue_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                                simsobj.visa_issue_date = "";
                            }
                            simsobj.visa_issuing_authority = dr["sims_admission_visa_issuing_authority"].ToString();
                            simsobj.visa_issuing_place = dr["sims_admission_visa_issuing_place"].ToString();
                            simsobj.visa_number = dr["sims_admission_visa_number"].ToString();
                            simsobj.visa_type_desc = dr["sims_admission_visa_type_desc"].ToString();
                            simsobj.visa_type = dr["sims_admission_visa_type"].ToString();
                            if (dr["sims_admission_transport_status"].ToString() == "1")
                            {
                                simsobj.transport_status = true;
                            }
                            else
                            {
                                simsobj.transport_status = false;
                            }
                            simsobj.transport_desc = dr["sims_admission_transport_desc"].ToString();
                            simsobj.ip = dr["sims_admission_ip"].ToString();
                            #endregion
                            simsobj.sims_admission_recommendation = dr["sims_admission_recommendation"].ToString();
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_student_attribute1 = dr["sims_student_attribute1"].ToString();
                            simsobj.sims_student_attribute2 = dr["sims_student_attribute2"].ToString();
                            simsobj.sims_student_attribute3 = dr["sims_student_attribute3"].ToString();
                            simsobj.sims_student_attribute4 = dr["sims_student_attribute4"].ToString();

                            simsobj.sims_student_health_respiratory_status = dr["sims_student_health_respiratory_status"].ToString();
                            simsobj.sims_student_health_respiratory_desc = dr["sims_student_health_respiratory_desc"].ToString();
                            simsobj.sims_student_health_hay_fever_status = dr["sims_student_health_hay_fever_status"].ToString();
                            simsobj.sims_student_health_hay_fever_desc = dr["sims_student_health_hay_fever_desc"].ToString();
                            simsobj.sims_student_health_epilepsy_status = dr["sims_student_health_epilepsy_status"].ToString();
                            simsobj.sims_student_health_epilepsy_desc = dr["sims_student_health_epilepsy_desc"].ToString();
                            simsobj.sims_student_health_skin_status = dr["sims_student_health_skin_status"].ToString();
                            simsobj.sims_student_health_skin_desc = dr["sims_student_health_skin_desc"].ToString();
                            simsobj.sims_student_health_diabetes_status = dr["sims_student_health_diabetes_status"].ToString();
                            simsobj.sims_student_health_diabetes_desc = dr["sims_student_health_diabetes_desc"].ToString();
                            simsobj.sims_student_health_surgery_status = dr["sims_student_health_surgery_status"].ToString();
                            simsobj.sims_student_health_surgery_desc = dr["sims_student_health_surgery_desc"].ToString();


                            simsobj.admission_number = admission_number;
                            simsobj.stud_full_name = simsobj.first_name + " " + simsobj.middle_name + " " + simsobj.last_name;// o.stud_full_name;
                            simsobj.fee_category_code = dr["sims_admission_fee_category"].ToString();
                            try
                            {
                                simsobj.admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());//o.admission_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.admission_date = "";
                            }
                            simsobj.pros_num = dr["pros_no"].ToString();//o.pros_no;
                            simsobj.school_code = dr["sims_admission_school_code"].ToString();
                            simsobj.term_code = dr["sims_admission_term_code"].ToString();
                            try
                            {
                                simsobj.tent_join_date = db.UIDDMMYYYYformat(dr["tent_join_date"].ToString());//o.tent_join_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.tent_join_date = "";
                            }
                            simsobj.gender_desc = dr["gender"].ToString();// o.gender;
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            try
                            {
                                simsobj.birth_date = db.UIDDMMYYYYformat(dr["dob"].ToString());//o.birth_date;
                            }
                            catch (Exception ex)
                            {
                                simsobj.birth_date = "";
                            }
                            simsobj.curr_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.academic_year_desc = dr["academic_year_desc"].ToString();
                            simsobj.academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.nationality_code = dr["sims_admission_nationality_code"].ToString();//o.nation;
                            if (dr["sibling_status"].ToString() == "1")
                            {
                                simsobj.sibling_status = true;
                            }
                            else
                            {
                                simsobj.sibling_status = false;
                            }
                            simsobj.sibling_enroll = dr["sibling_enroll"].ToString();
                            // simsobj.sibling_enroll = (simsobj.sibling_status.Equals("1", StringComparison.CurrentCultureIgnoreCase) ? dr["sibling_enroll"].ToString() : "N/A");
                            simsobj.employee_code = dr["emp_code"].ToString();
                           // simsobj.section_code = "0018";

                            try
                            {
                                simsobj.category_code = dr["sims_student_category"].ToString();
                            }
                            catch(Exception e)
                            {
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(dr["sims_admission_father_salary"].ToString()))
                                    simsobj.father_salary = dr["sims_admission_father_salary"].ToString();


                                if (!string.IsNullOrEmpty(dr["sims_admission_mother_salary"].ToString()))
                                    simsobj.mother_salary = dr["sims_admission_mother_salary"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_admission_fee_month_code"].ToString()))
                                    simsobj.sims_fee_month_code = dr["sims_admission_fee_month_code"].ToString();

                                if (!string.IsNullOrEmpty(dr["feemonth_name"].ToString()))
                                    simsobj.sims_fee_month_name = dr["feemonth_name"].ToString();

                                if (!string.IsNullOrEmpty(dr["sims_student_img"].ToString()))
                                    simsobj.sims_student_img = dr["sims_student_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_father_img"].ToString()))
                                    simsobj.sims_father_img = dr["sims_father_img"].ToString();
                                if (!string.IsNullOrEmpty(dr["sims_mother_img"].ToString()))
                                    simsobj.sims_mother_img = dr["sims_mother_img"].ToString();

                                if (!string.IsNullOrEmpty(dr["comn_seq"].ToString()))
                                    simsobj.comn_seq = dr["comn_seq"].ToString();

                                if (!string.IsNullOrEmpty(dr["parent_comn_seq"].ToString()))
                                    simsobj.parent_comn_seq = dr["parent_comn_seq"].ToString();

                                simsobj.sims_student_attribute5 = dr["sims_student_attribute5"].ToString();
                                simsobj.sims_student_attribute6 = dr["sims_student_attribute6"].ToString();
                                simsobj.sims_student_attribute7 = dr["sims_student_attribute7"].ToString();
                                simsobj.sims_student_attribute8 = dr["sims_student_attribute8"].ToString();
                                simsobj.sims_student_attribute9 = dr["sims_student_attribute9"].ToString();
                                simsobj.sims_student_attribute10 = dr["sims_student_attribute10"].ToString();
                                simsobj.sims_student_attribute11 = dr["sims_student_attribute11"].ToString();
                                simsobj.sims_student_attribute12 = dr["sims_student_attribute12"].ToString();

                               // simsobj.sims_admission_second_lang_code = dr["sims_admission_second_lang_code"].ToString();
                               // simsobj.sims_admission_second_lang_name = dr["sims_admission_second_lang_name"].ToString();
                                // simsobj.transport_status=dr["sims_admission_transport_status"].ToString();
                            }
                            catch (Exception ee)
                            {
                            }

                        }

                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        [Route("GetAll_SimsProspects")]
        public HttpResponseMessage GetAll_SimsProspects(string pros_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudents(),PARAMETERS :: pros_no{2}";
            Log.Debug(string.Format(debug, "Admission", "GetAll_SimsProspects", pros_no));
            string cnt = "0", str = "";

            Sims034 pros_obj = new Sims034();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Sims_Prospect",
                        new List<SqlParameter>()
                        {
                           new SqlParameter("@opr", "S"),
                           new SqlParameter("@sims_pros_number", pros_no)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            pros_obj.sims_pros_number = dr["sims_pros_number"].ToString();
                            pros_obj.sims_pros_application_number = dr["sims_pros_application_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_date_created"].ToString()))
                                pros_obj.sims_pros_date_created = db.UIDDMMYYYYformat(dr["sims_pros_date_created"].ToString());
                            pros_obj.sims_pros_school_code = dr["sims_pros_school_code"].ToString();
                            pros_obj.sims_pros_cur_code = dr["sims_pros_cur_code"].ToString();
                            pros_obj.sims_pros_academic_year = dr["sims_pros_academic_year"].ToString();
                            pros_obj.sims_pros_grade_code = dr["sims_pros_grade_code"].ToString();
                            pros_obj.sims_pros_term_code = dr["sims_pros_term_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_tentative_joining_date"].ToString()))
                                //pros_obj.sims_pros_tentative_joining_date = DateTime.Parse(dr["sims_pros_tentative_joining_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_tentative_joining_date = db.UIDDMMYYYYformat(dr["sims_pros_tentative_joining_date"].ToString());
                            pros_obj.sims_pros_passport_number = dr["sims_pros_passport_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_passport_issue_date"].ToString()))
                                //pros_obj.sims_pros_passport_issue_date = DateTime.Parse(dr["sims_pros_passport_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_passport_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_passport_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_passport_expiry_date"].ToString()))
                                //pros_obj.sims_pros_passport_expiry_date = DateTime.Parse(dr["sims_pros_passport_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_passport_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_passport_expiry_date"].ToString());
                            pros_obj.sims_pros_passport_issuing_authority = dr["sims_pros_passport_issuing_authority"].ToString();
                            pros_obj.sims_pros_passport_issue_place = dr["sims_pros_passport_issue_place"].ToString();
                            pros_obj.sims_pros_passport_first_name_en = dr["sims_pros_passport_first_name_en"].ToString();
                            pros_obj.sims_pros_passport_middle_name_en = dr["sims_pros_passport_middle_name_en"].ToString();
                            pros_obj.sims_pros_passport_last_name_en = dr["sims_pros_passport_last_name_en"].ToString();
                            pros_obj.sims_pros_family_name_en = dr["sims_pros_family_name_en"].ToString();
                            pros_obj.sims_pros_nickname = dr["sims_pros_nickname"].ToString();
                            pros_obj.sims_pros_passport_first_name_ot = dr["sims_pros_passport_first_name_ot"].ToString();
                            pros_obj.sims_pros_passport_middle_name_ot = dr["sims_pros_passport_middle_name_ot"].ToString();
                            pros_obj.sims_pros_passport_last_name_ot = dr["sims_pros_passport_last_name_ot"].ToString();
                            pros_obj.sims_pros_family_name_ot = dr["sims_pros_family_name_ot"].ToString();
                            pros_obj.sims_pros_gender = dr["sims_pros_gender"].ToString();
                            pros_obj.sims_pros_religion_code = dr["sims_pros_religion_code"].ToString();
                            pros_obj.sims_pros_dob = dr["sims_pros_dob"].ToString();
                            pros_obj.sims_pros_birth_country_code = dr["sims_pros_birth_country_code"].ToString();
                            pros_obj.sims_pros_nationality_code = dr["sims_pros_nationality_code"].ToString();
                            pros_obj.sims_pros_ethnicity_code = dr["sims_pros_ethnicity_code"].ToString();
                            pros_obj.sims_pros_visa_number = dr["sims_pros_visa_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_visa_issue_date"].ToString()))
                                //pros_obj.sims_pros_visa_issue_date = DateTime.Parse(dr["sims_pros_visa_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_visa_expiry_date"].ToString()))
                                //pros_obj.sims_pros_visa_expiry_date = DateTime.Parse(dr["sims_pros_visa_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_visa_expiry_date"].ToString());
                            pros_obj.sims_pros_visa_issuing_place = dr["sims_pros_visa_issuing_place"].ToString();
                            pros_obj.sims_pros_visa_issuing_authority = dr["sims_pros_visa_issuing_authority"].ToString();
                            pros_obj.sims_pros_visa_type = dr["sims_pros_visa_type"].ToString();
                            pros_obj.sims_pros_national_id = dr["sims_pros_national_id"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_national_id_issue_date"].ToString()))
                                //pros_obj.sims_pros_national_id_issue_date = DateTime.Parse(dr["sims_pros_national_id_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_national_id_expiry_date"].ToString()))
                                //pros_obj.sims_pros_national_id_expiry_date = DateTime.Parse(dr["sims_pros_national_id_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_national_id_expiry_date"].ToString());
                            pros_obj.sims_pros_main_language_code = dr["sims_pros_main_language_code"].ToString();
                            pros_obj.sims_pros_main_language_r = dr["sims_pros_main_language_r"].ToString();
                            pros_obj.sims_pros_main_language_w = dr["sims_pros_main_language_w"].ToString();
                            pros_obj.sims_pros_main_language_s = dr["sims_pros_main_language_s"].ToString();
                            pros_obj.sims_pros_main_language_m = dr["sims_pros_main_language_m"].ToString();
                            pros_obj.sims_pros_other_language = dr["sims_pros_other_language"].ToString();
                            pros_obj.sims_pros_sibling_status = dr["sims_pros_sibling_status"].ToString();
                            pros_obj.sims_pros_sibling_name = dr["sims_pros_sibling_name"].ToString();
                            pros_obj.sims_pros_sibling_enroll_number = dr["sims_pros_sibling_enroll_number"].ToString();
                            pros_obj.sims_pros_parent_id = dr["sims_pros_parent_id"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_sibling_dob"].ToString()))
                                //pros_obj.sims_pros_sibling_dob = DateTime.Parse(dr["sims_pros_sibling_dob"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_sibling_dob = db.UIDDMMYYYYformat(dr["sims_pros_sibling_dob"].ToString());
                            pros_obj.sims_pros_sibling_school_code = dr["sims_pros_sibling_school_code"].ToString();
                            pros_obj.sims_pros_primary_contact_code = dr["sims_pros_primary_contact_code"].ToString();
                            pros_obj.sims_pros_primary_contact_pref = dr["sims_pros_primary_contact_pref"].ToString();
                            pros_obj.sims_pros_fee_payment_contact_pref = dr["sims_pros_fee_payment_contact_pref"].ToString();
                            pros_obj.sims_pros_transport_status = dr["sims_pros_transport_status"].ToString();
                            pros_obj.sims_pros_transport_desc = dr["sims_pros_transport_desc"].ToString();
                            pros_obj.sims_parent_is_employment_status = dr["sims_parent_is_employment_status"].ToString();
                            pros_obj.sims_parent_is_employment_number = dr["sims_parent_is_employment_number"].ToString();
                            pros_obj.sims_pros_father_salutation_code = dr["sims_pros_father_salutation_code"].ToString();
                            pros_obj.sims_pros_father_first_name = dr["sims_pros_father_first_name"].ToString();
                            pros_obj.sims_pros_father_middle_name = dr["sims_pros_father_middle_name"].ToString();
                            pros_obj.sims_pros_father_last_name = dr["sims_pros_father_last_name"].ToString();
                            pros_obj.sims_pros_father_family_name = dr["sims_pros_father_family_name"].ToString();
                            pros_obj.sims_pros_father_name_ot = dr["sims_pros_father_name_ot"].ToString();
                            pros_obj.sims_pros_father_nationality1_code = dr["sims_pros_father_nationality1_code"].ToString();
                            pros_obj.sims_pros_father_nationality2_code = dr["sims_pros_father_nationality2_code"].ToString();
                            pros_obj.sims_pros_father_appartment_number = dr["sims_pros_father_appartment_number"].ToString();
                            pros_obj.sims_pros_father_building_number = dr["sims_pros_father_building_number"].ToString();
                            pros_obj.sims_pros_father_street_number = dr["sims_pros_father_street_number"].ToString();
                            pros_obj.sims_pros_father_area_number = dr["sims_pros_father_area_number"].ToString();
                            pros_obj.sims_pros_father_summary_address = dr["sims_pros_father_summary_address"].ToString();
                            pros_obj.sims_pros_father_city = dr["sims_pros_father_city"].ToString();
                            pros_obj.sims_pros_father_state = dr["sims_pros_father_state"].ToString();
                            pros_obj.sims_pros_father_country_code = dr["sims_pros_father_country_code"].ToString();
                            pros_obj.sims_pros_father_phone = dr["sims_pros_father_phone"].ToString();
                            pros_obj.sims_pros_father_mobile = dr["sims_pros_father_mobile"].ToString();
                            pros_obj.sims_pros_father_email = dr["sims_pros_father_email"].ToString();
                            pros_obj.sims_pros_father_fax = dr["sims_pros_father_fax"].ToString();
                            pros_obj.sims_pros_father_po_box = dr["sims_pros_father_po_box"].ToString();
                            pros_obj.sims_pros_father_occupation = dr["sims_pros_father_occupation"].ToString();
                            pros_obj.sims_pros_father_company = dr["sims_pros_father_company"].ToString();
                            pros_obj.sims_pros_father_passport_number = dr["sims_pros_father_passport_number"].ToString();
                            pros_obj.sims_pros_mother_salutation_code = dr["sims_pros_mother_salutation_code"].ToString();
                            pros_obj.sims_pros_mother_first_name = dr["sims_pros_mother_first_name"].ToString();
                            pros_obj.sims_pros_mother_middle_name = dr["sims_pros_mother_middle_name"].ToString();
                            pros_obj.sims_pros_mother_last_name = dr["sims_pros_mother_last_name"].ToString();
                            pros_obj.sims_pros_mother_family_name = dr["sims_pros_mother_family_name"].ToString();
                            pros_obj.sims_pros_mother_name_ot = dr["sims_pros_mother_name_ot"].ToString();
                            pros_obj.sims_pros_mother_nationality1_code = dr["sims_pros_mother_nationality1_code"].ToString();
                            pros_obj.sims_pros_mother_nationality2_code = dr["sims_pros_mother_nationality2_code"].ToString();
                            pros_obj.sims_pros_mother_appartment_number = dr["sims_pros_mother_appartment_number"].ToString();
                            pros_obj.sims_pros_mother_building_number = dr["sims_pros_mother_building_number"].ToString();
                            pros_obj.sims_pros_mother_street_number = dr["sims_pros_mother_street_number"].ToString();
                            pros_obj.sims_pros_mother_area_number = dr["sims_pros_mother_area_number"].ToString();
                            pros_obj.sims_pros_mother_summary_address = dr["sims_pros_mother_summary_address"].ToString();
                            pros_obj.sims_pros_mother_city = dr["sims_pros_mother_city"].ToString();
                            pros_obj.sims_pros_mother_state = dr["sims_pros_mother_state"].ToString();
                            pros_obj.sims_pros_mother_country_code = dr["sims_pros_mother_country_code"].ToString();
                            pros_obj.sims_pros_mother_phone = dr["sims_pros_mother_phone"].ToString();
                            pros_obj.sims_pros_mother_mobile = dr["sims_pros_mother_mobile"].ToString();
                            pros_obj.sims_pros_mother_email = dr["sims_pros_mother_email"].ToString();
                            pros_obj.sims_pros_mother_fax = dr["sims_pros_mother_fax"].ToString();
                            pros_obj.sims_pros_mother_po_box = dr["sims_pros_mother_po_box"].ToString();
                            pros_obj.sims_pros_mother_occupation = dr["sims_pros_mother_occupation"].ToString();
                            pros_obj.sims_pros_mother_company = dr["sims_pros_mother_company"].ToString();
                            pros_obj.sims_pros_mother_passport_number = dr["sims_pros_mother_passport_number"].ToString();
                            pros_obj.sims_pros_guardian_salutation_code = dr["sims_pros_guardian_salutation_code"].ToString();
                            pros_obj.sims_pros_guardian_first_name = dr["sims_pros_guardian_first_name"].ToString();
                            pros_obj.sims_pros_guardian_middle_name = dr["sims_pros_guardian_middle_name"].ToString();
                            pros_obj.sims_pros_guardian_last_name = dr["sims_pros_guardian_last_name"].ToString();
                            pros_obj.sims_pros_guardian_family_name = dr["sims_pros_guardian_family_name"].ToString();
                            pros_obj.sims_pros_guardian_name_ot = dr["sims_pros_guardian_name_ot"].ToString();
                            pros_obj.sims_pros_guardian_nationality1_code = dr["sims_pros_guardian_nationality1_code"].ToString();
                            pros_obj.sims_pros_guardian_nationality2_code = dr["sims_pros_guardian_nationality2_code"].ToString();
                            pros_obj.sims_pros_guardian_appartment_number = dr["sims_pros_guardian_appartment_number"].ToString();
                            pros_obj.sims_pros_guardian_building_number = dr["sims_pros_guardian_building_number"].ToString();
                            pros_obj.sims_pros_guardian_street_number = dr["sims_pros_guardian_street_number"].ToString();
                            pros_obj.sims_pros_guardian_area_number = dr["sims_pros_guardian_area_number"].ToString();
                            pros_obj.sims_pros_guardian_summary_address = dr["sims_pros_guardian_summary_address"].ToString();
                            pros_obj.sims_pros_guardian_city = dr["sims_pros_guardian_city"].ToString();
                            pros_obj.sims_pros_guardian_state = dr["sims_pros_guardian_state"].ToString();
                            pros_obj.sims_pros_guardian_country_code = dr["sims_pros_guardian_country_code"].ToString();
                            pros_obj.sims_pros_guardian_phone = dr["sims_pros_guardian_phone"].ToString();
                            pros_obj.sims_pros_guardian_mobile = dr["sims_pros_guardian_mobile"].ToString();
                            pros_obj.sims_pros_guardian_email = dr["sims_pros_guardian_email"].ToString();
                            pros_obj.sims_pros_guardian_fax = dr["sims_pros_guardian_fax"].ToString();
                            pros_obj.sims_pros_guardian_po_box = dr["sims_pros_guardian_po_box"].ToString();
                            pros_obj.sims_pros_guardian_occupation = dr["sims_pros_guardian_occupation"].ToString();
                            pros_obj.sims_pros_guardian_company = dr["sims_pros_guardian_company"].ToString();
                            pros_obj.sims_pros_guardian_relationship_code = dr["sims_pros_guardian_relationship_code"].ToString();
                            pros_obj.sims_pros_guardian_passport_number = dr["sims_pros_guardian_passport_number"].ToString();
                            pros_obj.sims_pros_current_school_status = dr["sims_pros_current_school_status"].ToString();
                            pros_obj.sims_pros_current_school_name = dr["sims_pros_current_school_name"].ToString();
                            pros_obj.sims_pros_current_school_grade = dr["sims_pros_current_school_grade"].ToString();
                            pros_obj.sims_pros_current_school_cur = dr["sims_pros_current_school_cur"].ToString();
                            pros_obj.sims_pros_current_school_enroll_number = dr["sims_pros_current_school_enroll_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_current_school_from_date"].ToString()))
                                //pros_obj.sims_pros_current_school_from_date = DateTime.Parse(dr["sims_pros_current_school_from_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_current_school_from_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_from_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_current_school_to_date"].ToString()))
                                //pros_obj.sims_pros_current_school_to_date = DateTime.Parse(dr["sims_pros_current_school_to_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_current_school_to_date = db.UIDDMMYYYYformat(dr["sims_pros_current_school_to_date"].ToString());
                            pros_obj.sims_pros_current_school_language = dr["sims_pros_current_school_language"].ToString();
                            pros_obj.sims_pros_current_school_head_teacher = dr["sims_pros_current_school_head_teacher"].ToString();
                            pros_obj.sims_pros_current_school_phone = dr["sims_pros_current_school_phone"].ToString();
                            pros_obj.sims_pros_current_school_fax = dr["sims_pros_current_school_fax"].ToString();
                            pros_obj.sims_pros_current_school_city = dr["sims_pros_current_school_city"].ToString();
                            pros_obj.sims_pros_current_school_country_code = dr["sims_pros_current_school_country_code"].ToString();
                            pros_obj.sims_pros_current_school_address = dr["sims_pros_current_school_address"].ToString();
                            pros_obj.sims_pros_marketing_code = dr["sims_pros_marketing_code"].ToString();
                            pros_obj.sims_pros_marketing_description = dr["sims_pros_marketing_description"].ToString();
                            pros_obj.sims_pros_parent_status_code = dr["sims_pros_parent_status_code"].ToString();
                            pros_obj.sims_pros_legal_custody = dr["sims_pros_legal_custody"].ToString();
                            pros_obj.sims_pros_health_card_number = dr["sims_pros_health_card_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_pros_health_card_issue_date"].ToString()))
                                //pros_obj.sims_pros_health_card_issue_date = DateTime.Parse(dr["sims_pros_health_card_issue_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_health_card_issue_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_pros_health_card_expiry_date"].ToString()))
                                //pros_obj.sims_pros_health_card_expiry_date = DateTime.Parse(dr["sims_pros_health_card_expiry_date"].ToString()).ToShortDateString();
                                pros_obj.sims_pros_health_card_expiry_date = db.UIDDMMYYYYformat(dr["sims_pros_health_card_expiry_date"].ToString());
                            pros_obj.sims_pros_health_card_issuing_authority = dr["sims_pros_health_card_issuing_authority"].ToString();
                            pros_obj.sims_pros_blood_group_code = dr["sims_pros_blood_group_code"].ToString();
                            pros_obj.sims_pros_medication_status = dr["sims_pros_medication_status"].ToString();
                            pros_obj.sims_pros_medication_desc = dr["sims_pros_medication_desc"].ToString();
                            pros_obj.sims_pros_disability_status = dr["sims_pros_disability_status"].ToString();
                            pros_obj.sims_pros_disability_desc = dr["sims_pros_disability_desc"].ToString();
                            pros_obj.sims_pros_behaviour_status = dr["sims_pros_behaviour_status"].ToString();
                            pros_obj.sims_pros_behaviour_desc = dr["sims_pros_behaviour_desc"].ToString();
                            pros_obj.sims_pros_health_restriction_status = dr["sims_pros_health_restriction_status"].ToString();
                            pros_obj.sims_pros_health_restriction_desc = dr["sims_pros_health_restriction_desc"].ToString();
                            pros_obj.sims_pros_health_hearing_status = dr["sims_pros_health_hearing_status"].ToString();
                            pros_obj.sims_pros_health_hearing_desc = dr["sims_pros_health_hearing_desc"].ToString();
                            pros_obj.sims_pros_health_vision_status = dr["sims_pros_health_vision_status"].ToString();
                            pros_obj.sims_pros_health_vision_desc = dr["sims_pros_health_vision_desc"].ToString();
                            pros_obj.sims_pros_health_other_status = dr["sims_pros_health_other_status"].ToString();
                            pros_obj.sims_pros_health_other_desc = dr["sims_pros_health_other_desc"].ToString();
                            pros_obj.sims_pros_language_support_status = dr["sims_pros_language_support_status"].ToString();
                            pros_obj.sims_pros_language_support_desc = dr["sims_pros_language_support_desc"].ToString();
                            pros_obj.sims_pros_gifted_status = dr["sims_pros_gifted_status"].ToString();
                            pros_obj.sims_pros_gifted_desc = dr["sims_pros_gifted_desc"].ToString();
                            pros_obj.sims_pros_music_status = dr["sims_pros_music_status"].ToString();
                            pros_obj.sims_pros_music_desc = dr["sims_pros_music_desc"].ToString();
                            pros_obj.sims_pros_sports_status = dr["sims_pros_sports_status"].ToString();
                            pros_obj.sims_pros_sports_desc = dr["sims_pros_sports_desc"].ToString();
                            pros_obj.sims_pros_declaration_status = dr["sims_pros_declaration_status"].ToString();
                            pros_obj.sims_pros_status = dr["sims_pros_status"].ToString();
                            pros_obj.sims_pros_ip = dr["sims_pros_ip"].ToString();
                            pros_obj.sims_pros_dns = dr["sims_pros_dns"].ToString();
                            pros_obj.sims_pros_user_code = dr["sims_pros_user_code"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, pros_obj);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("getRegistration")]
        public HttpResponseMessage getRegistration()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRegistration(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Registration", "Registration"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_appl_parameter_reg = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1_reg = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("GetcriteriaMarks")]
        public HttpResponseMessage GetcriteriaMarks(string admission_num)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetcriteriaMarks(),PARAMETERS :: admission_num{2}";
            Log.Debug(string.Format(debug, "Student", "GetcriteriaMarks", admission_num));

            List<Uccw230> type_list = new List<Uccw230>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@ADMISSION_NUM", admission_num)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw230 simsobj = new Uccw230();
                            simsobj.admis_num = admission_num;
                            simsobj.sims_admission_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            simsobj.sims_criteria_type = dr["sims_criteria_type"].ToString();
                            simsobj.sims_admission_marks = dr["sims_admission_marks"].ToString();
                            simsobj.sims_admission_rating = dr["sims_admission_rating"].ToString();
                            simsobj.sims_marks = dr["sims_marks"].ToString();
                            // simsobj.sims_criteria_status = dr["sims_criteria_status"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetCriteriaName")]
        public HttpResponseMessage GetCriteriaName(string admission_num)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCriteriaName(),PARAMETERS :: admission_num{2}";
            Log.Debug(string.Format(debug, "Student", "GetCriteriaName", admission_num));

            List<Uccw229> type_list = new List<Uccw229>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@sims_admission_number", admission_num)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw229 simsobj = new Uccw229();
                            simsobj.admis_num = admission_num;
                            simsobj.sims_criteria_code = dr["sims_criteria_code"].ToString();
                            simsobj.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            simsobj.sims_criteria_type = dr["sims_criteria_type"].ToString();

                            simsobj.count = int.Parse(dr["count"].ToString());

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                               new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'C'),
                                    new SqlParameter("@sims_admission_number", admission_num),
                                    new SqlParameter("@sims_admission_criteria_code", simsobj.sims_criteria_code)
                                });
                                if (dr1.HasRows)
                                {
                                    string sims_admission_doc_path = "";
                                    bool status = false, verify = false;
                                    while (dr1.Read())
                                    {
                                        Uccw229 simsobj1 = new Uccw229();
                                        simsobj1.sims_admission_doc_path = dr1["sims_admission_doc_path"].ToString();
                                        sims_admission_doc_path = sims_admission_doc_path + ',' + simsobj1.sims_admission_doc_path;

                                        simsobj1.if_uploaded = !string.IsNullOrEmpty(simsobj1.sims_admission_doc_path);
                                        if (dr1["sims_admission_doc_status"].ToString() == "0")
                                            simsobj1.sims_admission_doc_status = false;
                                        else if (dr1["sims_admission_doc_status"].ToString() == "1")
                                            simsobj1.sims_admission_doc_status = true;
                                        if (dr1["sims_admission_doc_verify"].ToString() == "0")
                                            simsobj1.sims_admission_doc_verify = false;
                                        else if (dr1["sims_admission_doc_verify"].ToString() == "1")
                                            simsobj1.sims_admission_doc_verify = true;

                                        simsobj1.if_status = simsobj1.if_uploaded;//for isenable
                                        simsobj1.if_verify = (simsobj1.if_status && simsobj1.if_uploaded);
                                        status = simsobj1.sims_admission_doc_status;
                                        verify = simsobj1.sims_admission_doc_verify;

                                    }
                                    simsobj.sims_admission_doc_path = sims_admission_doc_path.Substring(1);
                                    simsobj.sims_admission_doc_status = status;
                                    simsobj.sims_admission_doc_verify = verify;
                                }
                            }
                            //for isenable
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUD_UpdateAdmissionMarks")]
        public HttpResponseMessage CUD_UpdateAdmissionMarks(List<Uccw230> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAdmissionMarks(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "AdmissionMarks", "UpdateAdmissionMarks"));

            List<Uccw230> type_list = new List<Uccw230>();
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Uccw230 simsobj in lstsimsobj)
                    {
                        if (!string.IsNullOrEmpty(simsobj.sims_admission_marks))
                        {
                            if (decimal.Parse(simsobj.sims_marks) < decimal.Parse(simsobj.sims_admission_marks))
                            {
                                simsobj.sims_admission_marks = "";
                            }
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_update_admissionmarks_proc",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_criteria_code", simsobj.sims_admission_criteria_code),
                            new SqlParameter("@sims_admission_marks", simsobj.sims_admission_marks),
                            new SqlParameter("@sims_admission_rating", simsobj.sims_admission_rating),
                            new SqlParameter("@sims_admission_date_created", System.DateTime.Now.Date),
                            new SqlParameter("@sims_admission_user_code", "")
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            message.strMessage = "Admission Marks Updated Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                        else
                        {
                            inserted = false;
                            return Request.CreateResponse(HttpStatusCode.OK, "Fail");
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }
            catch (Exception x)
            {
                message.strMessage = "Error In Updating Admission Marks Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUD_Insert_Admission_Doc")]
        public HttpResponseMessage CUD_Insert_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Insert_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Insert_Admission_Doc"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("insert_admission_doc",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                            new SqlParameter("@sims_admission_doc_path", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify", simsobj.sims_admission_doc_verify== true?"1":"0")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUD_Delete_Admission_Doc")]
        public HttpResponseMessage CUD_Delete_Admission_Doc(string adm_no, string criteria_code, string doc_path)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Insert_Admission_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Insert_Admission_Doc"));
            // bool deleted = false;
            Message message = new Message();
            try
            {
                // if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_update_admissionmarks_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr","D"),
                            new SqlParameter("@sims_admission_number", adm_no),
                            new SqlParameter("@sims_admission_criteria_code",criteria_code),
                            new SqlParameter("@sims_admission_doc_path", doc_path),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Document Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUD_Update_Admission_Doc")]
        public HttpResponseMessage CUD_Update_Admission_Doc(Uccw229 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                            new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                             new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                            new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                            new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                         });
                        if (ins > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("CUD_Update_Admission_DocList")]
        public HttpResponseMessage CUD_Update_Admission_DocList(List<Uccw229> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            Array str;
            string str2;
            try
            {
                if (lstsimsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw229 simsobj in lstsimsobj)
                        {
                            str2 = simsobj.sims_admission_doc_path;
                            str = str2.Split(',');

                            foreach (string k in str)
                            {
                                string sims_admission_doc_path = k;
                                //}
                                int ins = db.ExecuteStoreProcedureforInsert("sims_update_admissiondoc",
                                    new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr","NNN"),
                                new SqlParameter("@sims_admission_doc_admission_number", simsobj.admis_num),
                                new SqlParameter("@sims_admission_doc_criteria_code", simsobj.sims_criteria_code),
                                // new SqlParameter("@sims_admission_doc_path_old", simsobj.sims_admission_doc_path),
                                // new SqlParameter("@sims_admission_doc_path_new", simsobj.sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_path_old", sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_path_new", sims_admission_doc_path),
                                new SqlParameter("@sims_admission_doc_status",simsobj.sims_admission_doc_status== true?"1":"0"),
                                new SqlParameter("@sims_admission_doc_verify",simsobj.sims_admission_doc_verify== true?"1":"0")
                                });
                                if (ins > 0)
                                {
                                    updated = true;
                                }
                                else
                                {
                                    updated = false;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("GetAdmissionTemplates")]
        public HttpResponseMessage GetAdmissionTemplates()
        {
            List<Sims010_Auth> temp_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'X')

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_type = dr["sims_msg_type"].ToString();
                            simsobj.sims_msg_subject = dr["sims_msg_subject"].ToString();
                            simsobj.sims_msg_body = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                            temp_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, temp_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, temp_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, temp_list);
            }
        }

        [Route("GetAdmissionTemplatesBody")]
        public HttpResponseMessage GetAdmissionTemplatesBody(string template_subject)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_msg_subject", template_subject),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_body = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
        }

        [Route("GetAdmission_EmailHistory")]
        public HttpResponseMessage GetAdmission_EmailHistory(string adnum)
        {
            List<Sims010_Auth> lst_ad = new List<Sims010_Auth>();
            lst_ad = GetAdmission_EmailIds1(adnum);
            string emailid = string.Empty;

            foreach (Sims010_Auth o in lst_ad)
            {
                emailid = o.emailid;
            }
            List<Comn_email> lst_emails = new List<Comn_email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@emailid", emailid),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn_email simsemail = new Comn_email();
                            simsemail.subject = dr["sims_email_subject"].ToString();
                            //simsemail.emaildate = Convert.ToDateTime(dr["sims_email_date"].ToString()).ToShortDateString();
                            simsemail.emaildate = db.UIDDMMYYYYformat(dr["sims_email_date"].ToString());
                            simsemail.body = dr["sims_email_message"].ToString();
                            lst_emails.Add(simsemail);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lst_emails);
            }
        }

        [Route("GetAdmission_EmailIds")]
        public HttpResponseMessage GetAdmission_EmailIds(string admission_nos)
        {
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Z'),
                            new SqlParameter("@adm_nos", admission_nos),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.emailid = dr["EmailID"].ToString();
                            simsobj.chk_email = true;
                            email_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, email_list);
            }
        }

        private List<Sims010_Auth> GetAdmission_EmailIds1(string admission_nos)
        {
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'Z'),
                            new SqlParameter("@adm_nos", admission_nos),
                         });
                    while (dr.Read())
                    {
                        Sims010_Auth simsobj = new Sims010_Auth();
                        simsobj.emailid = dr["EmailID"].ToString();
                        simsobj.chk_email = true;
                        email_list.Add(simsobj);
                    }
                }
                return email_list;
            }
            catch (Exception x)
            {
                return email_list;
            }
        }

        [Route("getCommunication")]
        public HttpResponseMessage getCommunication(string adm_no)
        {
            List<Uccw034> type_list = new List<Uccw034>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_comm_proc]",
                        new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "S"),
                       new SqlParameter("@ADMISSION_NUM", adm_no),
                       new SqlParameter("@COMM_MODE", ""),
                       new SqlParameter("@COMM_DATE", ""),
                       new SqlParameter("@COMM_DESC", ""),
                       new SqlParameter("@COMM_ENQRER_REM", ""),
                       new SqlParameter("@COMM_STATUS","A"),
                    });
                    while (dr.Read())
                    {
                        Uccw034 simsobj = new Uccw034();
                        simsobj.admis_num = adm_no;
                        simsobj.comm_desc = dr["sims_admission_comm_desc"].ToString();
                        simsobj.comm_method = dr["sims_admission_comm_mode"].ToString();
                        simsobj.comm_mode_desc = dr["Mode"].ToString();
                        simsobj.comm_date =db.UIDDMMYYYYformat(dr["sims_admission_comm_date"].ToString());
                        //simsobj.comm_date = DateTime.Parse(dr["sims_admission_comm_date"].ToString()).ToShortDateString();
                        simsobj.enq_rem = dr["sims_admission_comm_inquirer_remark"].ToString();
                        simsobj.status = dr["sims_admission_comm_status"].ToString();
                        type_list.Add(simsobj);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUDCommunication")]
        public HttpResponseMessage CUDCommunication(List<Uccw034> data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Uccw034 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_comm_proc]",
                                new List<SqlParameter>() 
                         {                             
                                new SqlParameter("@opr", "I"),//simsobj.opr
                                new SqlParameter("@ADMISSION_NUM", simsobj.admis_num),
                                new SqlParameter("@COMM_MODE", simsobj.comm_method),
                                new SqlParameter("@COMM_DATE", simsobj.comm_date),
                                new SqlParameter("@COMM_DESC", simsobj.comm_desc),
                                new SqlParameter("@COMM_ENQRER_REM", simsobj.enq_rem),
                                new SqlParameter("@COMM_STATUS", simsobj.status),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetNationalityCntDetails")]
        public HttpResponseMessage GetNationalityCntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetNationalityCntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetNationalityCntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            //new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.nationality_code = dr["nation_cnt"].ToString();
                            simsobj.nationality_name = dr["nationality"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetGendercntDetails")]
        public HttpResponseMessage GetGendercntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGendercntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetGendercntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.gender_cnt = dr["gender_cnt"].ToString();
                            simsobj.gender_code = dr["sims_admission_gender"].ToString();
                            simsobj.gender = dr["gender"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetEmployeeCodecntDetails")]
        public HttpResponseMessage GetEmployeeCodecntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeeCodecntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetEmployeeCodecntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Sims_admission_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'K'),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                            //new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.empl_code = dr["sims_admission_employee_code"].ToString();
                            simsobj.empcode_cnt = dr["emp_cnt"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetsiblingcntDetails")]
        public HttpResponseMessage GetsiblingcntDetails(string curr_code, string AcadmicYear, string gradeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";
            Log.Debug(string.Format(debug, "Student", "GetsiblingcntDetails", curr_code, AcadmicYear, gradeCode));

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (gradeCode == "undefined")
                    {
                        gradeCode = null;
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "L1"),
                            new SqlParameter("@CURR_CODE", curr_code),
                            new SqlParameter("@ACA_YEAR", AcadmicYear),
                            new SqlParameter("@GRADE_CODE", gradeCode),
                           // new SqlParameter("@sims_admission_status", admission_status)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            //simsobj.sibling_enroll = dr["sims_admission_sibling_enroll_number"].ToString();
                            simsobj.siblingNo_cnt = dr["sibling_cnt"].ToString();
                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetAdm_dashRpt")]
        public HttpResponseMessage GetAdm_dashRpt()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string reportName = "Sims.SIMR41";
            lst.Add(new SqlParameter("@OPR", "FR"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]", lst);//sims.sims_student_fee_TRANS1
                    if (dr.Read())
                    {
                        reportName = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, reportName);
        }

        [Route("GetVisible")]
        public HttpResponseMessage GetVisible()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "WS"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("GetuserID")]
        public HttpResponseMessage GetuserID(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetsiblingcntDetails(),PARAMETERS :: curr_code{2},AcadmicYear{3},gradeCode{4}";

            List<Sims010_Auth> type_list = new List<Sims010_Auth>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "WR"),
                            new SqlParameter("@user_name", user),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims010_Auth simsobj = new Sims010_Auth();
                            simsobj.comn_user_role_id = dr["comn_user_role_id"].ToString();

                            type_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, type_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
        }

        [Route("CUDUpdateAdm_Studentimage")]
        public HttpResponseMessage CUDUpdateAdm_Studentimage(Sims010_Edit data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateAdmission()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUDUpdateAdmission"));
            Sims010_Edit res = new Sims010_Edit();
            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Sims_admission_detail_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@ADMISSION_NUM", data.admission_number),
                            new SqlParameter("@sims_student_img", data.sims_student_img),
                             new SqlParameter("@sims_mother_img", data.sims_mother_img),
                              new SqlParameter("@sims_father_img", data.sims_father_img),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}