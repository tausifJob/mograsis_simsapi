﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/HouseAllocation")]
    public class HouseAllocationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getStudentNameHA")]
        public HttpResponseMessage getStudentNameHA(string curcode, string gradecode, string academicyear, string section)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<sims168> mod_list = new List<sims168>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_house_asign_proc",
                    new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M"),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@sims_academic_year", academicyear),
                            new SqlParameter("@sims_grade_code", gradecode),
                            new SqlParameter("@sims_section_code", section),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            sims168 simsobj = new sims168();
                            str1 = dr["sims_student_enroll_number"].ToString();
                            var v = from p in mod_list where p.enroll == str1 select p;

                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.enroll = dr["sims_student_enroll_number"].ToString();
                            simsobj.house_lst = new List<house>();

                            house h = new house();

                            h.house_code = dr["sims_house_code"].ToString();
                            h.sims_house_name = dr["sims_house_name"].ToString();
                            h.sims_status = dr["assign"].ToString().Equals("1") ? true : false;
                            try
                            {
                                h.assign_count = dr["assign_count"].ToString();
                            }
                            catch
                            { 
                            }

                            if (v.Count() == 0)
                            {
                                simsobj.house_lst.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).house_lst.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("allgetHouse")]
        public HttpResponseMessage allgetHouse(string curcode, string academicyear)
        {
            List<Sims029> house = new List<Sims029>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_asign_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", 'D'),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@sims_academic_year", academicyear)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();
                            obj.sims_house_code = dr["sims_house_code"].ToString();
                            obj.sims_house_name = dr["sims_house_name"].ToString();
                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("getallgetHousesibling")]
        public HttpResponseMessage getallgetHousesibling(String studentlist)
        {
            if (studentlist == "undefined" || studentlist == "null") {
                studentlist = null;
            }
            List<Sims029> house = new List<Sims029>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_asign_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", 'Z'),
                            new SqlParameter("@student_List", studentlist)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();
                            obj.sims_parent_number = dr["sims_parent_number"].ToString();
                            obj.parent_name = dr["parent_name"].ToString();
                            obj.sims_sibling_student_enroll_number = dr["sims_sibling_student_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_academic_year=dr["sims_academic_year"].ToString();
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.house_name = dr["house_name"].ToString();
                            obj.house_code = dr["house_code"].ToString();
                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


        [Route("getallHouseFilterhome")]
        public HttpResponseMessage getallHouseFilterhome(string curcode, string academicyear, string search_block)
        {
            
            List<Sims029> house = new List<Sims029>();
            try
            {

                //if(search_block == "NULL") 
                //{
                //    search_block = null;
                //}
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_asign_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", 'Z'),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@sims_academic_year", academicyear),
                            new SqlParameter("@search_block", search_block)
                             

                        }
                         );

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();
                            obj.sims_parent_number = dr["sims_parent_number"].ToString();
                            obj.parent_name = dr["parent_name"].ToString();
                            obj.sims_sibling_student_enroll_number = dr["sims_sibling_student_enroll_number"].ToString();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.house_name = dr["house_name"].ToString();
                            obj.house_code = dr["house_code"].ToString();
                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


        [Route("CUDSiblingUpdate")]
        public HttpResponseMessage CUDSiblingUpdate(List<Sims024> data)
        {

            bool inserted = false;
            //Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims024 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_house_asign_proc]",
                                new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@house_name", simsobj.house_name),                            
                            new SqlParameter("@parent_number", simsobj.sims_parent_number),
                            
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }

                            
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        [Route("getAllhouseName")]
        public HttpResponseMessage getAllhouseName()
        {
            List<Sims029> house = new List<Sims029>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_asign_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", 'B'),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();
                            obj.sims_house_code = dr["sims_house_code"].ToString();
                            obj.sims_house_name = dr["sims_house_name"].ToString();
                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }
        [Route("getallgetHousesiblingParent")]
        public HttpResponseMessage getallgetHousesiblingParent(String studentlist)
       {
            if (studentlist == "undefined" || studentlist == "null")
            {
                studentlist = null;
            }
            List<Sims029> house = new List<Sims029>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_asign_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", 'Z'),
                            new SqlParameter("@student_List", studentlist)
                            //new SqlParameter("@sims_parent_number", sims_parent_number),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();
                            obj.student_name = dr["student_name"].ToString();
                            obj.sims_sibling_student_enroll_number = dr["sims_sibling_student_enroll_number"].ToString();
                            obj.house_name = dr["house_name"].ToString();
                            obj.house_code = dr["house_code"].ToString();

                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }



        [Route("HouseAllOcCUD")]
        public HttpResponseMessage HouseAllOcCUD(List<sims168> lst)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "HouseCUD", lst));

            bool inserted = false;
            try
            {

                foreach (sims168 simsobj in lst)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_house_asign_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr","U"),
                          new SqlParameter("@student_List",simsobj.enroll),
                          new SqlParameter("@sims_student_house",simsobj.house_code),
                          //new SqlParameter("@sims_cur_code",simsobj.curr_code),
                          //new SqlParameter("@sims_academic_year",simsobj.academic_year),
                          //new SqlParameter("@sims_grade_code",simsobj.grade_code)


                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        

    }
}