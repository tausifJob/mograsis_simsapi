﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;

using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/SectionTerm")]
    [BasicAuthentication]
    public class SectionTermController : ApiController
    {


        public string section_term_section_code { get; set; }private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims059> lstCuriculum = new List<Sims059>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "G"),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 sequence = new Sims059();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims059> lstModules = new List<Sims059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@OPR", "R"),
                            new SqlParameter("@sims_cur_code", curCode)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 sequence = new Sims059();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllCurLevelName")]
        public HttpResponseMessage getAllCurLevelName(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCurLevelName(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAllCurLevelName", cur_code, academic_year));

            List<Sims059> cur_list = new List<Sims059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                       new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "L"),
                               new SqlParameter("@sims_cur_code", cur_code), 
                               new SqlParameter("@sims_academic_year",  academic_year), 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 objNew = new Sims059();
                            objNew.section_term_cur_level_code = dr["sims_cur_level_code"].ToString();
                            objNew.section_term_cur_level_name = dr["sims_cur_level_name_en"].ToString();
                            cur_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, cur_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, cur_list);
            }
        }

        [Route("getAllCurLevelGradeName")]
        public HttpResponseMessage getAllCurLevelGradeName(string cur_code, string academic_year, string cur_level_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCurLevelGradeName(),PARAMETERS :: CURCODE{2},ACAYEAR{3},CURLEVELCODE{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAllCurLevelGradeName", cur_code, academic_year, cur_level_code));

            List<Sims059> grade_list = new List<Sims059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "sims.sims_grade g inner join [sims].[sims_cur_level_grade] clg on g.sims_academic_year=clg.sims_academic_year and g.sims_cur_code=clg.sims_cur_code and g.sims_grade_code=clg.sims_grade_code"),
                                new SqlParameter("@tbl_col_name1", "clg.sims_grade_code,g.sims_grade_name_en"),
                                new SqlParameter("@tbl_cond", "clg.[sims_cur_code]='" + cur_code + "' and clg.[sims_academic_year]='" + academic_year + "' and clg.[sims_cur_level_code]='" + cur_level_code + "'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 simsobj = new Sims059();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllSectionName")]
        public HttpResponseMessage getAllSectionName(string cur_name, string academic_year, string grade_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getAllSectionName"));

            List<Sims059> section_list = new List<Sims059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_section]"),
                                new SqlParameter("@tbl_col_name1", "[sims_section_name_en],sims_section_code"),
                                new SqlParameter("@tbl_cond", "sims_status='A' and [sims_cur_code]=(Select [sims_cur_code] from [sims].[sims_cur] where [sims_cur_full_name_en]='"+cur_name+"') and [sims_academic_year]=" + academic_year + " and [sims_grade_code]=(Select [sims_grade_code] from [sims].[sims_grade] where [sims_grade_name_en]=" + "'" + grade_name + "' and [sims_cur_code]=(Select [sims_cur_code] from [sims].[sims_cur] where [sims_cur_full_name_en]='"+cur_name+"') and [sims_academic_year]='" + academic_year+"')")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 objNew = new Sims059();
                            objNew.sims_section_code_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_code_name = dr["sims_section_name_en"].ToString();
                            objNew.section_fee_section_name = dr["sims_section_name_en"].ToString();
                            objNew.screening_section_code_name = dr["sims_section_name_en"].ToString();
                            objNew.std_section_name_en = dr["sims_section_name_en"].ToString();
                            objNew.sims_report_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_report_section_code = dr["sims_section_code"].ToString();
                            section_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, section_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, section_list);
            }
        }

        [Route("getAllTermDescription")]
        public HttpResponseMessage getAllTermDescription(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTermDescription(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAllTermDescription", cur_code, academic_year));

            List<Sims059> term_list = new List<Sims059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_academic_year",  academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 simsobj = new Sims059();


                            simsobj.section_term_code = dr["sims_term_code"].ToString();
                            simsobj.section_term_name = dr["sims_term_desc_en"].ToString();

                            simsobj.section_term2_code = dr["sims_term_code"].ToString();
                            simsobj.section_term2_name = dr["sims_term_desc_en"].ToString();

                            simsobj.section_term3_code = dr["sims_term_code"].ToString();
                            simsobj.section_term3_name = dr["sims_term_desc_en"].ToString();

                            simsobj.section_term4_name = dr["sims_term_desc_en"].ToString();
                            simsobj.section_term4_code = dr["sims_term_code"].ToString();

                            simsobj.section_term5_name = dr["sims_term_desc_en"].ToString();
                            simsobj.section_term5_code = dr["sims_term_code"].ToString();

                            simsobj.section_term6_name = dr["sims_term_desc_en"].ToString();
                            simsobj.section_term6_code = dr["sims_term_code"].ToString();

                            simsobj.sims_term_start_date = dr["sims_term_start_date"].ToString();
                            simsobj.sims_term_end_date = dr["sims_term_end_date"].ToString();
                            term_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, term_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, term_list);
            }
        }

        [Route("getAllSectionTerm")]
        public HttpResponseMessage getAllSectionTerm()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getAllSectionTerm"));

            List<Sims059> sectionterm_list = new List<Sims059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_term",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S')                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims059 simsobj = new Sims059();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name"].ToString();
                            simsobj.section_term_code = dr["sims_section_term_1"].ToString();
                            simsobj.section_term_name = dr["sims_section_term1_name"].ToString();


                            if (!string.IsNullOrEmpty(dr["sims_section_term1_start_date"].ToString()))
                            {
                                simsobj.section_term_start_date = db.UIDDMMYYYYformat(dr["sims_section_term1_start_date"].ToString());
                                simsobj.section_term_end_date = db.UIDDMMYYYYformat(dr["sims_section_term1_end_date"].ToString());
                            }
                            simsobj.section_term2_code = dr["sims_section_term_2"].ToString();
                            simsobj.section_term2_name = dr["sims_section_term2_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_section_term2_start_date"].ToString()))
                            {
                                simsobj.section_term2_start_date = db.UIDDMMYYYYformat(dr["sims_section_term2_start_date"].ToString());
                                simsobj.section_term2_end_date = db.UIDDMMYYYYformat(dr["sims_section_term2_end_date"].ToString());
                            }
                            simsobj.section_term3_code = dr["sims_section_term_3"].ToString();
                            simsobj.section_term3_name = dr["sims_section_term3_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_section_term3_start_date"].ToString()))
                            {
                                simsobj.section_term3_start_date = db.UIDDMMYYYYformat(dr["sims_section_term3_start_date"].ToString());
                                simsobj.section_term3_end_date = db.UIDDMMYYYYformat(dr["sims_section_term3_end_date"].ToString());
                            }
                            simsobj.section_term4_code = dr["sims_section_term_4"].ToString();
                            simsobj.section_term4_name = dr["sims_section_term4_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_section_term4_start_date"].ToString()))
                            {
                                simsobj.section_term4_start_date = db.UIDDMMYYYYformat(dr["sims_section_term4_start_date"].ToString());
                                simsobj.section_term4_end_date = db.UIDDMMYYYYformat(dr["sims_section_term4_end_date"].ToString());
                            }
                            simsobj.section_term5_code = dr["sims_section_term_5"].ToString();
                            simsobj.section_term5_name = dr["sims_section_term5_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_section_term5_start_date"].ToString()))
                            {
                                simsobj.section_term5_start_date = db.UIDDMMYYYYformat(dr["sims_section_term5_start_date"].ToString());
                                simsobj.section_term5_end_date = db.UIDDMMYYYYformat(dr["sims_section_term5_end_date"].ToString());
                            }
                            simsobj.section_term6_code = dr["sims_section_term_6"].ToString();
                            simsobj.section_term6_name = dr["sims_section_term6_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_section_term6_start_date"].ToString()))
                            {
                                simsobj.section_term6_start_date = db.UIDDMMYYYYformat(dr["sims_section_term6_start_date"].ToString());
                                simsobj.section_term6_end_date = db.UIDDMMYYYYformat(dr["sims_section_term6_end_date"].ToString());
                            }
                            if (dr["sims_status"].ToString().Equals("A"))
                                simsobj.section_term_status = true;
                            else
                                simsobj.section_term_status = false;

                            sectionterm_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, sectionterm_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, sectionterm_list);
        }

        [Route("getAllPriandcurAcademicYear")]
        public HttpResponseMessage getAllPriandcurAcademicYear(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPriandcurAcademicYear(),PARAMETERS :: CURCODE{2}";
            Log.Debug(string.Format(debug, "COMMON", "getAllPriandcurAcademicYear", cur_code));

            List<SimsClass> acayear_list = new List<SimsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsClass simsobj = new SimsClass();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_academic_year_start_date"].ToString()))
                                simsobj.sims_academic_year_start_date = DateTime.Parse(dr["sims_academic_year_start_date"].ToString()).ToShortDateString();
                            if (!string.IsNullOrEmpty(dr["sims_academic_year_end_date"].ToString()))
                                simsobj.sims_academic_year_end_date = DateTime.Parse(dr["sims_academic_year_end_date"].ToString()).ToShortDateString();
                            acayear_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAllGrades", cur_code, ac_year));

            List<SimsClass> grade_list = new List<SimsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Z"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsClass simsobj = new SimsClass();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("getAllSections")]
        public HttpResponseMessage getAllSections(string cur_code, string ac_year, string g_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSections(),PARAMETERS :: CURCODE{2},ACAYEAR{3},GRADECODE{4}";
            Log.Debug(string.Format(debug, "COMMON", "getAllSections", cur_code, ac_year, g_code));

            List<SimsClass> section_list = new List<SimsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                                new SqlParameter("@sims_g_code", g_code),
                                new SqlParameter("@sims_cur_name",cur_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsClass simsobj = new SimsClass();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_grade_code = g_code;
                            section_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, section_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, section_list);
        }

        [Route("Update_section_term")]
        public HttpResponseMessage Update_section_term(string gradeobj)
        {


            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Update_section_term(),PARAMETERS ::gradeobj{2}";
            Log.Debug(string.Format(debug, "STUDENT", "Update_section_term", gradeobj));

            Sims059 from = new Sims059();

            if (gradeobj == "undefined")
            {
                gradeobj = null;
            }
            else
            {
                from = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims059>(gradeobj);
            }

            Message message = new Message();

            try
            {
                if (from != null)
                {

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_section_term", new List<SqlParameter>() 
                         {
                    new SqlParameter("@opr", 'U'),
                    new SqlParameter("@sims_cur_code",from.sims_cur_code), 
                    new SqlParameter("@sims_academic_year", from.sims_academic_year),
                    new SqlParameter("@sims_cur_level_code",from.section_term_cur_level_code),
                    new SqlParameter("@sims_grade_code",from.sims_grade_code),
                    new SqlParameter("@sims_section_code",from.sims_section_code),
                    new SqlParameter("@sims_section_term_code",from.section_term_name),
                    new SqlParameter("@sims_section_term_start_date", db1.DBYYYYMMDDformat(from.section_term_start_date)),
                    new SqlParameter("@sims_section_term_end_date", db1.DBYYYYMMDDformat(from.section_term_end_date)),
                    new SqlParameter("@sims_section_term_2", from.section_term2_name),
                    new SqlParameter("@sims_section_term2_start_date",  db1.DBYYYYMMDDformat(from.section_term2_start_date)),
                    new SqlParameter("@sims_section_term2_end_date",  db1.DBYYYYMMDDformat(from.section_term2_end_date)),
                    new SqlParameter("@sims_section_term_3", from.section_term3_name),
                    new SqlParameter("@sims_section_term3_start_date", db1.DBYYYYMMDDformat(from.section_term3_start_date)),
                    new SqlParameter("@sims_section_term3_end_date",  db1.DBYYYYMMDDformat(from.section_term3_end_date)),
                    new SqlParameter("@sims_section_term_4", from.section_term4_name),
                    new SqlParameter("@sims_section_term4_start_date",  db1.DBYYYYMMDDformat(from.section_term4_start_date)),
                    new SqlParameter("@sims_section_term4_end_date",  db1.DBYYYYMMDDformat(from.section_term4_end_date)),
                    new SqlParameter("@sims_section_term_5", from.section_term5_name),
                    new SqlParameter("@sims_section_term5_start_date",  db1.DBYYYYMMDDformat(from.section_term5_start_date)),
                    new SqlParameter("@sims_section_term5_end_date",  db1.DBYYYYMMDDformat(from.section_term5_end_date)),
                    new SqlParameter("@sims_section_term_6", from.section_term6_name),
                    new SqlParameter("@sims_section_term6_start_date",  db1.DBYYYYMMDDformat(from.section_term6_start_date)),
                    new SqlParameter("@sims_section_term6_end_date",  db1.DBYYYYMMDDformat(from.section_term6_end_date)),
                    new SqlParameter("@sims_status", from.section_term_status==true?"A":"I")

                         });
                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "Section Term updated Successfully";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {

                            message.strMessage = "Section Term Not updated";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }

                    }
                }

                else
                {

                    message.strMessage = "Error In Parsing Data";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }

            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("InsertSims_Section_Term")]
        public HttpResponseMessage InsertSims_Section_Term(string simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_Section_Term(),PARAMETERS ::simsobj{2}";
            Log.Debug(string.Format(debug, "STUDENT", "InsertSims_Section_Term", simsobj));



            Sims059 from = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims059>(simsobj);

            Message message = new Message();

            try
            {
                if (from != null)
                {

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_section_term", new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", 'I'),
                new SqlParameter("@sims_cur_code", from.sims_cur_code),
                new SqlParameter("@sims_academic_year", from.sims_academic_year),
                new SqlParameter("@sims_cur_level_code", from.section_term_cur_level_code),
                new SqlParameter("@sims_grade_code", from.sims_grade_code),
                new SqlParameter("@sims_section_code", from.sims_section_code),
               
                new SqlParameter("@sims_section_term_code", from.section_term_name),
                //new SqlParameter("@sims_section_term_start_date", string.IsNullOrEmpty(from.section_term_start_date)?null:from.section_term_start_date),
                //new SqlParameter("@sims_section_term_end_date",  string.IsNullOrEmpty(from.section_term_end_date)?null:from.section_term_end_date),
                new SqlParameter("@sims_section_term_start_date", db1.DBYYYYMMDDformat(from.section_term_start_date)),
                new SqlParameter("@sims_section_term_end_date",  db1.DBYYYYMMDDformat(from.section_term_end_date)),

                new SqlParameter("@sims_section_term_2", from.section_term2_name),
                //new SqlParameter("@sims_section_term2_start_date",  string.IsNullOrEmpty(from.section_term2_start_date)?null:from.section_term2_start_date),
                //new SqlParameter("@sims_section_term2_end_date",  string.IsNullOrEmpty(from.section_term2_end_date)?null:from.section_term2_end_date),
                new SqlParameter("@sims_section_term2_start_date",  db1.DBYYYYMMDDformat(from.section_term2_start_date)),
                new SqlParameter("@sims_section_term2_end_date",  db1.DBYYYYMMDDformat(from.section_term2_end_date)),


                new SqlParameter("@sims_section_term_3", from.section_term3_name),
                //new SqlParameter("@sims_section_term3_start_date", string.IsNullOrEmpty(from.section_term3_start_date)?null:from.section_term3_start_date),
                //new SqlParameter("@sims_section_term3_end_date", string.IsNullOrEmpty(from.section_term3_end_date)?null:from.section_term3_end_date),
                new SqlParameter("@sims_section_term3_start_date", db1.DBYYYYMMDDformat(from.section_term3_start_date)),
                new SqlParameter("@sims_section_term3_end_date", db1.DBYYYYMMDDformat(from.section_term3_end_date)),
                

                new SqlParameter("@sims_section_term_4", from.section_term4_name),
                //new SqlParameter("@sims_section_term4_start_date", string.IsNullOrEmpty(from.section_term4_start_date)?null:from.section_term4_start_date),
                //new SqlParameter("@sims_section_term4_end_date",  string.IsNullOrEmpty(from.section_term4_end_date)?null:from.section_term4_end_date),
                new SqlParameter("@sims_section_term4_start_date", db1.DBYYYYMMDDformat(from.section_term4_start_date)),
                new SqlParameter("@sims_section_term4_end_date",  db1.DBYYYYMMDDformat(from.section_term4_end_date)),
                

                new SqlParameter("@sims_section_term_5", from.section_term5_name),
                //new SqlParameter("@sims_section_term5_start_date", string.IsNullOrEmpty(from.section_term5_start_date)?null:from.section_term5_start_date),
                //new SqlParameter("@sims_section_term5_end_date", string.IsNullOrEmpty(from.section_term5_end_date)?null:from.section_term5_end_date),
                new SqlParameter("@sims_section_term5_start_date", db1.DBYYYYMMDDformat(from.section_term5_start_date)),
                new SqlParameter("@sims_section_term5_end_date", db1.DBYYYYMMDDformat(from.section_term5_end_date)),
                

                new SqlParameter("@sims_section_term_6", from.section_term6_name),
                //new SqlParameter("@sims_section_term6_start_date",  string.IsNullOrEmpty(from.section_term6_start_date)?null:from.section_term6_start_date),
                //new SqlParameter("@sims_section_term6_end_date",  string.IsNullOrEmpty(from.section_term6_end_date)?null:from.section_term6_end_date),
                new SqlParameter("@sims_section_term6_start_date",  db1.DBYYYYMMDDformat(from.section_term6_start_date)),
                new SqlParameter("@sims_section_term6_end_date",  db1.DBYYYYMMDDformat(from.section_term6_end_date)),

                new SqlParameter("@sims_status", from.section_term_status.Equals(true)?"A":"I")

                      });

                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "Section Term Inserted Successfully";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            return Request.CreateResponse(HttpStatusCode.OK, message);

                        }
                        else
                        {

                            message.strMessage = "Section Term Not Inserted";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;

                        }

                    }
                }
                else
                {

                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;

                }

                return Request.CreateResponse(HttpStatusCode.OK, message);

            }

            catch (Exception ex)
            {
                message.strMessage = "Record already exist ";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, message);

        }

        [Route("getSection_Term")]
        public HttpResponseMessage getSection_Term(string cur_code, string academic_year, string term_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Section_Term(),PARAMETERS :: CURCODE{2},ACAYEAR{3},TERM_CODE{4}";
            Log.Debug(string.Format(debug, "COMMON", "Section_Term", cur_code, academic_year, term_code));



            List<Sims059> termlist = new List<Sims059>();

            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_section_term", new List<SqlParameter>() 
                         {
                
               new SqlParameter("@opr",'P'),
	           new SqlParameter("@sims_term_code",term_code),
	           new SqlParameter("@sims_cur_code",cur_code), 
               new SqlParameter("@sims_academic_year",academic_year)
                         });

                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            Sims059 lst = new Sims059();
                            lst.sims_term_start_date = db1.UIDDMMYYYYformat(dr1["sims_term_start_date"].ToString());
                            lst.sims_term_end_date = db1.UIDDMMYYYYformat(dr1["sims_term_end_date"].ToString());


                            lst.sims_term_desc_en = dr1["sims_term_desc_en"].ToString();
                            lst.sims_term_code = dr1["sims_term_code"].ToString();

                            termlist.Add(lst);
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, termlist);
        }


        [Route("cudSims_Section_Term")]
        public HttpResponseMessage cudSims_Section_Term(List<Sims059> delete)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    foreach (Sims059 del in delete)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_section_term", new List<SqlParameter>() 
                         {

              new SqlParameter("@opr",'D'),
              new SqlParameter("@sims_cur_code",del.sims_cur_code),
              new SqlParameter("@sims_academic_year",del.sims_academic_year), 
              new SqlParameter("@sims_grade_code", del.sims_grade_code),
			  new SqlParameter("@sims_section_code",del.sims_section_code),
              new SqlParameter("@sims_section_term_code",del.section_term_code),

                         });

                        if (dr1.RecordsAffected > 0)
                        {

                            message.strMessage = "Section Term Record Deleted succssefully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;

                        }
                        else
                        {
                            message.strMessage = "Section Term Record Not Deleted succssefully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;

                        }
                        dr1.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

    }
}

