﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/ReAdmission")]
    public class ReAdmissionController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_studentlist")]
        public HttpResponseMessage Get_studentlist(string enroll_no)
        {
            List<Sims613> lstcertificate = new List<Sims613>();
            if (enroll_no == "undefined" || enroll_no == "\"\"")
            {
                enroll_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),
                           new SqlParameter("@sims_enroll_number", enroll_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims613 obj = new Sims613();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_enroll_no = dr["sims_enroll_number"].ToString();
                            obj.stud_full_name = dr["S_name"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [Route("CUDRe_admission")]
        public HttpResponseMessage CUDRe_admission(List<Sims613> data)
        {
            Message message = new Message();
            string inserted_msg = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims613 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Re_ADMISSION_Student_proc]",
                        new List<SqlParameter>()
                     {
                        new SqlParameter("@opr", simsobj.opr),
                        new SqlParameter("@ENROLL", simsobj.sims_enroll_no),
                        new SqlParameter("@USERCODE", simsobj.username),
                        new SqlParameter("@old_cur", simsobj.sims_cur_code),
                        new SqlParameter("@old_aca", simsobj.sims_academic_year),
                        new SqlParameter("@old_grd", simsobj.sims_grade_code),
                        new SqlParameter("@old_sec", simsobj.sims_section_code),
                        new SqlParameter("@new_cur", simsobj.sims_cur_code_new),
                        new SqlParameter("@new_aca", simsobj.sims_academic_year_new),
                        new SqlParameter("@new_grd", simsobj.sims_grade_code_new),
                        new SqlParameter("@new_sec", simsobj.sims_section_code_new),  
                        new SqlParameter("@attendace_date",db.DBYYYYMMDDformat(simsobj.sims_attendance_date)),
                        new SqlParameter("@fee_date",db.DBYYYYMMDDformat(simsobj.sims_fee_from_date)),
                        new SqlParameter("@REASON", simsobj.sims_readmission_remark),
                     });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                inserted_msg = dr["status_msg"].ToString(); 
                                //if (!string.IsNullOrEmpty(dr["status_msg"].ToString()))
                                //{
                                //    inserted_msg = dr["status_msg"].ToString();                   
                                //}
                                //else
                                //{
                                //    inserted_msg = "0";
                                //}

                            }
                        }
                        //if (dr.RecordsAffected > 0)
                        //{
                        //    inserted = true;
                        //}
                        //else
                        //{
                        //    inserted = false;
                        //}
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted_msg);
                }

            }
            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted_msg);
        }


        //public HttpResponseMessage CUDRe_admission(List<Sims613> data)
        //{
        //    Message message = new Message();
        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (Sims613 simsobj in data)
        //            {
        //                int ins = db.ExecuteStoreProcedureforInsert("[sims].[Re_ADMISSION_Student]",
        //                new List<SqlParameter>()
        //             {
        //                new SqlParameter("@opr", simsobj.opr),
        //                new SqlParameter("@ENROLL", simsobj.sims_enroll_no),
        //                new SqlParameter("@old_cur", simsobj.sims_cur_code),
        //                new SqlParameter("@old_aca", simsobj.sims_academic_year),
        //                new SqlParameter("@old_grd", simsobj.sims_grade_code),
        //                new SqlParameter("@old_sec", simsobj.sims_section_code),
        //                new SqlParameter("@new_cur", simsobj.sims_cur_code_new),
        //                new SqlParameter("@new_aca", simsobj.sims_academic_year_new),
        //                new SqlParameter("@new_grd", simsobj.sims_grade_code_new),
        //                new SqlParameter("@new_sec", simsobj.sims_section_code_new),  
        //             });

        //                if (ins > 0)
        //                {
        //                    inserted = true;
        //                }
        //                else
        //                {
        //                    inserted = false;
        //                }
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //        }

        //    }
        //    catch (Exception x)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}
    }
}