﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Linq;
using SIMSAPI.Models.SIMS.simsClass;
//using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.SchoolActivity
{
    [RoutePrefix("api/Survey")]
    [BasicAuthentication]
    public class SurveyController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("get_allSurvey")]
        public HttpResponseMessage get_allSurvey()
        {
            List<Sims692> mod_list = new List<Sims692>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims692 simsobj = new Sims692();
                            simsobj.sims_survey_code = dr["sims_survey_code"].ToString();
                            simsobj.sims_survey_subject = dr["sims_survey_subject"].ToString();
                            //simsobj.sims_mom_type_status = dr["sims_mom_type_status"].ToString().Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("get_SurveyQuestion")]
        public HttpResponseMessage get_SurveyQuestion()
        {
            List<Sims692> mod_list = new List<Sims692>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","Q")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims692 simsobj = new Sims692();
                            //simsobj.answers = new List<ans>();
                            string str = dr["sims_survey_question_code"].ToString();
                            var v = (from p in mod_list where p.sims_survey_question_code == str select p);
                            simsobj.sims_survey_question_code = dr["sims_survey_question_code"].ToString();
                            simsobj.sims_survey_question_desc_en = dr["sims_survey_question_desc_en"].ToString();
                            simsobj.sims_survey_question_type = dr["sims_survey_question_type"].ToString();
                            simsobj.sims_survey_code = dr["sims_survey_code"].ToString();
                            simsobj.answers = new List<ans>();
                            ans as1 = new ans();

                            as1.sims_survey_question_answer_code = dr["sims_survey_question_answer_code"].ToString();
                            as1.sims_survey_question_answer_desc_en = dr["sims_survey_question_answer_desc_en"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.answers.Add(as1);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).answers.Add(as1);
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
                
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUD_Survey")]
        public HttpResponseMessage CUD_Survey(Sims692 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Survey()";
            Log.Debug(string.Format(debug, "PP", "CUD_Survey"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_survey_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_survey_user_response_user_code", simsobj.sims_survey_user_response_user_code),
                            new SqlParameter("@sims_survey_code", simsobj.sims_survey_code),
                            new SqlParameter("@questionanswer", simsobj.questionanswer),
                           // new SqlParameter("@sims_survey_user_response_answer_text", simsobj.sims_survey_user_response_answer_text),
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}