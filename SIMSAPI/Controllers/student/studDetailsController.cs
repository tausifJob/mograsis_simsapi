﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

using SIMSAPI.Models.ParentClass;
using System.Web;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.COMMON;
using System.IO;
namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/student/fileUpload")]
    public class studDetailsController : ApiController
    {
        [Route("SDFU_insert")]
        public HttpResponseMessage SDFU_insert(SDFU SDFUobj)
        {
            string insertedSrNo = string.Empty;
            try
            {
                if (SDFUobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_details_bulk_upload_proc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_cur_code",SDFUobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year",SDFUobj.sims_academic_year),
                            new SqlParameter("@sims_file_uploaded_by",SDFUobj.sims_file_uploaded_by),
                            //new SqlParameter("@sims_file_uploaded_datetime",SDFUobj.sims_file_uploaded_datetime),
                            new SqlParameter("@sims_file_path",SDFUobj.sims_file_path),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (dr.Read())
                            {
                                insertedSrNo = dr["sr_no"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, '0');
            }
            return Request.CreateResponse(HttpStatusCode.OK, insertedSrNo);
        }

        [Route("SDFUDetails_insert")]
        public HttpResponseMessage SDFUDetails_insert(List<SDFU> SDFUobj)
        {
            string insertedSrNo = string.Empty;
            int ins;
            bool inserted = false;
            try
            {

                foreach (SDFU cr in SDFUobj)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        ins = db1.ExecuteStoreProcedureforInsert("sims.sims_student_details_bulk_upload_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "IU"),
                            new SqlParameter("@sims_sr_no",cr.sims_sr_no),
		                    new SqlParameter("@sims_admission_application_number",cr.sims_admission_application_number),
			                new SqlParameter("@sims_admission_date",cr.sims_admission_date),
			                new SqlParameter("@sims_admission_cur_code",cr.sims_admission_cur_code),
			                new SqlParameter("@sims_admission_academic_year",cr.sims_admission_academic_year),
			                new SqlParameter("@sims_admission_grade_code",cr.sims_admission_grade_code), 
			                new SqlParameter("@sims_admission_tentative_joining_date",cr.sims_admission_tentative_joining_date),
			                new SqlParameter("@sims_admission_passport_first_name_en",cr.sims_admission_passport_first_name_en), 
			                new SqlParameter("@sims_admission_passport_middle_name_en",cr.sims_admission_passport_middle_name_en),
			                new SqlParameter("@sims_admission_passport_last_name_en",cr.sims_admission_passport_last_name_en),
			                new SqlParameter("@sims_admission_gender",cr.sims_admission_gender),
			                new SqlParameter("@sims_admission_dob",cr.sims_admission_dob),
			                new SqlParameter("@sims_admission_father_first_name",cr.sims_admission_father_first_name),
			                new SqlParameter("@sims_admission_father_middle_name",cr.sims_admission_father_middle_name),
			                new SqlParameter("@sims_admission_father_last_name",cr.sims_admission_father_last_name),
			                new SqlParameter("@sims_admission_father_mobile",cr.sims_admission_father_mobile),
			                new SqlParameter("@sims_admission_father_email",cr.sims_admission_father_email),
			                new SqlParameter("@sims_admission_mother_first_name",cr.sims_admission_mother_first_name),
			                new SqlParameter("@sims_admission_mother_middle_name",cr.sims_admission_mother_middle_name),
			                new SqlParameter("@sims_admission_mother_last_name",cr.sims_admission_mother_last_name),
			                new SqlParameter("@sims_admission_mother_mobile",cr.sims_admission_mother_mobile),
			                new SqlParameter("@sims_admission_mother_email",cr.sims_admission_mother_email)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, '0');
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("SDFUselectAll")]
        public HttpResponseMessage SDFUselectAll()
        {
            List<Sims042> objArray = new List<Sims042>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_details_bulk_upload_proc",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "SA")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims042 obj = new Sims042();

                            obj.student_enroll_no = dr["sims_student_enroll_number"].ToString();
                            obj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            obj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            obj.sims_parent_father_first_name = dr["sims_parent_father_first_name"].ToString();
                            obj.sims_parent_father_last_name = dr["sims_parent_father_last_name"].ToString();
                            obj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            objArray.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, objArray);
        }

        [Route("SDFU_process")]
        public HttpResponseMessage SDFU_process(SDFU SDFUobj)
        {
            string insertedSrNo = string.Empty;
            try
            {
                if (SDFUobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_details_bulk_upload_details_proc",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@sims_cur_code",SDFUobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year",SDFUobj.sims_academic_year),
                            new SqlParameter("@sims_sr_no",SDFUobj.sims_sr_no),
                         });
                        //if (dr.RecordsAffected > 0)
                        //{
                        //    if (dr.Read())
                        //    {
                        //        insertedSrNo = dr["sr_no"].ToString();
                        //    }
                        //}
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, '0');
            }
            return Request.CreateResponse(HttpStatusCode.OK, insertedSrNo);
        }


        [Route("AcesCustomer_insert")]
        public HttpResponseMessage AcesCustomer_insert(List<AcesObj> AcesObj)
        {
            string insertedSrNo = string.Empty;
            int ins;
            bool inserted = false;
            try
            {
                foreach (AcesObj cr in AcesObj)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        ins = db1.ExecuteStoreProcedureforInsert("sims_aces_customer_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@country_code", cr.country_code),
                            new SqlParameter("@state_code", cr.state_code),
                            new SqlParameter("@region_code", cr.region_code),
                            new SqlParameter("@district_code", cr.district_code),
                            new SqlParameter("@city_code", cr.city_code),
                            new SqlParameter("@taluka_code", cr.taluka_code),
                            new SqlParameter("@area_code", cr.area_code),
                            new SqlParameter("@school_code", cr.school_code),
                            new SqlParameter("@school_type", cr.school_type),
                            new SqlParameter("@school_name", cr.school_name),
                            new SqlParameter("@school_short_name", cr.school_short_name),
                            new SqlParameter("@school_other_name", cr.school_other_name),
                            new SqlParameter("@school_logo", cr.school_logo),
                            new SqlParameter("@school_address", cr.school_address),
                            new SqlParameter("@contact_person", cr.contact_person),
                            new SqlParameter("@fax_no", cr.fax_no),
                            new SqlParameter("@tel_no", cr.tel_no),
                            new SqlParameter("@email", cr.email),
                            new SqlParameter("@website_url", cr.website_url),
                            new SqlParameter("@school_curriculum", cr.school_curriculum),
                            new SqlParameter("@agreement_date", cr.agreement_date),
                            new SqlParameter("@subscription_date", cr.subscription_date),
                            new SqlParameter("@subscription_expiry_date", cr.subscription_expiry_date),
                            new SqlParameter("@grace_30", cr.grace_30),
                            new SqlParameter("@grace_60", cr.grace_60),
                            new SqlParameter("@grace_90", cr.grace_90),
                            new SqlParameter("@status", cr.status),
                            new SqlParameter("@time_zone", cr.time_zone),
                            new SqlParameter("@latitude", cr.latitude),
                            new SqlParameter("@longitude", cr.longitude)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, '0');
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}