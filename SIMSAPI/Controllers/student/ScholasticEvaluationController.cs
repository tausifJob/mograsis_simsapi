﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/scholastic_evaluation")]
    public class ScholasticEvaluationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Activity Master



        [Route("GetActGroups")]
        public HttpResponseMessage GetActGroups(string cur_code, string acdemic_year)
        {
            List<sims_activity_group_master> mod_list = new List<sims_activity_group_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "GM"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_group_master simsobj = new sims_activity_group_master();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_activity_group_code = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_activity_group_desc = dr["sims_activity_group_desc"].ToString();
                            simsobj.sims_activity_group_status = dr["sims_activity_group_status"].ToString().Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetActCurName")]
        public HttpResponseMessage GetActCurName()
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "CS"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetActAcademic")]
        public HttpResponseMessage GetActAcademic(string cur_code)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "AY"),
                                new SqlParameter("@CUR_CODE", cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();

                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetActTerms")]
        public HttpResponseMessage GetActTerms(string cur_code, string acdemic_year)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "ST"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();

                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_name = dr["sims_term_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetIndicatorGroups")]
        public HttpResponseMessage GetIndicatorGroups(string cur_code, string acdemic_year, string group_code)
        {
            List<sims_indicator_group> mod_list = new List<sims_indicator_group>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "IG"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@config_sr_no", group_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_indicator_group simsobj = new sims_indicator_group();

                            simsobj.sims_indicator_group_code = dr["sims_indicator_group_code"].ToString();
                            simsobj.sims_activity_group_code = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_indicator_group_name = dr["sims_indicator_group_name"].ToString();
                            simsobj.sims_indicator_group_desc = dr["sims_indicator_group_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetActGrades")]
        public HttpResponseMessage GetActGrades(string cur_code, string acdemic_year, string user_name)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "GS"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@CREATED_USR", user_name),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();

                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetActSections")]
        public HttpResponseMessage GetActSections(string cur_code, string acdemic_year, string grade, string user_name)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "SS"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@GRADE_CODE", grade),
                                new SqlParameter("@CREATED_USR", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("ActSectionsSubjects")]
        public HttpResponseMessage ActSectionsSubjects(string cur_code, string acdemic_year, string act_group_code)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "SU"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@SECTION_CODE", act_group_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetActTypes")]
        public HttpResponseMessage GetActTypes()
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "CT")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("defineactivityConfig")]
        public HttpResponseMessage defineactivityConfig(string act_group_code, sims_activity_master obj)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'I'),
                                new SqlParameter("@OPR", "IC"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@SECTION_CODE", act_group_code),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code_lst),
                                new SqlParameter("@TERM_CODE", obj.sims_term_code),
                                new SqlParameter("@CONFIG_DESC", obj.sims_config_desc),
                                new SqlParameter("@CONFIG_TYPE",  obj.sims_type_code),
                                new SqlParameter("@CONFIG_VALUES", string.IsNullOrEmpty(obj.sims_type_value)?"0":obj.sims_type_value.ToString()),
                                new SqlParameter("@STATUS", obj.sims_activity_status==true?"A":"I" )
                         });
                    int t = dr.RecordsAffected;
                    res = t > 0 ? true : true;
                    return Request.CreateResponse(HttpStatusCode.OK, res);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }


        [Route("defineactivity")]
        public HttpResponseMessage defineactivity(string opr_param, sims_activity_master obj)
        {
            string opr = "I";
            opr = opr_param.Equals("I") ? "IA" : "UA";
            bool res = false;
            string sims_acativity_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'I'),
                                new SqlParameter("@OPR", opr),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@config_sr_no", obj.sims_config_sr_no),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@TERM_CODE", obj.sims_term_code),
                                new SqlParameter("@ACTIVITY_NAME", obj.sims_activity_name),
                                new SqlParameter("@ACTIVITY_NAME_AR", obj.sims_activity_name_ar),

                                new SqlParameter("@ACTIVITY_DESC", obj.sims_activity_desc),
                                new SqlParameter("@ACTIVITY_RMK",  obj.sims_activity_rmk),
                                new SqlParameter("@ACTIVITY_GRD_SCALE", obj.narr_grade_group_code),
                                new SqlParameter("@ACTIVITY_MAX_POINT", string.IsNullOrEmpty(obj.sims_activity_max_pnt)?"0":obj.sims_activity_max_pnt.ToString()),
                                new SqlParameter("@ACTIVITY_MIN_POINT", string.IsNullOrEmpty(obj.sims_activity_min_pnt)?"0":obj.sims_activity_min_pnt.ToString()),
                                new SqlParameter("@STATUS", obj.sims_activity_status==true?"A":"I" ),
                                new SqlParameter("@activity_st_no", obj.sims_activity_sr_no)

                         });
                    res = dr.RecordsAffected >= 1 ? true : false;
                    dr.Close();
                    dr.Dispose();
                }

                //int t = dr.RecordsAffected;
                //res = t > 0 ? true : true;
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }

        /*GET CONFIG FRO COPY ACITVITY */
        [Route("GetAllActivitiesConfigCopy")]
        public HttpResponseMessage GetAllActivitiesConfigCopy(string cur_code, string acdemic_year, string term, string sections)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "CO"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@TERM_CODE", term),
                                new SqlParameter("@SECTION_CODE", sections),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_acativity_config_name = dr["activity_config_name"].ToString();
                            simsobj.sims_config_sr_no = dr["sims_config_sr_no"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetnarrtiveGrades")]
        public HttpResponseMessage GetnarrtiveGrades()
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "NG")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.narr_grade_group_code = dr["narr_grade_group_code"].ToString();
                            simsobj.narr_grade_group_name = dr["narr_grade_group_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllActivitiesConfig")]
        public HttpResponseMessage GetAllActivitiesConfig(string cur_code, string acdemic_year, string term,string section)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "AS"),
                                new SqlParameter("@CUR_CODE", cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", acdemic_year),
                                new SqlParameter("@TERM_CODE", term),
                                new SqlParameter("@SECTION_CODE", section),

                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_acativity_config_name = dr["activity_config_name"].ToString();
                            simsobj.sims_config_sr_no = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_config_desc = dr["sims_config_desc"].ToString();
                            simsobj.sims_type_code = dr["sims_config_type"].ToString();
                            simsobj.sims_type_value = dr["sims_config_value"].ToString();
                            simsobj.sims_activity_status = dr["sims_status"].ToString().Equals("A") ? true : false;

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("AllSubActivities")]
        public HttpResponseMessage AllSubActivities(sims_activity obj)
        {
            List<sims_activity> acativities = new List<sims_activity>();
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@OPR_MAIN", 'S'),
                                    new SqlParameter("@OPR", "SA"),
                                    new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                    new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                    new SqlParameter("@config_sr_no",obj.sims_config_sr_no ),
                                    new SqlParameter("@SUBJECT_CODE",obj.sims_subject_code )

                                    
                                });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            sims_activity act = new sims_activity();
                            act.sims_config_sr_no = obj.sims_config_sr_no;
                            act.sims_activity_sr_no = dr1["sims_activity_code"].ToString();
                            act.sims_activity_name = dr1["sims_activity_name"].ToString();
                            act.sims_activity_name_ar = dr1["sims_activity_name_ar"].ToString();
                            act.sims_activity_desc = dr1["sims_activity_desc"].ToString();
                            act.sims_activity_rmk = dr1["sims_activity_remark"].ToString();
                            act.narr_grade_group_code = dr1["sims_activity_grade_scale_code"].ToString();
                            act.sims_activity_max_pnt = dr1["sims_activity_max_points_possible"].ToString();
                            act.sims_activity_min_pnt = dr1["sims_activity_min_points"].ToString();
                            act.sims_activity_status = dr1["sims_status"].ToString().Equals("A") ? true : false;
                            act.sims_subject_code = dr1["sims_subject_code"].ToString();
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                    new List<SqlParameter>() 
                                { 
                                new SqlParameter("@OPR_MAIN", 'S'),
                                new SqlParameter("@OPR", "SI"),
                                new SqlParameter("@activity_st_no",act.sims_activity_sr_no ),
                                });
                                if (dr2.HasRows)
                                {
                                    while (dr2.Read())
                                    {
                                        sims_indicator ind = new sims_indicator();
                                        ind.sims_config_sr_no = dr2["sims_indicator_code"].ToString();
                                        ind.sims_activity_sr_no = dr2["sims_activity_code"].ToString();
                                        ind.sims_indicator_name = dr2["sims_indicator_name"].ToString();
                                        ind.sims_indicator_name_ar = dr2["sims_indicator_name_ar"].ToString();
                                        ind.sims_indicator_desc = dr2["sims_indicator_desc"].ToString();
                                        ind.sims_indicator_rmk = dr2["sims_indicator_remark"].ToString();
                                        ind.narr_grade_group_code = dr2["sims_indicator_grade_scale_code"].ToString();
                                        ind.sims_indicator_max_pnt = dr2["sims_indicator_max_points_possible"].ToString();
                                        ind.sims_indicator_min_pnt = dr2["sims_indicator_min_points"].ToString();
                                        ind.sims_indicator_status = dr2["sims_status"].ToString().Equals("A") ? true : false;
                                        act.indicators.Add(ind);
                                    }
                                }
                                dr2.Close();
                                dr2.Dispose();
                            }
                            acativities.Add(act);
                        }
                    }
                    dr1.Close();
                    dr1.Dispose();
                    return Request.CreateResponse(HttpStatusCode.OK, acativities);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, acativities);
            }
        }

        /* INDICATOR*/
        [Route("defineindicator")]
        public HttpResponseMessage defineindicator(string opr_param, sims_activity_master obj)
        {
            string opr = opr_param.Equals("I") ? "II" : "UI";
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'I'),
                                new SqlParameter("@OPR", opr),
                                new SqlParameter("@config_sr_no", obj.sims_config_sr_no),
                                new SqlParameter("@activity_st_no", obj.sims_activity_sr_no),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@INDICATOR_NAME", obj.sims_indicator_name),
                                new SqlParameter("@INDICATOR_NAME_AR", obj.sims_indicator_name_ar),
                                new SqlParameter("@INDICATOR_DESC", obj.sims_indicator_desc),
                                new SqlParameter("@INDICATOR_RMK",  obj.sims_indicator_rmk),
                                new SqlParameter("@ACTIVITY_GRD_SCALE", obj.narr_grade_group_code),
                                new SqlParameter("@ACTIVITY_MAX_POINT",string.IsNullOrEmpty(obj.sims_indicator_max_pnt)?"0":obj.sims_indicator_max_pnt.ToString()),
                                new SqlParameter("@ACTIVITY_MIN_POINT", string.IsNullOrEmpty(obj.sims_indicator_min_pnt)?"0":obj.sims_indicator_min_pnt.ToString()),
                                new SqlParameter("@STATUS", obj.sims_indicator_status==true?"A":"I" )
                         });
                    int t = dr.RecordsAffected;
                    res = t > 0 ? true : true;
                    return Request.CreateResponse(HttpStatusCode.OK, res);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        /* INDICATOR COMMENT*/
        [Route("defineindicatorComment")]
        public HttpResponseMessage defineindicatorComment(string opr_param, sims_indicator_comment_master obj)
        {
            string opr = string.Empty;
            if (opr_param.Equals("I"))
                opr = "CM";
            else if (opr_param.Equals("U"))
                opr = "UC";
            else if (opr_param.Equals("D"))
                opr = "DC";


            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", 'I'),
                                new SqlParameter("@OPR", opr),
                                new SqlParameter("@INDICATOR_GROUP_CODE", obj.sims_indicator_group_code),
                                new SqlParameter("@INDICATOR_CODE", obj.sims_indicator_code),
                                new SqlParameter("@activity_st_no", obj.sims_activity_code),
                                new SqlParameter("@COMMENT_DESC", obj.sims_comment_desc),
                                new SqlParameter("@COMMENT_DESC_AR", obj.sims_comment_desc_ar),
                                new SqlParameter("@COMMENT_CODE", obj.sims_indicator_comment_code),
                                new SqlParameter("@STATUS", obj.sims_indicator_comment_status ==true?"A":"I" )
                         });
                    int t = dr.RecordsAffected;
                    res = t > 0 ? true : true;
                    return Request.CreateResponse(HttpStatusCode.OK, res);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }


        [Route("indicatorComment")]
        public HttpResponseMessage indicatorComment(sims_indicator_comment_master obj)
        {
            List<sims_indicator_comment_master> acativities = new List<sims_indicator_comment_master>();
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@OPR_MAIN", 'S'),
                                    new SqlParameter("@OPR", "SC"),
                                    new SqlParameter("@activity_st_no",obj.sims_activity_code ),
                                    new SqlParameter("@INDICATOR_CODE",obj.sims_indicator_code ),
                                    new SqlParameter("@INDICATOR_GROUP_CODE",obj.sims_indicator_group_code )
                                });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            sims_indicator_comment_master act = new sims_indicator_comment_master();
                            act.sims_activity_code = obj.sims_activity_code;
                            act.sims_indicator_code = obj.sims_indicator_code;
                            act.sims_indicator_group_code = dr1["sims_indicator_group_code"].ToString();
                            act.sims_indicator_comment_code = dr1["sims_indicator_comment_code"].ToString();
                            act.sims_comment_desc = dr1["sims_comment_desc"].ToString();
                            act.sims_comment_desc_ar = dr1["sims_comment_desc_ar"].ToString();
                            act.sims_indicator_group_desc = dr1["sims_indicator_group_desc"].ToString();
                            act.sims_indicator_comment_status = dr1["sims_status"].ToString().Equals("A") ? true : false;
                            act.sims_indicator_comment_delete_status = dr1["Action_status"].ToString().Equals("A") ? true : false;
                            acativities.Add(act);
                        }
                    }
                    dr1.Close();
                    dr1.Dispose();
                    return Request.CreateResponse(HttpStatusCode.OK, acativities);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, acativities);
            }
        }

        #endregion

        #region ActivityMarkEntry

        [Route("MEAllActivitiesConfig")]
        public HttpResponseMessage MEAllActivitiesConfig(sims_activity_master obj)
        {
            List<sims_activity_master> mod_list = new List<sims_activity_master>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "AS"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                new SqlParameter("@TERM_CODE", obj.sims_term_code),
                                new SqlParameter("@CREATED_USR", obj.sims_user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_activity_master simsobj = new sims_activity_master();
                            simsobj.sims_acativity_config_name = dr["activity_config_name"].ToString();
                            simsobj.sims_config_sr_no = dr["sims_activity_group_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_config_desc = dr["sims_config_desc"].ToString();
                            simsobj.sims_type_code = dr["sims_config_type"].ToString();
                            simsobj.sims_type_value = dr["sims_config_value"].ToString();
                            simsobj.sims_grade_code = obj.sims_grade_code;
                            simsobj.sims_section_code = obj.sims_section_code;
                            simsobj.sims_activity_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                    new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@OPR_MAIN", "ME"),
                                    new SqlParameter("@OPR", "SA"),
                                    new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                    new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                    new SqlParameter("@config_sr_no", simsobj.sims_config_sr_no ),
                                    new SqlParameter("@SUBJECT_CODE",simsobj.sims_subject_code ),
                                    new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                    new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                    new SqlParameter("@TERM_CODE", obj.sims_term_code),
                                    new SqlParameter("@CREATED_USR", obj.sims_user_code)
                                });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        sims_activity act = new sims_activity();
                                        act.sims_config_sr_no = simsobj.sims_config_sr_no;
                                        act.sims_activity_sr_no = dr1["sims_activity_code"].ToString();
                                        act.sims_activity_name = dr1["sims_activity_name"].ToString();
                                        act.sims_activity_desc = dr1["sims_activity_desc"].ToString();
                                        act.sims_activity_rmk = dr1["sims_activity_remark"].ToString();
                                        act.narr_grade_group_code = dr1["sims_activity_grade_scale_code"].ToString();
                                        act.sims_activity_max_pnt = dr1["sims_activity_max_points_possible"].ToString();
                                        act.sims_activity_min_pnt = dr1["sims_activity_min_points"].ToString();
                                        act.sims_activity_status = dr1["sims_status"].ToString().Equals("A") ? true : false;
                                        act.sims_subject_code = dr1["sims_subject_code"].ToString();
                                        if (simsobj.lst_activity == null)
                                            simsobj.lst_activity = new List<sims_activity>();
                                        simsobj.lst_activity.Add(act);
                                        //
                                        using (DBConnection db2 = new DBConnection())
                                        {
                                            db2.Open();
                                            SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                                new List<SqlParameter>() 
                                                { 
                                                new SqlParameter("@OPR_MAIN", 'S'),
                                                new SqlParameter("@OPR", "SI"),
                                                new SqlParameter("@activity_st_no",act.sims_activity_sr_no ),
                                                });
                                            if (dr2.HasRows)
                                            {
                                                while (dr2.Read())
                                                {
                                                    sims_indicator ind = new sims_indicator();
                                                    ind.sims_config_sr_no = dr2["sims_indicator_code"].ToString();
                                                    ind.sims_activity_sr_no = dr2["sims_activity_code"].ToString();
                                                    ind.sims_indicator_name = dr2["sims_indicator_name"].ToString();
                                                    ind.sims_indicator_desc = dr2["sims_indicator_desc"].ToString();
                                                    ind.sims_indicator_rmk = dr2["sims_indicator_remark"].ToString();
                                                    ind.narr_grade_group_code = dr2["sims_indicator_grade_scale_code"].ToString();
                                                    ind.sims_indicator_max_pnt = dr2["sims_indicator_max_points_possible"].ToString();
                                                    ind.sims_indicator_min_pnt = dr2["sims_indicator_min_points"].ToString();
                                                    ind.sims_indicator_status = dr2["sims_status"].ToString().Equals("A") ? true : false;
                                                    act.indicators.Add(ind);
                                                }
                                            }
                                            dr2.Close();
                                            dr2.Dispose();

                                            //
                                        }
                                    }
                                }
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("MEStudentComemnts")]
        public HttpResponseMessage MEStudentComemnts(sims_activity_master obj)
        {
            List<sims_activity_students> mod_list = new List<sims_activity_students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader drstu = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "STU"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                         });
                    if (drstu.HasRows)
                    {
                        while (drstu.Read())
                        {
                            sims_activity_students simsobj = new sims_activity_students();
                            simsobj.sims_cur_code = drstu["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = drstu["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = drstu["sims_grade_code"].ToString();
                            simsobj.sims_section_code = drstu["sims_section_code"].ToString();
                            simsobj.sims_enroll_number = drstu["sims_enroll_number"].ToString();
                            simsobj.sims_student_name = drstu["sims_Student_name"].ToString();
                            simsobj.indicators = studentIndicators(simsobj.sims_enroll_number, obj);
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        public List<sims_indicator> studentIndicators(string enroll_no, sims_activity_master obj)
        {
            List<sims_indicator> mod_list = new List<sims_indicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SAI"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", enroll_no),
                                new SqlParameter("@activity_st_no", obj.sims_activity_sr_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_indicator simsobj = new sims_indicator();
                            simsobj.sims_config_sr_no = dr["sims_indicator_code"].ToString();
                            simsobj.sims_indicator_desc = dr["sims_indicator_name"].ToString();
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                    new List<SqlParameter>() 
                                                { 
                                               new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SCM"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", enroll_no),
                                new SqlParameter("@activity_st_no", simsobj.sims_config_sr_no),
                                });
                                if (dr2.HasRows)
                                {
                                    sims_indicator_comment_master selected_comment = new sims_indicator_comment_master();
                                    while (dr2.Read())
                                    {
                                        sims_indicator_comment_master comment = new sims_indicator_comment_master();
                                        comment.sims_indicator_comment_code = dr2["sims_indicator_comment_code"].ToString();
                                        comment.sims_indicator_comment_code_old = string.Empty;
                                        comment.sims_indicator_group_code = dr2["sims_indicator_group_code"].ToString();
                                        comment.sims_indicator_code = dr2["sims_indicator_code"].ToString();
                                        comment.sims_activity_code = dr2["sims_activity_code"].ToString();
                                        comment.sims_cur_code = dr2["sims_cur_code"].ToString();
                                        comment.sims_academic_year = dr2["sims_academic_year"].ToString();
                                        comment.sims_grade_code = dr2["sims_grade_code"].ToString();
                                        comment.sims_section_code = dr2["sims_section_code"].ToString();
                                        comment.sims_term_code = dr2["sims_term_code"].ToString();
                                        comment.sims_subject_code = dr2["sims_subject_code"].ToString();
                                        comment.sims_comment_desc = dr2["sims_comment_desc"].ToString();
                                        comment.sims_comment_desc_ar = dr2["sims_comment_desc_ar"].ToString();
                                        //comment.sims_enroll_number = dr2["sims_enroll_number"].ToString();
                                        comment.sims_indicator_group_short_name = dr2["sims_indicator_group_name"].ToString();
                                        comment.sims_indicator_group_desc = dr2["sims_indicator_group_desc"].ToString();
                                        comment.sims_indicator_comment_status = dr2["sims_student_comment_status"].ToString().Equals("A") ? true : false;
                                        if (comment.sims_indicator_comment_status)
                                            selected_comment = comment;
                                        simsobj.comments.Add(comment);
                                    }
                                    foreach (sims_indicator_comment_master comment in simsobj.comments)
                                    {
                                        if (comment.sims_indicator_code == selected_comment.sims_indicator_code && comment.sims_activity_code == selected_comment.sims_activity_code)
                                        {
                                            comment.sims_indicator_comment_code_old = selected_comment.sims_indicator_comment_code;
                                        }
                                    }
                                }
                                dr2.Close();
                                dr2.Dispose();

                                //
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                    return mod_list;
                }
            }
            catch (Exception e)
            {
                return mod_list;
            }
        }

        [Route("MEStudentComemntsIndicator")]
        public HttpResponseMessage MEStudentComemntsIndicator(string enroll_no, sims_activity_master obj)
        {
            List<sims_indicator> mod_list = new List<sims_indicator>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SAI"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", enroll_no),
                                new SqlParameter("@activity_st_no", obj.sims_activity_sr_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_indicator simsobj = new sims_indicator();
                            simsobj.sims_config_sr_no = dr["sims_indicator_code"].ToString();
                            simsobj.sims_indicator_desc = dr["sims_indicator_name"].ToString();
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                    new List<SqlParameter>() 
                                                { 
                                               new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SCM"),
                                new SqlParameter("@CUR_CODE", obj.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", obj.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", obj.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", obj.sims_section_code),
                                new SqlParameter("@SUBJECT_CODE", obj.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", enroll_no),
                                new SqlParameter("@activity_st_no", simsobj.sims_config_sr_no),
                                });
                                if (dr2.HasRows)
                                {
                                    sims_indicator_comment_master selected_comment = new sims_indicator_comment_master();
                                    while (dr2.Read())
                                    {
                                        sims_indicator_comment_master comment = new sims_indicator_comment_master();
                                        comment.sims_indicator_comment_code = dr2["sims_indicator_comment_code"].ToString();
                                        comment.sims_indicator_comment_code_old = string.Empty;
                                        comment.sims_indicator_group_code = dr2["sims_indicator_group_code"].ToString();
                                        comment.sims_indicator_code = dr2["sims_indicator_code"].ToString();
                                        comment.sims_activity_code = dr2["sims_activity_code"].ToString();
                                        comment.sims_cur_code = dr2["sims_cur_code"].ToString();
                                        comment.sims_academic_year = dr2["sims_academic_year"].ToString();
                                        comment.sims_grade_code = dr2["sims_grade_code"].ToString();
                                        comment.sims_section_code = dr2["sims_section_code"].ToString();
                                        comment.sims_term_code = dr2["sims_term_code"].ToString();
                                        comment.sims_subject_code = dr2["sims_subject_code"].ToString();
                                        comment.sims_comment_desc = dr2["sims_comment_desc"].ToString();
                                        comment.sims_comment_desc_ar = dr2["sims_comment_desc_ar"].ToString();
                                        //comment.sims_enroll_number = dr2["sims_enroll_number"].ToString();
                                        comment.sims_indicator_group_short_name = dr2["sims_indicator_group_name"].ToString();
                                        comment.sims_indicator_group_desc = dr2["sims_indicator_group_desc"].ToString();
                                        comment.sims_indicator_comment_status = dr2["sims_student_comment_status"].ToString().Equals("A") ? true : false;
                                        comment.sims_indicator_comment_delete_status = false;
                                        if (comment.sims_indicator_comment_status)
                                            selected_comment = comment;
                                        simsobj.comments.Add(comment);
                                    }
                                    foreach (sims_indicator_comment_master comment in simsobj.comments)
                                    {
                                        if (comment.sims_indicator_code == selected_comment.sims_indicator_code && comment.sims_activity_code == selected_comment.sims_activity_code)
                                        {
                                            comment.sims_indicator_comment_code_old = selected_comment.sims_indicator_comment_code;
                                        }
                                    }
                                }
                                dr2.Close();
                                dr2.Dispose();

                                //
                            }
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("MEDefineStudentComemnt")]
        public HttpResponseMessage MEDefineStudentComemnt(List<sims_activity_students> students)
        {
            bool res = false;
            try
            {
                //obj
                foreach (sims_activity_students s in students)
                {

                    foreach (sims_indicator ind in s.indicators)
                    {
                        foreach (sims_indicator_comment_master c in ind.comments)
                        {
                            if (c.sims_indicator_comment_status)
                            {
                                using (DBConnection db = new DBConnection())
                                {
                                    db.Open();
                                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                        new List<SqlParameter>() 
                                { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SCI"),
                                new SqlParameter("@INDICATOR_COMMENT_CODE_NEW", c.sims_indicator_comment_code),
                                new SqlParameter("@INDICATOR_COMMENT_CODE", c.sims_indicator_comment_code_old),
                                new SqlParameter("@INDICATOR_GROUP_CODE", c.sims_indicator_group_code),
                                new SqlParameter("@INDICATOR_CODE", c.sims_indicator_code),
                                new SqlParameter("@activity_st_no", c.sims_activity_code),
                                new SqlParameter("@CUR_CODE", c.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", c.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", c.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", c.sims_section_code),
                                new SqlParameter("@TERM_CODE", c.sims_term_code),
                                new SqlParameter("@SUBJECT_CODE", c.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", s.sims_enroll_number),
                                new SqlParameter("@STATUS", c.sims_indicator_comment_status==true?"A":"I")
                                });
                                    res = dr.RecordsAffected > 0 ? true : false;
                                }
                            }
                            else if (c.sims_indicator_comment_delete_status)
                            {
                                using (DBConnection db = new DBConnection())
                                {
                                    db.Open();
                                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_scholastic_evaluation_config_all_proc",
                                        new List<SqlParameter>() 
                                { 
                                new SqlParameter("@OPR_MAIN", "ME"),
                                new SqlParameter("@OPR", "SCD"),
                                new SqlParameter("@INDICATOR_COMMENT_CODE_NEW", c.sims_indicator_comment_code),
                                new SqlParameter("@INDICATOR_COMMENT_CODE", c.sims_indicator_comment_code_old),
                                new SqlParameter("@INDICATOR_GROUP_CODE", c.sims_indicator_group_code),
                                new SqlParameter("@INDICATOR_CODE", c.sims_indicator_code),
                                new SqlParameter("@activity_st_no", c.sims_activity_code),
                                new SqlParameter("@CUR_CODE", c.sims_cur_code),
                                new SqlParameter("@ACADEMIC_YEAR", c.sims_academic_year),
                                new SqlParameter("@GRADE_CODE", c.sims_grade_code),
                                new SqlParameter("@SECTION_CODE", c.sims_section_code),
                                new SqlParameter("@TERM_CODE", c.sims_term_code),
                                new SqlParameter("@SUBJECT_CODE", c.sims_subject_code),
                                new SqlParameter("@ENROLL_NUMBER", s.sims_enroll_number),
                                new SqlParameter("@STATUS", c.sims_indicator_comment_status==true?"A":"I")
                                });
                                    res = dr.RecordsAffected > 0 ? true : false;
                                }

                            }

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }


        #endregion

    }
}