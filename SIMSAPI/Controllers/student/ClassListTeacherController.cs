﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/Classteacher")]
    [BasicAuthentication]
    public class ClassListTeacherController : ApiController
    {
        [Route("getclasslistgrades")]
        public HttpResponseMessage getclasslistgrades(string cur, string a_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getclasslistgrades(),PARAMETERS :: NO";

            List<Cls10T> dashboard_list = new List<Cls10T>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_class_teacher_view_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),
                           new SqlParameter("@sims_cur_code", cur),
                           new SqlParameter("@sims_academic_year", a_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Cls10T simsobj = new Cls10T();

                            simsobj.sims_bell_grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();

                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }


        [Route("getclasslistsection")]
        public HttpResponseMessage getclasslistsection(string cur, string a_year, string grades)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getclasslistsection(),PARAMETERS :: NO";

            List<Cls10T> dashboard_list = new List<Cls10T>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_class_teacher_view_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'T'),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", a_year),
                            new SqlParameter("@sims_grade_code",grades),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Cls10T simsobj = new Cls10T();

                            simsobj.sims_bell_section_code = dr["sims_bell_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }


        [Route("getallData")]
        public HttpResponseMessage getallData(string cur, string a_year, string grades, string section, string login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getallData(),PARAMETERS :: NO";

            List<Cls10T> dashboard_list = new List<Cls10T>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_class_teacher_view_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", a_year),
                            new SqlParameter("@sims_grade_code",grades),
                            new SqlParameter("@sims_section_code",section),
                            new SqlParameter("@em_login_code", login_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Cls10T simsobj = new Cls10T();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_student_dob = db.UIDDMMYYYYformat(dr["sims_student_dob"].ToString());
                            simsobj.sims_student_nationality_code = dr["sims_student_nationality_code"].ToString();
                            simsobj.nationality_name = dr["nationality_name"].ToString();
                            simsobj.sims_student_religion_code = dr["sims_student_religion_code"].ToString();
                            simsobj.std_religion = dr["std_religion"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.mother_name = dr["mother_name"].ToString();
                            simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sibling_detil = dr["sibling_detil"].ToString();

                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }


    }
}