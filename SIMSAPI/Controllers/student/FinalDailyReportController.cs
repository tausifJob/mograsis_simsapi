﻿using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.student
{

    [RoutePrefix("api/DailyReport")]
    public class FinalDailyReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGradesNew")]
        public HttpResponseMessage getAllGradesNew(string cur_code, string academic_year, string user)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                                new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeNew")]
        public HttpResponseMessage getSectionFromGradeNew(string cur_code, string grade_code, string academic_year, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code),
                                  new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }

        }


        [Route("getFinalDailyReport")]
        public HttpResponseMessage getFinalDailyReport(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code)
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                           


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getFinalDailyReport")]
        public HttpResponseMessage getFinalDailyReport(string cur_code, string academic_year, string grade_code, string section_code,string date)
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@sims_student_transaction_date",db.DBYYYYMMDDformat(date)),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_student_img = dr["sims_student_img"].ToString();
                            sequence.parent_name = dr["parent_name"].ToString();

                            sequence.flag = dr["cnt"].ToString();


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        //[Route("getallreadyInserted")]
        //public HttpResponseMessage getallreadyInserted(string cur_code, string academic_year, string grade_code, string section_code,string enroll_number,string mom_start_date)
        //{
        //    List<Fdr001> lstCuriculum = new List<Fdr001>();
        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                     new SqlParameter("@opr", 'T'),
        //                     new SqlParameter("@cur_code", cur_code),
        //                     new SqlParameter("@academic_year", academic_year),
        //                     new SqlParameter("@grade_code", grade_code),
        //                     new SqlParameter("@section_code", section_code),
        //                     new SqlParameter("@sims_enroll_number", enroll_number),
        //                     new SqlParameter("@sims_student_transaction_date",db.DBYYYYMMDDformat(mom_start_date)),

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Fdr001 sequence = new Fdr001();
        //                    sequence.flag = dr["flag"].ToString();

        //                    lstCuriculum.Add(sequence);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        //}





        [Route("geteditedData")]
        public HttpResponseMessage geteditedData(string cur_code, string academic_year, string grade_code, string section_code, string enroll_number, string date)
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'Q'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@sims_enroll_number", enroll_number),
                             new SqlParameter("@sims_student_transaction_date",db.DBYYYYMMDDformat(date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_student_feeling = dr["sims_student_feeling"].ToString();

                            sequence.sims_student_activity = dr["sims_student_activity"].ToString();
                            sequence.sims_student_nappy_changing = dr["sims_student_nappy_changing"].ToString();
                            sequence.sims_student_nappy_changing_dry = dr["sims_student_nappy_changing_dry"].ToString();
                            sequence.sims_student_nappy_changing_dry_frequency = dr["sims_student_nappy_changing_dry_frequency"].ToString();
                            sequence.sims_student_nappy_changing_dry_Remark = dr["sims_student_nappy_changing_dry_Remark"].ToString();
                            sequence.sims_student_nappy_changing_wet = dr["sims_student_nappy_changing_wet"].ToString();


                            sequence.sims_student_nappy_changing_wet_frequency = dr["sims_student_nappy_changing_wet_frequency"].ToString();
                            sequence.sims_student_nappy_changing_wet_Remark = dr["sims_student_nappy_changing_wet_Remark"].ToString();
                            sequence.sims_student_nappy_changing_bm = dr["sims_student_nappy_changing_bm"].ToString();
                            sequence.sims_student_nappy_changing_bm_frequency = dr["sims_student_nappy_changing_bm_frequency"].ToString();
                            sequence.sims_student_nappy_changing_bm_Remark = dr["sims_student_nappy_changing_bm_Remark"].ToString();
                            sequence.sims_student_toilet_training = dr["sims_student_toilet_training"].ToString();


                            sequence.sims_student_toilet_training_Number1 = dr["sims_student_toilet_training_Number1"].ToString();
                            sequence.sims_student_toilet_training_Number1_frequency = dr["sims_student_toilet_training_Number1_frequency"].ToString();
                            sequence.sims_student_toilet_training_Number1_Remark = dr["sims_student_toilet_training_Number1_Remark"].ToString();
                            sequence.sims_student_toilet_training_Number2 = dr["sims_student_toilet_training_Number2"].ToString();
                            sequence.sims_student_toilet_training_Number2_frequency = dr["sims_student_toilet_training_Number2_frequency"].ToString();
                            sequence.sims_student_toilet_training_Number2_Remark = dr["sims_student_toilet_training_Number2_Remark"].ToString();

                            sequence.sims_student_toilet_training_Askedtogo = dr["sims_student_toilet_training_Askedtogo"].ToString();
                            sequence.sims_student_toilet_training_Askedtogo_frequency = dr["sims_student_toilet_training_Askedtogo_frequency"].ToString();
                            sequence.sims_student_toilet_training_Askedtogo_Remark = dr["sims_student_toilet_training_Askedtogo_Remark"].ToString();
                            sequence.sims_student_fluid_intake = dr["sims_student_fluid_intake"].ToString();
                            sequence.sims_student_fluid_intake_Milk = dr["sims_student_fluid_intake_Milk"].ToString();
                            sequence.sims_student_fluid_intake_Milk_qty = dr["sims_student_fluid_intake_Milk_qty"].ToString();


                            sequence.sims_student_fluid_intake_Milk_frequency = dr["sims_student_fluid_intake_Milk_frequency"].ToString();
                            sequence.sims_student_fluid_intake_Milk_remark = dr["sims_student_fluid_intake_Milk_remark"].ToString();
                            sequence.sims_student_fluid_intake_Water = dr["sims_student_fluid_intake_Water"].ToString();
                            sequence.sims_student_fluid_intake_Water_qty = dr["sims_student_fluid_intake_Water_qty"].ToString();
                            sequence.sims_student_fluid_intake_water_frequency = dr["sims_student_fluid_intake_water_frequency"].ToString();
                            sequence.sims_student_fluid_intake_Water_remark = dr["sims_student_fluid_intake_Water_remark"].ToString();

                            sequence.sims_student_food_Morning_snack = dr["sims_student_food_Morning_snack"].ToString();
                            sequence.sims_student_food_Morning_snack_quantity = dr["sims_student_food_Morning_snack_quantity"].ToString();
                            sequence.sims_student_food_Morning_snack_remark = dr["sims_student_food_Morning_snack_remark"].ToString();
                            sequence.sims_student_food_Lunch = dr["sims_student_food_Lunch"].ToString();
                            sequence.sims_student_food_Lunch_quantity = dr["sims_student_food_Lunch_quantity"].ToString();
                            sequence.sims_student_food_Lunch_remark = dr["sims_student_food_Lunch_remark"].ToString();

                            sequence.sims_student_food_Afternoon_snack = dr["sims_student_food_Afternoon_snack"].ToString();
                            sequence.sims_student_food_Afternoon_snack_quantity = dr["sims_student_food_Afternoon_snack_quantity"].ToString();
                            sequence.sims_student_food_Afternoon_snack_remark = dr["sims_student_food_Afternoon_snack_remark"].ToString();
                            sequence.sims_student_slep_time_am = dr["sims_student_slep_time_am"].ToString();
                            sequence.sims_student_slep_time_pm = dr["sims_student_slep_time_pm"].ToString();
                            //sequence.sims_student_any_incidence = (== "F" ? false : true);

                            if(dr["sims_student_any_incidence"].ToString().Equals("T"))
                            {
                                sequence.sims_student_any_incidence = true;
                            }
                            else
                            {
                                sequence.sims_student_any_incidence = false;
                            }
                            sequence.sims_student_incidence_remark = dr["sims_student_incidence_remark"].ToString();
                            sequence.sims_student_provide_type = dr["sims_student_provide_type"].ToString();
                            sequence.sims_student_provide_quantity = dr["sims_student_provide_quantity"].ToString();
                            sequence.sims_student_teacher_comment = dr["sims_student_teacher_comment"].ToString();
                            sequence.sims_student_parent_comment = dr["sims_student_parent_comment"].ToString();

                            sequence.sims_student_please_provide_diaper = dr["sims_student_please_provide_diaper"].ToString();
                            sequence.sims_student_please_provide_diaper_qty = dr["sims_student_please_provide_diaper_qty"].ToString();
                            sequence.sims_student_please_provide_diaper_remark = dr["sims_student_please_provide_diaper_remark"].ToString();
                            sequence.sims_student_please_provide_Wet_wipes = dr["sims_student_please_provide_Wet_wipes"].ToString();
                            sequence.sims_student_please_provide_Wet_wipes_qty = dr["sims_student_please_provide_Wet_wipes_qty"].ToString();
                            sequence.sims_student_please_provide_Wet_wipes_remark = dr["sims_student_please_provide_Wet_wipes_remark"].ToString();
                            sequence.sims_student_please_provide_Wet_Formula = dr["sims_student_please_provide_Wet_Formula"].ToString();
                            sequence.sims_student_please_provide_Wet_Formula_qty = dr["sims_student_please_provide_Wet_Formula_qty"].ToString();
                            sequence.sims_student_please_provide_Wet_Formula_remark = dr["sims_student_please_provide_Wet_Formula_remark"].ToString();
                            sequence.sims_student_please_provide_Wet_Clothing = dr["sims_student_please_provide_Wet_Clothing"].ToString();
                            sequence.sims_student_please_provide_Wet_Clothing_qty = dr["sims_student_please_provide_Wet_Clothing_qty"].ToString();
                            sequence.sims_student_please_provide_Wet_Clothing_remark = dr["sims_student_please_provide_Wet_Clothing_remark"].ToString();
                            sequence.sims_student_please_provide_Wet_Others = dr["sims_student_please_provide_Wet_Others"].ToString();
                            sequence.sims_student_please_provide_Wet_Others_qty = dr["sims_student_please_provide_Wet_Others_qty"].ToString();
                            sequence.sims_student_please_provide_Wet_Others_remark = dr["sims_student_please_provide_Wet_Others_remark"].ToString();

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }



        [Route("getFeeling")]
        public HttpResponseMessage getFeeling()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.feeling_code = dr["feeling_code"].ToString();
                            sequence.feeling_desc = dr["feeling_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getActivity")]
        public HttpResponseMessage getActivity()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.activity_code = dr["activity_code"].ToString();
                            sequence.activity_desc = dr["activity_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getFrequency")]
        public HttpResponseMessage getFrequency()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.frequency_code = dr["frequency_code"].ToString();
                            sequence.frequency_desc = dr["frequency_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getQuantity")]
        public HttpResponseMessage getQuantity()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "D"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.quantity_code = dr["quantity_code"].ToString();
                            sequence.quantity_desc = dr["quantity_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getPlzProvide")]
        public HttpResponseMessage getPlzProvide()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "E"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.please_prov_code = dr["please_prov_code"].ToString();
                            sequence.please_prov_desc = dr["please_prov_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getPlzProvideQty")]
        public HttpResponseMessage getPlzProvideQty()
        {
            List<Fdr001> lstCuriculum = new List<Fdr001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_final_daily_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "F"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fdr001 sequence = new Fdr001();
                            sequence.please_prov_qty_code = dr["please_prov_qty_code"].ToString();
                            sequence.please_prov_qty_desc = dr["please_prov_qty_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("CUDDetails")]
        public HttpResponseMessage CUDDetails(List<Fdr001> data)
        {
            bool insert = false;

            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fdr001 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_final_daily_report_proc]",

                        new List<SqlParameter>()
                     {

                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@cur_code",simsobj.sims_cur_code),
                                 new SqlParameter("@academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@grade_code", simsobj.sims_grade_code),
                                 new SqlParameter("@section_code", simsobj.sims_section_code),
                                 new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                 new SqlParameter("@sims_student_feeling", simsobj.sims_student_feeling),
                                 new SqlParameter("@sims_student_activity",simsobj.sims_student_activity),

                                 new SqlParameter("@sims_student_nappy_changing", simsobj.sims_student_nappy_changing),

                                 new SqlParameter("@sims_student_nappy_changing_dry", simsobj.sims_student_nappy_changing_dry),
                                 new SqlParameter("@sims_student_nappy_changing_dry_frequency", simsobj.sims_student_nappy_changing_dry_frequency),
                                 new SqlParameter("@sims_student_nappy_changing_dry_Remark", simsobj.sims_student_nappy_changing_dry_Remark),

                                 new SqlParameter("@sims_student_nappy_changing_wet", simsobj.sims_student_nappy_changing_wet),
                                 new SqlParameter("@sims_student_nappy_changing_wet_frequency", simsobj.sims_student_nappy_changing_wet_frequency),
                                 new SqlParameter("@sims_student_nappy_changing_wet_Remark", simsobj.sims_student_nappy_changing_wet_Remark),

                                 new SqlParameter("@sims_student_nappy_changing_bm", simsobj.sims_student_nappy_changing_bm),
                                 new SqlParameter("@sims_student_nappy_changing_bm_frequency", simsobj.sims_student_nappy_changing_bm_frequency),
                                 new SqlParameter("@sims_student_nappy_changing_bm_Remark", simsobj.sims_student_nappy_changing_bm_Remark),



                                 new SqlParameter("@sims_student_toilet_training", simsobj.sims_student_toilet_training),

                                 new SqlParameter("@sims_student_toilet_training_Number1", simsobj.sims_student_toilet_training_Number1),
                                 new SqlParameter("@sims_student_toilet_training_Number1_frequency", simsobj.sims_student_toilet_training_Number1_frequency),
                                 new SqlParameter("@sims_student_toilet_training_Number1_Remark", simsobj.sims_student_toilet_training_Number1_Remark),

                                 new SqlParameter("@sims_student_toilet_training_Number2", simsobj.sims_student_toilet_training_Number2),
                                 new SqlParameter("@sims_student_toilet_training_Number2_frequency", simsobj.sims_student_toilet_training_Number2_frequency),
                                 new SqlParameter("@sims_student_toilet_training_Number2_Remark", simsobj.sims_student_toilet_training_Number2_Remark),


                                 new SqlParameter("@sims_student_toilet_training_Askedtogo", simsobj.sims_student_toilet_training_Askedtogo),
                                 new SqlParameter("@sims_student_toilet_training_Askedtogo_frequency", simsobj.sims_student_toilet_training_Askedtogo_frequency),
                                 new SqlParameter("@sims_student_toilet_training_Askedtogo_Remark", simsobj.sims_student_toilet_training_Askedtogo_Remark),




                                 new SqlParameter("@sims_student_fluid_intake", simsobj.sims_student_fluid_intake),

                                 new SqlParameter("@sims_student_fluid_intake_Milk", simsobj.sims_student_fluid_intake_Milk),
                                 new SqlParameter("@sims_student_fluid_intake_Milk_qty", simsobj.sims_student_fluid_intake_Milk_qty),
                                 new SqlParameter("@sims_student_fluid_intake_Milk_frequency",simsobj.sims_student_fluid_intake_Milk_frequency),
                                 new SqlParameter("@sims_student_fluid_intake_Milk_remark", simsobj.sims_student_fluid_intake_Milk_remark),

                                 new SqlParameter("@sims_student_fluid_intake_Water", simsobj.sims_student_fluid_intake_Water),
                                 new SqlParameter("@sims_student_fluid_intake_Water_qty", simsobj.sims_student_fluid_intake_Water_qty),
                                 new SqlParameter("@sims_student_fluid_intake_water_frequency", simsobj.sims_student_fluid_intake_water_frequency),
                                 new SqlParameter("@sims_student_fluid_intake_Water_remark", simsobj.sims_student_fluid_intake_Water_remark),

                                 new SqlParameter("@sims_student_food", simsobj.sims_student_food),

                                new SqlParameter("@sims_student_food_Morning_snack", simsobj.sims_student_food_Morning_snack),
                                 new SqlParameter("@sims_student_food_Morning_snack_quantity", simsobj.sims_student_food_Morning_snack_quantity),
                                 new SqlParameter("@sims_student_food_Morning_snack_remark", simsobj.sims_student_food_Morning_snack_remark),

                                 new SqlParameter("@sims_student_food_Lunch", simsobj.sims_student_food_Lunch),
                                 new SqlParameter("@sims_student_food_Lunch_quantity", simsobj.sims_student_food_Lunch_quantity),
                                 new SqlParameter("@sims_student_food_Lunch_remark", simsobj.sims_student_food_Lunch_remark),

                                 new SqlParameter("@sims_student_food_Afternoon_snack", simsobj.sims_student_food_Afternoon_snack),
                                 new SqlParameter("@sims_student_food_Afternoon_snack_quantity", simsobj.sims_student_food_Afternoon_snack_quantity),
                                 new SqlParameter("@sims_student_food_Afternoon_snack_remark", simsobj.sims_student_food_Afternoon_snack_remark),



                                 new SqlParameter("@sims_student_slep_time_am",simsobj.sims_student_slep_time_am),
                                 new SqlParameter("@sims_student_slep_time_pm", simsobj.sims_student_slep_time_pm),
                                 new SqlParameter("@sims_student_any_incidence", simsobj.sims_student_any_incidence==true?"T":"F"),
                                 new SqlParameter("@sims_student_incidence_remark", simsobj.sims_student_incidence_remark),
                                 new SqlParameter("@sims_student_provide_type", simsobj.sims_student_provide_type),
                                 new SqlParameter("@sims_student_provide_quantity", simsobj.sims_student_provide_quantity),
                                 new SqlParameter("@sims_student_teacher_comment",simsobj.sims_student_teacher_comment),
                                 new SqlParameter("@sims_student_parent_comment", simsobj.sims_student_parent_comment),
                                 new SqlParameter("@sims_student_transaction_date", db.DBYYYYMMDDformat(simsobj.mom_start_date)),
                                 new SqlParameter("@sims_student_created_by", simsobj.sims_student_created_by),

                                 new SqlParameter("@sims_student_please_provide_diaper", simsobj.sims_student_please_provide_diaper),
                                 new SqlParameter("@sims_student_please_provide_diaper_qty", simsobj.sims_student_please_provide_diaper_qty),
                                 new SqlParameter("@sims_student_please_provide_diaper_remark", simsobj.sims_student_please_provide_diaper_remark),
                                 new SqlParameter("@sims_student_please_provide_Wet_wipes", simsobj.sims_student_please_provide_Wet_wipes),
                                 new SqlParameter("@sims_student_please_provide_Wet_wipes_qty", simsobj.sims_student_please_provide_Wet_wipes_qty),
                                 new SqlParameter("@sims_student_please_provide_Wet_wipes_remark", simsobj.sims_student_please_provide_Wet_wipes_remark),
                                 new SqlParameter("@sims_student_please_provide_Wet_Formula", simsobj.sims_student_please_provide_Wet_Formula),
                                 new SqlParameter("@sims_student_please_provide_Wet_Formula_qty", simsobj.sims_student_please_provide_Wet_Formula_qty),
                                 new SqlParameter("@sims_student_please_provide_Wet_Formula_remark", simsobj.sims_student_please_provide_Wet_Formula_remark),
                                 new SqlParameter("@sims_student_please_provide_Wet_Clothing", simsobj.sims_student_please_provide_Wet_Clothing),
                                 new SqlParameter("@sims_student_please_provide_Wet_Clothing_qty", simsobj.sims_student_please_provide_Wet_Clothing_qty),
                                 new SqlParameter("@sims_student_please_provide_Wet_Clothing_remark", simsobj.sims_student_please_provide_Wet_Clothing_remark),
                                 new SqlParameter("@sims_student_please_provide_Wet_Others", simsobj.sims_student_please_provide_Wet_Others),
                                 new SqlParameter("@sims_student_please_provide_Wet_Others_qty", simsobj.sims_student_please_provide_Wet_Others_qty),
                                  new SqlParameter("@sims_student_please_provide_Wet_Others_remark", simsobj.sims_student_please_provide_Wet_Others_remark)









                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



     
    }
}