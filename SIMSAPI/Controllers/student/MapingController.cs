﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/MapingController")]
    public class MapingController: ApiController
    {
        [Route("getDetails")]
        public HttpResponseMessage getDetails(string cur_code, string academic_year, string grd_code,string status, bool mappingstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getDetails()PARAMETERS ::NA";
            List<mappingClass> mod_list = new List<mappingClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    string mappingstatus1 = string.Empty;

                    if (mappingstatus == true)
                    {
                        mappingstatus1 = "Y";
                    }
                    else
                    {
                        mappingstatus1 = "N";
                    }
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (cur_code == "undefined" || cur_code == "" || cur_code == null)
                        cur_code = null;
                    if (academic_year == "undefined" || academic_year == "" || academic_year == null)
                        academic_year = null;                    
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Mapping]",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", status),
                            new SqlParameter("@sims_admission_cur_code", cur_code),
                            new SqlParameter("@sims_admission_academic_year", academic_year),
                            new SqlParameter("@sims_admission_grade_code", grd_code),
                            new SqlParameter("@sims_admission_mapping_status", mappingstatus1),
                        });

                    if (dr.HasRows)
                    {                       
                        while (dr.Read())
                        {
                            mappingClass obj_mappingClass = new mappingClass();
                            obj_mappingClass.sims_admission_number = dr["sims_admission_number"].ToString();
                            obj_mappingClass.name = dr["Student_Name"].ToString();
                            obj_mappingClass.sims_employee_ID = dr["sims_employee_ID"].ToString();
                            if (status.Equals("A"))
                            {
                                obj_mappingClass.sims_admission_pros_number = dr["sims_admission_pros_number"].ToString();
                            }
                            obj_mappingClass.sims_grade = dr["sims_grade"].ToString();
                            mod_list.Add(obj_mappingClass);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("SearchEmployee")]
        public HttpResponseMessage SearchEmployee()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : SearchEmployee()PARAMETERS ::NA";
            List<mappingClass> mod_list = new List<mappingClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Mapping]",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "E"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mappingClass obj_mappingClass = new mappingClass();
                            obj_mappingClass.sims_emp_code = dr["em_number"].ToString();
                            obj_mappingClass.sims_emp_code_name = dr["EmpName"].ToString();
                            mod_list.Add(obj_mappingClass);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("insertdata")]
        public HttpResponseMessage insertdata(List<mappingClass> data)
        {
            bool inserted = false;
            int ins = 0;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (mappingClass invsobj in data)
                        {
                            ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Mapping]",
                                new List<SqlParameter>()
                            {
                                    new SqlParameter("@opr", "I"),
                                    new SqlParameter("@sims_prospect_number", invsobj.sims_admission_number),
                                    new SqlParameter("@sims_employee_code",invsobj.sims_employee_ID),
                                    new SqlParameter("@sims_created_by", invsobj.created_by),
                                    new SqlParameter("@sims_application_pros_adm_status", invsobj.sims_application_pros_adm_status),
                                    
                           });                           
                        }
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("getDetailsParent")]
        public HttpResponseMessage getDetailsParent(string cur_code, string academic_year, string grd_code, string mappingstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getDetails()PARAMETERS ::NA";
            List<mappingClass> mod_list = new List<mappingClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //string mappingstatus1 = string.Empty;
                    //if (mappingstatus == true)
                    //{
                    //    mappingstatus1 = "Y";
                    //}
                    //else
                    //{
                    //    mappingstatus1 = "N";
                    //}
                    if (grd_code == "undefined" || grd_code == "" || grd_code == null)
                        grd_code = null;
                    if (cur_code == "undefined" || cur_code == "" || cur_code == null)
                        cur_code = null;
                    if (academic_year == "undefined" || academic_year == "" || academic_year == null)
                        academic_year = null;

                    if (mappingstatus == "undefined" || mappingstatus == "" || mappingstatus == null)
                        mappingstatus = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parent_employee_mapping]",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_admission_cur_code", cur_code),
                            new SqlParameter("@sims_admission_academic_year", academic_year),
                            new SqlParameter("@sims_admission_grade_code", grd_code),
                            new SqlParameter("@sims_admission_mapping_status", mappingstatus),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mappingClass obj_mappingClass = new mappingClass();
                            obj_mappingClass.sims_parent_login_code = dr["sims_parent_login_code"].ToString();
                            obj_mappingClass.parent_nm = dr["parent_nm"].ToString();
                            obj_mappingClass.sims_employee_ID = dr["sims_employee_code"].ToString();
                            
                                
                            mod_list.Add(obj_mappingClass);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("insertemployeedata")]
        public HttpResponseMessage insertemployeedata(List<mappingClass> data)
        {
            bool inserted = false;
            int ins = 0;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (mappingClass invsobj in data)
                        {
                            ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_parent_employee_mapping]",
                                new List<SqlParameter>()
                            {
                                    new SqlParameter("@opr", "I"),
                                    new SqlParameter("@sims_parent_login_code", invsobj.sims_parent_login_code),
                                    new SqlParameter("@sims_employee_code",invsobj.sims_employee_ID),
                                    new SqlParameter("@sims_created_by", invsobj.created_by),
                                   // new SqlParameter("@sims_application_pros_adm_status", invsobj.name),

                           });
                        }
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}
