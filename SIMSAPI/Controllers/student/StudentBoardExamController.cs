﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/student/studentexam")]
    public class StudentBoardExamController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getBoard")]
        public HttpResponseMessage getBoard()
        {
            List<Sims185> lstBoard = new List<Sims185>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims185 sequence = new Sims185();
                            sequence.sims_board_code = dr["sims_board_code"].ToString();
                            sequence.sims_board_name = dr["sims_board_name"].ToString();
                            lstBoard.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstBoard);
        }

        [Route("getBoardExam")]
        public HttpResponseMessage getBoardExam(string board_code, string acd)
        {
            List<Sims185> lstBoard = new List<Sims185>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_exam_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "H"),
                             new SqlParameter("@sims_board_code", board_code),
                             new SqlParameter("@sims_academic_year", acd)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims185 sequence = new Sims185();
                            sequence.sims_board_code = dr["sims_board_code"].ToString();
                            sequence.sims_board_exam_code = dr["sims_board_exam_code"].ToString();
                            sequence.sims_board_exam_name = dr["sims_board_exam_name"].ToString();
                            lstBoard.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstBoard);
        }


        [Route("getStudentsDetails")]
        public HttpResponseMessage getStudentsDetails(string curcode, string academicyear, string boardcode, string boardexamcode)
        {
            List<Sims185> lstBoard = new List<Sims185>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                                new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@sims_academic_year", academicyear),
                             new SqlParameter("@sims_board_code", boardcode),
                             new SqlParameter("@sims_board_exam_cod", boardexamcode),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims185 sequence = new Sims185();

                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();

                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();

                            sequence.sims_student_passport_first_name_en = dr["sims_student_passport_last_name_en"].ToString();

                            sequence.sims_student_passport_full_name_en = dr["sims_student_passport_first_name_en"].ToString() + ' ' + dr["sims_student_passport_last_name_en"];

                            // sequence.sims_student_admission_grade_code = dr["sims_student_admission_grade_code"].ToString();

                            lstBoard.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstBoard);
        }



        [Route("getStudentsExamDetails")]
        public HttpResponseMessage getStudentsExamDetails(string curcode, string academicyear, string boardcode, string boardexamcode, string gradecode, string sectioncode)
        {
            List<Sims185> studentexamdetail = new List<Sims185>();
            try
            {   
                if(boardcode == "undefined"){
                    boardcode = null;
                }
                if (boardexamcode == "undefined"){
                    boardexamcode = null;
                }
                if (gradecode == "undefined" || gradecode == "null")
                {
                    gradecode = null;
                }
                if (sectioncode == "undefined" || sectioncode == "null")
                {
                    sectioncode = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_exam_students_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", curcode),
                             new SqlParameter("@sims_academic_year", academicyear),
                             new SqlParameter("@sims_board_code", boardcode),
                             new SqlParameter("@sims_board_exam_code", boardexamcode),

                             new SqlParameter("@sims_grade_code", gradecode),
                             new SqlParameter("@sims_section_code", sectioncode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims185 sequence = new Sims185();

                            sequence.StudentName = dr["StudentName"].ToString();
                            sequence.sims_board_exam_enroll_number = dr["sims_board_exam_enroll_number"].ToString();
                            sequence.sims_board_code = dr["sims_board_code"].ToString();
                            sequence.sims_board_exam_code = dr["sims_board_exam_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_board_exam_registration_no = dr["sims_board_exam_registration_no"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            //sequence.sims_board_exam_status = dr["sims_board_exam_status"].ToString();
                            if (dr["sims_board_exam_status"].ToString() == "A")
                                sequence.sims_board_exam_status = true;
                            else
                                sequence.sims_board_exam_status = false;
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_board_name = dr["sims_board_name"].ToString();
                            try
                            {
                                sequence.sims_roll_number = dr["sims_roll_number"].ToString();
                            }
                            catch (Exception o)
                            {

                               
                            }
                           
                            // sequence.sims_student_admission_grade_code = dr["sims_student_admission_grade_code"].ToString();

                            studentexamdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, studentexamdetail);
        }

        [Route("StudentBoardCUD")]
        public HttpResponseMessage StudentBoardCUD(List<Sims185> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims185 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_board_exam_students_proc]",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                 new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@sims_board_code", simsobj.sims_board_code),
                                 new SqlParameter("@sims_board_exam_code", simsobj.sims_board_exam_code),
                                 new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                 new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                 new SqlParameter("@sims_board_exam_student_fee_paid_status", 'P'),
                                 new SqlParameter("@sims_board_exam_enroll_number", simsobj.sims_board_exam_enroll_number),
                                 new SqlParameter("@sims_board_exam_registration_no", simsobj.sims_board_exam_registration_no),
                                 new SqlParameter("@sims_board_exam_student_status", simsobj.sims_board_exam_status==true?"A":"I"),



                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}