﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Web;
using System.IO;


namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/SurveyRating")]
    public class SurveyRatingController : ApiController
    {
        [Route("CUDSurveyRatingSave")]
        public HttpResponseMessage CUDSurveyRatingSave(List<surveyObj> obj)
        {
            object o = null;
            bool result = true;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                foreach (surveyObj data in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_survey_rating_proc]",
                             new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",data.opr),
                          new SqlParameter("@sims_survey_rating_group_code",data.RatingGroupCode),
                          new SqlParameter("@sims_survey_rating_group_desc_en",data.DescriptionG),
                          new SqlParameter("@sims_survey_rating_group_desc_ot",data.DescriptionOtherLanguage),
                          new SqlParameter("@sims_survey_rating_group_status",data.StatusR == true? "A":"I"),
                          new SqlParameter("@sims_survey_rating_code",data.RatingCode),
                          new SqlParameter("@sims_survey_rating_name",data.RatingName),
                          new SqlParameter("@sims_survey_rating_desc",data.DescriptionD),
                          new SqlParameter("@sims_survey_rating_point",data.Point),
                          new SqlParameter("@sims_survey_rating_status",data.StatusD == true? "A":"I"), 
                          new SqlParameter("@sims_survey_rating_img_path", data.sims_survey_rating_img_path)
                        
                        });
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                result = false;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, result);
        }
        
        [Route("CUD_SaveSurveyDetails")]
        public HttpResponseMessage CUD_SaveSurveyDetails(surveyObj v)
        {
            bool result = false;
              string RatingGroupCode="";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_rating_proc]",
                    new List<SqlParameter>() {
                          new SqlParameter("@opr", "IM"),
                          new SqlParameter("@sims_survey_rating_group_desc_en",v.DescriptionG),
                          new SqlParameter("@sims_survey_rating_group_desc_ot",v.DescriptionOtherLanguage),
                          new SqlParameter("@sims_survey_rating_group_status",v.StatusR == true? "A":"I"),
                        });
                    while (dr.Read())
                    {  RatingGroupCode = dr["RatingGroupCode"].ToString();
                    }
                    int r = dr.RecordsAffected;
                    dr.Close();
                    if (r > 0)
                    {

                        result = true;
                        foreach (survey_Details vd in v.survey_det)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_survey_rating_proc]",
                            new List<SqlParameter>() {
                                  new SqlParameter("@opr", "ID"),
                                  new SqlParameter("@sims_survey_rating_group_code",RatingGroupCode),
                                  new SqlParameter("@sims_survey_rating_code",vd.RatingCode),
                                  new SqlParameter("@sims_survey_rating_name",vd.RatingName),
                                  new SqlParameter("@sims_survey_rating_desc",vd.DescriptionD),
                                  new SqlParameter("@sims_survey_rating_point",vd.Point),
                                  new SqlParameter("@sims_survey_rating_status",vd.StatusD == true? "A":"I"),
                                  new SqlParameter("@sims_survey_rating_img_path", vd.sims_survey_rating_img_path)
                                });
                            dr1.Close();
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("RatingCommon")]
        public HttpResponseMessage RatingCommon(Dictionary<string, string> sf)
        
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_survey_rating_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("RatingDetails")]
        public HttpResponseMessage RatingDetails(string rating)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<surveyObj> type_list = new List<surveyObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_rating_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'E'),
                       new SqlParameter ("@sims_survey_rating_group_code", rating)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                surveyObj simsobj = new surveyObj();
                                simsobj.RatingGroupCode = dr["sims_survey_rating_group_code"].ToString();
                                simsobj.RatingCode = dr["sims_survey_rating_code"].ToString();
                                simsobj.DescriptionG = dr["sims_survey_rating_group_desc_en"].ToString();
                                simsobj.StatusD = dr["sims_survey_rating_status"].Equals("A") ? true : false; 
                                simsobj.RatingName = dr["sims_survey_rating_name"].ToString();
                                simsobj.DescriptionD = dr["sims_survey_rating_desc"].ToString();
                                simsobj.Point = dr["sims_survey_rating_point"].ToString();
                                simsobj.StatusR = dr["sims_survey_rating_group_status"].Equals("A") ? true : false; ;
                                simsobj.DescriptionOtherLanguage = dr["sims_survey_rating_group_desc_ot"].ToString();
                                simsobj.sims_survey_rating_img_path = dr["sims_survey_rating_img_path"].ToString();
                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Images/Emojis/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [Route("getRatingGroupCode")]
        public HttpResponseMessage getRatingGroupCode()
        {
            List<surveyObj> mod_list = new List<surveyObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_rating_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","G")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            surveyObj simsobj = new surveyObj();
                            simsobj.RatingGroupCode = dr["sims_survey_rating_group_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}