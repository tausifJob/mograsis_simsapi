﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.student
{
    [RoutePrefix("api/StudentClearanceStatus")]

    public class StudentClearanceStatusController : ApiController
    {
        
        [Route("getStudentClearanceStatus")]
        public HttpResponseMessage getStudentClearanceStatus()
        {
            
            List<sims_student_cls_status> admission_list = new List<sims_student_cls_status>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_clearance_staus_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            sims_student_cls_status objNew = new sims_student_cls_status();

                            objNew.sims_sr_no = dr["sims_sr_no"].ToString();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            objNew.student_name = dr["student_name"].ToString();
                            objNew.sims_fee_clr_status = dr["sims_fee_clr_status"].ToString();
                            objNew.sims_fee_clr_by = dr["sims_fee_clr_by"].ToString();
                            objNew.sims_fee_clr_date = dr["sims_fee_clr_date"].ToString();
                            objNew.sims_fee_clr_remark = dr["sims_fee_clr_remark"].ToString();
                            objNew.sims_finn_clr_status = dr["sims_finn_clr_status"].ToString();
                            objNew.sims_finn_clr_by = dr["sims_finn_clr_by"].ToString();
                            objNew.sims_finn_clr_date = dr["sims_finn_clr_date"].ToString();
                            objNew.sims_finn_clr_remark = dr["sims_finn_clr_remark"].ToString();
                            objNew.sims_inv_clr_status = dr["sims_inv_clr_status"].ToString();
                            objNew.sims_inv_clr_by = dr["sims_inv_clr_by"].ToString();
                            objNew.sims_inv_clr_date = dr["sims_inv_clr_date"].ToString();
                            objNew.sims_inv_clr_remark = dr["sims_inv_clr_remark"].ToString();
                            objNew.sims_inci_clr_status = dr["sims_inci_clr_status"].ToString();
                            objNew.sims_inci_clr_by = dr["sims_inci_clr_by"].ToString();
                            objNew.sims_inci_clr_date = dr["sims_inci_clr_date"].ToString();
                            objNew.sims_inci_clr_remark = dr["sims_inci_clr_remark"].ToString();
                            objNew.sims_lib_clr_status = dr["sims_lib_clr_status"].ToString();
                            objNew.sims_lib_clr_by = dr["sims_lib_clr_by"].ToString();
                            objNew.sims_lib_clr_date = dr["sims_lib_clr_date"].ToString();
                            objNew.sims_lib_clr_remark = dr["sims_lib_clr_remark"].ToString();
                            objNew.sims_trans_clr_status = dr["sims_trans_clr_status"].ToString();
                            objNew.sims_trans_clr_by = dr["sims_trans_clr_by"].ToString();
                            objNew.sims_trans_clr_date = dr["sims_trans_clr_date"].ToString();
                            objNew.sims_trans_clr_remark = dr["sims_trans_clr_remark"].ToString();
                            objNew.sims_acad_clr_by = dr["sims_acad_clr_by"].ToString();
                            objNew.sims_acad_clr_date = dr["sims_acad_clr_date"].ToString();

                            admission_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, admission_list);
            }
            catch (Exception e)
            {                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        
    }
}

