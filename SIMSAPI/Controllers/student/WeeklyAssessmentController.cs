﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/WeeklyAssessment")]
    public class WeeklyAssessmentController : ApiController
    {

        [Route("AllGradeBooks")]
        public HttpResponseMessage AllGradeBooks(gradebook gb)
        {
            List<gradebook> lst = new List<gradebook>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                        new List<SqlParameter>() 
                         { 
               
                new SqlParameter("@OPR", "S"),
                new SqlParameter("@CUR_CODE", gb.sims_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", gb.sims_academic_year),
                new SqlParameter("@GB_TEACHER_CODE", gb.sims_teacher_code),
                });

                    while (dr.Read())
                    {

                        gradebook o = new gradebook();
                        o.ischecked = false;
                        o.cnt = 0;
                        o.Categories = new List<gradebook_category>();
                        o.cur_code = dr["sims_cur_code"].ToString();
                        o.gb_number = dr["sims_gb_number"].ToString();
                        o.academic_year = dr["sims_academic_year"].ToString();
                        o.grade_code = dr["sims_grade_code"].ToString();
                        o.section_code = dr["sims_section_code"].ToString();
                        o.gb_name = dr["sims_gb_name"].ToString();
                        o.gb_type = dr["sims_gb_type"].ToString();
                        o.gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                        o.Subject_Code = dr["sims_gb_subject_code"].ToString();
                        o.gb_status = dr["sims_gb_status"].Equals("A") ? true : false;
                        lst.Add(o);

                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        gradebook_category gc = new gradebook_category();
                        gc.ischecked = false;
                        gc.cnt = 0;
                        gc.AssignMents = new List<SIMSAPI.Models.ERP.gradebookClass.Assignment>();
                        gc.cur_code = dr["sims_cur_code"].ToString();
                        gc.cat_code = dr["sims_gb_cat_code"].ToString();
                        gc.cat_name = dr["sims_gb_cat_name"].ToString();
                        gc.academic_year = dr["sims_academic_year"].ToString();
                        gc.grade_code = dr["sims_grade_code"].ToString();
                        gc.gb_number = dr["sims_gb_number"].ToString();
                        gc.section_code = dr["sims_section_code"].ToString();

                        gradebook O = lst.Single(q => q.academic_year == gc.academic_year && q.cur_code == gc.cur_code && q.gb_number == gc.gb_number && q.grade_code == gc.grade_code && q.section_code == gc.section_code);
                        if (O.Categories == null) O.Categories = new List<gradebook_category>();
                        O.Categories.Add(gc);


                    }
                    dr.NextResult();

                    while (dr.Read())
                    {
                        SIMSAPI.Models.ERP.gradebookClass.Assignment assign = new SIMSAPI.Models.ERP.gradebookClass.Assignment();
                        assign.Assignment_academic_year = dr["sims_academic_year"].ToString();
                        assign.Assignment_cat_code = dr["sims_gb_cat_code"].ToString();
                        assign.Assignment_Code = dr["sims_gb_cat_assign_number"].ToString();
                        assign.Assignment_cur_code = dr["sims_cur_code"].ToString();
                        assign.Assignment_Date = db.UIDDMMYYYYformat(dr["sims_gb_cat_assign_date"].ToString());
                        assign.Assignment_Due_Date = db.UIDDMMYYYYformat(dr["sims_gb_cat_assign_due_date"].ToString());
                        assign.Assignment_gb_number = dr["sims_gb_number"].ToString();
                        assign.Assignment_grade_code = dr["sims_grade_code"].ToString();
                        assign.Assignment_Name = dr["sims_gb_cat_assign_name"].ToString();
                        assign.Assignment_Remark = dr["sims_gb_cat_assign_remarks"].ToString();
                        assign.Assignment_section_code = dr["sims_section_code"].ToString();

                        gradebook_category gc1 = lst.Single(p => p.gb_number == assign.Assignment_gb_number && p.cur_code == assign.Assignment_cur_code && p.grade_code == assign.Assignment_grade_code && p.section_code == assign.Assignment_section_code).Categories.Single(w => w.cur_code == assign.Assignment_cur_code && w.academic_year == assign.Assignment_academic_year && w.grade_code == assign.Assignment_grade_code && w.section_code == assign.Assignment_section_code && w.cat_code == assign.Assignment_cat_code);

                        if (gc1.AssignMents == null) gc1.AssignMents = new List<SIMSAPI.Models.ERP.gradebookClass.Assignment>();
                        gc1.AssignMents.Add(assign);
                    }
                }
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUDGradeBookDataOpr")]
        public HttpResponseMessage CUDGradeBookDataOpr(List<gradebook> simsobj)
        {
            bool inserted = false;
            gradebook o = new gradebook();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (gradebook data in simsobj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                            new List<SqlParameter>() 
                         { 
                
                new SqlParameter("@OPR",data.OPR),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_GRADE_SCALE", data.gb_grade_scale),
                new SqlParameter("@GB_TEACHER_CODE", data.teacher_code),
                new SqlParameter("@GB_STATUS", data.gb_status==true?"A":"I"),
                new SqlParameter("@GB_SUBJECT_CODE", data.Subject_Code),
                
                         });


                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
        
        [Route("CategoryDataOpr")]
        public HttpResponseMessage CategoryDataOpr(List<gradebook_category> data1)
        {

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (gradebook_category data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.academic_year),
                new SqlParameter("@GRADE_CODE", data.grade_code),
                new SqlParameter("@SECTION_CODE", data.section_code),
                new SqlParameter("@GB_NUMBER", data.gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.cat_code),
                new SqlParameter("@GB_CAT_NAME", data.cat_name),
               
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

        [Route("AssignmentDataOpr")]
        public HttpResponseMessage AssignmentDataOpr(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)//List<SubjectAttr> attrList
        {

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                         new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                new SqlParameter("@GB_ASSIGNMENT_NAME", data.Assignment_Name),
                new SqlParameter("@GB_ASSIGNMENT_REMARK",data.Assignment_Remark),
                new SqlParameter("@GB_ASSIGNMENT_DATE",db.DBYYYYMMDDformat(data.Assignment_Date)),
                new SqlParameter("@GB_ASSIGNMENT_DUE_DATE",db.DBYYYYMMDDformat(data.Assignment_Due_Date))

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("AssignmentDataOprDelete")]
        public HttpResponseMessage AssignmentDataOprDelete(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data1)//List<SubjectAttr> attrList
        {

            string insert = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Assignment data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                         new List<SqlParameter>()
                         {
                new SqlParameter("@OPR", data.OPR),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                new SqlParameter("@GB_ASSIGNMENT_NAME", data.Assignment_Name),
                new SqlParameter("@GB_ASSIGNMENT_REMARK",data.Assignment_Remark),
                new SqlParameter("@GB_ASSIGNMENT_DATE",db.DBYYYYMMDDformat(data.Assignment_Date)),
                new SqlParameter("@GB_ASSIGNMENT_DUE_DATE",db.DBYYYYMMDDformat(data.Assignment_Due_Date))

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if(dr.Read())
                            {
                                insert = dr["assigned_mark"].ToString();
                            }
                           
                        }
                        else
                        {
                            insert = "Assignment Not Deleted";
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



        [Route("AssignmentStudent")]
        public HttpResponseMessage AssignmentStudent(SIMSAPI.Models.ERP.gradebookClass.Assignment data)//List<SubjectAttr> attrList
        {
          List<assessment_student> lst=new List<assessment_student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                         new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "SS"),
                new SqlParameter("@CUR_CODE", data.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", data.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", data.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", data.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.Assignment_Code),
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {

                                assessment_student std=new assessment_student();
                                std.scalelist=new List<student_grade_scale>();

                                string str  =dr["sims_gb_cat_assign_enroll_number"].ToString();

                                std.sims_academic_year                   =dr["sims_academic_year"].ToString();
                                std.sims_cur_code                        =dr["sims_cur_code"].ToString();
                                std.sims_enroll_number                   =dr["sims_gb_cat_assign_enroll_number"].ToString();
                                std.sims_grade_code                      =dr["sims_grade_code"].ToString();
                                std.sims_section_code                    =dr["sims_section_code"] .ToString();
                                std.Stud_Full_Name                       =dr["student_name"].ToString();
                                std.sims_gb_number = dr["sims_gb_number"].ToString();
                                std.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                                std.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                                student_grade_scale sc=new student_grade_scale();
                                sc.sims_gb_cat_assign_final_grade        =dr["sims_gb_cat_assign_final_grade"].ToString();
                                sc.sims_mark_grade_code               =dr["sims_mark_grade_code"].ToString();
                                sc.sims_mark_grade                  =dr["sims_mark_grade"].ToString();
                                sc.sims_mark_grade_description         =dr["sims_mark_grade_description"].ToString();

                                sc.ischecked = dr["status"].ToString().Equals("1") ? true : false;
                                var v = (from p in lst where p.sims_enroll_number == str select p);

                                if(v.Count()==0)
                                {
                                    std.scalelist.Add(sc);
                                    lst.Add(std);
                                }
                                else
                                {
                                    v.ElementAt(0).scalelist.Add(sc);
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "sims.sims_gradebook");
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("AssignMarksToStudentsM")]
        public HttpResponseMessage AssignMarksToStudentsM(List<SIMSAPI.Models.ERP.gradebookClass.Assignment> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (SIMSAPI.Models.ERP.gradebookClass.Assignment a in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "SU"),
                new SqlParameter("@CUR_CODE", a.Assignment_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", a.Assignment_academic_year),
                new SqlParameter("@GRADE_CODE", a.Assignment_grade_code),
                new SqlParameter("@SECTION_CODE", a.Assignment_section_code),
                new SqlParameter("@GB_NUMBER", a.Assignment_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", a.Assignment_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", a.Assignment_Code),
                new SqlParameter("@GB_ASSIGN_FINAL_GRADE", a.sims_mark_final_grade_code),
                new SqlParameter("@GB_ENROLL", a.StudEnroll)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}