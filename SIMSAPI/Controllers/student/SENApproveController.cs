﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.student
{
    [RoutePrefix("api/studentApprove")]
    [BasicAuthentication]
    public class SENApproveController : ApiController
    {

        [Route("getStudSearch")]
        public HttpResponseMessage getStudSearch(string sims_cur_code, string sims_academic_year, string sims_grade_code, string value)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim987> stud_list = new List<Sim987>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'), 
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                             new SqlParameter("@value",value),
                            
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sim987 simsobj = new Sim987();
                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            // simsobj.sims_admission_date = dr["sims_admission_date"].ToString();
                            simsobj.name = dr["name"].ToString();
                            simsobj.sims_admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_sen_approve_remark = dr["sims_sen_approve_remark"].ToString();

                            simsobj.sims_admission_sen_status_created_by = dr["sims_admission_sen_status_created_by"].ToString();
                            simsobj.sims_admission_sen_status_created_date = db.UIDDMMYYYYformat(dr["sims_admission_sen_status_created_date"].ToString());
                            simsobj.sims_admission_sen_approve_status = dr["sims_admission_sen_approve_status"].ToString().Equals("1") ? true : false;

                            simsobj.sims_sen_status_remark = dr["sims_sen_status_remark"].ToString();
                            simsobj.sims_admission_special_education_status = dr["sims_admission_special_education_status"].ToString().Equals("1") ? true : false;
                            simsobj.sims_admission_sr_no = dr["sims_admission_sr_no"].ToString();
                            simsobj.sims_admission_sen_compelted_status = dr["sims_admission_sen_compelted_status"].ToString();
                            stud_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        [Route("CUDStudentSenApprove")]
        public HttpResponseMessage CUDStudentSen(List<Sim987> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sim987 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                                new SqlParameter("@sims_sen_status_remark", simsobj.sims_sen_approve_remark),
                                new SqlParameter("@sims_admission_special_education_status", simsobj.sims_admission_sen_approve_status==true?"1":"0"),
                                  new SqlParameter("@value", simsobj.value),
                                  new SqlParameter("@user_name", simsobj.user_name),
                                  new SqlParameter("@sims_admission_sr_no", simsobj.sims_admission_sr_no),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    message.strMessage = st;
                        // inserted = false;
                        //}
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getApproveList")]
        public HttpResponseMessage getApproveList(string sims_cur_code, string sims_academic_year, string sims_grade_code, string value)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sim987> stud_list = new List<Sim987>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Z'), 
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@value",value),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sim987 simsobj = new Sim987();
                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            // simsobj.sims_admission_date = dr["sims_admission_date"].ToString();
                            simsobj.name = dr["name"].ToString();
                            simsobj.sims_admission_date = db.UIDDMMYYYYformat(dr["sims_admission_date"].ToString());
                            simsobj.sims_admission_dob = db.UIDDMMYYYYformat(dr["sims_admission_dob"].ToString());
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_admission_mobile = dr["sims_admission_mobile"].ToString();
                            simsobj.admission_class = dr["admission_class"].ToString();
                            simsobj.show_button = dr["show_button"].ToString();
                            simsobj.sims_admission_grade_code = dr["sims_admission_grade_code"].ToString();
                            simsobj.sims_admission_academic_year = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_admission_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_admission_sen_approve_status = dr["sims_admission_sen_approve_status"].ToString().Equals("1") ? true : false;

                            simsobj.sims_sen_status_remark = dr["sims_sen_status_remark"].ToString();
                            simsobj.sims_admission_sen_status_created_by = dr["sims_admission_sen_status_created_by"].ToString();
                            simsobj.sims_admission_sen_status_created_date = db.UIDDMMYYYYformat(dr["sims_admission_sen_status_created_date"].ToString());
                            simsobj.sims_sen_approve_remark = dr["sims_sen_approve_remark"].ToString();
                            simsobj.sims_admission_sen_approve_created_by = dr["sims_admission_sen_approve_created_by"].ToString();
                            simsobj.sims_admission_sen_approve_created_date = db.UIDDMMYYYYformat(dr["sims_admission_sen_approve_created_date"].ToString());
                            stud_list.Add(simsobj);


                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        [Route("getStudAttribute")]
        public HttpResponseMessage getStudAttribute()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<Sims031> stud_list = new List<Sims031>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims031 simsobj = new Sims031();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_appl_form_field_value2 = dr["sims_appl_form_field_value2"].ToString();
                            stud_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        [Route("CUDStudentSenApproveList")]
        public HttpResponseMessage CUDStudentSenApproveList(List<SENLST> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (SENLST simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'G'),      
                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                                new SqlParameter("@sims_admission_subject_group", simsobj.sims_admission_subject_group),
                                new SqlParameter("@sims_admission_subject_group_value", simsobj.sims_admission_subject_group_value),
                                new SqlParameter("@sims_diagnosis", simsobj.sims_diagnosis),
                                new SqlParameter("@sims_created_by", simsobj.sims_created_by),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getStudAttributeAssign")]
        public HttpResponseMessage getStudAttributeAssign(string sims_admission_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAgendaDetails(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "getAgendaDetails"));

            List<SENLST> stud_list = new List<SENLST>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),  
                             new SqlParameter("@sims_admission_number", sims_admission_number),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SENLST simsobj = new SENLST();
                            simsobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            simsobj.sims_admission_subject_group = dr["sims_admission_subject_group"].ToString();
                            simsobj.sims_admission_subject_group_value = dr["sims_admission_subject_group_value"].ToString();
                            stud_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, stud_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, stud_list);
        }

        [Route("CUDStudentSenDeleteApproveList")]
        public HttpResponseMessage CUDStudentSenDeleteApproveList(List<SENLST> data)
        {
            Message message = new Message();
            SENLST simsobj = new SENLST();
            if (data.Count>0)
            simsobj = data[0];
            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                 //   foreach (SENLST simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'D'),      
                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                                new SqlParameter("@sims_admission_subject_group", simsobj.sims_admission_subject_group),
                                new SqlParameter("@sims_admission_subject_group_value", simsobj.sims_admission_subject_group_value),
                                new SqlParameter("@sims_diagnosis", simsobj.sims_diagnosis),
                                new SqlParameter("@sims_created_by", simsobj.sims_created_by),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDStudentSenRejectApproveList")]
        public HttpResponseMessage CUDStudentSenRejectApproveList(List<SENLST> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (SENLST simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sen_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'P'),      
                                new SqlParameter("@sims_admission_number", simsobj.sims_admission_number),
                                new SqlParameter("@sims_admission_sr_no", simsobj.sims_admission_sr_no),
                                new SqlParameter("@sims_sen_rejected_by", simsobj.sims_sen_rejected_by),
                                new SqlParameter("@sims_sen_rejected_remark", simsobj.sims_sen_rejected_remark),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}
