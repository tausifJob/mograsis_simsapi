﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/DefineStudentFee")]
    [BasicAuthentication]
    public class DefineStudentFeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Normal Define Student Fee API For All School...

        [Route("GetPageLoadValues")]
        public HttpResponseMessage GetPageLoadValues()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCategoryNames(),PARAMETERS  :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCategoryNames"));

            //List<Sims036> fee_list = new List<Sims036>();


            Dictionary<string, List<Sims036>> fee_list_dictionary = new Dictionary<string, List<Sims036>>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                       new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Curriculum_Names", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.section_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.transport_fee_category_name = dr["sims_fee_category_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Catagory", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Description", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                      new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_frequency = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_frequency_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Frequency", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code_type = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_code_type_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_code_type", fee_list);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, fee_list_dictionary);
            }
            catch (Exception e)
            {
                Log.Error(e);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list_dictionary);
            }
        }

        [Route("getStudentFeeDetails")]
        public HttpResponseMessage getStudentFeeDetails(string cur_name, string academic_year, string grade_name, string section_name, string enrollmentNo)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            List<Sims052> fee_list = new List<Sims052>();
            //int total = 0, skip = 0;
            string str = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_fee_cur_code", cur_name),
                                new SqlParameter("@sims_fee_academic_year", academic_year),
                                new SqlParameter("@sims_fee_grade_code", grade_name),
                                new SqlParameter("@sims_fee_section_code", section_name),
                                 new SqlParameter("@sims_enroll_no", enrollmentNo),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objNew = new Sims052();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();

                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in fee_list where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                objNew.sims_fee_cur_code = dr["sims_cur_code"].ToString();
                                objNew.sims_fee_academic_year = dr["sims_academic_year"].ToString();
                                objNew.sims_fee_grade_code = dr["sims_grade_code"].ToString();
                                objNew.sims_fee_section_code = dr["sims_section_code"].ToString();

                                objNew.sims_fee_category = dr["sims_fee_category"].ToString();
                                objNew.Concession = dr["Concession"].ToString();
                                objNew.studentName = dr["studentName"].ToString();
                                objNew.sims_fee_status = dr["sims_fee_status"].ToString() == "Y" ? true : false;

                                objNew.sims_term_start_date = dr["term_start_date"].ToString();
                                objNew.sims_month_code = dr["month_code"].ToString();
                                Fee_code_list fee_code = new Fee_code_list();
                                fee_code.sims_fee_code = dr["sims_fee_code"].ToString();
                                fee_code.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                                fee_code.isassigned = (dr["isassigned"].ToString() == "1" ? true : false);
                                fee_code.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;
                                fee_code.sims_fee_category = dr["sims_fee_category"].ToString();

                                objNew.sublist.Add(fee_code);


                                fee_list.Add(objNew);
                            }
                            else
                            {
                                Fee_code_list fee_code = new Fee_code_list();
                                fee_code.sims_fee_code = dr["sims_fee_code"].ToString();
                                fee_code.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                                fee_code.isassigned = (dr["isassigned"].ToString() == "1" ? true : false);
                                fee_code.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;
                                fee_code.sims_fee_category = dr["sims_fee_category"].ToString();

                                v.ElementAt(0).sublist.Add(fee_code);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("getStudentDetails")]
        public HttpResponseMessage getStudentDetails(string data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            List<Sims052> fee_list = new List<Sims052>();
            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(data);



            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                 new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                  new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                   new SqlParameter("@sims_fee_code", obj.sims_fee_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objNew = new Sims052();
                            objNew.Concession = "0";
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            objNew.sims_fee_number = dr["sims_fee_number"].ToString();
                            objNew.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            //objNew.studentName = dr["studentName"].ToString();
                            objNew.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.sims_fee_category = dr["sims_fee_category"].ToString();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            objNew.sims_fee_frequency = dr["sims_fee_frequency"].ToString();
                            objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            objNew.sims_term_start_date = dr["term_start_date"].ToString();
                            objNew.sims_month_code = dr["month_code"].ToString();
                            objNew.sims_fee_installment_mode = (dr["sims_fee_installment_mode"].ToString() == "Y" ? true : false);
                            objNew.sims_fee_installment_min_amount = dr["sims_fee_installment_min_amount"].ToString();
                            objNew.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                            objNew.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                            objNew.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                            objNew.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                            objNew.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                            objNew.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                            objNew.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                            objNew.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                            objNew.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                            objNew.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                            objNew.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                            objNew.sims_fee_period12 = dr["sims_fee_period12"].ToString();
                            objNew.sims_fee_status = (dr["sims_fee_status"].ToString() == "A" ? true : false);

                            fee_list.Add(objNew);
                        }
                    }

                    dr.Close();
                    dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                    new List<SqlParameter>()
                                {
                                     new SqlParameter("@opr", 'C'),
                                     new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                     new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                       new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objnew = new Sims052();
                            concessiondetails conc = new concessiondetails();
                            objnew.Concession = "1";
                            conc.sims_concession_no = dr["sims_concession_no"].ToString();
                            conc.sims_concession_desc = dr["sims_concession_desc"].ToString();
                            conc.sims_concession_type = dr["sims_concession_type"].ToString();
                            conc.sims_conession_fee_type = dr["sims_conession_fee_type"].ToString();
                            conc.sims_conession_to_date = dr["sims_conession_to_date"].ToString();
                            conc.sims_conession_status = dr["sims_conession_status"].ToString() == "P" ? "Approved" : "Active";
                            conc.sims_fee_period1_concession = dr["sims_fee_period1_concession"].ToString();
                            conc.sims_fee_period1_concession_paid = dr["sims_fee_period1_concession_paid"].ToString();
                            conc.sims_fee_period2_concession = dr["sims_fee_period2_concession"].ToString();
                            conc.sims_fee_period2_concession_paid = dr["sims_fee_period2_concession_paid"].ToString();
                            conc.sims_fee_period3_concession = dr["sims_fee_period3_concession"].ToString();
                            conc.sims_fee_period3_concession_paid = dr["sims_fee_period3_concession_paid"].ToString();
                            conc.sims_fee_period4_concession = dr["sims_fee_period4_concession"].ToString();
                            conc.sims_fee_period4_concession_paid = dr["sims_fee_period4_concession_paid"].ToString();

                            conc.sims_fee_period5_concession = dr["sims_fee_period5_concession"].ToString();
                            conc.sims_fee_period5_concession_paid = dr["sims_fee_period5_concession_paid"].ToString();
                            conc.sims_fee_period6_concession = dr["sims_fee_period6_concession"].ToString();
                            conc.sims_fee_period6_concession_paid = dr["sims_fee_period6_concession_paid"].ToString();
                            conc.sims_fee_period7_concession = dr["sims_fee_period7_concession"].ToString();
                            conc.sims_fee_period7_concession_paid = dr["sims_fee_period7_concession_paid"].ToString();
                            conc.sims_fee_period8_concession = dr["sims_fee_period8_concession"].ToString();
                            conc.sims_fee_period8_concession_paid = dr["sims_fee_period8_concession_paid"].ToString();
                            conc.sims_fee_period9_concession = dr["sims_fee_period9_concession"].ToString();
                            conc.sims_fee_period9_concession_paid = dr["sims_fee_period9_concession_paid"].ToString();
                            conc.sims_fee_period10_concession = dr["sims_fee_period10_concession"].ToString();
                            conc.sims_fee_period10_concession_paid = dr["sims_fee_period10_concession_paid"].ToString();

                            conc.sims_fee_period11_concession = dr["sims_fee_period11_concession"].ToString();
                            conc.sims_fee_period11_concession_paid = dr["sims_fee_period11_concession_paid"].ToString();
                            conc.sims_fee_period12_concession = dr["sims_fee_period12_concession"].ToString();
                            conc.sims_fee_period12_concession_paid = dr["sims_fee_period12_concession_paid"].ToString();
                            conc.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;

                            objnew.concessiondetails.Add(conc);


                            fee_list.Add(objnew);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("updatstudentdetails")]
        public HttpResponseMessage updatstudentdetails(string update_data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(update_data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),

                                new SqlParameter("@sims_fee_frequency", obj.sims_fee_frequency),
                                new SqlParameter("@sims_fee_installment_mode", obj.sims_fee_installment_mode==true?"Y":"N"),
                                new SqlParameter("@sims_fee_installment_min_amount", obj.sims_fee_installment_min_amount),
                                new SqlParameter("@sims_fee_period1", obj.sims_fee_period1),
                                new SqlParameter("@sims_fee_period2", obj.sims_fee_period2),
                                new SqlParameter("@sims_fee_period3", obj.sims_fee_period3),
                                new SqlParameter("@sims_fee_period4", obj.sims_fee_period4),
                                new SqlParameter("@sims_fee_period5", obj.sims_fee_period5),
                                new SqlParameter("@sims_fee_period6", obj.sims_fee_period6),
                                new SqlParameter("@sims_fee_period7", obj.sims_fee_period7),
                                new SqlParameter("@sims_fee_period8", obj.sims_fee_period8),
                                new SqlParameter("@sims_fee_period9", obj.sims_fee_period9),
                                new SqlParameter("@sims_fee_period10", obj.sims_fee_period10),
                                new SqlParameter("@sims_fee_period11", obj.sims_fee_period11),
                                new SqlParameter("@sims_fee_period12", obj.sims_fee_period12),
                                new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("UnAssignStudentFee")]
        public HttpResponseMessage UnAssignStudentFee(string update_data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(update_data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                //new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),
                              
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("Assign_fee_to_student")]
        public HttpResponseMessage Assign_fee_to_student(string data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_frequency","M"),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@sims_fee_status",obj.sims_fee_status==true?"A":"I"),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("Gettermmonthcodes")]
        public HttpResponseMessage Gettermmonthcodes(string cur_code, string academic_year)
        {
            List<Sims052> fee_list = new List<Sims052>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                       new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", 'T'),
                         new SqlParameter("@sims_fee_cur_code", cur_code),
                         new SqlParameter("@sims_fee_academic_year", academic_year)

                         });
                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            Sims052 simsobj = new Sims052();

                            simsobj.sims_month_code = dr["month_code"].ToString();

                            fee_list.Add(simsobj);
                        }

                    }
                    dr.Close();

                }


            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);

        }

        [Route("Gettableheaderfees")]
        public HttpResponseMessage Gettableheaderfees()
        {
            List<Sims036> fee_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_proc]",
                       new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", 'Q'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                    dr.Close();

                }
            }
            catch (Exception e)
            {
                Log.Error(e);


            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);

        }

        #endregion

        #region Define Student Fee API For NISS...

        [Route("Getfeecategory")]
        public HttpResponseMessage Getfeecategory()
        {
            List<Sims052> fee_list = new List<Sims052>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_niss_proc]",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 simsobj = new Sims052();
                            simsobj.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            simsobj.sims_fee_category1 = dr["sims_fee_category"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("updatstudentdetailswithfc")]
        public HttpResponseMessage updatstudentdetailswithfc(string update_data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(update_data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_niss_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@sims_fee_category1", obj.sims_fee_category1),
                                new SqlParameter("@sims_fee_frequency", obj.sims_fee_frequency),
                                new SqlParameter("@sims_fee_installment_mode", obj.sims_fee_installment_mode==true?"Y":"N"),
                                new SqlParameter("@sims_fee_installment_min_amount", obj.sims_fee_installment_min_amount),
                                new SqlParameter("@sims_fee_period1", obj.sims_fee_period1),
                                new SqlParameter("@sims_fee_period2", obj.sims_fee_period2),
                                new SqlParameter("@sims_fee_period3", obj.sims_fee_period3),
                                new SqlParameter("@sims_fee_period4", obj.sims_fee_period4),
                                new SqlParameter("@sims_fee_period5", obj.sims_fee_period5),
                                new SqlParameter("@sims_fee_period6", obj.sims_fee_period6),
                                new SqlParameter("@sims_fee_period7", obj.sims_fee_period7),
                                new SqlParameter("@sims_fee_period8", obj.sims_fee_period8),
                                new SqlParameter("@sims_fee_period9", obj.sims_fee_period9),
                                new SqlParameter("@sims_fee_period10", obj.sims_fee_period10),
                                new SqlParameter("@sims_fee_period11", obj.sims_fee_period11),
                                new SqlParameter("@sims_fee_period12", obj.sims_fee_period12),
                                new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),

                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("Assign_fee_to_studentwithfc")]
        public HttpResponseMessage Assign_fee_to_studentwithfc(string data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_niss_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_category1", obj.sims_fee_category1),
                                new SqlParameter("@sims_fee_frequency","M"),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@sims_fee_status",obj.sims_fee_status==true?"A":"I"),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("UnAssignStudentFeewithfc")]
        public HttpResponseMessage UnAssignStudentFeewithfc(string update_data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(update_data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_niss_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_category1", obj.sims_fee_category1),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                //new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),
                              
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        #endregion


        #region define student fee new table structure


        [Route("GetPageLoadValuesNew")]
        public HttpResponseMessage GetPageLoadValuesNew()
        {

            //List<Sims036> fee_list = new List<Sims036>();


            Dictionary<string, List<Sims036>> fee_list_dictionary = new Dictionary<string, List<Sims036>>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                       new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Curriculum_Names", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee_new",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.section_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.transport_fee_category_name = dr["sims_fee_category_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Catagory", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee_new",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Description", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee_new",
                      new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_frequency = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_frequency_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Frequency", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee_new",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code_type = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_code_type_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_code_type", fee_list);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, fee_list_dictionary);
            }
            catch (Exception e)
            {
                Log.Error(e);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list_dictionary);
            }
        }

        [Route("getStudentFeeDetailsNew")]
        public HttpResponseMessage getStudentFeeDetailsNew(string cur_name, string academic_year, string grade_name, string section_name, string enrollmentNo, string user_name)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            List<Sims052> fee_list = new List<Sims052>();
            //int total = 0, skip = 0;
            string str = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_fee_cur_code", cur_name),
                                new SqlParameter("@sims_fee_academic_year", academic_year),
                                new SqlParameter("@sims_fee_grade_code", grade_name),
                                new SqlParameter("@sims_fee_section_code", section_name),
                                new SqlParameter("@sims_enroll_no", enrollmentNo),
                                new SqlParameter("@user_name", user_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objNew = new Sims052();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();

                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in fee_list where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                objNew.sims_fee_cur_code = dr["sims_cur_code"].ToString();
                                objNew.sims_fee_academic_year = dr["sims_academic_year"].ToString();
                                objNew.sims_fee_grade_code = dr["sims_grade_code"].ToString();
                                objNew.sims_fee_section_code = dr["sims_section_code"].ToString();

                                objNew.sims_fee_category = dr["sims_fee_category"].ToString();
                                objNew.Concession = dr["Concession"].ToString();
                                objNew.studentName = dr["studentName"].ToString();
                                objNew.sims_fee_status = dr["sims_fee_status"].ToString() == "Y" ? true : false;

                                objNew.sims_term_start_date = dr["term_start_date"].ToString();
                                objNew.sims_month_code = dr["month_code"].ToString();
                                Fee_code_list fee_code = new Fee_code_list();
                                fee_code.sims_fee_code = dr["sims_fee_code"].ToString();
                                fee_code.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                                fee_code.isassigned = (dr["isassigned"].ToString() == "1" ? true : false);
                                fee_code.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;
                                fee_code.sims_fee_category = dr["sims_fee_category"].ToString();

                                objNew.sublist.Add(fee_code);


                                fee_list.Add(objNew);
                            }
                            else
                            {
                                Fee_code_list fee_code = new Fee_code_list();
                                fee_code.sims_fee_code = dr["sims_fee_code"].ToString();
                                fee_code.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                                fee_code.isassigned = (dr["isassigned"].ToString() == "1" ? true : false);
                                fee_code.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;
                                fee_code.sims_fee_category = dr["sims_fee_category"].ToString();

                                v.ElementAt(0).sublist.Add(fee_code);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("getStudentDetailsNew")]
        public HttpResponseMessage getStudentDetailsNew(string data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            List<Sims052> fee_list = new List<Sims052>();
            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(data);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objNew = new Sims052();
                            objNew.Concession = "0";
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            // objNew.sims_fee_number = dr["sims_fee_number"].ToString();
                            objNew.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            //objNew.studentName = dr["studentName"].ToString();
                            objNew.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.sims_fee_category = dr["sims_fee_category"].ToString();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            objNew.sims_fee_frequency = dr["sims_fee_frequency"].ToString();
                            objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            objNew.sims_term_start_date = dr["term_start_date"].ToString();
                            objNew.sims_month_code = dr["month_code"].ToString();
                            objNew.sims_fee_installment_mode = (dr["sims_fee_installment_mode"].ToString() == "Y" ? true : false);
                            objNew.sims_fee_installment_min_amount = dr["sims_fee_installment_min_amount"].ToString();
                            objNew.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                            objNew.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                            objNew.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                            objNew.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                            objNew.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                            objNew.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                            objNew.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                            objNew.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                            objNew.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                            objNew.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                            objNew.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                            objNew.sims_fee_period12 = dr["sims_fee_period12"].ToString();
                            objNew.sims_fee_status = (dr["sims_fee_status"].ToString() == "A" ? true : false);

                            fee_list.Add(objNew);
                        }
                    }

                    dr.Close();
                    dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                    new List<SqlParameter>()
                                {
                                     new SqlParameter("@opr", 'C'),
                                     new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                     new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                       new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims052 objnew = new Sims052();
                            concessiondetails conc = new concessiondetails();
                            objnew.Concession = "1";
                            conc.sims_concession_no = dr["sims_concession_no"].ToString();
                            conc.sims_concession_desc = dr["sims_concession_desc"].ToString();
                            conc.sims_concession_type = dr["sims_concession_type"].ToString();
                            conc.sims_conession_fee_type = dr["sims_conession_fee_type"].ToString();
                            conc.sims_conession_to_date = dr["sims_conession_to_date"].ToString();
                            conc.sims_conession_status = dr["sims_conession_status"].ToString() == "P" ? "Approved" : "Active";
                            conc.sims_fee_period1_concession = dr["sims_fee_period1_concession"].ToString();
                            conc.sims_fee_period1_concession_paid = dr["sims_fee_period1_concession_paid"].ToString();
                            conc.sims_fee_period2_concession = dr["sims_fee_period2_concession"].ToString();
                            conc.sims_fee_period2_concession_paid = dr["sims_fee_period2_concession_paid"].ToString();
                            conc.sims_fee_period3_concession = dr["sims_fee_period3_concession"].ToString();
                            conc.sims_fee_period3_concession_paid = dr["sims_fee_period3_concession_paid"].ToString();
                            conc.sims_fee_period4_concession = dr["sims_fee_period4_concession"].ToString();
                            conc.sims_fee_period4_concession_paid = dr["sims_fee_period4_concession_paid"].ToString();

                            conc.sims_fee_period5_concession = dr["sims_fee_period5_concession"].ToString();
                            conc.sims_fee_period5_concession_paid = dr["sims_fee_period5_concession_paid"].ToString();
                            conc.sims_fee_period6_concession = dr["sims_fee_period6_concession"].ToString();
                            conc.sims_fee_period6_concession_paid = dr["sims_fee_period6_concession_paid"].ToString();
                            conc.sims_fee_period7_concession = dr["sims_fee_period7_concession"].ToString();
                            conc.sims_fee_period7_concession_paid = dr["sims_fee_period7_concession_paid"].ToString();
                            conc.sims_fee_period8_concession = dr["sims_fee_period8_concession"].ToString();
                            conc.sims_fee_period8_concession_paid = dr["sims_fee_period8_concession_paid"].ToString();
                            conc.sims_fee_period9_concession = dr["sims_fee_period9_concession"].ToString();
                            conc.sims_fee_period9_concession_paid = dr["sims_fee_period9_concession_paid"].ToString();
                            conc.sims_fee_period10_concession = dr["sims_fee_period10_concession"].ToString();
                            conc.sims_fee_period10_concession_paid = dr["sims_fee_period10_concession_paid"].ToString();

                            conc.sims_fee_period11_concession = dr["sims_fee_period11_concession"].ToString();
                            conc.sims_fee_period11_concession_paid = dr["sims_fee_period11_concession_paid"].ToString();
                            conc.sims_fee_period12_concession = dr["sims_fee_period12_concession"].ToString();
                            conc.sims_fee_period12_concession_paid = dr["sims_fee_period12_concession_paid"].ToString();
                            conc.sims_fee_status = dr["sims_fee_status"].ToString() == "A" ? true : false;

                            objnew.concessiondetails.Add(conc);


                            fee_list.Add(objnew);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("updatstudentdetailsNew")]
        public HttpResponseMessage updatstudentdetailsNew(List<Sims052> update_data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims052 obj in update_data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                            new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@sims_fee_frequency", obj.sims_fee_frequency),
                                new SqlParameter("@sims_fee_amount",obj.sims_fee_amount),
                                new SqlParameter("@sims_fee_installment_mode", obj.sims_fee_installment_mode==true?"Y":"N"),
                                new SqlParameter("@sims_fee_installment_min_amount", obj.sims_fee_installment_min_amount),
                                new SqlParameter("@sims_fee_period_code", obj.sims_fee_period_code),
                                new SqlParameter("@sims_fee_period_amt", obj.sims_fee_period_amt),
                                new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),

                             });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("UnAssignStudentFeeNew")]
        public HttpResponseMessage UnAssignStudentFeeNew(string update_data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(update_data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@user_name", obj.user_name),

                                //new SqlParameter("@sims_fee_status", obj.sims_fee_status==true?"A":"I"),
                              
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("Assign_fee_to_studentNew")]
        public HttpResponseMessage Assign_fee_to_studentNew(string data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudentFeeDetails(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5}";
            //  Log.Debug(string.Format(debug, "STUDENT", "getStudentFeeDetails"));

            Sims052 obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims052>(data);

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_enroll_no", obj.sims_enroll_number),
                                new SqlParameter("@sims_fee_cur_code", obj.sims_fee_cur_code),
                                new SqlParameter("@sims_fee_academic_year", obj.sims_fee_academic_year),
                                new SqlParameter("@sims_fee_grade_code", obj.sims_fee_grade_code),
                                new SqlParameter("@sims_fee_section_code", obj.sims_fee_section_code),
                                new SqlParameter("@sims_fee_category", obj.sims_fee_category),
                                new SqlParameter("@sims_fee_frequency","M"),
                                new SqlParameter("@sims_fee_code", obj.sims_fee_code),
                                new SqlParameter("@sims_fee_status",obj.sims_fee_status==true?"A":"I"),
                                new SqlParameter("@user_name", obj.user_name),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
        }

        [Route("GettermmonthcodesNew")]
        public HttpResponseMessage GettermmonthcodesNew(string cur_code, string academic_year)
        {
            List<Sims052> fee_list = new List<Sims052>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                       new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", 'T'),
                         new SqlParameter("@sims_fee_cur_code", cur_code),
                         new SqlParameter("@sims_fee_academic_year", academic_year)

                         });
                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            Sims052 simsobj = new Sims052();

                            simsobj.sims_month_code = dr["month_code"].ToString();

                            fee_list.Add(simsobj);
                        }

                    }
                    dr.Close();

                }


            }
            catch (Exception e)
            {
                Log.Error(e);


            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);

        }

        [Route("GettableheaderfeesNew")]
        public HttpResponseMessage GettableheaderfeesNew()
        {
            List<Sims036> fee_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[define_student_fee_New_proc]",
                       new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", 'Q'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                    dr.Close();

                }
            }
            catch (Exception e)
            {
                Log.Error(e);


            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);

        }


        #endregion


    }
}