﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.StudentEANumberController
{
    [RoutePrefix("api/StudentEA")]
    [BasicAuthentication]
    public class StudentEANumberController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllStudentEA")]
        public HttpResponseMessage getAllStudentEA()
        {

            List<StudentEA> list = new List<StudentEA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_ea_number_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "S"),
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StudentEA simsobj = new StudentEA();
                            simsobj.sims_enrollment_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_full_name = dr["Name_in_English"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_cur_code = dr["curr_code"].ToString();
                            simsobj.sims_academic_year = dr["acad_yr"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.status_name = dr["status_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getStudentEA")]
        public HttpResponseMessage getStudentEA(string cur_code, string academic_year, string grade_code, string section_code)
        {
            if (grade_code == "undefined")
            {
                grade_code = null;
            }
            if (section_code == "undefined")
            {
                section_code = null;
            }
            List<StudentEA> list = new List<StudentEA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_ea_number_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                         new SqlParameter("@opr", "K"),
                         new SqlParameter("@sims_cur_code",cur_code),
                         new SqlParameter("@sims_academic_year",academic_year),
                         new SqlParameter("@sims_grade_code",grade_code),
                         new SqlParameter("@sims_section_code",section_code)
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StudentEA simsobj = new StudentEA();
                            simsobj.sims_enrollment_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_student_full_name = dr["Name_in_English"].ToString();
                            simsobj.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            simsobj.sims_cur_code = dr["curr_code"].ToString();
                            simsobj.sims_academic_year = dr["acad_yr"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.status_name = dr["status_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CUDStudentEA")]
        public HttpResponseMessage CUDStudentEA(List<StudentEA> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (StudentEA simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_ea_number_proc]",
                            new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_student_enroll_number",simsobj.sims_enrollment_number),
                               new SqlParameter("@sims_student_ea_number",simsobj.sims_student_ea_number),
                         
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}