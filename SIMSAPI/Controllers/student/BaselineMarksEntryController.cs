﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.gradebookClass;


namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/BaselineMarksEntry")]
    public class BaselineMarksEntryController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("insert_student_section_subject")]
        public HttpResponseMessage insert_student_section_subject(List<Sims089> ob)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insert_student_section_subject(),PARAMETERS :: obj {2},Opr {2}";
            Log.Debug(string.Format(debug, "PP", "insert_student_section_subject", ob));

            Message message = new Message();
            string opr = "";

            List<SqlParameter> lst = new List<SqlParameter>();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    foreach (Sims089 o in ob)
                    {
                        
                        //foreach (subject x in o.sublist)
                        //{
                            lst.Clear();
                            lst.Add(new SqlParameter("@opr", "I")); //"P"
                            lst.Add(new SqlParameter("@sims_cur_code", o.sims_cur_code));
                            lst.Add(new SqlParameter("@sims_academic_year", o.sims_academic_year));
                            lst.Add(new SqlParameter("@sims_grade_code", o.sims_grade_code));
                            lst.Add(new SqlParameter("@sims_section_code", o.sims_section_code));
                            lst.Add(new SqlParameter("@sims_enroll_number", o.sims_student_enroll_number));
                            lst.Add(new SqlParameter("@sims_subject_code", o.sims_subject_code));
                            lst.Add(new SqlParameter("@sims_subject_baseline_obtained_score", o.sims_mark_code));


                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_baseline_marks_entry_proc]", lst);


                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        
                    }
                }
            }

            catch (Exception x)
            {
                message.strMessage = "Error In Parsing Fee Details";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }








        [Route("getStudentName")]
        public HttpResponseMessage getStudentName(string curcode, string gradecode, string academicyear, string section, string subcode)
        {
            string cur_code = string.Empty;
            string str = "";
            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_baseline_marks_entry_proc",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","Q"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_subject_code", subcode==""?null:subcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.sims_student_enroll_number == str select p);

                            Sims089 ob = new Sims089();
                            ob.sublist = new List<subject>();
                            ob.sims_cur_code = dr["sims_cur_code"].ToString();
                            ob.sims_academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_student_enroll_number = str;
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_student_name = dr["StudentName"].ToString();
                            ob.sims_subject_baseline_score_type = dr["sims_subject_baseline_score_type"].ToString();


                            subject sb = new subject();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_mark_code = dr["sims_subject_baseline_obtained_score"].ToString();
                            sb.sims_mark_grade_high_marks = dr["sims_mark_grade_high_marks"].ToString();
                            sb.sims_status = dr["Status"].Equals(1) ? true : false;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }


        [Route("getSectionsubject_name")]
        public HttpResponseMessage getSectionsubject_name(string curcode, string gradecode, string academicyear, string section, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","Z"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getmark_grade_name")]
        public HttpResponseMessage getmark_grade_name(string sims_mark_code)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_baseline_marks_entry_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","X"),
                new SqlParameter("@sims_mark_code", sims_mark_code)
                 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();

                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("getcheckSectionsubject_name")]
        public HttpResponseMessage getcheckSectionsubject_name(string curcode, string gradecode, string academicyear, string section, string enroll_number, string subject_code)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","C"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_enroll_number", enroll_number),
                new SqlParameter("@sims_subject_code", subject_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_student_name = dr["student_name"].ToString();
                            newobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("getgrade_name")]
        public HttpResponseMessage getgrade_name(string curcode, string academicyear, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","G"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            newobj.grade_code_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getsection_name")]
        public HttpResponseMessage getsection_name(string curcode, string academicyear, string gradecode, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","M"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                  new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            newobj.sims_section_code = dr["sims_section_code"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetRCGrade")]
        public HttpResponseMessage GetRCGrade(string curcode, string ayear, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_subject_proc",
                    new List<SqlParameter>()
                    {

                        new SqlParameter("@opr", "R"),
                        new SqlParameter("@sims_cur_code", curcode),
                        new SqlParameter("@bell_academic_year", ayear),
                        new SqlParameter("@user_name", teachercode),
                        new SqlParameter("@teacher_code", teachercode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCSection")]
        public HttpResponseMessage GetRCSection(string curcode, string ayear, string gradecode, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_subject_proc",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", gradecode),
                            new SqlParameter("@user_name", teachercode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("getGrade")]
        public HttpResponseMessage getGrade()
        {

            List<Sims090> Grade = new List<Sims090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_baseline_marks_entry_proc",
                    new List<SqlParameter>()
                    {

                        new SqlParameter("@opr", "V"),
                        //new SqlParameter("@sims_mark_grade_code", sims_mark_grade_code),
                        //new SqlParameter("@sims_mark_grade_name", sims_mark_grade_name),
                 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims090 obj = new Sims090();
                            obj.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
                            obj.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
                            Grade.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, Grade);
        }

        [Route("getType")]
        public HttpResponseMessage getType(string curCode, string acyear, string gradecode, string sectcode, string subcode)
        {

            List<Sims090> Base = new List<Sims090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_baseline_marks_entry_proc",
                    new List<SqlParameter>()
                    {

                        new SqlParameter("@opr", "W"),
                         new SqlParameter("@sims_cur_code", curCode),
                         new SqlParameter("@sims_academic_year", acyear),
                         new SqlParameter("@sims_grade_code", gradecode),
                         new SqlParameter("@sims_section_code", sectcode),
                         new SqlParameter("@sims_subject_code", subcode),    
                  
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims090 obj = new Sims090();
                            obj.sims_subject_baseline_score_type = dr["sims_subject_baseline_score_type"].ToString();
                            if (obj.sims_subject_baseline_score_type == "M")
                            {
                                obj.G = false;
                            }
                            else
                            {
                                obj.M = false;
                            }
                            Base.Add(obj);

                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, Base);
        }

        [Route("getGradePoints")]
        public HttpResponseMessage getGradePoints(string grCode)
        {

            List<Sims090> Point = new List<Sims090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_baseline_marks_entry_proc",
                    new List<SqlParameter>()
                    {

                        new SqlParameter("@opr", "Y"),
                        new SqlParameter("@sims_mark_grade_code",grCode),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims090 obj = new Sims090();
                            obj.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
                            obj.sims_mark_grade_high = dr["sims_mark_grade_high"].ToString();
                            Point.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, Point);
        }

        //[Route("getGradeData")]
        //public HttpResponseMessage getGradEData()
        //{

        //    List<NarrativeGrade> lst = new List<NarrativeGrade>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims.sims_baseline_marks_entry_proc]",
        //                new List<SqlParameter>()
        //                 {

        //                     new SqlParameter("@OPR", "ZX"),

        //                 });
        //            while (dr.Read())
        //            {
        //                NarrativeGrade o = new NarrativeGrade();
        //                o.sims_mark_grade_code = dr["sims_mark_grade_code"].ToString();
        //                o.sims_mark_grade_name = dr["sims_mark_grade_name"].ToString();
        //                //o.sims_mark_grade_description = dr["sims_mark_grade_description"].ToString();
        //                o.sims_mark_grade_low = dr["sims_mark_grade_low"].ToString();
        //                o.sims_mark_grade_high = dr["sims_mark_grade_high"].ToString();
        //                o.sims_mark_grade_point = dr["sims_mark_grade_point"].ToString();
        //                o.GradeGroupCode = dr["sims_mark_grade"].ToString();
        //                //o.GradeGroupDesc = dr["Grade_D"].ToString();
        //                lst.Add(o);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, lst);

        //}
    }
}