﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.ERP.gradebookClass;
using SIMSAPI.Helper;
using System.Net;
using System.Collections;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/analysisreport")]
    public class AnalysisReportController : ApiController
    {

        // reportgraph api's

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string cur_code)
        {

            List<studstatus> mod_list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@cur_code",cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<studstatus> mod_list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'H'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.cur_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.cur_code = dr["sims_cur_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getStudentNationalityData")]
        public HttpResponseMessage getStudentNationalityData(string curCode, string acaYear)
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "A"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.country = dr["sims_nationality_name_en"].ToString();
                            simsobj.female = dr["Female"].ToString();
                            simsobj.male = dr["Male"].ToString();
                            simsobj.total = dr["Total"].ToString();
                            simsobj.percentage = dr["percentage"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getStudentStatusData")]
        public HttpResponseMessage getStudentStatusData(string curCode, string acaYear)
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "B"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.grades = dr["sims_grade_name_en"].ToString();
                            simsobj.present_class = dr["present_opened_class"].ToString();
                            simsobj.class_capacity = dr["capacity_per_class"].ToString();
                            simsobj.curr_capacity = dr["current_capacity"].ToString();
                            simsobj.active_students = dr["active_students"].ToString();
                            simsobj.withdrawn_students = dr["withdrawn_students"].ToString();
                            simsobj.present_attendees = dr["present_attendees"].ToString();
                            simsobj.vacancy = dr["vacancy"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getStaffDetailsData")]
        public HttpResponseMessage getStaffDetailsData()
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[dbo].[sims_daily_analysis]",
                       new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }

        [Route("getNewStaffDetailsData")] // only for siso
        public HttpResponseMessage getNewStaffDetailsData(string sopr)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[dbo].[sims_daily_analysis]",
                       new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "J"),
                             new SqlParameter("@sopr", sopr),
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }
        
        
        [Route("getStudentIntakeData")]
        public HttpResponseMessage getStudentIntakeData(string curCode, string acaYear)
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "E"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.enquiry_cnt = dr["enquiry_cnt"].ToString();
                            simsobj.reg_stud = dr["reg_stud"].ToString();
                            simsobj.paid_stud = dr["paid_stud"].ToString();
                            simsobj.per_stud_reg_to_enq = dr["per_stud_reg_to_enq"].ToString();
                            simsobj.per_stud_paid_to_enq = dr["per_stud_paid_to_enq"].ToString();
                            simsobj.per_stud_paid_to_reg = dr["per_stud_paid_to_reg"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getStudentDiscount")]
        public HttpResponseMessage getStudentDiscount(string curCode, string acaYear)
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "G"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.Concession= dr["sims_concession_description"].ToString();
                            simsobj.Discount = dr["sims_concession_discount_value"].ToString();
                            simsobj.Student = dr["stud_cnt"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getOtherStatistics")]
        public HttpResponseMessage getOtherStatistics(string curCode, string acaYear)
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "F"),
                           new SqlParameter("@cur_code", curCode),
                           new SqlParameter("@acad_year", acaYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            //  simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.avg_expected = dr["avg_expected"].ToString();
                            simsobj.avg_concession = dr["avg_concession"].ToString();
                            simsobj.avg_net_tution = dr["avg_net_tution"].ToString();
                            simsobj.per_stud_avail_discount = dr["per_stud_avail_discount"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getMovementOfMonth")]
        public HttpResponseMessage getMovementOfMonth()
        {
            List<studstatus> list = new List<studstatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_daily_analysis]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "D")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studstatus simsobj = new studstatus();
                            simsobj.staff_name = dr["staff_name"].ToString();
                            simsobj.nationality_name = dr["sims_nationality_name_en"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            simsobj.staff_type = dr["staff_type"].ToString();
                            simsobj.staff_status = dr["em_service_status"].ToString();
                            simsobj.em_date_of_join = dr["em_date_of_join"].ToString();
                            simsobj.em_left_date = dr["em_left_date"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        //////////////////// class /////////////////////


    }
}