﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using System.Net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/TeacherAssessmentEntry")]
    public class TeacherAssessmentEntryController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Student Assessment Schedule

        [Route("GetAssesmentDate")]
        public HttpResponseMessage GetAssesmentDate(string cur_code, string acad_yr, string grade_code)
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_assesment_entry_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "A"),
                                 new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_acad_yr",acad_yr),
                                new SqlParameter("@sims_grade_code",grade_code),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAssesmentTeacherEntryData")]
        public HttpResponseMessage GetAssesmentTeacherEntryData(string cur_code, string acad_yr, string grade_code,string assessment_date)
        {
            List<studAss> mod_list = new List<studAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_assesment_entry_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@sims_cur_code",cur_code),
                                new SqlParameter("@sims_acad_yr",acad_yr),
                                new SqlParameter("@sims_grade_code",grade_code),
                                new SqlParameter("@sims_assesment_date",assessment_date)
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studAss simsobj = new studAss();
                            simsobj.sims_pros_no = dr["sims_admission_number"].ToString();
                            simsobj.sims_student_name = dr["stud_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_admission_cur_code"].ToString();
                            simsobj.sims_acad_yr = dr["sims_admission_academic_year"].ToString();
                            simsobj.sims_appl_date = dr["sims_pros_date_created"].ToString();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            simsobj.sims_assesment_teacher_code = dr["criteria_name"].ToString();
                           // simsobj.sims_assesment_teacher_name = dr["teacher_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CUDInsertAssesmentTeacherEntry")]
        public HttpResponseMessage CUDInsertAssesmentTeacherEntry(List<studAss> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (studAss simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_teacher_assesment_entry_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_yr", simsobj.sims_acad_yr),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_teacher_code",simsobj.sims_teacher_code),
                            new SqlParameter("@sims_assesment_date", db.DBYYYYMMDDformat(simsobj.sims_assesment_date)),
                            new SqlParameter("@sims_prosno_list",simsobj.sims_prosno_list),
                            new SqlParameter("@sims_assessment_created_user",simsobj.sims_assessment_created_user)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("assesmentTeacherEntry")]
        public HttpResponseMessage assesmentTeacherEntry(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_teacher_assesment_entry_proc]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CURDassesmentTeacherEntry")]
        public HttpResponseMessage CURDassesmentTeacherEntry(List<studAss> data)
        {
            bool alert_status = false;
          //  studAss attObj = Newtonsoft.Json.JsonConvert.DeserializeObject<reportCardAttend>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (studAss obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_assesment_entry_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                //new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                //new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                //new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                //new SqlParameter("@sims_section_code", obj.sims_section_code),
                                //new SqlParameter("@sims_enrollment_number", obj.sims_enrollment_number),
                                new SqlParameter("@sims_prosno_list", obj.admission_no),
                                new SqlParameter("@sims_admission_criteria_code", obj.sims_attendance_code),
                                new SqlParameter("@sims_admission_marks", obj.sims_attendance_cout),
                                new SqlParameter("@sims_admission_student_remark",obj.sims_admission_student_remark )
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

        #endregion


        #region Employee Assessment Schedule

        [Route("GetAssesmentEmployeeDate")]
        public HttpResponseMessage GetAssesmentEmployeeDate()
        {
            List<EmpAss> mod_list = new List<EmpAss>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_entry_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "A"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpAss simsobj = new EmpAss();
                            simsobj.sims_assesment_date = dr["sims_assesment_date"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetEmployeeAssesmentSchedule")]
        public HttpResponseMessage GetEmployeeAssesmentSchedule(string date)
        {
            List<EmpAss> mod_list = new List<EmpAss>();
            if (date == "undefined" || date == "")
            {
                date = null;
            }
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_entry_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", "S"),
                                new SqlParameter("@date",date),
                                 
                                
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpAss simsobj = new EmpAss();
                            simsobj.em_applicant_id = dr["em_applicant_id"].ToString();
                            simsobj.em_sr_no = dr["em_sr_no"].ToString();
                            simsobj.emplyee_name = dr["emplyee_name"].ToString();
                            simsobj.em_application_date = dr["em_application_date"].ToString();
                            simsobj.em_interview_date = dr["em_interview_date"].ToString();
                            simsobj.em_interview_remark = dr["em_interview_remark"].ToString();
                            simsobj.em_doc_path = dr["em_doc_path"].ToString();

                            try
                            {
                                simsobj.em_employee_id = dr["em_employee_id"].ToString();
                                simsobj.interviwer_name = dr["interviwer_name"].ToString();
                            }
                            catch (Exception ex)
                            {

                               
                            }
                            
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("CURDassesmentEmployeeEntry")]
        public HttpResponseMessage CURDassesmentEmployeeEntry(List<EmpAss> data)
        {
            bool alert_status = false;
            //  studAss attObj = Newtonsoft.Json.JsonConvert.DeserializeObject<reportCardAttend>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (EmpAss obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_entry_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", 'U'),
                                 
                                new SqlParameter("@em_interview_remark", obj.em_interview_remark),
                                new SqlParameter("@em_applicant_id", obj.em_applicant_id)
                                
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, alert_status);
                }
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

        [Route("CUD_Update_Employee_Doc")]
        public HttpResponseMessage CUD_Update_Admission_Doc(EmpAss simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Update_Admission_Doc()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Update_Admission_Doc"));
            bool updated = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_employee_assesment_entry_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@em_applicant_id",simsobj.em_applicant_id),
                            new SqlParameter("@em_doc_path",simsobj.em_doc_path),
                            //new SqlParameter("@sims_admission_doc_criteria_code",simsobj.sims_criteria_code),
                            

                         });
                        if (ins > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("CUD_Delete_Employee_Doc")]
        public HttpResponseMessage CUD_Delete_Employee_Doc(string em_app_id, string doc_path)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Delete_Employee_Doc(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Delete_Employee_Doc"));
            // bool deleted = false;
            Message message = new Message();
            try
            {
                // if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_assesment_entry_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","E"),
                            new SqlParameter("@em_applicant_id", em_app_id),
                             
                            new SqlParameter("@em_doc_path", doc_path),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Document Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            dr.Close();
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        #endregion
    }
}