﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Helper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{
     [RoutePrefix("api/AdmissionFee")]
    public class AdmissionFeeController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        //Select
        [Route("getAllAdmissionFee")]
        public HttpResponseMessage getAllAdmissionFee()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAdmissionFee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllAdmissionFee"));

            List<Sims563> goaltarget_list = new List<Sims563>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "S"),
                           new SqlParameter("@opr_upd", "SAF"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims563 simsobj = new Sims563();
                            simsobj.sr_no = Convert.ToInt32(dr["sr_no"].ToString());
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_name = dr["sims_fee_name"].ToString();
                            simsobj.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            simsobj.sims_rev_acc_no = dr["sims_rev_acc_no"].ToString();
                            simsobj.sims_rev_acc_name = dr["sims_rev_acc_name"].ToString();
                            simsobj.sims_cash_acc_no = dr["sims_cash_acc_no"].ToString();
                            simsobj.sims_cash_acc_name = dr["sims_cash_acc_name"].ToString();

                            simsobj.disable_status = dr["dis_status"].ToString()=="1" ? true : false;
                            string sims_fee_status = string.Empty;
                            sims_fee_status = dr["sims_fee_status"].ToString();
                            if (sims_fee_status == "A")
                                simsobj.sims_fee_status = true;
                            else
                                simsobj.sims_fee_status = false;
                            simsobj.sims_adjustable_fee_type = dr["sims_adjustable_fee_type"].ToString();
                            simsobj.sims_adjustable_fee_type_name = dr["sims_adjustable_fee_type_name"].ToString();

                            simsobj.is_admission_or_prospect_fee = dr["is_admission_or_prospect_fee"].ToString();
                            string sims_is_fee_adjustable = string.Empty;
                            sims_is_fee_adjustable = dr["sims_is_fee_adjustable"].ToString();
                            if (sims_is_fee_adjustable == "A")
                                simsobj.sims_is_fee_adjustable = true;
                            else
                                simsobj.sims_is_fee_adjustable = false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getAllFeeType")]
        public HttpResponseMessage getAllFeeType()
        {
            List<Sims563> goaltarget_list = new List<Sims563>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                           new SqlParameter("@opr_upd", "FC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims563 simsobj = new Sims563();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_name = dr["sims_fee_code_description"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("FeeCount")]
        public HttpResponseMessage FeeCount(Sims563 data)
        {
            int feecount = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_proc]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", "S"),
                           new SqlParameter("@opr_upd", "CCF"),
                            new SqlParameter("@sims_cur_code",data.sims_cur_code ),
                           new SqlParameter("@sims_academic_year", data.sims_academic_year),
                            new SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new SqlParameter("@sims_fee_code", data.sims_fee_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            feecount =Convert.ToInt32( dr["FeeCount"].ToString());                            
                           
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, feecount);
        }

        [Route("CUDAdmissionFee")]
        public HttpResponseMessage CUDAdmissionFee(List<Sims563> data)
        {
            bool insert = false;
             string status,is_fee_adjustable;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims563 simsobj in data)
                    {
                         if (simsobj.sims_fee_status == true)
                            status = "A";
                        else
                            status = "I";
                         if (simsobj.sims_is_fee_adjustable == true)
                            is_fee_adjustable = "A";
                        else
                            is_fee_adjustable = "I";

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_admission_fee_proc]",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sr_no", simsobj.sr_no),
                            new SqlParameter("@opr_upd", simsobj.opr_upd),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                            new SqlParameter("@sims_fee_amount", simsobj.sims_fee_amount),
                            new SqlParameter("@sims_rev_acc_no", simsobj.sims_rev_acc_no),
                            new SqlParameter("@sims_cash_acc_no", simsobj.sims_cash_acc_no),
                            new SqlParameter("@sims_adjustable_fee_type", simsobj.sims_adjustable_fee_type),
                            new SqlParameter("@sims_fee_status",status),
                            new SqlParameter("@sims_is_fee_adjustable", is_fee_adjustable),
                            new SqlParameter("@is_admission_or_prospect_fee", simsobj.is_admission_or_prospect_fee),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getAllAccountName")]
        public HttpResponseMessage getAllAccountName()
        {
            List<Sims563> goaltarget_list = new List<Sims563>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_fee_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                           new SqlParameter("@opr_upd", "AC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims563 simsobj = new Sims563();
                            simsobj.acc_name = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString() + "_" + dr["glma_acct_name"].ToString();
                            simsobj.acc_code = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }
    }
}