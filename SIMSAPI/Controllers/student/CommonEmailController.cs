﻿using SIMSAPI.Helper;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.ParentClass;
using System.Threading;
using System.Net.Mail;
using System.Text;
using System.IO;

namespace SIMSAPI.Controllers.student
{
    //mail sending
    [RoutePrefix("api/common/Email")]

    public class CommonEmailController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string sims_smtp_address;
        private string sims_smtp_port;
        private string sims_smtp_password;
        private string sims_sslReq;
        private string sims_senderid;

        [Route("GetcheckEmailProfile")]
        public HttpResponseMessage GetcheckEmailProfile(string sr_no)
        {
            Comn_email em = new Comn_email();
            List<Comn_email> lstEmails = new List<Comn_email>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Insert_Sims_Email_Schedule_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'W'),
                            new SqlParameter("@sims_msg_sr_no",sr_no)
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            em.username = dr["sims_smtp_username"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, em.username);
            }
            return Request.CreateResponse(HttpStatusCode.OK, em.username);
        }


        [Route("ScheduleMails")]
        public HttpResponseMessage ScheduleMails(string filenames, List<Comn_email> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool flag = true;

            List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            //List<comn_email_attachments> data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(emailList[1].ToString());
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;
                    bool inserted = false;
                    string attachments = string.Empty;
                    foreach (comn_email_attachments att in data)
                    {
                        attachments = attachments + ',' + att.attFilename;
                    }
                    if (!string.IsNullOrEmpty(attachments))
                    {
                        attachments = attachments.Substring(1);
                    }


                    foreach (Comn_email email in emailList)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                        });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            msg.strMessage = st;
                        }

                        else
                        {
                            st = "Not Inserted";
                            msg.strMessage = st;
                        }
                        //  GetScheduledMails();
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        private int GetScheduledMails()
        {
            int cnt = 0;
            List<Sims010_Auth> email_list = new List<Sims010_Auth>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'S'),
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            cnt = int.Parse(dr[0].ToString());
                        }
                    }
                    if (cnt > 0)
                    {
                        GetSpecificScheduledMails();
                    }

                    return cnt;
                }
            }
            catch (Exception x)
            {
                return cnt;
            }
        }

        public void GetSpecificScheduledMails()
        {
            List<Comn_email> lstEmails = new List<Comn_email>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'A'),
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            Comn_email em = new Comn_email();
                            em.emaildate = dr["sims_email_date"].ToString();
                            em.username = dr["sims_email_user_code"].ToString();
                            if (!string.IsNullOrEmpty(em.username))
                                em.emp_emailid = dr["sims_recepient_id"].ToString();
                            em.emailnumber = dr["sims_email_number"].ToString();
                            em.emailsendto = dr["sims_recepient_id"].ToString();
                            em.sender_emailid = dr["sims_sender_id"].ToString();
                            em.sims_email_attachment = dr["sims_email_attachment"].ToString();
                            em.sims_email_message = dr["sims_email_message"].ToString();
                            em.sims_email_recurrance_id = dr["sims_email_recurrance_id"].ToString();
                            em.sims_email_schedule_date = dr["sims_email_schedule_date"].ToString();
                            em.sims_email_subject = dr["sims_email_subject"].ToString();
                            em.sims_recepient_cc_id = dr["sims_recepient_cc_id"].ToString();
                            em.sims_recepient_id = dr["sims_recepient_id"].ToString();
                            em.sims_recepient_search_id = dr["sims_recepient_id"].ToString();
                            em.status = dr["sims_email_status"].ToString();
                            em.subject = dr["sims_email_subject"].ToString();
                            em.body = dr["sims_email_message"].ToString();
                            em.sims_email_attachment = dr["sims_email_attachment"].ToString();
                            List<comn_email_attachments> lst_stratt = new List<comn_email_attachments>();
                            if (!string.IsNullOrEmpty(em.sims_email_attachment))
                            {
                                string[] stringSeparators = new string[] { "," };
                                var result = em.sims_email_attachment.Split(stringSeparators, StringSplitOptions.None);

                                foreach (string Str in result)
                                {
                                    comn_email_attachments eatt = new comn_email_attachments();
                                    eatt.attFilename = Str;
                                    eatt.attFilePath = "Docs\\Attachments\\";
                                    lst_stratt.Add(eatt);
                                }
                            }
                            em.comn_email_attachments = lst_stratt;
                            lstEmails.Add(em);
                        }
                    }
                    GetSenderEID();
                    UpdateProc(lstEmails);
                    SendScheduledEmail(lstEmails);
                    GetScheduledMails();
                }
            }
            catch (Exception x)
            {

            }
        }

        private void GetSenderEID()
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                    new List<SqlParameter>() 
                    { 
                        new SqlParameter("@opr", 'G'),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_smtp_address = dr["sims_smtp_address"].ToString();
                            sims_smtp_port = dr["sims_smtp_port"].ToString();
                            sims_smtp_password = dr["sims_smtp_password"].ToString();
                            sims_sslReq = dr["sims_smtp_ssl_required"].ToString();
                            sims_senderid = dr["sims_smtp_from_email"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private void UpdateProc(List<Comn_email> emailList)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Comn_email email in emailList)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                        new List<SqlParameter>() 
                        { 
                        new SqlParameter("@opr", 'C'),
                        new SqlParameter("@senderid", sims_senderid),
                        new SqlParameter("@email_number", email.emailnumber),
                        new SqlParameter("@updCnt", emailList.Count),
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);

            }
        }

        private int SendScheduledEmail(List<Comn_email> emailList)
        {
            int result = 0;
            string emailStatus = string.Empty;
            Comn_email_smtp comn_smtp = new Comn_email_smtp();

            try
            {
                #region SEND EMAIL CODE

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.IsBodyHtml = true;
                mail.BodyEncoding = Encoding.UTF8;
                mail.SubjectEncoding = Encoding.UTF8;
                List<comn_email_attachments> lstAtt = new List<comn_email_attachments>();
                foreach (Comn_email obj in emailList)
                {
                    obj.sender_emailid = sims_senderid;
                    if (obj.sender_emailid != "")
                    {
                        // mail.Bcc.Add(obj.emailsendto);
                        mail.To.Add(obj.emailsendto);
                        mail.Subject = obj.subject;
                        mail.Body = obj.body;

                        string st = AppDomain.CurrentDomain.BaseDirectory;

                        foreach (comn_email_attachments a_str in obj.comn_email_attachments)
                        {
                            string str = st + "Docs\\Attachments\\" + a_str.attFilename;
                            if (File.Exists(str))
                            {
                                Attachment att = new Attachment(str);
                                mail.Attachments.Add(att);
                            }
                        }

                    }
                }

                //Get School Name
                string Schoolname = "Mograsys Support";
                Schoolname = GetSchoolName();
                if (string.IsNullOrEmpty(Schoolname))
                    Schoolname = "Mograsys Support";

                mail.From = new MailAddress(sims_senderid, Schoolname, System.Text.Encoding.UTF8);
                //"This is a test mail for Mail Integration Service of Mograsys.Thank You.";
                mail.Priority = MailPriority.High;


                SmtpClient client = new SmtpClient();
                // client.UseDefaultCredentials = false;
                // client.Credentials = new System.Net.NetworkCredential(sims_senderid.ToString(), sims_smtp_password, "login.microsoftonline.com");
                client.Credentials = new System.Net.NetworkCredential(sims_senderid.ToString(), sims_smtp_password);
                client.Port = Convert.ToInt32(sims_smtp_port);//587;
                client.Host = sims_smtp_address;//"smtp.gmail.com";
                if (client.Host == "smtp.office365.com")
                    client.TargetName = "STARTTLS/smtp.office365.com";
                if (!string.IsNullOrEmpty(sims_sslReq))
                {
                    if (sims_sslReq == "Y" || sims_sslReq == "y")
                        client.EnableSsl = true;
                    else if (sims_sslReq == "N" || sims_sslReq == "n")
                        client.EnableSsl = false;
                    else
                        client.EnableSsl = true;
                }
                client.Timeout = 300000;

                try
                {
                    client.Send(mail);
                    //InsertEmailTransaction(emailList);
                    Thread.Sleep(500);
                    emailStatus = "S";
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;

                    string errorMessage = string.Empty;
                    while (ex2 != null)
                    {
                        errorMessage += ex2.ToString();
                        ex2 = ex2.InnerException;
                    }
                    emailStatus = "F";
                    mail.Dispose();
                    client.Dispose();
                }

                mail.Dispose();
                client.Dispose();

                #endregion
            }
            catch (Exception)
            {
            }

            result = 0;
            return result;
        }

        private string GetSchoolName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDetails(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "MAIL", "GetSchoolName"));
            string name = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_send_mail]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'M')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            name = dr["lic_school_name"].ToString();
                        }
                    }

                    return name;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return name;
            }
        }

        [Route("ScheduleMails_Att")]
        public HttpResponseMessage ScheduleMails_Att(string filenames, List<Comn_email> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool flag = true;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;
                    bool inserted = false;

                    foreach (Comn_email email in emailList)
                    {
                        string attachments = string.Empty;
                        foreach (comn_email_attachments att in email.comn_email_attachments)
                        {
                            attachments = attachments + ',' + att.attFilename;
                        }
                        if (!string.IsNullOrEmpty(attachments))
                        {
                            attachments = attachments.Substring(1);
                        }

                        SqlDataReader dr1 = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now), 
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                        });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            msg.strMessage = st;
                        }

                        else
                        {
                            st = "Not Inserted";
                            msg.strMessage = st;
                        }
                        //  GetScheduledMails();
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        [Route("ScheduleMails_dpsd")]
        public HttpResponseMessage ScheduleMails_dpsd(string filenames, List<Comn_email> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool flag = true;

            List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            //List<comn_email_attachments> data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(emailList[1].ToString());
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;
                    bool inserted = false;
                    string attachments = string.Empty;
                    //foreach (comn_email_attachments att in data)
                    //{
                    //    attachments = attachments + ',' + att.attFilename;
                    //}
                    //if (!string.IsNullOrEmpty(attachments))
                    //{
                    //    attachments = attachments.Substring(1);
                    //}


                    foreach (Comn_email email in emailList)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                        });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            msg.strMessage = st;
                        }

                        else
                        {
                            st = "Not Inserted";
                            msg.strMessage = st;
                        }
                        //  GetScheduledMails();
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }


        [Route("ScheduleMails_prospect")]
        public HttpResponseMessage ScheduleMails_prospect(string filenames, List<Comn_email> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool flag = true;

            List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            //List<comn_email_attachments> data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(emailList[1].ToString());
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;
                    bool inserted = false;
                    string attachments = string.Empty;
                    //foreach (comn_email_attachments att in data)
                    //{
                    //    attachments = attachments + ',' + att.attFilename;
                    //}
                    //if (!string.IsNullOrEmpty(attachments))
                    //{
                    //    attachments = attachments.Substring(1);
                    //}


                    foreach (Comn_email email in emailList)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("Insert_Sims_Email_Schedule",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                        });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            msg.strMessage = st;
                        }

                        else
                        {
                            st = "Not Inserted";
                            msg.strMessage = st;
                        }
                        //  GetScheduledMails();
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }
    }
}