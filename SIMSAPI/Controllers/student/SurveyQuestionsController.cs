﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/SurveyQuestionAnswer")]
    public class SurveyQuestionsController : ApiController
    {
        [Route("getSurveyQuestionType")]
        public HttpResponseMessage getSurveyQuestionType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyQuestionType()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims711> mod_list = new List<Sims711>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_questionanswer_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "L"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 simsobj = new Sims711();
                            simsobj.sims_survey_question_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_survey_question_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getSurveyQuestionRateGroup")]
        public HttpResponseMessage getSurveyQuestionRateGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyQuestionRateGroup()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims711> mod_list = new List<Sims711>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_questionanswer_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "R"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 simsobj = new Sims711();
                            simsobj.sims_survey_rating_group_code = dr["sims_survey_rating_group_code"].ToString();
                            simsobj.sims_survey_rating_group_desc_en = dr["sims_survey_rating_group_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getSurveyQuestionRateDetails")]
        public HttpResponseMessage getSurveyQuestionRateDetails(string group_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSurveyQuestionRateDetails()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims711> mod_list = new List<Sims711>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_survey_questionanswer_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "O"),
                            new SqlParameter("@sims_survey_question_rating_group_code", group_code)
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims711 simsobj = new Sims711();
                            simsobj.sims_survey_rating_code = dr["sims_survey_rating_code"].ToString();
                            simsobj.sims_survey_rating_name = dr["sims_survey_rating_name"].ToString();
                            simsobj.sims_survey_rating_desc = dr["sims_survey_rating_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDSurveyQuestionAnswer")]
        public HttpResponseMessage CUDSurveyQuestionAnswer(List<Sims711> data)
        {
            Message message = new Message();
            bool insert = false;
            string count;
            int cnt = 1;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims711 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_survey_questionanswer_proc]",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", 'I'),
                            //new SqlParameter("@sims_survey_question_code",	            simsobj.sims_survey_question_code),	
                            new SqlParameter("@sims_survey_question_desc_en",           simsobj.sims_survey_question_desc_en),
                            new SqlParameter("@sims_survey_question_desc_ot",           simsobj.sims_survey_question_desc_ot),
                            new SqlParameter("@sims_survey_question_type",              simsobj.sims_survey_question_type),
                            new SqlParameter("@sims_survey_question_weightage",         simsobj.sims_survey_question_weightage),
                            new SqlParameter("@sims_survey_question_display_order",     simsobj.sims_survey_question_display_order),
                            new SqlParameter("@sims_survey_question_difficulty_level",  simsobj.sims_survey_question_difficulty_level),
                            new SqlParameter("@sims_survey_question_rating_group_code", simsobj.sims_survey_question_rating_group_code),
                            new SqlParameter("@sims_survey_question_status",            simsobj.sims_survey_question_status),

                           //new SqlParameter("@sims_survey_question_code",                 simsobj.sims_survey_question_code),
                           //new SqlParameter("@sims_survey_question_answer_code",          simsobj.sims_survey_question_answer_code),
                            new SqlParameter("@sims_survey_question_answer_desc_en",       simsobj.sims_survey_question_answer_desc_en), 
                            new SqlParameter("@sims_survey_question_answer_desc_ot",       simsobj.sims_survey_question_answer_desc_ot),
                            new SqlParameter("@sims_survey_question_answer_weightage",     simsobj.sims_survey_question_answer_weightage),
                            new SqlParameter("@sims_survey_question_answer_display_order", simsobj.sims_survey_question_answer_display_order),
                            new SqlParameter("@sims_survey_question_answer_status",        simsobj.sims_survey_question_answer_status),
                            new SqlParameter("@sims_survey_question_correct_answer_flag",  simsobj.sims_survey_question_correct_answer_flag==true?"T":"F"),                            
                            new SqlParameter("@count",cnt)
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                        cnt=cnt+1;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }

            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }            
    }
}