﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.student
{
    [RoutePrefix("api/admissionQuota")]
   
    public class admissionQuotaController : ApiController
    {

        public string section_term_section_code { get; set; }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getAllAdmissionQuota")]
        public HttpResponseMessage getAllAdmissionQuota()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAdmissionQuota(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getAllAdmissionQuota"));

            List<sims_admission_quota_master> admission_list = new List<sims_admission_quota_master>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_quota_p_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_admission_quota_master objNew = new sims_admission_quota_master();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_quota_desc = dr["sims_quota_desc"].ToString();
                            objNew.sims_q_strength = dr["sims_q_strength"].ToString();
                            objNew.sims_q_status = dr["sims_q_status"].ToString();
                            objNew.sims_quota_id = dr["sims_quota_id"].ToString();
                            admission_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, admission_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("addUpdateAdmissionQuota")]
        public HttpResponseMessage addUpdateAdmissionQuota(List<sims_admission_quota_master> data)
        {
            bool flag = false;
            try
            {
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims_admission_quota_master admiObj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_quota_p_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", admiObj.opr),
                                new SqlParameter("@sims_curriculum_code", admiObj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", admiObj.sims_academic_year),
                                new SqlParameter("@sims_quota_desc", admiObj.sims_quota_desc),
                                new SqlParameter("@sims_quota_sterngth", admiObj.sims_q_strength),
                                new SqlParameter("@sims_quota_status",admiObj.sims_q_status.Equals("true")?"A":"I"),
                                new SqlParameter("@sims_quota_id",admiObj.sims_quota_id)
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                        dr.Close();
                      }
                }                    
                return Request.CreateResponse(HttpStatusCode.OK, flag);
                           
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            
        }

        [Route("updateAdmissionQuota")]
        public HttpResponseMessage updateAdmissionQuota(sims_admission_quota_master admiObj)
        {   
            Message message = new Message();

            try
            {
                if (admiObj != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[sims_admission_quota_p_proc]", new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_curriculum_code", admiObj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", admiObj.sims_academic_year),
                                new SqlParameter("@sims_quota_desc", admiObj.sims_quota_desc),
                                new SqlParameter("@sims_quota_sterngth", admiObj.sims_q_strength),
                                new SqlParameter("@sims_quota_status",admiObj.sims_q_status.Equals("true")?"A":"I"),
                                new SqlParameter("@sims_quota_id",admiObj.sims_quota_id)
                        });

                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "Admission quota updated successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Admission quota not updated";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        dr1.Close();
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Data";
                    message.systemMessage = string.Empty;
                    message.messageType = MessageType.Success;
                }
                
            }
            catch (Exception ex)
            {
                message.strMessage = "Server Error";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        
    }
}

       