﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using System.Text;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/CreateOrder")]
    public class CreateOrderController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_RequestDetails_forOrder")]
        public HttpResponseMessage Get_RequestDetails_forOrder(string dep_code, string req_type)
        {
            //string input = "حصلون على الموعد و المكان عبر رسال";
            //StringBuilder s = new StringBuilder();
            //foreach (char chr in input)
            //{
            //    s.Append(Convert.ToString(chr, 16).PadLeft(4, '0'));
            //}

            //string ss = s.ToString();

            List<Invs044_request> group_list = new List<Invs044_request>();
            List<Invs044_requestdetails> reqdetails = new List<Invs044_requestdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@dep_code", dep_code),
                                new SqlParameter("@req_si_type", req_type)
                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        Invs044_request invsobj = new Invs044_request();
                        invsobj.invs058_req_no = dr["req_no"].ToString();
                        invsobj.invs058_req_type = dr["req_type_name"].ToString();
                        invsobj.invs058_dep_code = dr["dep_code"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        invsobj.invs058_dep_name = dr["dep_name"].ToString();
                        invsobj.invs058_req_status = dr["req_status_name"].ToString();
                       // invsobj.invs058_req_date = DateTime.Parse(dr["req_date"].ToString()).ToShortDateString();
                        invsobj.invs058_req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                        invsobj.invs058_dm_code = dr["dm_code"].ToString();
                        invsobj.invs058_dm_name = dr["dm_name"].ToString();
                        invsobj.invs058_sm_name = dr["sm_name"].ToString();
                        invsobj.invs058_sm_code = dr["sm_code"].ToString();
                        if (string.IsNullOrEmpty(dr["req_discount_pct"].ToString()))
                            invsobj.invs058_req_discount_pct = dr["req_discount_pct"].ToString();
                        invsobj.invs058_req_requester_dep = dr["req_requester_dep"].ToString();
                        invsobj.invs058_req_remarks = dr["req_remarks"].ToString();
                        bool agency_flag = true;
                        if (dr["req_agency_flag"].ToString() == "Y")
                            agency_flag = true;
                        else
                            agency_flag = false;
                        invsobj.updatedR = false;
                        invsobj.invs058_req_agency_flag = agency_flag;
                        reqdetails = new List<Invs044_requestdetails>();
                        reqdetails = Get_RDetails_forOrder(invsobj.invs058_req_no, req_type);

                        invsobj.req_details = reqdetails;

                        if (reqdetails.Count > 0)
                            group_list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, group_list);

        }

        public List<Invs044_requestdetails> Get_RDetails_forOrder(string reqno, string req_type)
        {
            List<Invs044_requestdetails> group_list = new List<Invs044_requestdetails>();
            try
            {
                string opr = "";
                if (req_type == "I")
                    opr = "E";
                if (req_type == "S")
                    opr = "A5";

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", opr),
                new SqlParameter("@req_no", reqno),

                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        Invs044_requestdetails obj = new Invs044_requestdetails();
                        obj.invs058_req_no = dr["req_no"].ToString();
                        // invsobj. = dr["req_type"].ToString();
                        obj.invs058_rd_line_no = dr["rd_line_no"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        //obj.invs058_dep_name = Dep_Name.ToString();
                        obj.invs058_im_inv_no = dr["im_inv_no"].ToString();
                        obj.invs058_sup_code = dr["sup_code"].ToString();
                        obj.invs058_uom_code = dr["uom_code"].ToString();
                        obj.invs058_rd_quantity = dr["rd_approved_quantity"].ToString();
                        obj.invs058_rd_estimated_price = dr["rd_estimated_price"].ToString();
                        obj.invs058_cur_code = dr["cur_code"].ToString();
                        obj.invs058_rd_discount_pct = dr["rd_discount_pct"].ToString();
                        if (!string.IsNullOrEmpty(dr["rd_date_required"].ToString()))
                            obj.invs058_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());    //DateTime.Parse(dr["rd_date_required"].ToString());
                        else
                            obj.invs058_date_required = null;
                        obj.invs058_co_no = dr["co_no"].ToString();
                        // obj.invs058_rd_status = Convert.ToBoolean(dr["rd_status"].ToString());
                        obj.invs058_rd_remarks = dr["rd_remarks"].ToString();
                        obj.invs058_rd_item_desc = dr["rd_item_desc"].ToString();
                        //obj.invs058_dep_name = dr["dep_name"].ToString();
                        obj.sup_name = dr["sup_name"].ToString();
                        obj.im_item_code = dr["im_item_code"].ToString();
                        //obj.id_cur_qty = dr["id_cur_qty"].ToString();
                        obj.updatedRD = false;
                        obj.new_quantity = dr["rd_quantity"].ToString();
                        obj.invs058_req_date = string.Empty;
                        obj.invs058_req_type = string.Empty;
                        obj.invs058_req_remarks = string.Empty;
                        obj.invs058_dm_name = string.Empty;
                        obj.total = 0;
                        obj.chk = false;
                        if (req_type == "S")
                        {
                            obj.servicevalue = false;
                        }
                        else
                        {
                            obj.servicevalue = true;
                        }
                        // obj.lst_suppliers = GetItemSuppliers(obj.invs058_im_inv_no);
                        group_list.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return group_list;
        }

        //GetDepartment
        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'K'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("Get_Order_report")]
        public HttpResponseMessage Get_Order_report()
        {
            string report_name = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "RT")
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        report_name = dr["report_name"].ToString();
                        return Request.CreateResponse(HttpStatusCode.OK, report_name);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, report_name);
            }
            return Request.CreateResponse(HttpStatusCode.OK, report_name);
        }


        //GetItemSuppliers

        private List<invs_suppliers> GetItemSuppliers(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetItemSuppliers()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<invs_suppliers> doc_list = new List<invs_suppliers>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A2"),
                            new SqlParameter("@im_inv_no", im_inv_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs_suppliers simsobj = new invs_suppliers();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["supname"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return doc_list;
                    }
                    else
                        return doc_list;
                }
            }
            catch (Exception x)
            {
                Log.Error(x);

                return doc_list;
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetSupplierDepartment
        [Route("GetSupplierDepartment")]
        public HttpResponseMessage GetSupplierDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSupplierDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetDeliveryMOde
        [Route("GetDeliveryMOde")]
        public HttpResponseMessage GetDeliveryMOde()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDeliveryMOde()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'M'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //Getpaymentmodes
        [Route("Getpaymentmodes")]
        public HttpResponseMessage Getpaymentmodes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getpaymentmodes()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'N'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.pm_code = dr["pm_code"].ToString();
                            simsobj.pm_desc = dr["pm_desc"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetTradeterms
        [Route("GetTradeterms")]
        public HttpResponseMessage GetTradeterms()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTradeterms()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'T'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.trt_code = dr["trt_code"].ToString();
                            simsobj.trt_desc = dr["trt_desc"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetLetterOfCredit
        [Route("GetLetterOfCredit")]
        public HttpResponseMessage GetLetterOfCredit()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLetterOfCredit()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Q'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.lc_no = dr["lc_no"].ToString();
                            simsobj.bk_code = dr["bk_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetForwardAgent
        [Route("GetForwardAgent")]
        public HttpResponseMessage GetForwardAgent()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetForwardAgent()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.fa_code = dr["fa_code"].ToString();
                            simsobj.fa_name = dr["fa_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetCurrencyMaster
        [Route("GetCurrencyMaster")]
        public HttpResponseMessage GetCurrencyMaster()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCurrencyMaster()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDOrderDetails")]
        public HttpResponseMessage CUDRequestDetails(string data1, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = null;
            Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(obj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", obj.order_dep_code),
                                      new SqlParameter("@order_sup_code", obj.order_sup_code),
                                      new SqlParameter("@order_cur_code", obj.order_cur_code),
                                      new SqlParameter("@order_fa_code", obj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", obj.order_dm_code),
                                      new SqlParameter("@order_trt_code", obj.order_trt_code),
                                      new SqlParameter("@order_pm_code", obj.order_pm_code),
                                      new SqlParameter("@order_lc_no", obj.order_lc_no),
                                      new SqlParameter("@order_ord_supl_quot_ref",obj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(obj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),
                                    
                                 });
                    if (dr.Read())
                    {
                        order_no = dr[0].ToString();
                    }
                    dr.Close();

                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@order_d_ord_no",order_no),
                                new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                new SqlParameter("@order_d_od_status",0),
                                new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                new SqlParameter("@order_d_od_shipped_qty",0),
                                new SqlParameter("@order_d_od_received_qty",0),
                                new SqlParameter("@order_d_od_orig_line_no",0),
                         });

                        if (dr.Read())
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("SaveOrderDetails")]
        public HttpResponseMessage SaveOrderDetails(string data1, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A3"),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(obj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", obj.order_dep_code),
                                      new SqlParameter("@order_sup_code", obj.order_sup_code),
                                      new SqlParameter("@order_cur_code", obj.order_cur_code),
                                      new SqlParameter("@order_fa_code", obj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", obj.order_dm_code),
                                      new SqlParameter("@order_trt_code", obj.order_trt_code),
                                      new SqlParameter("@order_pm_code", obj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",obj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(obj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),                        
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",obj.invs058_req_type),
                                      new SqlParameter("@randomno",obj.randomnumber),    
             
                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),                 
                                      new SqlParameter("@order_createdby",obj.invs058_order_createdby),                 
                                      new SqlParameter("@order_heading",obj.invs058_heading),                 
                                      new SqlParameter("@order_notes",obj.invs058_notes),                 
                                      new SqlParameter("@order_apply_aexp",obj.invs058_order_apply_aexp),       
          
                                      new SqlParameter("@ord_addexps",obj.invs058_ord_addexps),       
                                      new SqlParameter("@ord_dispct",obj.invs058_ord_dispct),       
                                      new SqlParameter("@ord_disamt",obj.invs058_ord_disamt),       
                                      new SqlParameter("@od_total_discount",obj.invs058_od_total_discount),       
                                      new SqlParameter("@od_total_expense",obj.invs058_od_total_expense),       
                                      new SqlParameter("@ord_vehicleno",obj.sims_transport_vehicle_code)
                                    
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A9"),                 
                                      new SqlParameter("@randomno",obj.randomnumber),                 
                                 });

                        if (dr.Read())
                        {
                            inserted = true;
                            OrderNumber = dr["OrderNumbers"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("SaveOrderDetails_VAT")]
        public HttpResponseMessage SaveOrderDetails_VAT(string data1, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","A3"),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(obj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", obj.order_dep_code),
                                      new SqlParameter("@order_sup_code", obj.order_sup_code),
                                      new SqlParameter("@order_cur_code", obj.order_cur_code),
                                      new SqlParameter("@order_fa_code", obj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", obj.order_dm_code),
                                      new SqlParameter("@order_trt_code", obj.order_trt_code),
                                      new SqlParameter("@order_pm_code", obj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",obj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(obj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",obj.invs058_req_type),
                                      new SqlParameter("@randomno",obj.randomnumber),

                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),
                                      new SqlParameter("@order_createdby",obj.invs058_order_createdby),
                                      new SqlParameter("@order_heading",obj.invs058_heading),
                                      new SqlParameter("@order_notes",obj.invs058_notes),
                                      new SqlParameter("@order_apply_aexp",obj.invs058_order_apply_aexp),

                                      new SqlParameter("@ord_addexps",obj.invs058_ord_addexps),
                                      new SqlParameter("@ord_dispct",obj.invs058_ord_dispct),
                                      new SqlParameter("@ord_disamt",obj.invs058_ord_disamt),
                                      new SqlParameter("@od_total_discount",obj.invs058_od_total_discount),
                                      new SqlParameter("@od_total_expense",obj.invs058_od_total_expense),
                                      new SqlParameter("@ord_vehicleno",obj.sims_transport_vehicle_code),
                                      new SqlParameter("@ord_vat_total_amount",obj.ord_vat_total_amount),
                                      new SqlParameter("@od_item_vat_per",simsobj.od_item_vat_per),
                                      new SqlParameter("@od_item_vat_amount",simsobj.od_item_vat_amount)
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","A9"),
                                      new SqlParameter("@randomno",obj.randomnumber),
                                 });

                        if (dr.Read())
                        {
                            inserted = true;
                            OrderNumber = dr["OrderNumbers"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("SaveOrderDetails1")]
        public HttpResponseMessage SaveOrderDetails1(List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            string randomnumber = string.Empty;
            //Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    //foreach (Invs044_requestdetails obj in data1)
                    //{
                    //    randomnumber = obj.randomnumber;
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        randomnumber = simsobj.randomnumber;
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A3"),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(simsobj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", simsobj.order_dep_code),
                                      new SqlParameter("@order_sup_code", simsobj.order_sup_code),
                                      new SqlParameter("@order_cur_code", simsobj.order_cur_code),
                                      new SqlParameter("@order_fa_code", simsobj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", simsobj.order_dm_code),
                                      new SqlParameter("@order_trt_code", simsobj.order_trt_code),
                                      new SqlParameter("@order_pm_code", simsobj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",simsobj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(simsobj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),                        
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",simsobj.invs058_req_type),
                                      new SqlParameter("@randomno",simsobj.randomnumber),    
             
                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),                 
                                      new SqlParameter("@order_createdby",simsobj.invs058_order_createdby),                 
                                      new SqlParameter("@order_heading",simsobj.invs058_heading),                 
                                      new SqlParameter("@order_notes",simsobj.invs058_notes),                 
                                      new SqlParameter("@order_apply_aexp",simsobj.invs058_order_apply_aexp),       
          
                                      new SqlParameter("@ord_addexps",simsobj.invs058_ord_addexps),       
                                      new SqlParameter("@ord_dispct",simsobj.invs058_ord_dispct),       
                                      new SqlParameter("@ord_disamt",simsobj.invs058_ord_disamt),       
                                      new SqlParameter("@od_total_discount",simsobj.invs058_od_total_discount),       
                                      new SqlParameter("@od_total_expense",simsobj.invs058_od_total_expense),       
                                      new SqlParameter("@ord_vehicleno",simsobj.sims_transport_vehicle_code),      
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    // }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A9"),                 
                                      new SqlParameter("@randomno",randomnumber),                 
                                 });

                        if (dr.Read())
                        {
                            inserted = true;
                            OrderNumber = dr["OrderNumbers"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("SaveOrderDetails1_VAT")]
        public HttpResponseMessage SaveOrderDetails1_VAT(List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            string randomnumber = string.Empty;
            //Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    //foreach (Invs044_requestdetails obj in data1)
                    //{
                    //    randomnumber = obj.randomnumber;
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        randomnumber = simsobj.randomnumber;
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","A3"),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(simsobj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", simsobj.order_dep_code),
                                      new SqlParameter("@order_sup_code", simsobj.order_sup_code),
                                      new SqlParameter("@order_cur_code", simsobj.order_cur_code),
                                      new SqlParameter("@order_fa_code", simsobj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", simsobj.order_dm_code),
                                      new SqlParameter("@order_trt_code", simsobj.order_trt_code),
                                      new SqlParameter("@order_pm_code", simsobj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",simsobj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(simsobj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",simsobj.invs058_req_type),
                                      new SqlParameter("@randomno",simsobj.randomnumber),

                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),
                                      new SqlParameter("@order_createdby",simsobj.invs058_order_createdby),
                                      new SqlParameter("@order_heading",simsobj.invs058_heading),
                                      new SqlParameter("@order_notes",simsobj.invs058_notes),
                                      new SqlParameter("@order_apply_aexp",simsobj.invs058_order_apply_aexp),

                                      new SqlParameter("@ord_addexps",simsobj.invs058_ord_addexps),
                                      new SqlParameter("@ord_dispct",simsobj.invs058_ord_dispct),
                                      new SqlParameter("@ord_disamt",simsobj.invs058_ord_disamt),
                                      new SqlParameter("@od_total_discount",simsobj.invs058_od_total_discount),
                                      new SqlParameter("@od_total_expense",simsobj.invs058_od_total_expense),
                                      new SqlParameter("@ord_vehicleno",simsobj.sims_transport_vehicle_code),
                                      new SqlParameter("@ord_vat_total_amount",simsobj.ord_vat_total_amount),
                                      new SqlParameter("@od_item_vat_per",simsobj.od_item_vat_per),
                                      new SqlParameter("@od_item_vat_amount",simsobj.od_item_vat_amount)
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    // }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","A9"),
                                      new SqlParameter("@randomno",randomnumber),
                                 });

                        if (dr.Read())
                        {
                            inserted = true;
                            OrderNumber = dr["OrderNumbers"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        //GetServiceTypes
        [Route("GetServiceTypes")]
        public HttpResponseMessage GetServiceTypes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetServiceTypes()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A1"),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.servicetype = dr["invs_appl_form_field_value1"].ToString();
                            simsobj.servicevalue = dr["invs_appl_parameter"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        ///////////////////////////////////////////////////////////

        [Route("getDeparments_approve")]
        public HttpResponseMessage getDeparments_approve()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeparments_approve()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeparments_approve"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dept_code = dr["dep_code"].ToString();
                            simsobj.dept_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getRequestTypeNew")]
        public HttpResponseMessage getRequestTypeNew()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Y'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_mode_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_mode_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getOrderDetailsforapprove")]
        public HttpResponseMessage getOrderDetailsforapprove(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Invs044_requestdetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                             {
                                //new SqlParameter("@opr", "A6"),
                                new SqlParameter("@opr", "B3"),
                                new SqlParameter("@ord_no", sf.ord_no),
                                new SqlParameter("@dep_code", sf.dep_code),
                                new SqlParameter("@rd_from_date", db.DBYYYYMMDDformat(sf.rd_from_date)),
                                new SqlParameter("@rd_up_date", db.DBYYYYMMDDformat(sf.rd_up_date)),
                                new SqlParameter("@rd_item_desc", sf.rd_item_desc),
                                new SqlParameter("@req_type", sf.request_mode_code),
                                new SqlParameter("@req_no", sf.invs058_req_no),
                                new SqlParameter("@ord_range_no", sf.req_range_no),
                                new SqlParameter("@login_user", sf.loginuser),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("getOrderDetailsforapprove_VAT")]
        public HttpResponseMessage getOrderDetailsforapprove_VAT(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Invs044_requestdetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_Order_proc_new_vat]",
                        new List<SqlParameter>()
                             {
                                //new SqlParameter("@opr", "A6"),
                                new SqlParameter("@opr", "B3"),
                                new SqlParameter("@ord_no", sf.ord_no),
                                new SqlParameter("@dep_code", sf.dep_code),
                                new SqlParameter("@rd_from_date", db.DBYYYYMMDDformat(sf.rd_from_date)),
                                new SqlParameter("@rd_up_date", db.DBYYYYMMDDformat(sf.rd_up_date)),
                                new SqlParameter("@rd_item_desc", sf.rd_item_desc),
                                new SqlParameter("@req_type", sf.request_mode_code),
                                new SqlParameter("@req_no", sf.invs058_req_no),
                                new SqlParameter("@ord_range_no", sf.req_range_no),
                                new SqlParameter("@login_user", sf.loginuser),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("UpdateOrderDetailsforapprove")]
        public HttpResponseMessage UpdateOrderDetailsforapprove(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B4"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateOrderDetailsforapprove_VAT")]
        public HttpResponseMessage UpdateOrderDetailsforapprove_VAT(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B4"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateOrderDetailsforreject")]
        public HttpResponseMessage UpdateOrderDetailsforreject(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B7"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateOrderDetailsforreject_vat")]
        public HttpResponseMessage UpdateOrderDetailsforreject_vat(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B7"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //-----------------------------------------------------------------------------------------------------

        //GetSuppliers
        [Route("GetSuppliers")]
        public HttpResponseMessage GetSuppliers()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetForwardAgent()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A8"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //--------------------------NEW Order Form--------------------------------------------------

        [Route("Get_ALLRequestDetails")]
        public HttpResponseMessage Get_ALLRequestDetails(string dep_code, string req_type, string srch_supcode, string srch_itemdesc, string srch_itemcode, string srch_req_no,string reject_variable,string reject_order_no)
        {
            List<Invs044_requestdetails> group_list = new List<Invs044_requestdetails>();
            List<Invs044_requestdetails> reqdetails = new List<Invs044_requestdetails>();
            try
            {
                if (reject_variable == "undefined" || reject_variable == "" || reject_variable == null)
                    reject_variable = null;
                if (reject_order_no == "undefined" || reject_order_no == "" || reject_order_no == null)
                    reject_order_no = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@dep_code", dep_code),
                                new SqlParameter("@req_si_type", req_type),
                                new SqlParameter("@srch_supcode", srch_supcode),
                                new SqlParameter("@srch_itemdesc", srch_itemdesc),
                                new SqlParameter("@srch_itemcode", srch_itemcode),
                                new SqlParameter("@srch_reqno", srch_req_no),
                                new SqlParameter("@reject_variable", reject_variable),
                                new SqlParameter("@reject_order_no", reject_order_no)
                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        Invs044_requestdetails invsobj = new Invs044_requestdetails();
                        invsobj.invs058_req_no = dr["req_no"].ToString();
                        invsobj.invs058_req_type = dr["req_type_name"].ToString();
                        invsobj.invs058_dep_code = dr["dep_code"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        invsobj.invs058_dep_name = dr["dep_name"].ToString();
                        //invsobj.invs058_req_status = dr["req_status_name"].ToString();
                        invsobj.invs058_req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                        //invsobj.invs058_dm_code = dr["dm_code"].ToString();
                        invsobj.invs058_dm_name = dr["dm_name"].ToString();
                        invsobj.invs058_sm_name = dr["sm_name"].ToString();
                        invsobj.invs058_sm_code = dr["sm_code"].ToString();
                        if (string.IsNullOrEmpty(dr["req_discount_pct"].ToString()))
                            invsobj.invs058_req_discount_pct = dr["req_discount_pct"].ToString();
                        invsobj.invs058_req_requester_dep = dr["req_requester_dep"].ToString();
                        invsobj.invs058_req_remarks = dr["req_remarks"].ToString();
                        bool agency_flag = true;
                        if (dr["req_agency_flag"].ToString() == "Y")
                            agency_flag = true;
                        else
                            agency_flag = false;
                        //invsobj.updatedR = false;
                        invsobj.invs058_req_agency_flag = agency_flag;
                        //reqdetails = new List<Invs044_requestdetails>();
                        //reqdetails = Get_RDetails_forOrder(invsobj.invs058_req_no, req_type);
                        invsobj.invs058_req_no = dr["req_no"].ToString();
                        // invsinvsobj. = dr["req_type"].ToString();
                        invsobj.invs058_rd_line_no = dr["rd_line_no"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        //obj.invs058_dep_name = Dep_Name.ToString();
                        invsobj.invs058_im_inv_no = dr["im_inv_no"].ToString();
                        invsobj.invs058_sup_code = dr["sup_code"].ToString();
                        invsobj.invs058_uom_code = dr["uom_code"].ToString();
                        invsobj.invs058_rd_quantity = dr["rd_approved_quantity"].ToString();
                        invsobj.invs058_rd_estimated_price = dr["rd_estimated_price"].ToString();
                        invsobj.invs058_cur_code = dr["cur_code"].ToString();
                        invsobj.invs058_rd_discount_pct = dr["rd_discount_pct"].ToString();
                        if (!string.IsNullOrEmpty(dr["rd_date_required"].ToString()))
                            invsobj.invs058_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                        else
                            invsobj.invs058_date_required = null;
                        invsobj.invs058_co_no = dr["co_no"].ToString();
                        // invsobj.invs058_rd_status = Convert.ToBoolean(dr["rd_status"].ToString());
                        invsobj.invs058_rd_remarks = dr["rd_remarks"].ToString();
                        invsobj.invs058_rd_item_desc = dr["rd_item_desc"].ToString();
                        //invsobj.invs058_dep_name = dr["dep_name"].ToString();
                        invsobj.sup_name = dr["sup_name"].ToString();
                        invsobj.im_item_code = dr["im_item_code"].ToString();
                        //invsobj.id_cur_qty = dr["id_cur_qty"].ToString();
                        invsobj.updatedRD = false;
                        invsobj.new_quantity = dr["rd_quantity"].ToString();
                        //invsobj.invs058_req_date = string.Empty;
                        //invsobj.invs058_req_type = string.Empty;
                        //invsobj.invs058_req_remarks = string.Empty;
                        //invsobj.invs058_dm_name = string.Empty;
                        invsobj.total = 0;
                        invsobj.chk = false;
                        //if (req_type == "S")
                        //{
                        //    invsobj.servicevalue = false;
                        //}
                        //else
                        //{
                        //    invsobj.servicevalue = true;
                        //}
                        invsobj.servicevalue = true;
                        invsobj.currentqty = dr["currentqty"].ToString();
                        invsobj.orderedqty = dr["orderedqty"].ToString();
                        invsobj.req_imn_no_totqty = dr["tot_qty"].ToString();
                        //invsobj.req_details = reqdetails;

                        invsobj.reqdocs = GetRequestDocs("1", invsobj.invs058_req_no);

                        if (!string.IsNullOrEmpty(invsobj.invs058_rd_line_no))
                            group_list.Add(invsobj);
                        try
                        {
                           invsobj.ord_no = dr["ord_no"].ToString();
                        }
                        catch(Exception x)
                        {

                        }
                        try
                        {
                            invsobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            invsobj.im_sell_price = dr["im_sell_price"].ToString();
                        }
                        catch (Exception x)
                        {

                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, group_list);

        }

        [Route("Get_ALLRequestDetails_VAT")]
        public HttpResponseMessage Get_ALLRequestDetails_VAT(string dep_code, string req_type, string srch_supcode, string srch_itemdesc, string srch_itemcode, string srch_req_no, string reject_variable, string reject_order_no)
        {
            List<Invs044_requestdetails> group_list = new List<Invs044_requestdetails>();
            List<Invs044_requestdetails> reqdetails = new List<Invs044_requestdetails>();
            try
            {
                if (reject_variable == "undefined" || reject_variable == "" || reject_variable == null)
                    reject_variable = null;
                if (reject_order_no == "undefined" || reject_order_no == "" || reject_order_no == null)
                    reject_order_no = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@dep_code", dep_code),
                                new SqlParameter("@req_si_type", req_type),
                                new SqlParameter("@srch_supcode", srch_supcode),
                                new SqlParameter("@srch_itemdesc", srch_itemdesc),
                                new SqlParameter("@srch_itemcode", srch_itemcode),
                                new SqlParameter("@srch_reqno", srch_req_no),
                                new SqlParameter("@reject_variable", reject_variable),
                                new SqlParameter("@reject_order_no", reject_order_no)
                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        Invs044_requestdetails invsobj = new Invs044_requestdetails();
                        invsobj.invs058_req_no = dr["req_no"].ToString();
                        invsobj.invs058_req_type = dr["req_type_name"].ToString();
                        invsobj.invs058_dep_code = dr["dep_code"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        invsobj.invs058_dep_name = dr["dep_name"].ToString();
                        //invsobj.invs058_req_status = dr["req_status_name"].ToString();
                        invsobj.invs058_req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                        //invsobj.invs058_dm_code = dr["dm_code"].ToString();
                        invsobj.invs058_dm_name = dr["dm_name"].ToString();
                        invsobj.invs058_sm_name = dr["sm_name"].ToString();
                        invsobj.invs058_sm_code = dr["sm_code"].ToString();
                        if (string.IsNullOrEmpty(dr["req_discount_pct"].ToString()))
                            invsobj.invs058_req_discount_pct = dr["req_discount_pct"].ToString();
                        invsobj.invs058_req_requester_dep = dr["req_requester_dep"].ToString();
                        invsobj.invs058_req_remarks = dr["req_remarks"].ToString();
                        bool agency_flag = true;
                        if (dr["req_agency_flag"].ToString() == "Y")
                            agency_flag = true;
                        else
                            agency_flag = false;
                        //invsobj.updatedR = false;
                        invsobj.invs058_req_agency_flag = agency_flag;
                        //reqdetails = new List<Invs044_requestdetails>();
                        //reqdetails = Get_RDetails_forOrder(invsobj.invs058_req_no, req_type);
                        invsobj.invs058_req_no = dr["req_no"].ToString();
                        // invsinvsobj. = dr["req_type"].ToString();
                        invsobj.invs058_rd_line_no = dr["rd_line_no"].ToString();
                        //string Dep_Name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                        //obj.invs058_dep_name = Dep_Name.ToString();
                        invsobj.invs058_im_inv_no = dr["im_inv_no"].ToString();
                        invsobj.invs058_sup_code = dr["sup_code"].ToString();
                        invsobj.invs058_uom_code = dr["uom_code"].ToString();
                        invsobj.invs058_rd_quantity = dr["rd_approved_quantity"].ToString();
                        invsobj.invs058_rd_estimated_price = dr["rd_estimated_price"].ToString();
                        invsobj.invs058_cur_code = dr["cur_code"].ToString();
                        invsobj.invs058_rd_discount_pct = dr["rd_discount_pct"].ToString();
                        if (!string.IsNullOrEmpty(dr["rd_date_required"].ToString()))
                            invsobj.invs058_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                        else
                            invsobj.invs058_date_required = null;
                        invsobj.invs058_co_no = dr["co_no"].ToString();
                        // invsobj.invs058_rd_status = Convert.ToBoolean(dr["rd_status"].ToString());
                        invsobj.invs058_rd_remarks = dr["rd_remarks"].ToString();
                        invsobj.invs058_rd_item_desc = dr["rd_item_desc"].ToString();
                        //invsobj.invs058_dep_name = dr["dep_name"].ToString();
                        invsobj.sup_name = dr["sup_name"].ToString();
                        invsobj.im_item_code = dr["im_item_code"].ToString();
                        //invsobj.id_cur_qty = dr["id_cur_qty"].ToString();
                        invsobj.updatedRD = false;
                        invsobj.new_quantity = dr["rd_quantity"].ToString();
                        //invsobj.invs058_req_date = string.Empty;
                        //invsobj.invs058_req_type = string.Empty;
                        //invsobj.invs058_req_remarks = string.Empty;
                        //invsobj.invs058_dm_name = string.Empty;
                        invsobj.total = 0;
                        invsobj.chk = false;
                        //if (req_type == "S")
                        //{
                        //    invsobj.servicevalue = false;
                        //}
                        //else
                        //{
                        //    invsobj.servicevalue = true;
                        //}
                        invsobj.servicevalue = true;
                        invsobj.currentqty = dr["currentqty"].ToString();
                        invsobj.orderedqty = dr["orderedqty"].ToString();
                        invsobj.req_imn_no_totqty = dr["tot_qty"].ToString();
                        //invsobj.req_details = reqdetails;

                        invsobj.reqdocs = GetRequestDocs("1", invsobj.invs058_req_no);

                        if (!string.IsNullOrEmpty(invsobj.invs058_rd_line_no))
                            group_list.Add(invsobj);
                        try
                        {
                            invsobj.ord_no = dr["ord_no"].ToString();
                        }
                        catch (Exception x)
                        {

                        }
                        try
                        {
                            invsobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            invsobj.im_sell_price = dr["im_sell_price"].ToString();
                            invsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            invsobj.im_malc_rate_old = dr["im_malc_rate_old"].ToString();
                        }
                        catch (Exception x)
                        {

                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, group_list);

        }

        [Route("GetOrderAttributes")]
        public HttpResponseMessage GetOrderAttributes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetOrderAttributes()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_request> doc_list = new List<Invs044_request>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B1"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.or_atdesc = dr["dco_desc"].ToString();
                            simsobj.or_atvalue = dr["dco_value"].ToString();
                            simsobj.or_atstatus = dr["dco_status"].ToString();
                            simsobj.or_atamount = "0";
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("COrderDetails")]
        public HttpResponseMessage COrderDetails(string ordno, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = null;
            //Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    foreach (Invs044_requestdetails simsobj in data)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B2"),
                                new SqlParameter("@order_d_ord_no",ordno),                                
                                new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),                               
                                new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),                                
                                new SqlParameter("@order_d_od_status",0),
                                new SqlParameter("@order_d_od_shipped_qty",0),
                                new SqlParameter("@order_d_od_received_qty",0),
                                new SqlParameter("@order_d_od_orig_line_no",0),
                         });
                        if (dr.Read())
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllItemsInSubCategoryNew")]
        public HttpResponseMessage GetAllItemsInSubCategoryNew(string dep_code, string req_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategoryNew(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategoryNew"));

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>() 
                         {                            
                 
                new SqlParameter("@opr", "B5"),
                new SqlParameter("@dep_code", dep_code),
                new SqlParameter("@req_si_type", req_type)   
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();

                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAllRequestNos")]
        public HttpResponseMessage GetAllRequestNos(string dep_code, string req_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllRequestNos(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllRequestNos"));

            List<Invs044_request> mod_list = new List<Invs044_request>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "B6"),
                new SqlParameter("@dep_code", dep_code),
                new SqlParameter("@req_si_type", req_type)            
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Invs044_request invsObj = new Invs044_request();
                            invsObj.invs058_req_no = dr["req_no"].ToString();
                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetCompanyCurrency")]
        public HttpResponseMessage GetCompanyCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCompanyCurrency(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetCompanyCurrency"));

            Invs044_request invsObj = new Invs044_request();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>() 
                         {                           
                 
                            new SqlParameter("@opr", "B8")                         
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invsObj.or_compcurrency = dr["comp_curcy_code"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj.or_compcurrency);

        }
        
        //Getvehiclergnos
        [Route("Getvehiclergnos")]
        public HttpResponseMessage Getvehiclergnos()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getvehiclergnos()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_requestdetails> doc_list = new List<Invs044_requestdetails>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C1"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_requestdetails simsobj = new Invs044_requestdetails();
                            simsobj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            simsobj.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            simsobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //****************RFQ Methods******************//

        [Route("CRFQDetails")]
        public HttpResponseMessage CRFQDetails(string data1, List<requestforquotation_details> data)
        {
            string inserted = "0";
            string rfqno = null;
            requestforquotation simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<requestforquotation>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                  new List<SqlParameter>() 
                                 { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@sup_code",simsobj.sup_code),                               
                                new SqlParameter("@rfq_date", db.DBYYYYMMDDformat(simsobj.rfq_date)),                                
                                new SqlParameter("@rfq_type",simsobj.rfq_type),
                                new SqlParameter("@rfq_dep_code",simsobj.rfq_dep_code),
                                new SqlParameter("@rfq_close_date",db.DBYYYYMMDDformat(simsobj.rfq_close_date)),
                                new SqlParameter("@rfq_quote_ref",simsobj.rfq_quote_ref),
                                new SqlParameter("@rfq_quote_date",db.DBYYYYMMDDformat(simsobj.rfq_date)),
                                new SqlParameter("@cur_code",simsobj.cur_code),
                                new SqlParameter("@rfq_remarks",simsobj.rfq_remarks),
                                new SqlParameter("@rfq_create_user_code",simsobj.rfq_create_user_code),
                                new SqlParameter("@rfq_creation_date",db.DBYYYYMMDDformat(simsobj.rfq_creation_date)),                                
                                new SqlParameter("@rfq_status","A"),
                         });

                    if (dr.Read())
                    {
                        rfqno = dr["rfq_no"].ToString();
                    }
                }


                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();

                    foreach (requestforquotation_details simsobj1 in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                      new List<SqlParameter>() 
                                 { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@rfqno",rfqno),                               
                                new SqlParameter("@req_no", simsobj1.req_no),                                
                                new SqlParameter("@rfq_line_no","0"),
                                new SqlParameter("@im_inv_no",simsobj1.im_inv_no),
                                new SqlParameter("@uom_code",simsobj1.uom_code),
                                new SqlParameter("@rfq_rd_quantity",simsobj1.rfq_rd_quantity),
                                new SqlParameter("@rfq_supply_wanted_date",db1.DBYYYYMMDDformat(simsobj1.rfq_supply_wanted_date)),
                                new SqlParameter("@rfq_remarks_details",simsobj1.rfq_remarks_details),
                                new SqlParameter("@rfq_status_details","A"),                                
                         });

                        if (dr1.Read())
                        {
                            inserted = dr1["insertedval"].ToString();
                        }

                        dr1.Close();
                        //db1.Dispose();
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("Get_ALLRFQDetails")]
        public HttpResponseMessage Get_ALLRFQDetails(string dep_code, string req_type, string srch_supcode, string srch_itemdesc, string srch_itemcode)
        {
            List<requestforquotation> group_list = new List<requestforquotation>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@dep_code", dep_code),
                                new SqlParameter("@req_si_type", req_type),
                                new SqlParameter("@srch_supcode", srch_supcode),
                                new SqlParameter("@srch_itemdesc", srch_itemdesc),
                                new SqlParameter("@srch_itemcode", srch_itemcode),
                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        requestforquotation invsobj = new requestforquotation();

                        invsobj.rfq_no = dr["rfq_no"].ToString();
                        invsobj.sup_code = dr["sup_code"].ToString();
                        invsobj.sup_name = dr["sup_name"].ToString();
                        invsobj.rfq_date = dr["rfq_date"].ToString();
                        invsobj.rfq_type = dr["rfq_type"].ToString();
                        invsobj.requesttype = dr["requesttype"].ToString();
                        invsobj.rfq_dep_name = dr["dep_name"].ToString();
                        invsobj.rfq_dep_code = dr["rfq_dep_code"].ToString();
                        invsobj.rfq_close_date = db.UIDDMMYYYYformat(dr["rfq_close_date"].ToString());
                        invsobj.rfq_quote_ref = dr["rfq_quote_ref"].ToString();
                        invsobj.rfq_quote_date = db.UIDDMMYYYYformat(dr["rfq_quote_date"].ToString());
                        invsobj.cur_code = dr["cur_code"].ToString();
                        invsobj.cur_name = dr["cur_name"].ToString();
                        invsobj.rfq_remarks = dr["rfq_remarks"].ToString();
                        invsobj.rfq_create_user_code = dr["rfq_create_user_code"].ToString();
                        invsobj.rfq_create_user_name = dr["rfq_create_user_name"].ToString();
                        invsobj.rfq_creation_date = db.UIDDMMYYYYformat(dr["rfq_creation_date"].ToString());
                        invsobj.rfq_approve_user_code = dr["rfq_approve_user_name"].ToString();
                        invsobj.rfq_approval_date = db.UIDDMMYYYYformat(dr["rfq_approval_date"].ToString());
                        invsobj.rfq_approval_remark = dr["rfq_approval_remark"].ToString();
                        invsobj.rfq_status = dr["rfq_status"].ToString();
                        invsobj.req_no1 = false;
                        invsobj.im_inv_no = dr["im_inv_no"].ToString();
                        invsobj.lst_rfq_details = getrfqdetails(invsobj.rfq_no);
                        group_list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, group_list);

        }

        private List<requestforquotation_details> getrfqdetails(string rfq_no)
        {
            List<requestforquotation_details> group_list = new List<requestforquotation_details>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'D'),
                                new SqlParameter("@rfqno", rfq_no),                                
                         });
                    //string str=null;
                    while (dr.Read())
                    {
                        requestforquotation_details invsobj = new requestforquotation_details();
                        invsobj.rfq_no = dr["rfq_no"].ToString();
                        invsobj.req_no = dr["req_no"].ToString();
                        invsobj.rfq_line_no = dr["rfq_line_no"].ToString();
                        invsobj.im_inv_no = dr["im_inv_no"].ToString();
                        invsobj.uom_code = dr["uom_code"].ToString();
                        invsobj.uom_name = dr["uom_name"].ToString();
                        invsobj.im_item_code = dr["im_item_code"].ToString();
                        invsobj.im_desc = dr["im_desc"].ToString();
                        invsobj.rfq_rd_quantity = dr["rfq_rd_quantity"].ToString();
                        invsobj.pqd_quoted_price = "0";
                        invsobj.rfq_remarks_details = dr["rfq_remarks"].ToString();
                        invsobj.rfq_supply_wanted_date = db.UIDDMMYYYYformat(dr["rfq_supply_wanted_date"].ToString());
                        invsobj.pqd_quoted_supply_date = db.UIDDMMYYYYformat(dr["rfq_supply_wanted_date"].ToString());
                        invsobj.rfq_status_details = dr["rfq_status"].ToString();

                        group_list.Add(invsobj);
                    }
                }
            }
            catch (Exception de)
            { }

            return group_list;
        }

        [Route("SaveQuotationDetails")]
        public HttpResponseMessage SaveQuotationDetails(List<requestforquotation_details> data)
        {
            string inserted = "0";
            int i = 0;
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();

                    foreach (requestforquotation_details simsobj1 in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                      new List<SqlParameter>() 
                                 { 
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@val",i),                               
                                new SqlParameter("@rfqno",simsobj1.rfq_no),                               
                                new SqlParameter("@req_no", simsobj1.req_no),                                
                                new SqlParameter("@rfq_line_no",simsobj1.rfq_line_no),
                                //new SqlParameter("@im_inv_no",simsobj1.im_inv_no),
                                //new SqlParameter("@uom_code",simsobj1.uom_code),
                                new SqlParameter("@pqd_quoted_price",simsobj1.pqd_quoted_price),
                                new SqlParameter("@rfq_quoted_quantity",simsobj1.pqd_quoted_qty),
                                new SqlParameter("@rfq_supply_wanted_date",db1.DBYYYYMMDDformat(simsobj1.rfq_supply_wanted_date)),
                                new SqlParameter("@pqd_quoted_supply_date",db1.DBYYYYMMDDformat(simsobj1.pqd_quoted_supply_date)),
                                new SqlParameter("@rfq_remarks_details",simsobj1.rfq_remarks_details),
                                new SqlParameter("@terms",simsobj1.terms),
                                new SqlParameter("@depcode",simsobj1.depcode),
                                new SqlParameter("@im_inv_no",simsobj1.im_inv_no),
                                new SqlParameter("@rfq_type",simsobj1.rfq_type),
                         });
                        i = i + 1;

                        if (dr1.Read())
                        {
                            inserted = dr1["retval"].ToString();
                        }

                        dr1.Close();
                        //db1.Dispose();
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        //******Supplier Comparison  API's***********//

        [Route("GetQuotationrequestsAll")]
        public HttpResponseMessage GetQuotationrequestsAll()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationrequests()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<requestforquotation_details> doc_list = new List<requestforquotation_details>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "R"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            requestforquotation_details simsobj = new requestforquotation_details();
                            simsobj.req_no = dr["req_no"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetQuotationrequests")]
        public HttpResponseMessage GetQuotationrequests(string dep_code, string req_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationrequests()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<requestforquotation_details> doc_list = new List<requestforquotation_details>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {

                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@dep_code", dep_code),
                            new SqlParameter("@req_type", req_type),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            requestforquotation_details simsobj = new requestforquotation_details();
                            simsobj.req_no = dr["req_no"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        public List<requestforquotation_details> GetQuotationsuppliers(string reqnos)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationsuppliers()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<requestforquotation_details> doc_list = new List<requestforquotation_details>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "H"),
                            new SqlParameter("@requestno",reqnos),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            requestforquotation_details simsobj = new requestforquotation_details();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return doc_list;
        }

        public List<q_det> GetQuotationsuppliersdetails(string req_no, string im_inv_no, string rdlineno, string reqnos)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationsuppliers()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<requestforquotation_details> sup_list = new List<requestforquotation_details>();
            List<q_det> doc_list = new List<q_det>();

            sup_list = GetQuotationsuppliers(reqnos);

            string supplierlist = string.Empty;

            foreach (requestforquotation_details rq in sup_list)
            {
                supplierlist = supplierlist + "," + rq.sup_code;
            }

            supplierlist = supplierlist.Substring(1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@im_inv_no", im_inv_no),
                            new SqlParameter("@req_no", req_no),
                            new SqlParameter("@rd_line_no", rdlineno),
                            new SqlParameter("@sup_code", supplierlist),
                            new SqlParameter("@requestno",reqnos),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            q_det simsobj = new q_det();
                            simsobj.sup_itemchk = false;
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.pqd_quoted_qty = dr["pqd_quoted_qty"].ToString();
                            simsobj.pqd_quoted_price = dr["pqd_quoted_price"].ToString();
                            simsobj.pqd_supplydate = db.UIDDMMYYYYformat(dr["pqd_quoted_supply_date"].ToString());
                            simsobj.pqd_remarks = dr["pqd_remarks"].ToString();
                            string minq = dr["minq"].ToString();
                            if (minq == dr["pqd_quoted_price"].ToString())
                                simsobj.sup_itemchk = true;
                            else
                                simsobj.sup_itemchk = false;

                            simsobj.im_inv_no = im_inv_no;
                            simsobj.req_no = req_no;

                            doc_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return doc_list;
        }

        //public List<q_det> GetQuotationsuppliersdetails(string req_no,string im_inv_no,string rdlineno,string reqnos)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationsuppliers()PARAMETERS ::NA";
        //    //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

        //    List<requestforquotation_details> sup_list = new List<requestforquotation_details>();
        //    List<q_det> doc_list = new List<q_det>();

        //    sup_list = GetQuotationsuppliers(reqnos);

        //    foreach (requestforquotation_details rq in sup_list)
        //    {
        //        try
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                db.Open();
        //                SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
        //                    new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", "J"),
        //                    new SqlParameter("@im_inv_no", im_inv_no),
        //                    new SqlParameter("@req_no", req_no),
        //                    new SqlParameter("@rd_line_no", rdlineno),
        //                    //new SqlParameter("@sup_code", rq.sup_code),

        //                 });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        bool flgchk = false;
        //                        q_det simsobj = new q_det();
        //                        foreach (q_det qdt in doc_list)
        //                        {
        //                            if (qdt.sup_code == rq.sup_code)
        //                            {
        //                                flgchk = true;
        //                            }
        //                        }
        //                        if (flgchk == false)
        //                        {
        //                            if (dr["sup_code"].ToString() == rq.sup_code)
        //                            {
        //                                simsobj.sup_code = dr["sup_code"].ToString();
        //                                simsobj.sup_name = dr["sup_name"].ToString();
        //                                simsobj.pqd_quoted_qty = dr["pqd_quoted_qty"].ToString();
        //                                simsobj.pqd_quoted_price = dr["pqd_quoted_price"].ToString();
        //                                simsobj.sup_itemchk = false;
        //                            }
        //                            else
        //                            {
        //                                simsobj.sup_code = rq.sup_code;
        //                                simsobj.sup_name = rq.sup_name;
        //                                simsobj.pqd_quoted_qty = "";
        //                                simsobj.pqd_quoted_price = "";
        //                                simsobj.sup_itemchk = false;
        //                            }
        //                            doc_list.Add(simsobj);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception x)
        //        {
        //        }
        //    }
        //    return doc_list;
        //}

        [Route("GetQuotationItemRequests")]
        public HttpResponseMessage GetQuotationItemRequests(string reqnos)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetQuotationItemRequests()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<requestforquotation_details> doc_list = new List<requestforquotation_details>();

            Message message = new Message();
            string str = "", str1 = "", str2 = "";
            List<requestforquotation_details> inv_list = new List<requestforquotation_details>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "K"),
                            new SqlParameter("@requestno",reqnos),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            requestforquotation_details simsobj = new requestforquotation_details();

                            str = dr["im_inv_no"].ToString();
                            str1 = dr["req_no"].ToString();
                            str2 = dr["rd_line_no"].ToString();
                            var v = (from p in inv_list where (p.im_inv_no == str && p.req_no == str1 && p.rfq_line_no == str2) select p);

                            if (v.Count() == 0)
                            {
                                simsobj.im_desc = dr["item"].ToString();
                                simsobj.req_no = dr["req_no"].ToString();
                                simsobj.im_inv_no = dr["im_inv_no"].ToString();
                                simsobj.rfq_line_no = dr["rd_line_no"].ToString();

                                simsobj.lstqdetails = GetQuotationsuppliersdetails(simsobj.req_no, simsobj.im_inv_no, simsobj.rfq_line_no, reqnos);

                                inv_list.Add(simsobj);
                            }

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inv_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, inv_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
        
        [Route("SaveQuotationOrderDetails")]
        public HttpResponseMessage SaveQuotationOrderDetails(List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            string randomnumber = string.Empty;
            //Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    //foreach (Invs044_requestdetails obj in data1)
                    //{
                    //    randomnumber = obj.randomnumber;
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        randomnumber = simsobj.randomnumber;
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[purchase_rfq_proc]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A3"),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(simsobj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", simsobj.order_dep_code),
                                      new SqlParameter("@order_sup_code", simsobj.order_sup_code),
                                      new SqlParameter("@order_cur_code", simsobj.order_cur_code),
                                      new SqlParameter("@order_fa_code", simsobj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", simsobj.order_dm_code),
                                      new SqlParameter("@order_trt_code", simsobj.order_trt_code),
                                      new SqlParameter("@order_pm_code", simsobj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",simsobj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(simsobj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),                        
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",simsobj.invs058_req_type),
                                      new SqlParameter("@randomno",simsobj.randomnumber),    
             
                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),                 
                                      new SqlParameter("@order_createdby",simsobj.invs058_order_createdby),                 
                                      new SqlParameter("@order_heading",simsobj.invs058_heading),                 
                                      new SqlParameter("@order_notes",simsobj.invs058_notes),                 
                                      new SqlParameter("@order_apply_aexp",simsobj.invs058_order_apply_aexp),       
          
                                      new SqlParameter("@ord_addexps",simsobj.invs058_ord_addexps),       
                                      new SqlParameter("@ord_dispct",simsobj.invs058_ord_dispct),       
                                      new SqlParameter("@ord_disamt",simsobj.invs058_ord_disamt),       
                                      new SqlParameter("@od_total_discount",simsobj.invs058_od_total_discount),       
                                      new SqlParameter("@od_total_expense",simsobj.invs058_od_total_expense),       
                                      new SqlParameter("@ord_vehicleno",simsobj.sims_transport_vehicle_code),      
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    // }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[purchase_rfq_proc]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","A9"),                 
                                      new SqlParameter("@randomno",randomnumber),                 
                                 });

                        if (dr.Read())
                        {
                            inserted = true;
                            OrderNumber = dr["OrderNumbers"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, OrderNumber);
            }
        }
        
        //Upload Documents API

        [Route("SaveOrderDocs")]
        public HttpResponseMessage SaveOrderDocs(List<Inv044_OrderDocs> data)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : SaveOrderDocs(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool inserted = false;
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv044_OrderDocs simsobj in data)
                    {
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr","C3"),
                                      new SqlParameter("@comp_code",simsobj.comcode),
                                      new SqlParameter("@vouchernum",simsobj.vouchernum),
                                      new SqlParameter("@vouchertype",simsobj.vouchertype),
                                      new SqlParameter("@doclinenum",simsobj.doclineno),
                                      new SqlParameter("@filename",simsobj.filename),
                                      new SqlParameter("@filename_en",simsobj.filename_en),
                                      new SqlParameter("@filepath",simsobj.filepath),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(simsobj.orddate)),
                               
                         });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
                        
        private List<Inv044_OrderDocs> GetRequestDocs(string vouchertype,string vouchernum)
        {
            List<Inv044_OrderDocs> doc_list = new List<Inv044_OrderDocs>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C2"),
                            new SqlParameter("@vouchertype", vouchertype),
                            new SqlParameter("@vouchernum", vouchernum),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv044_OrderDocs simsobj = new Inv044_OrderDocs();
                            simsobj.filename = dr["invs_ref_doc_name"].ToString();
                            simsobj.filename_en = dr["invs_ref_doc_desc"].ToString();
                            doc_list.Add(simsobj);
                        }                      

                    }
                }
            }
            catch (Exception x)
            {
               
            }

            return doc_list;
        }

        [Route("GetOrderDocsApprove")]
        public HttpResponseMessage GetOrderDocsApprove(string vtype, string vouchernum)
        {
            List<Inv044_OrderDocs> doc_list = new List<Inv044_OrderDocs>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C2"),
                            new SqlParameter("@vouchertype", vtype),
                            new SqlParameter("@vouchernum", vouchernum),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv044_OrderDocs simsobj = new Inv044_OrderDocs();
                            simsobj.filename = dr["invs_ref_doc_name"].ToString();
                            simsobj.filename_en = dr["invs_ref_doc_desc"].ToString();
                            doc_list.Add(simsobj);
                        }

                    }
                }
            }
            catch (Exception x)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, doc_list);

        }

        [Route("DelOrderDocs")]
        public HttpResponseMessage DelOrderDocs(List<Inv044_OrderDocs> data)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : DelOrderDocs(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool inserted = false;
            string ordnumber = "";
            string filename_en = "";
            int i = 0;
            foreach (Inv044_OrderDocs simsobj in data)
            {
                if (i == 0)
                    ordnumber = simsobj.vouchernum;
                    filename_en = simsobj.filename_en;

            i = i + 1;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]",
                           new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr","C4"),
                               new SqlParameter("@comp_code","1"),
                               new SqlParameter("@vouchernum",ordnumber),
                               new SqlParameter("@vouchertype","2"),
                               new SqlParameter("@filename_en",filename_en)
                         });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                    
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //GetDepartment
        [Route("GetLastPurchaseDetails")]
        public HttpResponseMessage GetLastPurchaseDetails(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLastPurchaseDetails()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs044_requestdetails> doc_list = new List<Invs044_requestdetails>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C5"),
                            new SqlParameter("@im_inv_no", im_inv_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_requestdetails simsobj = new Invs044_requestdetails();
                            simsobj.order_ord_date = dr["ord_date"].ToString();
                            simsobj.ord_no = dr["ord_no"].ToString();
                            simsobj.order_d_od_order_qty = dr["od_order_qty"].ToString();
                            simsobj.order_d_od_received_qty = dr["od_approved_quantity"].ToString();
                            simsobj.invs058_sup_price = dr["od_supplier_price"].ToString();
                            simsobj.order_d_od_discount_pct = dr["discount"].ToString();
                           // simsobj.invs058_discount = dr["od_discount_amt"].ToString();
                            simsobj.order_ord_status = dr["od_status"].ToString();
                            simsobj.currentqty = dr["currentqty"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetAllRejectdOrder")]
        public HttpResponseMessage GetAllRejectdOrder(string dep_code, string req_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllRejectdOrder(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllRejectdOrder"));

           //List<invs035> mod_list = new List<invs035>();
            DataSet ds = new DataSet();
            //if (reject_order_no == "undefined" || reject_order_no == "" || reject_order_no == null)
            //    reject_order_no = null;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new]",
                    ds = db.ExecuteStoreProcedureDS("[invs].[invs_Order_proc_new]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "RO"),
                        new SqlParameter("@dep_code", dep_code),
                        new SqlParameter("@req_si_type", req_type)
                        //new SqlParameter("@reject_order_no", reject_order_no)
                    });                   
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
               
            }          
            return Request.CreateResponse(HttpStatusCode.OK, ds);
        }

        [Route("UpdateOrderDetails")]
        public HttpResponseMessage UpdateOrderDetails(string data1, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new]",
                           new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","UP"),
                                      new SqlParameter("@reject_order_no",obj.ord_no),
                                      new SqlParameter("@order_ord_status",0),
                                      new SqlParameter("@order_ord_date", db.DBYYYYMMDDformat(obj.order_ord_date)),
                                      new SqlParameter("@order_dep_code", obj.order_dep_code),
                                      new SqlParameter("@order_sup_code", obj.order_sup_code),
                                      new SqlParameter("@order_cur_code", obj.order_cur_code),
                                      new SqlParameter("@order_fa_code", obj.order_fa_code),
                                      new SqlParameter("@order_ord_agency_flag",1),
                                      new SqlParameter("@order_dm_code", obj.order_dm_code),
                                      new SqlParameter("@order_trt_code", obj.order_trt_code),
                                      new SqlParameter("@order_pm_code", obj.order_pm_code),
                                      //new SqlParameter("@order_lc_no", Convert.ToInt32(obj.order_lc_no)),
                                      new SqlParameter("@order_ord_supl_quot_ref",obj.order_ord_supl_quot_ref),
                                      new SqlParameter("@order_ord_shipment_date", db.DBYYYYMMDDformat(obj.order_ord_shipment_date)),
                                      new SqlParameter("@order_ord_loading_note_no",""),
                                      new SqlParameter("@order_ord_loading_note_date",""),
                                      new SqlParameter("@order_ord_confirmation_no",""),
                                      new SqlParameter("@order_ord_confirmation_date",""),
                                      new SqlParameter("@order_d_ord_no",order_no),
                                      new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),
                                      new SqlParameter("@order_d_uom_code",simsobj.order_d_uom_code),
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),
                                      new SqlParameter("@order_d_od_status",0),
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),
                                      new SqlParameter("@order_d_od_shipped_qty",0),
                                      new SqlParameter("@order_d_od_received_qty",0),
                                      new SqlParameter("@order_d_od_orig_line_no",0),
                                      new SqlParameter("@req_si_type",obj.invs058_req_type),
                                      new SqlParameter("@randomno",obj.randomnumber),

                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),
                                      new SqlParameter("@order_createdby",obj.invs058_order_createdby),
                                      new SqlParameter("@order_heading",obj.invs058_heading),
                                      new SqlParameter("@order_notes",obj.invs058_notes),
                                      new SqlParameter("@order_apply_aexp",obj.invs058_order_apply_aexp),

                                      new SqlParameter("@ord_addexps",obj.invs058_ord_addexps),
                                      new SqlParameter("@ord_dispct",obj.invs058_ord_dispct),
                                      new SqlParameter("@ord_disamt",obj.invs058_ord_disamt),
                                      new SqlParameter("@od_total_discount",obj.invs058_od_total_discount),
                                      new SqlParameter("@od_total_expense",obj.invs058_od_total_expense),
                                      new SqlParameter("@ord_vehicleno",obj.sims_transport_vehicle_code),
                                 });

                        if (ins1 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                                        
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


        [Route("UpdateOrderDetails_VAT")]
        public HttpResponseMessage UpdateOrderDetails_VAT(string data1, List<Invs044_requestdetails> data)
        {
            bool inserted = false;
            string order_no = "0";
            string OrderNumber = string.Empty;
            Invs044_requestdetails obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data1);
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs044_requestdetails simsobj in data)
                    {
                        if (string.IsNullOrEmpty(simsobj.order_d_od_order_qty))
                            simsobj.order_d_od_order_qty = "0";
                        if (string.IsNullOrEmpty(simsobj.order_d_od_supplier_price))
                            simsobj.order_d_od_supplier_price = "0";
                        //int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_Order_proc_new_vat]",
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                            new List<SqlParameter>()
                                 {
                                      new SqlParameter("@opr","U1"),
                                      
                                      new SqlParameter("@order_d_ord_no",obj.ord_no),
                                      //new SqlParameter("@order_d_req_no",simsobj.order_d_req_no),
                                      new SqlParameter("@order_d_rd_line_no",simsobj.order_d_rd_line_no),
                                      new SqlParameter("@order_d_im_inv_no",simsobj.order_d_im_inv_no),                                    
                                      new SqlParameter("@order_d_od_order_qty",simsobj.order_d_od_order_qty),
                                      new SqlParameter("@order_d_od_supplier_price", simsobj.order_d_od_supplier_price),
                                      new SqlParameter("@order_d_od_discount_pct", simsobj.order_d_od_discount_pct),                                      
                                      new SqlParameter("@order_d_od_remarks",simsobj.order_d_od_remarks),                                      
                                      new SqlParameter("@od_discount_amt",simsobj.invs058_discount_amt),
                                     // new SqlParameter("@order_createdby",obj.invs058_order_createdby),                                      
                                      new SqlParameter("@od_item_vat_per",simsobj.od_item_vat_per),
                                      new SqlParameter("@od_item_vat_amount",simsobj.od_item_vat_amount)                                      
                                 });

                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr1.Close();
                    }

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                         new List<SqlParameter>()
                         {
                              new SqlParameter("@opr","U2"),                              
                              new SqlParameter("@order_sup_code",obj.order_sup_code),
                              new SqlParameter("@order_cur_code",obj.order_cur_code),
                              new SqlParameter("@order_dm_code",obj.order_dm_code),
                              new SqlParameter("@ord_shipment_date",db.DBYYYYMMDDformat(obj.order_ord_shipment_date)),
                              new SqlParameter("@order_pm_code",obj.order_pm_code),
                              new SqlParameter("@order_trt_code",obj.order_trt_code),
                              new SqlParameter("@order_fa_code",obj.order_fa_code),
                              new SqlParameter("@order_ord_supl_quot_ref",obj.order_ord_supl_quot_ref),
                              new SqlParameter("@ord_vehicleno",obj.sims_transport_vehicle_code),
                              new SqlParameter("@order_heading",obj.invs058_heading),
                              new SqlParameter("@order_notes",obj.invs058_notes),
                              new SqlParameter("@ord_addexps",obj.invs058_ord_addexps),
                              new SqlParameter("@ord_dispct",obj.invs058_ord_dispct),
                              new SqlParameter("@ord_disamt",obj.invs058_ord_disamt),
                              new SqlParameter("@ord_vat_total_amount",obj.ord_vat_total_amount),
                              new SqlParameter("@order_d_ord_no",obj.ord_no)
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;                         
                        }

                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


        [Route("GetPurchaseOrder")]
        public HttpResponseMessage GetPurchaseOrder(string dept_code,string req_type)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                //Invs044_requestdetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);
                if (dept_code == "undefined" || dept_code == "")
                    dept_code = null;
                if (req_type == "undefined" || req_type == "")
                    req_type = null;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_Order_proc_new_vat]",
                        new List<SqlParameter>()
                             {                                 
                                 new SqlParameter("@opr", "PO"),                                
                                 new SqlParameter("@dep_code", dept_code),                                
                                 new SqlParameter("@req_type", req_type)                                
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("GetGLPurchaseBudget")]
        public HttpResponseMessage GetGLPurchaseBudget()
        {            
            List<Invs044_request> doc_list = new List<Invs044_request>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_Order_proc_new_vat]",
                    new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "AC")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs044_request simsobj = new Invs044_request();
                            simsobj.glpr_acct_code = dr["glpr_acct_code"].ToString();
                            simsobj.glac_name = dr["glac_name"].ToString();
                            simsobj.glpr_bud_amt = dr["glpr_bud_amt"].ToString();
                            simsobj.glpr_dr_amt = dr["glpr_dr_amt"].ToString();
                            simsobj.glpr_cr_amt = dr["glpr_cr_amt"].ToString();
                            simsobj.transaction = dr["transaction"].ToString();
                            simsobj.available_balance = dr["available_balance"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }





    }
}