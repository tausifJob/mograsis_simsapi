﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/UnitOfMeasurement")]
    [BasicAuthentication]

    public class UnitOfMeasurementController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Select
        [Route("getAllUnitOfMeasurement")]
        public HttpResponseMessage getAllUnitOfMeasurement()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllUnitOfMeasurement(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllUnitOfMeasurement"));

            List<Invs001> goaltarget_list = new List<Invs001>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[unit_of_measurement_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs001 simsobj = new Invs001();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();                          

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDUnitOfMeasurement")]
        public HttpResponseMessage CUDUnitOfMeasurement(List<Invs001> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs001 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[unit_of_measurement_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@uom_name", simsobj.uom_name),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

       
    }
}