﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.inventoryClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/StoreIssueController")]
    public class StoreIssueController : ApiController
    {

        [Route("GetCur")]
        public HttpResponseMessage GetCur()
        {
            List<InvSto> mod_list = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto inveObj = new InvSto();
                            inveObj.sims_cur_code = dr["sims_cur_code"].ToString();
                            inveObj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();

                            mod_list.Add(inveObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAca")]
        public HttpResponseMessage GetAca(string cur)
        {
            List<InvSto> mod_list = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_cur_code", cur)

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto inveObj = new InvSto();
                            inveObj.sims_acaedmic_year = dr["sims_academic_year"].ToString();
                            inveObj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            mod_list.Add(inveObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getgrd")]
        public HttpResponseMessage Getgrd(string cur, string aca, string user)
        {
            List<InvSto> mod_list = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "G"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto inveObj = new InvSto();
                            inveObj.sims_grade_code = dr["sims_grade_code"].ToString();
                            inveObj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(inveObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getsec")]
        public HttpResponseMessage Getsec(string cur, string aca, string grd, string user)
        {
            List<InvSto> mod_list = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_login_code", user)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto inveObj = new InvSto();
                            inveObj.sims_section_code = dr["sims_section_code"].ToString();
                            inveObj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(inveObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        //[Route("getDetails")]
        //public HttpResponseMessage getDetails(string cur, string aca, string grd, string section,string fromdate, string todate)
        //{
        //    List<InvSto> mod_list = new List<InvSto>();
        //    try
        //    {
        //        if (grd.Contains("undefined"))
        //        {
        //            grd = null;
        //        }
        //        if (section.Contains("undefined"))
        //        {
        //            section = null;
        //        }


        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
        //                new List<SqlParameter>()
        //                 {
        //        new SqlParameter("@opr", "Z"),
        //        new SqlParameter("@sims_cur_code", cur),
        //        new SqlParameter("@sims_acaedmic_year", aca),
        //        new SqlParameter("@sims_grade_code", grd),
        //        new SqlParameter("@sims_section_code", section),
        //        new SqlParameter("@from_date", db.DBYYYYMMDDformat(fromdate)),
        //        new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
        //        });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    InvSto inveObj = new InvSto();
        //                    inveObj.invoice_no = dr["doc_prov_no"].ToString();
        //                    inveObj.enroll_no = dr["sims_student_enroll_number"].ToString();
        //                    inveObj.student_name = dr["student_name"].ToString();
        //                    inveObj.sell_amt = dr["doc_total_amount"].ToString();
        //                    inveObj.im_item_code = dr["im_item_code"].ToString();
        //                   // inveObj.dd_issue_qty = dr["dd_issue_qty"].ToString();
        //                    inveObj.im_desc = dr["im_desc"].ToString();
        //                    inveObj.id_cur_qty = dr["id_cur_qty"].ToString();
        //                    inveObj.dd_qty = dr["dd_qty"].ToString();
        //                   // inveObj.doc_issue_date = dr["doc_issue_date"].ToString();
        //                    mod_list.Add(inveObj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        //[Route("getDetails_by_doc_prov_no")]
        //public HttpResponseMessage getDetails_by_doc_prov_no(string doc_prov_no)
        //{
        //    List<InvSto> mod_list = new List<InvSto>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc] ",
        //                new List<SqlParameter>()
        //                 {
        //        new SqlParameter("@opr", "Y"),
        //        new SqlParameter("@doc_prov_no", doc_prov_no)

        //        });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    InvSto inveObj = new InvSto();
        //                    inveObj.im_item_code= dr["im_item_code"].ToString();
        //                    inveObj.dd_issue_qty= dr["dd_issue_qty"].ToString();
        //                    inveObj.im_desc = dr["im_desc"].ToString();
        //                    inveObj.id_cur_qty = dr["id_cur_qty"].ToString();
        //                    inveObj.dd_qty = dr["dd_qty"].ToString();
        //                    inveObj.doc_issue_date = dr["doc_issue_date"].ToString();
        //                    mod_list.Add(inveObj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //}

        [Route("getDetailsNew")]
        public HttpResponseMessage getDetailsNew(string cur, string aca, string grd, string section, string fromdate, string todate)
        {
            string str = string.Empty;
            if (grd.Contains("undefined"))
            {
                grd = null;
            }
            if (section.Contains("undefined"))
            {
                section = null;
            }

            List<InvSto> house = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                       new List<SqlParameter>()
                        {
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@from_date", db.DBYYYYMMDDformat(fromdate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["doc_prov_no"].ToString();

                            var v = (from p in house where p.invoice_no == str select p);

                            if (v.Count() == 0)
                            {
                                InvSto ob = new InvSto();
                                ob.sublist1 = new List<InvSto_sub_list>();
                                ob.invoice_no = dr["doc_prov_no"].ToString();
                                ob.enroll_no = dr["sims_student_enroll_number"].ToString();
                                ob.student_name = dr["student_name"].ToString();
                                ob.sell_amt = dr["doc_total_amount"].ToString();
                                ob.creation_user = dr["creation_user"].ToString();
                                ob.doc_prov_date = dr["doc_prov_date"].ToString();
                                ob.sims_icon = "fa fa-plus-circle";


                                InvSto_sub_list sub = new InvSto_sub_list();
                                sub.invoice_no = dr["doc_prov_no"].ToString();
                                sub.im_item_code = dr["im_item_code"].ToString();
                                sub.issued_dd_issue_qty = dr["issued_dd_issue_qty"].ToString();
                                sub.im_desc = dr["im_desc"].ToString();
                                sub.id_cur_qty = dr["id_cur_qty"].ToString();
                                sub.dd_qty = dr["dd_qty"].ToString();
                                sub.doc_issue_date = dr["doc_issue_date"].ToString();
                                sub.enroll_no = dr["sims_student_enroll_number"].ToString();
                                sub.student_name = dr["student_name"].ToString();
                                sub.sell_amt = dr["doc_total_amount"].ToString();
                                sub.creation_user = dr["creation_user"].ToString();
                                sub.doc_prov_date = dr["doc_prov_date"].ToString();
                                sub.doc_dept = dr["dep_code"].ToString();
                                sub.creation_date = dr["creation_date"].ToString();
                                sub.im_inv_no = dr["im_inv_no"].ToString();
                                sub.loc_code = dr["loc_code"].ToString();
                                sub.dd_physical_qty = dr["dd_physical_qty"].ToString();
                                ob.sublist1.Add(sub);
                                house.Add(ob);
                            }
                            else
                            {
                                InvSto_sub_list sub = new InvSto_sub_list();
                                sub.invoice_no = dr["doc_prov_no"].ToString();
                                sub.im_item_code = dr["im_item_code"].ToString();
                                sub.issued_dd_issue_qty = dr["issued_dd_issue_qty"].ToString();
                                sub.im_desc = dr["im_desc"].ToString();
                                sub.id_cur_qty = dr["id_cur_qty"].ToString();
                                sub.dd_qty = dr["dd_qty"].ToString();
                                sub.doc_issue_date = dr["doc_issue_date"].ToString();
                                sub.enroll_no = dr["sims_student_enroll_number"].ToString();
                                sub.student_name = dr["student_name"].ToString();
                                sub.sell_amt = dr["doc_total_amount"].ToString();
                                sub.creation_user = dr["creation_user"].ToString();
                                sub.doc_prov_date = dr["doc_prov_date"].ToString();
                                sub.doc_dept = dr["dep_code"].ToString();
                                sub.creation_date = dr["creation_date"].ToString();
                                sub.loc_code = dr["loc_code"].ToString();
                                sub.im_inv_no = dr["im_inv_no"].ToString();
                                sub.dd_physical_qty = dr["dd_physical_qty"].ToString();
                                v.ElementAt(0).sublist1.Add(sub);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           
        }

        [Route("insertStoreIssueData")]

        public HttpResponseMessage insertStoreIssueData(List<InvSto_sub_list> InvSto_data)
        {
            bool inserted = false;
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (InvSto_sub_list inner_tebel_data in InvSto_data)
                    {

                        ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                       {
                             #region Adding data into database
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@dep_code", inner_tebel_data.doc_dept ),
                             new SqlParameter("@doc_prov_no",  inner_tebel_data.invoice_no ),
                             new SqlParameter("@doc_prov_date", inner_tebel_data.doc_prov_date),
                             new SqlParameter("@enroll_no",  inner_tebel_data.enroll_no ),
                             new SqlParameter("@im_inv_no", inner_tebel_data.im_inv_no ),
                             new SqlParameter("@loc_code", inner_tebel_data.loc_code ),
                             new SqlParameter("@dd_physical_qty", inner_tebel_data.dd_physical_qty),
                             new SqlParameter("@dd_qty", inner_tebel_data.dd_qty ),
                             new SqlParameter("@id_cur_qty", inner_tebel_data.id_cur_qty ),
                             new SqlParameter("@issue_quantity", inner_tebel_data.issue_quantity ),
                             new SqlParameter("@issued_dd_issue_qty", inner_tebel_data.issued_dd_issue_qty ),
                             new SqlParameter("@issue_date", inner_tebel_data.doc_issue_date),
                             new SqlParameter("@creation_user", inner_tebel_data.creation_user ),
                             new SqlParameter("@creation_date", db.DBYYYYMMDDformat(inner_tebel_data.creation_date)),
                             new SqlParameter("@remarks", inner_tebel_data.remarks ),
                            #endregion
                        });
                    }

                    if (ins >= 1)
                    {
                        int ins2 = db.ExecuteStoreProcedureforInsert("[dbo].[invs_store_issue_proc]",
                                          new List<SqlParameter>()
                                         {
                             #region Adding data into database
                             new SqlParameter("@opr", "I"),
                                              #endregion
                                          });

                        if (ins2 > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getreport_store")]
        public HttpResponseMessage getreport_store(string doc_prov_no, string enroll_no, string sims_cur_code, string sims_acaedmic_year, string doc_prov_date)
        {
            string doc_sr_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "P"),
                new SqlParameter("@enroll_no", enroll_no),
                new SqlParameter("@doc_prov_no", doc_prov_no),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            doc_sr_code = dr["doc_sr_code"].ToString();

                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_sr_code);

        }

        [Route("getreport_store_1")]
        public HttpResponseMessage getreport_store_1()
        {
            string sims_appl_form_field_value1 = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R"),

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_appl_form_field_value1);

        }

        [Route("getreport_invoice_1")]
        public HttpResponseMessage getreport_invoice_1()
        {
            string sims_appl_form_field_value1 = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "L"),

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_appl_form_field_value1);

        }

        [Route("get_afterSubmitgetreport")]
        public HttpResponseMessage get_afterSubmitgetreport(string doc_prov_no, string sims_cur_code, string sims_acaedmic_year, string doc_prov_date, string user_login)
        {
            string doc_sr_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "M"),
                new SqlParameter("@doc_prov_no_enroll_no", doc_prov_no),
                new SqlParameter("@creation_user", user_login),
                new SqlParameter("@sims_cur_code", sims_cur_code),
                new SqlParameter("@sims_acaedmic_year", sims_acaedmic_year),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            doc_sr_code = dr["doc_sr_code"].ToString();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_sr_code);

        }

        [Route("get_afterSubmitgetreport_single")]
        public HttpResponseMessage get_afterSubmitgetreport_single(string doc_prov_no)
        {
            string doc_sr_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "K"),
                new SqlParameter("@doc_prov_no_enroll_no", doc_prov_no),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            doc_sr_code = dr["doc_sr_code"].ToString();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_sr_code);

        }

        [Route("getDetailsSMFC")]
        public HttpResponseMessage getDetailsSMFC(string cur, string aca, string grd, string section, string fromdate, string todate, string searchText)
        {
            string str = string.Empty;
            if (grd.Contains("undefined"))
            {
                grd = null;
            }
            if (section.Contains("undefined"))
            {
                section = null;
            }
            if (aca.Contains("undefined"))
            {
                aca = null;
            }
            if (cur.Contains("undefined"))
            {
                cur = null;
            }
            if (fromdate.Contains("undefined"))
            {
                fromdate = null;
            }
            if (todate.Contains("undefined"))
            {
                todate = null;
            }
            if (searchText.Contains("undefined"))
            {
                searchText = null;
            }
            List<InvSto> house = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                       new List<SqlParameter>()
                        {
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@from_date", db.DBYYYYMMDDformat(fromdate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
                new SqlParameter("@doc_no", searchText),

               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["doc_prov_no"].ToString();
                            var v = (from p in house where p.invoice_no == str select p);
                            if (v.Count() == 0)
                            {
                                InvSto ob = new InvSto();
                                ob.invoice_no = dr["doc_prov_no"].ToString();
                                ob.enroll_no = dr["sims_student_enroll_number"].ToString();
                                ob.student_name = dr["student_name"].ToString();
                                ob.sell_amt = dr["doc_total_amount"].ToString();
                                ob.creation_user = dr["creation_user"].ToString();
                                ob.doc_prov_date = dr["doc_prov_date"].ToString();
                                ob.dt_code= dr["dt_code"].ToString();                              
                                ob.sims_icon = "fa fa-plus-circle";
                                house.Add(ob);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
        }

        [Route("getDetailsinnerTableSMFC")]
        public HttpResponseMessage getDetailsinnerTableSMFC(string cur, string aca, string grd, string section, string docprovDate, string docprovNo)
        {
            string str = string.Empty;
            if (grd.Contains("undefined"))
            {
                grd = null;
            }
            if (section.Contains("undefined"))
            {
                section = null;
            }
            if (aca.Contains("undefined"))
            {
                aca = null;
            }
            if (cur.Contains("undefined"))
            {
                cur = null;
            }
            if (docprovDate.Contains("undefined"))
            {
                docprovDate = null;
            }
            if (docprovNo.Contains("undefined"))
            {
                docprovNo = null;
            }

            List<InvSto_sub_list> house = new List<InvSto_sub_list>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                       new List<SqlParameter>()
                        {
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@sims_cur_code", cur),
                new SqlParameter("@sims_acaedmic_year", aca),
                new SqlParameter("@sims_grade_code", grd),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@from_date", docprovDate),
                new SqlParameter("@to_date", docprovDate),
                new SqlParameter("@doc_no", docprovNo),
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto_sub_list sub = new InvSto_sub_list();
                            sub.invoice_no = dr["doc_prov_no"].ToString();
                            sub.im_item_code = dr["im_item_code"].ToString();
                            sub.issued_dd_issue_qty = dr["issued_dd_issue_qty"].ToString();
                            sub.im_desc = dr["im_desc"].ToString();
                            sub.id_cur_qty = dr["id_cur_qty"].ToString();
                            sub.dd_qty = dr["dd_qty"].ToString();
                            sub.doc_issue_date = dr["doc_issue_date"].ToString();
                            sub.enroll_no = dr["sims_student_enroll_number"].ToString();
                            sub.student_name = dr["student_name"].ToString();
                            sub.sell_amt = dr["doc_total_amount"].ToString();
                            sub.creation_user = dr["creation_user"].ToString();
                            sub.doc_prov_date = dr["doc_prov_date"].ToString();
                            sub.doc_dept = dr["dep_code"].ToString();
                            sub.creation_date = dr["creation_date"].ToString();
                            sub.im_inv_no = dr["im_inv_no"].ToString();
                            sub.loc_code = dr["loc_code"].ToString();
                            sub.dd_physical_qty = dr["dd_physical_qty"].ToString();                            
                            house.Add(sub);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
        }

        [Route("getReportDetails")]
        public HttpResponseMessage getReportDetails(string doc_sr_code)
        {
            List<InvSto_sub_list> list_of_report = new List<InvSto_sub_list>();
            try
            {
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_store_issue_proc]",
                       new List<SqlParameter>()
                        {
                new SqlParameter("@opr", "O"),
                new SqlParameter("@doc_sr_code", doc_sr_code),
               });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvSto_sub_list ob_InvSto_sub_list = new InvSto_sub_list();
                            ob_InvSto_sub_list.report_im_invoice_no = dr["doc_prov_no"].ToString();
                            ob_InvSto_sub_list.report_invoice_date = dr["doc_issue_date"].ToString();
                            ob_InvSto_sub_list.report_invoice_voucher_no = dr["doc_sr_code"].ToString();
                            ob_InvSto_sub_list.report_particulars = dr["im_desc"].ToString();
                            ob_InvSto_sub_list.report_invoice_qty = dr["dd_qty"].ToString();
                            ob_InvSto_sub_list.report_issued_qty = dr["dd_issue_qty"].ToString();
                            list_of_report.Add(ob_InvSto_sub_list);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list_of_report);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);

            }
        
        }

        [Route("updateRecordData")]
        public HttpResponseMessage updateRecordData(List<InvSto_sub_list> InvSto_data)
        {
            bool inserted = false;
            int ins = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (InvSto_sub_list inner_tebel_data in InvSto_data)
                    {

                        ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_store_issue_proc]",
                        new List<SqlParameter>()
                       {
                             #region Adding data into database
                             new SqlParameter("@opr", "U"),
                             new SqlParameter("@dd_qty", inner_tebel_data.dd_qty),
                             new SqlParameter("@im_inv_no", inner_tebel_data.im_inv_no),
                             new SqlParameter("@dep_code", inner_tebel_data.doc_dept),
                             new SqlParameter("@loc_code", inner_tebel_data.loc_code),                             
                            #endregion
                        });
                    }
                    if (ins >= 1)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}