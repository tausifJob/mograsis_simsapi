﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/CustomClearanceDetails")]
    public class CustomClearanceController:ApiController
    {

        [Route("getCustomClearanceDetails")]
        public HttpResponseMessage getCustomClearanceDetails()
        {
            List<Inv133> Custom_list = new List<Inv133>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_clearance_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv133 obj = new Inv133();

                            obj.ccl_reg_no = dr["ccl_reg_no"].ToString();
                            obj.ccl_date = dr["ccl_date"].ToString();
                            obj.sr_shipment_no = dr["sr_shipment_no"].ToString();
                            obj.ccl_customs_entry_no = dr["ccl_customs_entry_no"].ToString();
                            obj.ccl_customs_entry_date =db.UIDDMMYYYYformat(dr["ccl_customs_entry_date"].ToString());
                            obj.ccl_remarks = dr["ccl_remarks"].ToString();
                            obj.ccl_bill_of_lading_date = db.UIDDMMYYYYformat(dr["ccl_bill_of_lading_date"].ToString());
                            obj.ccl_bill_of_lading = dr["ccl_bill_of_lading"].ToString();

                            Custom_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Custom_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Custom_list);
        }

        [Route("CustomClearanceCUD")]
        public HttpResponseMessage CustomClearanceCUD(List<Inv133> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv133 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_custom_clearance_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@ccl_reg_no",simsobj.ccl_reg_no),
                          new SqlParameter("@ccl_date", db.DBYYYYMMDDformat(simsobj.ccl_date)),
                          new SqlParameter("@sr_shipment_no", simsobj.sr_shipment_no),
                          new SqlParameter("@ccl_customs_entry_no",simsobj.ccl_customs_entry_no),
                          new SqlParameter("@ccl_customs_entry_date", db.DBYYYYMMDDformat(simsobj.ccl_customs_entry_date)),
                          new SqlParameter("@ccl_remarks", simsobj.ccl_remarks),
                          new SqlParameter("@ccl_bill_of_lading_date",db.DBYYYYMMDDformat(simsobj.ccl_bill_of_lading_date)),
                          new SqlParameter("@ccl_bill_of_lading", simsobj.ccl_bill_of_lading)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}