﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/BinsDetail")]
    public class BinsController:ApiController
    {

        [Route("getBinsDetail")]
        public HttpResponseMessage getBinsDetail()
        {
            List<Inv100> Bins = new List<Inv100>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_bins]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Models.ERP.inventoryClass.Inv100 obj = new Inv100();


                            obj.bin_code = dr["bin_code"].ToString();
                            obj.bin_desc = dr["bin_desc"].ToString();
                            Bins.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Bins);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Bins);
        }

        [Route("BinsCUD")]
        public HttpResponseMessage BinsCUD(List<Inv100> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv100 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_bins]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@bin_code", simsobj.bin_code),
                                 new SqlParameter("@bin_desc", simsobj.bin_desc),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}