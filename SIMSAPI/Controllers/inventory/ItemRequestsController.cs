﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.modules.Medical_Immunization_StudentController
{
    [RoutePrefix("api/ItemRequests")]
    [BasicAuthentication]
    public class ItemRequestsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("GetAllSalesType")]
        public HttpResponseMessage GetAllSalesType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[sale_types]"),
                 new SqlParameter("@tbl_col_name1", "[sal_type],[sal_name]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sal_type]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sal_type = dr["sal_type"].ToString();
                            invsObj.sal_type_name = dr["sal_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("GetAllBanks")]
        public HttpResponseMessage GetAllBanks()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[banks]"),
                 new SqlParameter("@tbl_col_name1", "[bk_code],[bk_name]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[bk_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.bk_code = dr["bk_code"].ToString();
                            invsObj.bk_name = dr["bk_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("GetAllDocumentType")]
        public HttpResponseMessage GetAllDocumentType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllDocumentType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllDocumentType"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[document_types]"),
                 new SqlParameter("@tbl_col_name1", "[dt_code],[dt_desc]"),
                 new SqlParameter("@tbl_cond", "dt_ind='A'"),
                 //new SqlParameter("@tbl_ordby", "[sal_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.dt_code = dr["dt_code"].ToString();
                            invsObj.dt_desc = dr["dt_desc"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetAllSupplierGroupName")]
        public HttpResponseMessage GetAllSupplierGroupName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSupplierGroupName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSupplierGroupName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[supplier_groups]"),
                 new SqlParameter("@tbl_col_name1", "[sg_name],[sg_desc]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sg_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.name = dr["sg_desc"].ToString();
                            invsObj.code = dr["sg_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_supplier_code")]
        public HttpResponseMessage get_supplier_code()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSupplierGroupName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSupplierGroupName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[suppliers]"),
                 new SqlParameter("@tbl_col_name1", "sup_code,sup_name"),
               //  new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "sup_name")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();

                            invsObj.name = dr["sup_name"].ToString();
                            invsObj.code = dr["sup_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_Categories")]
        public HttpResponseMessage get_Categories()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Invs021_get_Categories(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Invs021_get_Categories"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_master",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "X"),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.category_name = dr["pc_desc"].ToString();
                            invsObj.category_code = dr["pc_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_SubCategories")]
        public HttpResponseMessage get_SubCategories(string pc_parentcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Invs021_get_Categories(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Invs021_get_Categories"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_master",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@pc_parent_code",pc_parentcode),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.subcategory_name = dr["pc_desc"].ToString();
                            invsObj.subcategory_code = dr["pc_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllItemsInSubCategory")]
        public HttpResponseMessage GetAllItemsInSubCategory(string pc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[item_master]"),
                 new SqlParameter("@tbl_col_name1", "im_inv_no,[im_item_code],[im_desc]"),
                 new SqlParameter("@tbl_cond", "[im_assembly_ind]='N' and pc_code='" + pc_code + "'")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Fetch_ItemDetails_ItemSet")]
        public HttpResponseMessage Fetch_ItemDetails_ItemSet(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'B'),
                 new SqlParameter("@im_inv_no", im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimal component_qty = 0;

                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            {
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            }
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            try
                            {
                                itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            }
                            catch (Exception ex) {};

                            //   list.Add(itsrobj);


                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("invs_itemsearch_query",
                                    new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'c'),
                 new SqlParameter("@im_inv_no",  itsrobj.im_inv_no),
                 new SqlParameter("@im_inv_no_for",  im_inv_no),

                                           
                         });

                                if (dr1.Read())
                                {
                                    component_qty = decimal.Parse(dr1["Quantity"].ToString());

                                }
                                itsrobj.ia_component_qty = component_qty;

                            }


                            list.Add(itsrobj);



                        }
                    }
                    db.Dispose();

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetItemCodeDetails")]
        public HttpResponseMessage GetItemCodeDetails(string item_code, string sg_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "S"),
                 new SqlParameter("@im_item_code", item_code),
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.original_qty = "0";
                            invsObj.im_sell_price = 0;
                            try
                            {
                                invsObj.original_qty = dr["curr_qty"].ToString();
                            }
                            catch (Exception dee)
                            {
                                invsObj.original_qty = "0";
                            }
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetItemCodeLocations")]
        public HttpResponseMessage GetItemCodeLocations(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetItemCodeLocations(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetItemCodeLocations"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "invs.item_locations a inner join invs.locations b on a.loc_code=b.loc_code"),
                 new SqlParameter("@tbl_col_name1", "a.im_inv_no,a.loc_code,a.il_cur_qty,b.loc_name"),
                 new SqlParameter("@tbl_cond", "im_inv_no=" + im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.item_quantity = int.Parse(dr["il_cur_qty"].ToString());
                            invsObj.item_location_name = dr["loc_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllSalesmans")]
        public HttpResponseMessage GetAllSalesmans()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesmans(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSalesmans"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[salesman]"),
                 new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name]"),
                 new SqlParameter("@tbl_cond", "sm_status='A'"),//invs_comp_code=1 and invs_appl_code='Inv035' and invs_appl_form_field='Sales Type'
              
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        public class Invs058_itemNew
        {

            public bool im_assembly_ind { get; set; }

            public string im_inv_no { get; set; }

            public string invs021_sg_name { get; set; }

            public string im_item_code { get; set; }

            public string im_desc { get; set; }

            public string invs021_dep_code { get; set; }

            public string sec_code { get; set; }

            public string invs021_sup_code { get; set; }

            public string im_assembly_ind_s { get; set; }

            public string category_code { get; set; }

            public string subcategory_code { get; set; }
        }
        //Select
        [Route("postgetItemSerch")]
        public HttpResponseMessage postgetItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));
            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                 new SqlParameter("@opr", opr),
                 new SqlParameter("@im_inv_no", itsr.im_inv_no),
                 new SqlParameter("@sg_name", itsr.invs021_sg_name),
                 new SqlParameter("@im_item_code", itsr.im_item_code),
                 new SqlParameter("@im_desc", itsr.im_desc),
                 new SqlParameter("@dep_code", itsr.invs021_dep_code),
                 new SqlParameter("@sec_code", itsr.sec_code),
                 new SqlParameter("@sup_code", itsr.invs021_sup_code),
                 new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                 new SqlParameter("@category_code", itsr.category_code),
                 new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();
                            try
                            {
                                itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();                                
                            }
                            catch(Exception ex) { }



                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_StudentsforSearch")]
        public HttpResponseMessage Get_StudentsforSearch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_StudentsforSearch(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Get_StudentsforSearch"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "P"),
               
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.studentenroll = dr["sims_student_enroll_number"].ToString();
                            invsObj.studentname = dr["studentname"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        public string GetStudentName(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));
            string StudentName = string.Empty;

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@tbl_name", "sims.sims_student"),
                new SqlParameter("@tbl_col_name1", "[sims_student_passport_first_name_en]+' '+[sims_student_passport_middle_name_en]+' '+[sims_student_passport_last_name_en] as StudentName"),
                new SqlParameter("@tbl_cond", "sims_student_enroll_number='" + enrollnum + "'"),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            StudentName = dr["StudentName"].ToString();

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return StudentName;

            }
            return StudentName;
        }

        public List<invs035> GetSalesmanName(string sales_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesmanName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesmanName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@tbl_name", "[invs].[salesman]"),
                new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name]"),
                new SqlParameter("@tbl_cond", "sm_status='A' and sm_user_code='" + sales_code + "'"),//invs_comp_code=1 and invs_appl_code='Inv035' and invs_appl_form_field='Sales Type'
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return list;

            }
            return list;
        }

        [Route("Get_Sale_DocumentsFromSearchResults")]
        public HttpResponseMessage Get_Sale_DocumentsFromSearchResults(invs035 obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Sale_DocumentsFromSearchResults(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Get_Sale_DocumentsFromSearchResults"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                new SqlParameter("@opr", 'B'),
                new SqlParameter("@doc_prov_no", obj.doc_prov_no),
                new SqlParameter("@from_docdate", db.DBYYYYMMDDformat(obj.fromdate)),
                new SqlParameter("@to_docdate", db.DBYYYYMMDDformat(obj.todate)),
                new SqlParameter("@cus_account_no", obj.cus_account_no),
                new SqlParameter("@studentname", obj.studentname),
                new SqlParameter("@sm_code", obj.sm_code),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_prov_no"].ToString()) == false)
                                invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_prov_date"].ToString()) == false)
                            {
                                string dt = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                                invsObj.doc_prov_date = dt;
                            }
                            if (string.IsNullOrEmpty(dr["doc_no"].ToString()) == false)
                                invsObj.doc_no = int.Parse(dr["doc_no"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_date"].ToString()) == false)
                            {
                                string dt1 = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                                invsObj.doc_date = dt1.ToString();
                            }

                            invsObj.search_dtcode = dr["dcode"].ToString();
                            invsObj.dt_code = ": " + dr["dt_code"].ToString();

                            invsObj.dr_code = dr["dr_code"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.search_smcode = dr["sm_code"].ToString();
                            if (!(string.IsNullOrEmpty(invsObj.sm_code)))
                            {
                                List<invs035> lst_obj = new List<invs035>();
                                lst_obj = GetSalesmanName(invsObj.sm_code);
                                foreach (invs035 obj1 in lst_obj)
                                {
                                    invsObj.sm_code = obj1.sm_name;
                                }
                            }

                            invsObj.up_name = dr["up_name"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();
                            invsObj.sal_type_name = ": " + dr["sal_type_name"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            invsObj.doc_status = dr["doc_status"].ToString();
                            invsObj.doc_status_name = dr["doc_status_name"].ToString();
                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            if (invsObj.sal_type == "04")
                            {
                                invsObj.ic_account_no = GetStudentName(invsObj.cus_account_no);
                            }
                            else
                            {
                                invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            }
                            invsObj.doc_special_name = invsObj.ic_account_no;//dr["doc_special_name"].ToString();
                            invsObj.dep_code_caused_by = dr["dep_code_caused_by"].ToString();
                            invsObj.doc_order_ref_no = dr["doc_order_ref_no"].ToString();
                            invsObj.doc_other_charge_desc = dr["doc_other_charge_desc"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_other_charge_amount"].ToString()) == false)
                                invsObj.doc_other_charge_amount = decimal.Parse(dr["doc_other_charge_amount"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_discount_pct"].ToString()) == false)
                                invsObj.doc_discount_pct = decimal.Parse(dr["doc_discount_pct"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_discount_amount"].ToString()) == false)
                                invsObj.doc_discount_amount = decimal.Parse(dr["doc_discount_amount"].ToString());
                            invsObj.doc_delivery_remarks = dr["doc_delivery_remarks"].ToString();
                            invsObj.doc_validity_remarks = dr["doc_validity_remarks"].ToString();
                            invsObj.ow_id = dr["ow_id"].ToString();
                            invsObj.doc_jobcard_dept = dr["doc_jobcard_dept"].ToString();
                            invsObj.doc_jobcard_loc = dr["doc_jobcard_loc"].ToString();
                            invsObj.doc_jobcard_no = dr["doc_jobcard_no"].ToString();
                            invsObj.sd_icc_code = dr["sd_icc_code"].ToString();
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.sec_code = dr["sec_code"].ToString();
                            if (string.IsNullOrEmpty(dr["creation_date"].ToString()) == false)
                                invsObj.creation_date = dr["creation_date"].ToString();
                            invsObj.creation_user = dr["creation_user"].ToString();
                            invsObj.creation_term = dr["creation_term"].ToString();
                            if (string.IsNullOrEmpty(dr["modification_date"].ToString()) == false)
                                invsObj.modification_date = dr["modification_date"].ToString();
                            invsObj.modification_user = dr["modification_user"].ToString();
                            invsObj.modification_term = dr["modification_term"].ToString();
                            invsObj.doc_cheque_no = dr["doc_cheque_no"].ToString();
                            invsObj.doc_cheque_date = dr["doc_cheque_date"].ToString();
                            invsObj.doc_cheque_bank = dr["doc_cheque_bank"].ToString();
                            invsObj.original_qty = "0";
                            if (!string.IsNullOrEmpty(dr["doc_discount_pct"].ToString()))
                            {
                                invsObj.discnt_percentage = decimal.Parse(dr["doc_discount_pct"].ToString());
                            }
                            else
                                invsObj.discnt_percentage = 0;


                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSale_FeeDetails")]
        public HttpResponseMessage GetSale_FeeDetails(string enroll)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<sims043> list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "R"),
                 new SqlParameter("@studentenrollnum", enroll),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_id = dr["id"].ToString();
                            simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                            simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                            simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                            simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                            simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                            simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                            simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                            simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                            simsobj.std_fee_child_paying_amount_temp = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_concession_amount_used = "0";

                            if (dr["installment_mode"].ToString().ToLower() == "y")
                                simsobj.Installment_mode = true;
                            else
                                simsobj.Installment_mode = false;

                            simsobj.Installment_mode_chk = false;
                            simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                            simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetSalesman_code")]
        public HttpResponseMessage GetSalesman_code(string usercd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[salesman]"),
                 new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name]"),
                 new SqlParameter("@tbl_cond", "sm_status='A' and sm_user_code='" + usercd + "'"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetItemsBySupplier")]
        public HttpResponseMessage GetItemsBySupplier(string sup_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[item_master]"),
                 new SqlParameter("@tbl_col_name1", "im_inv_no],[im_desc]"),
                 new SqlParameter("@tbl_cond", "sup_code='" + sup_code + "'"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.code = dr["im_inv_no"].ToString();
                            invsObj.name = dr["im_desc"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Insert_Sale_Documents")]
        public HttpResponseMessage Insert_Sale_Documents(invs035 invsObj)
        {
            string prov_no = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));
                    para.Add(new SqlParameter("@req_type", invsObj.servicevalue));


                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, prov_no);
            }

            return Request.CreateResponse(HttpStatusCode.OK, prov_no);


        }


        [Route("Insert_Sale_Documents_Details")]
        public HttpResponseMessage Insert_Sale_Documents_Details(invs035 invsObj)
        {
            bool result = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                    //if (invsObj.im_inv_no > 0)
                    para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                    if (string.IsNullOrEmpty(invsObj.loc_code))
                        invsObj.loc_code = "01";
                    para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                    para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                    para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                    para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                    para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                    para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                    para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                    para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                    para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                    para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                    para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                    para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                    para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                    para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                    para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                    para.Add(new SqlParameter("@test", invsObj.test));
                    para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                    para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                    para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                    para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                    para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                    para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                    try
                    {
                        string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                        para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                    }
                    catch (Exception dee)
                    { para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                    para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                    SqlDataReader dr = db.ExecuteStoreProcedure("item_requests_details", para);
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }


        [Route("Insert_Sale_Documents_Details1")]
        public HttpResponseMessage Insert_Sale_Documents_Details1(List<invs035> invslst)
        {
            bool result = false;

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'I'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));
                        para.Add(new SqlParameter("@doc_require_date", db.DBYYYYMMDDformat(invsObj.doc_require_date)));

                        try
                        {
                            string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                            para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                        }
                        catch (Exception dee)
                        { para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                        SqlDataReader dr = db.ExecuteStoreProcedure("item_requests_details", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);


        }


        [Route("GetUserProfiles")]
        public HttpResponseMessage GetUserProfiles()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "V"),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.up_num = dr["up_name"].ToString();
                            invsObj.up_name = dr["up_actual_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Insert_User_profiles")]
        public HttpResponseMessage Insert_User_profiles(string name)
        {
            bool result = false;
            string inserted = string.Empty;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'W'));
                    para.Add(new SqlParameter("@up_actual_name", name));

                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request", para);
                    // int r = dr.RecordsAffected;
                    //result = r > 0 ? true : false;

                    if (dr.Read())
                    {
                        inserted = dr["UPNUM"].ToString();
                    }

                    //                    inserted = dr["UPNUM"].ToString();

                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }


        [Route("GetStudentInfo")]
        public HttpResponseMessage GetStudentInfo(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            invs035 invsObj = new invs035();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "Q"),
                new SqlParameter("@studentenrollnum", enrollnum),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            invsObj.grade_name = dr["gradename"].ToString();
                            invsObj.section_name = dr["sectionname"].ToString();
                            invsObj.house_name = dr["housename"].ToString();
                            invsObj.studentname = dr["studentname"].ToString();
                            invsObj.studentenroll = enrollnum;

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);

        }



        [Route("GetAllItemsInSubCategoryNew")]
        public HttpResponseMessage GetAllItemsInSubCategoryNew(string pc_code,string dep_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                    new List<SqlParameter>() 
                    {                     
                        new SqlParameter("@opr", "X"),
                        new SqlParameter("@pc_code", pc_code),
                        new SqlParameter("@dep_code", dep_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            try
                            {
                                invsObj.uom_code = dr["uom_code"].ToString();
                                invsObj.uom_name = dr["uom_name"].ToString();
                            }
                            catch(Exception ex) { };

                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("GetPrintDetails")]
        public HttpResponseMessage GetPrintDetails(string provNo)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "C"),
                 new SqlParameter("@doc_prov_no", provNo),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_remark = dr["doc_remark"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();

                            invsObj.creation_date = dr["creation_date"].ToString();
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_date = dr["doc_prov_date"].ToString();
                            invsObj.doc_date = dr["doc_date"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();

                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();


                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            else
                                invsObj.doc_total_amount = 0;


                            if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                invsObj.dd_physical_qty = decimal.Parse(dr["id_cur_qty"].ToString());
                            else
                                invsObj.dd_physical_qty = 0;


                            //    invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_inv_no"].ToString() + " - " + dr["im_desc"].ToString();
                            //invsObj.im_sell_price = 0;

                            if (string.IsNullOrEmpty(dr["dd_qty"].ToString()) == false)
                                invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            else
                                invsObj.dd_qty = 0;

                            if (string.IsNullOrEmpty(dr["dd_sell_value_final"].ToString()) == false)
                                invsObj.dd_sell_value_final = decimal.Parse(dr["dd_sell_value_final"].ToString());
                            else
                                invsObj.dd_sell_value_final = 0;

                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;



                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("update_salesDocList")]
        public HttpResponseMessage update_salesDocList(string provNo, string amt)
        {
            bool result = false;
            string inserted = string.Empty;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'Y'));
                    para.Add(new SqlParameter("@doc_prov_no", provNo));
                    para.Add(new SqlParameter("@dd_sell_price_final", amt));

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]", para);
                    // int r = dr.RecordsAffected;
                    //result = r > 0 ? true : false;

                    if (dr.Read())
                    {
                        inserted = dr["UPNUM"].ToString();
                    }

                    //                    inserted = dr["UPNUM"].ToString();

                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }


        [Route("Insert_Sale_Documents_Details1_Print")]
        public HttpResponseMessage Insert_Sale_Documents_Details1_Print(List<invs035> invslst)
        {
            bool result = false;

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'X'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));

                        try
                        {
                            string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                            para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                        }
                        catch (Exception dee)
                        { para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                        SqlDataReader dr = db.ExecuteStoreProcedure("item_requests_details", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    // return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);


        }


        [Route("GetExhangeDetails")]
        public HttpResponseMessage GetExhangeDetails(string provNo)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "B"),
                 new SqlParameter("@doc_prov_no", provNo),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_remark = dr["doc_remark"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();

                            invsObj.creation_date = dr["creation_date"].ToString();
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_date = dr["doc_prov_date"].ToString();
                            invsObj.doc_date = dr["doc_date"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();

                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();


                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            else
                                invsObj.doc_total_amount = 0;


                            if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                invsObj.dd_physical_qty = decimal.Parse(dr["id_cur_qty"].ToString());
                            else
                                invsObj.dd_physical_qty = 0;


                            //    invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_inv_no"].ToString() + " - " + dr["im_desc"].ToString();
                            //invsObj.im_sell_price = 0;

                            if (string.IsNullOrEmpty(dr["dd_qty"].ToString()) == false)
                                invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            else
                                invsObj.dd_qty = 0;

                            if (string.IsNullOrEmpty(dr["dd_sell_value_final"].ToString()) == false)
                                invsObj.dd_sell_value_final = decimal.Parse(dr["dd_sell_value_final"].ToString());
                            else
                                invsObj.dd_sell_value_final = 0;

                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;



                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetEmployeeInfo")]
        public HttpResponseMessage GetEmployeeInfo(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            invs035 invsObj = new invs035();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "Y"),
                new SqlParameter("@studentenrollnum", enrollnum),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            //invsObj.grade_name = dr["gradename"].ToString();
                            //invsObj.section_name = dr["sectionname"].ToString();
                            //invsObj.house_name = dr["housename"].ToString();
                            invsObj.studentname = dr["em_name"].ToString();
                            invsObj.studentenroll = enrollnum;

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);

        }



        [Route("GetReceiptDetails")]
        public HttpResponseMessage GetReceiptDetails(string prov_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            ReceiptResponse rr = new ReceiptResponse();
            rr.receipt_lines = new List<ReceiptLine>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr0 = db.ExecuteStoreProcedure("sims.[sale_document_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@doc_prov_no", prov_no),
                 });

                    while (dr0.Read())
                    {
                        rr.enroll_number = dr0["enroll_number"].ToString();
                        rr.student_name = dr0["student_name"].ToString();
                        rr.grade = dr0["grade"].ToString();
                        rr.receipt_date = dr0["receipt_date"].ToString();
                        rr.receipt_number = dr0["receipt_number"].ToString();
                        rr.parent_name = dr0["parent_name"].ToString();
                        rr.receipt_remark = dr0["receipt_remark"].ToString();
                        rr.receipt_grand_total = dr0["receipt_grand_total"].ToString();
                        rr.payment_mode = dr0["payment_mode"].ToString();
                        rr.received_by = dr0["received_by"].ToString();
                        rr.amt_in_words = dr0["amt_in_words"].ToString();


                    }
                    dr0.NextResult();

                    ReceiptLine rl = new ReceiptLine();
                    float total = 0;
                    while (dr0.Read())
                    {
                        rl = new ReceiptLine();
                        rl.line_type = "C";
                        rl.line_sr_no = "";
                        rl.line_particulars = dr0["pc_desc"].ToString();
                        rl.line_remark = "";
                        rl.line_price = "";
                        rl.line_quantity = "";
                        rl.line_amount = "";

                        rr.receipt_lines.Add(rl);

                        string pc_code = dr0["pc_code"].ToString();
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();
                            SqlDataReader dr1 = db2.ExecuteStoreProcedure("sims.[sale_document_receipt_proc]",
                                new List<SqlParameter>() 
                         { 
                          new SqlParameter("@opr", "A"),
                          new SqlParameter("@doc_prov_no", prov_no),
                          new SqlParameter("@pc_code", pc_code)

                 });

                            if (dr1.HasRows)
                            {
                                ReceiptLine rl1 = new ReceiptLine();
                                var i = 1;
                                while (dr1.Read())
                                {
                                    rl1 = new ReceiptLine();
                                    rl1.line_type = "I";
                                    rl1.line_sr_no = i.ToString();


                                    rl1.im_item_code = dr1["im_item_code"].ToString();

                                    rl1.line_particulars = dr1["im_desc"].ToString();
                                    rl1.line_remark = dr1["doc_remark"].ToString();
                                    rl1.line_price = dr1["dd_sell_price"].ToString();
                                    rl1.line_quantity = dr1["dd_qty"].ToString();
                                    rl1.line_amount = dr1["dd_sell_value_final"].ToString();

                                    rr.receipt_lines.Add(rl1);
                                    i = i + 1;
                                    total = total + float.Parse(rl1.line_amount);
                                }
                            }

                            dr1.Close();
                            // DB2.Close();
                        }
                        rl = new ReceiptLine();
                        rl.line_type = "S";
                        rl.line_sr_no = "";
                        rl.line_particulars = "Subtotal:";
                        rl.line_remark = "";
                        rl.line_price = "";
                        rl.line_quantity = "";
                        rl.line_amount = String.Format("{0:0.00}", total);

                        rr.receipt_lines.Add(rl);
                    }


                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, rr);

        }

        public class ReceiptResponse
        {
            public List<ReceiptLine> receipt_lines { get; set; }


            public string enroll_number { get; set; }
            public string student_name { get; set; }
            public string grade { get; set; }
            public string receipt_date { get; set; }
            public string receipt_number { get; set; }
            public string parent_name { get; set; }
            public string receipt_remark { get; set; }
            public string receipt_grand_total { get; set; }
            public string payment_mode { get; set; }
            public string received_by { get; set; }


            public string amt_in_words { get; set; }
        }

        public class ReceiptLine
        {
            public string line_type { get; set; }
            public string line_sr_no { get; set; }
            public string line_particulars { get; set; }
            public string line_remark { get; set; }
            public string line_price { get; set; }
            public string line_quantity { get; set; }
            public string line_amount { get; set; }
            public string im_item_code { get; set; }
        }

        [Route("getRequestDetailsforapprove")]
        public HttpResponseMessage getRequestDetailsforapprove(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Invs044_requestdetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("item_request",
                        new List<SqlParameter>()
                             {
                                //new SqlParameter("@opr", "A6"),
                                new SqlParameter("@opr", "B3"),
                                new SqlParameter("@req_no", sf.ord_no),                                
                                new SqlParameter("@rd_from_date",db.DBYYYYMMDDformat(sf.rd_from_date)),
                                new SqlParameter("@rd_up_date", db.DBYYYYMMDDformat(sf.rd_up_date)),
                                new SqlParameter("@rd_item_desc", sf.rd_item_desc),                               
                                new SqlParameter("@req_range_no", sf.req_range_no),
                                new SqlParameter("@login_user", sf.loginuser),
                                new SqlParameter("@req_status", sf.request_issuestatus),
                                new SqlParameter("@req_type", sf.request_mode_code),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getRequestDetailsforapprovePeriodWise")]
        public HttpResponseMessage getRequestDetailsforapprovePeriodWise(string description,string month)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("item_request",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "Z"),
                                new SqlParameter("@im_desc", description),
                                new SqlParameter("@month", month)
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateRequestDetailsforapprove")]
        public HttpResponseMessage UpdateRequestDetailsforapprove(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "B5";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("item_request", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("item_request",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B4"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateRequestDetailsforreject")]
        public HttpResponseMessage UpdateRequestDetailsforreject(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "B5";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("item_request", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("item_request",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","B6"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateRequestDetailstoissue")]
        public HttpResponseMessage UpdateRequestDetailstoissue(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "B5";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("item_request", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("item_request",
                               new List<SqlParameter>()
                               {
                                //new SqlParameter("@opr","F1"),
                                new SqlParameter("@opr","C3"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //For Service
        [Route("getServiceSearch")]
        public HttpResponseMessage getServiceSearch(string im_no, string des_ser, string dep_name)
        {

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "C2"),
                                new SqlParameter("@im_inv_no",im_no),
                                new SqlParameter("@im_desc", des_ser),
                                new SqlParameter("@dep_code",dep_name),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_name"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            try
                            {
                                simsobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            }
                            catch(Exception e) { };

                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetServiceCodeDetails")]
        public HttpResponseMessage GetServiceCodeDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetServiceCodeDetails(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetServiceCodeDetails"));

            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         {                            
                            new SqlParameter("@opr", "BB"),
                                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetServiceDetails")]
        public HttpResponseMessage GetServiceDetails(string item_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetServiceDetails(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetServiceDetails"));

            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>() 
                         { 
                           
                            new SqlParameter("@opr", "C1"),
                            new SqlParameter("@im_sm_no", item_code),
                                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetItemRequest")]
        public HttpResponseMessage GetItemRequest(string login_user)
        {
            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_request",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "IT"),
                              new SqlParameter("@login_user",login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();                            
                            invsObj.creation_user = dr["creation_user"].ToString();
                            mod_list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetApprovedItemRequest")]
        public HttpResponseMessage GetApprovedItemRequest(string doc_prov_no)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
               // Invs044_requestdetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs044_requestdetails>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("item_request",
                        new List<SqlParameter>()
                             {                               
                                new SqlParameter("@opr", "GA"),
                                new SqlParameter("@req_no",doc_prov_no)                                
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

    }
}