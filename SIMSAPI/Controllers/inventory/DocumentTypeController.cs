﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.DocumentTypeController
{
    [RoutePrefix("api/doctype")]
    [BasicAuthentication]
    public class DocumentTypeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllDocumentTypes")]
        public HttpResponseMessage getAllDocumentTypes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDocumentTypes(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllDocumentTypes"));

            List<Inv071> para_list = new List<Inv071>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_document_types_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv071 simsobj = new Inv071();
                            simsobj.doc_type_code = dr["dt_code"].ToString();
                            simsobj.doc_type_desc = dr["dt_desc"].ToString();
                            simsobj.doc_type_status = dr["dt_ind"].Equals("A") ? true : false;
                            para_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, para_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, para_list);
        }

        [Route("CUDDocumentTypes")]
        public HttpResponseMessage CUDDocumentTypes(List<Inv071> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDocumentTypes(),PARAMETERS :: OPR {2}";
            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv071 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_document_types_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@dt_code", simsobj.doc_type_code),
                                new SqlParameter("@dt_desc", simsobj.doc_type_desc),
                                 new SqlParameter("@dt_ind", simsobj.doc_type_status.Equals(true)?"A":"I"),
                                
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
