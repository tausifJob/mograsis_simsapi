﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.inventoryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/common/ShipmentDetails")]

    public class ShipmentDetailsController : ApiController
    {
        string shiftment_no = "";
        #region ServiceSupplierMapping

        [Route("getServieSuppliersDetails")]
        public HttpResponseMessage getServieSuppliersDetails()
        {
            Inv021_Sevice obj = new Inv021_Sevice();
            List<Inv021_Sevice> ItemMaster_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_service_suppliers",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "S")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice simobj = new Inv021_Sevice();

                            simobj.im_inv_no = dr["im_sm_no"].ToString();
                            simobj.im_desc = dr["im_service_desc"].ToString();
                            simobj.sup_code = dr["sup_code"].ToString();
                            simobj.sup_name = dr["sup_name"].ToString();
                            simobj.im_supl_catalog_price = dr["is_price"].ToString();
                            simobj.im_status_ind = false;//dr["is_cur_code"].ToString().Equals("Y") ? true : false;
                            simobj.im_creation_date = db.UIDDMMYYYYformat(dr["is_price_date"].ToString());
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }


        [Route("getServieDesc")]
        public HttpResponseMessage getServieDesc()
        {
            List<Inv021_Sevice> Product_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_service_suppliers",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "P"),
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.im_inv_no = dr["im_sm_no"].ToString();
                            obj.im_desc = dr["im_service_desc"].ToString();

                            Product_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Product_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Product_list);
        }


        [Route("ServieSuppliersCUD")]
        public HttpResponseMessage ServieSuppliersCUD(List<Inv021_Sevice> obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            string st = string.Empty;
            try
            {
                foreach (Inv021_Sevice simsobj in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        if (simsobj.im_inv_no == "" || simsobj.im_inv_no == "undefined")
                            simsobj.im_inv_no = null;
                        if (simsobj.sup_code == "" || simsobj.sup_code == "undefined")
                            simsobj.sup_code = null;
                        if (simsobj.sg_name == "" || simsobj.sg_name == "undefined")
                            simsobj.sg_name = null;
                        if (simsobj.im_creation_date == "" || simsobj.im_creation_date == "undefined")
                            simsobj.im_creation_date = null;
                        if (simsobj.cur_code == "" || simsobj.cur_code == "undefined")
                            simsobj.cur_code = null;

                        SqlDataReader dr = db.ExecuteStoreProcedure("invs_service_suppliers",
                        new List<SqlParameter>()
                       {
                          new SqlParameter("@opr","I"),
                          new SqlParameter("@im_sm_no",simsobj.im_inv_no),
                          new SqlParameter("@sup_code",simsobj.sup_code),
                          new SqlParameter("@is_price", simsobj.im_supl_catalog_price),
                          new SqlParameter("@is_price_date", db.DBYYYYMMDDformat(simsobj.im_creation_date)),
                          new SqlParameter("@is_cur_code", simsobj.cur_code),
                          new SqlParameter("@cur_code", simsobj.cur_code)
                       });
                        if (dr.Read())
                        {
                            st = dr["Result"].ToString();
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, st);
        }

        [Route("DeleteServieSuppliers")]
        public HttpResponseMessage DeleteServieSuppliers(List<Inv021_Sevice> obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            string st = string.Empty;
            try
            {
                foreach (Inv021_Sevice simsobj in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("invs_service_suppliers",
                        new List<SqlParameter>()
                       {
                          new SqlParameter("@opr","D"),
                          new SqlParameter("@im_sm_no",simsobj.im_inv_no),
                          new SqlParameter("@sup_code",simsobj.sup_code)
                          //new SqlParameter("@is_price_date", db.DBYYYYMMDDformat(simsobj.im_creation_date)),
                          //new SqlParameter("@is_cur_code", string.Empty),
                          //new SqlParameter("@is_price", simsobj.im_supl_catalog_price)
                       });
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, st);
        }
        #endregion


        #region ServieItem_Master
        [Route("getServieDepartments")]
        public HttpResponseMessage getServieDepartments()
        {
            List<Inv021_Sevice> Dept_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "R")

                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();

                            Dept_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Dept_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Dept_list);
        }


        [Route("getServieSupplier")]
        public HttpResponseMessage getServieSupplier()
        {
            List<Inv021_Sevice> Supplier_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "T")

                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.cur_code = dr["cur_code"].ToString();

                            Supplier_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Supplier_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Supplier_list);
        }


        [Route("getServieSupplierGroupNamewithouitem")]
        public HttpResponseMessage getServieSupplierGroupNamewithouitem()
        {
            List<Inv021_Sevice> Supplieritem_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "K")

                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.sg_desc = dr["sg_desc"].ToString();
                            obj.sg_name = dr["sg_name"].ToString();

                            Supplieritem_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Supplieritem_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Supplieritem_list);
        }

        [Route("getServieSupplierGroupName")]
        public HttpResponseMessage getServieSupplierGroupName()
        {
            List<Inv021_Sevice> SupplierGrp_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "A")

                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.sg_desc = dr["sg_desc"].ToString();
                            obj.sg_name = dr["sg_name"].ToString();

                            SupplierGrp_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
        }


        [Route("getServieSectionName")]
        public HttpResponseMessage getServieSectionName(string dept_code)
        {
            List<Inv021_Sevice> Section_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@dep_code", dept_code)
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.sec_name = dr["sec_name"].ToString();
                            obj.sec_code = dr["sec_code"].ToString();

                            Section_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Section_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Section_list);
        }

        [Route("getServieProductDesc")]
        public HttpResponseMessage getServieProductDesc()
        {
            List<Inv021_Sevice> Product_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "C"),
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.pc_desc = dr["pc_desc"].ToString();
                            obj.pc_code = dr["pc_code"].ToString();

                            Product_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Product_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Product_list);
        }

        [Route("getServieUnitOfMeasurement")]
        public HttpResponseMessage getServieUnitOfMeasurement()
        {
            List<Inv021_Sevice> Measurement_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "E"),
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.uom_name = dr["uom_name"].ToString();
                            obj.uom_code = dr["uom_code"].ToString();

                            Measurement_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
        }

        [Route("getServieTradeCategory")]
        public HttpResponseMessage getServieTradeCategory()
        {
            List<Inv021_Sevice> Measurement_list = new List<Inv021_Sevice>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "F"),
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice obj = new Inv021_Sevice();

                            obj.trade_cat_name = dr["invs_appl_form_field_value1"].ToString();
                            obj.im_trade_cat = dr["invs_appl_parameter"].ToString();

                            Measurement_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
        }

        [Route("getServieItemMasterDetails")]
        public HttpResponseMessage getServieItemMasterDetails(string data)
        {
            Inv021_Sevice obj = new Inv021_Sevice();
            List<Inv021_Sevice> ItemMaster_list = new List<Inv021_Sevice>();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Inv021_Sevice>(data);
            }
            if (obj.im_inv_no == "undefined" || obj.im_inv_no == "")
            {
                obj.im_inv_no = null;
            }
            if (obj.im_desc == "undefined" || obj.im_desc == "")
            {
                obj.im_desc = null;
            }
            if (obj.im_item_code == "undefined" || obj.im_item_code == "")
            {
                obj.im_item_code = null;
            }
            if (obj.dep_code == "undefined" || obj.dep_code == "")
            {
                obj.dep_code = null;
            }
            if (obj.sec_code == "undefined" || obj.sec_code == "")
            {
                obj.sec_code = null;
            }
            if (obj.sup_code == "undefined" || obj.sup_code == "")
            {
                obj.sup_code = null;
            }
            if (obj.pc_code == "undefined" || obj.pc_code == "")
            {
                obj.pc_code = null;
            }
            if (obj.sg_name == "undefined" || obj.sg_name == "")
            {
                obj.sg_name = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                        {

                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@im_inv_no",obj.im_inv_no),
                            new SqlParameter("@im_desc",obj.im_desc),
                            new SqlParameter("@im_item_code",obj.im_item_code),
                            new SqlParameter("@dep_code", obj.dep_code),
                            new SqlParameter("@sec_code", obj.sec_code),
                            new SqlParameter("@sup_code", obj.sup_code),
                            new SqlParameter("@pc_code", obj.pc_code),
                            new SqlParameter("@sg_name", obj.sg_name)
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021_Sevice simobj = new Inv021_Sevice();

                            simobj.im_inv_no = dr["im_inv_no"].ToString();

                            simobj.sg_name = dr["sg_name"].ToString();
                            simobj.sg_desc = dr["sg_desc"].ToString();

                            simobj.im_item_code = dr["im_item_code"].ToString();
                            simobj.im_desc = dr["im_desc"].ToString();

                            simobj.dep_code = dr["dep_code"].ToString();
                            simobj.dep_name = dr["dep_name"].ToString();

                            simobj.sup_code = dr["sup_code"].ToString();
                            simobj.sup_name = dr["sup_name"].ToString();

                            simobj.im_supl_catalog_price = dr["im_supl_catalog_price"].ToString();
                            simobj.im_supl_price_date = db.UIDDMMYYYYformat(dr["im_supl_price_date"].ToString());
                            simobj.im_supl_buying_price = dr["im_supl_buying_price"].ToString();
                            simobj.im_supl_buying_price_old = dr["im_supl_buying_price_old"].ToString();
                            simobj.im_malc_rate_old = dr["im_malc_rate_old"].ToString();
                            simobj.im_approximate_price = dr["im_approximate_price"].ToString();
                            simobj.im_estimated_price = dr["im_estimated_price"].ToString();
                            simobj.im_creation_date = db.UIDDMMYYYYformat(dr["im_creation_date"].ToString());
                            simobj.im_trade_cat = dr["im_trade_cat"].ToString();
                            simobj.trade_cat_name = dr["trade_cat_name"].ToString();
                            simobj.im_proprietary_flag = dr["im_proprietary_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_status_ind = dr["im_status_ind"].ToString().Equals("Y") ? true : false;
                            simobj.uom_code = dr["uom_code"].ToString();
                            simobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            


                            simobj.compname = dr["compname"].ToString();
                            simobj.comp_code = dr["compcode"].ToString();
                            simobj.fin_year = dr["st_fin_year"].ToString();
                            simobj.cost_account = dr["cost_code"].ToString();
                            simobj.cost_accountname = dr["cost_accno"].ToString();
                            simobj.dis_account = dr["discount_code"].ToString();
                            simobj.dis_accountname = dr["discount_accno"].ToString();
                            simobj.dis_accountname = dr["discount_accno"].ToString();

                            ItemMaster_list.Add(simobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("ServieItemMasterDetailsCUD")]
        public HttpResponseMessage ServieItemMasterDetailsCUD(List<Inv021_Sevice> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv021_Sevice simsobj in data)
                    {


                        if (simsobj.sg_name == "" || simsobj.sg_name == "undefined")
                            simsobj.sg_name = null;
                        if (simsobj.im_model_name == "" || simsobj.im_model_name == "undefined")
                            simsobj.im_model_name = null;
                        if (simsobj.im_supl_catalog_price == "" || simsobj.im_supl_catalog_price == "undefined")
                            simsobj.im_supl_catalog_price = null;
                        if (simsobj.im_supl_min_qty == "" || simsobj.im_supl_min_qty == "undefined")
                            simsobj.im_supl_min_qty = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_supl_buying_price == "" || simsobj.im_supl_buying_price == "undefined")
                            simsobj.im_supl_buying_price = null;
                        if (simsobj.im_min_level == "" || simsobj.im_min_level == "undefined")
                            simsobj.im_min_level = null;
                        if (simsobj.im_max_level == "" || simsobj.im_max_level == "undefined")
                            simsobj.im_max_level = null;
                        if (simsobj.im_sell_price == "" || simsobj.im_sell_price == "undefined")
                            simsobj.im_sell_price = null;
                        if (simsobj.im_sell_price_old == "" || simsobj.im_sell_price_old == "undefined")
                            simsobj.im_sell_price_old = null;
                        if (simsobj.im_sellprice_special == "" || simsobj.im_sellprice_special == "undefined")
                            simsobj.im_sellprice_special = null;

                        if (simsobj.im_approximate_price == "" || simsobj.im_approximate_price == "undefined")
                            simsobj.im_approximate_price = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;

                        if (simsobj.im_sell_min_qty == "" || simsobj.im_sell_min_qty == "undefined")
                            simsobj.im_sell_min_qty = null;
                        if (simsobj.im_estimated_price == "" || simsobj.im_estimated_price == "undefined")
                            simsobj.im_estimated_price = null;
                        if (simsobj.im_no_of_packing == "" || simsobj.im_no_of_packing == "undefined")
                            simsobj.im_no_of_packing = null;


                        if (simsobj.im_status_date == "" || simsobj.im_status_date == "undefined")
                            simsobj.im_status_date = null;
                        if (simsobj.im_last_receipt_date == "" || simsobj.im_last_receipt_date == "undefined")
                            simsobj.im_last_receipt_date = null;
                        if (simsobj.im_stock_check_date == "" || simsobj.im_stock_check_date == "undefined")
                            simsobj.im_stock_check_date = null;
                        if (simsobj.im_creation_date == "" || simsobj.im_creation_date == "undefined")
                            simsobj.im_creation_date = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_last_issue_date == "" || simsobj.im_last_issue_date == "undefined")
                            simsobj.im_last_issue_date = null;


                        if (simsobj.im_trade_cat == "" || simsobj.im_trade_cat == "undefined")
                            simsobj.im_trade_cat = null;
                        if (simsobj.im_average_month_demand == "" || simsobj.im_average_month_demand == "undefined")
                            simsobj.im_average_month_demand = null;
                        if (simsobj.im_manufact_serial_no == "" || simsobj.im_manufact_serial_no == "undefined")
                            simsobj.im_manufact_serial_no = null;
                        if (simsobj.im_last_supp_cur == "" || simsobj.im_last_supp_cur == "undefined")
                            simsobj.im_last_supp_cur = null;
                        if (simsobj.im_last_supp_code == "" || simsobj.im_last_supp_code == "undefined")
                            simsobj.im_last_supp_code = null;


                        if (simsobj.im_supl_buying_price_old == "" || simsobj.im_supl_buying_price_old == "undefined")
                            simsobj.im_supl_buying_price_old = null;
                        if (simsobj.im_economic_order_qty == "" || simsobj.im_economic_order_qty == "undefined")
                            simsobj.im_economic_order_qty = null;
                        if (simsobj.im_rso_qty == "" || simsobj.im_rso_qty == "undefined")
                            simsobj.im_rso_qty = null;
                        if (simsobj.im_malc_rate_old == "" || simsobj.im_malc_rate_old == "undefined")
                            simsobj.im_malc_rate_old = null;
                        if (simsobj.im_approximate_lead_time == "" || simsobj.im_approximate_lead_time == "undefined")
                            simsobj.im_approximate_lead_time = null;
                        if (simsobj.uom_code == "" || simsobj.uom_code == "undefined")
                            simsobj.uom_code = null;
                        if (simsobj.im_item_vat_percentage == "" || simsobj.im_item_vat_percentage == "undefined")
                            simsobj.im_item_vat_percentage = null;


                        //int ins = db.ExecuteStoreProcedureforInsert("[invs].[item_master_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[service_master_proc]",
                        new List<SqlParameter>()
                       {

                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                          new SqlParameter("@sg_name",simsobj.sg_name),
                          new SqlParameter("@im_item_code", simsobj.im_item_code),
                          new SqlParameter("@im_desc", simsobj.im_desc),
                          new SqlParameter("@dep_code",simsobj.dep_code),
                          new SqlParameter("@sup_code", simsobj.sup_code),
                          new SqlParameter("@im_supl_catalog_price", simsobj.im_supl_catalog_price),
                          new SqlParameter("@im_supl_price_date", db.DBYYYYMMDDformat(simsobj.im_supl_price_date)),
                          new SqlParameter("@im_supl_buying_price", simsobj.im_supl_buying_price),
                          new SqlParameter("@im_supl_buying_price_old", simsobj.im_supl_buying_price_old),
                          new SqlParameter("@im_malc_rate_old",simsobj.im_malc_rate_old),
                          new SqlParameter("@im_approximate_price",simsobj.im_approximate_price),
                          new SqlParameter("@im_estimated_price", simsobj.im_estimated_price),
                          new SqlParameter("@im_creation_date", db.DBYYYYMMDDformat(simsobj.im_creation_date)),
                          new SqlParameter("@im_trade_cat",simsobj.im_trade_cat),
                          new SqlParameter("@im_proprietary_flag",simsobj.im_proprietary_flag==true?"Y":"N"),
                          new SqlParameter("@im_status_ind", simsobj.im_status_ind==true?"Y":"N"),
                          new SqlParameter("@uom_code",simsobj.uom_code),
                          new SqlParameter("@im_item_vat_percentage",simsobj.im_item_vat_percentage),

                          /*Service Account Details Parameter*/

                          new SqlParameter("@comp_code",simsobj.comp_code),
                          new SqlParameter("@sims_finance_year",simsobj.fin_year),
                          new SqlParameter("@glma_acct_code11",simsobj.cost_account),
                          new SqlParameter("@glma_acct_code",simsobj.dis_account)
                       });

                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}
                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
        #endregion

        #region Shipment

        [Route("Get_Shipment_report")]
        public HttpResponseMessage Get_Shipment_report()
        {
            string report_name = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "RT")
                        });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        report_name = dr["report_name"].ToString();
                        return Request.CreateResponse(HttpStatusCode.OK, report_name);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, report_name);
            }
            return Request.CreateResponse(HttpStatusCode.OK, report_name);
        }


        [Route("Get_Search_Order")]
        public HttpResponseMessage Get_Search_Order(string search, string fromDate, string toDate)
        {
            search = !string.IsNullOrEmpty(search) ? search : null;
            fromDate = !string.IsNullOrEmpty(fromDate) ? fromDate : null;
            toDate = !string.IsNullOrEmpty(toDate) ? toDate : null;

            List<Invs051> group_list = new List<Invs051>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@search", search),
                            new SqlParameter("@order_from_date", db.DBYYYYMMDDformat(fromDate)),
                            new SqlParameter("@order_to_date", db.DBYYYYMMDDformat(toDate)),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051 invsobj = new Invs051();
                            invsobj.Invs051_ord_no = dr["ord_no"].ToString();
                            invsobj.Invs051_dep_code = dr["dep_code"].ToString();
                            invsobj.Invs051_dep_name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                            invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                            invsobj.Invs051_sup_name = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                            invsobj.Invs051_fa_code = dr["fa_code"].ToString();
                            invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                            invsobj.Invs051_dm_name = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                            invsobj.Invs051_trtCode = dr["trt_code"].ToString();
                            invsobj.Invs051_trt_desc = dr["trt_code"].ToString() + "-" + dr["trt_desc"].ToString();
                            invsobj.Invs051_pm_code = dr["pm_code"].ToString();
                            invsobj.Invs051_pm_desc = dr["pm_code"].ToString() + "-" + dr["pm_desc"].ToString();
                            invsobj.Invs051_lc_no = dr["lc_no"].ToString();
                            invsobj.Invs051_Ord_Date = db.UIDDMMYYYYformat(dr["ord_date"].ToString());
                            group_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, group_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("Get_Search_Shipment")]
        public HttpResponseMessage Get_Search_Shipment(string search, string fromDate, string toDate)
        {
            search = !string.IsNullOrEmpty(search) ? search : null;
            fromDate = !string.IsNullOrEmpty(fromDate) ? fromDate : null;
            toDate = !string.IsNullOrEmpty(toDate) ? toDate : null;

            List<Invs051> group_list = new List<Invs051>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "L"),
                            new SqlParameter("@search", search),
                            new SqlParameter("@order_from_date", db.DBYYYYMMDDformat(fromDate)),
                            new SqlParameter("@order_to_date", db.DBYYYYMMDDformat(toDate)),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051 invsobj = new Invs051();
                            invsobj.Invs051_sr_shipment_no = dr["sr_shipment_no"].ToString();
                            invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                            invsobj.Invs051_sup_name = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                            invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                            invsobj.Invs051_dm_name = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                            invsobj.Invs051_sr_party_ref = dr["sr_party_ref"].ToString();
                            invsobj.Invs051_sr_bill_of_lading_date = db.UIDDMMYYYYformat(dr["sr_bill_of_lading_date"].ToString());
                            invsobj.Invs051_sr_remarks = dr["sr_remarks"].ToString();
                            group_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, group_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, group_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        [Route("GetShipmentId")]
        public HttpResponseMessage GetShipmentId()
        {
            List<Invs051> group_list = new List<Invs051>();
            string id = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "N"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            id = dr["comn_number_sequence"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, id);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            return Request.CreateResponse(HttpStatusCode.OK, id);
        }

        [Route("Get_DeliveryMode")]
        public HttpResponseMessage Get_DeliveryMode()
        {
            List<Invs051> mod_list = new List<Invs051>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "V"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051 Invsobj = new Invs051();
                            Invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                            Invsobj.Invs051_dm_name = dr["dm_name"].ToString();
                            Invsobj.deliveryMode = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                            mod_list.Add(Invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Get_SupplierCode")]
        public HttpResponseMessage Get_SupplierCode()
        {
            List<Invs051> mod_list = new List<Invs051>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "W"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051 Invsobj = new Invs051();
                            Invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                            Invsobj.Invs051_sup_name = dr["sup_name"].ToString();
                            Invsobj.supCode = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                            mod_list.Add(Invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getSearch_Order_Details")]
        public HttpResponseMessage getSearch_Order_Details(string ob)
        {
            Invs051 objSearchItem = new Invs051();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //  foreach (Invs051 objSearchItem in data)
                    // {
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "E"),
                             new SqlParameter("@ord_no", ob)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051_OrderDetails invsobj = new Invs051_OrderDetails();
                            invsobj.Invs051_sr_shipment_no = string.Empty;
                            invsobj.old_sr_shipment_no = string.Empty;
                            invsobj.ord_no = dr["ord_no"].ToString();
                            invsobj.od_line_no = dr["od_line_no"].ToString();
                            invsobj.im_item_code = dr["im_item_code"].ToString();
                            invsobj.im_desc = dr["im_desc"].ToString();
                            invsobj.im_inv_no = dr["im_inv_no"].ToString();
                            invsobj.uom_code = dr["uom_code"].ToString();
                            invsobj.od_sup_code = dr["sup_code"].ToString();
                            invsobj.od_sup_name = dr["sup_name"].ToString();
                            invsobj.od_order_qty = dr["od_order_qty"].ToString();
                            //if (!string.IsNullOrEmpty(dr["od_shipped_qty"].ToString()))
                            invsobj.od_shipped_qty = dr["od_shipped_qty"].ToString();

                            invsobj.upd = false;
                            objSearchItem.OrderDetails.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
                    }
                    //}
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
        }

        [Route("Search_Shipment_Details")]
        public HttpResponseMessage Search_Shipment_Details(List<Invs051> data)
        {
            List<Invs051> mod_list = new List<Invs051>();
            Invs051 ob = new Invs051();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs051 objSearchItem in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@sr_shipment_no", objSearchItem.Invs051_sr_shipment_no)
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Invs051_shipmentOrder invsobj = new Invs051_shipmentOrder();
                                invsobj.old_sr_shipment_no = string.Empty;
                                invsobj.Invs051_sr_shipment_no = dr["sr_shipment_no"].ToString();
                                invsobj.ord_no = dr["ord_no"].ToString();
                                invsobj.od_line_no = dr["od_line_no"].ToString();
                                invsobj.od_order_qty = dr["od_order_qty"].ToString();
                                invsobj.od_shipped_qty = dr["so_shipped_qty"].ToString();
                                invsobj.im_item_code = dr["im_item_code"].ToString();
                                invsobj.im_desc = dr["im_desc"].ToString();
                                invsobj.uom_code = dr["uom_code"].ToString();
                                invsobj.od_sup_code = dr["sup_code"].ToString();
                                invsobj.od_sup_name = dr["sup_name"].ToString();
                                invsobj.upd = true;
                                ob.ShipmentOrder.Add(invsobj);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, ob);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ob);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ob);
        }




        [Route("CUDInsertShipment_regiser")]
        public HttpResponseMessage CUDInsertShipment_regiser(string editOrNew, List<Invs051> shiftList)
        {
            // bool inserted = true;
            int reg = 2;

            List<Invs051_shipmentOrder> shift_order = new List<Invs051_shipmentOrder>();
            if (editOrNew.ToLower() == "e")
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        foreach (Invs051 Invsobj in shiftList)
                        {
                            shiftment_no = Invsobj.Invs051_sr_shipment_no;
                            shift_order = Invsobj.ShipmentOrder;

                            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                                new List<SqlParameter>()
                       {
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@sr_shipment_no",shiftment_no),
                            new SqlParameter("@sup_code", Invsobj.Invs051_sup_code),
                            new SqlParameter("@dm_code", Invsobj.Invs051_dm_code),
                            new SqlParameter("@sr_bill_of_lading", "yyy"),
                            new SqlParameter("@sr_house_bill_of_lading", null),
                            new SqlParameter("@sr_bill_of_lading_date",db.DBYYYYMMDDformat(Invsobj.Invs051_sr_bill_of_lading_date)),
                            new SqlParameter("@sr_shipment_port", "123"),
                            new SqlParameter("@sr_port_via", null),
                            new SqlParameter("@sr_expected_departure", null),
                            new SqlParameter("@sr_expected_arrival", null),
                            new SqlParameter("@sr_vessel_name", "ABC"),
                            new SqlParameter("@sr_voyage_no", null),
                            new SqlParameter("@sr_container_no", null),
                            new SqlParameter("@sr_consignment_weight", "50"),
                            new SqlParameter("@sr_consignment_size", "a12"),
                            new SqlParameter("@sr_party_ref", Invsobj.Invs051_sr_party_ref),
                            new SqlParameter("@sr_party_ref_date", null),
                            new SqlParameter("@sr_remarks", Invsobj.Invs051_sr_remarks),
                       });

                            while (dr.Read())
                            {
                                string cnt = dr["shift_cnt"].ToString();
                                if (cnt == "3")
                                {
                                    reg = CUDUpdateShipment_details(shift_order);  ////Insert Shiftment Register
                                }
                                if (cnt == "4")
                                {
                                    reg = 4;      ////Update Shiftment Register     
                                    reg = CUDUpdateShipment_details(shift_order);
                                }

                            }
                            dr.Close();
                        }
                        //db.Dispose();
                    }

                }
                catch (Exception x)
                {

                }
            }
            if (editOrNew.ToLower() == "n")
            {
                List<string> suppliers = shiftList[0].ShipmentOrder.Select(x => x.od_sup_code).Distinct().ToList();
                try
                {
                    foreach (string sup in suppliers)
                    {

                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            Invs051 Invsobj = shiftList[0];
                            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                               new List<SqlParameter>()
                       {
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@sr_shipment_no",null),
                            new SqlParameter("@sup_code", sup),
                            new SqlParameter("@dm_code", Invsobj.Invs051_dm_code),
                            new SqlParameter("@sr_bill_of_lading", "yyy"),
                            new SqlParameter("@sr_house_bill_of_lading", null),
                            new SqlParameter("@sr_bill_of_lading_date", db.DBYYYYMMDDformat(Invsobj.Invs051_sr_bill_of_lading_date)),
                            new SqlParameter("@sr_shipment_port", "123"),
                            new SqlParameter("@sr_port_via", null),
                            new SqlParameter("@sr_expected_departure", null),
                            new SqlParameter("@sr_expected_arrival", null),
                            new SqlParameter("@sr_vessel_name", "ABC"),
                            new SqlParameter("@sr_voyage_no", null),
                            new SqlParameter("@sr_container_no", null),
                            new SqlParameter("@sr_consignment_weight", "50"),
                            new SqlParameter("@sr_consignment_size", "a12"),
                            new SqlParameter("@sr_party_ref", Invsobj.Invs051_sr_party_ref),
                            new SqlParameter("@sr_party_ref_date", null),
                            new SqlParameter("@sr_remarks", Invsobj.Invs051_sr_remarks),
                       });
                            while (dr.Read())
                            {
                                string cnt = dr["shift_cnt"].ToString();
                                shiftment_no = dr["sr_shipment_no"].ToString();
                                if (cnt == "3")
                                {

                                    var shift_order1 = shiftList[0].ShipmentOrder.Where(xx => xx.od_sup_code == sup);
                                    List<Invs051_shipmentOrder> orders = new List<Invs051_shipmentOrder>(shift_order1);
                                    reg = CUDUpdateShipment_details(orders);  ////Insert Shiftment Register
                                }
                            }
                            dr.Close();

                        }
                    }
                }
                catch (Exception x)
                {
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, reg);
        }

        private int CUDUpdateShipment_details(List<Invs051_shipmentOrder> shift_order)
        {
            int reg = 1;
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();

                    foreach (Invs051_shipmentOrder grdlist in shift_order)
                    {
                        string opr = "";

                        if (grdlist.upd == true)
                        {
                            if (grdlist.ischecked == true)
                                opr = "R";
                        }
                        if (grdlist.upd == false)
                            opr = "I";

                        if (!string.IsNullOrEmpty(opr))
                        {
                            SqlDataReader dr1 = db1.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                             new List<SqlParameter>()
                           {
                                new SqlParameter("@opr",opr),
                                new SqlParameter("@sr_shipment_no", shiftment_no),
                                new SqlParameter("@ord_no", grdlist.ord_no),
                                new SqlParameter("@od_line_no", grdlist.od_line_no),
                                new SqlParameter("@so_shipped_qty", grdlist.od_shipped_qty),
                           });

                            while (dr1.Read())
                            {
                                string cnt = dr1["shift_cnt"].ToString();
                                if (cnt == "6")
                                {
                                    reg = 6;                   ////Update shiftment Order
                                }
                                if (cnt == "5")
                                {
                                    reg = 5;                   ///Insert shiftment Order
                                }

                            }
                            dr1.Close();
                        }
                        else
                        {
                            reg = 4;
                        }


                    }
                    //db1.Dispose();
                }
            }
            catch (System.Exception)
            {

            }
            return reg;

        }

        [Route("CUDeleteShipment_details")]
        public HttpResponseMessage CUDeleteShipment_details(string shift_no)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_purs_shipment_register_proc]",
                            new List<SqlParameter>()
                       {
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@sr_shipment_no",shift_no),
                       });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion


        /*
       [Route("Get_Search_Order")]
       public HttpResponseMessage Get_Search_Order()
       {
           List<Invs051> group_list = new List<Invs051>();

           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                       new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "P"),                                
                        });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           Invs051 invsobj = new Invs051();
                           invsobj.Invs051_ord_no = dr["ord_no"].ToString();
                           invsobj.Invs051_dep_code = dr["dep_code"].ToString();
                           invsobj.Invs051_dep_name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                           invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                           invsobj.Invs051_sup_name = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                           invsobj.Invs051_fa_code = dr["fa_code"].ToString();
                           invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                           invsobj.Invs051_dm_name = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                           invsobj.Invs051_trtCode = dr["trt_code"].ToString();
                           invsobj.Invs051_trt_desc = dr["trt_code"].ToString() + "-" + dr["trt_desc"].ToString();
                           invsobj.Invs051_pm_code = dr["pm_code"].ToString();
                           invsobj.Invs051_pm_desc = dr["pm_code"].ToString() + "-" + dr["pm_desc"].ToString();
                           invsobj.Invs051_lc_no = dr["lc_no"].ToString();
                           invsobj.Invs051_Ord_Date = dr["ord_date"].ToString();
                           group_list.Add(invsobj);
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, group_list);
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, group_list);
           }
           return Request.CreateResponse(HttpStatusCode.OK, group_list);
       }

       [Route("Get_Search_Shipment")]
       public HttpResponseMessage Get_Search_Shipment()
       {
           List<Invs051> group_list = new List<Invs051>();

           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                       new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "L"),                                
                        });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           Invs051 invsobj = new Invs051();
                           invsobj.Invs051_sr_shipment_no = dr["sr_shipment_no"].ToString();
                           invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                           invsobj.Invs051_sup_name = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                           invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                           invsobj.Invs051_dm_name = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                           invsobj.Invs051_sr_party_ref = dr["sr_party_ref"].ToString();
                           invsobj.Invs051_sr_bill_of_lading_date = dr["sr_bill_of_lading_date"].ToString();
                           invsobj.Invs051_sr_remarks = dr["sr_remarks"].ToString();
                           group_list.Add(invsobj);
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, group_list);
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, group_list);
           }
           return Request.CreateResponse(HttpStatusCode.OK, group_list);
       }

       [Route("GetShipmentId")]
       public HttpResponseMessage GetShipmentId()
       {
           List<Invs051> group_list = new List<Invs051>();
           string id = null;
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                       new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "N"),                                
                        });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           id = dr["comn_number_sequence"].ToString();
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, id);
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, id);
           }
           return Request.CreateResponse(HttpStatusCode.OK, id);
       }

       [Route("Get_DeliveryMode")]
       public HttpResponseMessage Get_DeliveryMode()
       {
           List<Invs051> mod_list = new List<Invs051>();

           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                       new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "V"),                                
                        });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           Invs051 Invsobj = new Invs051();
                           Invsobj.Invs051_dm_code = dr["dm_code"].ToString();
                           Invsobj.Invs051_dm_name = dr["dm_name"].ToString();
                           Invsobj.deliveryMode = dr["dm_code"].ToString() + "-" + dr["dm_name"].ToString();
                           mod_list.Add(Invsobj);
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, mod_list);
           }
           return Request.CreateResponse(HttpStatusCode.OK, mod_list);
       }

       [Route("Get_SupplierCode")]
       public HttpResponseMessage Get_SupplierCode()
       {
           List<Invs051> mod_list = new List<Invs051>();

           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                       new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "W"),                                
                        });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           Invs051 Invsobj = new Invs051();
                           Invsobj.Invs051_sup_code = dr["sup_code"].ToString();
                           Invsobj.Invs051_sup_name = dr["sup_name"].ToString();
                           Invsobj.supCode = dr["sup_code"].ToString() + "-" + dr["sup_name"].ToString();
                           mod_list.Add(Invsobj);
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, mod_list);
           }
           return Request.CreateResponse(HttpStatusCode.OK, mod_list);
       }

       [Route("getSearch_Order_Details")]
       public HttpResponseMessage getSearch_Order_Details(string ob)
       {
           Invs051 objSearchItem = new Invs051();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                 //  foreach (Invs051 objSearchItem in data)
                  // {
                       SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                           new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "E"),  
                            new SqlParameter("@ord_no", ob)  
                        });
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               Invs051_OrderDetails invsobj = new Invs051_OrderDetails();
                               invsobj.Invs051_sr_shipment_no = string.Empty;
                               invsobj.old_sr_shipment_no = string.Empty;
                               invsobj.ord_no = dr["ord_no"].ToString();
                               invsobj.od_line_no = dr["od_line_no"].ToString();
                               invsobj.im_item_code = dr["im_item_code"].ToString();
                               invsobj.im_desc = dr["im_desc"].ToString();
                               invsobj.im_inv_no = dr["im_inv_no"].ToString();
                               invsobj.uom_code = dr["uom_code"].ToString();
                               if (!string.IsNullOrEmpty(dr["od_shipped_qty"].ToString()))
                                   invsobj.od_shipped_qty = dr["od_shipped_qty"].ToString();
                               else
                                   invsobj.od_shipped_qty = "0";
                               invsobj.upd = false;
                               objSearchItem.OrderDetails.Add(invsobj);
                           }
                           return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
                       }
                   //}
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
           }
           return Request.CreateResponse(HttpStatusCode.OK, objSearchItem);
       }

       [Route("Search_Shipment_Details")]
       public HttpResponseMessage Search_Shipment_Details(List<Invs051> data)
       {
           List<Invs051> mod_list = new List<Invs051>();
           Invs051 ob = new Invs051();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   foreach (Invs051 objSearchItem in data)
                   {
                       SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                           new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "M"),   
                           new SqlParameter("@sr_shipment_no", objSearchItem.Invs051_sr_shipment_no)
                        });
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               Invs051_shipmentOrder invsobj = new Invs051_shipmentOrder();
                               invsobj.old_sr_shipment_no = string.Empty;
                               invsobj.Invs051_sr_shipment_no = dr["sr_shipment_no"].ToString();
                               invsobj.ord_no = dr["ord_no"].ToString();
                               invsobj.od_line_no = dr["od_line_no"].ToString();
                               invsobj.od_shipped_qty = dr["so_shipped_qty"].ToString();
                               invsobj.im_item_code = dr["im_item_code"].ToString();
                               invsobj.im_desc = dr["im_desc"].ToString();
                               invsobj.uom_code = dr["uom_code"].ToString();
                               invsobj.upd = true;
                               ob.ShipmentOrder.Add(invsobj);
                           }
                           return Request.CreateResponse(HttpStatusCode.OK, ob);
                       }
                   }
               }
           }
           catch (Exception x)
           {
               return Request.CreateResponse(HttpStatusCode.OK, ob);
           }
           return Request.CreateResponse(HttpStatusCode.OK, ob);
       }

       [Route("CUDInsertShipment_regiser")]
       public HttpResponseMessage CUDInsertShipment_regiser(List<Invs051> shiftList)
       {
          // bool inserted = true;
           int reg=2;

           List<Invs051_shipmentOrder> shift_order = new List<Invs051_shipmentOrder>();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();

                   foreach (Invs051 Invsobj in shiftList)
                   {
                       shiftment_no = Invsobj.Invs051_sr_shipment_no;
                       shift_order = Invsobj.ShipmentOrder;

                       SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                           new List<SqlParameter>()
                       {
                           new SqlParameter("@opr", "X"),
                           new SqlParameter("@sr_shipment_no",shiftment_no),
                           new SqlParameter("@sup_code", Invsobj.Invs051_sup_code),
                           new SqlParameter("@dm_code", Invsobj.Invs051_dm_code),
                           new SqlParameter("@sr_bill_of_lading", "yyy"),
                           new SqlParameter("@sr_house_bill_of_lading", null),
                           new SqlParameter("@sr_bill_of_lading_date", Invsobj.Invs051_sr_bill_of_lading_date),
                           new SqlParameter("@sr_shipment_port", "123"),
                           new SqlParameter("@sr_port_via", null),
                           new SqlParameter("@sr_expected_departure", null),
                           new SqlParameter("@sr_expected_arrival", null),
                           new SqlParameter("@sr_vessel_name", "ABC"),
                           new SqlParameter("@sr_voyage_no", null),
                           new SqlParameter("@sr_container_no", null),
                           new SqlParameter("@sr_consignment_weight", "50"),
                           new SqlParameter("@sr_consignment_size", "a12"),
                           new SqlParameter("@sr_party_ref", Invsobj.Invs051_sr_party_ref),
                           new SqlParameter("@sr_party_ref_date", null),
                           new SqlParameter("@sr_remarks", Invsobj.Invs051_sr_remarks),
                       });

                       while (dr.Read())
                       {
                           string cnt = dr["shift_cnt"].ToString();
                           if (cnt == "3")
                           {
                               reg = CUDUpdateShipment_details(shift_order);  ////Insert Shiftment Register
                           }
                           if (cnt == "4")
                           {
                               reg = 4;      ////Update Shiftment Register     
                               reg = CUDUpdateShipment_details(shift_order); 
                           }

                       }
                       dr.Close();
                   }
                   //db.Dispose();
               }
           }
           catch (Exception x)
           {

           }
           return Request.CreateResponse(HttpStatusCode.OK, reg);
       }

       private int CUDUpdateShipment_details(List<Invs051_shipmentOrder> shift_order)
       {
           int reg = 1;
           try
           {
               using (DBConnection db1 = new DBConnection())
               {
                   db1.Open();

                   foreach (Invs051_shipmentOrder grdlist in shift_order)
                   {
                       string opr = "";

                       if (grdlist.upd == true)
                       {
                           if (grdlist.ischecked == true)
                               opr = "R";
                       }
                       if (grdlist.upd == false)
                           opr = "I";

                       if (!string.IsNullOrEmpty(opr))
                       {
                           SqlDataReader dr1 = db1.ExecuteStoreProcedure("[invs].[invs_purs_shipment_register_proc]",
                            new List<SqlParameter>()
                           {
                               new SqlParameter("@opr",opr),
                               new SqlParameter("@sr_shipment_no", shiftment_no),
                               new SqlParameter("@ord_no", grdlist.ord_no),
                               new SqlParameter("@od_line_no", grdlist.od_line_no),
                               new SqlParameter("@so_shipped_qty", grdlist.od_shipped_qty),
                           });

                           while (dr1.Read())
                           {
                               string cnt = dr1["shift_cnt"].ToString();
                               if (cnt == "6")
                               {
                                   reg = 6;                   ////Update shiftment Order
                               }
                               if (cnt == "5")
                               {
                                   reg = 5;                   ///Insert shiftment Order
                               }

                           }
                           dr1.Close();
                       }
                       else
                       {
                           reg = 4;  
                       }


                   }
                   //db1.Dispose();
               }
           }
           catch (System.Exception)
           {

           }
           return reg;

       }

       [Route("CUDeleteShipment_details")]
       public HttpResponseMessage CUDeleteShipment_details(string shift_no)
       {
           bool inserted = false;
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_purs_shipment_register_proc]",
                           new List<SqlParameter>() 
                       { 
                           new SqlParameter("@opr", "D"),
                           new SqlParameter("@sr_shipment_no",shift_no),
                       });
                   if (ins > 0)
                   {
                       inserted = true;
                   }
                   else
                   {
                       inserted = false;
                   }

               }
           }
           catch (Exception x)
           {
           }
           return Request.CreateResponse(HttpStatusCode.OK, inserted);
       }

        */

        //private int CUDInsertShipment_details(List<Invs051_shipmentOrder> shift_order)
        //{
        //    int reg = 4;
        //    try
        //    {
        //        using (DBConnection db1 = new DBConnection())
        //        {
        //            db1.Open();

        //            foreach (Invs051_shipmentOrder grdlist in shift_order)
        //            {
        //               int ins = db1.ExecuteStoreProcedureforInsert("[invs].[invs_purs_shipment_register_proc]",
        //                new List<SqlParameter>()
        //                    {
        //                            new SqlParameter("@opr", "W"),
        //                            new SqlParameter("@sr_shipment_no", shiftment_no),
        //                            new SqlParameter("@ord_no", grdlist.ord_no),
        //                            new SqlParameter("@od_line_no", grdlist.od_line_no),
        //                            new SqlParameter("@so_shipped_qty", grdlist.od_shipped_qty),
        //                    });
        //               if (ins > 0)
        //               {
        //                   reg = 5;
        //               }
        //               else
        //               {
        //                   reg = 4;
        //               }

        //            }
        //        }
        //    }
        //    catch (System.Exception)
        //    {

        //    }
        //    return reg;

        //}

        //[Route("CUDUpdateShipment_regiser")]
        //public HttpResponseMessage CUDUpdateShipment_regiser(List<Invs051> shiftList)
        //{
        //    // bool inserted = true;
        //    int reg = 2;

        //    List<Invs051_shipmentOrder> shift_order = new List<Invs051_shipmentOrder>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            foreach (Invs051 Invsobj in shiftList)
        //            {
        //                shiftment_no = Invsobj.Invs051_sr_shipment_no;
        //                shift_order = Invsobj.ShipmentOrder;

        //                int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_purs_shipment_register_proc]",
        //                    new List<SqlParameter>()
        //                {
        //                    new SqlParameter("@opr", "U"),
        //                    new SqlParameter("@sr_shipment_no", shiftment_no),
        //                    new SqlParameter("@sr_remarks", Invsobj.Invs051_sr_remarks),
        //                    new SqlParameter("@sr_bill_of_lading_date", Invsobj.Invs051_sr_bill_of_lading_date),
        //                });

        //                if (ins > 0)
        //                {
        //                    reg = 3;
        //                    reg = CUDUpdateShipment_details(shift_order);
        //                }
        //                else
        //                {
        //                    reg = 2;
        //                }



        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, reg);
        //}



    }
}