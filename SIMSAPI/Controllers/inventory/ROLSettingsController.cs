﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.ROLSettingsController
{
    [RoutePrefix("api/rolsettings")]
    [BasicAuthentication]
    public class ROLSettingsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllROLSettings")]
        public HttpResponseMessage getAllROLSettings()
        {

            List<Inv131> desg_list = new List<Inv131>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.im_average_month_demand = dr["im_average_month_demand"].ToString();
                            simsobj.im_approximate_lead_time = dr["im_approximate_lead_time"].ToString();
                            simsobj.im_reorder_level = dr["im_reorder_level"].ToString();

                            simsobj.im_min_level = dr["im_min_level"].ToString();
                            simsobj.im_max_level = dr["im_max_level"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_mark_up = dr["im_mark_up"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.im_sellprice_special = dr["im_sellprice_special"].ToString();
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getROLSettingsbyFilter")]
        public HttpResponseMessage getROLSettingsbyFilter(string sg_name, string dep_code, string pc_code, string uom_code)
        {

            if (sg_name == "undefined")
            {
                sg_name = null;
            }
            if (pc_code == "undefined")
            {
                pc_code = null;
            }
            if (uom_code == "undefined")
            {
                uom_code = null;
            }

            List<Inv131> desg_list = new List<Inv131>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@opr_upd", 'S'), 
                             new SqlParameter("@sg_name",sg_name),
                             new SqlParameter("@dep_code",dep_code),
                             new SqlParameter("@pc_code",pc_code),
                             new SqlParameter("@uom_code",uom_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.im_average_month_demand = dr["im_average_month_demand"].ToString();
                            simsobj.im_approximate_lead_time = dr["im_approximate_lead_time"].ToString();
                            simsobj.im_reorder_level = dr["im_reorder_level"].ToString();

                            simsobj.im_min_level = dr["im_min_level"].ToString();
                            simsobj.im_max_level = dr["im_max_level"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_mark_up = dr["im_mark_up"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.im_sellprice_special = dr["im_sellprice_special"].ToString();
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<Inv131> goal_list = new List<Inv131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'O'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.com_code = dr["com_code"].ToString();
                            simsobj.com_name = dr["com_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeparments")]
        public HttpResponseMessage getDeparments()
        {

            List<Inv131> goal_list = new List<Inv131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Z'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getProductName")]
        public HttpResponseMessage getProductName()
        {

            List<Inv131> goal_list = new List<Inv131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.pc_code = dr["pc_code"].ToString();
                            simsobj.pc_desc = dr["pc_desc"].ToString();
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getUOM")]
        public HttpResponseMessage getUOM()
        {

            List<Inv131> goal_list = new List<Inv131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSupplierName")]
        public HttpResponseMessage getSupplierName()
        {


            List<Inv131> goal_list = new List<Inv131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'M'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv131 simsobj = new Inv131();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDROLSettings")]
        public HttpResponseMessage CUDCompany(List<Inv131> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv131 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_item_master_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@opr_upd", simsobj.opr_upd),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@im_average_month_demand", simsobj.im_average_month_demand),
                                new SqlParameter("@im_approximate_lead_time", simsobj.im_approximate_lead_time),
                                new SqlParameter("@im_reorder_level", simsobj.im_reorder_level),
                                //new SqlParameter("@im_malc_rate", simsobj.im_malc_rate),
                                //new SqlParameter("@im_mark_up", simsobj.im_mark_up),
                                //new SqlParameter("@im_sell_price", simsobj.im_sell_price),
                                //new SqlParameter("@im_sellprice_special", simsobj.im_sellprice_special),
                               
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
