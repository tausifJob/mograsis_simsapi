﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/common/ItemSupplier")]

    public class ItemSupplierController : ApiController
    {
          [Route("GetAllSuppliers")]
          public HttpResponseMessage GetAllSuppliers()
          {
              List<InvsClass> mod_list = new List<InvsClass>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),                                
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              InvsClass invsObj = new InvsClass();
                              invsObj.sup_name = dr["sup_code"].ToString() + '-' + dr["sup_name"].ToString();
                              invsObj.sup_code = dr["sup_code"].ToString();
                              mod_list.Add(invsObj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                      }
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, mod_list);
              }
              return Request.CreateResponse(HttpStatusCode.OK, mod_list);
          }

          [Route("GetAllItems")]
          public HttpResponseMessage GetAllItems()
          {
              List<InvsClass> mod_list = new List<InvsClass>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B"),                                
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              InvsClass invsObj = new InvsClass();
                              invsObj.im_inv_no = dr["im_inv_no"].ToString();
                              invsObj.im_item_code = dr["im_item_code"].ToString();
                              invsObj.item_name = dr["im_desc"].ToString();
                              invsObj.dep_code = dr["dep_code"].ToString();
                              invsObj.dep_name = dr["dep_name"].ToString();
                              mod_list.Add(invsObj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                      }
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, mod_list);
              }
              return Request.CreateResponse(HttpStatusCode.OK, mod_list);
          }

          [Route("GetAllCurrency")]
          public HttpResponseMessage GetAllCurrency()
          {
              List<InvsClass> mod_list = new List<InvsClass>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),                                
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              InvsClass invsObj = new InvsClass();
                              invsObj.curr_code = dr["excg_curcy_code"].ToString();
                              invsObj.curr_name = dr["excg_curcy_desc"].ToString();
                              mod_list.Add(invsObj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                      }
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, mod_list);
              }
              return Request.CreateResponse(HttpStatusCode.OK, mod_list);
          }

          [Route("GetAllUOM")]
          public HttpResponseMessage GetAllUOM()
          {
              List<InvsClass> mod_list = new List<InvsClass>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M"),                                
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              InvsClass invsObj = new InvsClass();
                              invsObj.uom_code = dr["uom_code"].ToString();
                              invsObj.uom_name = dr["uom_name"].ToString();
                              mod_list.Add(invsObj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                      }
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, mod_list);
              }
              return Request.CreateResponse(HttpStatusCode.OK, mod_list);
          }

          [Route("Get_ItemSupplier")]
          public HttpResponseMessage Get_ItemSupplier()
          {
              List<Invs040> mod_list = new List<Invs040>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              Invs040 invsObj = new Invs040();
                              invsObj.sup_code = dr["sup_code"].ToString();
                              invsObj.sup_name = dr["sup_code"].ToString()+'-'+dr["sup_name"].ToString();
                              if (string.IsNullOrEmpty(dr["im_inv_no"].ToString()) == false)
                                  invsObj.im_inv_no =dr["im_inv_no"].ToString();
                              invsObj.im_item_code = dr["im_item_code"].ToString();
                              invsObj.item_name = dr["item_name"].ToString();
                              invsObj.is_uom = dr["is_uom"].ToString();
                              invsObj.uom_name = dr["is_uom"].ToString()+'-'+dr["uom_name"].ToString();
                              invsObj.is_cur_code = dr["is_cur_code"].ToString();
                              invsObj.is_cur_name = dr["is_cur_code"].ToString()+'-'+dr["is_cur_name"].ToString();
                              if (string.IsNullOrEmpty(dr["is_price"].ToString()) == false)
                                  invsObj.is_price =dr["is_price"].ToString();
                              if (string.IsNullOrEmpty(dr["is_price_date"].ToString()) == false)
                                  invsObj.is_price_date =db.UIDDMMYYYYformat(dr["is_price_date"].ToString());
                              if (string.IsNullOrEmpty(dr["is_economic_order_qty"].ToString()) == false)
                                  invsObj.is_economic_order_qty = int.Parse(dr["is_economic_order_qty"].ToString());
                              mod_list.Add(invsObj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                      }
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, mod_list);
              }
              return Request.CreateResponse(HttpStatusCode.OK, mod_list);
          }

          [Route("CUDItemSupplier")]
          public HttpResponseMessage CUDItemSupplier(List<Invs040> data)
          {
              Message message = new Message();
              bool inserted = false;
              try
              {
                  if (data != null)
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          foreach (Invs040 invsobj in data)
                          {
                              SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                                  new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", invsobj.opr),
                                    new SqlParameter("@sup_code", invsobj.sup_code),
                                    new SqlParameter("@im_inv_no", invsobj.im_inv_no),
                                    new SqlParameter("@is_uom", invsobj.is_uom),
                                    new SqlParameter("@is_cur_code", invsobj.is_cur_code),
                                    new SqlParameter("@is_price", invsobj.is_price),
                                    new SqlParameter("@is_price_date",db.DBYYYYMMDDformat(invsobj.is_price_date)),
                                    new SqlParameter("@is_economic_order_qty", invsobj.is_economic_order_qty),
                            });
                              while (dr.Read())
                              {
                                  string cnt = dr["param_cnt"].ToString();
                                  if (cnt == "1")
                                  {
                                      inserted = true;
                                  }
                                  else
                                  {
                                      inserted = false;
                                  }
                              }
                          }
                      }
                  }
                  else
                  {
                      return Request.CreateResponse(HttpStatusCode.OK, inserted);
                  }

              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, x.Message);
              }
              return Request.CreateResponse(HttpStatusCode.OK, inserted);
          }

          [Route("ItemSupplier_updatedel")]
          public HttpResponseMessage ItemSupplier_updatedel(List<Invs040> data)
          {
              Message message = new Message();
              bool inserted = false;
              try
              {
                  if (data != null)
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          foreach (Invs040 invsobj in data)
                          {
                              int ins = db.ExecuteStoreProcedureforInsert("[invs].[item_suppliers_proc]",
                                  new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", invsobj.opr),
                                    new SqlParameter("@sup_code", invsobj.sup_code),
                                    new SqlParameter("@im_inv_no", invsobj.im_inv_no),
                                    new SqlParameter("@is_uom", invsobj.is_uom),
                                    new SqlParameter("@is_cur_code", invsobj.is_cur_code),
                                    new SqlParameter("@is_price", invsobj.is_price),
                                    new SqlParameter("@is_price_date", db.DBYYYYMMDDformat(invsobj.is_price_date)),
                                    new SqlParameter("@is_economic_order_qty", invsobj.is_economic_order_qty),
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                          }
                      }
                  }
                  else
                  {
                      return Request.CreateResponse(HttpStatusCode.OK, inserted);
                  }

              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, x.Message);
              }
              return Request.CreateResponse(HttpStatusCode.OK, inserted);
          }
    }
}