﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using SIMSAPI.Models.ERP.inventoryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/CustomExpenses")]
    public class CustomExpensesController:ApiController
    {

        [Route("getCustomExpensesDetails")]
        public HttpResponseMessage getCustomExpensesDetails()
        {
            List<Inv134> Custom_expenses = new List<Inv134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_expenses_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv134 obj = new Inv134();

                            obj.ccl_reg_no = dr["ccl_reg_no"].ToString();
                            obj.pet_code = dr["pet_code"].ToString();
                            obj.pet_desc = dr["pet_desc"].ToString();
                            obj.ce_ref_no = dr["ce_ref_no"].ToString();
                            obj.ce_amount = dr["ce_amount"].ToString();
                            obj.ce_ref_date = db.UIDDMMYYYYformat(dr["ce_ref_date"].ToString());
                            obj.cc_no = dr["cc_no"].ToString();

                            Custom_expenses.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Custom_expenses);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Custom_expenses);
        }

        [Route("CustomExpensesCUD")]
        public HttpResponseMessage CustomExpensesCUD(List<Inv134> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv134 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_custom_expenses_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@ccl_reg_no",simsobj.ccl_reg_no),
                          new SqlParameter("@pet_code", simsobj.pet_code),
                          new SqlParameter("@old_pet_code",simsobj.old_pet_code),
                          new SqlParameter("@ce_amount",simsobj.ce_amount),
                          new SqlParameter("@ce_ref_date", db.DBYYYYMMDDformat(simsobj.ce_ref_date)),
                          new SqlParameter("@ce_ref_no", simsobj.ce_ref_no),
                          new SqlParameter("@cc_no", simsobj.cc_no)
                        });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getReg_noFromClearance")]
        public HttpResponseMessage getReg_noFromClearance()
        {
            List<Inv134> CclRegNo_list = new List<Inv134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_expenses_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "R")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv134 obj = new Inv134();

                            obj.ccl_reg_no = dr["ccl_reg_no"].ToString();
                            obj.ccl_date = db.UIDDMMYYYYformat(dr["ccl_date"].ToString());
                            try
                            {
                                obj.sr_shipment_no = dr["sr_shipment_no"].ToString();
                            }
                            catch(Exception e) { };

                            CclRegNo_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, CclRegNo_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, CclRegNo_list);
        }

        [Route("getShipmentDetails")]
        public HttpResponseMessage getShipmentDetails(string shipment_id)
        {
            List<Invs051_shipmentOrder> mod_list = new List<Invs051_shipmentOrder>();
            Invs051 ob = new Invs051();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_expenses_proc]",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", "A"),
                         new SqlParameter("@sr_shipment_no",shipment_id)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs051_shipmentOrder invsobj = new Invs051_shipmentOrder();
                            invsobj.old_sr_shipment_no = string.Empty;
                            invsobj.Invs051_sr_shipment_no = dr["sr_shipment_no"].ToString();
                            invsobj.ord_no = dr["ord_no"].ToString();
                            invsobj.od_line_no = dr["od_line_no"].ToString();
                            invsobj.od_order_qty = dr["od_order_qty"].ToString();
                            invsobj.od_shipped_qty = dr["so_shipped_qty"].ToString();
                            invsobj.im_item_code = dr["im_item_code"].ToString();
                            invsobj.im_desc = dr["im_desc"].ToString();
                            invsobj.uom_code = dr["uom_code"].ToString();
                            invsobj.od_sup_code = dr["sup_code"].ToString();
                            invsobj.od_sup_name = dr["sup_name"].ToString();
                            try
                            {                                
                                invsobj.sr_party_ref = dr["sr_party_ref"].ToString();
                                invsobj.dm_name = dr["dm_name"].ToString();
                                invsobj.sr_remarks = dr["sr_remarks"].ToString();
                                invsobj.sr_bill_of_lading_date = db.UIDDMMYYYYformat(dr["sr_bill_of_lading_date"].ToString());
                            }
                            catch(Exception ex){};
                            
                            mod_list.Add(invsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


    }
}