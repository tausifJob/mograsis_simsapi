﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;

using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/ItemGradeSection")]
    [BasicAuthentication]
    public class ItemGradeSectionController : ApiController
    {



        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<itemgradesec> lstCuriculum = new List<itemgradesec>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec sequence = new itemgradesec();
                            sequence.cur = dr["sims_cur_code"].ToString();
                            sequence.cur_name= dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<itemgradesec> lstModules = new List<itemgradesec>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", curCode)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec sequence = new itemgradesec();
                            sequence.aca= dr["sims_academic_year"].ToString();
                            sequence.aca_name= dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getCat")]
        public HttpResponseMessage getCat()
        {
            List<itemgradesec> lstCuriculum = new List<itemgradesec>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "T"),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec sequence = new itemgradesec();
                            sequence.cat = dr["pc_code"].ToString();
                            sequence.cat_name = dr["pc_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getSubCat")]
        public HttpResponseMessage getSubCat(string cat)
        {
            List<itemgradesec> lstModules = new List<itemgradesec>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@cat_code", cat)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec sequence = new itemgradesec();
                            sequence.sub_cat = dr["pc_code"].ToString();
                            sequence.sub_cat_name = dr["pc_desc"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string ac_year)
        {

            List<itemgradesec> grade_list = new List<itemgradesec>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec simsobj = new itemgradesec();
                            simsobj.grade = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("AllSections")]
        public HttpResponseMessage AllSections(List<itemgradesec> temp)
        {
            string cur = "", aca = "", grd = "";
            foreach (var item in temp)
            {
                cur = item.cur;
                aca = item.aca;
                grd = grd + item.grade + "/";
            }
            List<itemgradesec> section_list = new List<itemgradesec>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@sims_cur_code", cur),
                                new SqlParameter("@sims_academic_year", aca),
                                new SqlParameter("@sims_grade_code", grd),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            itemgradesec simsobj = new itemgradesec();
                            simsobj.section = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            section_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, section_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, section_list);
        }

        [Route("allData")]
        public HttpResponseMessage allData(List<itemgradesec> temp)
        {
            string cur = "", aca = "", grd = "", cat = "", sub_cat="";
            foreach (var item in temp)
            {
                cur = item.cur;
                aca = item.aca;
                grd = grd + item.section + "/";
              
                cat = item.cat;
                sub_cat = item.sub_cat;
                if (cat == "undefined")
                    cat = null;
                if (sub_cat == "undefined")
                    sub_cat = null;

            }

            List<itemgradesec> termlist = new List<itemgradesec>();

            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]", new List<SqlParameter>() 
                         {
                
               new SqlParameter("@opr",'P'),
               new SqlParameter("@sims_cur_code",cur),
               new SqlParameter("@sims_academic_year",aca),
               new SqlParameter("@sims_grade_code",grd),
               new SqlParameter("@cat_code",cat),
               new SqlParameter("@sub_code",sub_cat)
                         });

                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            itemgradesec lst = new itemgradesec();
                            lst.item_no = dr1["im_inv_no"].ToString();
                            lst.item_desc = dr1["im_desc"].ToString();
                            lst.dep_name = dr1["dep_name"].ToString();
                            lst.item_qty = dr1["id_cur_qty"].ToString();
                            if (string.IsNullOrEmpty(dr1["qty_for_parent"].ToString()))
                                lst.qty_for_parent = lst.item_qty;
                            else
                            lst.qty_for_parent = dr1["qty_for_parent"].ToString();
                            lst.grade= dr1["sims_grade_code"].ToString();
                            //lst.section= dr1["sims_section_code"].ToString();
                            if(dr1["im_edit_qty_flag"].ToString()=="Y")
                                lst.qty_flag = true;
                            else
                                lst.qty_flag = false;
                            if (!string.IsNullOrEmpty(lst.grade))
                                lst.isselect = true;
                            else
                                lst.isselect = false;

                            termlist.Add(lst);
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, termlist);
        }

        [Route("Updateitemgrdsec")]
        public HttpResponseMessage Updateitemgrdsec(List<itemgradesec> data)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (itemgradesec invsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "I"),
                 new SqlParameter("@im_inv_no", invsobj.item_no),
	  new SqlParameter("@sims_cur_code", invsobj.cur),
	  new SqlParameter("@sims_academic_year", invsobj.aca),
	  new SqlParameter("@sims_grade_code", invsobj.grade),
	  new SqlParameter("@sims_section_code", invsobj.section),
	  new SqlParameter("@im_item_qty", invsobj.qty_for_parent),
	  new SqlParameter("@im_edit_qty_flag", invsobj.qty_flag==true?"Y":"N"),
               
                
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                //Inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("UDeleteitemgrdsec")]
        public HttpResponseMessage UDeleteitemgrdsec(List<itemgradesec> data)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (itemgradesec invsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_grade_section_mapping]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "D"),
                 new SqlParameter("@im_inv_no", invsobj.item_no),
	  new SqlParameter("@sims_cur_code", invsobj.cur),
	  new SqlParameter("@sims_academic_year", invsobj.aca),
	  new SqlParameter("@sims_grade_code", invsobj.grade),
	  new SqlParameter("@sims_section_code", invsobj.section),
	  new SqlParameter("@im_item_qty", invsobj.qty_for_parent),
	  new SqlParameter("@im_edit_qty_flag", invsobj.qty_flag==true?"Y":"N"),
               
                
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

   







    }
}

