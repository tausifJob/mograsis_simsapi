﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/ItemMasterDetails")]
    public class ItemMasterController:ApiController
    {
        [Route("getSupplierGroupNamewithouitem")]
        public HttpResponseMessage getSupplierGroupNamewithouitem()
        {
            List<Inv021> Supplieritem_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "K")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.sg_desc = dr["sg_desc"].ToString();
                            obj.sg_name = dr["sg_name"].ToString();

                            Supplieritem_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Supplieritem_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Supplieritem_list);
        }

        [Route("getSupplierGroupName")]
        public HttpResponseMessage getSupplierGroupName()
        {
            List<Inv021> SupplierGrp_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.sg_desc = dr["sg_desc"].ToString();
                            obj.sg_name = dr["sg_name"].ToString();

                            SupplierGrp_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
        }
        
        [Route("getSectionName")]
        public HttpResponseMessage getSectionName(string dept_code)
        {
            List<Inv021> Section_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@dep_code", dept_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.sec_name = dr["sec_name"].ToString();
                            obj.sec_code = dr["sec_code"].ToString();

                            Section_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Section_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Section_list);
        }

        [Route("getProductDesc")]
        public HttpResponseMessage getProductDesc()
        {
            List<Inv021> Product_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.pc_desc = dr["pc_desc"].ToString();
                            obj.pc_code = dr["pc_code"].ToString();

                            Product_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Product_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Product_list);
        }

        [Route("getUnitOfMeasurement")]
        public HttpResponseMessage getUnitOfMeasurement()
        {
            List<Inv021> Measurement_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "E"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.uom_name = dr["uom_name"].ToString();
                            obj.uom_code = dr["uom_code"].ToString();

                            Measurement_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
        }

        [Route("getTradeCategory")]
        public HttpResponseMessage getTradeCategory()
        {
            List<Inv021> Measurement_list = new List<Inv021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 obj = new Inv021();

                            obj.trade_cat_name = dr["invs_appl_form_field_value1"].ToString();
                            obj.im_trade_cat = dr["invs_appl_parameter"].ToString();

                            Measurement_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Measurement_list);
        }

        [Route("getItemMasterDetails")]
        public HttpResponseMessage getItemMasterDetails(string data)
        {
            Inv021 obj = new Inv021();
            List<Inv021> ItemMaster_list = new List<Inv021>();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Inv021>(data);
            }
            if (obj.im_inv_no == "undefined" || obj.im_inv_no == "")
            {
                obj.im_inv_no = null;
            }
            if (obj.im_desc == "undefined" || obj.im_desc == "")
            {
                obj.im_desc = null;
            }
            if (obj.im_item_code == "undefined" || obj.im_item_code == "")
            {
                obj.im_item_code = null;
            }
            if (obj.dep_code == "undefined" || obj.dep_code == "")
            {
                obj.dep_code = null;
            }
            if (obj.sec_code == "undefined" || obj.sec_code == "")
            {
                obj.sec_code = null;
            }
            if (obj.sup_code == "undefined" || obj.sup_code == "")
            {
                obj.sup_code = null;
            }
            if (obj.pc_code == "undefined" || obj.pc_code == "")
            {
                obj.pc_code = null;
            }
            if (obj.sg_name == "undefined" || obj.sg_name == "")
            {
                obj.sg_name = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                         {
                           
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@im_inv_no",obj.im_inv_no),
                            new SqlParameter("@im_desc",obj.im_desc),
                            new SqlParameter("@im_item_code",obj.im_item_code),
                            new SqlParameter("@dep_code", obj.dep_code),
                            new SqlParameter("@sec_code", obj.sec_code),
                            new SqlParameter("@sup_code", obj.sup_code),
                            new SqlParameter("@pc_code", obj.pc_code),
                            new SqlParameter("@sg_name", obj.sg_name)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv021 simobj = new Inv021();

                            simobj.im_inv_no = dr["im_inv_no"].ToString();

                            simobj.sg_name = dr["sg_name"].ToString();
                            simobj.sg_desc = dr["sg_desc"].ToString();

                            simobj.im_item_code = dr["im_item_code"].ToString();
                            simobj.im_desc = dr["im_desc"].ToString();
                            simobj.im_model_name = dr["im_model_name"].ToString();

                            simobj.dep_code = dr["dep_code"].ToString();
                            simobj.dep_name = dr["dep_name"].ToString();

                            simobj.sec_code = dr["sec_code"].ToString();
                            simobj.sec_name = dr["sec_name"].ToString();
                            simobj.pc_code = dr["pc_code"].ToString();
                            simobj.pc_desc = dr["pc_desc"].ToString();

                            simobj.sup_code = dr["sup_code"].ToString();
                            simobj.sup_name = dr["sup_name"].ToString();
                            simobj.uom_code = dr["uom_code"].ToString();
                            simobj.uom_name = dr["uom_name"].ToString();
                            simobj.uom_code_has = dr["uom_code_has"].ToString();
                            simobj.uom_name_has = dr["uom_name_has"].ToString();
                            simobj.im_sell_min_qty = dr["im_sell_min_qty"].ToString();
                            simobj.im_supl_catalog_price = dr["im_supl_catalog_price"].ToString();
                            simobj.im_supl_price_date = db.UIDDMMYYYYformat(dr["im_supl_price_date"].ToString());
                            simobj.im_supl_min_qty = dr["im_supl_min_qty"].ToString();
                            simobj.im_supl_buying_price = dr["im_supl_buying_price"].ToString();
                            simobj.im_supl_buying_price_old = dr["im_supl_buying_price_old"].ToString();
                            simobj.im_min_level = dr["im_min_level"].ToString();
                            simobj.im_max_level = dr["im_max_level"].ToString();
                            simobj.im_reorder_level = dr["im_reorder_level"].ToString();
                            simobj.im_economic_order_qty = dr["im_economic_order_qty"].ToString();
                            simobj.im_rso_qty = dr["im_rso_qty"].ToString();
                            simobj.im_average_month_demand = dr["im_average_month_demand"].ToString();
                            simobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simobj.im_malc_rate_old = dr["im_malc_rate_old"].ToString();
                            simobj.im_mark_up = dr["im_mark_up"].ToString();
                            simobj.im_sell_price = dr["im_sell_price"].ToString();
                            simobj.im_sell_price_old = dr["im_sell_price_old"].ToString();
                            simobj.im_sellprice_special = dr["im_sellprice_special"].ToString();
                            simobj.im_sellprice_freeze_ind = dr["im_sellprice_freeze_ind"].ToString().Equals("Y") ? true : false;
                            simobj.im_approximate_price = dr["im_approximate_price"].ToString();
                            simobj.im_estimated_price = dr["im_estimated_price"].ToString();
                            simobj.im_creation_date = db.UIDDMMYYYYformat(dr["im_creation_date"].ToString());
                            simobj.im_stock_check_date = db.UIDDMMYYYYformat(dr["im_stock_check_date"].ToString());
                            simobj.im_no_of_packing = dr["im_no_of_packing"].ToString();
                            simobj.im_last_receipt_date = dr["im_last_receipt_date"].ToString();
                            simobj.im_last_issue_date = dr["im_last_issue_date"].ToString();
                            simobj.im_last_supp_code = dr["im_last_supp_code"].ToString();
                            simobj.im_approximate_lead_time = dr["im_approximate_lead_time"].ToString();
                            simobj.im_manufact_serial_no = dr["im_manufact_serial_no"].ToString();
                            simobj.im_last_supp_cur = dr["im_last_supp_cur"].ToString();

                            simobj.im_trade_cat = dr["im_trade_cat"].ToString();
                            simobj.trade_cat_name = dr["trade_cat_name"].ToString();

                            simobj.im_one_time_flag = dr["im_one_time_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_agency_flag = dr["im_agency_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_assembly_ind = dr["im_assembly_ind"].ToString().Equals("Y") ? true : false;
                            simobj.im_proprietary_flag = dr["im_proprietary_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_reusable_flag = dr["im_reusable_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_supersed_ind = dr["im_supersed_ind"].ToString().Equals("Y") ? true : false;
                            simobj.im_obsolete_excess_ind = dr["im_obsolete_excess_ind"].ToString().Equals("Y") ? true : false;
                            simobj.im_invest_flag = dr["im_invest_flag"].ToString().Equals("Y") ? true : false;
                            simobj.im_img = dr["im_img"].ToString();
                            simobj.im_status_ind = dr["im_status_ind"].ToString().Equals("Y") ? true : false;
                            simobj.im_status_date = dr["im_status_date"].ToString();
                            try
                            {
                                simobj.im_Vat_Percentage = Convert.ToDouble(dr["im_item_vat_percentage"].ToString());                                
                            }
                            catch (Exception ex)
                            {
                                simobj.im_Vat_Percentage = 0;
                            }
                            try
                            {
                                simobj.im_sell_price_including_vat = dr["im_sell_price_including_vat"].ToString();
                            }
                            catch (Exception ex)
                            {
                                simobj.im_sell_price_including_vat = "";
                            }
                            ItemMaster_list.Add(simobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("ItemMasterDetailsCUD")]
        public HttpResponseMessage ItemMasterDetailsCUD(List<Inv021> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv021 simsobj in data)
                    {


                        if (simsobj.sg_name == "" || simsobj.sg_name == "undefined")
                            simsobj.sg_name = null;
                        if (simsobj.im_model_name == "" || simsobj.im_model_name == "undefined")
                            simsobj.im_model_name = null;
                        if (simsobj.im_supl_catalog_price == "" || simsobj.im_supl_catalog_price == "undefined")
                            simsobj.im_supl_catalog_price = null;
                        if (simsobj.im_supl_min_qty == "" || simsobj.im_supl_min_qty == "undefined")
                            simsobj.im_supl_min_qty = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_supl_buying_price == "" || simsobj.im_supl_buying_price == "undefined")
                            simsobj.im_supl_buying_price = null;
                        if (simsobj.im_min_level == "" || simsobj.im_min_level == "undefined")
                            simsobj.im_min_level = null;
                        if (simsobj.im_max_level == "" || simsobj.im_max_level == "undefined")
                            simsobj.im_max_level = null;
                        if (simsobj.im_sell_price == "" || simsobj.im_sell_price == "undefined")
                            simsobj.im_sell_price = null;
                        if (simsobj.im_sell_price_old == "" || simsobj.im_sell_price_old == "undefined")
                            simsobj.im_sell_price_old = null;
                        if (simsobj.im_sellprice_special == "" || simsobj.im_sellprice_special == "undefined")
                            simsobj.im_sellprice_special = null;

                        if (simsobj.im_approximate_price == "" || simsobj.im_approximate_price == "undefined")
                            simsobj.im_approximate_price = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;

                        if (simsobj.im_sell_min_qty == "" || simsobj.im_sell_min_qty == "undefined")
                            simsobj.im_sell_min_qty = null;
                        if (simsobj.im_estimated_price == "" || simsobj.im_estimated_price == "undefined")
                            simsobj.im_estimated_price = null;
                        if (simsobj.im_no_of_packing == "" || simsobj.im_no_of_packing == "undefined")
                            simsobj.im_no_of_packing = null;


                        if (simsobj.im_status_date == "" || simsobj.im_status_date == "undefined")
                            simsobj.im_status_date = null;
                        if (simsobj.im_last_receipt_date == "" || simsobj.im_last_receipt_date == "undefined")
                            simsobj.im_last_receipt_date = null;
                        if (simsobj.im_stock_check_date == "" || simsobj.im_stock_check_date == "undefined")
                            simsobj.im_stock_check_date = null;
                        if (simsobj.im_creation_date == "" || simsobj.im_creation_date == "undefined")
                            simsobj.im_creation_date = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_last_issue_date == "" || simsobj.im_last_issue_date == "undefined")
                            simsobj.im_last_issue_date = null;


                        if (simsobj.im_trade_cat == "" || simsobj.im_trade_cat == "undefined")
                            simsobj.im_trade_cat = null;
                        if (simsobj.im_average_month_demand == "" || simsobj.im_average_month_demand == "undefined")
                            simsobj.im_average_month_demand = null;
                        if (simsobj.im_manufact_serial_no == "" || simsobj.im_manufact_serial_no == "undefined")
                            simsobj.im_manufact_serial_no = null;
                        if (simsobj.im_last_supp_cur == "" || simsobj.im_last_supp_cur == "undefined")
                            simsobj.im_last_supp_cur = null;
                        if (simsobj.im_last_supp_code == "" || simsobj.im_last_supp_code == "undefined")
                            simsobj.im_last_supp_code = null;


                        if (simsobj.im_supl_buying_price_old == "" || simsobj.im_supl_buying_price_old == "undefined")
                            simsobj.im_supl_buying_price_old = null;
                        if (simsobj.im_economic_order_qty == "" || simsobj.im_economic_order_qty == "undefined")
                            simsobj.im_economic_order_qty = null;
                        if (simsobj.im_rso_qty == "" || simsobj.im_rso_qty == "undefined")
                            simsobj.im_rso_qty = null;
                        if (simsobj.im_malc_rate_old == "" || simsobj.im_malc_rate_old == "undefined")
                            simsobj.im_malc_rate_old = null;
                        if (simsobj.im_approximate_lead_time == "" || simsobj.im_approximate_lead_time == "undefined")
                            simsobj.im_approximate_lead_time = null;
                        if (simsobj.im_sell_price_including_vat == "" || simsobj.im_sell_price_including_vat == "undefined")
                            simsobj.im_sell_price_including_vat = null; 

                        //int ins = db.ExecuteStoreProcedureforInsert("[invs].[item_master_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()                                                              
                        {
                      
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@im_inv_no",simsobj.im_inv_no),                                        
                          new SqlParameter("@sg_name",simsobj.sg_name),                                            
                          new SqlParameter("@im_item_code", simsobj.im_item_code),                                 
                          new SqlParameter("@im_desc", simsobj.im_desc),                             
                          new SqlParameter("@im_model_name", simsobj.im_model_name),                               
                          new SqlParameter("@dep_code",simsobj.dep_code),                                           
                          new SqlParameter("@sec_code", simsobj.sec_code),                                         
                          new SqlParameter("@pc_code", simsobj.pc_code),                                           
                          new SqlParameter("@sup_code", simsobj.sup_code),                                         
                          new SqlParameter("@uom_code",simsobj.uom_code),                                          
                          new SqlParameter("@uom_code_has", simsobj.uom_code_has),                       
                          new SqlParameter("@im_supl_catalog_price", simsobj.im_supl_catalog_price),  
                          new SqlParameter("@im_supl_price_date", db.DBYYYYMMDDformat(simsobj.im_supl_price_date)),   
                          new SqlParameter("@im_supl_min_qty",simsobj.im_supl_min_qty),                   
                          new SqlParameter("@im_supl_buying_price", simsobj.im_supl_buying_price),            
                          new SqlParameter("@im_supl_buying_price_old", simsobj.im_supl_buying_price_old),        
                          new SqlParameter("@im_sell_min_qty",simsobj.im_sell_min_qty),           
                          new SqlParameter("@im_min_level", simsobj.im_min_level),                 
                          new SqlParameter("@im_max_level", simsobj.im_max_level),                    
                          new SqlParameter("@im_reorder_level", simsobj.im_reorder_level),                  
                          new SqlParameter("@im_economic_order_qty",simsobj.im_economic_order_qty),                 
                          new SqlParameter("@im_rso_qty", simsobj.im_rso_qty),                        
                          new SqlParameter("@im_average_month_demand", simsobj.im_average_month_demand),    
                          new SqlParameter("@im_malc_rate", simsobj.im_malc_rate),                    
                          new SqlParameter("@im_malc_rate_old",simsobj.im_malc_rate_old),         
                          new SqlParameter("@im_mark_up",simsobj.im_mark_up),                  
                          new SqlParameter("@im_sell_price", simsobj.im_sell_price),                
                          new SqlParameter("@im_sell_price_old", simsobj.im_sell_price_old),              
                          new SqlParameter("@im_sellprice_special", simsobj.im_sellprice_special),          
                          new SqlParameter("@im_sellprice_freeze_ind",simsobj.im_sellprice_freeze_ind==true?"Y":"N"),
                          new SqlParameter("@im_approximate_price",simsobj.im_approximate_price),                
                          new SqlParameter("@im_estimated_price", simsobj.im_estimated_price),                 
                          new SqlParameter("@im_creation_date",db.DBYYYYMMDDformat(simsobj.im_creation_date)),               
                          new SqlParameter("@im_stock_check_date", db.DBYYYYMMDDformat(simsobj.im_stock_check_date)),       
                          new SqlParameter("@im_no_of_packing",simsobj.im_no_of_packing),    
                          new SqlParameter("@im_last_receipt_date", simsobj.im_last_receipt_date),   
                          new SqlParameter("@im_last_issue_date", simsobj.im_last_issue_date),                
                          new SqlParameter("@im_last_supp_code", simsobj.im_last_supp_code),                
                          new SqlParameter("@im_approximate_lead_time",simsobj.im_approximate_lead_time),          
                          new SqlParameter("@im_manufact_serial_no", simsobj.im_manufact_serial_no),                  
                          new SqlParameter("@im_last_supp_cur", simsobj.im_last_supp_cur),           
                          new SqlParameter("@im_trade_cat",simsobj.im_trade_cat),                       
                          new SqlParameter("@im_one_time_flag", simsobj.im_one_time_flag==true?"Y":"N"),           
                          new SqlParameter("@im_agency_flag", simsobj.im_agency_flag==true?"Y":"N"),          
                          new SqlParameter("@im_assembly_ind", simsobj.im_assembly_ind==true?"Y":"N"),              
                          new SqlParameter("@im_proprietary_flag",simsobj.im_proprietary_flag==true?"Y":"N"),       
                          new SqlParameter("@im_reusable_flag", simsobj.im_reusable_flag==true?"Y":"N"),           
                          new SqlParameter("@im_supersed_ind", simsobj.im_supersed_ind==true?"Y":"N"),             
                          new SqlParameter("@im_obsolete_excess_ind", simsobj.im_obsolete_excess_ind==true?"Y":"N"),
                          new SqlParameter("@im_invest_flag",simsobj.im_invest_flag==true?"Y":"N"),                 
                          new SqlParameter("@im_img", null),                         
                          new SqlParameter("@im_status_ind", "Y"),
                          new SqlParameter("@im_status_date", simsobj.im_status_date),
                          new SqlParameter("@im_sell_price_including_vat", simsobj.im_sell_price_including_vat),
                          

                    });

                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}
                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                         //   msg.strMessage = "Record cannot be deleted as it is already mapped.";
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                         //   msg.strMessage = "Record deleted successfully.";
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                //msg.strMessage = "Record cannot be deleted as it is already mapped.";
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("ItemMasterDetailsCUD_VAT")]
        public HttpResponseMessage ItemMasterDetailsCUD_VAT(List<Inv021> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv021 simsobj in data)
                    {

                        if (simsobj.sg_name == "" || simsobj.sg_name == "undefined")
                            simsobj.sg_name = null;
                        if (simsobj.im_model_name == "" || simsobj.im_model_name == "undefined")
                            simsobj.im_model_name = null;
                        if (simsobj.im_supl_catalog_price == "" || simsobj.im_supl_catalog_price == "undefined")
                            simsobj.im_supl_catalog_price = null;
                        if (simsobj.im_supl_min_qty == "" || simsobj.im_supl_min_qty == "undefined")
                            simsobj.im_supl_min_qty = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_supl_buying_price == "" || simsobj.im_supl_buying_price == "undefined")
                            simsobj.im_supl_buying_price = null;
                        if (simsobj.im_min_level == "" || simsobj.im_min_level == "undefined")
                            simsobj.im_min_level = null;
                        if (simsobj.im_max_level == "" || simsobj.im_max_level == "undefined")
                            simsobj.im_max_level = null;
                        if (simsobj.im_sell_price == "" || simsobj.im_sell_price == "undefined")
                            simsobj.im_sell_price = null;
                        if (simsobj.im_sell_price_old == "" || simsobj.im_sell_price_old == "undefined")
                            simsobj.im_sell_price_old = null;
                        if (simsobj.im_sellprice_special == "" || simsobj.im_sellprice_special == "undefined")
                            simsobj.im_sellprice_special = null;

                        if (simsobj.im_approximate_price == "" || simsobj.im_approximate_price == "undefined")
                            simsobj.im_approximate_price = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;
                        if (simsobj.im_reorder_level == "" || simsobj.im_reorder_level == "undefined")
                            simsobj.im_reorder_level = null;

                        if (simsobj.im_sell_min_qty == "" || simsobj.im_sell_min_qty == "undefined")
                            simsobj.im_sell_min_qty = null;
                        if (simsobj.im_estimated_price == "" || simsobj.im_estimated_price == "undefined")
                            simsobj.im_estimated_price = null;
                        if (simsobj.im_no_of_packing == "" || simsobj.im_no_of_packing == "undefined")
                            simsobj.im_no_of_packing = null;


                        if (simsobj.im_status_date == "" || simsobj.im_status_date == "undefined")
                            simsobj.im_status_date = null;
                        if (simsobj.im_last_receipt_date == "" || simsobj.im_last_receipt_date == "undefined")
                            simsobj.im_last_receipt_date = null;
                        if (simsobj.im_stock_check_date == "" || simsobj.im_stock_check_date == "undefined")
                            simsobj.im_stock_check_date = null;
                        if (simsobj.im_creation_date == "" || simsobj.im_creation_date == "undefined")
                            simsobj.im_creation_date = null;
                        if (simsobj.im_supl_price_date == "" || simsobj.im_supl_price_date == "undefined")
                            simsobj.im_supl_price_date = null;
                        if (simsobj.im_last_issue_date == "" || simsobj.im_last_issue_date == "undefined")
                            simsobj.im_last_issue_date = null;


                        if (simsobj.im_trade_cat == "" || simsobj.im_trade_cat == "undefined")
                            simsobj.im_trade_cat = null;
                        if (simsobj.im_average_month_demand == "" || simsobj.im_average_month_demand == "undefined")
                            simsobj.im_average_month_demand = null;
                        if (simsobj.im_manufact_serial_no == "" || simsobj.im_manufact_serial_no == "undefined")
                            simsobj.im_manufact_serial_no = null;
                        if (simsobj.im_last_supp_cur == "" || simsobj.im_last_supp_cur == "undefined")
                            simsobj.im_last_supp_cur = null;
                        if (simsobj.im_last_supp_code == "" || simsobj.im_last_supp_code == "undefined")
                            simsobj.im_last_supp_code = null;


                        if (simsobj.im_supl_buying_price_old == "" || simsobj.im_supl_buying_price_old == "undefined")
                            simsobj.im_supl_buying_price_old = null;
                        if (simsobj.im_economic_order_qty == "" || simsobj.im_economic_order_qty == "undefined")
                            simsobj.im_economic_order_qty = null;
                        if (simsobj.im_rso_qty == "" || simsobj.im_rso_qty == "undefined")
                            simsobj.im_rso_qty = null;
                        if (simsobj.im_malc_rate_old == "" || simsobj.im_malc_rate_old == "undefined")
                            simsobj.im_malc_rate_old = null;
                        if (simsobj.im_approximate_lead_time == "" || simsobj.im_approximate_lead_time == "undefined")
                            simsobj.im_approximate_lead_time = null;
                        if (simsobj.im_Vat_Percentage.ToString() == "" || simsobj.im_Vat_Percentage.ToString() == "undefined")
                            simsobj.im_Vat_Percentage = 0;
                        if (simsobj.im_sell_price_including_vat == "" || simsobj.im_sell_price_including_vat == "undefined")
                            simsobj.im_sell_price_including_vat = null;
                        //int ins = db.ExecuteStoreProcedureforInsert("[invs].[item_master_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc_1]",
                        new List<SqlParameter>()
                        {

                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                          new SqlParameter("@sg_name",simsobj.sg_name),
                          new SqlParameter("@im_item_code", simsobj.im_item_code),
                          new SqlParameter("@im_desc", simsobj.im_desc),
                          new SqlParameter("@im_model_name", simsobj.im_model_name),
                          new SqlParameter("@dep_code",simsobj.dep_code),
                          new SqlParameter("@sec_code", simsobj.sec_code),
                          new SqlParameter("@pc_code", simsobj.pc_code),
                          new SqlParameter("@sup_code", simsobj.sup_code),
                          new SqlParameter("@uom_code",simsobj.uom_code),
                          new SqlParameter("@uom_code_has", simsobj.uom_code_has),
                          new SqlParameter("@im_supl_catalog_price", simsobj.im_supl_catalog_price),
                          new SqlParameter("@im_supl_price_date", db.DBYYYYMMDDformat(simsobj.im_supl_price_date)),
                          new SqlParameter("@im_supl_min_qty",simsobj.im_supl_min_qty),
                          new SqlParameter("@im_supl_buying_price", simsobj.im_supl_buying_price),
                          new SqlParameter("@im_supl_buying_price_old", simsobj.im_supl_buying_price_old),
                          new SqlParameter("@im_sell_min_qty",simsobj.im_sell_min_qty),
                          new SqlParameter("@im_min_level", simsobj.im_min_level),
                          new SqlParameter("@im_max_level", simsobj.im_max_level),
                          new SqlParameter("@im_reorder_level", simsobj.im_reorder_level),
                          new SqlParameter("@im_economic_order_qty",simsobj.im_economic_order_qty),
                          new SqlParameter("@im_rso_qty", simsobj.im_rso_qty),
                          new SqlParameter("@im_average_month_demand", simsobj.im_average_month_demand),
                          new SqlParameter("@im_malc_rate", simsobj.im_malc_rate),
                          new SqlParameter("@im_malc_rate_old",simsobj.im_malc_rate_old),
                          new SqlParameter("@im_mark_up",simsobj.im_mark_up),
                          new SqlParameter("@im_sell_price", simsobj.im_sell_price),
                          new SqlParameter("@im_sell_price_old", simsobj.im_sell_price_old),
                          new SqlParameter("@im_sellprice_special", simsobj.im_sellprice_special),
                          new SqlParameter("@im_sellprice_freeze_ind",simsobj.im_sellprice_freeze_ind==true?"Y":"N"),
                          new SqlParameter("@im_approximate_price",simsobj.im_approximate_price),
                          new SqlParameter("@im_estimated_price", simsobj.im_estimated_price),
                          new SqlParameter("@im_creation_date",db.DBYYYYMMDDformat(simsobj.im_creation_date)),
                          new SqlParameter("@im_stock_check_date", db.DBYYYYMMDDformat(simsobj.im_stock_check_date)),
                          new SqlParameter("@im_no_of_packing",simsobj.im_no_of_packing),
                          new SqlParameter("@im_last_receipt_date", simsobj.im_last_receipt_date),
                          new SqlParameter("@im_last_issue_date", simsobj.im_last_issue_date),
                          new SqlParameter("@im_last_supp_code", simsobj.im_last_supp_code),
                          new SqlParameter("@im_approximate_lead_time",simsobj.im_approximate_lead_time),
                          new SqlParameter("@im_manufact_serial_no", simsobj.im_manufact_serial_no),
                          new SqlParameter("@im_last_supp_cur", simsobj.im_last_supp_cur),
                          new SqlParameter("@im_trade_cat",simsobj.im_trade_cat),
                          new SqlParameter("@im_one_time_flag", simsobj.im_one_time_flag==true?"Y":"N"),
                          new SqlParameter("@im_agency_flag", simsobj.im_agency_flag==true?"Y":"N"),
                          new SqlParameter("@im_assembly_ind", simsobj.im_assembly_ind==true?"Y":"N"),
                          new SqlParameter("@im_proprietary_flag",simsobj.im_proprietary_flag==true?"Y":"N"),
                          new SqlParameter("@im_reusable_flag", simsobj.im_reusable_flag==true?"Y":"N"),
                          new SqlParameter("@im_supersed_ind", simsobj.im_supersed_ind==true?"Y":"N"),
                          new SqlParameter("@im_obsolete_excess_ind", simsobj.im_obsolete_excess_ind==true?"Y":"N"),
                          new SqlParameter("@im_invest_flag",simsobj.im_invest_flag==true?"Y":"N"),
                          new SqlParameter("@im_img", null),
                          new SqlParameter("@im_status_ind", "Y"),
                          new SqlParameter("@im_status_date", simsobj.im_status_date),
                          new SqlParameter("@im_item_vat_percentage", simsobj.im_Vat_Percentage),
                          new SqlParameter("@im_sell_price_including_vat", simsobj.im_sell_price_including_vat),

                    });

                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}
                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                            //   msg.strMessage = "Record cannot be deleted as it is already mapped.";
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            //   msg.strMessage = "Record deleted successfully.";
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                //msg.strMessage = "Record cannot be deleted as it is already mapped.";
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        [Route("getChkitemPresent")]
        public HttpResponseMessage getChkitemPresent(string item_code)
        {
           // List<Inv021> Section_list = new List<Inv021>();
            bool ins = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_master_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Q"),
                            new SqlParameter("@im_item_code", item_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           // Inv021 obj = new Inv021();
                            if (int.Parse(dr["im_item_code"].ToString()) > 0)
                                ins = true;
                           // Section_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ins);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }
    }
}