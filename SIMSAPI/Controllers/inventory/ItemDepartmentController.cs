﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using log4net;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
     [RoutePrefix("api/ItemDepartment")]
    public class ItemDepartmentController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("getItemDepartments")]
         public HttpResponseMessage getItemDepartments(string dept)
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@dep", dept),
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsObj = new invs038();
                             if (string.IsNullOrEmpty(dr["im_inv_no"].ToString()) == false)
                             invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                             invsObj.im_item_code = dr["im_item_code"].ToString();
                             invsObj.im_desc = dr["im_desc"].ToString();
                             invsObj.dep_code = dr["dep_code"].ToString();
                             invsObj.dep_name = dr["dep_name"].ToString();
                             if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                 invsObj.id_cur_qty = int.Parse(dr["id_cur_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_do_qty"].ToString()) == false)
                                 invsObj.id_do_qty = int.Parse(dr["id_do_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_alloc_qty"].ToString()) == false)
                                 invsObj.id_alloc_qty = int.Parse(dr["id_alloc_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_yob_qty"].ToString()) == false)
                                 invsObj.id_yob_qty = int.Parse(dr["id_yob_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_foc_qty"].ToString()) == false)
                                 invsObj.id_foc_qty = int.Parse(dr["id_foc_qty"].ToString());

                             goaltarget_list.Add(invsObj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getItemDepartmentsData")]
         public HttpResponseMessage getItemDepartmentsData(string invno,string dept,string category,string subcategory)
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@im_inv_no", invno),
                              new SqlParameter("@dep_code", dept),
                            new SqlParameter("@pc_parent_code", category),
                            new SqlParameter("@pc_parent_code1", subcategory),
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsObj = new invs038();
                             if (string.IsNullOrEmpty(dr["im_inv_no"].ToString()) == false)
                                 invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                             invsObj.im_item_code = dr["im_item_code"].ToString();
                             invsObj.im_desc = dr["im_desc"].ToString();
                             invsObj.dep_code = dr["dep_code"].ToString();
                             invsObj.dep_name = dr["dep_name"].ToString();
                             if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                 invsObj.id_cur_qty = int.Parse(dr["id_cur_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_do_qty"].ToString()) == false)
                                 invsObj.id_do_qty = int.Parse(dr["id_do_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_alloc_qty"].ToString()) == false)
                                 invsObj.id_alloc_qty = int.Parse(dr["id_alloc_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_yob_qty"].ToString()) == false)
                                 invsObj.id_yob_qty = int.Parse(dr["id_yob_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["id_foc_qty"].ToString()) == false)
                                 invsObj.id_foc_qty = int.Parse(dr["id_foc_qty"].ToString());
                             invsObj.category_code = dr["pc_code"].ToString();
                             invsObj.category_name = dr["pc_desc"].ToString();
                             goaltarget_list.Add(invsObj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }


         [Route("getItemLocationData")]
         public HttpResponseMessage getItemLocationData(string invno, string location, string category, string subcategory)
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'G'),
                            new SqlParameter("@im_inv_no", invno),
                             new SqlParameter("@loc_code", location),
                            new SqlParameter("@pc_parent_code", category),
                            new SqlParameter("@pc_parent_code1", subcategory),
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsObj = new invs038();
                             if (string.IsNullOrEmpty(dr["im_inv_no"].ToString()) == false)
                                 invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                             invsObj.im_item_code = dr["im_item_code"].ToString();
                             invsObj.im_desc = dr["im_desc"].ToString();
                             invsObj.loc_code = dr["loc_code"].ToString();
                             invsObj.loc_name = dr["loc_name"].ToString();
                             if (string.IsNullOrEmpty(dr["il_cur_qty"].ToString()) == false)
                                 invsObj.id_cur_qty = int.Parse(dr["il_cur_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["il_do_qty"].ToString()) == false)
                                 invsObj.id_do_qty = int.Parse(dr["il_do_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["il_alloc_qty"].ToString()) == false)
                                 invsObj.id_alloc_qty = int.Parse(dr["il_alloc_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["il_yob_qty"].ToString()) == false)
                                 invsObj.id_yob_qty = int.Parse(dr["il_yob_qty"].ToString());

                             if (string.IsNullOrEmpty(dr["il_foc_qty"].ToString()) == false)
                                 invsObj.id_foc_qty = int.Parse(dr["il_foc_qty"].ToString());
                             invsObj.category_code = dr["pc_code"].ToString();
                             invsObj.category_name = dr["pc_desc"].ToString();
                             invsObj.il_bin_loc1 = dr["il_bin_loc1"].ToString();
                             invsObj.il_bin_loc2 = dr["il_bin_loc1"].ToString();
                             goaltarget_list.Add(invsObj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("CUDItemDepartments")]
         public HttpResponseMessage CUDItemDepartments(List<invs038> data)
         {
             bool insert = false;
             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (invs038 invsObj in data)
                     {
                         int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", invsObj.opr),
                                new SqlParameter("@im_inv_no", invsObj.im_inv_no),
                                new SqlParameter("@dep_code", invsObj.dep_code),
                                new SqlParameter("@id_cur_qty", invsObj.id_cur_qty),
                                new SqlParameter("@id_do_qty", invsObj.id_do_qty),
                                new SqlParameter("@id_alloc_qty", invsObj.id_alloc_qty),
                                new SqlParameter("@id_yob_qty", invsObj.id_yob_qty),
                                new SqlParameter("@id_foc_qty", invsObj.id_foc_qty),
                                
                     });
                         if (ins > 0)
                         {
                             insert = true;
                         }


                         else
                         {
                             insert = false;
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, insert);
                 }

             }


             catch (Exception x)
             {

                 /*message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;*/
                 return Request.CreateResponse(HttpStatusCode.OK, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, insert);
         }

         [Route("getDepartmentNames")]
         public HttpResponseMessage getDepartmentNames()
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'A'),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsobj = new invs038();
                             invsobj.dep_name = dr["dep_code"].ToString() + "-" + dr["dep_name"].ToString();
                             invsobj.dep_code = dr["dep_code"].ToString();
                             goaltarget_list.Add(invsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getLocationNames")]
         public HttpResponseMessage getLocationNames()
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'E'),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsobj = new invs038();
                             invsobj.loc_name = dr["loc_code"].ToString() + "-" + dr["loc_name"].ToString();
                             invsobj.loc_code = dr["loc_code"].ToString();
                             goaltarget_list.Add(invsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getCategories")]
         public HttpResponseMessage getCategories()
         {

            
             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'B'),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsobj = new invs038();
                             invsobj.category_name = dr["pc_code"].ToString() + "-" + dr["pc_desc"].ToString();
                             invsobj.category_code = dr["pc_code"].ToString();
                             goaltarget_list.Add(invsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getSubCategories")]
         public HttpResponseMessage getSubCategories(string pc_parentcode)
         {


             List<invs038> goaltarget_list = new List<invs038>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'c'),
                             new SqlParameter("@pc_parent_code", pc_parentcode),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             invs038 invsobj = new invs038();
                             invsobj.subcategory_name = dr["pc_code"].ToString() + "-" + dr["pc_desc"].ToString();
                             invsobj.subcategory_code = dr["pc_code"].ToString();
                             goaltarget_list.Add(invsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
         }

         [Route("getRoleofUser")]
         public HttpResponseMessage getRoleofUser(string username)
         {


             int rolecount = 0;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_departments_proc]",
                         new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'H'),
                             new SqlParameter("@username", username),
                           
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {

                             rolecount =Convert.ToInt32( dr["rolecount"].ToString());
                            
                         }
                     }
                 }

             }
             catch (Exception x)
             {

                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, rolecount);
             }

             return Request.CreateResponse(HttpStatusCode.OK, rolecount);
         }

    }
}