﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.inventoryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/SupplierGroup")]

    public class SupplierGroupController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        // This Show the list of record

        [Route("getSupplierGroups")]
        public HttpResponseMessage getSupplierGroups()
        {
            List<Inv011> group_list = new List<Inv011>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_groups_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv011 invsgobj = new Inv011();
                            invsgobj.sg_name = dr["sg_name"].ToString();
                            invsgobj.old_sg_name = dr["sg_name"].ToString();
                            invsgobj.sg_desc = dr["sg_desc"].ToString();
                            invsgobj.sg_type = dr["sg_type"].ToString();
                            invsgobj.sg_type_nm = dr["sg_type_nm"].ToString();

                            group_list.Add(invsgobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        [Route("getSupplierGroupsType")]
        public HttpResponseMessage getSupplierGroupsType()
        {
            List<Inv011> group_list = new List<Inv011>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_groups_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv011 invsgobj = new Inv011();
                            invsgobj.sg_type = dr["invs_appl_parameter"].ToString();
                            invsgobj.sg_type_nm = dr["invs_appl_form_field_value1"].ToString();

                            group_list.Add(invsgobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }


        // This is used for the CRUD operetions
        
        [Route("CUDSupplierGroups")]
        public HttpResponseMessage CUDSupplierGroups(List<Inv011> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv011 insert_invs in data)   // insert_invs
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_supplier_groups_proc]",
                            new List<SqlParameter>()
                         {

                           new SqlParameter("@opr", insert_invs.opr),       // insert_invs
                           new SqlParameter("@invs_sg_name", insert_invs.sg_name),      // insert_invs
                           new SqlParameter("@invs_sg_desc", insert_invs.sg_desc) ,      // insert_invs
                           new SqlParameter("@sg_type", insert_invs.sg_type)  ,     // insert_invs
                           new SqlParameter("@old_sg_name", insert_invs.old_sg_name)       // insert_invs
                         
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }                        
                    }

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }


}










