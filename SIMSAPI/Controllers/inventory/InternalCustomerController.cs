﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/common/InternalCustomer")]

    public class InternalCustomerController : ApiController
    {
        [Route("GetAll_internal_customers_status")]
        public HttpResponseMessage GetAll_internal_customers_status()
        {
            List<Invs036> com_list = new List<Invs036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_internal_customers_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs036 invs = new Invs036();
                            invs.ic_status = dr["invs_appl_form_field_value1"].ToString();
                            invs.ic_status_code = dr["invs_appl_parameter"].ToString();
                            com_list.Add(invs);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, com_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("Get_InternalCustomer")]
        public HttpResponseMessage Get_InternalCustomer()
        {
            List<Invs036> com_list = new List<Invs036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_internal_customers_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs036 invs = new Invs036();
                            invs.ic_account_no = dr["ic_account_no"].ToString();
                            invs.ic_account_name = dr["ic_account_name"].ToString();
                            invs.ic_status = dr["ic_status"].ToString();
                            invs.ic_status_code = dr["cust_status"].ToString();
                            com_list.Add(invs);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, com_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("CUDInternalCustomer")]
        public HttpResponseMessage CUDInternalCustomer(List<Invs036> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Invs036 invsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_internal_customers_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", invsobj.opr),
                                    new SqlParameter("@ic_account_no", invsobj.ic_account_no),
                                    new SqlParameter("@ic_account_name", invsobj.ic_account_name),
                                    new SqlParameter("@ic_status", invsobj.ic_status_code),
                            });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("InternalCustomerdetails_updatedel")]
        public HttpResponseMessage InternalCustomerdetails_updatedel(List<Invs036> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Invs036 invsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_internal_customers_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", invsobj.opr),
                                    new SqlParameter("@ic_account_no", invsobj.ic_account_no),
                                    new SqlParameter("@ic_account_name", invsobj.ic_account_name),
                                    new SqlParameter("@ic_status", invsobj.ic_status_code),
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}