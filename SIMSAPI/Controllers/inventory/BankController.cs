﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/Bank")]
    [BasicAuthentication]
    public class BankController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCountry")]
        public HttpResponseMessage getCountry()
        {
            List<Invs042> goaltarget_list = new List<Invs042>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_country]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs042 invsobj = new Invs042();
                            invsobj.con_code = dr["sims_country_code"].ToString();
                            invsobj.con_name = dr["sims_country_name_en"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getBank")]
        public HttpResponseMessage getBank()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBank(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getBank"));

            List<Invs042> goaltarget_list = new List<Invs042>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_banks_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs042 invsobj = new Invs042();
                            invsobj.bk_code = dr["bk_code"].ToString();
                            invsobj.bk_name = dr["bk_name"].ToString();
                            invsobj.bk_orig_code = dr["bk_orig_code"].ToString();
                            invsobj.bk_address1 = dr["bk_address1"].ToString();
                            invsobj.bk_address2 = dr["bk_address2"].ToString();
                            invsobj.bk_address3 = dr["bk_address3"].ToString();
                            invsobj.bk_city = dr["bk_city"].ToString();
                            invsobj.con_code = dr["con_code"].ToString();
                            invsobj.bk_phone_no = dr["bk_phone_no"].ToString();
                            invsobj.bk_fax_no = dr["bk_fax_no"].ToString();
                            invsobj.bk_telex_no = dr["bk_telex_no"].ToString();
                            invsobj.bk_contact_person = dr["bk_contact_person"].ToString();
                            invsobj.bk_designation = dr["bk_designation"].ToString();
                            invsobj.con_name = dr["con_name"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDBank")]
        public HttpResponseMessage CUDBank(List<Invs042> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs042 invsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_banks_proc]",
                        new List<SqlParameter>()
                     {
                         new SqlParameter("@opr", invsobj.opr),
                         new SqlParameter("@bk_code", invsobj.bk_code),
                         new SqlParameter("@bk_name", invsobj.bk_name),
                         new SqlParameter("@bk_orig_code", invsobj.bk_orig_code),
                         new SqlParameter("@bk_address1", invsobj.bk_address1),
                         new SqlParameter("@bk_address2", invsobj.bk_address2),
                         new SqlParameter("@bk_address3", invsobj.bk_address3),
                         new SqlParameter("@bk_city", invsobj.bk_city),
                         new SqlParameter("@con_code", invsobj.con_code),
                         new SqlParameter("@bk_phone_no", invsobj.bk_phone_no),
                         new SqlParameter("@bk_fax_no", invsobj.bk_fax_no),
                         new SqlParameter("@bk_telex_no", invsobj.bk_telex_no),
                         new SqlParameter("@bk_contact_person", invsobj.bk_contact_person),
                         new SqlParameter("@bk_designation", invsobj.bk_designation)                               
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }


                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}