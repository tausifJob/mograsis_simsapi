﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/SupplierDepartmentDetails")]
    public class SupplierDepartmentsController : ApiController
    {


        [Route("getSupplierGroupName")]
        public HttpResponseMessage getSupplierGroupName()
        {
            List<Inv008> SupplierDept_list = new List<Inv008>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_departments_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv008 obj = new Inv008();

                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();

                            SupplierDept_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SupplierDept_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SupplierDept_list);
        }

        [Route("ItemMasterDetailsCUD")]
        public HttpResponseMessage ItemMasterDetailsCUD(List<Inv008> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv008 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_supplier_departments_proc]",
                        new List<SqlParameter>()
                        {
                            
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sup_code",simsobj.sup_code),
                          new SqlParameter("@dep_code",simsobj.dep_code),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }


}










