﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/VehicleConsumptionDetail")]
    public class VehicleConsumptionDetailsController:ApiController
    {

        [Route("getDriverDetail")]
        public HttpResponseMessage getDriverDetail(string vehicle_code)
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "E"),
                            new SqlParameter("@vehicle_code",vehicle_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.vehicle_consumption_number = dr["vehicle_consumption_number"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.vehicle_code = dr["vehicle_code"].ToString();
                            obj.driver_code = dr["driver_code"].ToString();
                            obj.sims_driver_name = dr["sims_driver_name"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("getAllDrivers")]
        public HttpResponseMessage getAllDrivers(string loginuser)
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@driver_code",loginuser)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.sims_employee_code = dr["sims_employee_code"].ToString();
                            obj.driver_code = dr["sims_driver_code"].ToString();
                            obj.sims_driver_name = dr["sims_driver_name"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("getSupplierDetail")]
        public HttpResponseMessage getSupplierDetail()
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "A"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.cur_code = dr["cur_code"].ToString();
                            obj.sup_contact_designation = dr["sup_contact_designation"].ToString();
                            obj.sup_type = dr["sup_type"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("getSupplierLocation")]
        public HttpResponseMessage getSupplierLocation()
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "B")
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_location_code = dr["sup_location_code"].ToString();
                            obj.sup_location_desc = dr["sup_location_desc"].ToString();
                            obj.sup_location_address = dr["sup_location_address"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        //get vehicle Details
        
        [Route("getVehicleDetails")]
        public HttpResponseMessage getVehicleDetails(string driver_code)
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "C"),
                            new SqlParameter("@driver_code", driver_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            obj.vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("getVehicleConsumption")]
        public HttpResponseMessage getVehicleConsumption(string login_name, string from_date, string to_date, string location, string driver, string vehicle)
        {
            List<Inv200> SalesType = new List<Inv200>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_vehicle_consumption_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S"),
                            //new SqlParameter("@driver_code", login_name),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@sup_location_code", location),
                            new SqlParameter("@driver_code", driver),
                            new SqlParameter("@vehicle_code", vehicle),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv200 obj = new Inv200();
                            obj.vehicle_consumption_number = dr["vehicle_consumption_number"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.sup_location_code = dr["sup_location_code"].ToString();
                            obj.sup_location_address = dr["sup_location_address"].ToString();
                            obj.vehicle_code = dr["vehicle_code"].ToString();
                            obj.vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            obj.im_inv_no = dr["im_inv_no"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sale_time"].ToString()))
                            //    obj.sale_time = DateTime.Parse(dr["sale_time"].ToString()).ToShortDateString();
                            obj.sale_time = dr["sale_time"].ToString();
                            obj.unit_price = dr["unit_price"].ToString();
                            obj.sale_qty = dr["sale_qty"].ToString();
                            obj.sale_period = dr["sale_period"].ToString();
                            obj.driver_code = dr["driver_code"].ToString();
                            obj.sims_driver_name = dr["sims_driver_name"].ToString();
                            obj.recorded_miles_reading = dr["recorded_miles_reading"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("VehicleConsumtionCUD")]
        public HttpResponseMessage VehicleConsumtionCUD(List<Inv200> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv200 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_vehicle_consumption_details]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@vehicle_consumption_number", simsobj.vehicle_consumption_number),
                                 new SqlParameter("@sup_code", simsobj.sup_code),
                                 new SqlParameter("@sup_location_code", simsobj.sup_location_code),
                                 new SqlParameter("@vehicle_code", simsobj.vehicle_code),
                                 new SqlParameter("@im_inv_no", simsobj.im_inv_no),
                                 new SqlParameter("@sale_time", simsobj.sale_time),
                                 new SqlParameter("@unit_price", simsobj.unit_price),
                                 new SqlParameter("@sale_qty", simsobj.sale_qty),
                                 new SqlParameter("@sale_period", simsobj.sale_period),
                                 new SqlParameter("@driver_code", simsobj.driver_code),
                                 new SqlParameter("@recorded_miles_reading", simsobj.recorded_miles_reading),
                            });


                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}