﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/Supplier")]
    [BasicAuthentication]
    public class SupplierMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCountry")]
        public HttpResponseMessage getCountry()
        {
            List<Invs006> goaltarget_list = new List<Invs006>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_country_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.con_code = dr["sims_country_code"].ToString();
                            invsobj.con_name = dr["sims_country_name_en"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("GetAllCity")]
        public HttpResponseMessage GetAllCity()
        {
            List<Invs006> goaltarget_list = new List<Invs006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_city_proc]",
                        new List<SqlParameter>()
                         {
                          new SqlParameter ("@opr", 'S'),
                          //new SqlParameter ("@sims_state_code", state_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.sup_city = dr["sims_city_code"].ToString();
                            invsobj.sup_city_name = dr["sims_city_name_en"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetAllSupplierGroupName")]
        public HttpResponseMessage GetAllSupplierGroupName()
        {
            List<Invs006> goaltarget_list = new List<Invs006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_groups_proc]",
                        new List<SqlParameter>()
                         {
                          new SqlParameter ("@opr", 'S'),                          

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.sg_name = dr["sg_name"].ToString();
                            invsobj.sg_desc = dr["sg_desc"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSupplierType")]
        public HttpResponseMessage GetSupplierType()
        {
            List<Invs006> goaltarget_list = new List<Invs006>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_suppliers_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.sup_type = dr["invs_appl_parameter"].ToString();
                            invsobj.sup_type_name = dr["invs_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSupplierLocation")]
        public HttpResponseMessage GetSupplierLocation()
        {
            List<Invs006> goaltarget_list = new List<Invs006>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_suppliers_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.sup_location_ind = dr["invs_appl_parameter"].ToString();
                            invsobj.sup_location_ind_name = dr["invs_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_Currency")]
        public HttpResponseMessage Get_Currency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Currency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_Currency"));

            List<Invs006> goaltarget_list = new List<Invs006>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_currency_master]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.cur_code = dr["excg_curcy_code"].ToString();
                            invsobj.cur_name = dr["excg_curcy_desc"].ToString();

                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_SblgrAc")]
        public HttpResponseMessage Get_SblgrAc(string lgcode, string sblac, string supname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_SblgrAc(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_SblgrAc"));
            List<Invs006> goaltarget_list = new List<Invs006>();

            if (lgcode == "undefined" || lgcode == "\"\"")
            {
                lgcode = null;
            }
            if (sblac == "undefined" || sblac == "\"\"")
            {
                sblac = null;
            }
            if (supname == "undefined" || supname == "\"\"")
            {
                supname = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_ledger_acc_control_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           new SqlParameter("@slac_ldgr_code", lgcode),
                           new SqlParameter("@slac_ctrl_acno", sblac),
                           new SqlParameter("@slac_cntrl_class_desc", supname),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsobj = new Invs006();
                            invsobj.slac_ldgr_code = dr["slac_ldgr_code"].ToString();
                            invsobj.sup_sblgr_acno = dr["slac_ctrl_acno"].ToString();
                            invsobj.sup_sblgr_name = dr["slac_cntrl_class_desc"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSupplier")]
        public HttpResponseMessage getSupplier()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSupplier(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSupplier"));

            List<Invs006> goaltarget_list = new List<Invs006>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_suppliers_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs006 invsObj = new Invs006();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.sg_desc = dr["sg_desc"].ToString();
                            invsObj.sup_name = dr["sup_name"].ToString();
                            invsObj.cur_code = dr["cur_code"].ToString();
                            invsObj.cur_name = dr["cur_name"].ToString();
                            invsObj.sup_location_ind = dr["sup_location_ind"].ToString();
                            invsObj.sup_location_ind_name = dr["sup_location_ind_name"].ToString();
                            invsObj.sup_orig_code = dr["sup_orig_code"].ToString();
                            invsObj.sup_contact_name = dr["sup_contact_name"].ToString();
                            invsObj.sup_contact_designation = dr["sup_contact_designation"].ToString();
                            invsObj.sup_address1 = dr["sup_address1"].ToString();
                            invsObj.sup_address2 = dr["sup_address2"].ToString();
                            invsObj.sup_address3 = dr["sup_address3"].ToString();
                            // invsObj.sup_state = dr["sup_state"].ToString();
                            invsObj.sup_city = dr["sup_city"].ToString();
                            invsObj.sup_city_name = dr["sup_city_name"].ToString();
                            invsObj.con_code = dr["con_code"].ToString();
                            invsObj.con_name = dr["con_name"].ToString();
                            invsObj.sup_tel_no = dr["sup_tel_no"].ToString();
                            invsObj.sup_fax_no = dr["sup_fax_no"].ToString();
                            invsObj.sup_telex_no = dr["sup_telex_no"].ToString();
                            invsObj.sup_mobile_no = dr["sup_mobile_no"].ToString();
                            invsObj.sup_email_address = dr["sup_email_address"].ToString();
                            invsObj.sup_web_site = dr["sup_web_site"].ToString();
                            if (string.IsNullOrEmpty(dr["sup_credit_period"].ToString()) == false)
                                invsObj.sup_credit_period = int.Parse(dr["sup_credit_period"].ToString());
                            if (string.IsNullOrEmpty(dr["sup_discount_pct"].ToString()) == false)
                                invsObj.sup_discount_pct = int.Parse(dr["sup_discount_pct"].ToString());

                            if (string.IsNullOrEmpty(dr["sup_lead_time_air"].ToString()) == false)
                                invsObj.sup_lead_time_air = int.Parse(dr["sup_lead_time_air"].ToString());

                            if (string.IsNullOrEmpty(dr["sup_lead_time_sea"].ToString()) == false)
                                invsObj.sup_lead_time_sea = int.Parse(dr["sup_lead_time_sea"].ToString());

                            if (string.IsNullOrEmpty(dr["sup_lead_time_road"].ToString()) == false)
                                invsObj.sup_lead_time_road = int.Parse(dr["sup_lead_time_road"].ToString());

                            if (string.IsNullOrEmpty(dr["sup_lead_time_courier"].ToString()) == false)
                                invsObj.sup_lead_time_courier = int.Parse(dr["sup_lead_time_courier"].ToString());

                            invsObj.sup_type = dr["sup_type"].ToString();
                            invsObj.sup_type_name = dr["sup_type_name"].ToString();

                            if (string.IsNullOrEmpty(dr["sup_agent_comm_pct"].ToString()) == false)
                                invsObj.sup_agent_comm_pct = decimal.Parse(dr["sup_agent_comm_pct"].ToString());

                            invsObj.sup_remarks = dr["sup_remarks"].ToString();
                            invsObj.sup_rating = dr["sup_rating"].ToString();
                            invsObj.sup_sblgr_acno = dr["sup_sblgr_acno"].ToString();
                            invsObj.sup_sblgr_name = dr["sup_sblgr_name"].ToString();
                            invsObj.supplier_desc_type = dr["supplier_desc_type"].ToString();
                            try
                            {
                                invsObj.sup_contact_person = dr["sup_location_contact_person"].ToString();
                                invsObj.sup_location_desc = dr["sup_location_desc"].ToString();
                                invsObj.sup_location_address = dr["sup_location_address"].ToString();
                                invsObj.sup_location_landmark = dr["sup_location_landmark"].ToString();
                            }
                            catch (Exception ex)
                            {
                                
                            }
                            goaltarget_list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDSupplier")]
        public HttpResponseMessage CUDSupplier(List<Invs006> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs006 invsObj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_suppliers_proc]",
                        new List<SqlParameter>()
                     {
                        new SqlParameter("@opr", invsObj.opr),
                        new SqlParameter("@sup_code", invsObj.sup_code),
                        new SqlParameter("@sg_name", invsObj.sg_name),
                        new SqlParameter("@sup_name", invsObj.sup_name),
                        new SqlParameter("@cur_code", invsObj.cur_code),
                        new SqlParameter("@sup_location_ind", invsObj.sup_location_ind),
                        new SqlParameter("@sup_orig_code", invsObj.sup_orig_code),
                        new SqlParameter("@sup_contact_name", invsObj.sup_contact_name),
                        new SqlParameter("@sup_contact_designation", invsObj.sup_contact_designation),
                        new SqlParameter("@sup_address1", invsObj.sup_address1),
                        new SqlParameter("@sup_address2", invsObj.sup_address2),
                        new SqlParameter("@sup_address3", invsObj.sup_address3),
                        new SqlParameter("@sup_city", invsObj.sup_city),
                        new SqlParameter("@con_code", invsObj.con_code),
                        new SqlParameter("@sup_tel_no", invsObj.sup_tel_no),
                        new SqlParameter("@sup_fax_no", invsObj.sup_fax_no),
                        new SqlParameter("@sup_telex_no", invsObj.sup_telex_no),
                        new SqlParameter("@sup_mobile_no", invsObj.sup_mobile_no),
                        new SqlParameter("@sup_email_address", invsObj.sup_email_address),
                        new SqlParameter("@sup_web_site", invsObj.sup_web_site),
                        new SqlParameter("@sup_merchandise_desc", ""),
                        new SqlParameter("@sup_credit_period", invsObj.sup_credit_period),
                        new SqlParameter("@sup_discount_pct", invsObj.sup_discount_pct),
                        new SqlParameter("@sup_lead_time_air", invsObj.sup_lead_time_air),
                        new SqlParameter("@sup_lead_time_sea", invsObj.sup_lead_time_sea),
                        new SqlParameter("@sup_lead_time_road", invsObj.sup_lead_time_road),
                        new SqlParameter("@sup_lead_time_courier", invsObj.sup_lead_time_courier),
                        new SqlParameter("@sup_type", invsObj.sup_type),
                        new SqlParameter("@sup_agent_comm_pct", invsObj.sup_agent_comm_pct),
                        new SqlParameter("@sup_remarks", invsObj.sup_remarks),
                        new SqlParameter("@sup_rating", invsObj.sup_rating),
                        new SqlParameter("@sup_sblgr_acno", invsObj.sup_sblgr_acno),
                        new SqlParameter("@supplier_desc_type", invsObj.supplier_desc_type),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("Get_SupplierID")]
        public HttpResponseMessage Get_SupplierID()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_SupplierID(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_SupplierID"));

            Invs006 invsObj = new Invs006();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_suppliers_proc]",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr", 'M')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {                         
                            invsObj.sup_code = dr["sup_code"].ToString();                            
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);
        }


    }
}