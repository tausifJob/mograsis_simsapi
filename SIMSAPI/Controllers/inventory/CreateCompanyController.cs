﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.CreateCompanyController
{
    [RoutePrefix("api/createcompany")]
    [BasicAuthentication]
    public class CreateCompanyController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllCompany")]
        public HttpResponseMessage getAllCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllCompany"));

            List<Inv018> desg_list = new List<Inv018>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_company_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv018 simsobj = new Inv018();
                            simsobj.com_code = dr["com_code"].ToString();
                            simsobj.comp_name = dr["comp_name"].ToString();

                            simsobj.cur_code = dr["cur_code"].ToString();
                            simsobj.excg_curc_desc = dr["excg_curc_desc"].ToString();

                            simsobj.com_name = dr["com_name"].ToString();
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrency()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCurrency"));

            List<Inv018> mod_list = new List<Inv018>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_company_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv018 simsobj = new Inv018();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curc_desc = dr["excg_curcy_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompany()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCompany"));

            List<Inv018> mod_list = new List<Inv018>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_company_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv018 simsobj = new Inv018();
                            simsobj.comp_code = dr["comp_code"].ToString();
                            simsobj.comp_name = dr["comp_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDCompany")]
        public HttpResponseMessage CUDCompany(List<Inv018> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDCompany(),PARAMETERS";
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv018 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_company_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@com_code", simsobj.comp_code),
                                new SqlParameter("@cur_code", simsobj.excg_curcy_code),
                                new SqlParameter("@com_name", simsobj.com_name),
                               
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
