﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.CompanyParameterController
{
    [RoutePrefix("api/companyparameter")]
    [BasicAuthentication]
    public class CompanyParameterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllCompanyParameter")]
        public HttpResponseMessage getAllCompanyParameter()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCompanyParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllCompanyParameter"));

            List<Inv020> para_list = new List<Inv020>();
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[company_parameters_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv020 simsobj = new Inv020();
                            simsobj.comp_para_code = dr["cp_code"].ToString();
                            simsobj.comp_para_desc = dr["cp_desc"].ToString();
                            simsobj.comp_para_value = dr["cp_value"].ToString();
                            para_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, para_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, para_list);
        }

        [Route("CUDCompanyParameter")]
        public HttpResponseMessage CUDCompanyParameter(List<Inv020> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDCompanyParameter(),PARAMETERS :: OPR {2}";
            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv020 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[company_parameters_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@cp_code", simsobj.comp_para_code),
                                new SqlParameter("@cp_desc", simsobj.comp_para_desc),
                                 new SqlParameter("@cp_value", simsobj.comp_para_value),
                              
                                
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
