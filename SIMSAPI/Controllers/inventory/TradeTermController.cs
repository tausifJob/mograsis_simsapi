﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/TradeTerm")]
    [BasicAuthentication]
    public class TradeTermController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        [Route("GetTradeTerm")]
        public HttpResponseMessage GetTradeTerm()
        {
        string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTradeTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetTradeTerm"));

            List<Invs064> goaltarget_list = new List<Invs064>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Invs_trade_terms_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs064 invsobj = new Invs064();
                            invsobj.trt_code = dr["trt_code"].ToString();
                            invsobj.trt_desc = dr["trt_desc"].ToString();

                            goaltarget_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDTradeTerm")]
        public HttpResponseMessage CUDTradeTerm(List<Invs064> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs064 invsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[Invs_trade_terms_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", invsobj.opr),
                                new SqlParameter("@trt_code", invsobj.trt_code),
                                new SqlParameter("@trt_desc", invsobj.trt_desc),                                                    
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }


                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}