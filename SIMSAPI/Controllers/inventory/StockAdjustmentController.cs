﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/StockAdjustment")]
    //[BasicAuthentication]

    public class StockAdjustmentController : ApiController
    {
        //private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region API For Stock Adjustment....
        /*
        [Route("getAllLocation")]
        public HttpResponseMessage getAllLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "A"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.loc_code = dr["loc_code"].ToString();
                            obj.loc_name = dr["loc_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllSupplierName")]
        public HttpResponseMessage getAllSupplierName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "B"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.sup_sblgr_acno = dr["sup_sblgr_acno"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllReason")]
        public HttpResponseMessage getAllReason()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "C"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.ar_code = dr["ar_code"].ToString();
                            obj.ar_desc = dr["ar_desc"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllType")]
        public HttpResponseMessage getAllType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "E"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.dco_code = dr["dco_code"].ToString();
                            obj.dco_desc = dr["dco_desc"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllDepartment")]
        public HttpResponseMessage getAllDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "Q"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getsearchItemss")]
        public HttpResponseMessage getsearchItemss(string itemcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getsearchItemss(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Invs047_it> list = new List<Invs047_it>();
            //Invs047_it obj = new Invs047_it();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "F"),
                           new SqlParameter("@itemcode", itemcode),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs047_it obj = new Invs047_it();
                            obj.im_inv_no = dr["im_inv_no"].ToString();
                            obj.im_desc = dr["im_desc"].ToString();
                            obj.im_malc_rate = dr["im_malc_rate"].ToString();
                            obj.id_cur_qty = dr["id_cur_qty"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getsuppliergroupname")]
        public HttpResponseMessage getsuppliergroupname()
        {

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() 
                         { 
              
                            new SqlParameter("@opr", "G"),
              
                         });

                    while (dr.Read())
                    {
                        invs047 invsobj = new invs047();
                        invsobj.name = dr["sg_desc"].ToString();
                        invsobj.code = dr["sg_name"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getCategories")]
        public HttpResponseMessage getCategories()
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                         {
                             new SqlParameter("@opr", "H")
                         });

                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.category_name = dr["pc_desc"].ToString();
                        invsobj.category_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getSubCategories")]
        public HttpResponseMessage getSubCategories(string pc_pcode)
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",

                     new List<SqlParameter>() 
                     { 
              
                        new SqlParameter("@opr", "J"),
                        new SqlParameter("@pc_parent_code", pc_pcode)
                     });

                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.subcategory_name = dr["pc_desc"].ToString();
                        invsobj.subcategory_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("postgetItemSerch")]
        public HttpResponseMessage postgetItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                 new SqlParameter("@opr", opr),
                 new SqlParameter("@im_inv_no", itsr.im_inv_no),
                 new SqlParameter("@sg_name", itsr.invs021_sg_name),
                 new SqlParameter("@im_item_code", itsr.im_item_code),
                 new SqlParameter("@im_desc", itsr.im_desc),
                 new SqlParameter("@dep_code", itsr.invs021_dep_code),
                 new SqlParameter("@sec_code", itsr.sec_code),
                 new SqlParameter("@sup_code", itsr.invs021_sup_code),
                 new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                 new SqlParameter("@category_code", itsr.category_code),
                 new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();


                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Insertadjustments")]
        public HttpResponseMessage Insertadjustments(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@ar_code", obj.ar_code),
                            //new SqlParameter("@dt_code",obj.dco_code),
                            new SqlParameter("@dt_code",obj.dep_code),
                            new SqlParameter("@adj_grv_no",obj.adj_grv_no),
                            new SqlParameter("@adj_date",obj.tdate),
                            new SqlParameter("@loc_code",obj.loc_code),
                            new SqlParameter("@loc_code_to",null),
                            new SqlParameter("@adj_doc_no_reversal_of",null),
                            new SqlParameter("@adj_status",'1'),
                            new SqlParameter("@remarks",obj.remark),
                            new SqlParameter("@sup_sblgr_avno",obj.sup_sblgr_acno),

                        });

                        if (dr.Read())
                        {
                            st = dr["dp_value"].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("Insertadjustmentsdetails")]
        public HttpResponseMessage Insertadjustmentsdetails(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                int i = 0;
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        i = i + 1;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@adj_doc_no",obj.ad_line_no),
                            new SqlParameter("@ad_line_no",i),
                            new SqlParameter("@im_inv_no",obj.adj_inv_no),
                            new SqlParameter("@loc_code",obj.loc_code),
                            new SqlParameter("@dt_code",obj.dep_code),           
                            new SqlParameter("@ad_qty",obj.oldQty),
                            new SqlParameter("@ad_value",obj.oldVal),
                            //new SqlParameter("@ad_qty",obj.newQty),
                            //new SqlParameter("@ad_value",obj.newValue),
                            
                            new SqlParameter("@ad_recvd_qty",null),
                            new SqlParameter("@remarks",obj.remark),
                            
                            new SqlParameter("@cal_qty",obj.newQty),
                            new SqlParameter("@malc_rate",obj.newValue),
                            //new SqlParameter("@cal_qty",obj.oldQty),
                            //new SqlParameter("@malc_rate",obj.oldVal),
                            new SqlParameter("@adj_date",obj.tdate),
                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
        */
        #endregion

        #region API For Stock Adjustment...

        [Route("getAllLocation")]
        public HttpResponseMessage getAllLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "A"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.loc_code = dr["loc_code"].ToString();
                            obj.loc_name = dr["loc_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllSupplierName")]
        public HttpResponseMessage getAllSupplierName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "B"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.sup_sblgr_acno = dr["sup_sblgr_acno"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllReason")]
        public HttpResponseMessage getAllReason()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "C"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.ar_code = dr["ar_code"].ToString();
                            obj.ar_desc = dr["ar_desc"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllType")]
        public HttpResponseMessage getAllType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "E"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.dco_code = dr["dco_code"].ToString();
                            obj.dco_desc = dr["dco_desc"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAllTypeR")]
        public HttpResponseMessage getAllTypeR()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "O"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.dco_code = dr["dco_code"].ToString();
                            obj.dco_desc = dr["dco_desc"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllDepartment")]
        public HttpResponseMessage getAllDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "Q"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getsearchItemss")]
        public HttpResponseMessage getsearchItemss(string itemcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getsearchItemss(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Invs047_it> list = new List<Invs047_it>();
            //Invs047_it obj = new Invs047_it();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "F"),
                           new SqlParameter("@itemcode", itemcode),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs047_it obj = new Invs047_it();
                            obj.im_inv_no = dr["im_inv_no"].ToString();
                            obj.im_desc = dr["im_desc"].ToString();
                            obj.im_malc_rate = dr["im_malc_rate"].ToString();
                            obj.id_cur_qty = dr["id_cur_qty"].ToString();
                            obj.im_malc_rate = dr["im_malc_rate"].ToString();
                            obj.im_malc_rate_old = dr["im_malc_rate_old"].ToString();
                            obj.im_sell_price = dr["im_sell_price"].ToString();
                            obj.im_sell_price_old = dr["im_sell_price_old"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getsuppliergroupname")]
        public HttpResponseMessage getsuppliergroupname()
        {

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() 
                         { 
              
                            new SqlParameter("@opr", "G"),
              
                         });

                    while (dr.Read())
                    {
                        invs047 invsobj = new invs047();
                        invsobj.name = dr["sg_desc"].ToString();
                        invsobj.code = dr["sg_name"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getCategories")]
        public HttpResponseMessage getCategories()
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                         {
                             new SqlParameter("@opr", "H")
                         });

                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.category_name = dr["pc_desc"].ToString();
                        invsobj.category_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getSubCategories")]
        public HttpResponseMessage getSubCategories(string pc_pcode)
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",

                     new List<SqlParameter>() 
                     { 
              
                        new SqlParameter("@opr", "J"),
                        new SqlParameter("@pc_parent_code", pc_pcode)
                     });

                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.subcategory_name = dr["pc_desc"].ToString();
                        invsobj.subcategory_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("postgetItemSerch")]
        public HttpResponseMessage postgetItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                 new SqlParameter("@opr", opr),
                 new SqlParameter("@im_inv_no", itsr.im_inv_no),
                 new SqlParameter("@sg_name", itsr.invs021_sg_name),
                 new SqlParameter("@im_item_code", itsr.im_item_code),
                 new SqlParameter("@im_desc", itsr.im_desc),
                 new SqlParameter("@dep_code", itsr.invs021_dep_code),
                 new SqlParameter("@sec_code", itsr.sec_code),
                 new SqlParameter("@sup_code", itsr.invs021_sup_code),
                 new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                 new SqlParameter("@category_code", itsr.category_code),
                 new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();


                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Insertadjustments")]
        public HttpResponseMessage Insertadjustments(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@ar_code", obj.ar_code),
                            new SqlParameter("@dt_code",obj.dco_code),
                            new SqlParameter("@dep_code", obj.dep_code),
                            new SqlParameter("@adj_grv_no",obj.adj_grv_no),
                            new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                            new SqlParameter("@loc_code",obj.loc_code),
                            new SqlParameter("@loc_code_to",null),
                            new SqlParameter("@adj_doc_no_reversal_of",null),
                            new SqlParameter("@adj_status",'1'),
                            new SqlParameter("@remarks",obj.remark),
                            new SqlParameter("@sup_sblgr_avno",obj.sup_sblgr_acno),

                        });

                        if (dr.Read())
                        {
                            st = dr["dp_value"].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("Insertadjustmentsdetails")]
        public HttpResponseMessage Insertadjustmentsdetails(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                int i = 0;
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        i = i + 1;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@adj_doc_no",obj.ad_line_no),
                            new SqlParameter("@ad_line_no",i),
                            new SqlParameter("@im_inv_no",obj.adj_inv_no),
                            new SqlParameter("@loc_code",obj.loc_code),
                            new SqlParameter("@dep_code", obj.dep_code),
                            new SqlParameter("@ad_qty",obj.newQty),
                            new SqlParameter("@ad_value",obj.newValue),
                            //new SqlParameter("@ad_qty_new",obj.newQty),
                            //new SqlParameter("@ad_value_new",obj.newValue),
                           
                            new SqlParameter("@ad_recvd_qty",null),
                            new SqlParameter("@remarks",obj.remark),
                            
                            new SqlParameter("@cal_qty",obj.oldQty),
                            new SqlParameter("@malc_rate",obj.new_malc),
                            //new SqlParameter("@cal_qty",obj.oldQty),
                            //new SqlParameter("@malc_rate",obj.oldVal),
                            new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("Removeadjustments")]
        public HttpResponseMessage Removeadjustments(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                               
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@ar_code", obj.ar_code),
                                new SqlParameter("@dt_code",obj.dco_code),
                                new SqlParameter("@dep_code", obj.dep_code),
                                new SqlParameter("@adj_grv_no",obj.adj_grv_no),
                                new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                                new SqlParameter("@loc_code",obj.loc_code),
                                new SqlParameter("@loc_code_to",null),
                                new SqlParameter("@adj_doc_no_reversal_of",null),
                                new SqlParameter("@adj_status",'1'),
                                new SqlParameter("@remarks",obj.remark),
                                new SqlParameter("@sup_sblgr_avno",obj.sup_sblgr_acno),
                        });

                        if (dr.Read())
                        {
                            st = dr["dp_value"].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("Removeadjustmentsdetails")]
        public HttpResponseMessage Removeadjustmentsdetails(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                int i = 0;
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        i = i + 1;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                            new List<SqlParameter>() { 
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@adj_doc_no",obj.ad_line_no),
                                new SqlParameter("@ad_line_no",i),
                                new SqlParameter("@dep_code", obj.dep_code),
                                new SqlParameter("@im_inv_no",obj.adj_inv_no),
                                new SqlParameter("@loc_code",obj.loc_code),
                                new SqlParameter("@ad_qty",obj.newQty),
                                new SqlParameter("@ad_value",obj.newValue),
                                new SqlParameter("@ad_recvd_qty",null),
                                new SqlParameter("@remarks",obj.remark),
                                new SqlParameter("@cal_qty",obj.oldQty),
                                new SqlParameter("@malc_rate",obj.oldVal),
                                new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        #endregion

        #region API For Supplier Location...

        [Route("getAllSupplierNameCode")]
        public HttpResponseMessage getAllSupplierNameCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<Invs669> list = new List<Invs669>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[supplier_locations_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "A"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs669 obj = new Invs669();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSupplierItemLocation")]
        public HttpResponseMessage getSupplierItemLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Invs669> mod_list = new List<Invs669>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[supplier_locations_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            //new SqlParameter("@grade_code", grd_code),
                            //new SqlParameter("@section_code", sec_code),
                            //new SqlParameter("@start_date", start_date),
                            //new SqlParameter("@end_date", end_date),
                            //new SqlParameter("@Search", user_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs669 simsobj = new Invs669();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_location_code = dr["sup_location_code"].ToString();
                            simsobj.sup_location_desc = dr["sup_location_desc"].ToString();
                            simsobj.sup_location_address = dr["sup_location_address"].ToString();
                            simsobj.sup_location_landmark = dr["sup_location_landmark"].ToString();
                            simsobj.sup_location_contact_no = dr["sup_location_contact_no"].ToString();
                            simsobj.sup_location_contact_person = dr["sup_location_contact_person"].ToString();
                            if (dr["sup_location_status"].ToString().Equals("A"))
                                simsobj.sup_location_status = true;
                            else
                                simsobj.sup_location_status = false;

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDSupplierLocation")]
        public HttpResponseMessage CUDSupplierLocation(List<Invs669> data)
        {
          
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs669 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[supplier_locations_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sup_code", simsobj.sup_code),
                            new SqlParameter("@sup_location_code", simsobj.sup_location_code),
                            new SqlParameter("@sup_location_desc", simsobj.sup_location_desc),
                            new SqlParameter("@sup_location_address", simsobj.sup_location_address),
                            new SqlParameter("@sup_location_landmark", simsobj.sup_location_landmark),
                            new SqlParameter("@sup_location_contact_no", simsobj.sup_location_contact_no),
                            new SqlParameter("@sup_location_contact_person", simsobj.sup_location_contact_person),
                            new SqlParameter("@sup_location_status",simsobj.sup_location_status==true?"A":"I"),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        /*
        [Route("getSupplierItemLocationSuppiler")]
        public HttpResponseMessage getSupplierItemLocationSuppiler(string sup_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Invs669> mod_list = new List<Invs669>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[supplier_locations_proc]",
                        new List<SqlParameter>() { 
                            
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sup_code", sup_code),
                            //new SqlParameter("@grade_code", grd_code),
                            //new SqlParameter("@section_code", sec_code),
                            //new SqlParameter("@start_date", start_date),
                            //new SqlParameter("@end_date", end_date),
                            //new SqlParameter("@Search", user_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs669 simsobj = new Invs669();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_location_code = dr["sup_location_code"].ToString();
                            simsobj.sup_location_desc = dr["sup_location_desc"].ToString();
                            simsobj.sup_location_address = dr["sup_location_address"].ToString();
                            simsobj.sup_location_landmark = dr["sup_location_landmark"].ToString();
                            simsobj.sup_location_contact_no = dr["sup_location_contact_no"].ToString();
                            simsobj.sup_location_contact_person = dr["sup_location_contact_person"].ToString();
                            if (dr["sup_location_status"].ToString().Equals("A"))
                                simsobj.sup_location_status = true;
                            else
                                simsobj.sup_location_status = false;

                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        */

        #endregion

        #region API For Stock Adjustment Approval...

        [Route("getDocno")]
        public HttpResponseMessage getDocno(string dep_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            //Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs047> list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>() 
                        { 
                           new SqlParameter("@opr", "K"),
                           new SqlParameter("@dep_code", dep_no),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs047 obj = new invs047();
                            obj.adj_doc_no = dr["adj_doc_no"].ToString();
                            list.Add(obj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getStockDetailsforapprove")]
        public HttpResponseMessage getRequestDetailsforapprove(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                invs047 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<invs047>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_stock_adjustments_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'L'),
                                new SqlParameter("@adj_doc_no", sf.adj_doc_no),
                                new SqlParameter("@dep_code", sf.dept_code),
                                //new SqlParameter("@rd_from_required", sf.rd_from_required),
                                //new SqlParameter("@rd_up_required", sf.rd_up_required),
                                //new SqlParameter("@rd_item_desc", sf.rd_item_desc),
                                //new SqlParameter("@req_type", sf.request_mode_code),
                                //new SqlParameter("@req_range_no", sf.req_range_no),
                                //new SqlParameter("@sm_code",sf.sm_code),
                                //new SqlParameter("@user_code",sf.username),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateStockAdjustment")]
        public HttpResponseMessage UpdateStockAdjustment(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            string l = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            l = v["ad_line_no"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "M";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_stock_adjustments_proc]", sp);
                        cnt = cnt + r;
                        if (cnt > 0)
                        {
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_stock_adjustments_proc]",
                                   new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","N"),
                                new SqlParameter("@val",z),
                                //new SqlParameter("@line_no",l )
                               });
                                if (dr.Read())
                                {
                                    o = dr[0].ToString();
                                    s = HttpStatusCode.OK;
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("AddAdjustmentItem")]
        public HttpResponseMessage AddAdjustmentItem(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_stock_transfer_proc]",
                            new List<SqlParameter>() {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@ar_code", obj.ar_code),
                            new SqlParameter("@dt_code",obj.dco_code),
                            new SqlParameter("@dep_code", obj.dep_code),
                            //new SqlParameter("@adj_grv_no",obj.adj_grv_no),
                            //new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                            new SqlParameter("@loc_code",obj.loc_code),
                            new SqlParameter("@loc_code_to",obj.loc_code_to),
                            new SqlParameter("@adj_doc_no_reversal_of",null),
                            new SqlParameter("@adj_status",'1'),
                            //new SqlParameter("@remarks",obj.remark),
                            //new SqlParameter("@sup_sblgr_avno",obj.sup_sblgr_acno),
                        });

                        if (dr.Read())
                        {
                            st = dr["dp_value"].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        [Route("AddAdjustmentItemDetails")]
        public HttpResponseMessage AddAdjustmentItemDetails(List<invs047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                int i = 0;
                foreach (invs047 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        i = i + 1;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_item_stock_transfer_proc]",
                            new List<SqlParameter>() {
                                new SqlParameter("@opr", "B"),
                                new SqlParameter("@adj_doc_no",obj.ad_line_no),
                                new SqlParameter("@ad_line_no",i),
                                new SqlParameter("@im_inv_no",obj.adj_inv_no),
                                new SqlParameter("@loc_code",obj.loc_code),
                                new SqlParameter("@dep_code", obj.dep_code),
                                new SqlParameter("@ad_qty",obj.newQty),
                                new SqlParameter("@ad_value",obj.newValue),                          
                                new SqlParameter("@ad_recvd_qty",null),
                                new SqlParameter("@remarks",obj.remark),
                                new SqlParameter("@cal_qty",obj.oldQty),
                                new SqlParameter("@ad_add_remove_status",obj.ad_add_remove_status)
                                //new SqlParameter("@malc_rate",obj.oldVal),                                
                                //new SqlParameter("@adj_date", db.DBYYYYMMDDformat(obj.tdate)),
                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        #endregion



    }
}