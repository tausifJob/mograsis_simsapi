﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/InterDepartmentTransfer")]
    public class InterDepartmentTransferController:ApiController
    
  
{

        [Route("GetAllSalesTypeidt")]
        public HttpResponseMessage GetAllSalesTypeidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[sale_types]"),
                 new SqlParameter("@tbl_col_name1", "[sal_type],[sal_name]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sal_type]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sal_type = dr["sal_type"].ToString();
                            invsObj.sal_type_name = dr["sal_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        
        [Route("GetAllDepartmentsidt")]
        public HttpResponseMessage GetAllDepartmentsidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "O")
                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.dep_name = dr["dep_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllSupplierGroupNameforidt")]
        public HttpResponseMessage GetAllSupplierGroupNameforidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[supplier_groups]"),
                 new SqlParameter("@tbl_col_name1", "[sg_name],[sg_desc]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sg_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.name = dr["sg_desc"].ToString();
                            invsObj.code = dr["sg_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_supplier_codeforidt")]
        public HttpResponseMessage get_supplier_codeforidt()
        {
            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[suppliers]"),
                 new SqlParameter("@tbl_col_name1", "sup_code,sup_name"),
               //  new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "sup_name")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();

                            invsObj.name = dr["sup_name"].ToString();
                            invsObj.code = dr["sup_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_Categoriesidt")]
        public HttpResponseMessage get_Categoriesidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.item_master_proc",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "X"),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.category_name = dr["pc_desc"].ToString();
                            invsObj.category_code = dr["pc_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_SubCategoriesidt")]
        public HttpResponseMessage get_SubCategoriesidt(string pc_parentcode)
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.item_master_proc",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@pc_parent_code",pc_parentcode),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.subcategory_name = dr["pc_desc"].ToString();
                            invsObj.subcategory_code = dr["pc_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllItemsInSubCategoryforidt")]
        public HttpResponseMessage GetAllItemsInSubCategoryforidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.[item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "G"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "#" + dr["im_desc"].ToString();
                            invsObj.original_qty = dr["il_cur_qty"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Fetch_ItemDetails_ItemSetidt")]
        public HttpResponseMessage Fetch_ItemDetails_ItemSetidt(string im_inv_no)
        {


            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'B'),
                 new SqlParameter("@im_inv_no", im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimal component_qty = 0;

                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            {
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            }
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());

                            //   list.Add(itsrobj);


                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("invs_itemsearch_query",
                                    new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'c'),
                 new SqlParameter("@im_inv_no",  itsrobj.im_inv_no),
                 new SqlParameter("@im_inv_no_for",  im_inv_no),

                                           
                         });

                                if (dr1.Read())
                                {
                                    component_qty = decimal.Parse(dr1["Quantity"].ToString());

                                }
                                itsrobj.ia_component_qty = component_qty;

                            }


                            list.Add(itsrobj);



                        }
                    }
                    db.Dispose();

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

       

        [Route("DetailsItemsForCancelItemidt")]
        public HttpResponseMessage DetailsItemsForCancelItemidt(ApprovedIDT obj)
        {

            List<ApprovedIDT> mod_list = new List<ApprovedIDT>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sale_documents_proc",
                           new List<SqlParameter>() 
                         { 
                      new SqlParameter("@opr", "B"),
                      new SqlParameter("@im_item_code", obj.im_item_code),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ApprovedIDTList invsObj = new ApprovedIDTList();

                            ApprovedIDT invsObj1 = new ApprovedIDT();


                            invsObj1.AppIDTList = new List<ApprovedIDTList>();

                            string str = dr["doc_prov_no"].ToString();

                            var v = (from p in mod_list where p.doc_prov_no == str select p);

                            invsObj1.dep_code = dr["dep_code"].ToString();
                            invsObj1.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj1.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj1.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj1.doc_status = dr["doc_status"].ToString();
                            invsObj1.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj1.loc_name = dr["loc_name"].ToString();
                            invsObj1.em_first_name = dr["em_first_name"].ToString();
                            invsObj1.up_name = dr["up_name"].ToString();
                            invsObj1.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj1.im_item_code = dr["im_item_code"].ToString();
                            invsObj1.im_desc = dr["im_desc"].ToString();
                            invsObj1.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj1.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj1.sm_code = dr["sm_code"].ToString();
                            invsObj1.sm_name = dr["sm_name"].ToString();
                            invsObj1.doc_discount_amount = dr["doc_discount_amount"].ToString();
                            invsObj1.dd_sell_price = dr["dd_sell_price"].ToString();
                            invsObj1.dd_sell_price_final = dr["doc_total_final_amount"].ToString();
                            invsObj1.dd_remarks = dr["doc_delivery_remarks"].ToString();

                            invsObj.dd_sell_price_final = dr["dd_sell_price_final"].ToString();
                            invsObj.dd_sell_price = dr["dd_sell_price"].ToString();
                            invsObj.loc_name = dr["loc_name"].ToString();
                            invsObj.from_department = dr["from_department"].ToString();
                            invsObj.to_department = dr["to_department"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj.doc_status = dr["doc_status"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.dd_sell_value_final = dr["dd_sell_value_final"].ToString();
                            invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj.dd_line_discount_amount = dr["dd_line_discount_amount"].ToString();
                            invsObj.dd_sell_price_discounted = dr["dd_sell_price_discounted"].ToString();
                            invsObj.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.dd_sell_cost_total = dr["dd_sell_cost_total"].ToString();
                            invsObj.dd_line_discount_pct = dr["dd_line_discount_pct"].ToString();
                            invsObj.dd_line_total = dr["dd_line_total"].ToString();
                            invsObj.dd_physical_qty = dr["dd_physical_qty"].ToString();
                            invsObj.dd_outstanding_qty = dr["dd_outstanding_qty"].ToString();
                            if (v.Count() == 0)
                            {
                                invsObj1.AppIDTList.Add(invsObj);
                                mod_list.Add(invsObj1);

                            }

                            else
                            {
                                v.ElementAt(0).AppIDTList.Add(invsObj);
                            }

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CUDDetailsItemsForCancelReceiptidt")]
        public HttpResponseMessage CUDDetailsItemsForCancelReceiptidt(List<ApprovedIDT> invs)
        {
            string prov_no = "";
            bool result1 = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ApprovedIDT invsObj in invs)
                    {
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'Z'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@old_doc_prov_no", invsObj.doc_prov_no));
                        para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sale_documents_proc]", para);
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                    prov_no = dr["prov_no"].ToString();
                            }
                            dr.Close();
                        }

                        List<SqlParameter> para1 = new List<SqlParameter>();
                        para1.Add(new SqlParameter("@opr", 'Z'));
                        para1.Add(new SqlParameter("@sub_opr", ""));
                        para1.Add(new SqlParameter("@doc_prov_no", prov_no));
                        para1.Add(new SqlParameter("@old_doc_prov_no", invsObj.doc_prov_no));
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sale_document_details_proc", para1);
                        int r = dr1.RecordsAffected;
                        result1 = r > 0 ? true : false;
                        dr1.Close();

                        if (result1 == true)
                        {
                            foreach (ApprovedIDTList inv in invsObj.AppIDTList)
                            {
                                List<SqlParameter> para2 = new List<SqlParameter>();
                                para2.Add(new SqlParameter("@opr", "P"));
                                para2.Add(new SqlParameter("@sub_opr", ""));
                                para2.Add(new SqlParameter("@dep_code", inv.dep_code));
                                para2.Add(new SqlParameter("@loc_code", inv.loc_code));
                                para2.Add(new SqlParameter("@dd_qty", inv.dd_qty));
                                para2.Add(new SqlParameter("@im_inv_no", inv.im_inv_no));
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("sims.sale_document_details_proc", para2);
                                int r1 = dr2.RecordsAffected;
                                result1 = r1 > 0 ? true : false;
                                dr2.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //result1 = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, prov_no);
        }

        [Route("GetAllItemsInSubCategoryidt")]
        public HttpResponseMessage GetAllItemsInSubCategoryidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.[item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "G"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "#" + dr["im_desc"].ToString();
                            invsObj.original_qty = dr["il_cur_qty"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetItemCodeLocationsidt")]
        public HttpResponseMessage GetItemCodeLocationsidt(string im_inv_no)
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "invs.item_locations a inner join invs.locations b on a.loc_code=b.loc_code"),
                 new SqlParameter("@tbl_col_name1", "a.im_inv_no,a.loc_code,a.il_cur_qty,b.loc_name"),
                 new SqlParameter("@tbl_cond", "im_inv_no=" + im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.item_quantity = int.Parse(dr["il_cur_qty"].ToString());
                            invsObj.item_location_name = dr["loc_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        //Select
        [Route("postgetItemSerchidt")]
        public HttpResponseMessage postgetItemSerchidt(Invs058_itemNew itsr)
        {

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                 new SqlParameter("@opr", opr),
                 new SqlParameter("@im_inv_no", itsr.im_inv_no),
                 new SqlParameter("@sg_name", itsr.invs021_sg_name),
                 new SqlParameter("@im_item_code", itsr.im_item_code),
                 new SqlParameter("@im_desc", itsr.im_desc),
                 new SqlParameter("@dep_code", itsr.invs021_dep_code),
                 new SqlParameter("@sec_code", itsr.sec_code),
                 new SqlParameter("@sup_code", itsr.invs021_sup_code),
                 new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                 new SqlParameter("@category_code", itsr.category_code),
                 new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();


                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSalesmanNameforidt")]
        public List<invs035> GetSalesmanNameforidt(string user_code)
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.[item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@pc_parent_code",user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return list;

            }
            return list;
        }

        [Route("GetSalesmanNameForCancelReceiptforidt")]
        public List<invs035> GetSalesmanNameForCancelReceiptforidt()
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sale_documents_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return list;

            }
            return list;
        }

        [Route("GetInternal_accountcpdeidt")]
        public HttpResponseMessage GetInternal_accountcpdeidt(string usercd)
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.[item_master_proc]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "Q"),
                 new SqlParameter("@dep_code", usercd),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj.ic_account_name = dr["ic_account_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

      
        [Route("Insert_Sale_Documentsidt")]
        public HttpResponseMessage Insert_Sale_Documentsidt(invs035 invsObj)
        {
            string prov_no = string.Empty;


            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));


                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sale_documents_proc", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, prov_no);
            }

                return Request.CreateResponse(HttpStatusCode.OK, prov_no);


        }

        [Route("Insert_Sale_Documents_Detailsidt")]
        public HttpResponseMessage Insert_Sale_Documents_Detailsidt(List<invs035> Obj)
        {
            bool result = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (invs035 invsObj in Obj)
                    {
                        List<SqlParameter> para = new List<SqlParameter>();

                        para.Add(new SqlParameter("@opr", 'I'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@doc_prov_no_have_previous", invsObj.doc_prov_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sale_document_details_proc", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        dr.Close();
                    }

                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);


        }

        [Route("GetAllItemsInSubCategoryNewidt")]
        public HttpResponseMessage GetAllItemsInSubCategoryNewidt(string pc_code)
        {

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.item_master_proc",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "N"),
                new SqlParameter("@pc_code", pc_code),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "#" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            invsObj.original_qty = dr["il_cur_qty"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;
                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        public class Invs058_itemNew
        {

            public bool im_assembly_ind { get; set; }

            public string im_inv_no { get; set; }

            public string invs021_sg_name { get; set; }

            public string im_item_code { get; set; }

            public string im_desc { get; set; }

            public string invs021_dep_code { get; set; }

            public string sec_code { get; set; }

            public string invs021_sup_code { get; set; }

            public string im_assembly_ind_s { get; set; }

            public string category_code { get; set; }

            public string subcategory_code { get; set; }
        }


        [Route("GetItemsBySupplieridt")]
        public HttpResponseMessage GetItemsBySupplieridt(string sup_code)
        {

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[item_master]"),
                 new SqlParameter("@tbl_col_name1", "im_inv_no],[im_desc]"),
                 new SqlParameter("@tbl_cond", "sup_code='" + sup_code + "'"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.code = dr["im_inv_no"].ToString();
                            invsObj.name = dr["im_desc"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("DetailsItemsForCancelReceiptidt")]
        public HttpResponseMessage DetailsItemsForCancelReceiptidt(ApprovedIDT obj)
        {

            List<ApprovedIDT> mod_list = new List<ApprovedIDT>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sale_documents_proc",
                           new List<SqlParameter>() 
                         { 
                      new SqlParameter("@opr", "G"),
                      new SqlParameter("@doc_prov_no", obj.doc_prov_no),
                      new SqlParameter("@dt_code", obj.dt_code),
                      new SqlParameter("@ic_account_no",obj.ic_account_no),
                      new SqlParameter("@sm_code", obj.sm_code),
                      new SqlParameter("@doc_date", db.DBYYYYMMDDformat(obj.doc_prov_date)),
                      new SqlParameter("@doc_date1", db.DBYYYYMMDDformat(obj.doc_date)),
                      new SqlParameter("@up_name", obj.up_name),
                      new SqlParameter("@sal_type", obj.sal_type),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ApprovedIDTList invsObj = new ApprovedIDTList();

                            ApprovedIDT invsObj1 = new ApprovedIDT();


                            invsObj1.AppIDTList = new List<ApprovedIDTList>();

                            string str = dr["doc_prov_no"].ToString();

                            var v = (from p in mod_list where p.doc_prov_no == str select p);

                            invsObj1.dep_code = dr["dep_code"].ToString();
                            invsObj1.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj1.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj1.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj1.doc_status = dr["doc_status"].ToString();
                            invsObj1.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj1.loc_name = dr["loc_name"].ToString();
                            invsObj1.em_first_name = dr["em_first_name"].ToString();
                            invsObj1.up_name = dr["up_name"].ToString();
                            invsObj1.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj1.im_item_code = dr["im_item_code"].ToString();
                            invsObj1.im_desc = dr["im_desc"].ToString();
                            invsObj1.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj1.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj1.sm_code = dr["sm_code"].ToString();
                            invsObj1.sm_name = dr["sm_name"].ToString();
                            invsObj1.doc_discount_amount = dr["doc_discount_amount"].ToString();
                            invsObj1.dd_sell_price = dr["dd_sell_price"].ToString();
                            invsObj1.dd_sell_price_final = dr["doc_total_final_amount"].ToString();
                            invsObj1.dd_remarks = dr["doc_delivery_remarks"].ToString();
                            invsObj1.doc_narration = dr["doc_narration"].ToString();
                            invsObj.dd_sell_price_final = dr["dd_sell_price_final"].ToString();
                            invsObj.dd_sell_price = dr["dd_sell_price"].ToString();
                            invsObj.loc_name = dr["loc_name"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj.doc_status = dr["doc_status"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.dd_sell_value_final = dr["dd_sell_value_final"].ToString();
                            invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj.dd_line_discount_amount = dr["dd_line_discount_amount"].ToString();
                            invsObj.dd_sell_price_discounted = dr["dd_sell_price_discounted"].ToString();
                            invsObj.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.dd_sell_cost_total = dr["dd_sell_cost_total"].ToString();
                            invsObj.dd_line_discount_pct = dr["dd_line_discount_pct"].ToString();
                            invsObj.dd_line_total = dr["dd_line_total"].ToString();
                            invsObj.dd_physical_qty = dr["dd_physical_qty"].ToString();
                            invsObj.dd_outstanding_qty = dr["dd_outstanding_qty"].ToString();
                            if (v.Count() == 0)
                            {
                                invsObj1.AppIDTList.Add(invsObj);
                                mod_list.Add(invsObj1);

                            }

                            else
                            {
                                v.ElementAt(0).AppIDTList.Add(invsObj);
                            }

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}