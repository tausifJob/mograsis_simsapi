﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/documentlist")]
    public class SaleDocumentListController:ApiController
    {

        [Route("getAllRecords")]
        public HttpResponseMessage getAllRecords(string from_date, string to_date, string doc_prov_no, string dep_code, string sm_code, string doc_status,string name)
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                if (dep_code == "undefined" || dep_code == "" && sm_code == "undefined" || sm_code == "" )
                {
                    dep_code = null;
                    sm_code = null;
                    //doc_status = null;
                }

                if (doc_status == "undefined")
                {
                    doc_status = null;
                }
                if (doc_status == "")
                {
                    doc_status = null;
                }
                if(doc_prov_no=="undefined")
                {
                    doc_prov_no = null;
                }
                if (name == "undefined")
                {
                    name = null;
                }


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@sm_code",sm_code),
                            new SqlParameter("@doc_status",doc_status),
                            new SqlParameter("@doc_prov_no",doc_prov_no),
                            new SqlParameter("@name",name)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.dep_code = dr["dep_code"].ToString();
                           // if (!string.IsNullOrEmpty(dr["doc_prov_date"].ToString()))
                            //    simsobj.doc_date = DateTime.Parse(dr["doc_prov_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            simsobj.dep_name = dr["dep_name"].ToString();
                           // simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_prov_no = dr["doc_prov_no"].ToString();
                            simsobj.dt_code = dr["dt_code"].ToString();
                            simsobj.dt_desc = dr["dt_desc"].ToString();
                            simsobj.dr_code = dr["dr_code"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.sal_type = dr["sal_type"].ToString();
                            simsobj.sal_name = dr["sal_name"].ToString();
                            simsobj.doc_total_amount = dr["doc_total_amount"].ToString();
                            simsobj.doc_delivery_remarks = dr["doc_delivery_remarks"].ToString();
                            simsobj.doc_validity_remarks = dr["doc_validity_remarks"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getAllRecords_dps")]
        public HttpResponseMessage getAllRecords_dps(string from_date, string to_date, string doc_prov_no, string dep_code, string sm_code, string doc_status, string name,string enroll)
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                if (dep_code == "undefined" || dep_code == "" && sm_code == "undefined" || sm_code == "")
                {
                    dep_code = null;
                    sm_code = null;
                    //doc_status = null;
                }

                if (doc_status == "undefined")
                {
                    doc_status = null;
                }
                if (doc_status == "")
                {
                    doc_status = null;
                }
                if (doc_prov_no == "undefined")
                {
                    doc_prov_no = null;
                }
                if (name == "undefined")
                {
                    name = null;
                }
                if (enroll == "undefined")
                {
                    enroll = null;
                }


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@sm_code",sm_code),
                            new SqlParameter("@doc_status",doc_status),
                            new SqlParameter("@doc_prov_no",doc_prov_no),
                            new SqlParameter("@name",name),
                            new SqlParameter("@enroll",enroll)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            // if (!string.IsNullOrEmpty(dr["doc_prov_date"].ToString()))
                            //    simsobj.doc_date = DateTime.Parse(dr["doc_prov_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            simsobj.dep_name = dr["dep_name"].ToString();
                            // simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_prov_no = dr["doc_prov_no"].ToString();
                            simsobj.dt_code = dr["dt_code"].ToString();
                            simsobj.dt_desc = dr["dt_desc"].ToString();
                            simsobj.dr_code = dr["dr_code"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.sal_type = dr["sal_type"].ToString();
                            simsobj.sal_name = dr["sal_name"].ToString();
                            simsobj.doc_total_amount = dr["doc_total_amount"].ToString();
                            simsobj.doc_delivery_remarks = dr["doc_delivery_remarks"].ToString();
                            simsobj.doc_validity_remarks = dr["doc_validity_remarks"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            try
                            {
                               simsobj.cus_account_no = dr["cus_account_no"].ToString();
                            }
                            catch(Exception e) { }

                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getAllRecords")]
        public HttpResponseMessage getAllRecords(string from_date, string to_date, string search, string dep_code, string sm_code, string doc_status)
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                if (dep_code == "undefined" || dep_code == "" && sm_code == "undefined" || sm_code == "")
                {
                    dep_code = null;
                    sm_code = null;
                    //doc_status = null;
                }

                if (doc_status == "undefined")
                {
                    doc_status = null;
                }
                if (doc_status == "")
                {
                    doc_status = null;
                }


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@sm_code",sm_code),
                            new SqlParameter("@doc_status",doc_status)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            // if (!string.IsNullOrEmpty(dr["doc_prov_date"].ToString()))
                            //    simsobj.doc_date = DateTime.Parse(dr["doc_prov_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            simsobj.dep_name = dr["dep_name"].ToString();
                            // simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_prov_no = dr["doc_prov_no"].ToString();
                            simsobj.dt_code = dr["dt_code"].ToString();
                            simsobj.dt_desc = dr["dt_desc"].ToString();
                            simsobj.dr_code = dr["dr_code"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.sal_type = dr["sal_type"].ToString();
                            simsobj.sal_name = dr["sal_name"].ToString();
                            simsobj.doc_total_amount = dr["doc_total_amount"].ToString();
                            simsobj.doc_delivery_remarks = dr["doc_delivery_remarks"].ToString();
                            simsobj.doc_validity_remarks = dr["doc_validity_remarks"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }




        [Route("getDepartment")]
        public HttpResponseMessage getDepartment()
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","W")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getSmCode")]
        public HttpResponseMessage getSmCode(string dept_code)
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","V"),
                            new SqlParameter("@dep_code",dept_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.sm_user_code = dr["sm_user_code"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getDocStatus")]
        public HttpResponseMessage getDocStatus()
        {
            List<Inv160> com_list = new List<Inv160>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sales_document_list_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","A")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv160 simsobj = new Inv160();
                            simsobj.invs_appl_parameter = dr["invs_appl_parameter"].ToString();
                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }
    }
}