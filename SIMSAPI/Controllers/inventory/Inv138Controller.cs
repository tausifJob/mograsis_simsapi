﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/LetterofCredits")]
    [BasicAuthentication]
    public class Inv138Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSupplierName")]
        public HttpResponseMessage getSupplierName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSupplierName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "INVENTORY", "getSupplierName"));

            List<Invs138> supplier_list = new List<Invs138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Letter_of_creadits",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", 'C'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs138 invsobj = new Invs138();
                            invsobj.sup_name = dr["sup_name"].ToString();
                            invsobj.sup_code = dr["sup_code"].ToString();
                            supplier_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, supplier_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, supplier_list);
        }

        [Route("getBankName")]
        public HttpResponseMessage getBankName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBankName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "INVENTORY", "getBankName"));

            List<Invs138> bank_list = new List<Invs138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Letter_of_creadits",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs138 invsobj = new Invs138();
                            invsobj.bk_name = dr["bk_name"].ToString();
                            invsobj.bk_code = dr["bk_code"].ToString();
                            bank_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, bank_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, bank_list);
        }

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "INVENTORY", "getCurrency"));

            List<Invs138> currency_list = new List<Invs138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Letter_of_creadits",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", 'F'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs138 invsobj = new Invs138();
                            invsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();
                            invsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            currency_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, currency_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, currency_list);
        }

        [Route("getLCNO")]
        public HttpResponseMessage getLCNO()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLCNO(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "INVENTORY", "getLCNO"));

            List<Invs138> lcno_list = new List<Invs138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Letter_of_creadits",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs138 invsobj = new Invs138();
                            invsobj.lc_no1 = dr["Lc_no1"].ToString();
                            lcno_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lcno_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lcno_list);
        }

        [Route("getLetterOfCreditByIndex")]
        public HttpResponseMessage getLetterOfCreditByIndex()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLetterOfCredit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "INVENTORY", "getLetterOfCredit"));
            int total = 0, skip = 0;
            List<Invs138> lc_list = new List<Invs138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Letter_of_creadits",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs138 invsobj = new Invs138();
                            invsobj.lc_no1 = dr["lc_no"].ToString();
                            invsobj.lc_date = db.UIDDMMYYYYformat(dr["lc_date"].ToString());
                            invsobj.bk_code = dr["bk_code"].ToString();
                            invsobj.cur_code = dr["cur_code"].ToString();
                            invsobj.lc_exchange_rate =  dr["lc_exchange_rate"].ToString();
                            invsobj.sup_code = dr["sup_code"].ToString();
                            invsobj.lc_bank_lc_no = dr["lc_bank_lc_no"].ToString();
                            invsobj.lc_open_date = db.UIDDMMYYYYformat(dr["lc_open_date"].ToString());
                            invsobj.lc_beneficiary = dr["lc_beneficiary"].ToString();
                            invsobj.lc_amount = dr["lc_amount"].ToString();
                            invsobj.lc_revised_amount = dr["lc_revised_amount"].ToString();
                            invsobj.lc_utilized_amount = dr["lc_utilized_amount"].ToString();
                            invsobj.lc_cancelled_amount = dr["lc_cancelled_amount"].ToString();
                            invsobj.lc_bank_charges = dr["lc_bank_charges"].ToString();
                            invsobj.lc_valid_date = db.UIDDMMYYYYformat(dr["lc_valid_date"].ToString());
                            invsobj.lc_terms = dr["lc_terms"].ToString();
                            invsobj.lc_remarks = dr["lc_remarks"].ToString();
                            invsobj.lc_close_date = db.UIDDMMYYYYformat(dr["lc_close_date"].ToString());
                            invsobj.bk_name = dr["bk_name"].ToString();
                            invsobj.sup_name = dr["sup_name"].ToString();
                            invsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();
                            lc_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lc_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lc_list);
        }

        [Route("CUDLetterOfCreditsDetails")]
        public HttpResponseMessage CUDLetterOfCreditsDetails(Invs138 invsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDLetterOfCreditsDetails(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "INVENTORY", "CUDLetterOfCreditsDetails", invsobj));

            //Invs138 invsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs138>(data);
            Message message = new Message();
            try
            {
                if (invsobj != null)
                {                    
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Letter_of_creadits]",
                           new List<SqlParameter>() 
                         {                            
                            new SqlParameter("@opr",invsobj.opr),
                            new SqlParameter("@lc_date", db.DBYYYYMMDDformat( invsobj.lc_date)),
                            new SqlParameter("@bk_code", invsobj.bk_code),
                            new SqlParameter("@cur_code", invsobj.excg_curcy_code),
                            new SqlParameter("@lc_exchange_rate", invsobj.lc_exchange_rate),
                            new SqlParameter("@sup_code", invsobj.sup_code),
                            new SqlParameter("@lc_bank_lc_no", invsobj.lc_bank_lc_no),
                            new SqlParameter("@lc_open_date",db.DBYYYYMMDDformat( invsobj.lc_open_date)),
                            new SqlParameter("@lc_beneficiary", invsobj.lc_beneficiary),
                            new SqlParameter("@lc_amount", invsobj.lc_amount),
                            new SqlParameter("@lc_revised_amount", invsobj.lc_revised_amount),
                            new SqlParameter("@lc_utilized_amount", invsobj.lc_utilized_amount),
                            new SqlParameter("@lc_cancelled_amount", invsobj.lc_cancelled_amount),
                            new SqlParameter("@lc_bank_charges",invsobj.lc_bank_charges),
                            new SqlParameter("@lc_valid_date",db.DBYYYYMMDDformat( invsobj.lc_valid_date)),
                            new SqlParameter("@lc_terms", invsobj.lc_terms),
                            new SqlParameter("@lc_remarks", invsobj.lc_remarks),
                            new SqlParameter("@lc_close_date",db.DBYYYYMMDDformat( invsobj.lc_close_date)),
                            new SqlParameter("@lc_no", invsobj.lc_no1),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (invsobj.opr.Equals("U"))
                                message.strMessage = "Letter of Credits Details Updated Sucessfully!!";
                            else if (invsobj.opr.Equals("I"))
                                message.strMessage = "Letter of Credits Details Added Sucessfully!!";
                            else if (invsobj.opr.Equals("D"))
                                message.strMessage = "Letter of Credits Details Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (invsobj.opr.Equals("U"))
                    message.strMessage = "Error In Updating Letter of Credits Details!!";
                else if (invsobj.opr.Equals("I"))
                    message.strMessage = "Error In Adding Letter of Credits Details!!";
                else if (invsobj.opr.Equals("D"))
                    message.strMessage = "Error In Deleting Letter of Credits Details!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


    }
}