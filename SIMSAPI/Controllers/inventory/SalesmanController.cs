﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.inventory
{
     [RoutePrefix("api/Salesman")]

    public class SalesmanController : ApiController
    {
        [Route("getAutoGeneratesmCode")]
        public HttpResponseMessage getAutoGeneratesmCode()
        {
            string commcode = string.Empty;
            Inv059 simsobj = new Inv059();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_salesman_proc]",
                        new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'E'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sm_code = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj.sm_code);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj.sm_code);
            }
        }

        [Route("Get_Salesman_AllDetails")]
        public HttpResponseMessage Get_Salesman_AllDetails()
        {
            List<Inv059> list = new List<Inv059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_salesman_proc]",
                        new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "S"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv059 invsobj = new Inv059();
                            invsobj.com_code = dr["com_code"].ToString();
                            invsobj.com_name = dr["com_name"].ToString();
                            invsobj.dep_code = dr["depcode"].ToString();
                            invsobj.dep_name = dr["dep_name"].ToString();
                            invsobj.sm_code = dr["sm_code"].ToString();
                            invsobj.sm_password = dr["sm_password"].ToString();
                            invsobj.sm_name = dr["sm_name"].ToString();
                            // invsobj.sm_name = dr["em_first_name"].ToString() + " " + dr["em_middle_name"].ToString() + " " + dr["em_last_name"].ToString();
                            invsobj.sm_min_comm = dr["sm_min_comm"].ToString();
                            invsobj.sm_max_comm = dr["sm_max_comm"].ToString();
                            invsobj.sm_max_discount = dr["sm_max_discount"].ToString();
                            invsobj.sm_target_level_1 = dr["sm_target_level_1"].ToString();
                            invsobj.sm_pct_1_comm = dr["sm_pct_1_comm"].ToString();
                            invsobj.sm_target_level_2 = dr["sm_target_level_2"].ToString();
                            invsobj.sm_pct_2_comm = dr["sm_pct_2_comm"].ToString();
                            invsobj.sm_target_level_3 = dr["sm_target_level_3"].ToString();
                            invsobj.sm_max_disc_pass = dr["sm_max_discount_password"].ToString();
                            if (dr["sm_status"].ToString() == "A")
                            {
                                invsobj.sm_status = true;
                            }
                            else
                            {
                                invsobj.sm_status = false;
                            }

                            list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CUDSalesmanDetails")]
        public HttpResponseMessage CUDSalesmanDetails(List<Inv059> data)
        {
            //Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Inv059 invsobj in data)
                        {

                            if (invsobj.sm_name == "" || invsobj.sm_name == "undefined")
                                invsobj.sm_name = null;
                            if (invsobj.dep_code == "" || invsobj.dep_code == "undefined")
                                invsobj.dep_code = null;
                            if (invsobj.sm_password == "" || invsobj.sm_password == "undefined")
                                invsobj.sm_password = null;
                            if (invsobj.sm_min_comm == "" || invsobj.sm_min_comm == "undefined")
                                invsobj.sm_min_comm = null;
                            if (invsobj.sm_max_comm == "" || invsobj.sm_max_comm == "undefined")
                                invsobj.sm_max_comm = null;
                            if (invsobj.sm_max_discount == "" || invsobj.sm_max_discount == "undefined")
                                invsobj.sm_max_discount = null;
                            if (invsobj.sm_target_level_1 == "" || invsobj.sm_target_level_1 == "undefined")
                                invsobj.sm_target_level_1 = null;
                            if (invsobj.sm_pct_1_comm == "" || invsobj.sm_pct_1_comm == "undefined")
                                invsobj.sm_pct_1_comm = null;
                            if (invsobj.sm_target_level_2 == "" || invsobj.sm_target_level_2 == "undefined")
                                invsobj.sm_target_level_2 = null;
                            if (invsobj.sm_pct_2_comm == "" || invsobj.sm_pct_2_comm == "undefined")
                                invsobj.sm_pct_2_comm = null;
                            if (invsobj.sm_target_level_3 == "" || invsobj.sm_target_level_3 == "undefined")
                                invsobj.sm_target_level_3 = null;
                            if (invsobj.sm_max_disc_pass == "" || invsobj.sm_max_disc_pass == "undefined")
                                invsobj.sm_max_disc_pass = null;
                            if (invsobj.username == "" || invsobj.username == "undefined")
                                invsobj.username = null;

                            int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_salesman_proc]",
                                new List<SqlParameter>()
                            {
                                    new SqlParameter("@opr", invsobj.opr),
                                    new SqlParameter("@sm_code", invsobj.sm_code),
                                    new SqlParameter("@sm_name",invsobj.sm_name),
                                    new SqlParameter("@dep_code", invsobj.dep_code),
                                    new SqlParameter("@sm_password", invsobj.sm_password),
                                    new SqlParameter("@sm_min_comm",invsobj.sm_min_comm),
                                    new SqlParameter("@sm_max_comm",invsobj.sm_max_comm),
                                    new SqlParameter("@sm_max_discount",invsobj.sm_max_discount),
                                    new SqlParameter("@sm_target_level_1",invsobj.sm_target_level_1),
                                    new SqlParameter("@sm_pct_1_comm",invsobj.sm_pct_1_comm),
                                    new SqlParameter("@sm_target_level_2",invsobj.sm_target_level_2),
                                    new SqlParameter("@sm_pct_2_comm",invsobj.sm_pct_2_comm),
                                    new SqlParameter("@sm_target_level_3",invsobj.sm_target_level_3),
                                    new SqlParameter("@sm_max_discount_password", invsobj.sm_max_disc_pass),
                                    new SqlParameter("@sm_status", invsobj.sm_status?"A":"I"),
                                    new SqlParameter("@sm_user_code", invsobj.username),
                           });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Get_Compny_name")]
        public HttpResponseMessage Get_Compny_name()
        {
            List<Inv059> list = new List<Inv059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_salesman_proc]",
                        new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "A"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv059 obj = new Inv059();
                            obj.com_name = dr["com_name"].ToString();
                            obj.com_code = dr["com_code"].ToString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_Dept_name")]
        public HttpResponseMessage Get_Dept_name(string comp_code)
        {
            List<Inv059> list = new List<Inv059>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_salesman_proc]",
                        new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@com_code",comp_code),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv059 iobj = new Inv059();
                            iobj.dep_name = dr["dep_name"].ToString();
                            iobj.dep_code = dr["dep_code"].ToString();
                            list.Add(iobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
    }
}