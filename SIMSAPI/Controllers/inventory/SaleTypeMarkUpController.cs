﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/SaleMark")]
    public class SaleTypeMarkUpController:ApiController
    {
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Inv126> doc_list = new List<Inv126>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv126 simsobj = new Inv126();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSaleTypeDetail")]
        public HttpResponseMessage getSaleTypeDetail()
        {
            List<Inv128> SaleMaType = new List<Inv128>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sales_type_markups]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv128 obj = new Inv128();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            obj.depcodewithname = dr["dep_code"].ToString() + '-' + dr["dep_name"].ToString();
                            obj.stm_indicator = dr["stm_indicator"].ToString();
                            obj.stm_rate = dr["stm_rate"].ToString();
                            obj.stm_sale_acno = dr["stm_sale_acno"].ToString();
                            obj.stm_cost_acno = dr["stm_cost_acno"].ToString();
                            obj.sal_type = dr["sal_type"].ToString();
                            obj.sal_name = dr["sal_name"].ToString();
                            obj.saletypewthname = dr["sal_type"].ToString() + '-' + dr["sal_name"].ToString();
                            SaleMaType.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, SaleMaType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SaleMaType);
        }

        [Route("getDepartment")]
        public HttpResponseMessage getDepartment()
        {
            List<Inv128> allinvdepart = new List<Inv128>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sales_type_markups]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv128 obj = new Inv128();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            obj.full_depart_name = dr["full_depart_name"].ToString();
                            allinvdepart.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, allinvdepart);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, allinvdepart);
        }

        [Route("getSaleType")]
        public HttpResponseMessage getSaleType()
        {
            List<Inv128> saletype = new List<Inv128>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sales_type_markups]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "V")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv128 obj = new Inv128();
                            obj.sal_type = dr["sal_type"].ToString();
                            obj.sal_name = dr["sal_name"].ToString();
                            obj.sal_indicator = dr["sal_indicator"].ToString();
                            obj.sale_acno = dr["sale_acno"].ToString();
                            obj.cost_acno = dr["cost_acno"].ToString();
                            obj.full_sal_name = dr["full_sal_name"].ToString();
                            saletype.Add(obj); 
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, saletype);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, saletype);
        }

        [Route("SalesMarkCUD")]
        public HttpResponseMessage SalesMarkCUD(List<Inv128> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv128 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_sales_type_markups]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@dep_name", simsobj.dep_code),
                                 new SqlParameter("@sal_type", simsobj.sal_type),
                                 new SqlParameter("@old_sal_type", simsobj.old_sal_type),
                                 new SqlParameter("@stm_indicator", simsobj.stm_indicator),
                                 new SqlParameter("@stm_rate", simsobj.stm_rate),
                                 new SqlParameter("@sal_account_name", simsobj.stm_sale_acno),
                                 new SqlParameter("@cost_account_name", simsobj.stm_cost_acno),
                                  new SqlParameter("@updateflag", simsobj.updateflag==true?"Y":"N")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}