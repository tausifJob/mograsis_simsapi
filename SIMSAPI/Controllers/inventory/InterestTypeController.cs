﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/InterestType")]
    public class InterestTypeController : ApiController
    {

        [Route("getInterestTypeDetails")]
        public HttpResponseMessage getInterestTypeDetails()
        {
            List<Inv145> Interest_list = new List<Inv145>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[interest_types_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv145 obj = new Inv145();

                            obj.int_code = dr["int_code"].ToString();
                            obj.int_desc = dr["int_desc"].ToString();

                            Interest_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Interest_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Interest_list);
        }

        [Route("InterestTypeCUD")]
        public HttpResponseMessage InterestTypeCUD(List<Inv145> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv145 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[interest_types_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@int_code",simsobj.int_code),
                          new SqlParameter("@int_desc",simsobj.int_desc)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}