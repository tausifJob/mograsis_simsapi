﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/DeliveryReasons")]
    [BasicAuthentication]
    public class DeliveryReasonsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getDeliveryReasons")]
        public HttpResponseMessage getDeliveryReasons()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeliveryReasons(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getDeliveryReasons"));

            List<Invs005> goaltarget_list = new List<Invs005>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_reasons_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs005 simsobj = new Invs005();
                            simsobj.dr_code = dr["dr_code"].ToString();

                            simsobj.dep_code = dr["dep_code"].ToString();

                            simsobj.dep_name = dr["dep_name"].ToString();

                            simsobj.dr_account_no = dr["dr_account_no"].ToString();

                            simsobj.dr_desc = dr["dr_desc"].ToString();
                          

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        //GetGLAccountNumber
        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs005> doc_list = new List<Invs005>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_reasons_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs005 simsobj = new Invs005();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetGLAccountNumber
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Fin138> doc_list = new List<Fin138>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin138 simsobj = new Fin138();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDDeliveryReasons")]
        public HttpResponseMessage CUDLibraryCatalogue(List<Invs005> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs005 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_delivery_reasons_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@dr_code", simsobj.dr_code),
                                new SqlParameter("@dep_code",simsobj.dep_code),
                                new SqlParameter("@dr_desc", simsobj.dr_desc),
                                new SqlParameter("@sal_account_name", simsobj.dr_account_no),

                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        //Fee POsting

        [Route("getProductAccountDetail")]
        public HttpResponseMessage getProductAccountDetail()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProductAccountDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getDeliveryReasons"));

            List<Invs005> goaltarget_list = new List<Invs005>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.product_code_account_detail_proc",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs005 simsobj = new Invs005();
                            simsobj.pc_code = dr["pc_code"].ToString();

                            simsobj.pc_fin_year = dr["pc_fin_year"].ToString();

                            simsobj.pc_revenue_acno = dr["pc_revenue_acno"].ToString();

                            simsobj.pc_stock_acno = dr["pc_stock_acno"].ToString();

                            simsobj.pc_cost_acno = dr["pc_cost_acno"].ToString();

                            simsobj.pc_discont_acno = dr["pc_discont_acno"].ToString();

                            simsobj.pc_desc = dr["pc_desc"].ToString();

                            simsobj.pc_status = dr["pc_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("GetAllGLAccountNos")]
        public HttpResponseMessage GetAllGLAccountNos(string glma_accountcode, string cmpnycode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllGLAccountNos()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Invs005> doc_list = new List<Invs005>();
            if (glma_accountcode == "undefined")
                glma_accountcode = null;


            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.product_code_account_detail_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@glma_comp_code",'1'),
                            new SqlParameter("@glma_acct_code",glma_accountcode)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs005 FinnObj = new Invs005();
                            FinnObj.revenue_accno = dr["gl_accno"].ToString();
                            FinnObj.revenue_acct_name = dr["glma_acct_name"].ToString();

                            FinnObj.stock_accno = dr["gl_accno"].ToString();
                            FinnObj.stock_acct_name = dr["glma_acct_name"].ToString();


                            FinnObj.cost_accno = dr["gl_accno"].ToString();
                            FinnObj.cost_acct_name = dr["glma_acct_name"].ToString();


                            FinnObj.discount_accno = dr["gl_accno"].ToString();
                            FinnObj.discount_acct_name = dr["glma_acct_name"].ToString();







                            doc_list.Add(FinnObj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("updateFeePosting")]
        public HttpResponseMessage updateFeePosting(List<Invs005> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs005 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("invs.product_code_account_detail_proc",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                               
                                new SqlParameter("@pc_code", simsobj.pc_code),
                                new SqlParameter("@pc_fin_year", simsobj.pc_fin_year),
                                new SqlParameter("@pc_revenue_acno", simsobj.pc_revenue_acno),
                                new SqlParameter("@pc_stock_acno", simsobj.pc_stock_acno),
                                new SqlParameter("@pc_cost_acno", simsobj.pc_cost_acno),
                                new SqlParameter("@pc_discont_acno", simsobj.pc_discont_acno),
                               // new SqlParameter("@pc_status", simsobj.pc_status),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }




    }


}










