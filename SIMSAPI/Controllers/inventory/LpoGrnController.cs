﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using System.Data;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/LPOGRM")]
    public class LpoGrnController: ApiController
    {
        #region Invs047(Adjustments)//By Shweta

        [Route("Get_Adj_Reasons")]
        public HttpResponseMessage Get_Adj_Reasons()
        {
            List<invs047> mod_list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_adjustment_reasons_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R")
                });
                    while (dr.Read())
                    {
                        invs047 obj = new invs047();
                        obj.ar_code = dr["ar_code"].ToString();
                        obj.ar_desc = dr["ar_desc"].ToString();
                        mod_list.Add(obj);
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("Get_Adj_Dttypes")]
        public HttpResponseMessage Get_Adj_Dttypes()
        {
            List<invs047> mod_list2 = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_department_constant_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R")
                });
                    while (dr.Read())
                    {
                        invs047 obj2 = new invs047();
                        obj2.dco_code = dr["dt_code"].ToString();
                        obj2.dco_desc = dr["dt_desc"].ToString();
                        mod_list2.Add(obj2);
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list2);


        }

        [Route("Get_Adj_locations")]
        public HttpResponseMessage Get_Adj_locations()
        {
            List<invs047> mod_list_loc = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_locations_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S")


                });
                    while (dr.Read())
                    {
                        invs047 obj1 = new invs047();
                        obj1.loc_code = dr["loc_code"].ToString();
                        obj1.loc_name = dr["loc_name"].ToString();
                        mod_list_loc.Add(obj1);
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list_loc);

        }

        [Route("Get_Adj_Suppliername")]
        public HttpResponseMessage Get_Adj_Suppliername()
        {
            List<invs047> mod_list = new List<invs047>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R")
                });
                    while (dr.Read())
                    {
                        invs047 obj = new invs047();
                        obj.sup_name = dr["sup_name"].ToString();
                        obj.sup_sblgr_acno = dr["sup_sblgr_acno"].ToString();
                        obj.sup_code = dr["sup_code"].ToString();
                        mod_list.Add(obj);
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Get_itemdetails")]
        public HttpResponseMessage Get_itemdetails(string itemcode)
        {
            //List<invsItemdetails> mod_list_item = new List<invsItemdetails>();
            Invs047_it obj1 = new Invs047_it();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_departments_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R"),
                new SqlParameter("@im_item_code", itemcode)

                });
                    if (dr.Read())
                    {
                        obj1.im_inv_no = dr["im_inv_no"].ToString();
                        obj1.im_desc = dr["im_desc"].ToString();
                        obj1.im_malc_rate = dr["im_malc_rate"].ToString();
                        obj1.id_cur_qty = dr["id_cur_qty"].ToString();
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj1);

        }

        [Route("Get_itemquantity")]
        public HttpResponseMessage Get_itemquantity(string im_inv_no)
        {
            invsItemdetails obj1 = new invsItemdetails();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_departments_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "C"),
                new SqlParameter("@im_inv_no", im_inv_no ),
                    new SqlParameter("@dep_code" , "10")

                });
                    if (dr.Read())
                    {
                        obj1.id_cur_qty = dr["id_cur_qty"].ToString();
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj1);

        }

        [Route("Invs047Get_item_master_detail")]
        public HttpResponseMessage Invs047Get_item_master_detail(Invs047_it data)
        {

            List<Invs047_it> group_list = new List<Invs047_it>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", 'S'),
                new SqlParameter("@im_inv_no", data.im_inv_no),
                new SqlParameter("@sg_name", data.old_sg_name),
                new SqlParameter("@im_desc", data.im_desc),
                new SqlParameter("@im_item_code", data.im_item_code),
                new SqlParameter("@dept_code", data.old_dep_code),
                new SqlParameter("@sec_code", data.sec_code),
                new SqlParameter("@sup_code", data.invs021_sup_code),
                new SqlParameter("@subcategory_code", data.subcategory_code),
                new SqlParameter("@category_code", data.im_trade_cat),
                });
                    while (dr.Read())
                    {
                        Invs047_it invsobj = new Invs047_it();
                        invsobj.im_inv_no = dr["im_inv_no"].ToString();
                        invsobj.old_sg_name = dr["sg_name"].ToString();
                        invsobj.invs021_sg_name = dr["sg_desc"].ToString();
                        invsobj.im_item_code = dr["im_item_code"].ToString();
                        invsobj.im_trade_cat = dr["trade_cat_name"].ToString();
                        invsobj.im_desc = dr["im_desc"].ToString();
                        invsobj.old_dep_code = dr["dep_code"].ToString();
                        invsobj.invs021_dep_code = dr["dep_name"].ToString();
                        invsobj.old_sec_code = dr["sec_code"].ToString();
                        invsobj.sec_code = dr["sec_name"].ToString();
                        invsobj.old_pc_code = dr["pc_code"].ToString();
                        invsobj.pc_code = dr["pc_desc"].ToString();
                        invsobj.old_sup_code = dr["sup_code"].ToString();
                        invsobj.invs021_sup_code = dr["sup_name"].ToString();
                        invsobj.old_uom_code = dr["uom_code"].ToString();
                        invsobj.uom_code = dr["uom_name"].ToString();
                        invsobj.old_uom_code_has = dr["uom_code_has"].ToString();
                        invsobj.uom_code_has = dr["has"].ToString();
                        invsobj.im_model_name = dr["im_model_name"].ToString();
                        invsobj.im_one_time_flag = dr["im_one_time_flag"].Equals("Y") ? true : false;
                        invsobj.im_agency_flag = dr["im_agency_flag"].Equals("Y") ? true : false;
                        invsobj.im_assembly_ind = dr["im_assembly_ind"].Equals("Y") ? true : false;
                        invsobj.im_supersed_ind = dr["im_supersed_ind"].Equals("Y") ? true : false;
                        group_list.Add(invsobj);
                    }

                }

            }
            catch (Exception e)
            {
                //cmd.Dispose();
            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);

        }

        [Route("Insertadjustments")]
        public HttpResponseMessage Insertadjustments(invs047 ss)
        {
            bool inserted = false;
            string dpval = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_LPO_GRN_insertadjustments_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@dep_code", ss.dep_code),
                            new SqlParameter("@ar_code", ss.ar_code),
                            new SqlParameter("@dt_code", ss.dco_code),
                            new SqlParameter("@up_name", null),
                            new SqlParameter("@adj_grv_no", ss.adj_grv_no),
                            new SqlParameter("@adj_date", System.DateTime.Now),
                            new SqlParameter("@loc_code", ss.loc_code),
                            new SqlParameter("@loc_code_to", null),
                            new SqlParameter("@adj_doc_no_reversal_of", null),
                            new SqlParameter("@dep_code_reversal_of", null),
                            new SqlParameter("@adj_status", 1),
                            new SqlParameter("@remarks", ss.remark),
                            new SqlParameter("@sup_sblgr_avno", ss.sup_sblgr_acno)
                        });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                        while (dr.Read())
                        {
                            dpval = dr["dp_value"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, dpval);


        }

        [Route("Insertadjustmentsdetails")]
        public HttpResponseMessage Insertadjustmentsdetails(List<Invs047_it> data)
        {
            bool Updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs047_it ss in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_LPO_GRN_insertadjustment_details_proc]",
                            new List<SqlParameter>()
                         {

                          new SqlParameter("@dep_code", ss.dep_code),
                          new SqlParameter("@adj_doc_no", Convert.ToDecimal(ss.adj_doc_no.ToString())),
                          new SqlParameter("@ad_line_no", Convert.ToDecimal(ss.ad_line_no.ToString())),
                          new SqlParameter("@im_inv_no", Convert.ToDecimal(ss.im_inv_no.ToString())),
                          new SqlParameter("@loc_code", ss.loc_code.ToString()),
                          new SqlParameter("@ad_qty", ss.newQty.ToString()),
                          new SqlParameter("@ad_value", Convert.ToDecimal(ss.newValue.ToString())),
                          new SqlParameter("@ad_recvd_qty", ss.newQty.ToString()),//As location is fixed.
                          new SqlParameter("@ad_remark", ss.remark.ToString()),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            Updated = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {

                Updated = false;
            }

            return Request.CreateResponse(HttpStatusCode.OK, Updated);

        }

        #endregion

        #region item_search

        [Route("get_Invs021_get_supplier_group_name")]
        public HttpResponseMessage get_Invs021_get_supplier_group_name()
        {

            List<invsClass> list = new List<invsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_groups_proc]",
                            new List<SqlParameter>()
                         {

               new SqlParameter("@opr", "S"),

              });
                    while (dr.Read())
                    {
                        invsClass invsobj = new invsClass();
                        invsobj.name = dr["sg_desc"].ToString();
                        invsobj.code = dr["sg_name"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("get_Invs021_get_Categories")]
        public HttpResponseMessage get_Invs021_get_Categories()
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc]",
                        new List<SqlParameter>()
                     {

               new SqlParameter("@opr", "X")
          });
                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.category_name = dr["pc_desc"].ToString();
                        invsobj.category_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("get_Invs021_get_SubCategories")]
        public HttpResponseMessage get_Invs021_get_SubCategories(string pc_parentcode)
        {
            string appl_code = string.Empty;
            List<Invs021> list = new List<Invs021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_master_proc]",
                        new List<SqlParameter>()
                     {

               new SqlParameter("@opr", "Y"),
               new SqlParameter("@pc_parent_code", pc_parentcode)
          });
                    while (dr.Read())
                    {
                        Invs021 invsobj = new Invs021();
                        invsobj.subcategory_name = dr["pc_desc"].ToString();
                        invsobj.subcategory_code = dr["pc_code"].ToString();
                        list.Add(invsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("Fetch_ItemDetails")]
        public HttpResponseMessage Fetch_ItemDetails(Invs058_item itsr)
        {
            List<Invs058_item> mod_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                          {

              itsr.im_assembly_ind == true?new SqlParameter("@opr", 'A' ): new SqlParameter("@opr", 'S'),
               new SqlParameter("@im_inv_no", itsr.im_inv_no),
               new SqlParameter("@sg_name", itsr.invs021_sg_name),
               new SqlParameter("@im_item_code", itsr.im_item_code),
               new SqlParameter("@im_desc", itsr.im_desc),
               new SqlParameter("@dep_code", itsr.invs021_dep_code),
               new SqlParameter("@sec_code", itsr.sec_code),
               new SqlParameter("@sup_code", itsr.invs021_sup_code),
               new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind==true?"Y":"N"),
               new SqlParameter("@category_code", itsr.category_code),
               new SqlParameter("@subcategory_code", itsr.subcategory_code)
               });
                    while (dr.Read())
                    {
                        Invs058_item itsrobj = new Invs058_item();
                        itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                        itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                        itsrobj.im_item_code = dr["im_item_code"].ToString();
                        itsrobj.im_desc = dr["im_desc"].ToString();
                        itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                        itsrobj.sec_code = dr["sec_code"].ToString();
                        itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                        itsrobj.old_uom_code = dr["uom_code"].ToString();
                        itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                        itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                        itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                        itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                        itsrobj.new_calqty = dr["ava_aty"].ToString();
                        itsrobj.newQty = dr["do_qty"].ToString();
                        itsrobj.sg_desc = dr["sg_desc"].ToString();
                        itsrobj.sup_name = dr["sup_name"].ToString();
                        itsrobj.dep_name = dr["dep_name"].ToString();
                        itsrobj.uom_name = dr["uom_name"].ToString();
                        itsrobj.sec_name = dr["sec_name"].ToString();
                        itsrobj.loc_code = dr["loc_code"].ToString();
                        itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                        itsrobj.sg_name = dr["sg_name"].ToString();

                        itsrobj.category_name = dr["Category"].ToString();
                        itsrobj.subcategory_name = dr["SubCategory"].ToString();


                        if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                        itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                        itsrobj.ia_component_qty = 0;
                        //itsrobj.pc_desc = dr["pc_desc"].ToString();
                        itsrobj.quantity_assembly = "0";
                        itsrobj.category_code = dr["Category"].ToString();
                        itsrobj.subcategory_code = dr["SubCategory"].ToString();
                        mod_list.Add(itsrobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        #endregion


        /* LPO Process */

        [Route("getOrderApprovedDetails")]
        public HttpResponseMessage getOrderApprovedDetails(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                AdjRe sf = Newtonsoft.Json.JsonConvert.DeserializeObject<AdjRe>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_lpo_process_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@SEL_ADJ_DATE_FROM", db.DBYYYYMMDDformat(sf.dateFrom)),
                            new SqlParameter("@SEL_ADJ_DATE_UPTO", db.DBYYYYMMDDformat(sf.dateUpto)),
                            new SqlParameter("@SEL_DEP_CODE", sf.dep_code),
                            new SqlParameter("@SEL_ITEM_DESC", sf.ar_desc),
                            new SqlParameter("@SUP_CODE", sf.sup_code),
                            new SqlParameter("@SEL_DOC_NO", sf.doc_no)
                        });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("getreport_name")]
        public HttpResponseMessage getreport_name()
        {
            string sims_appl_form_field_value1 = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_lpo_process_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "R"),

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_appl_form_field_value1);

        }

        [Route("getcomCode_name")]
        public HttpResponseMessage getcomCode_name()
        {
            string sims_appl_form_field_value1 = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_lpo_process_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "CR"),

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_appl_form_field_value1 = dr["comp_code"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_appl_form_field_value1);

        }

        [Route("approveRejectLPOProcess")]
        public HttpResponseMessage approveRejectLPOProcess(List<Invs058_item> data)
        {
            bool inserted = false;
            Message message = new Message();
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs058_item simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("invs.invs_lpo_process_proc",
                           new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@DOC_NO", simsobj.doc_no),
                                new SqlParameter("@DOC_LINE_NO", simsobj.doc_line_no),
                                new SqlParameter("@ITEM_INV_NO", simsobj.item_inv_no),
                                new SqlParameter("@R_QTY", simsobj.r_qty),
                                new SqlParameter("@GRV_NO", simsobj.grv_no),
                                new SqlParameter("@T_QTY", simsobj.t_qty),
                                new SqlParameter("@ADJ_STATUS", simsobj.adj_status),
                                new SqlParameter("@REQ_NO", simsobj.req_no),
                                new SqlParameter("@rd_line_no", simsobj.rd_line_no),
                                new SqlParameter("@REMARK", simsobj.remark),
                                new SqlParameter("@grn_inv_no", simsobj.grn_inv_no),
                                new SqlParameter("@ucode", simsobj.ucode),
                                new SqlParameter("@typ", simsobj.typ)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                            if (simsobj.opr.Equals("AU"))                            
                                message.strMessage = "LPO order release successfully";
                            else if (simsobj.opr.Equals("PU"))
                                message.strMessage = "LPO approve successfully";
                            else
                                message.strMessage = "LPO rejected successfully";
                            
                        }
                        else
                        {
                            inserted = false;
                            if (simsobj.opr.Equals("AU"))
                                message.strMessage = "LPO not order release";
                            else if (simsobj.opr.Equals("PU"))
                                message.strMessage = "LPO not approve";
                            else
                                message.strMessage = "LPO not rejected";
                        }
                    }

                }

            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


    }
}