﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

using SIMSAPI.Models.ERP.gradebookClass;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/StudentTrait")]
    public class StudentTraitController : ApiController
    {

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)
        {

            List<StuTrt> grade_list = new List<StuTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_define_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "G"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StuTrt simsobj = new StuTrt();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {

            List<StuTrt> grade_list = new List<StuTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_define_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StuTrt simsobj = new StuTrt();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllTerms")]
        public HttpResponseMessage getAllTerms(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<StuTrt> lstCuriculum = new List<StuTrt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_define_student_trait_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "K"),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),
                             new SqlParameter("@sims_grade_code", grade_code),
                              new SqlParameter("@sims_section_code", section_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StuTrt sequence = new StuTrt();
                            sequence.sims_term_desc_ar = dr["sims_term_desc_en"].ToString();
                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getTrait")]
        public HttpResponseMessage getTrait(string sims_grade_code, string sims_section_code, string sims_term_code)
        {
            List<StuTrt> mod_list = new List<StuTrt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_define_student_trait_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_term_code",sims_term_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StuTrt hrmsObj = new StuTrt();
                            hrmsObj.sims_trait_code = dr["sims_trait_code"].ToString();
                            hrmsObj.sims_trait_name = dr["sims_trait_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getAllStudentTrait")]
        public HttpResponseMessage getAllStudentTrait(string sims_grade_code, string sims_section_code, string sims_term_code, string sims_trait_code)
        {
            List<studentstrait> mod_list = new List<studentstrait>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_define_student_trait_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","Q"),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@sims_term_code",sims_term_code),
                            new SqlParameter("@sims_trait_code",sims_trait_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentstrait hrmsObj = new studentstrait();

                            hrmsObj.studtraitlst = new List<StuTrt>();
                            hrmsObj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            hrmsObj.stud_name = dr["stud_name"].ToString();

                            string str = dr["sims_enroll_number"].ToString();

                            var v = (from p in mod_list where p.sims_enroll_number == str select p);

                            StuTrt hrmsObj1 = new StuTrt();
                            hrmsObj1.sims_trait_code = dr["sims_trait_code"].ToString();
                            hrmsObj1.sims_trait_name = dr["sims_trait_name"].ToString();
                            hrmsObj1.sims_trait_comment1 = dr["sims_trait_comment1"].ToString();
                            hrmsObj1.sims_trait_comment2 = dr["sims_trait_comment2"].ToString();
                            hrmsObj1.sims_trait_comment3 = dr["sims_trait_comment3"].ToString();
                            hrmsObj1.sims_trait_comment4 = dr["sims_trait_comment4"].ToString();

                            if (v.Count() == 0)
                            {
                                hrmsObj.studtraitlst.Add(hrmsObj1);
                                mod_list.Add(hrmsObj);
                            }
                            else
                            {
                                v.ElementAt(0).studtraitlst.Add(hrmsObj1);
                            }
                        }
                    }
                }
            }

            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("CUStudentTrait")]
        public HttpResponseMessage CUDTrait(List<StuTrt> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (StuTrt simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_define_student_trait_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr","I"),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_term_code",simsobj.sims_term_code),
                                new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                new SqlParameter("@sims_trait_code",simsobj.sims_trait_code),
                                new SqlParameter("@sims_trait_comment1", simsobj.sims_trait_comment1),
                                new SqlParameter("@sims_trait_comment2", simsobj.sims_trait_comment2),
                                new SqlParameter("@sims_trait_comment3", simsobj.sims_trait_comment3),
                                new SqlParameter("@sims_trait_comment4", simsobj.sims_trait_comment4),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}