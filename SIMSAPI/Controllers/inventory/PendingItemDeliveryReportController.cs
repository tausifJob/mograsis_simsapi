﻿using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.inventory
{

    [RoutePrefix("api/pendingdelivery")]
    public class PendingItemDeliveryReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getGradesAll")]
        public HttpResponseMessage getGradesAll(string cur_code, string academic_year)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<PIDR01> grade_list = new List<PIDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_PendingItemDelivery_report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year)
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PIDR01 simsobj = new PIDR01();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeAllNew")]
        public HttpResponseMessage getSectionFromGradeNew(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<PIDR01> grade_list = new List<PIDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_PendingItemDelivery_report_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PIDR01 simsobj = new PIDR01();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }

        }


        [Route("getPendingItemDeliveryReport")]
        public HttpResponseMessage getPendingItemDeliveryReport(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<PIDR01> lstCuriculum = new List<PIDR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_PendingItemDelivery_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'R'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", section_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PIDR01 sequence = new PIDR01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.name = dr["name"].ToString();
                            sequence.doc_sr_code=dr["doc_sr_code"].ToString();
                            sequence.doc_prov_no = dr["doc_prov_no"].ToString();
                            sequence.im_inv_no = dr["im_inv_no"].ToString();
                            sequence.dd_qty = dr["dd_qty"].ToString();
                            sequence.dd_issue_qty = dr["dd_issue_qty"].ToString();
                            sequence.balance_qty = dr["balance_qty"].ToString();
                            sequence.dd_outstanding_qty = dr["dd_outstanding_qty"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.sal_type = dr["sal_type"].ToString();
                            sequence.part_status = dr["part_status"].ToString();
                            sequence.cus_account_no = dr["cus_account_no"].ToString();
                            sequence.creation_user = dr["creation_user"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }
        

       
    }
}