﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/SupplierDescriptionTypes")]
    [BasicAuthentication]
    public class SupplierDescriptionTypesController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Select
        [Route("getAllSupplierDescriptionTypes")]
        public HttpResponseMessage getAllSupplierDescriptionTypes()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSupplierDescriptionTypes(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllSupplierDescriptionTypes"));

            List<Invs009> goaltarget_list = new List<Invs009>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_desc_type_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs009 simsobj = new Invs009();
                            simsobj.sdt_type = dr["sdt_type"].ToString();
                            simsobj.sdt_desc = dr["sdt_desc"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDSupplierDescriptionTypes")]
        public HttpResponseMessage CUDSupplierDescriptionTypes(List<Invs009> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs009 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_supplier_desc_type_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sdt_type",simsobj.sdt_type),
                                new SqlParameter("@sdt_desc", simsobj.sdt_desc),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}