﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS.simsClass;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.FINNANCE;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/ProductCode")]
    [BasicAuthentication]
    public class ProductCodeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getProductCode")]
        public HttpResponseMessage getProductCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getProductCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getProductCode"));

            List<Invs063> goaltarget_list = new List<Invs063>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.pc_code = dr["pc_code"].ToString();
                            simsobj.pc_desc = dr["pc_desc"].ToString();

                            simsobj.compcode = dr["compcode"].ToString();
                            simsobj.compname = dr["compname"].ToString();
                            simsobj.finyear = dr["pc_fin_year"].ToString();
                            simsobj.revenue_acc_no_name = dr["rev_accno"].ToString();
                            simsobj.stock_acc_no_name = dr["stock_accno"].ToString();
                            simsobj.cost_acc_no_name = dr["cost_accno"].ToString();
                            simsobj.dis_acc_no_name = dr["dis_accno"].ToString();

                            simsobj.revenue_acc_no = dr["rev_code"].ToString();
                            simsobj.stock_acc_no = dr["stock_code"].ToString();
                            simsobj.cost_acc_no = dr["cost_code"].ToString();
                            simsobj.dis_acc_no = dr["dis_code"].ToString();

                            try
                            {
                               simsobj.pc_category_group = dr["pc_category_group"].ToString();
                               simsobj.pc_fin_year_desc = dr["pc_fin_year_desc"].ToString();
                            }
                            catch(Exception ex) {
                            }
                            

                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getSubcategoryData")]
        public HttpResponseMessage getSubcategoryData()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubcategoryData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSubcategoryData"));

            List<Invs063> goaltarget_list = new List<Invs063>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();

                            simsobj.pc_code = dr["pc_code"].ToString();
                            simsobj.catname = dr["catname"].ToString();
                            simsobj.subcatname = dr["subcatname"].ToString();
                            simsobj.pc_parent_code = dr["pc_parent_code"].ToString();


                            simsobj.compcode_sub = dr["compcode_sub"].ToString();
                            simsobj.compname_sub = dr["compname_sub"].ToString();
                            simsobj.finyear_sub = dr["pc_fin_year"].ToString();
                            simsobj.revenue_acc_no_name_sub = dr["rev_accno_sub"].ToString();
                            simsobj.stock_acc_no_name_sub = dr["stock_accno_sub"].ToString();
                            simsobj.cost_acc_no_name_sub = dr["cost_accno_sub"].ToString();
                            simsobj.dis_acc_no_name_sub = dr["dis_accno_sub"].ToString();

                            simsobj.revenue_acc_no_sub = dr["rev_code_sub"].ToString();
                            simsobj.stock_acc_no_sub = dr["stock_code_sub"].ToString();
                            simsobj.cost_acc_no_sub = dr["cost_code_sub"].ToString();
                            simsobj.dis_acc_no_sub = dr["dis_code_sub"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //Seelct
        //[Route("FillComboCategory")]
        //public HttpResponseMessage FillComboCategory()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : FillComboCategory(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "Common", "FillComboCategory"));

        //    List<Invs063> combo_list = new List<Invs063>();
        //    //int total = 0, skip = 0;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                   new SqlParameter("@opr", 'T')

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Invs063 simsobj = new Invs063();
        //                    simsobj.pc_code = dr["pc_code"].ToString();
        //                    simsobj.pc_desc = dr["pc_desc"].ToString();

        //                    combo_list.Add(simsobj);

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, combo_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, combo_list);
        //}


        [Route("CUDProductCode")]
        public HttpResponseMessage CUDProductCode(List<Invs063> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs063 simsobj in data)
                    {

                        if (simsobj.pc_parent_code == "undefined")
                        {
                            simsobj.pc_parent_code = null;
                        }
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@pc_desc",simsobj.pc_desc),
                             new SqlParameter("@pc_code", simsobj.pc_code),
                             new SqlParameter("@pc_parent_code", simsobj.pc_parent_code),
                             new SqlParameter("@fin_year", simsobj.finyear),
                             new SqlParameter("@fincompcode", simsobj.compcode),
                             new SqlParameter("@pc_revenue_acno", simsobj.revenue_acc_no),
                             new SqlParameter("@pc_stock_acno", simsobj.stock_acc_no),
                             new SqlParameter("@pc_cost_acno", simsobj.cost_acc_no),
                             new SqlParameter("@pc_discont_acno", simsobj.dis_acc_no),
                             new SqlParameter("@pc_status", 'A'),
                             new SqlParameter("@pc_category_group",simsobj.pc_category_group)
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("DelProductCode")]
        public HttpResponseMessage DelDProductCode(List<Invs063> data)
        {
            string insert = "0"; ;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs063 simsobj in data)
                    {

                        if (simsobj.pc_parent_code == "undefined")
                        {
                            simsobj.pc_parent_code = null;
                        }
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@pc_desc",simsobj.pc_desc),
                             new SqlParameter("@pc_code", simsobj.pc_code),
                             new SqlParameter("@pc_parent_code", simsobj.pc_parent_code),
                             new SqlParameter("@fin_year", simsobj.finyear),
                             new SqlParameter("@fincompcode", simsobj.compcode),
                             new SqlParameter("@pc_revenue_acno", simsobj.revenue_acc_no),
                             new SqlParameter("@pc_stock_acno", simsobj.stock_acc_no),
                             new SqlParameter("@pc_cost_acno", simsobj.cost_acc_no),
                             new SqlParameter("@pc_discont_acno", simsobj.dis_acc_no),
                             new SqlParameter("@pc_status", 'A'),

                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                insert = dr[0].ToString();
                            }
                        }
                        else
                        {
                            insert = "0";
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("GetCategoryNames")]
        public HttpResponseMessage GetCategoryNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCategoryNames()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs063> cat_list = new List<Invs063>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'T'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.pc_desc = dr["pc_desc"].ToString();
                            simsobj.pc_code = dr["pc_code"].ToString();
                            cat_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getGetCompany")]
        public HttpResponseMessage getGetCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGetCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "DEPARTMENTCOMPANY"));

            List<Finn010> company_list = new List<Finn010>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn010 finnobj = new Finn010();
                            finnobj.dept_comp_name = dr["comp_name"].ToString();
                            finnobj.dept_comp_code = dr["comp_code"].ToString();
                            company_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, company_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, company_list);
            }
        }


        [Route("GetDepartments")]
        public HttpResponseMessage GetDepartments()
         {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartments()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartments"));

            List<Invs063> cat_list = new List<Invs063>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.depcode = dr["dep_code"].ToString();
                            simsobj.depname = dr["depdesc"].ToString();
                            cat_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetGlAccounts")]
        public HttpResponseMessage GetGlAccounts(string compcode, string fin_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGlAccounts()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGlAccounts"));

            List<Invs063> cat_list = new List<Invs063>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Q'),
                            new SqlParameter("@fins_comp_code", compcode),
                            new SqlParameter("@fin_year", fin_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.gl_item_no = dr["glma_acct_code"].ToString();
                            simsobj.gl_accname = dr["gldesc"].ToString();
                            cat_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("GetFinancialyears")]
        public HttpResponseMessage GetFinancialyears()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFinancialyears()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetFinancialyears"));

            List<Invs063> cat_list = new List<Invs063>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.fin_year = dr["sims_financial_year"].ToString();
                            simsobj.fin_year_desc = dr["findesc"].ToString();
                            cat_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
               /* Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;*/
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("GetCategoryGroup")]
        public HttpResponseMessage GetCategoryGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFinancialyears()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetFinancialyears"));

            List<Invs063> cat_list = new List<Invs063>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_product_code_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'C')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.pc_category_group = dr["invs_appl_parameter"].ToString();
                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                            cat_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
                /* Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;*/
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

    }
}










