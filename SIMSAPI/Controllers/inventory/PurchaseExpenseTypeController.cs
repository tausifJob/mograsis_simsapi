﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/PurchaseExpenseType")]
    public class PurchaseExpenseTypeController:ApiController
    {

        [Route("getPurchaseExpenseTypeDetails")]
        public HttpResponseMessage getPurchaseExpenseTypeDetails()
        {
            List<Inv027> Expense_list = new List<Inv027>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purchase_expense_types_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv027 obj = new Inv027();
                            
                            obj.pet_code = dr["pet_code"].ToString();
                            obj.pet_desc = dr["pet_desc"].ToString();
                            obj.pet_expense_acno = dr["pet_expense_acno"].ToString();
                            obj.pet_expense_acname = dr["pet_expense_acname"].ToString();
                            obj.pet_flag = dr["pet_flag"].ToString().Equals("Y") ? true : false;

                            Expense_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Expense_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Expense_list);
        }

        [Route("CUDPurchaseExpenseType")]
        public HttpResponseMessage CUDPurchaseExpenseType(List<Inv027> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
         
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv027 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_purchase_expense_types_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@pet_code",simsobj.pet_code),
                          new SqlParameter("@pet_desc", simsobj.pet_desc),
                          new SqlParameter("@pet_expense_acno", simsobj.pet_expense_acno),
                          new SqlParameter("@pet_flag",simsobj.pet_flag==true?"Y":"N")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
              
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}