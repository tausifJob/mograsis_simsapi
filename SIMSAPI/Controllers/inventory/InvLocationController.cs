﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/Location")]
    [BasicAuthentication]
    public class InvLocationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetAllLocation")]
        public HttpResponseMessage GetAllLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLocation(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllLocation"));

            List<Invs062> goaltarget_list = new List<Invs062>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_locations_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs062 invsobj = new Invs062();
                            invsobj.location_code = dr["loc_code"].ToString();
                            invsobj.location_name = dr["loc_name"].ToString();
                            invsobj.address1 = dr["loc_address1"].ToString();
                            invsobj.address2 = dr["loc_address2"].ToString();
                            invsobj.contact_person = dr["loc_contact_person"].ToString();
                            invsobj.phone = dr["loc_phone"].ToString();
                            invsobj.fax_no = dr["loc_fax_no"].ToString();

                            goaltarget_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDLocation")]
        public HttpResponseMessage CUDLocation(List<Invs062> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs062 invsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_locations_proc]",
                        new List<SqlParameter>()
                     {
                         new SqlParameter("@opr", invsobj.opr),
                         new SqlParameter("@loc_code", invsobj.location_code),
                         new SqlParameter("@loc_name", invsobj.location_name),
                         new SqlParameter("@loc_address1", invsobj.address1),
                         new SqlParameter("@loc_address2", invsobj.address2),
                         new SqlParameter("@loc_contact_person", invsobj.contact_person),
                         new SqlParameter("@loc_phone", invsobj.phone),
                         new SqlParameter("@loc_fax_no", invsobj.fax_no),                                                       
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}