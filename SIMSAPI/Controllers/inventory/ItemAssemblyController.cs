﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/ItemAssembly")]
    [BasicAuthentication]
    public class ItemAssemblyController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getItemSerch")]
        public HttpResponseMessage getItemSerch(string im_inv_no,string im_desc,string im_item_code,string invs021_dep_code, string sec_code, string invs021_sup_code, string category_code, string subcategory_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                if (im_inv_no == "undefined") { im_inv_no = null; }
                if (im_desc == "undefined") { im_desc = null; }
                if (im_item_code == "undefined") { im_item_code = null; }
                if (invs021_dep_code == "undefined") { invs021_dep_code = null; }
                if (sec_code == "undefined") { sec_code = null; }
                if (invs021_sup_code == "undefined") { invs021_sup_code = null; }
                if (category_code == "undefined") { category_code = null; }
                if(subcategory_code == "undefined") { subcategory_code = null; }
               
                  using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                            new List<SqlParameter>()
                             {

                               new SqlParameter("@opr", 'S'),
                               new SqlParameter("@im_inv_no", im_inv_no),
                               new SqlParameter("@im_desc",  im_desc),
                               new SqlParameter("@im_item_code",im_item_code),
                               new SqlParameter("@dep_code", invs021_dep_code),
                               new SqlParameter("@sec_code", sec_code),
                               new SqlParameter("@sup_code", invs021_sup_code),
                               new SqlParameter("@category_code",category_code),
                               new SqlParameter("@subcategory_code", subcategory_code)
                             });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Invs058_item simsobj = new Invs058_item();
                                simsobj.im_inv_no = dr["im_inv_no"].ToString();
                                simsobj.sg_name = dr["sg_name"].ToString();
                                simsobj.im_item_code = dr["im_item_code"].ToString();
                                simsobj.im_desc = dr["im_desc"].ToString();
                                simsobj.category = dr["category"].ToString();
                                simsobj.subCategory = dr["subCategory"].ToString();
                                simsobj.dep_code = dr["dep_code"].ToString();
                                simsobj.sec_code = dr["sec_code"].ToString();
                                simsobj.sup_code = dr["sup_code"].ToString();
                                simsobj.uom_code = dr["uom_code"].ToString();
                                simsobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                                simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                                simsobj.im_sell_price = dr["im_sell_price"].ToString();
                                simsobj.sg_desc = dr["sg_desc"].ToString();
                                simsobj.dep_name = dr["dep_name"].ToString();
                                simsobj.uom_name = dr["uom_name"].ToString();
                                simsobj.sec_name = dr["sec_name"].ToString();
                                simsobj.sup_name = dr["sup_name"].ToString();
                                simsobj.curr_qty = dr["curr_qty"].ToString();
                                simsobj.do_qty = dr["do_qty"].ToString();
                                simsobj.ava_aty = dr["ava_aty"].ToString();

                                goaltarget_list.Add(simsobj);

                            }
                        }
                    }
                
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        //[Route("getItemSerchSet")]
        //public HttpResponseMessage getItemSerchSet()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerchSet(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "Common", "getItemSerchSet"));

        //    List<Invs058_item> goaltarget_list = new List<Invs058_item>();
        //    //int total = 0, skip = 0;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
        //                new List<SqlParameter>()
        //                 {

        //                    new SqlParameter("@opr", 'A'),
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Invs058_item simsobj = new Invs058_item();

        //                    simsobj.im_inv_no = dr["im_inv_no"].ToString();
        //                    simsobj.sg_name = dr["sg_name"].ToString();
        //                    simsobj.im_item_code = dr["im_item_code"].ToString();
        //                    simsobj.im_desc = dr["im_desc"].ToString();
        //                    simsobj.category = dr["category"].ToString();
        //                    simsobj.subCategory = dr["subCategory"].ToString();
        //                    simsobj.dep_code = dr["dep_code"].ToString();
        //                    simsobj.sec_code = dr["sec_code"].ToString();
        //                    simsobj.sup_code = dr["sup_code"].ToString();
        //                    simsobj.uom_code = dr["uom_code"].ToString();
        //                    simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
        //                    simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
        //                    simsobj.im_sell_price = dr["im_sell_price"].ToString();
        //                    simsobj.sg_desc = dr["sg_desc"].ToString();
        //                    simsobj.dep_name = dr["dep_name"].ToString();
        //                    simsobj.uom_name = dr["uom_name"].ToString();
        //                    simsobj.sec_name = dr["sec_name"].ToString();
        //                    simsobj.sup_name = dr["sup_name"].ToString();
        //                    simsobj.curr_qty = dr["curr_qty"].ToString();
        //                    simsobj.do_qty = dr["do_qty"].ToString();
        //                    simsobj.ava_aty = dr["ava_aty"].ToString();

        //                    goaltarget_list.Add(simsobj);

        //                }
        //            }
        //        }

        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        //}


        //For Insert


        [Route("Fetch_ItemDetails")]
        public HttpResponseMessage Fetch_ItemDetails(Invs058_item itsr)
        {
            List<Invs058_item> mod_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                          {

                               itsr.im_assembly_ind == true?new SqlParameter("@opr", 'A' ): new SqlParameter("@opr", 'S'),
                                  new SqlParameter("@im_inv_no", itsr.im_inv_no),
                               // new SqlParameter("@sg_name", itsr.invs021_sg_name),
                               
                                  new SqlParameter("@im_item_code", itsr.im_item_code),
                                new SqlParameter("@im_desc", itsr.im_desc),
                                new SqlParameter("@dep_code", itsr.invs021_dep_code),
                                new SqlParameter("@sec_code", itsr.sec_code),
                                new SqlParameter("@sup_code", itsr.invs021_sup_code),
                               // new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind==true?"Y":"N"),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code)
                          });
                    while (dr.Read())
                    {
                        Invs058_item itsrobj = new Invs058_item();
                        itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                        itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                        itsrobj.im_item_code = dr["im_item_code"].ToString();
                        itsrobj.im_desc = dr["im_desc"].ToString();
                        itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                        itsrobj.sec_code = dr["sec_code"].ToString();
                        itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                        itsrobj.old_uom_code = dr["uom_code"].ToString();
                        itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                        itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                        itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                        itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                        itsrobj.new_calqty = dr["ava_aty"].ToString();
                        itsrobj.newQty = dr["do_qty"].ToString();
                        itsrobj.sg_desc = dr["sg_desc"].ToString();
                        itsrobj.sup_name = dr["sup_name"].ToString();
                        itsrobj.dep_name = dr["dep_name"].ToString();
                        itsrobj.uom_name = dr["uom_name"].ToString();
                        itsrobj.sec_name = dr["sec_name"].ToString();
                        itsrobj.loc_code = dr["loc_code"].ToString();
                        itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                        itsrobj.sg_name = dr["sg_name"].ToString();

                        itsrobj.category_name = dr["Category"].ToString();
                        itsrobj.subcategory_name = dr["SubCategory"].ToString();


                        if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                        itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                        itsrobj.ia_component_qty = 0;
                        //itsrobj.pc_desc = dr["pc_desc"].ToString();
                        itsrobj.quantity_assembly = "0";
                        itsrobj.category_code = dr["Category"].ToString();
                        itsrobj.subcategory_code = dr["SubCategory"].ToString();
                        mod_list.Add(itsrobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CUDInvsItemSearch")]
        public HttpResponseMessage CUDInvsItemSearch(List<InvO65> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (InvO65 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_item_assembly]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@im_inv_no", simsobj.im_inv_no),
                                new SqlParameter("@im_inv_no_for", simsobj.im_inv_no_for),
                                new SqlParameter("@ia_component_qty", simsobj.ia_component_qty),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }
            catch (Exception x)
            {
               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        //AssemblyData
        [Route("GetAssembly")]
        public HttpResponseMessage GetAssembly()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAssembly()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetAssembly"));

            List<InvO65> doc_list = new List<InvO65>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvO65 simsobj = new InvO65();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.im_desc = dr["im_inv_no"].ToString()+"-"+ dr["im_desc"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //AssemblyData
        [Route("SelectPreview")]
        public HttpResponseMessage SelectPreview(string assembly)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAssembly()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetAssembly"));

            List<InvO65> doc_list = new List<InvO65>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_item_assembly]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'P'),
                              new SqlParameter("@im_inv_no_for", assembly),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvO65 simsobj = new InvO65();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString(); 
                            simsobj.Descr = dr["Descr"].ToString(); 
                            simsobj.category = dr["category"].ToString(); 
                            simsobj.subCategory = dr["subCategory"].ToString(); 
                            simsobj.ia_component_qty = dr["ia_component_qty"].ToString(); 
                          
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }



        [Route("getCurrencyCode")]
        public HttpResponseMessage getCurrencyCode()
        {
            List<InvO65> SupplierGrp_list = new List<InvO65>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_forward_agent_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvO65 obj = new InvO65();
                           

                            obj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            obj.excg_curcy_desc = dr["excg_curcy_desc"].ToString();

                            SupplierGrp_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
        }


        [Route("CUDForwardAgent")]
        public HttpResponseMessage CUDForwardAgent(List<InvO65> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (InvO65 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_forward_agent_proc]",
                        new List<SqlParameter>()
                     {

                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@fa_code",simsobj.fa_code),
	                      new SqlParameter("@fa_name",simsobj.fa_name),
	                      new SqlParameter("@cur_code",simsobj.excg_curcy_code),
	                      new SqlParameter("@fa_location_ind",simsobj.fa_location_ind),
	                      new SqlParameter("@fa_orig_code",simsobj.fa_orig_code),
	                      new SqlParameter("@fa_address1",simsobj.fa_address1),
	                      new SqlParameter("@fa_address2",simsobj.fa_address2),
	                      new SqlParameter("@fa_address3",simsobj.fa_address3),
	                      new SqlParameter("@fa_city",simsobj.fa_city),
	                      new SqlParameter("@con_code",simsobj.con_code),
	                      new SqlParameter("@fa_telephone_no",simsobj.fa_telephone_no),
	                      new SqlParameter("@fa_fax_no",simsobj.fa_fax_no),
	                      new SqlParameter("@fa_telex_no",simsobj.fa_telex_no),
	                      new SqlParameter("@fa_email_address",simsobj.fa_email_address),
	                      new SqlParameter("@fa_contact_name",simsobj.fa_contact_name),
	                      new SqlParameter("@fa_contact_designation",simsobj.fa_contact_designation),
	                      new SqlParameter("@fa_discount_pct",simsobj.fa_discount_pct),
	                      new SqlParameter("@fa_credit_period",simsobj.fa_credit_period),
	                      new SqlParameter("@fa_remarks",simsobj.@fa_remarks),

                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }
            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        //AssemblyData
        [Route("GetForwardAgent")]
        public HttpResponseMessage GetForwardAgent()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAssembly()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetAssembly"));

            List<InvO65> doc_list = new List<InvO65>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_forward_agent_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvO65 simsobj = new InvO65();
                          
                            simsobj.fa_code= dr["fa_code"].ToString();
	                        simsobj.fa_name= dr["fa_name"].ToString();
	                        simsobj.cur_code= dr["cur_code"].ToString();
	                        simsobj.fa_location_ind= dr["fa_location_ind"].ToString();
	                        simsobj.fa_orig_code= dr["fa_orig_code"].ToString();
	                        simsobj.fa_address1= dr["fa_address1"].ToString();
	                        simsobj.fa_address2= dr["fa_address2"].ToString();
	                        simsobj.fa_address3= dr["fa_address3"].ToString();
	                        simsobj.fa_city= dr["fa_city"].ToString();
	                        simsobj.con_code= dr["con_code"].ToString();
	                        simsobj.fa_telephone_no= dr["fa_telephone_no"].ToString();
	                        simsobj.fa_fax_no= dr["fa_fax_no"].ToString();
	                        simsobj.fa_telex_no= dr["fa_telex_no"].ToString();
	                        simsobj.fa_email_address= dr["fa_email_address"].ToString();
	                        simsobj.fa_contact_name= dr["fa_contact_name"].ToString();
	                        simsobj.fa_contact_designation= dr["fa_contact_designation"].ToString();
	                        simsobj.fa_discount_pct= dr["fa_discount_pct"].ToString();
	                        simsobj.fa_credit_period= dr["fa_credit_period"].ToString();
                            simsobj.fa_remarks = dr["fa_remarks"].ToString();
                            simsobj.currency_name= dr["currency_name"].ToString();
                            simsobj.country_name = dr["country_name"].ToString();

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getAgentCode")]
        public HttpResponseMessage getAgentCode()
        {
            List<InvO65> SupplierGrp_list = new List<InvO65>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_forward_agent_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvO65 obj = new InvO65();


                            obj.agent_code = dr["agent_code"].ToString();
                           
                            SupplierGrp_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SupplierGrp_list);
        }


    }


}










