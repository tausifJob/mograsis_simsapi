﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.modules.Medical_Immunization_StudentController
{
    [RoutePrefix("api/Sales_Doc")]
    [BasicAuthentication]
    public class Sales_DocController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("GetAllSalesType")]
        public HttpResponseMessage GetAllSalesType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[sale_types]"),
                 new SqlParameter("@tbl_col_name1", "[sal_type],[sal_name]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sal_type]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sal_type = dr["sal_type"].ToString();
                            invsObj.sal_type_name = dr["sal_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("GetAllBanks")]
        public HttpResponseMessage GetAllBanks()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "TimeTable"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[banks]"),
                 new SqlParameter("@tbl_col_name1", "[bk_code],[bk_name]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[bk_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.bk_code = dr["bk_code"].ToString();
                            invsObj.bk_name = dr["bk_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("GetAllDocumentType")]
        public HttpResponseMessage GetAllDocumentType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllDocumentType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllDocumentType"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[document_types]"),
                 new SqlParameter("@tbl_col_name1", "[dt_code],[dt_desc]"),
                 new SqlParameter("@tbl_cond", "dt_ind='A'"),
                 //new SqlParameter("@tbl_ordby", "[sal_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.dt_code = dr["dt_code"].ToString();
                            invsObj.dt_desc = dr["dt_desc"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetAllSupplierGroupName")]
        public HttpResponseMessage GetAllSupplierGroupName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSupplierGroupName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSupplierGroupName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[supplier_groups]"),
                 new SqlParameter("@tbl_col_name1", "[sg_name],[sg_desc]"),
                 new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "[sg_name]")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.name = dr["sg_desc"].ToString();
                            invsObj.code = dr["sg_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_supplier_code")]
        public HttpResponseMessage get_supplier_code()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSupplierGroupName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSupplierGroupName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[suppliers]"),
                 new SqlParameter("@tbl_col_name1", "sup_code,sup_name"),
               //  new SqlParameter("@tbl_cond", ""),
                 new SqlParameter("@tbl_ordby", "sup_name")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();

                            invsObj.name = dr["sup_name"].ToString();
                            invsObj.code = dr["sup_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_Categories")]
        public HttpResponseMessage get_Categories()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Invs021_get_Categories(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Invs021_get_Categories"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_master",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "X"),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.category_name = dr["pc_desc"].ToString();
                            invsObj.category_code = dr["pc_code"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_SubCategories")]
        public HttpResponseMessage get_SubCategories(string pc_parentcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Invs021_get_Categories(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Invs021_get_Categories"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("item_master",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "Y"),
                 new SqlParameter("@pc_parent_code",pc_parentcode),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.subcategory_name = dr["pc_desc"].ToString();
                            invsObj.subcategory_code = dr["pc_code"].ToString();
                            try {
                                invsObj.pc_grade_code = dr["pc_grade_code"].ToString();
                            }
                            catch (Exception ex) { }

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

       


        [Route("GetAllItemsInSubCategory")]
        public HttpResponseMessage GetAllItemsInSubCategory(string pc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[item_master]"),
                 new SqlParameter("@tbl_col_name1", "im_inv_no,[im_item_code],[im_desc]"),
                 new SqlParameter("@tbl_cond", "[im_assembly_ind]='N' and pc_code='" + pc_code + "'")
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("Fetch_ItemDetails_ItemSet")]
        public HttpResponseMessage Fetch_ItemDetails_ItemSet(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'B'),
                 new SqlParameter("@im_inv_no", im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimal component_qty = 0;

                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.loc_name = dr["loc_name"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();

                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            {
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            }

                            if (!string.IsNullOrEmpty(dr["im_sell_price"].ToString()))
                            {
                                itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            }
                            else
                            {
                                itsrobj.dd_sell_price = 0;
                            }

                            try
                            {
                               if (string.IsNullOrEmpty(dr["im_sell_price_including_vat"].ToString()) == false)
                                   itsrobj.im_sell_price_including_vat = decimal.Parse(dr["im_sell_price_including_vat"].ToString());
                               else
                                   itsrobj.im_sell_price_including_vat = 0;
                            }
                            catch (Exception e){}



                            //   list.Add(itsrobj);


                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("invs_itemsearch_query",
                                    new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr",  'c'),
                 new SqlParameter("@im_inv_no",  itsrobj.im_inv_no),
                 new SqlParameter("@im_inv_no_for",  im_inv_no),

                                           
                         });

                                if (dr1.Read())
                                {
                                    component_qty = decimal.Parse(dr1["Quantity"].ToString());

                                }
                                itsrobj.ia_component_qty = component_qty;

                            }


                            list.Add(itsrobj);



                        }
                    }
                    db.Dispose();

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Fetch_ItemDetails_ItemSetforPearl")]
        public HttpResponseMessage Fetch_ItemDetails_ItemSetforPearl(string im_inv_no,string loc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<Invs058_item> list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query_pearl",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr",  'B'),
                        new SqlParameter("@im_inv_no", im_inv_no),
                        new SqlParameter("@loc_code", loc_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimal component_qty = 0;

                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.loc_name = dr["loc_name"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();

                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                            {
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            }

                            if (!string.IsNullOrEmpty(dr["im_sell_price"].ToString()))
                            {
                                itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            }


                            //   list.Add(itsrobj);


                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("invs_itemsearch_query",
                                new List<SqlParameter>()
                                {
                                     new SqlParameter("@opr",  'c'),
                                     new SqlParameter("@im_inv_no",  itsrobj.im_inv_no),
                                     new SqlParameter("@im_inv_no_for",  im_inv_no),                                     
                                });

                                if (dr1.Read())
                                {
                                    component_qty = decimal.Parse(dr1["Quantity"].ToString());

                                }
                                itsrobj.ia_component_qty = component_qty;

                            }
                            
                            list.Add(itsrobj);                            
                        }
                    }
                    db.Dispose();

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetItemCodeDetails")]
        public HttpResponseMessage GetItemCodeDetails(string item_code, string sg_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "S"),
                 new SqlParameter("@im_item_code", item_code),
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.original_qty = "0";
                            //invsObj.im_sell_price = 0;
                            try
                            {
                                invsObj.original_qty = dr["curr_qty"].ToString();
                            }
                            catch (Exception dee)
                            {
                                invsObj.original_qty = "0";
                            }
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;
                            invsObj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();

                            try
                            {
                                if (string.IsNullOrEmpty(dr["im_sell_price_including_vat"].ToString()) == false)
                                    invsObj.im_sell_price_including_vat = decimal.Parse(dr["im_sell_price_including_vat"].ToString());
                                else
                                    invsObj.im_sell_price_including_vat = 0;
                            }
                            catch(Exception e){}
                            try
                            {
                                invsObj.dep_code = dr["dep_code"].ToString();
                            }
                            catch (Exception e){}
                            try
                            {
                                invsObj.im_revenue_acno = dr["im_revenue_acno"].ToString();
                                invsObj.im_cost_acno = dr["im_cost_acno"].ToString();
                                invsObj.im_vat_sale_profile = dr["im_vat_sale_profile"].ToString();                                
                            }
                            catch (Exception e) { }

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetItemCodeDetailsForPearl")]
        public HttpResponseMessage GetItemCodeDetailsForPearl(string item_code, string sg_name,string im_inv_no,string loc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs_itemsearch_query_pearl",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "S"),
                        new SqlParameter("@im_item_code", item_code),
                        new SqlParameter("@im_inv_no", im_inv_no),
                        new SqlParameter("@loc_code", loc_code)
                    });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.original_qty = "0";
                            invsObj.im_sell_price = 0;
                            try
                            {
                                invsObj.original_qty = dr["curr_qty"].ToString();
                            }
                            catch (Exception dee)
                            {
                                invsObj.original_qty = "0";
                            }
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;
                            invsObj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetItemCodeLocations")]
        public HttpResponseMessage GetItemCodeLocations(string im_inv_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetItemCodeLocations(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetItemCodeLocations"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "invs.item_locations a inner join invs.locations b on a.loc_code=b.loc_code"),
                 new SqlParameter("@tbl_col_name1", "a.im_inv_no,a.loc_code,a.il_cur_qty,b.loc_name"),
                 new SqlParameter("@tbl_cond", "im_inv_no=" + im_inv_no),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.item_quantity = int.Parse(dr["il_cur_qty"].ToString());
                            invsObj.item_location_name = dr["loc_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetLocations")]
        public HttpResponseMessage GetLocations()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetItemCodeLocations(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetLocations"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                    new List<SqlParameter>()
                    {                        
                         new SqlParameter("@tbl_name", "[invs].[locations]"),
                         new SqlParameter("@tbl_col_name1", "[loc_code],[loc_name]"),
                         //new SqlParameter("@tbl_cond", "sm_status='A'"),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.loc_code = dr["loc_code"].ToString();                            
                            invsObj.item_location_name = dr["loc_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAllSalesmans")]
        public HttpResponseMessage GetAllSalesmans()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSalesmans(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllSalesmans"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[salesman]"),
                 new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name]"),
                 new SqlParameter("@tbl_cond", "sm_status='A'"),//invs_comp_code=1 and invs_appl_code='Inv035' and invs_appl_form_field='Sales Type'
              
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        public class Invs058_itemNew
        {

            public bool im_assembly_ind { get; set; }

            public string im_inv_no { get; set; }

            public string invs021_sg_name { get; set; }

            public string im_item_code { get; set; }

            public string im_desc { get; set; }

            public string invs021_dep_code { get; set; }

            public string sec_code { get; set; }

            public string invs021_sup_code { get; set; }

            public string im_assembly_ind_s { get; set; }

            public string category_code { get; set; }

            public string subcategory_code { get; set; }
            public string loc_code { get; set; }
        }
        //Select
        [Route("postgetItemSerch")]
        public HttpResponseMessage postgetItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));
            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                 new SqlParameter("@opr", opr),
                 new SqlParameter("@im_inv_no", itsr.im_inv_no),
                 new SqlParameter("@sg_name", itsr.invs021_sg_name),
                 new SqlParameter("@im_item_code", itsr.im_item_code),
                 new SqlParameter("@im_desc", itsr.im_desc),
                 new SqlParameter("@dep_code", itsr.invs021_dep_code),
                 new SqlParameter("@sec_code", itsr.sec_code),
                 new SqlParameter("@sup_code", itsr.invs021_sup_code),
                 new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                 new SqlParameter("@category_code", itsr.category_code),
                 new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            //itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            // itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                itsrobj.dd_sell_price = 0;

                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();
                            try
                            {
                                itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            }
                            catch (Exception ex) { }

                            try
                            {
                                if (string.IsNullOrEmpty(dr["im_sell_price_including_vat"].ToString()) == false)
                                    itsrobj.im_sell_price_including_vat = decimal.Parse(dr["im_sell_price_including_vat"].ToString());
                                else
                                    itsrobj.im_sell_price_including_vat = 0;
                            }
                            catch (Exception e) { }
                            try
                            {
                                itsrobj.dep_code = dr["dep_code"].ToString();
                            }
                            catch (Exception e) { }
                            try
                            {
                                itsrobj.im_revenue_acno = dr["im_revenue_acno"].ToString();
                                itsrobj.im_cost_acno = dr["im_cost_acno"].ToString();
                                itsrobj.im_vat_sale_profile = dr["im_vat_sale_profile"].ToString();
                            }
                            catch (Exception e) { }

                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("postgetItemSerchPearl")]
        public HttpResponseMessage postgetItemSerchPearl(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));
            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_pearl_proc]",
                    new List<SqlParameter>()
                    {

                               new SqlParameter("@opr", opr),
                               new SqlParameter("@im_inv_no", itsr.im_inv_no),
                               new SqlParameter("@sg_name", itsr.invs021_sg_name),
                               new SqlParameter("@im_item_code", itsr.im_item_code),
                               new SqlParameter("@im_desc", itsr.im_desc),
                               new SqlParameter("@dep_code", itsr.invs021_dep_code),
                               new SqlParameter("@sec_code", itsr.sec_code),
                               new SqlParameter("@sup_code", itsr.invs021_sup_code),
                               new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                               new SqlParameter("@category_code", itsr.category_code),
                               new SqlParameter("@subcategory_code", itsr.subcategory_code),
                               new SqlParameter("@loc_code", itsr.loc_code),

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item itsrobj = new Invs058_item();
                            itsrobj.im_inv_no = dr["im_inv_no"].ToString();
                            itsrobj.invs021_sg_name = dr["sg_name"].ToString();
                            itsrobj.im_item_code = dr["im_item_code"].ToString();
                            itsrobj.im_desc = dr["im_desc"].ToString();
                            itsrobj.invs021_dep_code = dr["dep_code"].ToString();
                            itsrobj.sec_code = dr["sec_code"].ToString();
                            itsrobj.invs021_sup_code = dr["sup_code"].ToString();
                            itsrobj.old_uom_code = dr["uom_code"].ToString();
                            itsrobj.im_assembly_ind_s = dr["im_assembly_ind"].ToString();
                            itsrobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            itsrobj.im_sell_price = dr["im_sell_price"].ToString();
                            itsrobj.id_cur_qty = dr["curr_qty"].ToString();
                            itsrobj.new_calqty = dr["ava_aty"].ToString();
                            itsrobj.newQty = dr["do_qty"].ToString();
                            itsrobj.sg_desc = dr["sg_desc"].ToString();
                            itsrobj.sup_name = dr["sup_name"].ToString();
                            itsrobj.dep_name = dr["dep_name"].ToString();
                            itsrobj.uom_name = dr["uom_name"].ToString();
                            itsrobj.sec_name = dr["sec_name"].ToString();
                            itsrobj.loc_code = dr["loc_code"].ToString();
                            itsrobj.item_location_name = dr["loc_code"].ToString() + "-" + dr["il_cur_qty"].ToString();
                            itsrobj.sg_name = dr["sg_name"].ToString();

                            itsrobj.category_name = dr["Category"].ToString();
                            itsrobj.subcategory_name = dr["SubCategory"].ToString();


                            if (!string.IsNullOrEmpty(dr["il_cur_qty"].ToString()))
                                itsrobj.dd_qty = int.Parse(dr["il_cur_qty"].ToString());
                            itsrobj.dd_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            itsrobj.ia_component_qty = 0;
                            //itsrobj.pc_desc = dr["pc_desc"].ToString();
                            itsrobj.quantity_assembly = "0";
                            itsrobj.category_code = dr["Category"].ToString();
                            itsrobj.subcategory_code = dr["SubCategory"].ToString();
                            try
                            {
                                itsrobj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            }
                            catch (Exception ex) { }
                            goaltarget_list.Add(itsrobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("Get_StudentsforSearch")]
        public HttpResponseMessage Get_StudentsforSearch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_StudentsforSearch(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Get_StudentsforSearch"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "P"),
               
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.studentenroll = dr["sims_student_enroll_number"].ToString();
                            invsObj.studentname = dr["studentname"].ToString();

                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        public string GetStudentName(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));
            string StudentName = string.Empty;

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@tbl_name", "sims.sims_student"),
                new SqlParameter("@tbl_col_name1", "[sims_student_passport_first_name_en]+' '+[sims_student_passport_middle_name_en]+' '+[sims_student_passport_last_name_en] as StudentName"),
                new SqlParameter("@tbl_cond", "sims_student_enroll_number='" + enrollnum + "'"),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            StudentName = dr["StudentName"].ToString();

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return StudentName;

            }
            return StudentName;
        }





        public List<invs035> GetSalesmanName(string sales_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesmanName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesmanName"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@tbl_name", "[invs].[salesman]"),
                new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name],[dep_code]"),
                new SqlParameter("@tbl_cond", "sm_status='A' and sm_user_code='" + sales_code + "'"),//invs_comp_code=1 and invs_appl_code='Inv035' and invs_appl_form_field='Sales Type'
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return list;

            }
            return list;
        }


        [Route("Get_Sale_DocumentsFromSearchResults")]
        public HttpResponseMessage Get_Sale_DocumentsFromSearchResults(invs035 obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Sale_DocumentsFromSearchResults(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "Get_Sale_DocumentsFromSearchResults"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                new SqlParameter("@opr", 'B'),
                new SqlParameter("@doc_prov_no", obj.doc_prov_no),
                new SqlParameter("@from_docdate", db.DBYYYYMMDDformat(obj.fromdate)),
                new SqlParameter("@to_docdate", db.DBYYYYMMDDformat(obj.todate)),
                new SqlParameter("@cus_account_no", obj.cus_account_no),
                new SqlParameter("@studentname", obj.studentname),
                new SqlParameter("@sm_code", obj.sm_code),
                
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_prov_no"].ToString()) == false)
                                invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_prov_date"].ToString()) == false)
                            {
                                //string dt = Convert.ToDateTime(dr["doc_prov_date"].ToString()).ToShortDateString();
                                //invsObj.doc_prov_date = dt;
                                invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            }
                            if (string.IsNullOrEmpty(dr["doc_no"].ToString()) == false)
                                invsObj.doc_no = int.Parse(dr["doc_no"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_date"].ToString()) == false)
                            {
                                //string dt1 = Convert.ToDateTime(dr["doc_date"].ToString()).ToShortDateString();
                                //invsObj.doc_date = dt1.ToString();
                                invsObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            }

                            invsObj.search_dtcode = dr["dcode"].ToString();
                            invsObj.dt_code = ": " + dr["dt_code"].ToString();

                            invsObj.dr_code = dr["dr_code"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.search_smcode = dr["sm_code"].ToString();
                            if (!(string.IsNullOrEmpty(invsObj.sm_code)))
                            {
                                List<invs035> lst_obj = new List<invs035>();
                                lst_obj = GetSalesmanName(invsObj.sm_code);
                                foreach (invs035 obj1 in lst_obj)
                                {
                                    invsObj.sm_code = obj1.sm_name;
                                }
                            }

                            invsObj.up_name = dr["up_name"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();
                            invsObj.sal_type_name = ": " + dr["sal_type_name"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            invsObj.doc_status = dr["doc_status"].ToString();
                            invsObj.doc_status_name = dr["doc_status_name"].ToString();
                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            if (invsObj.sal_type == "04")
                            {
                                invsObj.ic_account_no = GetStudentName(invsObj.cus_account_no);
                            }
                            else
                            {
                                invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            }
                            invsObj.doc_special_name = invsObj.ic_account_no;//dr["doc_special_name"].ToString();
                            invsObj.dep_code_caused_by = dr["dep_code_caused_by"].ToString();
                            invsObj.doc_order_ref_no = dr["doc_order_ref_no"].ToString();
                            invsObj.doc_other_charge_desc = dr["doc_other_charge_desc"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_other_charge_amount"].ToString()) == false)
                                invsObj.doc_other_charge_amount = decimal.Parse(dr["doc_other_charge_amount"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_discount_pct"].ToString()) == false)
                                invsObj.doc_discount_pct = decimal.Parse(dr["doc_discount_pct"].ToString());
                            if (string.IsNullOrEmpty(dr["doc_discount_amount"].ToString()) == false)
                                invsObj.doc_discount_amount = decimal.Parse(dr["doc_discount_amount"].ToString());
                            invsObj.doc_delivery_remarks = dr["doc_delivery_remarks"].ToString();
                            invsObj.doc_validity_remarks = dr["doc_validity_remarks"].ToString();
                            invsObj.ow_id = dr["ow_id"].ToString();
                            invsObj.doc_jobcard_dept = dr["doc_jobcard_dept"].ToString();
                            invsObj.doc_jobcard_loc = dr["doc_jobcard_loc"].ToString();
                            invsObj.doc_jobcard_no = dr["doc_jobcard_no"].ToString();
                            invsObj.sd_icc_code = dr["sd_icc_code"].ToString();
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.sec_code = dr["sec_code"].ToString();
                            if (string.IsNullOrEmpty(dr["creation_date"].ToString()) == false)
                                invsObj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            invsObj.creation_user = dr["creation_user"].ToString();
                            invsObj.creation_term = dr["creation_term"].ToString();
                            if (string.IsNullOrEmpty(dr["modification_date"].ToString()) == false)
                                invsObj.modification_date = db.UIDDMMYYYYformat(dr["modification_date"].ToString());
                            invsObj.modification_user = dr["modification_user"].ToString();
                            invsObj.modification_term = dr["modification_term"].ToString();
                            invsObj.doc_cheque_no = dr["doc_cheque_no"].ToString();
                            invsObj.doc_cheque_date = db.UIDDMMYYYYformat(dr["doc_cheque_date"].ToString());
                            invsObj.doc_cheque_bank = dr["doc_cheque_bank"].ToString();
                            invsObj.original_qty = "0";
                            if (!string.IsNullOrEmpty(dr["doc_discount_pct"].ToString()))
                            {
                                invsObj.discnt_percentage = decimal.Parse(dr["doc_discount_pct"].ToString());
                            }
                            else
                                invsObj.discnt_percentage = 0;


                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetSale_FeeDetails")]
        public HttpResponseMessage GetSale_FeeDetails(string enroll)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<sims043> list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "R"),
                 new SqlParameter("@studentenrollnum", enroll),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_id = dr["id"].ToString();
                            simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                            simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                            simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                            simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                            simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                            simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                            simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                            simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                            simsobj.std_fee_child_paying_amount_temp = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_concession_amount_used = "0";

                            if (dr["installment_mode"].ToString().ToLower() == "y")
                                simsobj.Installment_mode = true;
                            else
                                simsobj.Installment_mode = false;

                            simsobj.Installment_mode_chk = false;
                            simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                            simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }




        [Route("GetSalesman_code")]
        public HttpResponseMessage GetSalesman_code(string usercd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[salesman]"),
                 new SqlParameter("@tbl_col_name1", "[sm_code],[sm_name]"),
                 new SqlParameter("@tbl_cond", "sm_status='A' and sm_user_code='" + usercd + "'"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sm_name = dr["sm_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetItemsBySupplier")]
        public HttpResponseMessage GetItemsBySupplier(string sup_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@tbl_name", "[invs].[item_master]"),
                 new SqlParameter("@tbl_col_name1", "im_inv_no],[im_desc]"),
                 new SqlParameter("@tbl_cond", "sup_code='" + sup_code + "'"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.code = dr["im_inv_no"].ToString();
                            invsObj.name = dr["im_desc"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Insert_Sale_Documents")]
        public HttpResponseMessage Insert_Sale_Documents(invs035 invsObj)
        {
            string prov_no = string.Empty;
            bool vat_flag = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));


                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, prov_no);
            }

            return Request.CreateResponse(HttpStatusCode.OK, prov_no);


        }

        [Route("Insert_Sale_Documents_VAT")]
        public HttpResponseMessage Insert_Sale_Documents_VAT(invs035 invsObj)
        {
            string prov_no = string.Empty;
            bool vat_flag = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));
                    para.Add(new SqlParameter("@doc_vat_total_amount", invsObj.doc_vat_total_amount));
                    para.Add(new SqlParameter("@doc_round_value", invsObj.doc_round_value));

                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, prov_no);
            }

            return Request.CreateResponse(HttpStatusCode.OK, prov_no);


        }

        [Route("Insert_Sale_Documents_Details")]
        public HttpResponseMessage Insert_Sale_Documents_Details(invs035 invsObj)
        {
            bool result = false;


            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                    //if (invsObj.im_inv_no > 0)
                    para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                    if (string.IsNullOrEmpty(invsObj.loc_code))
                        invsObj.loc_code = "01";
                    para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                    para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                    para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                    para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                    para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                    para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                    para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                    para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                    para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                    para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                    para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                    para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                    para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                    para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                    para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                    para.Add(new SqlParameter("@test", invsObj.test));
                    para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                    para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                    para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                    para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                    para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                    para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                    para.Add(new SqlParameter("@dd_cheque_date", db.DBYYYYMMDDformat(invsObj.dd_cheque_date)));

                    //try
                    //{
                    //    string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                    //    para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                    //}
                    //catch (Exception dee)
                    //{ para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                    para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_document_details", para);
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);


        }


        [Route("Insert_Sale_Documents_Details1")]
        public HttpResponseMessage Insert_Sale_Documents_Details1(List<invs035> invslst)
        {
            bool result = false;
            string msg = "";

            foreach (invs035 invsObj in invslst)
            {
            try
            {
               
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                    //if (invsObj.im_inv_no > 0)
                    para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                    if (string.IsNullOrEmpty(invsObj.loc_code))
                        invsObj.loc_code = "01";
                    para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                    para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                    para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                    para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                    para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                    para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                    para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                    para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                    para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                    para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                    para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                    para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                    para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                    para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                    para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                    para.Add(new SqlParameter("@test", invsObj.test));
                    para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                    para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                    para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                    para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                    para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                    para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                    para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));
                    para.Add(new SqlParameter("@dd_cheque_date", db.DBYYYYMMDDformat(invsObj.dd_cheque_date)));

                    //try
                    //{
                    //    string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                    //    para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                    //}
                    //catch (Exception dee)
                    //{ para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                    para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_document_details", para);
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                        try
                        {
                            if(dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    msg = dr["msg"].ToString();
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            msg = "";
                        }
                        
                    }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }
            
            if(string.IsNullOrEmpty(msg))
                return Request.CreateResponse(HttpStatusCode.OK, result);
            else
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            

        }

        [Route("Insert_Sale_Documents_Details1_VAT")]
        public HttpResponseMessage Insert_Sale_Documents_Details1_VAT(List<invs035> invslst)
        {
            bool result = false;
            string msg = "";

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'I'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));

                        para.Add(new SqlParameter("@dd_item_vat_per", invsObj.dd_item_vat_per));
                        para.Add(new SqlParameter("@dd_item_vat_amount", invsObj.dd_item_vat_amount));
                        para.Add(new SqlParameter("@dd_cheque_date", db.DBYYYYMMDDformat(invsObj.dd_cheque_date)));
                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));

                        
                        //try
                        //{
                        //    string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                        //    para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                        //}
                        //catch (Exception dee)
                        //{ para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }



                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_document_details_vat]", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        try
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    msg = dr["msg"].ToString();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            msg = "";
                        }
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }

            if (string.IsNullOrEmpty(msg))
                return Request.CreateResponse(HttpStatusCode.OK, result);
            else
                return Request.CreateResponse(HttpStatusCode.OK, msg);


        }


        [Route("GetUserProfiles")]
        public HttpResponseMessage GetUserProfiles()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSalesman_code(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetSalesman_code"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "V"),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.up_num = dr["up_name"].ToString();
                            invsObj.up_name = dr["up_actual_name"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Insert_User_profiles")]
        public HttpResponseMessage Insert_User_profiles(string name)
        {
            bool result = false;
            string inserted = string.Empty;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'W'));
                    para.Add(new SqlParameter("@up_actual_name", name));

                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents", para);
                    // int r = dr.RecordsAffected;
                    //result = r > 0 ? true : false;

                    if (dr.Read())
                    {
                        inserted = dr["UPNUM"].ToString();
                    }

                    //                    inserted = dr["UPNUM"].ToString();

                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }


        [Route("GetStudentInfo")]
        public HttpResponseMessage GetStudentInfo(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            invs035 invsObj = new invs035();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "Q"),
                new SqlParameter("@studentenrollnum", enrollnum),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            invsObj.grade_name = dr["gradename"].ToString();
                            invsObj.section_name = dr["sectionname"].ToString();
                            invsObj.house_name = dr["housename"].ToString();
                            invsObj.studentname = dr["studentname"].ToString();
                            invsObj.grade_code = dr["sims_grade_code"].ToString();

                            invsObj.studentenroll = enrollnum;

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);

        }



        [Route("GetAllItemsInSubCategoryNew")]
        public HttpResponseMessage GetAllItemsInSubCategoryNew(string pc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "X"),
                new SqlParameter("@pc_code", pc_code),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            try
                            {
                                invsObj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();                                
                            }
                            catch(Exception x)
                            {

                            }

                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAllItemsByItemCodeSubCategory")]
        public HttpResponseMessage GetAllItemsByItemCodeSubCategory(string pc_code, string loc_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "Z"),
                                new SqlParameter("@pc_code", pc_code),
                                new SqlParameter("@loc_code", loc_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            try
                            {
                                invsObj.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                            }
                            catch (Exception x)
                            {

                            }
                            mod_list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;
            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("GetAllItemsInSubCategoryByDepartment")]
        public HttpResponseMessage GetAllItemsInSubCategoryByDepartment(string pc_code,string dept_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            List<invs035> mod_list = new List<invs035>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                    new List<SqlParameter>()
                   {                        
                        new SqlParameter("@opr", "Z"),
                        new SqlParameter("@pc_code", pc_code),
                        new SqlParameter("@dep_code", dept_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            invs035 invsObj = new invs035();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_item_code"].ToString() + "-" + dr["im_desc"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());
                            invsObj.sg_name = dr["sg_name"].ToString();
                            invsObj.pc_code = dr["pc_code"].ToString();
                            invsObj.sup_code = dr["sup_code"].ToString();
                            try
                            {
                                invsObj.dep_code = dr["dep_code"].ToString();
                            }
                            catch (Exception e) { }

                            mod_list.Add(invsObj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetPrintDetails")]
        public HttpResponseMessage GetPrintDetails(string provNo)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "C"),
                 new SqlParameter("@doc_prov_no", provNo),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_remark = dr["doc_remark"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();

                            invsObj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj.doc_date = dr["doc_date"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();

                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();


                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            else
                                invsObj.doc_total_amount = 0;


                            if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                invsObj.dd_physical_qty = decimal.Parse(dr["id_cur_qty"].ToString());
                            else
                                invsObj.dd_physical_qty = 0;


                            //    invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_inv_no"].ToString() + " - " + dr["im_desc"].ToString();
                            //invsObj.im_sell_price = 0;

                            if (string.IsNullOrEmpty(dr["dd_qty"].ToString()) == false)
                                invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            else
                                invsObj.dd_qty = 0;

                            if (string.IsNullOrEmpty(dr["dd_sell_value_final"].ToString()) == false)
                                invsObj.dd_sell_value_final = decimal.Parse(dr["dd_sell_value_final"].ToString());
                            else
                                invsObj.dd_sell_value_final = 0;

                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;



                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("update_salesDocList")]
        public HttpResponseMessage update_salesDocList(string provNo, string amt)
        {
            bool result = false;
            string inserted = string.Empty;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();
                    para.Add(new SqlParameter("@opr", 'Y'));
                    para.Add(new SqlParameter("@doc_prov_no", provNo));
                    para.Add(new SqlParameter("@dd_sell_price_final", amt));

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]", para);
                    // int r = dr.RecordsAffected;
                    //result = r > 0 ? true : false;

                    if (dr.Read())
                    {
                        inserted = dr["UPNUM"].ToString();
                    }

                    //                    inserted = dr["UPNUM"].ToString();

                }
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }


        [Route("Insert_Sale_Documents_Details1_Print")]
        public HttpResponseMessage Insert_Sale_Documents_Details1_Print(List<invs035> invslst)
        {
            bool result = false;

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'X'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));

                        try
                        {
                            string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                            para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                        }
                        catch (Exception dee)
                        { para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }

                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));


                        SqlDataReader dr = db.ExecuteStoreProcedure("sale_document_details", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);


        }


        [Route("GetExhangeDetails")]
        public HttpResponseMessage GetExhangeDetails(string provNo)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllItemsInSubCategory(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetAllItemsInSubCategory"));

            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sale_document_details]",
                        new List<SqlParameter>() 
                         { 
                           
                 new SqlParameter("@opr", "B"),
                 new SqlParameter("@doc_prov_no", provNo),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_remark = dr["doc_remark"].ToString();
                            invsObj.im_inv_no = int.Parse(dr["im_inv_no"].ToString());

                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.sal_type = dr["sal_type"].ToString();

                            invsObj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            invsObj.doc_narration = dr["doc_narration"].ToString();
                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            invsObj.sm_code = dr["sm_code"].ToString();
                            invsObj.sg_name = dr["sg_name"].ToString();

                            invsObj.sm_name = dr["sm_name"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();


                            if (string.IsNullOrEmpty(dr["doc_total_amount"].ToString()) == false)
                                invsObj.doc_total_amount = decimal.Parse(dr["doc_total_amount"].ToString());
                            else
                                invsObj.doc_total_amount = 0;


                            if (string.IsNullOrEmpty(dr["id_cur_qty"].ToString()) == false)
                                invsObj.dd_physical_qty = decimal.Parse(dr["id_cur_qty"].ToString());
                            else
                                invsObj.dd_physical_qty = 0;


                            //    invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_inv_no"].ToString() + " - " + dr["im_desc"].ToString();
                            //invsObj.im_sell_price = 0;

                            if (string.IsNullOrEmpty(dr["dd_qty"].ToString()) == false)
                                invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            else
                                invsObj.dd_qty = 0;

                            if (string.IsNullOrEmpty(dr["dd_sell_value_final"].ToString()) == false)
                                invsObj.dd_sell_value_final = decimal.Parse(dr["dd_sell_value_final"].ToString());
                            else
                                invsObj.dd_sell_value_final = 0;

                            if (string.IsNullOrEmpty(dr["im_sell_price"].ToString()) == false)
                                invsObj.im_sell_price = decimal.Parse(dr["im_sell_price"].ToString());
                            else
                                invsObj.im_sell_price = 0;



                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetEmployeeInfo")]
        public HttpResponseMessage GetEmployeeInfo(string enrollnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            invs035 invsObj = new invs035();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents",
                        new List<SqlParameter>() 
                         { 
                           
                 
                new SqlParameter("@opr", "Y"),
                new SqlParameter("@studentenrollnum", enrollnum),
              
                                           
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            //invsObj.grade_name = dr["gradename"].ToString();
                            //invsObj.section_name = dr["sectionname"].ToString();
                            //invsObj.house_name = dr["housename"].ToString();
                            invsObj.studentname = dr["em_name"].ToString();
                            invsObj.studentenroll = enrollnum;

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);

        }



        [Route("GetReceiptDetails")]
        public HttpResponseMessage GetReceiptDetails(string prov_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStudentName(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetStudentName"));

            ReceiptResponse rr = new ReceiptResponse();
            rr.receipt_lines = new List<ReceiptLine>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr0 = db.ExecuteStoreProcedure("sims.[sale_document_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@doc_prov_no", prov_no),
                 });

                    while (dr0.Read())
                    {
                        rr.enroll_number = dr0["enroll_number"].ToString();
                        rr.student_name = dr0["student_name"].ToString();
                        rr.grade = dr0["grade"].ToString();
                        rr.receipt_date = db.UIDDMMYYYYformat(dr0["receipt_date"].ToString());
                        rr.receipt_number = dr0["receipt_number"].ToString();
                        rr.parent_name = dr0["parent_name"].ToString();
                        rr.receipt_remark = dr0["receipt_remark"].ToString();
                        rr.receipt_grand_total = dr0["receipt_grand_total"].ToString();
                        rr.payment_mode = dr0["payment_mode"].ToString();
                        rr.received_by = dr0["received_by"].ToString();
                        rr.amt_in_words = dr0["amt_in_words"].ToString();

                        
                    }
                    dr0.NextResult();

                    ReceiptLine rl = new ReceiptLine();
                    float total = 0;
                    while (dr0.Read())
                    {
                        rl = new ReceiptLine();
                        rl.line_type = "C";
                        rl.line_sr_no = "";
                        rl.line_particulars = dr0["pc_desc"].ToString();
                        rl.line_remark = "";
                        rl.line_price = "";
                        rl.line_quantity = "";
                        rl.line_amount = "";
                        float sub_total = 0;

                        rr.receipt_lines.Add(rl);

                        string pc_code = dr0["pc_code"].ToString();
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();
                            SqlDataReader dr1 = db2.ExecuteStoreProcedure("sims.[sale_document_receipt_proc]",
                                new List<SqlParameter>() 
                         { 
                          new SqlParameter("@opr", "A"),
                          new SqlParameter("@doc_prov_no", prov_no),
                          new SqlParameter("@pc_code", pc_code)

                 });

                            if (dr1.HasRows)
                            {
                                ReceiptLine rl1 = new ReceiptLine();
                                var i = 1;
                                while (dr1.Read())
                                {
                                    rl1 = new ReceiptLine();
                                    rl1.line_type = "I";
                                    rl1.line_sr_no = i.ToString();


                                    rl1.im_item_code = dr1["im_item_code"].ToString();

                                    rl1.line_particulars = dr1["im_desc"].ToString();
                                    rl1.line_remark = dr1["doc_remark"].ToString();
                                    rl1.line_price = dr1["dd_sell_price"].ToString();
                                    rl1.line_quantity = dr1["dd_qty"].ToString();
                                    rl1.line_amount = dr1["dd_sell_value_final"].ToString();

                                    rr.receipt_lines.Add(rl1);
                                    i = i + 1;
                                    total = total + float.Parse(rl1.line_amount);

                                    sub_total = sub_total + float.Parse(rl1.line_amount);
                                }
                            }

                            dr1.Close();
                            // DB2.Close();
                        }
                        rl = new ReceiptLine();
                        rl.line_type = "S";
                        rl.line_sr_no = "";
                        rl.line_particulars = "Subtotal:";
                        rl.line_remark = "";
                        rl.line_price = "";
                        rl.line_quantity = "";
                        //rl.line_amount = String.Format("{0:0.00}", total);
                        rl.line_amount = String.Format("{0:0.00}", sub_total);

                        rr.receipt_lines.Add(rl);
                    }


                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, rr);

        }


        [Route("getInvoiceDetails")]
        public HttpResponseMessage getInvoiceDetails(string cur, string aca, string grd, string section, string fromdate, string todate,string doc_no)
        {
            string str = string.Empty;
            if (grd.Contains("undefined"))
            {
                grd = null;
            }
            if (section.Contains("undefined"))
            {
                section = null;
            }
            if (doc_no.Contains("undefined"))
            {
                doc_no = null;
            }
            if (fromdate.Contains("undefined"))
            {
                fromdate = null;
            }
            if (todate.Contains("undefined"))
            {
                todate = null;
            }

            List<InvSto> house = new List<InvSto>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_document_details]",
                       new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", "Z"),
                             new SqlParameter("@sims_cur_code", cur),
                             new SqlParameter("@sims_acaedmic_year", aca),
                             new SqlParameter("@sims_grade_code", grd),
                             new SqlParameter("@sims_section_code", section),
                             new SqlParameter("@from_date", db.DBYYYYMMDDformat(fromdate)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
                             new SqlParameter("@doc_no", doc_no)
                       });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["doc_prov_no"].ToString();

                            var v = (from p in house where p.invoice_no == str select p);

                            if (v.Count() == 0)
                            {
                                InvSto ob = new InvSto();
                                ob.sublist1 = new List<InvSto_sub_list>();
                                ob.invoice_no = dr["doc_prov_no"].ToString();
                                ob.enroll_no = dr["sims_student_enroll_number"].ToString();
                                ob.student_name = dr["student_name"].ToString();
                                ob.sell_amt = dr["doc_total_amount"].ToString();
                                ob.creation_user = dr["creation_user"].ToString();
                                ob.doc_prov_date = dr["doc_prov_date"].ToString();
                                ob.doc_reference_prov_no = dr["doc_reference_prov_no"].ToString();
                                
                                ob.sims_icon = "fa fa-plus-circle";


                                InvSto_sub_list sub = new InvSto_sub_list();
                                sub.invoice_no = dr["doc_prov_no"].ToString();
                                sub.im_item_code = dr["im_item_code"].ToString();
                                sub.issued_dd_issue_qty = dr["issued_dd_issue_qty"].ToString();
                                sub.im_desc = dr["im_desc"].ToString();
                                sub.id_cur_qty = dr["id_cur_qty"].ToString();
                                sub.dd_qty = dr["dd_qty"].ToString();
                                sub.doc_issue_date = dr["doc_issue_date"].ToString();
                                sub.enroll_no = dr["sims_student_enroll_number"].ToString();
                                sub.student_name = dr["student_name"].ToString();
                                sub.sell_amt = dr["doc_total_amount"].ToString();
                                sub.creation_user = dr["creation_user"].ToString();
                                sub.doc_prov_date = dr["doc_prov_date"].ToString();
                                sub.doc_dept = dr["dep_code"].ToString();
                                sub.creation_date = dr["creation_date"].ToString();
                                sub.im_inv_no = dr["im_inv_no"].ToString();
                                sub.loc_code = dr["loc_code"].ToString();
                                sub.dd_physical_qty = dr["dd_physical_qty"].ToString();

                                sub.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                                sub.dd_item_vat_amount = dr["dd_item_vat_amount"].ToString();
                                sub.dd_sell_value_final = dr["dd_sell_value_final"].ToString();
                                sub.dd_sell_price_final = dr["dd_sell_price_final"].ToString();
                                sub.dd_sell_price = dr["dd_sell_price"].ToString();
                                sub.dd_sell_price_discounted = dr["dd_sell_price_discounted"].ToString();
                                sub.dd_line_total = dr["dd_line_total"].ToString();
                                sub.dd_line_discount_pct = dr["dd_line_discount_pct"].ToString();
                                sub.dd_line_discount_amount = dr["dd_line_discount_amount"].ToString();
                                sub.doc_other_charge_amount = dr["doc_other_charge_amount"].ToString();
                                sub.doc_discount_pct = dr["doc_discount_pct"].ToString();
                                sub.doc_discount_amount = dr["doc_discount_amount"].ToString();
                                sub.doc_total_amount = dr["doc_total_amount"].ToString();
                                sub.doc_vat_total_amount = dr["doc_vat_total_amount"].ToString();
                                sub.doc_round_value = dr["doc_round_value"].ToString();
                                sub.dd_payment_mode = dr["dd_payment_mode"].ToString();
                                sub.sg_name = dr["sg_name"].ToString();
                                sub.dt_code = dr["dt_code"].ToString();
                                sub.sal_type = dr["sal_type"].ToString();
                                sub.sm_code = dr["sm_code"].ToString();
                                sub.doc_order_ref_no = dr["doc_order_ref_no"].ToString();
                                sub.doc_remark = dr["doc_remark"].ToString();
                                sub.dd_cheque_number = dr["dd_cheque_number"].ToString();
                                sub.dd_cheque_date = dr["dd_cheque_date"].ToString();
                                sub.dd_cheque_bank_code = dr["dd_cheque_bank_code"].ToString();
                                sub.dd_credit_card_code = dr["dd_credit_card_code"].ToString();                                
                                sub.dd_item_vat_per = dr["dd_item_vat_per"].ToString();
                                sub.doc_status = dr["doc_status"].ToString();
                                sub.dd_status = dr["dd_status"].ToString();

                                ob.sublist1.Add(sub);
                                house.Add(ob);
                            }
                            else
                            {
                                InvSto_sub_list sub = new InvSto_sub_list();
                                sub.invoice_no = dr["doc_prov_no"].ToString();
                                sub.im_item_code = dr["im_item_code"].ToString();
                                sub.issued_dd_issue_qty = dr["issued_dd_issue_qty"].ToString();
                                sub.im_desc = dr["im_desc"].ToString();
                                sub.id_cur_qty = dr["id_cur_qty"].ToString();
                                sub.dd_qty = dr["dd_qty"].ToString();
                                sub.doc_issue_date = dr["doc_issue_date"].ToString();
                                sub.enroll_no = dr["sims_student_enroll_number"].ToString();
                                sub.student_name = dr["student_name"].ToString();
                                sub.sell_amt = dr["doc_total_amount"].ToString();
                                sub.creation_user = dr["creation_user"].ToString();
                                sub.doc_prov_date = dr["doc_prov_date"].ToString();
                                sub.doc_dept = dr["dep_code"].ToString();
                                sub.creation_date = dr["creation_date"].ToString();
                                sub.loc_code = dr["loc_code"].ToString();
                                sub.im_inv_no = dr["im_inv_no"].ToString();
                                sub.dd_physical_qty = dr["dd_physical_qty"].ToString();
                                sub.im_item_vat_percentage = dr["im_item_vat_percentage"].ToString();
                                sub.dd_item_vat_amount = dr["dd_item_vat_amount"].ToString();
                                sub.dd_sell_value_final = dr["dd_sell_value_final"].ToString();
                                sub.dd_sell_price_final = dr["dd_sell_price_final"].ToString();
                                sub.dd_sell_price = dr["dd_sell_price"].ToString();
                                sub.dd_sell_price_discounted = dr["dd_sell_price_discounted"].ToString();
                                sub.dd_line_total = dr["dd_line_total"].ToString();
                                sub.dd_line_discount_pct = dr["dd_line_discount_pct"].ToString();
                                sub.dd_line_discount_amount = dr["dd_line_discount_amount"].ToString();
                                sub.doc_other_charge_amount = dr["doc_other_charge_amount"].ToString();
                                sub.doc_discount_pct = dr["doc_discount_pct"].ToString();
                                sub.doc_discount_amount = dr["doc_discount_amount"].ToString();
                                sub.doc_total_amount = dr["doc_total_amount"].ToString();
                                sub.doc_vat_total_amount = dr["doc_vat_total_amount"].ToString();
                                sub.doc_round_value = dr["doc_round_value"].ToString();
                                sub.dd_payment_mode = dr["dd_payment_mode"].ToString();
                                sub.sg_name = dr["sg_name"].ToString();
                                sub.dt_code = dr["dt_code"].ToString();
                                sub.sal_type = dr["sal_type"].ToString();
                                sub.sm_code = dr["sm_code"].ToString();
                                sub.doc_order_ref_no = dr["doc_order_ref_no"].ToString();
                                sub.doc_remark = dr["doc_remark"].ToString();
                                sub.dd_cheque_number = dr["dd_cheque_number"].ToString();
                                sub.dd_cheque_date = dr["dd_cheque_date"].ToString();
                                sub.dd_cheque_bank_code = dr["dd_cheque_bank_code"].ToString();
                                sub.dd_credit_card_code = dr["dd_credit_card_code"].ToString();                                
                                sub.dd_item_vat_per = dr["dd_item_vat_per"].ToString();
                                sub.doc_status = dr["doc_status"].ToString();
                                sub.dd_status = dr["dd_status"].ToString();


                                v.ElementAt(0).sublist1.Add(sub);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


        [Route("Insert_Sale_Documents_Return")]
        public HttpResponseMessage Insert_Sale_Documents_Return(invs035 invsObj)
        {
            string prov_no = string.Empty;
            bool vat_flag = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'M'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));
                    para.Add(new SqlParameter("@doc_vat_total_amount", invsObj.doc_vat_total_amount));
                    para.Add(new SqlParameter("@doc_round_value", invsObj.doc_round_value));
                    para.Add(new SqlParameter("@old_doc_prov_no", invsObj.old_doc_prov_no));
                    para.Add(new SqlParameter("@doc_reference_prov_no", invsObj.doc_reference_prov_no));
                    para.Add(new SqlParameter("@save_finalize_flag", invsObj.save_finalize_flag));

                    SqlDataReader dr = db.ExecuteStoreProcedure("sale_documents", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, prov_no);


        }

        [Route("Insert_Sale_Documents_Details1_Return")]
        public HttpResponseMessage Insert_Sale_Documents_Details1_Return(List<invs035> invslst)
        {
            bool result = false;

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'M'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));

                        para.Add(new SqlParameter("@dd_item_vat_per", invsObj.dd_item_vat_per));
                        para.Add(new SqlParameter("@dd_item_vat_amount", invsObj.dd_item_vat_amount));
                        para.Add(new SqlParameter("@dd_cheque_date", db.DBYYYYMMDDformat(invsObj.dd_cheque_date)));
                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));
                       // para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@doc_prov_no_have_previous", invsObj.doc_prov_no_have_previous));

                        SqlDataReader dr = db.ExecuteStoreProcedure("sale_document_details", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }
                    
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
            
        }


        [Route("Get_Subledger_Master")]
        public HttpResponseMessage Get_Subledger_Master(string comp_code,string slma_ldgrctl_code)
        {
          
            List<Finn016> mod_list = new List<Finn016>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents]",
                     new List<SqlParameter>()
                     {
                           new SqlParameter("@opr", "1"),
                           new SqlParameter("@comp_code", comp_code),
                           new SqlParameter("@slma_ldgrctl_code", slma_ldgrctl_code)
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn016 finnobj = new Finn016();
                            finnobj.slma_comp_name = dr["slma_comp_name"].ToString();
                            finnobj.slma_comp_code = dr["slma_comp_code"].ToString();
                            finnobj.slma_ldgrctl_year = dr["slma_ldgrctl_year"].ToString();
                            finnobj.slma_ldgrctl_code = dr["slma_ldgrctl_code"].ToString();
                            finnobj.slma_ldgrctl_name = dr["slma_ldgrctl_code"].ToString() + "-" + dr["slma_ldgrctl_name"].ToString();
                            finnobj.slma_acno = dr["slma_acno"].ToString();
                            finnobj.slma_addr_id = dr["slma_addr_id"].ToString();
                            finnobj.slma_addr_type = dr["slma_addr_type"].ToString();
                            finnobj.slma_addr_type_name = dr["slma_addr_type_name"].ToString();
                            if (dr["slma_status"].ToString().Equals("A"))
                                finnobj.slma_status = true;
                            else
                                finnobj.slma_status = false;
                            finnobj.slma_cntrl_class = dr["slma_cntrl_class"].ToString();
                            finnobj.slma_cntrl_class_name = dr["slma_cntrl_class_name"].ToString();
                            finnobj.slma_pty_type = dr["slma_pty_type"].ToString();
                            finnobj.slma_pty_type_name = dr["slma_pty_type_name"].ToString();
                            finnobj.slma_ctry_code = dr["slma_ctry_code"].ToString();
                            finnobj.slma_ctry_name = dr["slma_ctry_name"].ToString();
                            finnobj.slma_xref_ldgr_code = dr["slma_xref_ldgr_code"].ToString();
                            finnobj.slma_xref_ldgr_name = dr["slma_xref_ldgr_code"].ToString() + "-" + dr["slma_xref_ldgr_name"].ToString();
                            finnobj.slma_xref_acno = dr["slma_xref_acno_name"].ToString() + "-" + dr["slma_xref_acno"].ToString();
                            if (dr["slma_fc_flag"].ToString().Equals("Y"))
                                finnobj.slma_fc_flag = true;
                            else
                                finnobj.slma_fc_flag = false;
                            if (dr["slma_qty_flag"].ToString().Equals("Y"))
                                finnobj.slma_qty_flag = true;
                            else
                                finnobj.slma_qty_flag = false;
                            if (dr["slma_remove_tran"].ToString().Equals("Y"))
                                finnobj.slma_remove_tran = true;
                            else
                                finnobj.slma_remove_tran = false;
                            if (string.IsNullOrEmpty(dr["slma_yob_amt"].ToString()) == false)
                                finnobj.slma_yob_amt = Decimal.Parse(dr["slma_yob_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_outstg_dr_amt"].ToString()) == false)
                                finnobj.slma_outstg_dr_amt = Decimal.Parse(dr["slma_outstg_dr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_outstg_cr_amt"].ToString()) == false)
                                finnobj.slma_outstg_cr_amt = Decimal.Parse(dr["slma_outstg_cr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_for_curcy_bal_amt"].ToString()) == false)
                                finnobj.slma_for_curcy_bal_amt = Decimal.Parse(dr["slma_for_curcy_bal_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_tdy_dr_amt"].ToString()) == false)
                                finnobj.slma_tdy_dr_amt = Decimal.Parse(dr["slma_tdy_dr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_tdy_cr_amt"].ToString()) == false)
                                finnobj.slma_tdy_cr_amt = Decimal.Parse(dr["slma_tdy_cr_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_cr_limit"].ToString()) == false)
                                finnobj.slma_cr_limit = Decimal.Parse(dr["slma_cr_limit"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_loan_inst_amt"].ToString()) == false)
                                finnobj.slma_loan_inst_amt = Decimal.Parse(dr["slma_loan_inst_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_orig_loan_amt"].ToString()) == false)
                                finnobj.slma_orig_loan_amt = Decimal.Parse(dr["slma_orig_loan_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_ovrid_amt"].ToString()) == false)
                                finnobj.slma_ovrid_amt = Decimal.Parse(dr["slma_ovrid_amt"].ToString());

                            if (string.IsNullOrEmpty(dr["slma_bank_clos_bal_amt"].ToString()) == false)
                                finnobj.slma_bank_clos_bal_amt = Decimal.Parse(dr["slma_bank_clos_bal_amt"].ToString());

                            finnobj.slma_curcy_code = dr["slma_curcy_code"].ToString();
                            finnobj.slma_curcy_name = dr["slma_curcy_name"].ToString();
                            // finnobj.slma_cash_crd_ind = Decimal.Parse(dr["slma_cash_crd_ind"].ToString());
                            if (dr["slma_cash_crd_ind"].ToString().Equals("1"))
                            {
                                finnobj.slma_cash_crd_ind = true;
                            }
                            else
                            {
                                finnobj.slma_cash_crd_ind = false;
                            }
                            if (dr["slma_stop_cr_ind"].ToString().Equals("1"))
                            {
                                finnobj.slma_stop_cr_ind = true;
                            }
                            else
                            {
                                finnobj.slma_stop_cr_ind = false;
                            }
                            finnobj.slma_pty_bank_acno = dr["slma_pty_bank_acno"].ToString();
                            if (!string.IsNullOrEmpty(dr["slma_bank_stmt_date"].ToString()))
                                finnobj.slma_bank_stmt_date = DateTime.Parse(dr["slma_bank_stmt_date"].ToString()).ToShortDateString();
                            else
                                finnobj.slma_bank_stmt_date = "";
                            if (!string.IsNullOrEmpty(dr["slma_pmt_start_date"].ToString()))
                                finnobj.slma_pmt_start_date = dr["slma_pmt_start_date"].ToString();
                            else
                                finnobj.slma_pmt_start_date = "";
                            if (!string.IsNullOrEmpty(dr["slma_doc_date"].ToString()))
                                finnobj.slma_doc_date = dr["slma_doc_date"].ToString();
                            else
                                finnobj.slma_doc_date = "";
                            finnobj.slma_pty_bank_id = dr["slma_pty_bank_id"].ToString();
                            finnobj.slma_pty_bank_name = dr["slma_pty_bank_id"].ToString() + "-" + dr["slma_pty_bank_name"].ToString();
                            finnobj.slma_loan_inst_type = dr["slma_loan_inst_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_yob_amt"].ToString()))
                                finnobj.slma_credit_prd = int.Parse(dr["slma_credit_prd"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_ovrid_prd"].ToString()))
                                finnobj.slma_ovrid_prd = int.Parse(dr["slma_ovrid_prd"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_stmt_prd"].ToString()))
                                finnobj.slma_stmt_prd = int.Parse(dr["slma_stmt_prd"].ToString());
                            finnobj.slma_sched_code = dr["slma_sched_code"].ToString();
                            finnobj.slma_sched_name = dr["slma_sched_name"].ToString();
                            finnobj.slma_qty_code = dr["slma_qty_code"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_last_pmt_date"].ToString()))
                                finnobj.slma_last_pmt_date = dr["slma_last_pmt_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_last_pmt_amt"].ToString()))
                                finnobj.slma_last_pmt_amt = Decimal.Parse(dr["slma_last_pmt_amt"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_int_rate"].ToString()))
                                finnobj.slma_int_rate = Decimal.Parse(dr["slma_int_rate"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_int_prd"].ToString()))
                                finnobj.slma_int_prd = int.Parse(dr["slma_int_prd"].ToString());
                            finnobj.slma_term_name = dr["slma_term_name"].ToString();

                            if (!string.IsNullOrEmpty(dr["slma_outstg_dr_amt_orig"].ToString()))
                                finnobj.slma_outstg_dr_amt_orig = Decimal.Parse(dr["slma_outstg_dr_amt_orig"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_outstg_cr_amt_orig"].ToString()))
                                finnobj.slma_outstg_cr_amt_orig = Decimal.Parse(dr["slma_outstg_cr_amt_orig"].ToString());

                            if (!string.IsNullOrEmpty(dr["slma_yob_amt_orig"].ToString()))
                                finnobj.slma_yob_amt_orig = Decimal.Parse(dr["slma_yob_amt_orig"].ToString());
                            if (!string.IsNullOrEmpty(dr["slma_reval_date"].ToString()))
                                finnobj.slma_reval_date = dr["slma_reval_date"].ToString();
                            else
                                finnobj.slma_reval_date = "";
                            finnobj.slma_cntrl_class_org = dr["slma_cntrl_class_org"].ToString();
                            finnobj.slma_credit_remark = dr["slma_credit_remark"].ToString();

                            finnobj.coad_dept_no = dr["coad_dept_no"].ToString();
                            finnobj.coad_xref_addr_id = dr["coad_xref_addr_id"].ToString();
                            finnobj.coad_xref_addr_name = dr["coad_xref_addr_name"].ToString();
                            finnobj.coad_pty_full_name = dr["coad_pty_full_name"].ToString();
                            finnobj.coad_pty_short_name = dr["coad_pty_short_name"].ToString();
                            finnobj.coad_alt_key = dr["coad_alt_key"].ToString();
                            finnobj.coad_pty_arab_name = dr["coad_pty_arab_name"].ToString();
                            finnobj.coad_line_1 = dr["coad_line_1"].ToString();
                            finnobj.coad_line_2 = dr["coad_line_2"].ToString();
                            finnobj.coad_po_box = dr["coad_po_box"].ToString();
                            finnobj.coad_tel_no = dr["coad_tel_no"].ToString();
                            finnobj.coad_fax = dr["coad_fax"].ToString();
                            finnobj.coad_telex = dr["coad_telex"].ToString();
                            finnobj.coad_region_code = dr["coad_region_code"].ToString();
                            finnobj.coad_region_name = dr["coad_region_name"].ToString();
                            finnobj.coad_class_level1 = dr["coad_class_level1"].ToString();
                            finnobj.coad_class_level2 = dr["coad_class_level2"].ToString();
                            finnobj.coad_class_level3 = dr["coad_class_level3"].ToString();
                            finnobj.coad_class_level4 = dr["coad_class_level4"].ToString();
                            finnobj.coad_class_level5 = dr["coad_class_level5"].ToString();
                            finnobj.coad_sms_no = dr["coad_sms_no"].ToString();
                            finnobj.coad_email = dr["coad_email"].ToString();
                            mod_list.Add(finnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }



        [Route("GetSalesType")]
        public HttpResponseMessage GetSalesType()
        {
         
            List<invs035> list = new List<invs035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents_leams]",
                    new List<SqlParameter>()
                    {
                         new SqlParameter("@opr",'N')                                           
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs035 invsObj = new invs035();
                            invsObj.invs_appl_parameter = dr["invs_appl_parameter"].ToString();
                            invsObj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                            list.Add(invsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {              
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Insert_Sale_Documents_VAT_Leams")]
        public HttpResponseMessage Insert_Sale_Documents_VAT_Leams(invs035 invsObj)
        {
            string prov_no = string.Empty;
            bool vat_flag = false;

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    para.Add(new SqlParameter("@opr", 'I'));
                    para.Add(new SqlParameter("@sub_opr", ""));
                    para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                        para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                    else
                        para.Add(new SqlParameter("@doc_prov_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_prov_date))
                        para.Add(new SqlParameter("@doc_prov_date", db.DBYYYYMMDDformat(invsObj.doc_prov_date)));
                    if (invsObj.doc_no > 0)
                        para.Add(new SqlParameter("@doc_no", invsObj.doc_no));
                    else
                        para.Add(new SqlParameter("@doc_no", 0));
                    if (!string.IsNullOrEmpty(invsObj.doc_date))
                        para.Add(new SqlParameter("@doc_date", db.DBYYYYMMDDformat(invsObj.doc_date)));
                    para.Add(new SqlParameter("@dt_code", invsObj.dt_code));
                    para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                    para.Add(new SqlParameter("@sm_code", invsObj.sm_code));
                    para.Add(new SqlParameter("@up_name", invsObj.up_name));
                    para.Add(new SqlParameter("@sal_type", invsObj.sal_type));
                    para.Add(new SqlParameter("@doc_total_amount", invsObj.doc_total_amount));
                    para.Add(new SqlParameter("@doc_status", invsObj.doc_status));
                    para.Add(new SqlParameter("@cus_account_no", invsObj.cus_account_no));
                    para.Add(new SqlParameter("@ic_account_no", invsObj.ic_account_no));
                    para.Add(new SqlParameter("@doc_special_name", invsObj.doc_special_name));
                    para.Add(new SqlParameter("@dep_code_caused_by", invsObj.dep_code_caused_by));
                    para.Add(new SqlParameter("@doc_order_ref_no", invsObj.doc_order_ref_no));
                    para.Add(new SqlParameter("@doc_other_charge_desc", invsObj.doc_other_charge_desc));
                    para.Add(new SqlParameter("@doc_other_charge_amount", invsObj.doc_other_charge_amount));
                    para.Add(new SqlParameter("@doc_discount_pct", invsObj.doc_discount_pct));
                    para.Add(new SqlParameter("@doc_discount_amount", invsObj.doc_discount_amount));
                    para.Add(new SqlParameter("@doc_delivery_remarks", invsObj.doc_delivery_remarks));
                    para.Add(new SqlParameter("@doc_validity_remarks", invsObj.doc_validity_remarks));
                    para.Add(new SqlParameter("@ow_id", invsObj.ow_id));
                    para.Add(new SqlParameter("@doc_jobcard_dept", invsObj.doc_jobcard_dept));
                    para.Add(new SqlParameter("@doc_jobcard_loc", invsObj.doc_jobcard_loc));
                    para.Add(new SqlParameter("@doc_jobcard_no", invsObj.doc_jobcard_no));
                    para.Add(new SqlParameter("@sd_icc_code", invsObj.sd_icc_code));
                    para.Add(new SqlParameter("@doc_narration", invsObj.doc_narration));
                    para.Add(new SqlParameter("@sec_code", invsObj.sec_code));
                    if (!string.IsNullOrEmpty(invsObj.creation_date))
                        para.Add(new SqlParameter("@creation_date", db.DBYYYYMMDDformat(invsObj.creation_date)));
                    para.Add(new SqlParameter("@creation_user", invsObj.creation_user));
                    para.Add(new SqlParameter("@creation_term", invsObj.creation_term));
                    if (!string.IsNullOrEmpty(invsObj.modification_date))
                        para.Add(new SqlParameter("@modification_date", db.DBYYYYMMDDformat(invsObj.modification_date)));
                    para.Add(new SqlParameter("@modification_user", invsObj.modification_user));
                    para.Add(new SqlParameter("@modification_term", invsObj.modification_term));
                    para.Add(new SqlParameter("@doc_cheque_no", invsObj.doc_cheque_no));
                    if (!string.IsNullOrEmpty(invsObj.doc_cheque_date))
                        para.Add(new SqlParameter("@doc_cheque_date", db.DBYYYYMMDDformat(invsObj.doc_cheque_date)));
                    para.Add(new SqlParameter("@doc_cheque_bank", invsObj.doc_cheque_bank));
                    para.Add(new SqlParameter("@doc_vat_total_amount", invsObj.doc_vat_total_amount));
                    para.Add(new SqlParameter("@doc_round_value", invsObj.doc_round_value));

                    para.Add(new SqlParameter("@doc_cash_amount", invsObj.doc_cash_amount));
                    para.Add(new SqlParameter("@doc_check_amount", invsObj.doc_check_amount));
                    para.Add(new SqlParameter("@doc_credit_card_amount", invsObj.doc_credit_card_amount));
                    para.Add(new SqlParameter("@doc_dd_amount", invsObj.doc_dd_amount));
                    para.Add(new SqlParameter("@doc_bank_transfer_amount", invsObj.doc_bank_transfer_amount));
                    para.Add(new SqlParameter("@doc_online_payment_amount", invsObj.doc_card_payment_amount));
                    para.Add(new SqlParameter("@payment_type", invsObj.payment_type));

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents_leams]", para);
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (!string.IsNullOrEmpty(dr["prov_no"].ToString()))
                                prov_no = dr["prov_no"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, prov_no);
            }

            return Request.CreateResponse(HttpStatusCode.OK, prov_no);            
        }



        [Route("Insert_Sale_Documents_Details1_VAT_Leams")]
        public HttpResponseMessage Insert_Sale_Documents_Details1_VAT_Leams(List<invs035> invslst)
        {
            bool result = false;
            string msg = "";

            foreach (invs035 invsObj in invslst)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", 'I'));
                        para.Add(new SqlParameter("@sub_opr", ""));
                        para.Add(new SqlParameter("@dep_code", invsObj.dep_code));
                        if (!string.IsNullOrEmpty(invsObj.doc_prov_no))
                            para.Add(new SqlParameter("@doc_prov_no", invsObj.doc_prov_no));
                        else
                            para.Add(new SqlParameter("@doc_prov_no", 0));
                        para.Add(new SqlParameter("@dd_line_no", invsObj.dd_line_no));
                        //if (invsObj.im_inv_no > 0)
                        para.Add(new SqlParameter("@im_inv_no", invsObj.im_inv_no));
                        if (string.IsNullOrEmpty(invsObj.loc_code))
                            invsObj.loc_code = "01";
                        para.Add(new SqlParameter("@loc_code", invsObj.loc_code));
                        para.Add(new SqlParameter("@dd_sell_price", invsObj.dd_sell_price));
                        para.Add(new SqlParameter("@dd_sell_price_discounted", invsObj.dd_sell_price_discounted));
                        para.Add(new SqlParameter("@dd_sell_price_final", invsObj.dd_sell_price_final));
                        para.Add(new SqlParameter("@dd_sell_value_final", invsObj.dd_sell_value_final));
                        para.Add(new SqlParameter("@dd_sell_cost_total", invsObj.dd_sell_cost_total));
                        para.Add(new SqlParameter("@dd_qty", invsObj.dd_qty));
                        para.Add(new SqlParameter("@dd_outstanding_qty", invsObj.dd_outstanding_qty));
                        para.Add(new SqlParameter("@dd_line_no_have_previous", invsObj.dd_line_no_have_previous));
                        para.Add(new SqlParameter("@dep_code_have_previous", invsObj.dep_code_have_previous));
                        para.Add(new SqlParameter("@dd_line_discount_pct", invsObj.dd_line_discount_pct));
                        para.Add(new SqlParameter("@dd_line_discount_amount", invsObj.dd_line_discount_amount));
                        para.Add(new SqlParameter("@dd_core_exchange", invsObj.dd_core_exchange));
                        para.Add(new SqlParameter("@dd_discount_type", invsObj.dd_discount_type));
                        para.Add(new SqlParameter("@dd_status", invsObj.dd_status));
                        para.Add(new SqlParameter("@test", invsObj.test));
                        para.Add(new SqlParameter("@dd_line_total", invsObj.dd_line_total));
                        para.Add(new SqlParameter("@dd_physical_qty", invsObj.dd_physical_qty));
                        para.Add(new SqlParameter("@dd_remarks", invsObj.dd_remarks));
                        para.Add(new SqlParameter("@doc_type_have_previous", invsObj.doc_type_have_previous));
                        para.Add(new SqlParameter("@doc_no_have_previous", invsObj.doc_no_have_previous));
                        para.Add(new SqlParameter("@dr_code", invsObj.dr_code));
                        para.Add(new SqlParameter("@dd_payment_mode", invsObj.dd_payment_mode));
                        para.Add(new SqlParameter("@dd_cheque_number", invsObj.dd_cheque_number));
                        para.Add(new SqlParameter("@doc_remark", invsObj.doc_remark));

                        para.Add(new SqlParameter("@dd_item_vat_per", invsObj.dd_item_vat_per));
                        para.Add(new SqlParameter("@dd_item_vat_amount", invsObj.dd_item_vat_amount));
                        para.Add(new SqlParameter("@dd_cheque_date", db.DBYYYYMMDDformat(invsObj.dd_cheque_date)));
                        para.Add(new SqlParameter("@dd_cheque_bank_code", invsObj.dd_cheque_bank_code));

                        para.Add(new SqlParameter("@dd_demand_draft_number", invsObj.dd_demand_draft_number));
                        para.Add(new SqlParameter("@dd_demand_draft_date", db.DBYYYYMMDDformat(invsObj.dd_demand_draft_date)));
                        para.Add(new SqlParameter("@dd_demand_draft_bank_code", invsObj.dd_demand_draft_bank_code));

                        para.Add(new SqlParameter("@dd_credit_card_number", invsObj.dd_credit_card_number));
                        para.Add(new SqlParameter("@dd_credit_card_date", db.DBYYYYMMDDformat(invsObj.dd_credit_card_date)));
                        para.Add(new SqlParameter("@dd_credit_card_bank_code", invsObj.dd_credit_card_bank_code));

                        para.Add(new SqlParameter("@dd_bank_transfer_trans_number", invsObj.dd_bank_transfer_trans_number));
                        para.Add(new SqlParameter("@dd_bank_transfer_trans_date", db.DBYYYYMMDDformat(invsObj.dd_bank_transfer_trans_date)));
                        para.Add(new SqlParameter("@dd_bank_transfer_trans_bank_code", invsObj.dd_bank_transfer_trans_bank_code));

                        para.Add(new SqlParameter("@dd_online_payemnt_trans_number", invsObj.dd_online_payemnt_trans_number));

                        para.Add(new SqlParameter("@im_revenue_acno", invsObj.im_revenue_acno));
                        para.Add(new SqlParameter("@im_cost_acno", invsObj.im_cost_acno));
                        para.Add(new SqlParameter("@im_vat_sale_profile", invsObj.im_vat_sale_profile));
                        //try
                        //{
                        //    string dt_chq = DateTime.Parse(invsObj.dd_cheque_date).Year + "-" + DateTime.Parse(invsObj.dd_cheque_date).Month + "-" + DateTime.Parse(invsObj.dd_cheque_date).Day;
                        //    para.Add(new SqlParameter("@dd_cheque_date", DateTime.Parse(dt_chq)));
                        //}
                        //catch (Exception dee)
                        //{ para.Add(new SqlParameter("@dd_cheque_date", DateTime.Now)); }



                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_document_details_vat_leams]", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        try
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    msg = dr["msg"].ToString();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            msg = "";
                        }
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }

            if (string.IsNullOrEmpty(msg))
                return Request.CreateResponse(HttpStatusCode.OK, result);
            else
                return Request.CreateResponse(HttpStatusCode.OK, msg);

        }

        [Route("GetGlAccounts")]
        public HttpResponseMessage GetGlAccounts()
        {
            List<Invs063> cat_list = new List<Invs063>();            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents_leams]",
                    new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", 'O')                            
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.gl_item_no = dr["glma_acct_code"].ToString();
                            simsobj.gl_accname = dr["gldesc"].ToString();
                            cat_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {               
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        [Route("GetVatProfile")]
        public HttpResponseMessage GetVatProfile()
        {
            List<Invs063> cat_list = new List<Invs063>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents_leams]",
                    new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", 'E')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs063 simsobj = new Invs063();
                            simsobj.vat_profile_no = dr["vat_code"].ToString();
                            simsobj.vat_profile_desc = dr["vat_desc"].ToString();
                            simsobj.vat_profile_type = dr["vat_included_in_price"].ToString();
                            simsobj.vat_profile_percentage = dr["vat_per_value"].ToString();
                                                        
                            cat_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, cat_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        [Route("GetSublegerInfo")]
        public HttpResponseMessage GetSublegerInfo(string slma_ldgrctl_code, string slma_acno)
        {            
            invs035 invsObj = new invs035();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sale_documents_leams]",
                     new List<SqlParameter>()
                     {                          
                        new SqlParameter("@opr", "F"),
                        new SqlParameter("@slma_ldgrctl_code", slma_ldgrctl_code),
                        new SqlParameter("@slma_acno", slma_acno)
                    });
                    if (dr.HasRows)
                    {
                        if(dr.Read())
                        {   
                            invsObj.studentname = dr["coad_pty_full_name"].ToString();
                            invsObj.studentenroll = dr["slma_acno"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,x.Message);
            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, invsObj);
        }


        public class ReceiptResponse
        {
            public List<ReceiptLine> receipt_lines { get; set; }


            public string enroll_number { get; set; }
            public string student_name { get; set; }
            public string grade { get; set; }
            public string receipt_date { get; set; }
            public string receipt_number { get; set; }
            public string parent_name { get; set; }
            public string receipt_remark { get; set; }
            public string receipt_grand_total { get; set; }
            public string payment_mode { get; set; }
            public string received_by { get; set; }


            public string amt_in_words { get; set; }
        }

        public class ReceiptLine
        {
            public string line_type { get; set; }
            public string line_sr_no { get; set; }
            public string line_particulars { get; set; }
            public string line_remark { get; set; }
            public string line_price { get; set; }
            public string line_quantity { get; set; }
            public string line_amount { get; set; }



            public string im_item_code { get; set; }
        }



    }
}