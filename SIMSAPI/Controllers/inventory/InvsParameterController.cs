﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/InvsParameter")]
    [BasicAuthentication]
    public class InvsParameterController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getInvsParameter")]
        public HttpResponseMessage getInvsParameter()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getInvsParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getInvsParameter"));

            List<Inv124> goaltarget_list = new List<Inv124>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv124 simsobj = new Inv124();
                            simsobj.invs_comp_code = dr["invs_comp_code"].ToString();

                            simsobj.comp_name = dr["comp_name"].ToString();

                            simsobj.invs_appl_code = dr["invs_appl_code"].ToString();

                            simsobj.invs_appl_form_field = dr["invs_appl_form_field"].ToString();

                            simsobj.invs_appl_parameter = dr["invs_appl_parameter"].ToString();

                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                            simsobj.invs_appl_form_field_value2 = dr["invs_appl_form_field_value2"].ToString();
                            simsobj.invs_appl_form_field_value3 = dr["invs_appl_form_field_value3"].ToString();
                            simsobj.invs_appl_form_field_value4 = dr["invs_appl_form_field_value4"].ToString();


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getInvsParameter")]
        public HttpResponseMessage getInvsParameter(string comn_appl_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getInvsParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getInvsParameter"));

            List<Inv124> goaltarget_list = new List<Inv124>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           new SqlParameter("@commonapplication",comn_appl_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv124 simsobj = new Inv124();
                            simsobj.invs_comp_code = dr["invs_comp_code"].ToString();

                            simsobj.comp_name = dr["comp_name"].ToString();

                            simsobj.invs_appl_code = dr["invs_appl_code"].ToString();

                            simsobj.invs_appl_form_field = dr["invs_appl_form_field"].ToString();

                            simsobj.invs_appl_parameter = dr["invs_appl_parameter"].ToString();

                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                            simsobj.invs_appl_form_field_value2 = dr["invs_appl_form_field_value2"].ToString();
                            simsobj.invs_appl_form_field_value3 = dr["invs_appl_form_field_value3"].ToString();
                            simsobj.invs_appl_form_field_value4 = dr["invs_appl_form_field_value4"].ToString();


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //GetGLAccountNumber
        [Route("GetCompanyName")]
        public HttpResponseMessage GetDepartment()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartment()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Inv124> doc_list = new List<Inv124>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv124 simsobj = new Inv124();
                            simsobj.com_name = dr["com_name"].ToString();
                            simsobj.com_code = dr["com_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetGLAccountNumber
        [Route("GetApplicationCode")]
        public HttpResponseMessage GetApplicationCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetApplicationCode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Inv124> doc_list = new List<Inv124>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv124 simsobj = new Inv124();
                            simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDInvsParamenter")]
        public HttpResponseMessage CUDInvsParamenter(List<Inv124> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv124 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@invs_comp_code", simsobj.invs_comp_code),
                                new SqlParameter("@invs_appl_code",simsobj.invs_appl_code),
                                new SqlParameter("@invs_appl_form_field", simsobj.invs_appl_form_field),
                                new SqlParameter("@invs_appl_parameter", simsobj.invs_appl_parameter),
                                
                                new SqlParameter("@invs_appl_form_field_value1", simsobj.invs_appl_form_field_value1),
                                new SqlParameter("@invs_appl_form_field_value2", simsobj.invs_appl_form_field_value2),
                                new SqlParameter("@invs_appl_form_field_value3", simsobj.invs_appl_form_field_value3),
                                new SqlParameter("@invs_appl_form_field_value4", simsobj.invs_appl_form_field_value4),


                                new SqlParameter("@invs_comp_code_old", simsobj.invs_comp_code_old),
                                new SqlParameter("@invs_appl_code_old",simsobj.invs_appl_code_old),
                                new SqlParameter("@invs_appl_form_field_old", simsobj.invs_appl_form_field_old),
                                new SqlParameter("@invs_appl_parameter_old", simsobj.invs_appl_parameter_old),

                                new SqlParameter("@invs_appl_form_field_value1_old", simsobj.invs_appl_form_field_value1_old),
                                new SqlParameter("@invs_appl_form_field_value2_old", simsobj.invs_appl_form_field_value2_old),
                                new SqlParameter("@invs_appl_form_field_value3_old", simsobj.invs_appl_form_field_value3_old),
                                new SqlParameter("@invs_appl_form_field_value4_old", simsobj.invs_appl_form_field_value4_old),



                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










