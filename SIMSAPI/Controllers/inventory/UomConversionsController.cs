﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.inventoryClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/UomConversions")]
    [BasicAuthentication]
    public class UomConversionsController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Select
        [Route("getAllUomConversions")]
        public HttpResponseMessage getAllUomConversions()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllUomConversions(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllUomConversions"));

            List<Invs002> goaltarget_list = new List<Invs002>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[uom_conversions_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs002 simsobj = new Invs002();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            //simobj.uom_name1=dr[""uom_name""].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_code_belongs = dr["uom_code_belongs"].ToString();
                            simsobj.uc_factor =Convert.ToDecimal(dr["uc_factor"].ToString());
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDUomConversions")]
        public HttpResponseMessage CUDUomConversions(List<Invs002> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs002 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[uom_conversions_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@uom_name",simsobj.uom_name),
                                new SqlParameter("@uom_code_belongs", simsobj.uom_code_belongs),
                               new SqlParameter("@uc_factor", simsobj.uc_factor),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

       
       
    }
}