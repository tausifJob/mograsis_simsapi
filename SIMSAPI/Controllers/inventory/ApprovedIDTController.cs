﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.inventoryClass;

using System.Linq;
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/ApproveIDT")]
    public class ApprovedIDTController:ApiController
    {
        [Route("CUDSale_Documents")]
        public HttpResponseMessage CUDSale_Documents(List<ApprovedIDT> invsObj)
        {

            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> para = new List<SqlParameter>();

                    foreach (ApprovedIDT data in invsObj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sale_document_details_proc]",
                           new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "W"),
                new SqlParameter("@dd_status", "3"),
                new SqlParameter("@doc_prov_no", data.doc_prov_no),
                new SqlParameter("@dd_qty", data.dd_qty),
                new SqlParameter("@loc_code", data.loc_code),
                new SqlParameter("@dep_code", data.dep_code),
                new SqlParameter("@dep_code1", data.cus_account_no),
                new SqlParameter("@im_inv_no", data.im_inv_no),
                });
                    if(dr.RecordsAffected>0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                    }
                }
            }
            
            catch (Exception e)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);


        }

       
        [Route("GetAllItemsforApprove")]
        public HttpResponseMessage GetAllItemsforApprove()
        {
            List<ApprovedIDT> mod_list = new List<ApprovedIDT>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.item_master_proc",
                        new List<SqlParameter>() 
                         { 
                      new SqlParameter("@opr", "H"),
                                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ApprovedIDTList invsObj = new ApprovedIDTList();

                            ApprovedIDT invsObj1 = new ApprovedIDT();


                            invsObj1.AppIDTList = new List<ApprovedIDTList>();

                            string str = dr["doc_prov_no"].ToString();

                            var v = (from p in mod_list where p.doc_prov_no == str select p);

                            invsObj1.dep_code = dr["dep_code"].ToString();
                            invsObj1.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj1.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj1.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj1.doc_status = dr["doc_status"].ToString();
                            invsObj1.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj1.loc_name = dr["loc_name"].ToString();
                            invsObj1.em_first_name = dr["em_first_name"].ToString();
                            invsObj1.up_name = dr["up_name"].ToString();
                            invsObj1.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj1.im_item_code = dr["im_item_code"].ToString();
                            invsObj1.im_desc = dr["im_desc"].ToString();
                            invsObj1.from_department = dr["from_department"].ToString();
                            invsObj1.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj1.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj1.sm_code = dr["sm_code"].ToString();
                            invsObj1.sm_name = dr["sm_name"].ToString();
                            invsObj1.to_department = dr["to_department"].ToString();
                            invsObj1.doc_discount_amount = dr["doc_discount_amount"].ToString();
                            invsObj1.dd_sell_price = dr["dd_sell_price"].ToString();
                            invsObj1.dd_sell_price_final = dr["doc_total_final_amount"].ToString();
                            invsObj1.dd_remarks = dr["doc_delivery_remarks"].ToString();

                            invsObj.dep_code = dr["dep_code"].ToString();
                            invsObj.doc_prov_no = dr["doc_prov_no"].ToString();
                            invsObj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            invsObj.doc_total_amount = dr["doc_total_amount"].ToString();
                            invsObj.doc_status = dr["doc_status"].ToString();
                            invsObj.loc_code = dr["loc_code"].ToString();
                            invsObj.dd_sell_value_final = dr["dd_sell_value_final"].ToString();
                            invsObj.dd_qty = decimal.Parse(dr["dd_qty"].ToString());
                            invsObj.dd_line_discount_amount = dr["dd_line_discount_amount"].ToString();
                            invsObj.im_inv_no = dr["im_inv_no"].ToString();
                            invsObj.im_item_code = dr["im_item_code"].ToString();
                            invsObj.im_desc = dr["im_desc"].ToString();
                            invsObj.cus_account_no = dr["cus_account_no"].ToString();
                            invsObj.ic_account_no = dr["ic_account_no"].ToString();
                            invsObj.sm_code = dr["sm_code"].ToString();

                            if (v.Count() == 0)
                            {
                                invsObj1.AppIDTList.Add(invsObj);
                                mod_list.Add(invsObj1);
                               
                            }

                            else
                            {
                                v.ElementAt(0).AppIDTList.Add(invsObj);
                            }
                           
                        }
                    }
                }
            }
            catch (Exception x)
            {

                //   return StudentName;

            }
            // return StudentName;
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

    }
}