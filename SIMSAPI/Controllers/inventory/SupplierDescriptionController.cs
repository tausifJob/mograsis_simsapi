﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using log4net;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/SupplierDescription")]
    [BasicAuthentication]
    public class SupplierDescriptionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        //select
        [Route("getAllSupplierDescription")]
        public HttpResponseMessage getAllSupplierDescription()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSupplierDescription(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllSupplierDescription"));

            List<Invs010> goaltarget_list = new List<Invs010>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_description_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs010 obj = new Invs010();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.sdt_type = dr["sdt_type"].ToString();
                            obj.sdt_desc = dr["sdt_desc"].ToString();
                            obj.sd_desc = dr["sd_desc"].ToString();
                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDSupplierDescription")]
        public HttpResponseMessage CUDSupplierDescription(List<Invs010> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs010 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_supplier_description_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@sdt_type",obj.sdt_type),
                                new SqlParameter("@sup_code", obj.sup_code),
                                new SqlParameter("@sd_desc", obj.sd_desc),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getSupplierCode")]
        public HttpResponseMessage getSupplierCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSupplierCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSupplierCode"));

            List<Invs010> goaltarget_list = new List<Invs010>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_supplier_description_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'A'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs010 obj = new Invs010();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                           
                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

    }
}