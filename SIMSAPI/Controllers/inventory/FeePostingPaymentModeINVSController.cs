﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.inventoryClass;



namespace SIMSAPI.Controllers.finance
{
    [RoutePrefix("api/INVSFeePosting")]
    [BasicAuthentication]
    public class FeePostingPaymentModeINVSController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

          [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<FeePPM> mod_list = new List<FeePPM>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[invs_fee_posting_payment_mode_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeePPM simsobj = new FeePPM();
                            simsobj.invs_company_code = dr["com_code"].ToString();
                            simsobj.invs_company_name = dr["com_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

          [Route("getAccYear")]
          public HttpResponseMessage getAccYear()
          {

              List<FeePPM> mod_list = new List<FeePPM>();

              Message message = new Message();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[invs_fee_posting_payment_mode_proc]",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'B'),
                          
                         });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              FeePPM simsobj = new FeePPM();
                              simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                              simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString(); ;
                              mod_list.Add(simsobj);

                          }
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                      }
                      else
                          return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                  }
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
              }



              //return Request.CreateResponse(HttpStatusCode.OK, feeType);
          }

        //Seelct
        [Route("getFeePosting")]
        public HttpResponseMessage getFeePosting()
        {

            List<FeePPM> goaltarget_list = new List<FeePPM>();
         
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[invs_fee_posting_payment_mode_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeePPM simsobj = new FeePPM();
                            simsobj.invs_company_code = dr["sd_company_code"].ToString();
                            simsobj.invs_company_name = dr["invs_compnay_name"].ToString();
                            simsobj.sd_fin_year = dr["sd_fin_year"].ToString();
                            simsobj.sd_payment_type = dr["sd_payment_type"].ToString();
                            simsobj.sd_payment_type_desc = dr["sd_payment_type_desc"].ToString();
                            simsobj.sd_payment_scno = dr["sd_payment_scno"].ToString();
                            simsobj.sd_status = dr["sd_status"].Equals("A") ? true : false;
                            simsobj.sims_academic_year = dr["glma_acct_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetPaymentMode")]
        public HttpResponseMessage GetPaymentMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetPaymentMode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetPaymentMode"));

            List<Fin207> doc_list = new List<Fin207>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[invs_fee_posting_payment_mode_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin207 simsobj = new Fin207();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetGLAccountNumber
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
          
            List<Fin138> doc_list = new List<Fin138>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin138 simsobj = new Fin138();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDFeePostingPaymentModeINVS")]
        public HttpResponseMessage CUDFeePostingPaymentModeINVS(List<FeePPM> data)
        {
           
            Message message = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (FeePPM simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[invs_fee_posting_payment_mode_proc]",
                         new List<SqlParameter>()
                      {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@invs_fee_comp_code", simsobj.invs_company_code),
                                new SqlParameter("@invs_fee_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@invs_fee_payment_type", simsobj.sims_appl_parameter),
                                new SqlParameter("@invs_fee_acno", simsobj.acno),
                                new SqlParameter("@invs_fee_acno_old",simsobj.invs_fee_acno_old),
                                new SqlParameter("@invs_fee_posting_status", simsobj.invs_fee_posting_status==true?"A":"I")
                        });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                           
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                           
                        }
                        if (inserted == true)
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, message);
        }

    }

}










