﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{

    [RoutePrefix("api/SalesTypeDetail")]
    public class SalesTypeController:ApiController
    {

        //[Route("getAccountNo")]
        //public HttpResponseMessage getAccountNo()
        //{
        //    List<Inv126> AccountNo = new List<Inv126>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sale_types]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@OPR", "W")

        //                 }
        //                 );
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Models.ERP.inventoryClass.Inv126 obj = new Inv126();
        //                    obj.glma_acct_code = dr["glma_acct_code"].ToString();
        //                    obj.glma_acct_name = dr["glma_acct_name"].ToString();
        //                    obj.full_account_name = dr["full_account_name"].ToString();
        //                    AccountNo.Add(obj);
        //                }
        //            }
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, AccountNo);
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, AccountNo);
        //}

        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Inv126> doc_list = new List<Inv126>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv126 simsobj = new Inv126();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.ar_account_no = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getSalesTypeDetail")]
        public HttpResponseMessage getSalesTypeDetail()
        {
            List<Inv126> SalesType = new List<Inv126>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sale_types]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Models.ERP.inventoryClass.Inv126 obj = new Inv126();
                            obj.sal_type = dr["sal_type"].ToString();
                            obj.sal_name = dr["sal_name"].ToString();
                            obj.sal_indicator = dr["sal_indicator"].ToString();
                            obj.sale_acno = dr["sale_acno"].ToString();
                            obj.cost_acno = dr["cost_acno"].ToString();
                            SalesType.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, SalesType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, SalesType);
        }

        [Route("SalesTypeCUD")]
        public HttpResponseMessage SalesTypeCUD(List<Inv126> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv126 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_sale_types]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sal_type", simsobj.sal_type),
                                 new SqlParameter("@sal_name", simsobj.sal_name),
                                 new SqlParameter("@sal_indicator", simsobj.salesind),
                                 new SqlParameter("@sal_account_name", simsobj.acno),
                                 new SqlParameter("@cost_account_name", simsobj.acno1),
                            });


                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}