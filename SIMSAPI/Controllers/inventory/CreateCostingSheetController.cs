﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/CreateCostingSheet")]
    public class CreateCostingSheetController:ApiController
    {
        [Route("getDepartments")]
        public HttpResponseMessage getDepartments()
        {
            List<Inv132> Dept_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "R")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();

                            Dept_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Dept_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Dept_list);
        }

        [Route("getForwardAgent")]
        public HttpResponseMessage getForwardAgent()
        {
            List<Inv132> Agent_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.fa_code = dr["fa_code"].ToString();
                            obj.fa_name = dr["fa_name"].ToString();
                            obj.cur_code = dr["cur_code"].ToString();

                            Agent_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Agent_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Agent_list);
        }

        [Route("getSupplier")]
        public HttpResponseMessage getSupplier()
        {
            List<Inv132> Supplier_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();
                            obj.cur_code = dr["cur_code"].ToString();

                            Supplier_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Supplier_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Supplier_list);
        }

        [Route("getLocation")]
        public HttpResponseMessage getLocation()
        {
            List<Inv132> Location_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "L")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.loc_code = dr["loc_code"].ToString();
                            obj.loc_name = dr["loc_name"].ToString();

                            Location_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Location_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Location_list);
        }

        [Route("getDeliveryMode")]
        public HttpResponseMessage getDeliveryMode()
        {
            List<Inv132> Dm_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "N")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.dm_code = dr["dm_code"].ToString();
                            obj.dm_name = dr["dm_name"].ToString();

                            Dm_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Dm_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Dm_list);
        }

        [Route("getTradeTerm")]
        public HttpResponseMessage getTradeTerm()
        {
            List<Inv132> Trade_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "M")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.trt_code = dr["trt_code"].ToString();
                            obj.trt_desc = dr["trt_desc"].ToString();

                            Trade_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Trade_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Trade_list);
        }

        [Route("CreateCustomSheetCUD")]
        public HttpResponseMessage CreateCustomSheetCUD(List<Inv132> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv132 simsobj in data)
                    {


                        if (simsobj.cs_no == "")
                        {
                            simsobj.cs_no = null;
                        }
                        if (simsobj.dep_code == "")
                        {
                            simsobj.dep_code = null;
                        }
                        if (simsobj.sup_code == "")
                        {
                            simsobj.sup_code = null;
                        }
                        if (simsobj.fa_code == "")
                        {
                            simsobj.fa_code = null;
                        }
                        if (simsobj.loc_code == "")
                        {
                            simsobj.loc_code = null;
                        }
                        if (simsobj.cs_desc == "")
                        {
                            simsobj.cs_desc = null;
                        }
                        if (simsobj.cs_date == "")
                        {
                            simsobj.cs_date = null;
                        }
                        if (simsobj.cs_costaccount == "")
                        {
                            simsobj.cs_costaccount = null;
                        }
                        if (simsobj.cs_supl_other_charge == "")
                        {
                            simsobj.cs_supl_other_charge = null;
                        }


                        if (simsobj.cs_forward_agent_charge == "")
                        {
                            simsobj.cs_forward_agent_charge = null;
                        }
                        if (simsobj.cs_remarks == "")
                        {
                            simsobj.cs_remarks = null;
                        }
                        if (simsobj.ccl_reg_no == "")
                        {
                            simsobj.ccl_reg_no = null;
                        }
                        if (simsobj.cs_prov_date == "")
                        {
                            simsobj.cs_prov_date = null;
                        }
                        if (simsobj.lc_no == "")
                        {
                            simsobj.lc_no = null;
                        }

                        if (simsobj.dm_code == "")
                        {
                            simsobj.dm_code = null;
                        }
                        if (simsobj.trt_code == "")
                        {
                            simsobj.trt_code = null;
                        }
                        if (simsobj.cs_loading_note_no == "")
                        {
                            simsobj.cs_loading_note_no = null;
                        }
                        if (simsobj.cs_loading_note_date == "")
                        {
                            simsobj.cs_loading_note_date = null;
                        }
                        if (simsobj.cs_received_location == "")
                        {
                            simsobj.cs_received_location = null;
                        }

                        if (simsobj.cs_received_by == "")
                        {
                            simsobj.cs_received_by = null;
                        }
                        if (simsobj.cur_code == "")
                        {
                            simsobj.cur_code = null;
                        }


                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                        {
                         
                 
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@cs_prov_no",simsobj.cs_prov_no),
                          new SqlParameter("@cs_no", simsobj.cs_no),
                          new SqlParameter("@dep_code",simsobj.dep_code),
                          new SqlParameter("@sup_code", simsobj.sup_code),
                          new SqlParameter("@fa_code", simsobj.fa_code),
                          new SqlParameter("@loc_code",simsobj.loc_code),
                          new SqlParameter("@cs_desc", simsobj.cs_desc),
                          new SqlParameter("@cs_date", db.DBYYYYMMDDformat(simsobj.cs_date)),
                          new SqlParameter("@cs_costaccount", simsobj.cs_costaccount),
                          new SqlParameter("@cs_supl_other_charge", simsobj.cs_supl_other_charge),
                          new SqlParameter("@cs_forward_agent_charge",simsobj.cs_forward_agent_charge),
                          new SqlParameter("@cs_remarks", simsobj.cs_remarks),


                          new SqlParameter("@ccl_reg_no",simsobj.ccl_reg_no),
                          new SqlParameter("@cs_prov_date", db.DBYYYYMMDDformat(simsobj.cs_prov_date)),
                          new SqlParameter("@lc_no", simsobj.lc_no),
                          new SqlParameter("@dm_code", simsobj.dm_code),
                          new SqlParameter("@trt_code", simsobj.trt_code),
                         
                          new SqlParameter("@cs_loading_note_no",simsobj.cs_loading_note_no),
                          new SqlParameter("@cs_loading_note_date", db.DBYYYYMMDDformat(simsobj.cs_loading_note_date)),
                          new SqlParameter("@cs_received_location", simsobj.cs_received_location),
                          new SqlParameter("@cs_received_by", simsobj.cs_received_by),

                          new SqlParameter("@cs_authorized_date",db.DBYYYYMMDDformat(simsobj.cs_date)),
                          new SqlParameter("@cur_code", simsobj.cur_code),
                          //new SqlParameter("@cs_payment_due_date", simsobj.cs_payment_due_date),
                          new SqlParameter("@cs_agency_flag","A"),
                          new SqlParameter("@cs_status","1"),
                          
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getCreateCustomSheet")]
        public HttpResponseMessage getCreateCustomSheet()
        {
            List<Inv132> CustomSheet_list = new List<Inv132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[insert_costing_sheets_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv132 obj = new Inv132();

                            obj.cs_prov_no = dr["cs_prov_no"].ToString();
                            obj.cs_no = dr["cs_no"].ToString();
                            obj.cs_date =db.UIDDMMYYYYformat(dr["cs_date"].ToString());
                            obj.cs_prov_date = db.UIDDMMYYYYformat(dr["cs_prov_date"].ToString());
                            obj.cs_authorized_date = db.UIDDMMYYYYformat(dr["cs_authorized_date"].ToString());
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.sup_name = dr["sup_name"].ToString();

                            obj.cur_code = dr["cur_code"].ToString();

                            obj.fa_code = dr["fa_code"].ToString();
                            obj.fa_name = dr["fa_name"].ToString();

                            obj.loc_code = dr["loc_code"].ToString();
                            obj.loc_name = dr["loc_name"].ToString();

                            obj.cs_costaccount = dr["cs_costaccount"].ToString();
                            obj.cs_desc = dr["cs_desc"].ToString();
                            obj.cs_supl_other_charge = dr["cs_supl_other_charge"].ToString();
                            obj.cs_forward_agent_charge = dr["cs_forward_agent_charge"].ToString();
                            obj.cs_remarks = dr["cs_remarks"].ToString();
                            obj.lc_no = dr["lc_no"].ToString();


                            obj.cs_received_by = dr["cs_received_by"].ToString();
                            obj.cs_received_location = dr["cs_received_location"].ToString();
                            obj.cs_loading_note_date = dr["cs_loading_note_date"].ToString();
                            obj.cs_loading_note_no = dr["cs_loading_note_no"].ToString();

                            obj.dm_code = dr["dm_code"].ToString();
                            obj.dm_name = dr["dm_name"].ToString();

                            obj.trt_code = dr["trt_code"].ToString();
                            obj.trt_desc = dr["trt_desc"].ToString();
                            obj.cs_payment_due_date = db.UIDDMMYYYYformat(dr["cs_payment_due_date"].ToString());
                            obj.ccl_reg_no = dr["ccl_reg_no"].ToString();


                            CustomSheet_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, CustomSheet_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, CustomSheet_list);
        }



    }
}