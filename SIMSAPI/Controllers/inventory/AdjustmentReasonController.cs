﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;
using System.Data;
using SIMSAPI.Attributes;

namespace SIMSAPI.Controllers.inventory
{
    [BasicAuthentication]
    [RoutePrefix("api/AdjustmentReason")]
    public class AdjustmentReasonController : ApiController
    {
        [Route("GetGLAccountNumber")]
        public HttpResponseMessage GetGLAccountNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGLAccountNumber()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Inv126> doc_list = new List<Inv126>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_bank_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv126 simsobj = new Inv126();
                            simsobj.glma_acct_name = dr["glma_acct_name"].ToString();
                            simsobj.acno = dr["acno"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAdjustmentDetail")]
        public HttpResponseMessage getAdjustmentDetail()
        {
            List<Inv046> AdjustmentReason = new List<Inv046>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_adjustment_reasons]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv046 obj = new Inv046();
                            obj.ar_code = dr["ar_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.ar_desc = dr["ar_desc"].ToString();

                            obj.ar_account_no = dr["ar_account_no"].ToString();
                            // obj.glma_acct_name = dr["glma_acct_name"].ToString();

                            obj.ar_indicator = dr["ar_indicator"].ToString().Equals("Y") ? true : false;
                            AdjustmentReason.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, AdjustmentReason);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, AdjustmentReason);
        }

        [Route("getDepartment")]
        public HttpResponseMessage getDepartment()
        {
            List<Inv046> AdjustmentDepart = new List<Inv046>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_adjustment_reasons]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Models.ERP.inventoryClass.Inv046 obj = new Inv046();
                            obj.dep_code = dr["dep_code"].ToString();
                            obj.dep_name = dr["dep_name"].ToString();

                            AdjustmentDepart.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, AdjustmentDepart);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, AdjustmentDepart);
        }

        [Route("AdjustmentReasonCUD")]
        public HttpResponseMessage AdjustmentReasonCUD(List<Inv046> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv046 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_adjustment_reasons]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@ar_code", simsobj.ar_code),
                                 new SqlParameter("@dep_code", simsobj.dep_code),
                                 new SqlParameter("@ar_desc", simsobj.ar_desc),
                                 new SqlParameter("@ar_indicator", simsobj.ar_indicator==true?"Y":"N"),
                                 new SqlParameter("@ar_account_no", simsobj.ar_account_no),
                            });


                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAdjDetails")]
        public HttpResponseMessage getAdjDetails(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                AdjRe sf = Newtonsoft.Json.JsonConvert.DeserializeObject<AdjRe>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_adjustment_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@SEL_ADJ_DATE_FROM", db.DBYYYYMMDDformat(sf.dateFrom)),
                            new SqlParameter("@SEL_ADJ_DATE_UPTO", db.DBYYYYMMDDformat(sf.dateUpto)),
                            new SqlParameter("@SEL_DEP_CODE", sf.dep_code),
                            new SqlParameter("@SEL_ITEM_DESC", sf.ar_desc),
							new SqlParameter("@SUP_CODE", sf.sup_code),
                            new SqlParameter("@SEL_DOC_NO", sf.doc_no),
                            new SqlParameter("@ord_no", sf.ord_no),
                        });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("saveAdjDetails")]
        public HttpResponseMessage saveAdjDetails(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("invs.invs_adjustment_proc", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("invs.invs_adjustment_proc",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","F"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("AdjDetailsGetFORApprove")]
        public HttpResponseMessage AdjDetailsGetFORApprove(Dictionary<string, string> sf)
        {
            object o = null;
            List<SqlParameter> sp = new List<SqlParameter>();
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
               
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var p in sf.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        if(pr1.ParameterName.Equals("@dateUpto"))
                        {
                            pr1.Value =db.DBYYYYMMDDformat(sf[p]);
                        }
                        else if (pr1.ParameterName.Equals("@dateFrom"))
                        {
                            pr1.Value = db.DBYYYYMMDDformat(sf[p]);
                        }
                        else
                        {
                            pr1.Value = sf[p];
                        }
                           
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "AG";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_adjustment_proc", sp);
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;


                }

            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("ApproveAdjDetails")]
        public HttpResponseMessage ApproveAdjDetails(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "AP";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("invs.invs_adjustment_proc", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("invs.invs_adjustment_proc",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","FG"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getDept")]
        public HttpResponseMessage getDept()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_adjustment_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'D')
                        });
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("Search_Shipment_Details")]
        public HttpResponseMessage Search_Shipment_Details(ShipmentOrderDetailsSearch sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetailsSearch() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr",'G'),
                            new SqlParameter("@ship_from_date", db.DBYYYYMMDDformat(sf.ship_from_date)),
                            new SqlParameter("@ship_to_date", db.DBYYYYMMDDformat(sf.ship_to_date)),
                            new SqlParameter("@ship_search_param", sf.ship_search_param),
                            new SqlParameter("@attr1", sf.attr1)}
                            );
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("Shipment_Order_Details")]
        public HttpResponseMessage Shipment_Order_Details(ShipmentOrderDetails sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","SO"),
                            new SqlParameter("@orderNO",sf.orderNO),
                            new SqlParameter("@shipmentNO",sf.shipmentNO),
                            new SqlParameter("@orderLineNO",sf.orderLineNO),
                            new SqlParameter("@req_type",sf.req_type)
                        });
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("Shipment_GET_Details")]
        public HttpResponseMessage Shipment_GET_Details(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","GC"),
                            new SqlParameter("@orderNO",sf.orderNO),
                            new SqlParameter("@shipmentNO",sf.shipmentNO),
                            new SqlParameter("@orderLineNO",sf.orderLineNO),
                            new SqlParameter("@req_type",sf.req_type)
                        });
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("Shipment_save")]
        public HttpResponseMessage Shipment_save(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","CC"),
                            new SqlParameter("@orderNO",sf.orderNO),
                            new SqlParameter("@shipmentNO",sf.shipmentNO),
                            new SqlParameter("@orderLineNO",sf.orderLineNO),
                            new SqlParameter("@req_type",sf.req_type)
                        });
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("Order_getAll")]
        public HttpResponseMessage Order_getAll(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.manage_orders",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","O"),
                            new SqlParameter("@orderNO",sf.orderNO)
                        });
                    ds.DataSetName = "ORDER";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("OrderDET_getAll")]
        public HttpResponseMessage OrderDET_getAll(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.manage_orders",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","OD"),
                            new SqlParameter("@orderNO",sf.orderNO)
                        });
                    ds.DataSetName = "ORDER";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("Request_getAll")]
        public HttpResponseMessage Request_getAll(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.manage_orders",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","R"),
                            new SqlParameter("@orderNO",sf.orderNO)
                        });
                    ds.DataSetName = "ORDER";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("RequestDET_getAll")]
        public HttpResponseMessage RequestDET_getAll(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.manage_orders",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","RD"),
                            new SqlParameter("@orderNO",sf.orderNO)
                        });
                    ds.DataSetName = "ORDER";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("ManageOrderSave")]
        public HttpResponseMessage ManageOrderSave(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "UM";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("invs.manage_orders", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("invs.manage_orders",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","FM"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("ExpTypes")]
        public HttpResponseMessage ExpTypes()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'E')
                        });
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("Trade_term")]
        public HttpResponseMessage Trade_term()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.costing_sheet_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "TT")
                        });
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("SearchCostingSheet")]
        public HttpResponseMessage SearchCostingSheet(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_costing_sheet_exp_details",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "CO")
                        });
                    ds.DataSetName = "SEARCHCOSTINGSHEET";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CostingSheetExpSave")]
        public HttpResponseMessage CostingSheetExpSave(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    foreach (var p in sf.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        pr1.Value = sf[p];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "IC";
                    sp.Add(pr0);

                    int r = db.ExecuteStoreProcedureforInsert("invs.invs_costing_sheet_exp_details", sp);
                    o = r;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CostingSheetExpUP")]
        public HttpResponseMessage CostingSheetExpUP(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    sp.Clear();
                    foreach (var p in sf.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        pr1.Value = sf[p];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "DC";
                    sp.Add(pr0);

                    int r = db.ExecuteStoreProcedureforInsert("invs.invs_costing_sheet_exp_details", sp);
                    o = r;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("SheetExpGET")]
        public HttpResponseMessage SheetExpGET(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            string z = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            z = sf["val"];
                            sp.Add(pr1);
                        }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "G";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_costing_sheet_exp_details", sp);
                    ds.DataSetName = "CostingSheetExpSave";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }
        /*Create Costing Sheet*/
        [Route("AllGRN_Details")]
        public HttpResponseMessage AllGRN_Details(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "GD";
                    sp.Add(pr0);                  
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        /*Create Costing Sheet*/
        [Route("GRN_DetailGet")]
        public HttpResponseMessage GRN_DetailGet(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "OD";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        /*Create Costing Sheet*/
        [Route("AllCostingSheetsGet")]
        public HttpResponseMessage AllCostingSheetsGet(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            if(pr1.ParameterName.Equals("@cs_to_date"))
                            {
                                pr1.Value =db.DBYYYYMMDDformat(sf[p]);
                            }
                            else if (pr1.ParameterName.Equals("@cs_from_date"))
                            {
                                pr1.Value = db.DBYYYYMMDDformat(sf[p]);
                            }
                            else
                            {
                                pr1.Value = sf[p];
                            }                            
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "CD";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_costing_sheet", sp);
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("CostingSheetsGetDetails")]
        public HttpResponseMessage CostingSheetsGetDetails(ShipmentOrderDetails sf)
        {
            object o = null;
            sf = (sf == null ? new ShipmentOrderDetails() : sf);
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_costing_sheet",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","CT"),
                            new SqlParameter("@param",sf.orderLineNO)
                        });
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("CostingSheet_Insert")]
        public HttpResponseMessage CostingSheet_Insert(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "SC";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CostingSheet_Update")]
        public HttpResponseMessage CostingSheet_Update(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "UC";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("FinalizeCostingSheet")]
        public HttpResponseMessage FinalizeCostingSheet(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "F";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[costing_sheet_finalize]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("ServiceGetAll")]
        public HttpResponseMessage ServiceGetAll(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "SA";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("ServiceGetDetails")]
        public HttpResponseMessage ServiceGetDetails(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "SD";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


        [Route("CostingSheetFORApprove")]
        public HttpResponseMessage CostingSheetFORApprove(ShipmentOrderDetails sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_costing_sheet_approve",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr","CT"),
                            new SqlParameter("@param",sf.shipmentNO)
                        });
                    ds.DataSetName = "SHIPMENT";
                    o = ds;
                    s = HttpStatusCode.OK;
                    return Request.CreateResponse(s, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(s, x);
            }
        }

        [Route("ApproveCostingSheet")]
        public HttpResponseMessage ApproveCostingSheet(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "AC";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].invs_costing_sheet_approve", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getDSSup")]
        public HttpResponseMessage getDSSup()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("invs.invs_adjustment_proc",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "DS")
                        });
                    ds.DataSetName = "AdjDept";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //get PO numbers
        [Route("getPOno")]
        public HttpResponseMessage getPOno(string supCode)
        {
            List<Inv100> studentexamdetail = new List<Inv100>();
            try
            {
                 using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("invs.invs_adjustment_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "PN"),
                              new SqlParameter("@sup_code", supCode),
                             


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv100 sequence = new Inv100();

                            sequence.ord_no = dr["ord_no"].ToString();
                            
                            studentexamdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, studentexamdetail);
        }

        //GetLedgerCode
        [Route("e059f8dab55a80d968e1336d8828a1a5015c35f6")]
        public HttpResponseMessage e059f8dab55a80d968e1336d8828a1a5015c35f6(Dictionary<string, string> sf)
        {
            object o = null;
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "LC";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        //GetGLNumber
        [Route("f773e93705ce3ac9d8210bbccbab995da52e4e2d")]
        public HttpResponseMessage f773e93705ce3ac9d8210bbccbab995da52e4e2d(Dictionary<string, string> sf)
        {
            object o = null;
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "GL";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        //GetSLNumber
        [Route("bd4eb48cb53f1389b3e89620b8cfb86dd0776a0c")]
        public HttpResponseMessage bd4eb48cb53f1389b3e89620b8cfb86dd0776a0c(Dictionary<string, string> sf)
        {
            object o = null;
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "SL";
                    sp.Add(pr0);
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_costing_sheet]", sp);
                    ds.DataSetName = "AdjDept";
                    o = ds;
                }
            }
            catch (Exception x)
            {
                o = x;
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        //Get Posting Details
        [Route("CostingSheetsGetPostingDetails")]
        public HttpResponseMessage CostingSheetsGetPostingDetails(string cs_prov_no)
        {            
            List<ShipmentOrderDetails> item_details = new List<ShipmentOrderDetails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_costing_sheet]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "AA"),
                                new SqlParameter("@param", cs_prov_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ShipmentOrderDetails obj = new ShipmentOrderDetails();
                            obj.itemdesc = dr["itemdesc"].ToString();
                            obj.debitacc = dr["debitacc"].ToString();
                            obj.creditacc = dr["creditacc"].ToString();
                            item_details.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, item_details);
            }

            return Request.CreateResponse(HttpStatusCode.OK, item_details);
        }

        //Get Assetization Details
        [Route("CostingSheetsGetAssetizationDetails")]
        public HttpResponseMessage CostingSheetsGetAssetizationDetails(string cs_prov_no)
        {
            List<ShipmentOrderDetails> item_details = new List<ShipmentOrderDetails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_costing_sheet]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "AB"),
                                new SqlParameter("@param", cs_prov_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ShipmentOrderDetails obj = new ShipmentOrderDetails();
                            obj.im_inv_no = dr["im_inv_no"].ToString();
                            obj.itemdesc = dr["ItemName"].ToString();
                            obj.itemdesc1 = dr["itemdesc"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.od_received_qty = dr["od_received_qty"].ToString();
                            obj.sup_code = dr["sup_code"].ToString();
                            obj.suppliername = dr["suppliername"].ToString();
                            obj.od_grv_no = dr["od_grv_no"].ToString();
                            obj.ord_no = dr["ord_no"].ToString();
                            obj.grn_inv_no = dr["grn_inv_no"].ToString();
                            obj.invdate = db.UIDDMMYYYYformat(dr["invdate"].ToString());
                            obj.grndate = db.UIDDMMYYYYformat(dr["grndate"].ToString());
                            obj.csd_landed_cost = dr["csd_landed_cost"].ToString();
                            item_details.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, item_details);
            }

            return Request.CreateResponse(HttpStatusCode.OK, item_details);
        }

        [Route("CSGetPDetails")]
        public HttpResponseMessage CSGetPDetails(string cs_prov_no)
        {
            string result = "no";

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_costing_sheet]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "AB"),
                                new SqlParameter("@param", cs_prov_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           
                            result = dr["result"].ToString();
                            
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}