﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.inventoryClass;
//using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/DeliveryMode")]

    public class DeliveryModeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region API For Delivery Mode...

        // This Show the list of record WebApi
        [Route("getDeliveryMode")]
        public HttpResponseMessage getDeliveryMode()
        {
            List<Inv017> group_list = new List<Inv017>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_mode_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv017 invsDobj = new Inv017();
                            invsDobj.dm_code = dr["dm_code"].ToString();
                            invsDobj.dm_name = dr["dm_name"].ToString();
                            invsDobj.dm_lead_time = dr["dm_lead_time"].ToString();
                            invsDobj.dm_type = dr["dm_type"].ToString();
                            invsDobj.dm_type_name = dr["Type"].ToString();

                            group_list.Add(invsDobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        // This is for the AutoGenerated   WebApi
        [Route("getAutoGenerateCode")]
        public HttpResponseMessage getAutoGenerateCode()
        {
            string dm_code = string.Empty;
            List<Inv017> group_list = new List<Inv017>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_mode_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            dm_code = dr["dm_code"].ToString();


                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dm_code);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dm_code);
        }

        // This is used for the CRUD operetions WebApi
        [Route("CUDDeliveryMode")]
        public HttpResponseMessage CUDDeliveryMode(List<Inv017> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv017 insert_invs in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_mode_proc]",
                            new List<SqlParameter>()
                         {

                           new SqlParameter("@opr", insert_invs.opr),
                           new SqlParameter("@dm_code", insert_invs.dm_code),
                           new SqlParameter("@dm_name", insert_invs.dm_name),
                           new SqlParameter("@dm_lead_time", insert_invs.dm_lead_time),
                           new SqlParameter("@dm_type", insert_invs.dm_type),
                           //new SqlParameter("@dm_type_name", insert_invs.dm_type_desc)
                         
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getType")]
        public HttpResponseMessage getType()
        {
            List<Inv017> group_list = new List<Inv017>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_delivery_mode_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv017 obj = new Inv017();

                            obj.dm_type_desc = dr["invs_appl_form_field_value1"].ToString();
                            obj.dm_type = dr["invs_appl_parameter"].ToString();

                            group_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, group_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, group_list);
        }

        #endregion

        #region API For Item Movements...

        [Route("getItemSerch")]
        public HttpResponseMessage getItemSerch(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_movements_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@req_type", sf.req_type),
                                new SqlParameter("@im_inv_no", sf.im_inv_no),
                                new SqlParameter("@im_desc", sf.rd_item_desc),
                                new SqlParameter("@im_item_code", sf.req_no),
                                new SqlParameter("@sec_code", sf.dept_code),
                                new SqlParameter("@sup_code", sf.rd_line_no),
                                new SqlParameter("@sm_code", sf.sm_code),
                                new SqlParameter("@category_code", sf.final_qnt),
                                new SqlParameter("@subcategory_code", sf.rd_quantity),
                              
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getServiceSerch")]
        public HttpResponseMessage getServiceSerch(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_movements_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'B'),
                                new SqlParameter("@req_type", sf.req_type),
                                new SqlParameter("@im_inv_no", sf.im_inv_no),
                                new SqlParameter("@im_item_desc", sf.rd_item_desc),
                                new SqlParameter("@im_item_code", sf.req_no),
                                new SqlParameter("@sec_code", sf.dept_code),
                                new SqlParameter("@sup_code", sf.rd_line_no),
                                new SqlParameter("@sm_code", sf.sm_code),
                                new SqlParameter("@category_code", sf.final_qnt),
                                new SqlParameter("@subcategory_code", sf.rd_quantity),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("getItemmovementdetails")]
        public HttpResponseMessage getItemmovementdetails(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_movements_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@req_type", sf.req_type),
                                new SqlParameter("@im_item_code", sf.im_inv_no),
                                new SqlParameter("@doc_date1", db.DBYYYYMMDDformat(sf.rd_from_required)),
                                new SqlParameter("@doc_date2", db.DBYYYYMMDDformat(sf.rd_up_required)),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        #region API For Badge Master...

        [Route("getBadgeMaster")]
        public HttpResponseMessage getBadgeMaster()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_badge_master_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CUDBadgeMaster")]
        public HttpResponseMessage CUDBadgeMaster(List<Inv017> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv017 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_badge_master_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                           new SqlParameter("@sims_badge_id", simsobj.dm_type_name),
                           new SqlParameter("@sims_badge_name", simsobj.dm_code),
                           new SqlParameter("@sims_badge_visible_on_parent_portal", simsobj.dm_name=="True"?"A":"I"),
                           new SqlParameter("@sims_badge_visible_on_school_level", simsobj.dm_lead_time=="True"?"A":"I"),
                           new SqlParameter("@sims_badge_status", simsobj.dm_type=="True"?"A":"I"),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region API For Badge Allocation...

        [Route("getSeachOptions")]
        public HttpResponseMessage getSeachOptions()
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_badge_master_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'E'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.fee_concession_number = dr["sims_appl_parameter"].ToString();
                            simsobj.fee_concession_description = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.fee_concession_type = dr["sims_appl_form_field_value3"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getBadgeOptions")]
        public HttpResponseMessage getBadgeOptions()
        {
            List<Inv017> mod_list = new List<Inv017>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_badge_master_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'F'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv017 simsobj = new Inv017();
                            simsobj.dm_type_name = dr["sims_badge_id"].ToString();
                            simsobj.dm_code = dr["sims_badge_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        
        [Route("getBadgeAllocationStudent")]
        public HttpResponseMessage getBadgeAllocationStudent(string data1)
        {
            string opr=null;
            object o1 = null;
            HttpStatusCode s1 = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data1);
                if (!string.IsNullOrEmpty(sf1.req_no) || !string.IsNullOrEmpty(sf1.rd_item_desc))
                    opr="G";
                else
                    opr="J";
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds1 = db.ExecuteStoreProcedureDS("[sims].[sims_badge_master_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", opr),
                                new SqlParameter("@sims_cur_code", sf1.req_no),
                                new SqlParameter("@sims_academic_year", sf1.dept_code),
                                new SqlParameter("@sims_grade_code", sf1.rd_item_desc),
                                new SqlParameter("@sims_section_code", sf1.request_mode_code),
                                new SqlParameter("@sims_search_code", sf1.req_range_no),
                             });
                    ds1.DataSetName = "AdjData1";
                    o1 = ds1;
                    s1 = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o1 = x;
                s1 = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s1, o1);
        }

        /*
        [Route("getBadgeAllocationStudent")]
        public HttpResponseMessage getBadgeAllocationStudent(string data1)
        {
            string opr=null;
            object o1 = null;
            List<Inv017> mod_list = new List<Inv017>();
            HttpStatusCode s1 = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data1);
                if (!string.IsNullOrEmpty(sf1.req_no) || !string.IsNullOrEmpty(sf1.rd_item_desc))
                    opr="G";
                else
                    opr="F";
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_badge_master_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", opr),
                            new SqlParameter("@sims_cur_code", sf1.req_no),
                            new SqlParameter("@sims_academic_year", sf1.dept_code),
                            new SqlParameter("@sims_grade_code", sf1.rd_item_desc),
                            new SqlParameter("@sims_section_code", sf1.request_mode_code),
                            new SqlParameter("@sims_search_code", sf1.req_range_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv017 simsobj = new Inv017();
                            simsobj.dm_name = dr["student_name"].ToString();
                            simsobj.dm_code = dr["sims_enroll_number"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }
        */

        [Route("CUDBadgeAllocationMaster")]
        public HttpResponseMessage CUDBadgeAllocationMaster(List<Inv017> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Inv017 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_badge_master_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                           new SqlParameter("@sims_cur_code", simsobj.dm_code),
                           new SqlParameter("@sims_academic_year", simsobj.dm_name),
                           new SqlParameter("@sims_badge_id", simsobj.dm_lead_time),
                           new SqlParameter("@sims_enroll_number", simsobj.dm_type),
                           new SqlParameter("@sims_user_code", simsobj.dm_type_desc),
                           new SqlParameter("@sims_remark", simsobj.dm_type_name),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

    }
}














