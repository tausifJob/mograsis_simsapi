﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/Sections")]
    [BasicAuthentication]
    public class SectionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetDepartment")]
        public HttpResponseMessage GetDepartment()
        {
            List<Invs052> goaltarget_list = new List<Invs052>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[departments]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs052 invsobj = new Invs052();
                            invsobj.dep_code = dr["dep_code"].ToString();
                            invsobj.dep_name = dr["dep_name"].ToString();
                            goaltarget_list.Add(invsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetStockType")]
        public HttpResponseMessage GetStockType()
        {
            List<Invs052> goaltarget_list = new List<Invs052>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sections_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs052 invsobj = new Invs052();
                            invsobj.sec_stocking_type = dr["invs_appl_parameter"].ToString();
                            invsobj.sec_stocking_type_name = dr["invs_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("GetSections")]
        public HttpResponseMessage GetSections()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSections(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSections"));

            List<Invs052> goaltarget_list = new List<Invs052>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[invs_sections_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs052 invsobj = new Invs052();
                            invsobj.dep_code = dr["dep_code"].ToString();
                            invsobj.dep_name = dr["dep_name"].ToString();
                            invsobj.sec_code = dr["sec_code"].ToString();
                            invsobj.sec_name = dr["sec_name"].ToString();
                            invsobj.sec_stock_account_no = dr["sec_stock_account_no"].ToString();
                            invsobj.sec_stocking_type = dr["sec_stocking_type"].ToString();
                            invsobj.sec_stocking_type_name = dr["sec_stocking_type_name"].ToString();
                            goaltarget_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDSections")]
        public HttpResponseMessage CUDSections(List<Invs052> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs052 invsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[invs_sections_proc]",
                        new List<SqlParameter>()
                     {
                         new SqlParameter("@opr", invsobj.opr),
                         new SqlParameter("@dep_code", invsobj.dep_code),
                         new SqlParameter("@sec_code", invsobj.sec_code),
                         new SqlParameter("@sec_name", invsobj.sec_name),
                         new SqlParameter("@sec_stock_account_no", invsobj.sec_stock_account_no),
                         new SqlParameter("@sec_stocking_type", invsobj.sec_stocking_type),                                                                              
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}