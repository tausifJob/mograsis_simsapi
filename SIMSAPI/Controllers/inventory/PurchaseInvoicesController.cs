﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.inventoryClass;

namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/purchaseinvices")]
    public class PurchaseInvoicesController:ApiController
    {
        #region Invs028(Purchase invoices)

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            List<Invs028> mod_list = new List<Invs028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs028 simsobj = new Invs028();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curc_desc = dr["excg_curcy_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }

                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("Get_Adj_Suppliername")]
        public HttpResponseMessage Get_Adj_Suppliername()
        {
            List<Invs028> mod_list = new List<Invs028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "R")
                });
                    while (dr.Read())
                    {
                        Invs028 obj = new Invs028();
                        obj.sup_name = dr["sup_name"].ToString();
                        obj.sup_code = dr["sup_code"].ToString();
                        mod_list.Add(obj);
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAll_Payment_Modes")]
        public HttpResponseMessage GetAll_Payment_Modes()
        {
            List<Invs028> list = new List<Invs028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                while (dr.Read())
                {
                    Invs028 invsobj = new Invs028();
                    invsobj.pm_code = dr["pm_code"].ToString();
                    invsobj.pm_desc = dr["pm_desc"].ToString();
                    list.Add(invsobj);
                }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetAll_Forward_Agents")]
        public HttpResponseMessage GetAll_Forward_Agents()
        {
            List<Invs028> list = new List<Invs028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A")
                });
                while (dr.Read())
                {
                    Invs028 invsobj = new Invs028();
                    invsobj.fa_code = dr["fa_code"].ToString();
                    invsobj.fa_name = dr["fa_name"].ToString();

                    list.Add(invsobj);
                }
                }
            }
            catch (Exception e)
            {
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetAll_final_payments")]
        public HttpResponseMessage GetAll_final_payments()
        {
            List<Invs028> list = new List<Invs028>();
            try
            {
                 using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[item_suppliers_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "F")
                });
                while (dr.Read())
                {
                    Invs028 invsobj = new Invs028();
                    invsobj.fp_no = dr["fp_no"].ToString();

                    list.Add(invsobj);
                }
                 }
            }
            catch (Exception e)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetAll_Purchase_Invoices")]
        public HttpResponseMessage GetAll_Purchase_Invoices()
        {
            List<Invs028> mod_list = new List<Invs028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purchase_invoices_proc]",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", 'S')
                });
                    while (dr.Read())
                    {
                        Invs028 invsObj = new Invs028();
                        if (!string.IsNullOrEmpty(dr["pi_no"].ToString()))
                            invsObj.pi_no = dr["pi_no"].ToString();
                        if (!string.IsNullOrEmpty(dr["cs_prov_no"].ToString()))
                            invsObj.cs_prov_no = dr["cs_prov_no"].ToString();
                        invsObj.sup_code = dr["sup_code"].ToString();
                        invsObj.fa_code = dr["fa_code"].ToString();
                        invsObj.cur_code = dr["cur_code"].ToString();
                        invsObj.pi_exchange_rate = dr["pi_exchange_rate"].ToString();
                        invsObj.pi_invoice_no = dr["pi_invoice_no"].ToString();
                        //if (!string.IsNullOrEmpty(dr["pi_invoice_date"].ToString()))
                        //    invsObj.pi_invoice_date = dr["pi_invoice_date"].ToString();
                        invsObj.pi_invoice_date = db.UIDDMMYYYYformat(dr["pi_invoice_date"].ToString());

                        invsObj.pi_amount = dr["pi_amount"].ToString();
                        invsObj.pi_discount_amount = dr["pi_discount_amount"].ToString();
                        invsObj.pi_freight = dr["pi_freight"].ToString();
                        invsObj.pi_insurance = dr["pi_insurance"].ToString();
                        invsObj.pi_other_charge = dr["pi_other_charge"].ToString();
                        invsObj.pi_payment_due_date = db.UIDDMMYYYYformat(dr["pi_payment_due_date"].ToString());
                        invsObj.pi_indicator = dr["pi_indicator"].Equals("Y") ? true : false;
                        invsObj.pi_paid_flag = dr["pi_paid_flag"].Equals("Y")?true:false;
                        invsObj.fp_no = dr["fp_no"].ToString();
                        invsObj.pm_code = dr["pm_code"].ToString();
                        invsObj.pi_remarks = dr["pi_remarks"].ToString();
                        invsObj.pi_debit_note_no = dr["pi_debit_note_no"].ToString();
                        invsObj.pi_debit_note_date = db.UIDDMMYYYYformat(dr["pi_debit_note_date"].ToString());
                        invsObj.pi_invoice_received_date = db.UIDDMMYYYYformat(dr["pi_invoice_received_date"].ToString());
                        invsObj.pi_creation_date = db.UIDDMMYYYYformat(dr["pi_creation_date"].ToString());
                        invsObj.sup_desc = dr["sup_desc"].ToString();
                        invsObj.fa_desc = dr["fa_desc"].ToString();
                        invsObj.cur_desc = dr["cur_desc"].ToString();
                        invsObj.pm_desc = dr["pm_desc"].ToString();
                      


                        mod_list.Add(invsObj);
                    }
                }
            }
            catch (Exception e)
            {
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUD_Purchase_Invoices")]
        public HttpResponseMessage Insert_Purchase_Invoices(List<Invs028> data)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs028 invsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purchase_invoices_proc]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", invsobj.opr),
                new SqlParameter("@pi_no", invsobj.pi_no),
                new SqlParameter("@cs_prov_no", invsobj.cs_prov_no),
                new SqlParameter("@sup_code", invsobj.sup_code),
                new SqlParameter("@fa_code", invsobj.fa_code),
                new SqlParameter("@cur_code", invsobj.cur_code),
                new SqlParameter("@pi_exchange_rate", invsobj.pi_exchange_rate),
                new SqlParameter("@pi_invoice_no", invsobj.pi_invoice_no),
                new SqlParameter("@pi_invoice_date", db.DBYYYYMMDDformat(invsobj.pi_invoice_date)),
                new SqlParameter("@pi_amount", invsobj.pi_amount),
                new SqlParameter("@pi_discount_amount", invsobj.pi_discount_amount),
                new SqlParameter("@pi_freight", invsobj.pi_freight),
                new SqlParameter("@pi_insurance", invsobj.pi_insurance),
                new SqlParameter("@pi_other_charge", invsobj.pi_other_charge),
                new SqlParameter("@pi_payment_due_date", db.DBYYYYMMDDformat(invsobj.pi_payment_due_date)),
                new SqlParameter("@pi_indicator", invsobj.pi_indicator==true?"Y":"N"),
                new SqlParameter("@pi_paid_flag", invsobj.pi_paid_flag==true?"Y":"N"),
                new SqlParameter("@fp_no", invsobj.fp_no),
                new SqlParameter("@pm_code", invsobj.pm_code),
                new SqlParameter("@pi_remarks", invsobj.pi_remarks),
                new SqlParameter("@pi_debit_note_no", invsobj.pi_debit_note_no),
                new SqlParameter("@pi_debit_note_date", db.DBYYYYMMDDformat(invsobj.pi_debit_note_date)),
                new SqlParameter("@pi_invoice_received_date", db.DBYYYYMMDDformat(invsobj.pi_invoice_received_date)),
                new SqlParameter("@pi_creation_date", db.DBYYYYMMDDformat(invsobj.pi_creation_date)),
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }


        #endregion
    }
}