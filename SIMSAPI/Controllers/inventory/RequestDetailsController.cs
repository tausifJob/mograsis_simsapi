﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.inventoryClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;


#region
/*
namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/RequestDetail")]
    public class RequestDetailsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Seelct
        [Route("getRequestItemSerch")]
        public HttpResponseMessage getRequestItemSerch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getRequestItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                         {

                            new SqlParameter("@opr", 'P'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.req_discount_pct = Convert.ToDecimal(dr["req_discount_pct"].ToString());
                            simsobj.req_requester_dep = dr["req_requester_dep"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            simsobj.req_agency_flag = dr["req_agency_flag"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getItemSerch")]
        public HttpResponseMessage getItemSerch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                            new SqlParameter("@opr", 'S'),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                           // simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSearchRequest")]
        public HttpResponseMessage getSearchRequest()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchRequest(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchRequest"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {

                            new SqlParameter("@opr", 'S'),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToInt32(dr["req_no"].ToString());
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.req_date = dr["req_date"].ToString();
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            //simsobj.subCategory = dr["subCategory"].ToString();
                            //simsobj.dep_code = dr["dep_code"].ToString();
                            //simsobj.sec_code = dr["sec_code"].ToString();
                            //simsobj.sup_code = dr["sup_code"].ToString();
                            //simsobj.uom_code = dr["uom_code"].ToString();
                            //simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            //simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            //simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            //simsobj.sg_desc = dr["sg_desc"].ToString();
                            //simsobj.dep_name = dr["dep_name"].ToString();
                            //simsobj.uom_name = dr["uom_name"].ToString();
                            //simsobj.sec_name = dr["sec_name"].ToString();
                            //simsobj.sup_name = dr["sup_name"].ToString();
                            //simsobj.curr_qty = dr["curr_qty"].ToString();
                            //simsobj.do_qty = dr["do_qty"].ToString();
                            //simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getGridDataforEdit")]
        public HttpResponseMessage getGridDataforEdit(string req_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGridDataforEdit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getGridDataforEdit"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@req_no",req_no)
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();


                            simsobj.excg_curcy_code = dr["cur_code"].ToString();
                            simsobj.im_desc = dr["rd_item_desc"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.im_sell_price = dr["rd_estimated_price"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            simsobj.rd_date_required = dr["rd_date_required"].ToString();
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getRequestType")]
        public HttpResponseMessage getRequestType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_type_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_type_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeparments")]
        public HttpResponseMessage getDeparments()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeparments()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeparments"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dept_code = dr["dep_code"].ToString();
                            simsobj.dept_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeliveryType")]
        public HttpResponseMessage getDeliveryType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeliveryType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeliveryType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestStatus")]
        public HttpResponseMessage getRequestStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestStatus()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestStatus"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_status_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_status_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSalesmanType")]
        public HttpResponseMessage getSalesmanType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSalesmanType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getSalesmanType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.salesman_code = dr["sm_code"].ToString();
                            simsobj.salesman_name = dr["sm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrency()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCurrency"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getItems")]
        public HttpResponseMessage getItems()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItems()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getItems"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'X'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDRequestDetails")]
        public HttpResponseMessage CUDRequestDetails(string data1, List<Invs058_item> data)
        {

            bool inserted = false;
            Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@req_no",obj.req_no),
                                      new SqlParameter("@req_type", obj.request_type_code),
                                      new SqlParameter("@dep_code", obj.dept_code),
                                      new SqlParameter("@dm_code", obj.dm_code),
                                      new SqlParameter("@req_status", obj.request_status_code),
                                      new SqlParameter("@sm_code", obj.salesman_code),
                                      new SqlParameter("@req_remarks", obj.req_remarks),
                                      new SqlParameter("@req_discount_pct",0),
                                      new SqlParameter("@req_agency_flag",'Y'),
                                 });
                    if (ins > 0)
                    {
                        inserted = true;

                    }

                    foreach (Invs058_item simsobj in data)
                    {

                        int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@rd_status",0),
                                new SqlParameter("@req_no",simsobj.req_no),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@cur_code",simsobj.excg_curcy_code),
                                new SqlParameter("@rd_estimated_price",simsobj.im_sell_price),
                                new SqlParameter("@rd_date_required",simsobj.rd_date_required),
                                new SqlParameter("@rd_quantity", simsobj.rd_quantity),
                                new SqlParameter("@rd_remarks", simsobj.rd_remarks),
                                new SqlParameter("@rd_item_desc",simsobj.im_desc)

                         });

                        if (ins1 > 0)
                        {
                            inserted = true;

                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("DelRequestDetails")]
        public HttpResponseMessage DelRequestDetails(List<Invs058_item> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDesignation(),PARAMETERS";
            bool inserted = false;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs058_item simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@req_no", simsobj.req_no),
                                  
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}
*/
#endregion


namespace SIMSAPI.Controllers.inventory
{
    [RoutePrefix("api/RequestDetail")]
    public class RequestDetailsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Service(API) For Request Details...
        /*
        [Route("getRequestItemSerch")]
        public HttpResponseMessage getRequestItemSerch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getRequestItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                         {

                            new SqlParameter("@opr", 'P'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.req_discount_pct = Convert.ToDecimal(dr["req_discount_pct"].ToString());
                            simsobj.req_requester_dep = dr["req_requester_dep"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            simsobj.req_agency_flag = dr["req_agency_flag"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        #region Code For Search Without Using Window...

        [Route("getItemSerch")]
        public HttpResponseMessage getItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                                new SqlParameter("@opr", opr),
                                new SqlParameter("@im_inv_no", itsr.im_inv_no),
                                new SqlParameter("@sg_name", itsr.invs021_sg_name),
                                new SqlParameter("@im_item_code", itsr.im_item_code),
                                new SqlParameter("@im_desc", itsr.im_desc),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@sec_code", itsr.sec_code),
                                new SqlParameter("@sup_code", itsr.invs021_sup_code),
                                new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            // simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("postItemSerch")]
        public HttpResponseMessage postItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            if (itsr.im_inv_no == "" || itsr.im_inv_no == "undefined")
                itsr.im_inv_no = null;


            if (itsr.im_desc == "" || itsr.im_desc == "undefined")
                itsr.im_desc = null;


            if (itsr.im_item_code == "" || itsr.im_item_code == "undefined")
                itsr.im_item_code = null;


            if (itsr.dept_code == "" || itsr.dept_code == "undefined")
                itsr.dept_code = null;


            if (itsr.invs021_sg_name == "" || itsr.invs021_sg_name == "undefined")
                itsr.invs021_sg_name = null;


            if (itsr.sec_code == "" || itsr.sec_code == "undefined")
                itsr.sec_code = null;


            if (itsr.sup_sblgr_acno == "" || itsr.sup_sblgr_acno == "undefined")
                itsr.sup_sblgr_acno = null;


            if (itsr.category_code == "" || itsr.category_code == "undefined")
                itsr.category_code = null;


            if (itsr.subcategory_code == "" || itsr.subcategory_code == "undefined")
                itsr.subcategory_code = null;

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                                new SqlParameter("@opr", opr),
                                new SqlParameter("@im_inv_no", itsr.im_inv_no),
                                new SqlParameter("@im_desc", itsr.im_desc),
                                new SqlParameter("@im_item_code", itsr.im_item_code),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@sg_name", itsr.invs021_sg_name),
                                new SqlParameter("@sec_code", itsr.sec_code),
                                new SqlParameter("@sup_code", itsr.sup_sblgr_acno),
                                //new SqlParameter("@sup_code", itsr.code),
                                new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            // simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getServiceSerch")]
        public HttpResponseMessage getServiceSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        #endregion

        #region Code For Search For Update Request By Using Window...

        [Route("getSearchRequest")]
        public HttpResponseMessage getSearchRequest(string sr, string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchRequest(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchRequest"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@req_mode", sr),
                                new SqlParameter("@user_code", user_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToInt32(dr["req_no"].ToString());
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.req_date = dr["req_date"].ToString();
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.req_mode = dr["req_mode"].ToString();
                            simsobj.req_mode_name = dr["req_mode_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        #endregion

        [Route("getServiceSearch")]
        public HttpResponseMessage getServiceSearch(string im_no, string des_ser, string dep_name)
        {

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@im_inv_no",im_no),
                                new SqlParameter("@im_desc", des_ser),
                                new SqlParameter("@dep_code",dep_name),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getuomdetails")]
        public HttpResponseMessage getuomdetails()
        {

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getGridDataforEdit")]
        public HttpResponseMessage getGridDataforEdit(string req_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGridDataforEdit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getGridDataforEdit"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@req_no",req_no)
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();


                            simsobj.excg_curcy_code = dr["cur_code"].ToString();
                            simsobj.im_desc = dr["rd_item_desc"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.im_sell_price = dr["rd_estimated_price"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            simsobj.rd_date_required = dr["rd_date_required"].ToString();
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getRequestType")]
        public HttpResponseMessage getRequestType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_type_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_type_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestTypeNew")]
        public HttpResponseMessage getRequestTypeNew()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Y'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_mode_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_mode_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeparments")]
        public HttpResponseMessage getDeparments()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeparments()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeparments"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dept_code = dr["dep_code"].ToString();
                            simsobj.dept_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeliveryType")]
        public HttpResponseMessage getDeliveryType(string rtype)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeliveryType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeliveryType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@req_mode", rtype),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestStatus")]
        public HttpResponseMessage getRequestStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestStatus()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestStatus"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_status_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_status_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSalesmanType")]
        public HttpResponseMessage getSalesmanType(string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSalesmanType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getSalesmanType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@user_code", user_code),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.salesman_code = dr["sm_code"].ToString();
                            simsobj.salesman_name = dr["sm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrency()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCurrency"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getItems")]
        public HttpResponseMessage getItems()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItems()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getItems"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'X'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDRequestDetails")]
        public HttpResponseMessage CUDRequestDetails(string data1, List<Invs058_item> data)
        {
            Message msg = new Message();
            string st = string.Empty;
            string sn = string.Empty;

            bool inserted = false;
            Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@req_no",obj.req_no),
                                      new SqlParameter("@req_mode", obj.request_mode_code),
                                      new SqlParameter("@req_type", obj.request_type_code),
                                      new SqlParameter("@dep_code", obj.dept_code),
                                      new SqlParameter("@dm_code", obj.dm_code),
                                      new SqlParameter("@req_status", obj.request_status_code),
                                      new SqlParameter("@sm_code", obj.salesman_code),
                                      new SqlParameter("@req_remarks", obj.req_remarks),
                                      new SqlParameter("@req_discount_pct",0),
                                      new SqlParameter("@req_agency_flag",'Y'),

                                 });
                    //if (ins > 0)
                    //{
                    //    inserted = true;
                    //
                    //}

                    if (dr1.Read())
                    {
                        st = dr1["RequestNo"].ToString();
                    }

                    if (dr1.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr1.Close();


                    foreach (Invs058_item simsobj in data)
                    {
                        if (simsobj.excg_curcy_code != null)
                        {
                            //int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",

                               new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@rd_status",0),
                                new SqlParameter("@dep_code", obj.dept_code),
                                new SqlParameter("@req_mode", obj.request_mode_code),
                                new SqlParameter("@req_type", obj.request_type_code),
                                new SqlParameter("@req_no",simsobj.req_no),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@cur_code",simsobj.excg_curcy_code),
                                new SqlParameter("@rd_estimated_price",simsobj.im_sell_price),
                                new SqlParameter("@rd_date_required",simsobj.rd_date_required),
                                new SqlParameter("@rd_quantity", simsobj.rd_quantity),
                                new SqlParameter("@rd_remarks", simsobj.rd_remarks),
                                new SqlParameter("@rd_item_desc",simsobj.im_desc)

                         });

                            if (dr.Read())
                            {
                                st = dr["RequestNo"].ToString();

                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                            if (inserted == false)
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                                new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@req_mode", obj.request_mode_code),
                                    new SqlParameter("@req_type", obj.request_type_code),
                                    new SqlParameter("@req_no",st),
                                    new SqlParameter("@dep_code", obj.dept_code),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                            else
                            {
                                //sn = sn + st + ' ' + simsobj.im_desc + "</br>";
                                sn = sn + st + '-' + simsobj.im_desc + "</br>";
                            }
                            //if (ins1 > 0)
                            //{
                            //    inserted = true;

                            //}
                            //else
                            //{
                            //    inserted = false;
                            //}
                        }
                        else
                        {
                            //if (inserted == false)
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                                new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@req_mode", obj.request_mode_code),
                                    new SqlParameter("@req_type", obj.request_type_code),
                                    new SqlParameter("@req_no",st),
                                    new SqlParameter("@dep_code", obj.dept_code),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }
                    if (sn != null)
                    {
                        msg.strMessage = sn;
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDRequestDetailsUpdate")]
        public HttpResponseMessage CUDRequestDetailsUpdate(string data2, List<Invs058_item> data1)
        {
            Message msg = new Message();
            string st = string.Empty;
            string sn = string.Empty;

            bool inserted = false;
            Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data2);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@req_no",obj.req_no),
                                      new SqlParameter("@req_mode", obj.request_mode_code),
                                      new SqlParameter("@req_type", obj.request_type_code),
                                      new SqlParameter("@dep_code", obj.dept_code),
                                      new SqlParameter("@dm_code", obj.dm_code),
                                      new SqlParameter("@req_status", obj.request_status_code),
                                      new SqlParameter("@sm_code", obj.salesman_code),
                                      new SqlParameter("@req_remarks", obj.req_remarks),
                                      new SqlParameter("@req_discount_pct",0),
                                      new SqlParameter("@req_agency_flag",'Y'),
                                 });
                    if (ins > 0)
                    {
                        inserted = true;

                    }


                    foreach (Invs058_item simsobj in data1)
                    {
                        if (simsobj.excg_curcy_code != null)
                        {
                            int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",

                               new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@rd_status",0),
                                new SqlParameter("@req_mode", obj.request_mode_code),
                                new SqlParameter("@req_type", obj.request_type_code),
                                new SqlParameter("@req_no",simsobj.req_no),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@cur_code",simsobj.excg_curcy_code),
                                new SqlParameter("@rd_estimated_price",simsobj.im_sell_price),
                                new SqlParameter("@rd_date_required",simsobj.rd_date_required),
                                new SqlParameter("@rd_quantity", simsobj.rd_quantity),
                                new SqlParameter("@rd_remarks", simsobj.rd_remarks),
                                new SqlParameter("@rd_item_desc",simsobj.im_desc)

                         });


                            if (ins1 > 0)
                            {
                                inserted = true;

                            }
                            else
                            {
                                inserted = false;
                            }

                        }
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("DelRequestDetails")]
        public HttpResponseMessage DelRequestDetails(List<Invs058_item> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDesignation(),PARAMETERS";
            bool inserted = false;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs058_item simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@req_no", simsobj.req_no),
                                  
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        */
        #endregion

        #region Service(API) For Request Details...


        #region RO
        //Seelct
        [Route("getRequestItemSerch")]
        public HttpResponseMessage getRequestItemSerch(string sr, string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getRequestItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                         {

                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@req_mode", sr),
                            new SqlParameter("@user_code", user_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            //simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            //simsobj.req_type = dr["req_type"].ToString();
                            //simsobj.dep_code = dr["dep_code"].ToString();
                            //simsobj.req_status = dr["req_status"].ToString();
                            //simsobj.dep_name = dr["dep_name"].ToString();
                            //simsobj.req_type_name = dr["req_type_name"].ToString();
                            //simsobj.req_status_name = dr["req_status_name"].ToString();
                            //simsobj.dm_code = dr["dm_code"].ToString();
                            //simsobj.req_discount_pct = Convert.ToDecimal(dr["req_discount_pct"].ToString());
                            //simsobj.req_requester_dep = dr["req_requester_dep"].ToString();
                            //simsobj.req_remarks = dr["req_remarks"].ToString();
                            //simsobj.req_agency_flag = dr["req_agency_flag"].ToString();
                            //simsobj.dm_name = dr["dm_name"].ToString();
                            //simsobj.sm_code = dr["sm_name"].ToString();

                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_date = db.UIDDMMYYYYformat(dr["reqdate"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("RequestDetailsItemSearch")]
        public HttpResponseMessage RequestDetailsItemSearch(Invs058_item data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchRequest(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchRequest"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                           new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@req_no", data.req_no),
                                new SqlParameter("@req_mode", data.req_type),
                                new SqlParameter("@user_code", data.sm_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.reqQtyespricetotal = dr["reqQtyespricetotal"].ToString();
                            simsobj.rd_estimate_price = dr["rd_estimated_price"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            simsobj.req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                            simsobj.curr_qty = dr["id_cur_qty"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.req_mode = dr["req_mode"].ToString();
                            simsobj.req_mode_name = dr["req_mode_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["rd_item_desc"].ToString();
                            simsobj.rd_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                            simsobj.excg_curcy_code = dr["cur_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();


                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getItemSerch")]
        public HttpResponseMessage getItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                                new SqlParameter("@opr", opr),
                                new SqlParameter("@im_inv_no", itsr.im_inv_no),
                                new SqlParameter("@im_desc", itsr.im_desc),
                                new SqlParameter("@im_item_code", itsr.im_item_code),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@sg_name", itsr.invs021_sg_name),
                                new SqlParameter("@sec_code", itsr.sec_code),
                                new SqlParameter("@sup_code", itsr.sup_sblgr_acno),
                               // new SqlParameter("@sup_code", itsr.code),
                                new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            // simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("postItemSerch")]
        public HttpResponseMessage postItemSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";

            string opr = string.Empty;
            if (itsr == null)
                itsr = new Invs058_itemNew();
            if (itsr.im_assembly_ind == true)
            {
                opr = "A";
            }
            else
            {
                opr = "S";

            }

            if (itsr.im_inv_no == "" || itsr.im_inv_no == "undefined")
                itsr.im_inv_no = null;


            if (itsr.im_desc == "" || itsr.im_desc == "undefined")
                itsr.im_desc = null;


            if (itsr.im_item_code == "" || itsr.im_item_code == "undefined")
                itsr.im_item_code = null;


            if (itsr.dept_code == "" || itsr.dept_code == "undefined")
                itsr.dept_code = null;


            if (itsr.invs021_sg_name == "" || itsr.invs021_sg_name == "undefined")
                itsr.invs021_sg_name = null;


            if (itsr.sec_code == "" || itsr.sec_code == "undefined")
                itsr.sec_code = null;


            if (itsr.sup_sblgr_acno == "" || itsr.sup_sblgr_acno == "undefined")
                itsr.sup_sblgr_acno = null;


            if (itsr.category_code == "" || itsr.category_code == "undefined")
                itsr.category_code = null;


            if (itsr.subcategory_code == "" || itsr.subcategory_code == "undefined")
                itsr.subcategory_code = null;

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {

                                new SqlParameter("@opr", opr),
                                new SqlParameter("@im_inv_no", itsr.im_inv_no),
                                new SqlParameter("@im_desc", itsr.im_desc),
                                new SqlParameter("@im_item_code", itsr.im_item_code),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@sg_name", itsr.invs021_sg_name),
                                new SqlParameter("@sec_code", itsr.sec_code),
                                new SqlParameter("@sup_code", itsr.sup_sblgr_acno),
                                //new SqlParameter("@sup_code", itsr.code),
                                new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            // simsobj.im_assembly_ind = dr["im_assembly_ind"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getServiceSerch")]
        public HttpResponseMessage getServiceSerch(Invs058_itemNew itsr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                //new SqlParameter("@im_inv_no", itsr.im_inv_no),
                                //new SqlParameter("@sg_name", itsr.invs021_sg_name),
                                //new SqlParameter("@im_item_code", itsr.im_item_code),
                                //new SqlParameter("@im_desc", itsr.im_desc),
                                //new SqlParameter("@dep_code", itsr.dept_code),
                                //new SqlParameter("@sec_code", itsr.sec_code),
                                //new SqlParameter("@sup_code", itsr.invs021_sup_code),
                                //new SqlParameter("@im_assembly_ind", itsr.im_assembly_ind_s),
                                //new SqlParameter("@category_code", itsr.category_code),
                                //new SqlParameter("@subcategory_code", itsr.subcategory_code),

                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getServiceSearch")]
        public HttpResponseMessage getServiceSearch(string im_no, string des_ser, string dep_name)
        {

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@im_inv_no",im_no),
                                new SqlParameter("@im_desc", des_ser),
                                new SqlParameter("@dep_code",dep_name),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_sm_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate_old"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            //rd_date_required
                            //rd_remarks
                            simsobj.im_desc = dr["im_service_desc"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSearchRequest")]
        public HttpResponseMessage getSearchRequest(string sr, string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchRequest(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchRequest"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@req_mode", sr),
                                new SqlParameter("@user_code", user_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.req_status_name = dr["req_status_name"].ToString();
                            simsobj.req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_type_name = dr["req_type_name"].ToString();
                            simsobj.req_mode = dr["req_mode"].ToString();
                            simsobj.req_mode_name = dr["req_mode_name"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.sm_name = dr["sm_name"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getGridDataforEdit")]
        public HttpResponseMessage getGridDataforEdit(string req_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGridDataforEdit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getGridDataforEdit"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@req_no",req_no)
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.excg_curcy_code = dr["cur_code"].ToString();
                            simsobj.im_desc = dr["rd_item_desc"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.im_sell_price = dr["rd_estimated_price"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            simsobj.rd_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getRequestType")]
        public HttpResponseMessage getRequestType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_type_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_type_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestTypeNew")]
        public HttpResponseMessage getRequestTypeNew(string req_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Y'),
                            new SqlParameter("@req_type", req_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_mode_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_mode_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeparments")]
        public HttpResponseMessage getDeparments()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeparments()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeparments"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dept_code = dr["dep_code"].ToString();
                            simsobj.dept_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getDeparmentsUsercode")]
        public HttpResponseMessage getDeparmentsUsercode(string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeparments()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeparments"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@user_code", user_code),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dept_code = dr["dep_code"].ToString();
                            simsobj.dept_name = dr["dep_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getDeliveryType")]
        public HttpResponseMessage getDeliveryType(string rtype)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeliveryType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDeliveryType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@req_mode", rtype),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.dm_name = dr["dm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestStatus")]
        public HttpResponseMessage getRequestStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestStatus()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestStatus"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_status_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_status_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSalesmanType")]
        public HttpResponseMessage getSalesmanType(string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSalesmanType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getSalesmanType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@user_code", user_code),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.salesman_code = dr["sm_code"].ToString();
                            simsobj.salesman_name = dr["sm_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getCurrency")]
        public HttpResponseMessage getCurrency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrency()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCurrency"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.excg_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.excg_curcy_desc = dr["excg_curcy_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getuomdetails")]
        public HttpResponseMessage getuomdetails()
        {

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getItems")]
        public HttpResponseMessage getItems()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItems()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getItems"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'X'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDRequestDetails")]
        public HttpResponseMessage CUDRequestDetails(string data1, List<Invs058_item> data)
        {
            Message msg = new Message();
            string st = string.Empty;
            string sn = string.Empty;

            bool inserted = false;
            bool inserted712 = false;
            Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data1);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@req_no",obj.req_no),
                                      new SqlParameter("@req_mode", obj.request_mode_code),
                                      new SqlParameter("@req_date", db.DBYYYYMMDDformat(obj.req_date)),
                                      new SqlParameter("@req_type", obj.request_type_code),
                                      new SqlParameter("@dep_code", obj.dept_code),
                                      new SqlParameter("@dm_code", obj.dm_code),
                                      new SqlParameter("@req_status", obj.request_status_code),
                                      new SqlParameter("@sm_code", obj.salesman_code),
                                      new SqlParameter("@req_remarks", obj.req_remarks),
                                      new SqlParameter("@req_discount_pct",0),
                                      new SqlParameter("@req_agency_flag",'Y'),
                                 });

                    if (dr1.Read())
                    {
                        st = dr1["RequestNo"].ToString();
                    }

                    if (dr1.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr1.Close();


                    foreach (Invs058_item simsobj in data)
                    {
                        if (simsobj.excg_curcy_code != null)
                        {
                            //int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                            SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",

                               new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@rd_status",0),
                                new SqlParameter("@dep_code", obj.dept_code),
                                new SqlParameter("@req_mode", obj.request_mode_code),
                                new SqlParameter("@req_type", obj.request_type_code),
                                new SqlParameter("@req_no",st),//simsobj.req_no),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@cur_code",simsobj.excg_curcy_code),
                                new SqlParameter("@rd_estimated_price",simsobj.rd_estimate_price),
                                new SqlParameter("@rd_date_required", db.DBYYYYMMDDformat(simsobj.rd_date_required)),
                                new SqlParameter("@rd_quantity", simsobj.rd_quantity),
                                new SqlParameter("@rd_remarks", simsobj.rd_remarks),
                                new SqlParameter("@rd_item_desc",simsobj.im_desc)

                         });

                            if (dr.Read())
                            {
                                st = dr["RequestNo"].ToString();
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted712 = true;
                            }
                            dr.Close();
                            if (inserted712 == false)
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                                new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@req_mode", obj.request_mode_code),
                                    new SqlParameter("@req_type", obj.request_type_code),
                                    new SqlParameter("@req_no",st),
                                    new SqlParameter("@dep_code", obj.dept_code),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                            else
                            {
                                //sn = sn + st + '-' + simsobj.im_desc + "</br>";
                                //sn = "Request has been created successfully with request No:-" + st;
                                sn = st;
                            }
                        }

                        else
                        {
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                                new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@req_mode", obj.request_mode_code),
                                    new SqlParameter("@req_type", obj.request_type_code),
                                    new SqlParameter("@req_no",st),
                                    new SqlParameter("@dep_code", obj.dept_code),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }

                    if (sn != null)
                    {
                        msg.strMessage = sn;
                    }
                    else
                    {
                        msg.strMessage = "Request not created...";
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDRequestDetailsUpdate")]
        public HttpResponseMessage CUDRequestDetailsUpdate(string data2, List<Invs058_item> data1)
        {
            Message msg = new Message();
            string st = string.Empty;
            string sn = string.Empty;

            bool inserted = false;
            Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data2);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                       new List<SqlParameter>() 
                                 { 
                                      new SqlParameter("@opr",obj.opr),
                                      new SqlParameter("@req_no",obj.req_no),
                                      new SqlParameter("@req_mode", obj.req_type),
                                      new SqlParameter("@req_type", obj.req_mode),
                                      new SqlParameter("@dep_code", obj.dept_code),
                                      new SqlParameter("@dm_code", obj.dm_code),
                                      new SqlParameter("@req_status", obj.request_status_code),
                                      new SqlParameter("@sm_code", obj.salesman_code),
                                      new SqlParameter("@req_remarks", obj.req_remarks),
                                      new SqlParameter("@req_discount_pct",0),
                                      new SqlParameter("@req_agency_flag",'Y'),
                                 });
                    if (ins > 0)
                    {
                        inserted = true;

                    }

                    decimal request_no = 0;

                    foreach (Invs058_item simsobj in data1)
                    {
                        if (simsobj.req_no != 0)
                            request_no = simsobj.req_no;
                        if (simsobj.im_sell_price == "" || simsobj.im_sell_price == null)
                            simsobj.im_sell_price = "0";
                        if (simsobj.uom_code == "")
                            simsobj.uom_code = null;

                        if (simsobj.excg_curcy_code != null)
                        {
                            int ins1 = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",

                               new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@rd_status",simsobj.req_status),
                                new SqlParameter("@req_mode", obj.req_type),
                                new SqlParameter("@req_type", obj.req_mode),
                                //new SqlParameter("@req_no",simsobj.req_no),
                                new SqlParameter("@req_no",request_no),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@uom_code",simsobj.uom_code),
                                new SqlParameter("@cur_code",simsobj.excg_curcy_code),
                                new SqlParameter("@rd_estimated_price",simsobj.im_sell_price),
                                new SqlParameter("@rd_date_required", db.DBYYYYMMDDformat(simsobj.rd_date_required)),
                                new SqlParameter("@rd_quantity", simsobj.rd_quantity),
                                new SqlParameter("@rd_remarks", simsobj.rd_remarks),
                                new SqlParameter("@rd_item_desc",simsobj.im_desc)


                         });


                            if (ins1 > 0)
                            {
                                inserted = true;

                            }
                            else
                            {
                                inserted = false;
                            }

                        }
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("DelRequestDetails")]
        public HttpResponseMessage DelRequestDetails(List<Invs058_item> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDesignation(),PARAMETERS";
            bool inserted = false;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs058_item simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[invs].[invs_requests_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@req_no", simsobj.req_no),
                                  
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDSaveRequestDocs")]
        //public HttpResponseMessage CUDSaveRequestDocs(string compcode, string ord_number, string vouchertype, int doclinenum, string original_filename, string updatedfilename, string orddate, string filepath)
        public HttpResponseMessage CUDSaveRequestDocs(List<Invs058_item> data1)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsAsRead(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Invs058_item simsobj in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),                                
                                new SqlParameter("@vouchernum", simsobj.req_no),
                                new SqlParameter("@doclinenum", simsobj.im_inv_no),
                                new SqlParameter("@filename", simsobj.request_type_code),
                                new SqlParameter("@filename_en", simsobj.request_mode_code),
                                new SqlParameter("@filepath", simsobj.req_remarks),
                                //new SqlParameter("@vouchertype", vouchertype),
                                //new SqlParameter("@order_ord_date", orddate),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getDocDetails")]
        public HttpResponseMessage getDocDetails(string req_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSalesmanType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getSalesmanType"));

            List<Invs058_item> goal_list = new List<Invs058_item>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", '1'),
                            new SqlParameter("@req_no", req_no),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.salesman_code = dr["invs_ref_doc_line_no"].ToString();
                            simsobj.salesman_name = dr["invs_ref_doc_name"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        #endregion


        #endregion

        #region Service(API) For Arrove Request...

        [Route("getRequestDetailsforapprove")]
        public HttpResponseMessage getRequestDetailsforapprove(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                ItemSerive sf = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemSerive>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_requestsitemservice_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@req_no", sf.req_no),
                                new SqlParameter("@dep_code", sf.dept_code),
                                new SqlParameter("@rd_from_required", db.DBYYYYMMDDformat(sf.rd_from_required)),
                                new SqlParameter("@rd_up_required", sf.rd_up_required),
                                new SqlParameter("@rd_item_desc", sf.rd_item_desc),
                                new SqlParameter("@req_type", sf.request_mode_code),
                                new SqlParameter("@req_range_no", sf.req_range_no),
                                new SqlParameter("@sm_code",sf.sm_code),
                                new SqlParameter("@user_code",sf.username),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateRequestDetailsforapprove")]
        public HttpResponseMessage UpdateRequestDetailsforapprove(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_requestsitemservice_proc]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_requestsitemservice_proc]",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","F"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("UpdateRequestDetailsforrejection")]
        public HttpResponseMessage UpdateRequestDetailsforrejection(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "R";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("[invs].[invs_requestsitemservice_proc]", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[invs].[invs_requestsitemservice_proc]",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","A"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }
        
        /*
        [Route("getApproveReport")]
        public HttpResponseMessage getApproveReport()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItems()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getItems"));

            string invs_report_no = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requestsitemservice_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs_report_no = dr["invs_appl_parameter"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, invs_report_no);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, invs_report_no);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                invs_report_no = "No Records Found";
                return Request.CreateResponse(HttpStatusCode.InternalServerError, invs_report_no);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
        */

        [Route("getApproveReport")]
        public HttpResponseMessage getApproveReport(string Request_Type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItems()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getItems"));

            string invs_report_no = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requestsitemservice_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@req_type", Request_Type),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            invs_report_no = dr["invs_appl_parameter"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, invs_report_no);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, invs_report_no);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                invs_report_no = "No Records Found";
                return Request.CreateResponse(HttpStatusCode.InternalServerError, invs_report_no);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
        #endregion

        #region  Service(API) For Pending Request...
        [Route("getPendingRequest")]
        public HttpResponseMessage getPendingRequest(string sr, string user_code, string dept_code)
        {

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_pending_requests_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@req_mode", sr),
                                new SqlParameter("@user_code", user_code),
                                new SqlParameter("@dept_code", dept_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();//dr["rd_remarks"].ToString();
                            simsobj.rd_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                            simsobj.excg_curcy_desc = dr["cur_code"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        #endregion

        #region Service(API) For Pending Order...
        [Route("getPendingOrder")]
        public HttpResponseMessage getPendingOrder(string sr, string user_code, string dept_code)
        {

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_pending_requests_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'O'),
                                new SqlParameter("@req_mode", sr),
                                new SqlParameter("@user_code", user_code),
                                new SqlParameter("@dept_code", dept_code),
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["ord_no"].ToString());
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.sg_name = dr["sg_name"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            simsobj.category = dr["category"].ToString();
                            simsobj.subCategory = dr["subCategory"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.sec_code = dr["sec_code"].ToString();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.uom_code = dr["uom_code"].ToString();
                            simsobj.im_malc_rate = dr["im_malc_rate"].ToString();
                            simsobj.im_sell_price = dr["im_sell_price"].ToString();
                            simsobj.sg_desc = dr["sg_desc"].ToString();
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.uom_name = dr["uom_name"].ToString();
                            simsobj.sec_name = dr["sec_name"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            simsobj.curr_qty = dr["curr_qty"].ToString();
                            simsobj.do_qty = dr["do_qty"].ToString();
                            simsobj.ava_aty = dr["ava_aty"].ToString();
                            simsobj.rd_remarks = dr["rd_remarks"].ToString();//dr["rd_remarks"].ToString();
                            simsobj.rd_date_required = db.UIDDMMYYYYformat(dr["rd_date_required"].ToString());
                            simsobj.excg_curcy_desc = dr["cur_code"].ToString();
                            simsobj.rd_quantity = dr["rd_quantity"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        #endregion

        #region Request Details Peding And Order Details Pending...

        #region RPRINT...

        [Route("getRequestModeRP")]
        public HttpResponseMessage getRequestModeRP()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));
            List<Invs058_item> goal_list = new List<Invs058_item>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_approval_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_type_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_type_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getRequestStatusRP")]
        public HttpResponseMessage getRequestStatusRP()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRequestType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getRequestType"));
            List<Invs058_item> goal_list = new List<Invs058_item>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_approval_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.request_status_code = dr["invs_appl_parameter"].ToString();
                            simsobj.request_status_name = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getApprovalRequest")]
        public HttpResponseMessage getApprovalRequest(string subopr, string req_type, string req_mode, string req_status, string dept_code, string user_code, string from_date, string to_date, string transaction_no, string sup_code)
        {
            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if(from_date=="undefined")
                        from_date=null;
                    if (to_date == "undefined")
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_approval_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@subopr", subopr),
                                new SqlParameter("@req_type", req_type),
                                new SqlParameter("@req_mode", req_mode),
                                new SqlParameter("@req_status", req_status),
                                new SqlParameter("@dept_code", dept_code),
                                new SqlParameter("@user_code", user_code),
                                new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                                new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                                new SqlParameter("@req_no", transaction_no),
                                 new SqlParameter("@sup_code", sup_code),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            simsobj.dep_name = dr["dep_name"].ToString();
                            simsobj.req_date = db.UIDDMMYYYYformat(dr["req_date"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_mode = dr["req_mode"].ToString();
                            simsobj.dep_code = dr["dep_code"].ToString();
                            simsobj.dm_code = dr["dm_code"].ToString();
                            simsobj.req_agency_flag = dr["req_agency_flag"].ToString();
                            simsobj.req_discount_pct = Convert.ToDecimal(dr["req_discount_pct"].ToString());
                            simsobj.req_requester_dep = dr["req_requester_dep"].ToString();
                            simsobj.req_remarks = dr["req_remarks"].ToString();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.sm_code = dr["sm_code"].ToString();
                            simsobj.req_approved_by_user_code = dr["req_approved_by_user_code"].ToString();
                            simsobj.req_approved_date = db.UIDDMMYYYYformat(dr["req_approved_date"].ToString());
                            simsobj.request_status_name = dr["req_approval_status"].ToString();
                            simsobj.rd_estimate_price = dr["rd_estiated_price"].ToString();
                            try
                            {
                                simsobj.sup_name = dr["sup_name"].ToString();
                            }
                            catch (Exception s)
                            {

                                Log.Error(s);
                            }
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getTransactionNo")]
        public HttpResponseMessage getTransactionNo(string subopr, string req_type, string req_mode, string req_status, string dept_code, string user_code, string from_date, string to_date)
        {
            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined")
                        from_date = null;
                    if (to_date == "undefined")
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_approval_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@subopr", subopr),
                                new SqlParameter("@req_type", req_type),
                                new SqlParameter("@req_mode", req_mode),
                                new SqlParameter("@req_status", req_status),
                                new SqlParameter("@dept_code", dept_code),
                                new SqlParameter("@user_code", user_code),
                                new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                                new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                                
                             });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.req_no = Convert.ToDecimal(dr["req_no"].ToString());
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //getsuppliers
        //GetSuppliers
        [Route("GetSuppliers")]
        public HttpResponseMessage GetSuppliers(string subopr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetForwardAgent()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetDepartment"));

            List<Invs058_item> doc_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_approval_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A8"),
                            new SqlParameter("@subopr", subopr),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.sup_code = dr["sup_code"].ToString();
                            simsobj.sup_name = dr["sup_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        #endregion


        #endregion

        #region Item Valuation API...

        [Route("getitemsdetails")]
        public HttpResponseMessage getitemsdetails(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getItemSerch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getItemSerch"));

            List<Invs058_item> goaltarget_list = new List<Invs058_item>();
            Invs058_item itsr = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'F'),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.im_inv_no = dr["im_inv_no"].ToString();
                            simsobj.im_item_code = dr["im_item_code"].ToString();
                            simsobj.im_desc = dr["im_desc"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getitemsvaluationdetails")]
        public HttpResponseMessage getitemsvaluationdetails(string data, string data1)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Invs058_item itsr = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data);

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[invs].[invs_itemsearch_query_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@dep_code", itsr.dept_code),
                                new SqlParameter("@category_code", itsr.category_code),
                                new SqlParameter("@subcategory_code", itsr.subcategory_code),
                                new SqlParameter("@im_item_code", data1),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        [Route("getReportName")]
        public HttpResponseMessage getReportName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getReportName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getReportName"));

            List<Invs058_item> goal_list = new List<Invs058_item>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_requests_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", '4')
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Invs058_item simsobj = new Invs058_item();
                            simsobj.invs_appl_form_field = dr["invs_appl_form_field"].ToString();
                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString(); ;
                            goal_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

    }
}