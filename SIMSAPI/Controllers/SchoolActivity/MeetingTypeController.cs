﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
//using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.SchoolActivity
{
    [RoutePrefix("api/MeetingType")]
    public class MeetingTypeController : ApiController
    {
        [Route("GetAllMeetingType")]
        public HttpResponseMessage GetAllMeetingType()
        {
            List<minutesOfMeetingType> mod_list = new List<minutesOfMeetingType>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_meeting_type_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S")

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            minutesOfMeetingType simsobj = new minutesOfMeetingType();                                                        
                            simsobj.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            simsobj.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            simsobj.sims_mom_type_status = dr["sims_mom_type_status"].ToString().Equals("A") ? true : false;                            
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDMeetingType")]
        public HttpResponseMessage CURDMeetingType(List<minutesOfMeetingType> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (minutesOfMeetingType obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_meeting_type_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_mom_type_code", obj.sims_mom_type_code),
                                new SqlParameter("@sims_mom_type_desc_en", obj.sims_mom_type_desc_en),                                
                                new SqlParameter("@sims_mom_type_status", obj.sims_mom_type_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

    }
}