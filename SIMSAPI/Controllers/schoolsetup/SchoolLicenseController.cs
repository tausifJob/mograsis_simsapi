﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.SchoolLicenseController
{
    [RoutePrefix("api/SchoolLicense")]
    public class SchoolLicenseController : ApiController
    {

        static string lic_school_code = "";
        static string lic_grace_30 = "";
        static string lic_grace_60 = "";
        static string lic_grace_90 = "";



        [Route("GetSchoolDetails")]
        public HttpResponseMessage GetSchoolDetails()
        {
            //LicenseDetails License = new LicenseDetails();
            Message message = new Message();
            License obj = new License();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc", new List<SqlParameter>() { new SqlParameter("@opr", "S") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lic_school_code = dr["lic_school_code"].ToString();
                            obj.lic_agreement_date = db.UIDDMMYYYYformat(dr["lic_agreement_date"].ToString());
                            obj.lic_contact_person = dr["lic_contact_person"].ToString();
                            obj.lic_email = dr["lic_email"].ToString();
                            obj.lic_fax_no = dr["lic_fax_no"].ToString();
                            obj.lic_product_amt = dr["lic_product_amt"].ToString();
                            obj.lic_product_key = dr["lic_product_key"].ToString();
                            obj.lic_product_mem_qty = dr["lic_product_mem_qty"].ToString();
                            obj.lic_product_mem_qty_prorata = dr["lic_product_mem_qty_prorata"].ToString();
                            obj.lic_product_name = dr["lic_product_name"].ToString();
                            obj.lic_product_type = dr["lic_product_type"].ToString();
                            obj.lic_school_address = dr["lic_school_address"].ToString();
                            obj.lic_school_country = dr["lic_school_country"].ToString();
                            obj.lic_school_lms_link = dr["lic_school_lms_link"].ToString();
                            obj.lic_school_name = dr["lic_school_name"].ToString();
                            obj.lic_school_short_name = dr["lic_school_short_name"].ToString();
                            obj.lic_school_other_name = dr["lic_school_other_name"].ToString();
                            obj.lic_school_region = dr["lic_school_region"].ToString();
                            obj.lic_subscription_expiry_date = db.UIDDMMYYYYformat(dr["lic_subscription_expiry_date"].ToString());
                            obj.lic_website_url = dr["lic_website_url"].ToString();
                            obj.stringlic_subscription_date = db.UIDDMMYYYYformat(dr["lic_subscription_date"].ToString());
                            obj.lic_tel_no = dr["lic_tel_no"].ToString();
                            obj.lic_school_curriculum = dr["lic_school_curriculum"].ToString();
                            obj.lic_status = dr["lic_status"].Equals("A") ? true : false;
                            obj.lic_school_logo = dr["lic_school_logo"].ToString();
                            obj.lic_lat = dr["lic_lat"].ToString();
                            obj.lic_long = dr["lic_long"].ToString();
                            obj.lic_time_zone = dr["lic_time_zone"].ToString();
                            obj.lic_school_type = dr["lic_school_type"].ToString();
                            obj.lic_school_type_name = dr["lic_school_type_name"].ToString();
                            //   obj.lic_subscription_expiry_old_date = dr["lic_subscription_expiry_old_date"].ToString();
                            // obj.lic_grace_30 = dr["lic_grace_30"].ToString();
                            //  obj.lic_grace_60 = dr["lic_grace_60"].ToString();
                            //    obj.lic_grace_90 = dr["lic_grace_90"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, message);

        }

        [Route("UpdateSchoolDetails")]
        public HttpResponseMessage UpdateSchoolDetails(List<License> data)
        {
            //LicenseDetails License = new LicenseDetails();
            Message message = new Message();
            License obj = new License();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var da in data)
                    {
                        //License da = Newtonsoft.Json.JsonConvert.DeserializeObject<License>(data);
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc", new List<SqlParameter>()  {
                                new SqlParameter("@opr",da.opr),
                                  new SqlParameter("@lic_school_code",lic_school_code),
                                   new SqlParameter("@lic_grace_30",da.lic_grace_30),
                                  new SqlParameter("@lic_grace_60",da.lic_grace_60),
                                  new SqlParameter("@lic_grace_90",da.lic_grace_90),

                                  new SqlParameter("@lic_agreement_date" , db.DBYYYYMMDDformat(da.lic_agreement_date)),
                                  new SqlParameter("@lic_subscription_date", db.DBYYYYMMDDformat(da.stringlic_subscription_date)), 
                                  new SqlParameter("@lic_website_url",da.lic_website_url),
                                  new SqlParameter("@lic_tel_no",da.lic_tel_no),
                                  new SqlParameter("@lic_subscription_expiry_date", db.DBYYYYMMDDformat(da.lic_subscription_expiry_date)),
                                  new SqlParameter("@lic_school_region_name",da.lic_school_region),
                                  new SqlParameter("@lic_school_name",da.lic_school_name),
                                  new SqlParameter("@lic_school_short_name",da.lic_school_short_name),
                                  new SqlParameter("@lic_school_other_name",da.lic_school_other_name),
                                  new SqlParameter("@lic_school_logo",da.lic_school_logo),
                                  new SqlParameter("@lic_school_lms_link",da.lic_school_lms_link),
                                  new SqlParameter("@lic_school_country_name",da.lic_school_country),
                                  new SqlParameter("@lic_school_curriculum_name",da.lic_school_curriculum),
                                  new SqlParameter("@lic_school_address",da.lic_school_address),
                                  new SqlParameter("@lic_product_type",da.lic_product_type),
                                  new SqlParameter("@lic_product_name",da.lic_product_name),
                                  new SqlParameter("@lic_product_mem_qty_prorata",da.lic_product_mem_qty_prorata),
                                  new SqlParameter("@lic_product_mem_qty",da.lic_product_mem_qty),
                                  new SqlParameter("@lic_product_key",da.lic_product_key),
                                  new SqlParameter("@lic_product_amt",da.lic_product_amt),
                                  new SqlParameter("@lic_fax_no",da.lic_fax_no),
                                  new SqlParameter("@lic_email",da.lic_email),
                                  new SqlParameter("@lic_lat",da.lic_lat),
                                  new SqlParameter("@lic_long",da.lic_long),
                                  new SqlParameter("@lic_time_zone",da.lic_time_zone),
                                  new SqlParameter("@lic_subscription_expiry_old_date", db.DBYYYYMMDDformat(da.lic_subscription_expiry_old_date)),
                                 
                                  new SqlParameter("@lic_contact_person",da.lic_contact_person),
                                  new SqlParameter("@lic_status",da.lic_status.Equals(true)?"A":"I"),
                                  new SqlParameter("@lic_school_type",da.lic_school_type),

                   });
                        if (dr1.RecordsAffected > 0)
                        {
                            message.strMessage = "School License Updated Sucessfully";
                        }
                        else
                        {
                            message.strMessage = "School License Not Updated";
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }



            return Request.CreateResponse(HttpStatusCode.OK, message);



        }

        [Route("GetAllCountries")]
        public HttpResponseMessage GetAllCountries(string region_code)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Country> result = new List<Country>();
            lst.Add(new SqlParameter("@opr", "S"));
            lst.Add(new SqlParameter("@sims_region_name", region_code));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims_country", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Country c = new Country();
                        c.sims_country_code = dr["sims_country_code"].ToString();
                        c.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        result.Add(c);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Route("GetAllRegions")]
        public HttpResponseMessage GetAllRegions()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Region> result = new List<Region>();

            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims_region", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Region c = new Region();
                        c.Sims_Region_code = dr["sims_region_code"].ToString();
                        c.Sims_Region_name_en = dr["sims_region_name_en"].ToString();
                        result.Add(c);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Route("GetAllCurr")]
        public HttpResponseMessage GetAllCurr()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Curr> result = new List<Curr>();
            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Curr c = new Curr();
                        c.curriculum_code = dr["sims_cur_code"].ToString();
                        c.curriculum_short_name = dr["sims_cur_short_name_en"].ToString();
                        c.curriculum_short_name_ar = dr["sims_cur_short_name_ar"].ToString();
                        c.curriculum_short_name_fr = dr["sims_cur_short_name_fr"].ToString();
                        c.curriculum_short_name_ot = dr["sims_cur_short_name_ot"].ToString();
                        c.curriculum_full_name = dr["sims_cur_full_name_en"].ToString();
                        c.curriculum_full_name_ar = dr["sims_cur_full_name_ar"].ToString();
                        c.curriculum_full_name_fr = dr["sims_cur_full_name_fr"].ToString();
                        c.curriculum_full_name_ot = dr["sims_cur_full_name_ot"].ToString();
                        c.curriculum_status = dr["sims_cur_status"].ToString() != "I";
                        result.Add(c);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Route("GetOldDate1")]
        public HttpResponseMessage GetOldDate1()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<License> result = new List<License>();

            lst.Add(new SqlParameter("@opr", "V"));
            lst.Add(new SqlParameter("@lic_school_code", "01"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        License c = new License();
                        c.lic_subscription_expiry_date = dr["lic_subscription_expiry_date"].ToString();
                        result.Add(c);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }


        [Route("GetExpTime")]
        public HttpResponseMessage GetExpTime()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<License> result = new List<License>();

            lst.Add(new SqlParameter("@opr", "T"));

            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        License c = new License();
                        c.expirytime = dr["expirytime"].ToString();
                        result.Add(c);

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }



        [Route("GetschoolType")]
        public HttpResponseMessage GetschoolType()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<License> result = new List<License>();

            lst.Add(new SqlParameter("@opr", "L"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        License obj = new License();

                        obj.lic_school_type = dr["sims_appl_parameter"].ToString();
                        obj.lic_school_type_name = dr["sims_appl_form_field_value1"].ToString();
                        result.Add(obj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }
    }
}