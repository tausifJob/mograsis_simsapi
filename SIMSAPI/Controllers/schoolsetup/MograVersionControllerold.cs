﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.MograVersion
{
    [RoutePrefix("api/MograVersion")]
    public class MograVersionController : ApiController
    {
        [Route("mograCommon")]
        public HttpResponseMessage mograCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_MograVersion]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }
        [Route("SaveTeacherPreference")]
        public HttpResponseMessage SaveTeacherPreference(List<mograObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   

                    foreach (mograObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_MograVersion]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@cs_no",obj.cs_no),
                                new SqlParameter("@mod_name",obj.mod_name),
                                new SqlParameter("@app_name",obj.app_name),
                                new SqlParameter("@document",obj.document),
                                new SqlParameter("@testcase",obj.testcase),
                                new SqlParameter("@remark",obj.remark),
                                new SqlParameter("@SP",obj.SP),
                                new SqlParameter("@SP_remark",obj.SP_remark),
                                 new SqlParameter("@aprove_status",obj.aprove_status),
                                 new SqlParameter("@Pr_name",obj.Pr_name),
                                new SqlParameter("@developer_name",obj.developer_name),
                                new SqlParameter("@date",obj.date),
                                new SqlParameter("@dep_status",obj.dep_status),
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

    }
}