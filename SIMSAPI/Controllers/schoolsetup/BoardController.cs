﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/BoardDetail")]
    public class BoardController : ApiController
    {

        [Route("getBoardDetail")]
        public HttpResponseMessage getBoardDetail()
        {
            List<Sims183> Board = new List<Sims183>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims183 obj = new Sims183();


                            obj.sims_board_code = dr["sims_board_code"].ToString();
                            obj.sims_board_short_name = dr["sims_board_short_name"].ToString();

                            obj.sims_board_name = dr["sims_board_name"].ToString();
                            obj.sims_board_address = dr["sims_board_address"].ToString();
                            obj.sims_board_contact_no = dr["sims_board_contact_no"].ToString();

                            if (dr["sims_board_status"].ToString() == "A")
                                obj.sims_board_status = true;
                            else
                                obj.sims_board_status = false;
                            Board.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Board);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Board);
        }

        [Route("BoardCUD")]
        public HttpResponseMessage BoardCUD(List<Sims183> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims183 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_board_proc]",
                            new List<SqlParameter>()
             
                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_board_code", simsobj.sims_board_code),
                                 new SqlParameter("@sims_board_short_name", simsobj.sims_board_short_name),
                                 new SqlParameter("@sims_board_name", simsobj.sims_board_name),
                                 new SqlParameter("@sims_board_address", simsobj.sims_board_address),
                                 new SqlParameter("@sims_board_contact_no", simsobj.sims_board_contact_no),
                                 new SqlParameter("@sims_board_status", simsobj.sims_board_status==true?"A":"I")

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}