﻿using SIMSAPI.Helper;
using SIMSAPI.Models;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using log4net;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Term
{
    [RoutePrefix("api/term")]
    public class TermController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Comn004> lstModules = new List<Comn004>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_academic_year"),
                            new SqlParameter("@tbl_col_name1", "*"),
                            new SqlParameter("@tbl_cond", "sims_academic_year_status='c' AND sims_cur_code='"+ curCode +"'")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 sequence = new Comn004();
                            sequence.comm_cur_code = dr["sims_cur_code"].ToString();
                            sequence.comn_academic_year = dr["sims_academic_year"].ToString();
                            sequence.comn_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        //[Route("getTermByIndex")]
        //public HttpResponseMessage getTermByIndex()
        //{
        //    List<Sims_Term> result = new List<Sims_Term>();

        //    int cnt=1;
        //    List<SqlParameter> lst = new List<SqlParameter>();
        //    HttpStatusCode msg;
        //    string date = "", date1 = "";
        //    lst.Add(new SqlParameter("@opr", "S"));
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_proc]", lst);
        //            while (dr.Read())
        //            {
        //                Sims_Term c = new Sims_Term();
        //                c.myid = cnt;
        //                c.sims_cur_code = dr["sims_cur_code"].ToString();
        //                c.sims_academic_year = dr["sims_academic_year"].ToString();
        //                c.sims_term_code = dr["sims_term_code"].ToString();
        //                c.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
        //                c.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
        //                c.sims_term_desc_fr = dr["sims_term_desc_fr"].ToString();
        //                c.sims_term_desc_ot = dr["sims_term_desc_ot"].ToString();
        //                if(!string.IsNullOrEmpty(dr["sims_term_start_date"].ToString()))
        //                {
        //                 date= DateTime.Parse(dr["sims_term_start_date"].ToString()).Year.ToString()+'/'+DateTime.Parse(dr["sims_term_start_date"].ToString()).Month.ToString()+'/'+DateTime.Parse(dr["sims_term_start_date"].ToString()).Day.ToString();
        //                 c.sims_term_start_date = date;
        //                }
        //                if (!string.IsNullOrEmpty(dr["sims_term_end_date"].ToString()))
        //                {
        //                 date1 = DateTime.Parse(dr["sims_term_end_date"].ToString()).Year.ToString() + '/' + DateTime.Parse(dr["sims_term_end_date"].ToString()).Month.ToString() + '/' + DateTime.Parse(dr["sims_term_end_date"].ToString()).Day.ToString();
        //                 c.sims_term_end_date = date1;
        //                }
        //               // c.sims_term_start_date =DateTime.Parse(dr["sims_term_start_date"].ToString()).ToShortDateString();
        //                //c.sims_term_end_date = DateTime.Parse(dr["sims_term_end_date"].ToString()).ToShortDateString();
        //                c.sims_cur_name = dr["sims_cur_name"].ToString();
        //                c.sims_term_status = dr["sims_term_status"].ToString() == "A";
        //                c.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
        //                result.Add(c);
        //               // total = result.Count;
        //               // skip = PageSize * (PageIndex - 1);
        //                cnt++;
        //            }
        //            msg = HttpStatusCode.OK;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        msg = HttpStatusCode.InternalServerError;
        //    }
        //    return Request.CreateResponse(msg, result);
        //}

        [Route("getTermByIndex")]
        public HttpResponseMessage getTermByIndex()
        {
            List<Sims_Term> result = new List<Sims_Term>();

            int cnt = 1;
            List<SqlParameter> lst = new List<SqlParameter>();
            HttpStatusCode msg;
            string date = "", date1 = "";
            lst.Add(new SqlParameter("@opr", "S"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_proc]", lst);
                    while (dr.Read())
                    {
                        Sims_Term c = new Sims_Term();
                        c.myid = cnt;
                        c.sims_cur_code = dr["sims_cur_code"].ToString();
                        c.sims_cur_name = dr["sims_cur_name"].ToString();
                        c.sims_academic_year = dr["sims_academic_year"].ToString();
                        c.sims_term_code = dr["sims_term_code"].ToString();
                        c.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                        c.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                        c.sims_term_desc_fr = dr["sims_term_desc_fr"].ToString();
                        c.sims_term_desc_ot = dr["sims_term_desc_ot"].ToString();
                        if (!string.IsNullOrEmpty(dr["sims_term_start_date"].ToString()))
                        {
                            //date = DateTime.Parse(dr["sims_term_start_date"].ToString()).Year.ToString() + '/' + DateTime.Parse(dr["sims_term_start_date"].ToString()).Month.ToString() + '/' + DateTime.Parse(dr["sims_term_start_date"].ToString()).Day.ToString();
                            //c.sims_term_start_date = date;
                            c.sims_term_start_date =db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                        }

                        if (!string.IsNullOrEmpty(dr["sims_term_end_date"].ToString()))
                        {
                            //date1 = DateTime.Parse(dr["sims_term_end_date"].ToString()).Year.ToString() + '/' + DateTime.Parse(dr["sims_term_end_date"].ToString()).Month.ToString() + '/' + DateTime.Parse(dr["sims_term_end_date"].ToString()).Day.ToString();
                            //c.sims_term_end_date = date1;

                            c.sims_term_end_date =db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                        }

                        // 
                        //c.sims_term_end_date = DateTime.Parse(dr["sims_term_end_date"].ToString()).ToShortDateString();
                        c.sims_cur_name = dr["sims_cur_name"].ToString();
                        c.sims_term_status = dr["sims_term_status"].ToString() == "A";
                        c.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                        result.Add(c);
                        // total = result.Count;
                        // skip = PageSize * (PageIndex - 1);
                        cnt++;
                    }
                    msg = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                msg = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(msg, result);
        }


        [Route("getAllTerms")]
        public HttpResponseMessage GetAllTerms(string cur, string AY)
        {
            List<Sims_Term> result = new List<Sims_Term>();
            List<SqlParameter> lst = new List<SqlParameter>();
            HttpStatusCode msg;
            lst.Add(new SqlParameter("@opr", "S"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_term", lst);
                    while (dr.Read())
                    {
                        Sims_Term c = new Sims_Term();
                        c.sims_cur_code = dr["sims_cur_code"].ToString();
                        c.sims_academic_year = dr["sims_academic_year"].ToString();
                        c.sims_term_code = dr["sims_term_code"].ToString();
                        c.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                        c.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                        c.sims_term_desc_fr = dr["sims_term_desc_fr"].ToString();
                        c.sims_term_desc_ot = dr["sims_term_desc_ot"].ToString();
                        c.sims_term_start_date = db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                        c.sims_term_end_date = db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                        c.sims_term_status = dr["sims_term_status"].ToString() == "A";
                        c.sims_cur_name = dr["sims_cur_name"].ToString();
                        c.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                        result.Add(c);
                    }
                    msg = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                msg = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(msg, result);
        }

        [Route("CUDTerm")]
        public HttpResponseMessage CUDTerm(List<Sims_Term> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDTerm()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Term", "SCHOOLSETUP"));
            bool inserted = false;
            //  Sims024 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims024>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims_Term simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_term_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_term_code", simsobj.sims_term_code),
                            new SqlParameter("@sims_term_desc_en", simsobj.sims_term_desc_en),
                            new SqlParameter("@sims_term_desc_ar", simsobj.sims_term_desc_ar),
                            new SqlParameter("@sims_term_desc_fr", simsobj.sims_term_desc_fr),
                            new SqlParameter("@sims_term_desc_ot", simsobj.sims_term_desc_ot),
                            new SqlParameter("sims_term_status", simsobj.sims_term_status == true?"A":"I"),
                            //if (!string.IsNullOrEmpty(simsobj.sims_term_start_date))
                            new SqlParameter("@sims_term_start_date",db.DBYYYYMMDDformat(simsobj.sims_term_start_date)),
                           // if (!string.IsNullOrEmpty(simsobj.sims_term_end_date))
                            new SqlParameter("@sims_term_end_date",db.DBYYYYMMDDformat(simsobj.sims_term_end_date)),

                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDInsertTerm")]
        public HttpResponseMessage CUDInsertTerm(List<Sims_Term> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDTerm()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Term", "SCHOOLSETUP"));
            bool inserted = false;
            //  Sims024 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims024>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims_Term simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_term_code", simsobj.sims_term_code),
                            new SqlParameter("@sims_term_desc_en", simsobj.sims_term_desc_en),
                            new SqlParameter("@sims_term_desc_ar", simsobj.sims_term_desc_ar),
                            new SqlParameter("@sims_term_desc_fr", simsobj.sims_term_desc_fr),
                            new SqlParameter("@sims_term_desc_ot", simsobj.sims_term_desc_ot),
                            new SqlParameter("sims_term_status", simsobj.sims_term_status == true?"A":"I"),
                            //if (!string.IsNullOrEmpty(simsobj.sims_term_start_date))
                            new SqlParameter("@sims_term_start_date",db.DBYYYYMMDDformat(simsobj.sims_term_start_date)),
                           // if (!string.IsNullOrEmpty(simsobj.sims_term_end_date))
                            new SqlParameter("@sims_term_end_date",db.DBYYYYMMDDformat(simsobj.sims_term_end_date)),

                         });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getSectionTermSubject")]
        public HttpResponseMessage getSectionTermSubject(string curcode, string gradecode, string academicyear, string sectioncode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionNamesWithSubjects(),PARAMETERS :: CURCODE{2},GRADECODE{3},ACAYEAR{4}";
            Log.Debug(string.Format(debug, "STUDENT", "getSectionNamesWithSubjects", curcode, gradecode, academicyear));

            List<SecTermsubject> subject_list1 = new List<SecTermsubject>();

            try
            {

                using (DBConnection db11 = new DBConnection())
                {
                    db11.Open();

                    SqlDataReader dr11 = db11.ExecuteStoreProcedure("[sims].[sims_section_term_mapping_proc]", new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@sims_cur_code", curcode),
                                    new SqlParameter("@sims_grade_code", gradecode),
                                    new SqlParameter("@sims_academic_year", academicyear),
                                    new SqlParameter("@sims_section_code", sectioncode),
                                });
                    if (dr11.HasRows)
                    {
                        while (dr11.Read())
                        {
                            SecTermsubject simsobj1 = new SecTermsubject();
                            simsobj1.sims_cur_code= dr11["sims_cur_code"].ToString();
                            simsobj1.sims_academic_year= dr11["sims_academic_year"].ToString();
				            simsobj1.sims_grade_code= dr11["sims_grade_code"].ToString();
				            simsobj1.sims_section_code= dr11["sims_section_code"].ToString();
				            simsobj1.sims_subject_code= dr11["sims_subject_code"].ToString();
                            simsobj1.sims_subject_group_code = dr11["sims_subject_group_code"].ToString();
                            simsobj1.sims_section_subject_term_code = dr11["sims_section_subject_term_code"].ToString();
                            simsobj1.sims_section_subject_term_status = dr11["sims_section_subject_term_status"].Equals("Y") ? true : false;
							
                            simsobj1.sims_section_subject_term_dependency_status = dr11["sims_section_subject_term_dependency_status"].Equals("Y") ? true : false;
							simsobj1.sims_subject_main_subject_code= dr11["sims_subject_main_subject_code"].ToString();
                            simsobj1.sims_subject_group_dependent_subject_group_code = dr11["sims_subject_group_dependent_subject_group_code"].ToString();
                            simsobj1.sims_section_subject_term_dependency_code = dr11["sims_section_subject_term_dependency_code"].ToString();
                            simsobj1.sims_subject_dependent_subject_code = dr11["sims_subject_dependent_subject_code"].ToString();

                            string subject_code = dr11["sims_subject_code"].ToString();
				            simsobj1.sims_subject_name_en= dr11["sims_subject_name_en"].ToString();
                            SecTerm s = new SecTerm();
                            s.sims_subject_status = dr11["sims_section_subject_term_status"].Equals("Y") ? true : false;
                            s.sims_section_subject_term_dependency_status = dr11["sims_section_subject_term_dependency_status"].Equals("Y") ? true : false;
                            s.sims_display_order = dr11["sims_display_order"].ToString();
                            s.sims_display_report_order = dr11["sims_display_report_order"].ToString();
                            s.sims_section_subject_credit_hours = dr11["sims_section_subject_credit_hours"].ToString();
				            s.sims_term_code= dr11["sims_term_code"].ToString();
                            s.sims_term_desc_en = dr11["sims_term_desc_en"].ToString();
                            simsobj1.term = new List<SecTerm>();
                            var v = (from p in subject_list1 where p.sims_subject_code == subject_code select p);
                            if (v.Count() == 0)
                            {
                                simsobj1.term.Add(s);
                                subject_list1.Add(simsobj1);
                            }
                            else
                            {
                                v.ElementAt(0).term.Add(s);
                            }
                             
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, subject_list1);

                    }

                }


            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subject_list1);
            }

            return Request.CreateResponse(HttpStatusCode.OK, subject_list1);

        }

        [Route("getTermMapping")]
        public HttpResponseMessage getTermMapping(string cur_code, string academic_year)
        {
            List<SecTermsubject> lstCuriculum = new List<SecTermsubject>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_term_mapping_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "T"),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SecTermsubject sequence = new SecTermsubject();


                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            sequence.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("CUDsection_subject_term")]
        public HttpResponseMessage CUDsection_subject_term(List<SecTermsubject> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SecTermsubject simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_term_mapping_proc]",

                        new List<SqlParameter>()
                     {

                                 new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@sims_subject_group_code", simsobj.sims_subject_group_code),
                                new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                                 new SqlParameter("@sims_section_subject_term_code",simsobj.sims_section_subject_term_code),
                                new SqlParameter("@sims_display_report_order", simsobj.sims_display_report_order),
                                new SqlParameter("@sims_section_subject_credit_hours", simsobj.sims_section_subject_credit_hours),
                                new SqlParameter("@sims_section_subject_term_status", simsobj.sims_section_subject_term_status==true?"Y":"N"),

                              
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("CUDsection_subject_term_dependent")]
        public HttpResponseMessage CUDsection_subject_term_dependent(List<SecTermsubject> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SecTermsubject simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_section_term_mapping_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_section_subject_term_dependency_code",simsobj.sims_section_subject_term_dependency_code),
                                new SqlParameter("@sims_section_subject_term_code",simsobj.sims_section_subject_term_code),
                                new SqlParameter("@sims_subject_main_subject_code",simsobj.sims_subject_main_subject_code),
                                new SqlParameter("@sims_subject_group_main_subject_group_code",simsobj.sims_subject_group_main_subject_group_code),
                                new SqlParameter("@sims_subject_dependent_subject_code",simsobj.sims_subject_dependent_subject_code),
                                new SqlParameter("@sims_subject_group_dependent_subject_group_code",simsobj.sims_subject_group_dependent_subject_group_code),
                                new SqlParameter("@sims_section_subject_term_dependency_status",simsobj.sims_section_subject_term_dependency_status==true?"Y":"N"),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            //insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getdependentMapping_sub")]
        public HttpResponseMessage getdependentMapping_sub(string cur_code, string academic_year,string term_old, string sub_code_old)
        {
            List<SecTermsubject> lstCuriculum = new List<SecTermsubject>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_term_mapping_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "Q"),
                             new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),
                             new SqlParameter("@sims_section_subject_term_code", term_old),
                             new SqlParameter("@sims_subject_main_subject_code", sub_code_old),
                            


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SecTermsubject sequence = new SecTermsubject();

                            sequence.sims_subject_dependent_subject_code = dr["sims_subject_dependent_subject_code"].ToString();
                            sequence.sims_section_subject_term_dependency_code = dr["sims_section_subject_term_dependency_code"].ToString();
                            sequence.sims_section_subject_term_dependency_status = dr["sims_section_subject_term_dependency_status"].Equals("Y") ? true : false;
                           
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

    }
}