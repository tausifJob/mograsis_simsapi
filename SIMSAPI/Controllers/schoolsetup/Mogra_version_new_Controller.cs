﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/MograVersionDetailsNew")]
    public class MograVersionDetails_New : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        /* MOgra Version*/


        [Route("GetProjectNames")]
        public HttpResponseMessage GetProjectNames()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "PN") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_project_code = dr["sims_appl_parameter"].ToString();
                            pn.lic_project_name = dr["sims_appl_form_field_value1"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetProjectModulesApplications")]
        public HttpResponseMessage GetProjectModulesApplications()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "CM") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_mod_code = dr["comn_mod_code"].ToString();
                            pn.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                new List<SqlParameter>() { new SqlParameter("@opr", "CMA") ,
                                new SqlParameter("@appl_mod_code", pn.comn_mod_code)});
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_version_applications ao = new Mogra_version_applications();
                                        ao.comn_appl_code = dr1["comn_appl_code"].ToString();
                                        ao.comn_appl_name = dr1["comn_appl_name_en"].ToString();
                                        ao.comn_mod_code = dr["comn_mod_code"].ToString();
                                        ao.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                                        pn.comn_applications.Add(ao);
                                    }
                                }
                                dr1.Close();
                                dr1.Dispose();
                            }
                            lst.Add(pn);
                        }
                        dr.Close();
                        dr.Dispose();
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("Getdevelopers")]
        public HttpResponseMessage Getdevelopers()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "DN") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_developed_by = dr["lic_developed_by"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("GetMograVersionsDetails")]
        public HttpResponseMessage GetMograVersionsDetails(string project_code)
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "MV"),
                        new SqlParameter("@lic_project_code", project_code),
                        
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_changeset_no = dr["lic_changeset_no"].ToString();
                            pn.lic_project_code = dr["lic_project_code"].ToString();
                            pn.lic_project_name = dr["lic_project_name"].ToString();
                            pn.lic_developed_by = dr["lic_developed_by"].ToString();
                            pn.lic_developed_date = dr["lic_developed_date"].ToString();
                            pn.lic_approved_by = dr["lic_approved_by"].ToString();
                            pn.lic_deployed_by = dr["lic_deployed_by"].ToString();
                            pn.lic_deployed_date = dr["lic_deployed_date"].ToString();
                            pn.lic_deployed_status = dr["lic_deployed_status"].ToString();
                            pn.lic_creation_date = dr["lic_creation_date"].ToString();
                            pn.lic_approved_date = dr["lic_approved_date"].ToString();
                            pn.lic_version_number = dr["lic_version_number"].ToString();
                            pn.lic_release_number = dr["lic_release_number"].ToString();
                            pn.lic_patch_number = dr["lic_patch_number"].ToString();
                            if (pn.mogra_details == null)
                                pn.mogra_details = new List<Mogra_Version_Details>();

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                new List<SqlParameter>() {
                                        new SqlParameter("@opr", "MVS"),
                                        new SqlParameter("@lic_changeset_no", pn.lic_changeset_no),
                                });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_Version_Details vd = new Mogra_Version_Details();
                                        vd.lic_changeset_no = pn.lic_changeset_no;
                                        vd.lic_school_code = dr1["lic_school_code"].ToString();
                                        vd.lic_school_name = dr1["lic_school_name"].ToString();
                                        vd.lic_version_date = dr1["lic_version_date"].ToString();
                                        vd.lic_version_number = dr1["lic_version_number"].ToString();
                                        vd.lic_release_number = dr1["lic_release_number"].ToString();
                                        vd.lic_patch_number = dr1["lic_patch_number"].ToString();
                                        vd.lic_notes = dr1["lic_notes"].ToString().Equals("A") ? true : false;
                                        vd.lic_version_status = dr1["lic_version_status"].ToString().Equals("A") ? true : false;
                                        vd.lic_test_case = dr1["lic_test_case"].ToString().Equals("A") ? true : false;
                                        vd.lic_document = dr1["lic_document"].ToString().Equals("A") ? true : false;
                                        vd.lic_remark = dr1["lic_remark"].ToString();
                                        vd.lic_sp_name = dr1["lic_sp_name"].ToString();
                                        vd.lic_sp_remark = dr1["lic_sp_remark"].ToString();
                                        vd.lic_approve_by = dr1["lic_approve_by"].ToString();
                                        vd.lic_approve_date = dr1["lic_approve_date"].ToString();
                                        vd.lic_deployed = dr1["lic_deployed"].ToString();
                                        vd.lic_appl_code = dr1["lic_appl_code"].ToString();
                                        vd.comn_appl_name = dr1["comn_appl_name_en"].ToString();
                                        vd.comn_mod_name_e = dr1["comn_mod_name_en"].ToString();
                                        vd.comn_mod_code = dr1["comn_mod_code"].ToString();
                                        vd.lic_changeset_no = dr1["lic_changeset_no"].ToString();
                                        vd.is_new_flag = "N";
                                        pn.mogra_details.Add(vd);
                                    }
                                }
                                lst.Add(pn);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUD_SaveMograVersionsDetails")]
        public HttpResponseMessage CUD_SaveMograVersionsDetails(string opr, Mogra_Version obj)
        {
            string res = "true";
            try
            {
                if (opr.Equals("I"))
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                        new List<SqlParameter>() {
                        new SqlParameter("@opr", "IMV"),
                        new SqlParameter("@lic_changeset_no", obj.lic_changeset_no),
                        new SqlParameter("@lic_project_code", obj.lic_project_code),
                        new SqlParameter("@lic_developed_by", obj.lic_developed_by),
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            foreach (Mogra_Version_Details vd in obj.mogra_details)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                    new List<SqlParameter>() {
                            new SqlParameter("@opr", "IMD"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                                }
                            }
                        }
                        else
                        {
                            res = "exists";
                        }
                    }
                }
                else
                {
                    foreach (Mogra_Version_Details vd in obj.mogra_details)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                            new List<SqlParameter>() {
                            new SqlParameter("@opr", vd.is_new_flag.Equals("Y")?"IMD":"IMU"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            { res = "false"; }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("CUD_MograVersionsDetails")]
        public HttpResponseMessage CUD_MograVersionsDetails(Mogra_Version_Details vd)
        {
            string res = "true";
            try
            {

                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() {
                            new SqlParameter("@opr", "DMD"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                }
            }
            catch (Exception ex)
            { res = "false"; }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

    }

}