﻿using log4net;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Net;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/DataImportController")]
    public class dataimportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getdataimport")]
        public HttpResponseMessage getdataimportDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmailDetailsDatewise(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<dataimport> lst_emaildet = new List<dataimport>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payble_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", 'S'),
                            //new SqlParameter("@fromdate", db.DBYYYYMMDDformat( fromdate)),                          
                            // new SqlParameter("@todate", db.DBYYYYMMDDformat(todate)),                          
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            dataimport paysobj = new dataimport();
                            paysobj.number = dr["pa_number"].ToString();
                            paysobj.company_code = dr["pa_company_code"].ToString();
                            paysobj.pay_code = dr["pa_pay_code"].ToString();
                            paysobj.effective_from_date = db.UIDDMMYYYYformat(dr["effective_from_date"].ToString());
                            paysobj.amount = dr["pa_amount"].ToString();
                            paysobj.effective_upto = db.UIDDMMYYYYformat(dr["pa_effective_upto"].ToString());
                            paysobj.rowid = dr["rowid"].ToString();
                            paysobj.remark = dr["pa_remark"].ToString();
                            lst_emaildet.Add(paysobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
        }

        [Route("DataImport")]
        public HttpResponseMessage dataimport(List<dataimport> data)
        {
            List<dataimport> trans_list = new List<dataimport>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var o in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_data_import_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@employee_number", o.employee_number),
                            new SqlParameter("@company_code", o.company_code),
                            new SqlParameter("@pay_code", o.pay_code),
                            new SqlParameter("@effective_from_date", db.DBYYYYMMDDformat(o.effective_from_date)),
                            new SqlParameter("@amount", o.amount),                            
                            new SqlParameter("@effective_upto_date", db.DBYYYYMMDDformat(o.effective_upto_date)),
                            new SqlParameter("@remark", o.remark),
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                dataimport obj = new dataimport();
                                obj.employee_number = dr["employee_number"].ToString();
                                obj.company_code = dr["company_code"].ToString();
                                obj.pay_code = dr["pay_code"].ToString();
                                obj.effective_from_date = db.UIDDMMYYYYformat(dr["effective_from_date"].ToString());
                                obj.amount = dr["amount"].ToString();
                                obj.effective_upto_date =  db.UIDDMMYYYYformat(dr["effective_upto_date"].ToString());
                                obj.remark = dr["remark"].ToString();
                                // obj.color='red';
                                trans_list.Add(obj);
                            }
                            dr.Close();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }

        [Route("DataImportSave")]
        public HttpResponseMessage dataimportsave(List<dataimport> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (dataimport simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[payroll_data_import_temp_prc]",
                            

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@emp_number",simsobj.employee_number),
                                new SqlParameter("@company_code",simsobj.company_code),
                                new SqlParameter("@pay_code", simsobj.pay_code),
                               // new SqlParameter("@effective_from",  DateTime.Parse(simsobj.effective_from_date).Year + "-" + DateTime.Parse(simsobj.effective_from_date).Month + "-" + DateTime.Parse(simsobj.effective_from_date).Day),
                               new SqlParameter("@effective_from", db.DBYYYYMMDDformat(simsobj.effective_from_date)),
                                new SqlParameter("@amount ", simsobj.amount),
                                //new SqlParameter("@effective_upto",  DateTime.Parse(simsobj.effective_upto_date).Year + "-" + DateTime.Parse(simsobj.effective_upto_date).Month + "-" + DateTime.Parse(simsobj.effective_upto_date).Day),                                
                                new SqlParameter("@effective_upto", db.DBYYYYMMDDformat(simsobj.effective_upto_date)),
                                new SqlParameter("@remark", simsobj.remark),
                                //new SqlParameter("@created_user", simsobj.created_user),
                                //new SqlParameter("@created_date", simsobj.created_date),
                              //  new SqlParameter("@filename", simsobj.filename),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }
    }

}