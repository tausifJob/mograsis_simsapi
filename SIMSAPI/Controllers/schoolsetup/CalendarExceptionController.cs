﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.setupClass;


namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/calendarexception")]
    public class CalendarExceptionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAttendanceCuriculum")]
        public HttpResponseMessage getAttendanceCuriculum()
        {
            List<CalendarException> list = new List<CalendarException>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException simsobj = new CalendarException();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendanceyear")]
        public HttpResponseMessage getAttendanceyear(string cur_code)
        {
            List<CalendarException> list = new List<CalendarException>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException simsobj = new CalendarException();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_start_date =dr["sims_academic_year_start_date"].ToString();
                            simsobj.sims_academic_year_end_date = dr["sims_academic_year_end_date"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string acdm_yr)
        {
            List<CalendarException> list = new List<CalendarException>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","P"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year",acdm_yr)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException simsobj = new CalendarException();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getUserGroup")]
        public HttpResponseMessage getUserGroup()
        {
            List<CalendarException> list = new List<CalendarException>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","Q"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException simsobj = new CalendarException();
                            simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            simsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getSectionCodes")]
        public HttpResponseMessage getSectionCodes(string cur_code, string acdm_yr, string grd_code_list)
        {
            List<CalendarException> list = new List<CalendarException>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year",acdm_yr),
                            new SqlParameter("@sims_grade_list",grd_code_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException simsobj = new CalendarException();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_grade_code"].ToString() + dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getExceptionType")]
        public HttpResponseMessage getExceptionType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getExceptionType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getExceptionType"));

            List<CalendarException> excp_type = new List<CalendarException>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>() { new SqlParameter("@opr", "A") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            excp_type.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, excp_type);

            }
            return Request.CreateResponse(HttpStatusCode.OK, excp_type);

        }

        [Route("getDays")]
        public HttpResponseMessage getDays()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getExceptionType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getExceptionType"));

            List<CalendarException> excp_type = new List<CalendarException>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>() { new SqlParameter("@opr", "B") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.day_code = dr["sims_appl_parameter"].ToString();
                            obj.day_desc = dr["sims_appl_form_field_value1"].ToString();
                            excp_type.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, excp_type);

            }
            return Request.CreateResponse(HttpStatusCode.OK, excp_type);

        }

        [Route("getCalendarException")]
        public HttpResponseMessage getCalendarException(string acdm_yr, string excp_ty_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_academic_year",acdm_yr),
                            new SqlParameter("@sims_calendar_exception_type_name",excp_ty_name)
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                            obj.sims_cur_name = dr["sims_cur_name"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();
                            obj.sims_from_date = db.UIDDMMYYYYformat( dr["sims_from_date"].ToString());
                            obj.sims_to_date =db.UIDDMMYYYYformat(  dr["sims_to_date"].ToString());
                            obj.sims_calendar_exception_type = dr["sims_calendar_exception_type"].ToString();
                            obj.sims_calendar_exception_type_name = dr["sims_calendar_exception_type_name"].ToString();
                            obj.sims_calendar_exception_attendance_flag = dr["sims_calendar_exception_attendance_flag"].ToString().Equals("Y") ? true : false;
                            obj.sims_calendar_exception_instruction_flag = dr["sims_calendar_exception_instruction_flag"].ToString().Equals("Y") ? true : false;
                            obj.sims_calendar_exception_day_value = dr["sims_calendar_exception_day_value"].ToString();
                            obj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            if (!string.IsNullOrEmpty(dr["sims_calendar_exception_color_code"].ToString()))
                            {
                                if(dr["sims_calendar_exception_color_code"].ToString().Length==9)
                                obj.sims_calendar_exception_color_code = string.Format("#{0}", dr["sims_calendar_exception_color_code"].ToString().Substring(3));
                                else
                                obj.sims_calendar_exception_color_code = dr["sims_calendar_exception_color_code"].ToString();

                               // int t = dr["sims_calendar_exception_color_code"].ToString().Length;
                            }

                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }

        [Route("getCalendarException")]
        public HttpResponseMessage getCalendarException(string acdm_yr, string excp_ty_name,string cur)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_academic_year",acdm_yr),
                            new SqlParameter("@sims_calendar_exception_type_name",excp_ty_name),
                            new SqlParameter("@sims_cur_code",cur),

                            
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                            obj.sims_cur_name = dr["sims_cur_name"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();
                            obj.sims_from_date = db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
                            obj.sims_to_date = db.UIDDMMYYYYformat(dr["sims_to_date"].ToString());
                            obj.sims_calendar_exception_type = dr["sims_calendar_exception_type"].ToString();
                            obj.sims_calendar_exception_type_name = dr["sims_calendar_exception_type_name"].ToString();
                            obj.sims_calendar_exception_attendance_flag = dr["sims_calendar_exception_attendance_flag"].ToString().Equals("Y") ? true : false;
                            obj.sims_calendar_exception_instruction_flag = dr["sims_calendar_exception_instruction_flag"].ToString().Equals("Y") ? true : false;
                            obj.sims_calendar_exception_day_value = dr["sims_calendar_exception_day_value"].ToString();
                            obj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            if (!string.IsNullOrEmpty(dr["sims_calendar_exception_color_code"].ToString()))
                            {
                                if (dr["sims_calendar_exception_color_code"].ToString().Length == 9)
                                    obj.sims_calendar_exception_color_code = string.Format("#{0}", dr["sims_calendar_exception_color_code"].ToString().Substring(3));
                                else
                                    obj.sims_calendar_exception_color_code = dr["sims_calendar_exception_color_code"].ToString();
                            }
                            try
                            {
                                obj.sims_calendar_exception_staff_type = dr["sims_calendar_exception_staff_type"].ToString();
                            }
                            catch (Exception ex)
                            {                                
                            }
                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }



        [Route("insertCalendarException")]
        public HttpResponseMessage insertCalendarException(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

           // List<CalendarException> cal_list = new List<CalendarException>();
            bool result=false;
            int cnt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>(){
                             new SqlParameter("@opr","I"), 
                             new SqlParameter("@sims_cur_code",myobj.sims_cur_code),
                             new SqlParameter("@sims_academic_year",myobj.sims_academic_year),
                             new SqlParameter("@sims_calendar_exception_description",myobj.sims_calendar_exception_description),
                             new SqlParameter("@sims_from_date",db.DBYYYYMMDDformat(myobj.sims_from_date)),
                             new SqlParameter("@sims_to_date",db.DBYYYYMMDDformat(myobj.sims_to_date)),
                             new SqlParameter("@sims_calendar_exception_type_name",myobj.sims_calendar_exception_type),
                             new SqlParameter("@sims_calendar_exception_attendance_flag",myobj.sims_calendar_exception_attendance_flag==true?"Y":"N"),
                             new SqlParameter("@sims_calendar_exception_instruction_flag",myobj.sims_calendar_exception_instruction_flag==true?"Y":"N"),
                             new SqlParameter("@sims_calendar_exception_day_value",myobj.sims_calendar_exception_day_value),
                             new SqlParameter("@sims_status",myobj.sims_status==true?"A":"I"),
                             new SqlParameter("@sims_calendar_exception_color_code",myobj.sims_calendar_exception_color_code),
                             new SqlParameter("@sims_calendar_exception_student_flag",myobj.sims_calendar_exception_student_flag==true?"A":"I"),
                             new SqlParameter("@sims_calendar_exception_emp_flag",myobj.sims_calendar_exception_emp_flag==true?"A":"I"),
                             new SqlParameter("@sims_section_code",myobj.sims_section_code_list),
                             new SqlParameter("@weekendNames",myobj.weekendNames),
                             new SqlParameter("@sims_employee_group",myobj.sims_employee_group),
                             new SqlParameter("@sims_calendar_exception_staff_type",myobj.sims_calendar_exception_staff_type),                             
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                        cnt = dr.RecordsAffected;
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("calendarExceptionUpdate")]
        public HttpResponseMessage calendarExceptionUpdate(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            // List<CalendarException> cal_list = new List<CalendarException>();
            bool result = false;
            int cnt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_proc]",
                        new List<SqlParameter>(){
                             new SqlParameter("@opr","U"), 
                             new SqlParameter("@sims_academic_year",myobj.sims_academic_year),
                             new SqlParameter("@sims_calendar_exception_number",myobj.sims_calendar_exception_number),
                             new SqlParameter("@sims_calendar_exception_description",myobj.sims_calendar_exception_description),
                             new SqlParameter("@sims_from_date",db.DBYYYYMMDDformat( myobj.sims_from_date)),
                             new SqlParameter("@sims_to_date",db.DBYYYYMMDDformat(myobj.sims_to_date)),
                              new SqlParameter("@org_from_date",db.DBYYYYMMDDformat(myobj.org_from_date)),
                             new SqlParameter("@org_to_date",db.DBYYYYMMDDformat(myobj.org_to_date)),
                             new SqlParameter("@sims_calendar_exception_type_name",myobj.sims_calendar_exception_type),
                             new SqlParameter("@sims_calendar_exception_attendance_flag",myobj.sims_calendar_exception_attendance_flag==true?"Y":"N"),
                             new SqlParameter("@sims_calendar_exception_instruction_flag",myobj.sims_calendar_exception_instruction_flag==true?"Y":"N"),
                             new SqlParameter("@sims_calendar_exception_day_value",myobj.sims_calendar_exception_day_value),
                             new SqlParameter("@sims_status",myobj.sims_status==true?"A":"I"),
                             new SqlParameter("@sims_calendar_exception_color_code",myobj.sims_calendar_exception_color_code),
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                        cnt = dr.RecordsAffected;
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("calendarExceptionDelete")]
        public HttpResponseMessage calendarExceptionDelete(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            // List<CalendarException> cal_list = new List<CalendarException>();
            bool result = false;
            int cnt = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                        new List<SqlParameter>(){
                             new SqlParameter("@opr","D"), 
                             new SqlParameter("@sims_calendar_exception_number",myobj.sims_calendar_exception_number),
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                        cnt = dr.RecordsAffected;
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("calendarExceptionDeleteNew")]
        public HttpResponseMessage calendarExceptionDeleteNew(List<CalendarException> myobj1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            // List<CalendarException> cal_list = new List<CalendarException>();
            bool result = false;
            int cnt = 0;
            foreach (CalendarException myobj in myobj1)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_calendar_exception_proc]",
                            new List<SqlParameter>(){
                             new SqlParameter("@opr","D"), 
                             new SqlParameter("@sims_calendar_exception_number",myobj.sims_calendar_exception_number),
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                            cnt = dr.RecordsAffected;
                        }

                    }
                }


                catch (Exception e)
                {
                    Log.Error(e);
                   // return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("gettotalWorkingDays")]
        public HttpResponseMessage gettotalWorkingDays()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@opr_sub","WRK"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.total_working_days = dr["total_working_days"].ToString();

                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }

        [Route("getStudGrades")]
        public HttpResponseMessage getStudGrades(string cur_code,string acdm)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","A"),
                            new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year",acdm),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();

                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }


        [Route("getStudSections")]
        public HttpResponseMessage getStudSections(string cur_code, string acdm,string grd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year",acdm),
                             new SqlParameter("@sims_grade_code",grd),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }

        [Route("studentExceptionDetails")]
        public HttpResponseMessage studentExceptionDetails(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@opr_sub","STU"),
                            new SqlParameter("@sims_cur_code",myobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year",myobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code",myobj.sims_grade_code),
                            new SqlParameter("@sims_section_code",myobj.sims_section_code),
                            new SqlParameter("@sims_exception_type",myobj.sims_calendar_exception_type),

                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();
                            obj.sims_from_date =db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
                            obj.sims_status = dr["sims_status"].ToString().Equals("A")?true:false;
                            obj.sims_calendar_exception_type = dr["sims_calendar_exception_type"].ToString();
                            try
                            {
                                obj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                            }
                            catch (Exception ex) { }
                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }

        [Route("getEmplyoeeExceptionDetails")]
        public HttpResponseMessage getEmplyoeeExceptionDetails(string user_grp_code, string excp_type)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            List<CalendarException> cal_list = new List<CalendarException>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","S"),
                             new SqlParameter("@opr_sub","EMP"),
                            new SqlParameter("@sims_employee_group",user_grp_code),
                             new SqlParameter("@sims_exception_type",excp_type),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CalendarException obj = new CalendarException();
                            obj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                            obj.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();
//                            obj.sims_from_date = DateTime.Parse( dr["sims_from_date"].ToString()).ToShortDateString()
                                      obj.sims_from_date = db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
  //                          obj.sims_to_date =DateTime.Parse( dr["sims_to_date"].ToString()).ToShortDateString();
                                      obj.sims_to_date = db.UIDDMMYYYYformat(dr["sims_to_date"].ToString());
                            obj.sims_status = dr["sims_status"].ToString().Equals("A")?true:false;

                            cal_list.Add(obj);
                        }
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, cal_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cal_list);
        }


        [Route("studentExceptionDetailsUpdate")]
        public HttpResponseMessage studentExceptionDetailsUpdate(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalendarException(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CalendarException"));

            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","U"),
                            new SqlParameter("@opr_sub","STU"),
                            new SqlParameter("@sims_cur_code",myobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year",myobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code",myobj.sims_grade_code),
                            new SqlParameter("@sims_section_code",myobj.sims_section_code),
                            new SqlParameter("@sims_exception_type",myobj.sims_calendar_exception_type),
                            new SqlParameter("@sims_calendar_exception_number",myobj.sims_calendar_exception_number),

                            new SqlParameter("@sims_status",myobj.sims_status==true?"A":"I"),
//                            new SqlParameter("@sims_attendance_date",myobj.sims_attendance_date),
                         	new SqlParameter("@sims_attendance_date", db.DBYYYYMMDDformat(myobj.sims_attendance_date)), // Insert only

                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("EmplyoeeExceptionDetailsUpdate")]
        public HttpResponseMessage EmplyoeeExceptionDetailsUpdate(CalendarException myobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : EmplyoeeExceptionDetailsUpdate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "EmplyoeeExceptionDetailsUpdate"));

            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_exception_student_and_employee_proc]",
                        new List<SqlParameter>(){
                            new SqlParameter("@opr","U"),
                             new SqlParameter("@opr_sub","EMP"),
                            new SqlParameter("@sims_employee_group",myobj.sims_employee_group),
                             new SqlParameter("@sims_calendar_exception_number",myobj.sims_calendar_exception_number),
                             new SqlParameter("@sims_status",myobj.sims_status==true?"A":"I")
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        res = true;
                    }

                }

            }

            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }

            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


    }
}