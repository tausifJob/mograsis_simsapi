﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

namespace SIMSRestAPI.Controllers.ParentPortal
{
    [RoutePrefix("api/calender")]
    [BasicAuthentication]
    public class CalenderController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCategorize")]
        public HttpResponseMessage getCategorize()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCategorize(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Uccw008> events = new List<Uccw008>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_calendar_proc",
                        new List<SqlParameter>() { new SqlParameter("@opr", "A") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw008 obj = new Uccw008();
                            obj.sims_appl_form_field = dr["sims_appl_form_field"].ToString();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            obj.color_code = dr["sims_appl_form_field_value2"].ToString();
                            events.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("getTimeMarkers")]
        public HttpResponseMessage getTimeMarkers()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Uccw008> events = new List<Uccw008>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_calendar_proc",
                        new List<SqlParameter>() { new SqlParameter("@opr", "B") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw008 obj = new Uccw008();
                            obj.sims_appl_form_field = dr["sims_appl_form_field"].ToString();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            obj.color_code = dr["sims_appl_form_field_value2"].ToString();
                            events.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("getCalender")]
        public HttpResponseMessage getCalender()
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalender(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            List<Uccw008> events = new List<Uccw008>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_calendar_proc",
                        new List<SqlParameter>() { new SqlParameter("@opr", "S") });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw008 obj = new Uccw008();
                            obj.id = dr["sims_calendar_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_calendar_date"].ToString()))
                                obj.sims_calendar_date = dr["sims_calendar_date"].ToString();
//                                obj.sims_calendar_date = DateTime.Parse(dr["sims_calendar_date"].ToString()).ToShortDateString();
                            obj.start =dr["sims_calendar_date"].ToString();
                            obj.end =dr["sims_calendar_date"].ToString();
                            obj.sims_calendar_event_start_time = dr["sims_calendar_event_start_time"].ToString();
                            obj.sims_calendar_event_end_time = dr["sims_calendar_event_end_time"].ToString();
                            obj.title = dr["sims_calendar_event_short_desc"].ToString();
                            obj.sims_event_color = dr["sims_event_color"].ToString();
                            obj.sims_calendar_event_type = dr["sims_calendar_event_type"].ToString();
                            obj.sims_calendar_event_category = dr["sims_calendar_event_category"].ToString();
                            obj.sims_calendar_event_priority = dr["sims_calendar_event_priority"].ToString() == "0" ? false : true;
                            obj.sims_calendar_event_short_desc = dr["sims_calendar_event_short_desc"].ToString();
                            obj.sims_calendar_event_desc = dr["sims_calendar_event_desc"].ToString();
                            obj.sims_calendar_event_status = dr["sims_calendar_event_status"].ToString() == "1" ? true : false;
                            obj.sims_calendar_time_marker = dr["sims_calendar_time_marker"].ToString();
                            obj.sims_calendar_event_category_desc = dr["sims_calendar_event_category_desc"].ToString();
                            events.Add(obj);
                        }
                    }
                }

                //using (DBConnection db_appl = new DBConnection())
                //{
                //    db_appl.Open();
                //    List<Direction> comnappl_list = new List<Direction>();
                //    SqlDataReader dr_appl = db_appl.ExecuteStoreProcedure("pp_sims_transport_route_student", new List<SqlParameter>() 
                //               { 
                //                   new SqlParameter("@opr", 'S'),
                //                  new SqlParameter("@grade_code", grdCode),
                //                   new SqlParameter("@section_code", sectCode),
                //                   new SqlParameter("@academic_year", year),

                //               });
                //    if (dr_appl.HasRows)
                //    {
                //        while (dr_appl.Read())
                //        {
                //            Direction simsObj2 = new Direction();
                //            simsObj2.sims_transport_route_direction_code = dr_appl["sims_transport_route_direction"].ToString();
                //            simsObj2.sims_transport_route_direction_name = dr_appl["sims_transport_route_direction_1"].ToString();

                //            comnappl_list.Add(simsObj2);
                //        }
                //    }
                //    db_appl.Dispose();
                //    simsObj.direction = comnappl_list;
                //}




            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }


        [Route("getCalenderniss")]
        public HttpResponseMessage getCalenderniss(string cur)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCalender(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            List<Uccw008> events = new List<Uccw008>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_calendar_proc",
                        new List<SqlParameter>() { new SqlParameter("@opr", "S"), new SqlParameter("@sims_cur_code", cur) });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw008 obj = new Uccw008();
                            obj.id = dr["sims_calendar_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_calendar_date"].ToString()))
                                obj.sims_calendar_date = dr["sims_calendar_date"].ToString();
                            //                                obj.sims_calendar_date = DateTime.Parse(dr["sims_calendar_date"].ToString()).ToShortDateString();
                            obj.start = dr["sims_calendar_date"].ToString();
                            obj.end = dr["sims_calendar_date"].ToString();
                            obj.sims_calendar_event_start_time = dr["sims_calendar_event_start_time"].ToString();
                            obj.sims_calendar_event_end_time = dr["sims_calendar_event_end_time"].ToString();
                            obj.title = dr["sims_calendar_event_short_desc"].ToString();
                            obj.sims_event_color = dr["sims_event_color"].ToString();
                            obj.sims_calendar_event_type = dr["sims_calendar_event_type"].ToString();
                            obj.sims_calendar_event_category = dr["sims_calendar_event_category"].ToString();
                            obj.sims_calendar_event_priority = dr["sims_calendar_event_priority"].ToString() == "0" ? false : true;
                            obj.sims_calendar_event_short_desc = dr["sims_calendar_event_short_desc"].ToString();
                            obj.sims_calendar_event_desc = dr["sims_calendar_event_desc"].ToString();
                            obj.sims_calendar_event_status = dr["sims_calendar_event_status"].ToString() == "1" ? true : false;
                            obj.sims_calendar_time_marker = dr["sims_calendar_time_marker"].ToString();
                            obj.sims_calendar_event_category_desc = dr["sims_calendar_event_category_desc"].ToString();
                            events.Add(obj);
                        }
                    }
                }

                //using (DBConnection db_appl = new DBConnection())
                //{
                //    db_appl.Open();
                //    List<Direction> comnappl_list = new List<Direction>();
                //    SqlDataReader dr_appl = db_appl.ExecuteStoreProcedure("pp_sims_transport_route_student", new List<SqlParameter>() 
                //               { 
                //                   new SqlParameter("@opr", 'S'),
                //                  new SqlParameter("@grade_code", grdCode),
                //                   new SqlParameter("@section_code", sectCode),
                //                   new SqlParameter("@academic_year", year),

                //               });
                //    if (dr_appl.HasRows)
                //    {
                //        while (dr_appl.Read())
                //        {
                //            Direction simsObj2 = new Direction();
                //            simsObj2.sims_transport_route_direction_code = dr_appl["sims_transport_route_direction"].ToString();
                //            simsObj2.sims_transport_route_direction_name = dr_appl["sims_transport_route_direction_1"].ToString();

                //            comnappl_list.Add(simsObj2);
                //        }
                //    }
                //    db_appl.Dispose();
                //    simsObj.direction = comnappl_list;
                //}




            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }


        [Route("AddEventByDate")]
        public HttpResponseMessage AddEventByDate(string data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AddEventByDate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(data);
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_calendar_date",db.DBYYYYMMDDformat(events.sims_calendar_date)),
                            new SqlParameter("@sims_calendar_event_start_time", events.sims_calendar_event_start_time),
                            new SqlParameter("@sims_calendar_event_end_time", events.sims_calendar_event_end_time),
                            new SqlParameter("@sims_calendar_event_type", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_category", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_priority", events.sims_calendar_event_priority==true?"1":"0"),
                            new SqlParameter("@sims_calendar_event_short_desc", events.sims_calendar_event_short_desc),
                            new SqlParameter("@sims_calendar_event_desc", events.sims_calendar_event_desc),
                            new SqlParameter("@sims_calendar_event_status", events.sims_calendar_event_status==true?"1":"0"),
                            new SqlParameter("@sims_calendar_time_marker",events.sims_calendar_time_marker),
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception x)
            {

               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("AddEventByDate")]
        public HttpResponseMessage AddEventByDate(List<Uccw008> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AddEventByDate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //    Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(data);
            bool result = false;
            try
            {
                foreach (Uccw008 events in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                            new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_calendar_date",events.sims_calendar_date),
                            new SqlParameter("@sims_calendar_event_start_time", events.sims_calendar_event_start_time),
                            new SqlParameter("@sims_calendar_event_end_time", events.sims_calendar_event_end_time),
                            new SqlParameter("@sims_calendar_event_type", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_category", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_priority", events.sims_calendar_event_priority==true?"1":"0"),
                            new SqlParameter("@sims_calendar_event_short_desc", events.sims_calendar_event_short_desc),
                            new SqlParameter("@sims_calendar_event_desc", events.sims_calendar_event_desc),
                            new SqlParameter("@sims_calendar_event_status", events.sims_calendar_event_status==true?"1":"0"),
                            new SqlParameter("@sims_calendar_time_marker",events.sims_calendar_time_marker),
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                    }
                }

            }
            catch (Exception x)
            {

               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("AddEventByDateNiss")]
        public HttpResponseMessage AddEventByDateNiss(List<Uccw008> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : AddEventByDate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //    Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(data);
            bool result = false;
            try
            {
                foreach (Uccw008 events in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                            new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_calendar_date",events.sims_calendar_date),
                            new SqlParameter("@sims_calendar_event_start_time", events.sims_calendar_event_start_time),
                            new SqlParameter("@sims_calendar_event_end_time", events.sims_calendar_event_end_time),
                            new SqlParameter("@sims_calendar_event_type", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_category", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_priority", events.sims_calendar_event_priority==true?"1":"0"),
                            new SqlParameter("@sims_calendar_event_short_desc", events.sims_calendar_event_short_desc),
                            new SqlParameter("@sims_calendar_event_desc", events.sims_calendar_event_desc),
                            new SqlParameter("@sims_calendar_event_status", events.sims_calendar_event_status==true?"1":"0"),
                            new SqlParameter("@sims_calendar_time_marker",events.sims_calendar_time_marker),
                            new SqlParameter("@sims_cur_code",events.grade_cur_code),

                        });

                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                    }
                }

            }
            catch (Exception x)
            {

                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }




        [Route("EventUpdateByDate")]
        public HttpResponseMessage EventUpdateByDate(Uccw008 events)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : EventUpdateByDate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //   Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(Udata);
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_calendar_number",events.sims_calendar_number),
                            new SqlParameter("@sims_calendar_date",db.DBYYYYMMDDformat(events.sims_calendar_date)),
                            new SqlParameter("@sims_calendar_event_start_time", events.sims_calendar_event_start_time),
                            new SqlParameter("@sims_calendar_event_end_time", events.sims_calendar_event_end_time),
                            new SqlParameter("@sims_calendar_event_type", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_category", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_priority", events.sims_calendar_event_priority==true?"1":"0"),
                            new SqlParameter("@sims_calendar_event_short_desc", events.sims_calendar_event_short_desc),
                            new SqlParameter("@sims_calendar_event_desc", events.sims_calendar_event_desc),
                            new SqlParameter("@sims_calendar_event_status", events.sims_calendar_event_status==true?"1":"0"),
                            new SqlParameter("@sims_calendar_time_marker",events.sims_calendar_time_marker),
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("EventUpdateByDate_Niss")]
        public HttpResponseMessage EventUpdateByDate_Niss(Uccw008 events)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : EventUpdateByDate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //   Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(Udata);
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_calendar_number",events.sims_calendar_number),
                            new SqlParameter("@sims_calendar_date",db.DBYYYYMMDDformat(events.sims_calendar_date)),
                            new SqlParameter("@sims_calendar_event_start_time", events.sims_calendar_event_start_time),
                            new SqlParameter("@sims_calendar_event_end_time", events.sims_calendar_event_end_time),
                            new SqlParameter("@sims_calendar_event_type", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_category", events.sims_calendar_event_type),
                            new SqlParameter("@sims_calendar_event_priority", events.sims_calendar_event_priority==true?"1":"0"),
                            new SqlParameter("@sims_calendar_event_short_desc", events.sims_calendar_event_short_desc),
                            new SqlParameter("@sims_calendar_event_desc", events.sims_calendar_event_desc),
                            new SqlParameter("@sims_calendar_event_status", events.sims_calendar_event_status==true?"1":"0"),
                            new SqlParameter("@sims_calendar_time_marker",events.sims_calendar_time_marker),
                            new SqlParameter("@sims_cur_code",events.grade_cur_code),

                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }





        [Route("EventDeleteById")]
        public HttpResponseMessage EventDeleteById(Uccw008 events)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : EventDeleteById(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //   Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(Udata);
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_calendar_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@sims_calendar_number",events.sims_calendar_number),
                            
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}