﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.gradebookClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.Scheduling
{
     [RoutePrefix("api/assessmentreport")]
    public class WeeklyAssessmentShowController:ApiController
    {

         [Route("AssessmentChilddetails")]
         public HttpResponseMessage AssessmentChilddetails(assessment_student data)
         {
             List<assessment_student> term_list = new List<assessment_student>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "UR"),
                new SqlParameter("@CUR_CODE", data.sims_cur_code),
                new SqlParameter("@ACADEMIC_YEAR", data.sims_academic_year),
                //new SqlParameter("@GRADE_CODE", data.sims_grade_code),
                //new SqlParameter("@SECTION_CODE", data.sims_section_code),
                new SqlParameter("@GB_NUMBER", data.sims_gb_number),
                new SqlParameter("@GB_CAT_NUMBER", data.sims_gb_cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER", data.sims_gb_cat_assign_number),
                 new SqlParameter("@GB_ENROLL", data.sims_enroll_number)

                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             assessment_student simsobj = new assessment_student();
                             simsobj.month_name = dr["sims_gb_cat_name"].ToString();
                             simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                             simsobj.Week_name = dr["sims_gb_cat_assign_name"].ToString();
                             simsobj.Stud_Full_Name = dr["Student_name"].ToString();
                             simsobj.assessment_syllabus = dr["sims_gb_cat_assign_remarks"].ToString();
                             simsobj.grade_name = dr["sims_mark_grade_name"].ToString();
                             simsobj.Stud_Full_Name = dr["Student_name"].ToString();
                             simsobj.grade_section_name = dr["sims_grade_name_en"].ToString() + "/" + dr["sims_section_name_en"].ToString();
                             term_list.Add(simsobj);
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, term_list);

                 }
             }
             catch (Exception e)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, term_list);

         }

         [Route("getAssessmentsubjectdetails")]
         public HttpResponseMessage getAssessmentsubjectdetails(string enroll_number, string cat_code, string assign_code)
         {
             List<assessment_student> term_list = new List<assessment_student>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "UA"),
                new SqlParameter("@GB_ENROLL",enroll_number),
                new SqlParameter("@GB_CAT_NUMBER",cat_code),
                new SqlParameter("@GB_ASSIGNMENT_NUMBER",assign_code)
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             assessment_student simsobj = new assessment_student();
                             simsobj.sims_subject_code = dr["sims_gb_number"].ToString();
                             simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            

                             term_list.Add(simsobj);
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, term_list);

                 }
             }
             catch (Exception e)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, term_list);

         }

         [Route("getAssessmentmonthdetails")]
         public HttpResponseMessage getAssessmentmonthdetails(string enroll_number)
         {
             List<assessment_student> term_list = new List<assessment_student>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "UC"),
                new SqlParameter("@GB_ENROLL",enroll_number)

                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             assessment_student simsobj = new assessment_student();
                             simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                             simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                             term_list.Add(simsobj);
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, term_list);

                 }
             }
             catch (Exception e)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, term_list);

         }

         [Route("getAssessmentweekdetails")]
         public HttpResponseMessage getAssessmentweekdetails(string enroll_number, string month_name)
         {
             List<assessment_student> term_list = new List<assessment_student>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[ASSESSMENT_PROC]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "UU"),
                new SqlParameter("@GB_ENROLL",enroll_number),
                new SqlParameter("@GB_CAT_NUMBER",month_name)

                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             assessment_student simsobj = new assessment_student();
                             simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                             simsobj.sims_assignment_name = dr["sims_gb_cat_assign_name"].ToString();
                             term_list.Add(simsobj);
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, term_list);

                 }
             }
             catch (Exception e)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, term_list);

         }
    }
}