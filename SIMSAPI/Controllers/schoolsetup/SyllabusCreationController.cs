﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/syllabus_lesson")]
    public class SyllabusCreationController : ApiController
    {

        #region(Syllabus Cration Api)

        [Route("syllabus_subject")]
        public HttpResponseMessage syllabus_subject(syllabus_and_lesson data)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "GS"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@lesson_user",data.lesson_user),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }
        
        [Route("syllabus_grade")]
        public HttpResponseMessage syllabus_grade(syllabus_and_lesson data)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "GD"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@lesson_user",data.lesson_user),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("syllabus_section")]
        public HttpResponseMessage syllabus_section(syllabus_and_lesson data)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "SC"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code)
                                                    

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("syllabus_lesson")]
        public HttpResponseMessage syllabus_lesson(syllabus_and_lesson data)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                             new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code1), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                            new  SqlParameter("@lesson_user",data.lesson_user),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.sims_syllabus_start_date = db.UIDDMMYYYYformat(dr["sims_syllabus_start_date"].ToString());
                            obj.sims_syllabus_end_date = db.UIDDMMYYYYformat(dr["sims_syllabus_end_date"].ToString());
                            obj.sims_syllabus_remark = dr["sims_syllabus_remark"].ToString();
                            obj.sims_syllabus_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_creation_date"].ToString());
                            obj.sims_syllabus_created_by = dr["sims_syllabus_created_by"].ToString();
                            obj.sims_syllabus_status = dr["sims_syllabus_status"].ToString().Equals("A") ? true : false;
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.month_name = dr["month_name"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_display_name = obj.sims_grade_name.ToString() + " / " + obj.sims_section_name.ToString() + "- " + obj.sims_subject_name.ToString() + "- " + obj.month_name.ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("get_syllabus_lesson_for_approval")]
        public HttpResponseMessage get_syllabus_lesson_for_approval(string user)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "TB"),
                             new  SqlParameter("@lesson_user",user),
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();
                            obj.month_name = dr["month_name"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_creation_date"].ToString());
                            obj.sims_syllabus_created_by = dr["sims_syllabus_created_by"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name =(dr["sims_grade_name_en"].ToString() +" / "+ dr["sims_section_name_en"].ToString()).ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.sims_syllabus_start_date = db.UIDDMMYYYYformat(dr["sims_syllabus_start_date"].ToString());
                            obj.sims_syllabus_end_date = db.UIDDMMYYYYformat(dr["sims_syllabus_end_date"].ToString());
                            obj.sims_syllabus_lesson_plan_doc_path = dr["sims_syllabus_lesson_plan_doc_path"].ToString();
                            obj.sims_syllabus_created_name = dr["sims_syllabus_created_by1"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("get_syllabus_lesson_for_approvaled")]
        public HttpResponseMessage get_syllabus_lesson_for_approvaled(string user, string sims_academic_year)
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "TP"),
                             new  SqlParameter("@lesson_user",user),
                             new  SqlParameter("@sims_academic_year",sims_academic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.month_name = dr["month_name"].ToString();
                            obj.sims_syllabus_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_creation_date"].ToString());
                            obj.sims_syllabus_created_by = dr["sims_syllabus_created_by"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.sims_syllabus_start_date = db.UIDDMMYYYYformat(dr["sims_syllabus_start_date"].ToString());
                            obj.sims_syllabus_end_date = db.UIDDMMYYYYformat(dr["sims_syllabus_end_date"].ToString());
                            obj.sims_syllabus_lesson_plan_doc_path = dr["sims_syllabus_lesson_plan_doc_path"].ToString();
                            obj.Approved_status = dr["Approved_status"].ToString();
                            obj.sims_syllabus_created_name = dr["sims_syllabus_created_by1"].ToString();
                            obj.sims_syllabus_approved_name = dr["sims_syllabus_lesson_plan_approved_name"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("syllabus_lesson_uint_name")]
        public HttpResponseMessage syllabus_lesson_uint_name(syllabus_and_lesson data)
        {
            List<syllabus_unit> lst = new List<syllabus_unit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "UN"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code1),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@lesson_user",data.lesson_user)
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_unit obj = new syllabus_unit();
                            obj.sims_syllabus_unit_code = dr["sims_syllabus_unit_code"].ToString();
                           
                            obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                            
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }




        [Route("syllabus_Unit")]
        public HttpResponseMessage syllabus_Unit(syllabus_and_lesson data)
        {
            List<syllabus_unit> lst = new List<syllabus_unit>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SU"),
                            new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code1), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                            new  SqlParameter("@sims_month_code",data.sims_month_code),

                           new  SqlParameter("@lesson_user",data.lesson_user)


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_unit obj = new syllabus_unit();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_unit_code = dr["sims_syllabus_unit_code"].ToString();
                            obj.sims_syllabus_unit_created_by = dr["sims_syllabus_unit_created_by"].ToString();
                            obj.sims_syllabus_unit_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_unit_creation_date"].ToString());
                            obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                            obj.sims_syllabus_unit_short_desc = dr["sims_syllabus_unit_short_desc"].ToString();
                            obj.sims_syllabus_unit_status = dr["sims_syllabus_unit_status"].ToString().Equals("A") ? true : false;
                            obj.sims_display_name = dr["sims_grade_name_en"].ToString() + " / " + dr["sims_section_name_en"].ToString() + "- " + dr["sims_subject_name_en"].ToString() + "- " + dr["month_name"].ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("syllabus_Topic")]
        public HttpResponseMessage syllabus_Topic(syllabus_unit data)
        {
            List<syllabus_unit_topic> lst = new List<syllabus_unit_topic>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "UT"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code1),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@lesson_user",data.lesson_user)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_unit_topic obj = new syllabus_unit_topic();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_unit_code = dr["sims_syllabus_unit_code"].ToString();
                            obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                            obj.sims_syllabus_unit_topic_code = dr["sims_syllabus_unit_topic_code"].ToString();
                            obj.sims_syllabus_unit_topic_name = dr["sims_syllabus_unit_topic_name"].ToString();
                            obj.sims_syllabus_unit_topic_short_desc = dr["sims_syllabus_unit_topic_short_desc"].ToString();
                            obj.sims_syllabus_unit_topic_creation_date = dr["sims_syllabus_unit_topic_creation_date"].ToString();
                            obj.sims_syllabus_unit_topic_created_by = dr["sims_syllabus_unit_topic_created_by"].ToString();
                            obj.sims_syllabus_unit_topic_status = dr["sims_syllabus_unit_topic_status"].ToString().Equals("A") ? true : false;
                            obj.sims_display_name = dr["sims_grade_name_en"].ToString() + " / " + dr["sims_section_name_en"].ToString() + "- " + dr["sims_subject_name_en"].ToString() + "- " + dr["month_name"].ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("syllabus_Topic_Sub_Topics")]
        public HttpResponseMessage syllabus_Topic_Sub_Topics(syllabus_unit_topic data)
        {
            List<syllabus_unit_sub_topic> lst = new List<syllabus_unit_sub_topic>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "ST"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_code",data.sims_syllabus_unit_topic_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_unit_sub_topic obj = new syllabus_unit_sub_topic();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_unit_code = dr["sims_syllabus_unit_code"].ToString();
                            obj.sims_syllabus_unit_topic_code = dr["sims_syllabus_unit_topic_code"].ToString();
                            obj.sims_syllabus_unit_sub_topic_name = dr["sims_syllabus_unit_sub_topic_name"].ToString();
                            obj.sims_syllabus_unit_sub_topic_short_desc = dr["sims_syllabus_unit_sub_topic_short_desc"].ToString();
                            obj.sims_syllabus_unit_sub_topic_code = dr["sims_syllabus_unit_sub_topic_code"].ToString();
                            obj.sims_syllabus_unit_sub_topic_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_unit_sub_topic_creation_date"].ToString());
                            obj.sims_syllabus_unit_sub_topic_created_by = dr["sims_syllabus_unit_sub_topic_created_by"].ToString();
                            obj.sims_syllabus_unit_sub_topic_status = dr["sims_syllabus_unit_sub_topic_status"].ToString().Equals("A") ? true : false;

                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("syllabus_Topic_Sub_Topics_lesson_compose")]
        public HttpResponseMessage syllabus_Topic_Sub_Topics_lesson_compose(syllabus_and_lesson data)
        {
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "SS"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                            new  SqlParameter("@lesson_user",data.lesson_user)


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            compose_lesson_plan obj = new compose_lesson_plan();

                            string str= dr["checkedvalues"].ToString();

                            if (str=="A")
                            {
                                obj.sims_cur_code = dr["sims_cur_code"].ToString();
                                obj.sims_academic_year = dr["sims_academic_year"].ToString();
                                obj.sims_grade_code = dr["sims_grade_code"].ToString();
                                obj.sims_section_code = dr["sims_section_code1"].ToString();
                                obj.sims_subject_code = dr["sims_subject_code"].ToString();
                                obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                                obj.sims_month_code = dr["sims_month_code"].ToString();
                                obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                                obj.sims_syllabus_unit_topic_name = dr["sims_syllabus_unit_topic_name"].ToString();
                                obj.sims_syllabus_lesson_plan_code = dr["sims_syllabus_lesson_plan_code"].ToString();
                                obj.sims_syllabus_lesson_plan_description = dr["sims_syllabus_lesson_plan_description"].ToString();
                                obj.sims_syllabus_lesson_plan_no_of_periods = dr["sims_syllabus_lesson_plan_no_of_periods"].ToString();
                                obj.sims_syllabus_lesson_plan_no_of_exercise = dr["sims_syllabus_lesson_plan_no_of_exercise"].ToString();
                                obj.sims_syllabus_lesson_plan_no_of_activity = dr["sims_syllabus_lesson_plan_no_of_activity"].ToString();
                                obj.sims_syllabus_lesson_plan_no_of_weekly_test = dr["sims_syllabus_lesson_plan_no_of_weekly_test"].ToString();
                                obj.sims_syllabus_lesson_plan_addl_requirement = dr["sims_syllabus_lesson_plan_addl_requirement"].ToString();
                                obj.sims_syllabus_lesson_plan_creation_date = db.UIDDMMYYYYformat( dr["sims_syllabus_lesson_plan_creation_date"].ToString());
                                obj.sims_syllabus_lesson_plan_created_by = dr["sims_syllabus_lesson_plan_created_by"].ToString();
                                obj.sims_syllabus_lesson_plan_approval_date = db.UIDDMMYYYYformat( dr["sims_syllabus_lesson_plan_approval_date"].ToString());
                                obj.sims_syllabus_lesson_plan_no_of_homework = dr["sims_syllabus_lesson_plan_no_of_homework"].ToString();
                                obj.sims_syllabus_lesson_plan_approval_remark = dr["sims_syllabus_lesson_plan_approval_remark"].ToString();
                                obj.sims_syllabus_lesson_plan_approved_by = dr["sims_syllabus_lesson_plan_approved_by"].ToString();
                                obj.sims_syllabus_lesson_plan_attr1 = dr["sims_syllabus_lesson_plan_attr1"].ToString();
                                obj.sims_syllabus_lesson_plan_status = dr["sims_syllabus_lesson_plan_status"].ToString();
                                obj.sims_syllabus_lesson_plan_doc_path = dr["sims_syllabus_lesson_plan_doc_path"].ToString();
                                obj.checkedvalues = dr["checkedvalues"].ToString();
                                lst.Add(obj);
                                                               
                            }
                            else
                            {
                                obj.sims_cur_code = dr["sims_cur_code"].ToString();
                                obj.sims_academic_year = dr["sims_academic_year"].ToString();
                                obj.sims_grade_code = dr["sims_grade_code"].ToString();
                                obj.sims_section_code = dr["sims_section_code"].ToString();
                                obj.sims_subject_code = dr["sims_subject_code"].ToString();
                                obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                                obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                                obj.sims_syllabus_unit_topic_name = dr["sims_syllabus_unit_topic_name"].ToString();
                                obj.sims_month_code = dr["sims_month_code"].ToString();
                                obj.checkedvalues = dr["checkedvalues"].ToString();
                                lst.Add(obj);
                            }
                          }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("syllabus_Topic_Sub_Topics_lesson_Approve")]
        public HttpResponseMessage syllabus_Topic_Sub_Topics_lesson_Approve(syllabus_and_lesson data)
        {
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "AS"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_start_date",db.DBYYYYMMDDformat(data.sims_syllabus_start_date)),
                           new  SqlParameter("@sims_syllabus_end_date",db.DBYYYYMMDDformat(data.sims_syllabus_end_date)),
                            new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@lesson_user",data.sims_syllabus_created_by),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            compose_lesson_plan obj = new compose_lesson_plan();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code1"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            //obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                            obj.sims_syllabus_unit_topic_name = dr["sims_syllabus_unit_topic_name"].ToString();
                            obj.sims_syllabus_lesson_plan_code = dr["sims_syllabus_lesson_plan_code"].ToString();
                            obj.sims_syllabus_lesson_plan_description = dr["sims_syllabus_lesson_plan_description"].ToString();
                            obj.sims_syllabus_lesson_plan_no_of_periods = dr["sims_syllabus_lesson_plan_no_of_periods"].ToString();
                            obj.sims_syllabus_lesson_plan_no_of_exercise = dr["sims_syllabus_lesson_plan_no_of_exercise"].ToString();
                            obj.sims_syllabus_lesson_plan_no_of_activity = dr["sims_syllabus_lesson_plan_no_of_activity"].ToString();
                            obj.sims_syllabus_lesson_plan_no_of_weekly_test = dr["sims_syllabus_lesson_plan_no_of_weekly_test"].ToString();
                            obj.sims_syllabus_lesson_plan_addl_requirement = dr["sims_syllabus_lesson_plan_addl_requirement"].ToString();
                            obj.sims_syllabus_lesson_plan_creation_date = db.UIDDMMYYYYformat(dr["sims_syllabus_lesson_plan_creation_date"].ToString());
                            obj.sims_syllabus_lesson_plan_created_by = dr["sims_syllabus_lesson_plan_created_by"].ToString();
                            obj.sims_syllabus_lesson_plan_approval_date =db.UIDDMMYYYYformat( dr["sims_syllabus_lesson_plan_approval_date"].ToString());
                            obj.sims_syllabus_lesson_plan_no_of_homework = dr["sims_syllabus_lesson_plan_no_of_homework"].ToString();
                            obj.sims_syllabus_lesson_plan_approval_remark = dr["sims_syllabus_lesson_plan_approval_remark"].ToString();
                            obj.sims_syllabus_lesson_plan_approved_by = dr["sims_syllabus_lesson_plan_approved_by"].ToString();
                            obj.sims_syllabus_lesson_plan_attr1 = dr["sims_syllabus_lesson_plan_attr1"].ToString();
                            obj.sims_syllabus_lesson_plan_status = dr["sims_syllabus_lesson_plan_status"].ToString();
                            obj.sims_syllabus_lesson_plan_doc_path = dr["sims_syllabus_lesson_plan_doc_path"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }
       
        [Route("CUD_syllabus_Topic_Sub_Topics_lesson_Approve")]
        public HttpResponseMessage CUD_syllabus_Topic_Sub_Topics_lesson_Approve(List<compose_lesson_plan> data1)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (compose_lesson_plan data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                             {
                           new  SqlParameter("@opr", "AL"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_created_by",data.sims_syllabus_lesson_plan_created_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_approval_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approved_by",data.sims_syllabus_lesson_plan_approved_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_remark",data.sims_syllabus_lesson_plan_approval_remark),
                           new  SqlParameter("@sims_syllabus_lesson_plan_status",data.sims_syllabus_lesson_plan_status),
                             });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;

                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
        
        [Route("CUDsyllabus_Topic_Sub_Topics_lesson_compose")]
        public HttpResponseMessage CUDsyllabus_Topic_Sub_Topics_lesson_compose(List<compose_lesson_plan> data1)
        {
            string lesson_plan_code = "";
            bool inserted = false;
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (compose_lesson_plan data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                             {
                           new  SqlParameter("@opr", "LI"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_code",data.sims_syllabus_lesson_plan_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_description",data.sims_syllabus_lesson_plan_description),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_periods",data.sims_syllabus_lesson_plan_no_of_periods),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_exercise",data.sims_syllabus_lesson_plan_no_of_exercise),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_activity",data.sims_syllabus_lesson_plan_no_of_activity),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_homework",data.sims_syllabus_lesson_plan_no_of_homework),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_weekly_test",data.sims_syllabus_lesson_plan_no_of_weekly_test),
                           new  SqlParameter("@sims_syllabus_lesson_plan_addl_requirement",data.sims_syllabus_lesson_plan_addl_requirement),
                           new  SqlParameter("@sims_syllabus_lesson_plan_creation_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_creation_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_created_by",data.sims_syllabus_lesson_plan_created_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_approval_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approved_by",data.sims_syllabus_lesson_plan_approved_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_remark",data.sims_syllabus_lesson_plan_approval_remark),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr1",data.sims_syllabus_lesson_plan_attr1),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr2",data.sims_syllabus_lesson_plan_attr2),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr3",data.sims_syllabus_lesson_plan_attr3),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr4",data.sims_syllabus_lesson_plan_attr4),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr5",data.sims_syllabus_lesson_plan_attr5),
                           new  SqlParameter("@sims_syllabus_lesson_plan_status",data.sims_syllabus_lesson_plan_status.Equals(true)?"A":"I")

                              });
                        if (dr.RecordsAffected > 0)
                        {
                            while (dr.Read())
                            {
                                lesson_plan_code = dr["sims_syllabus_lesson_plan_code"].ToString();
                            }
                            dr.Close();
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                           new List<SqlParameter>()
                             {
                           new  SqlParameter("@opr", "LD"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_code",lesson_plan_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_path",data.sims_syllabus_lesson_plan_doc_path),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_status",data.sims_syllabus_lesson_plan_doc_status),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_upload_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_doc_upload_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_uploaded_by",data.sims_syllabus_lesson_plan_doc_uploaded_by)
                             });
                            if (dr1.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr1.Close();
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("CUDsyllabus_Topic_Sub_Topics_lesson_compose_for_Update")]
        public HttpResponseMessage CUDsyllabus_Topic_Sub_Topics_lesson_compose_for_Update(List<compose_lesson_plan> data1)
        {
             
            bool inserted = false;
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (compose_lesson_plan data in data1)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert ("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                             {
                           new  SqlParameter("@opr", "UL"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_code",data.sims_syllabus_lesson_plan_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_description",data.sims_syllabus_lesson_plan_description),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_periods",data.sims_syllabus_lesson_plan_no_of_periods),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_exercise",data.sims_syllabus_lesson_plan_no_of_exercise),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_activity",data.sims_syllabus_lesson_plan_no_of_activity),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_homework",data.sims_syllabus_lesson_plan_no_of_homework),
                           new  SqlParameter("@sims_syllabus_lesson_plan_no_of_weekly_test",data.sims_syllabus_lesson_plan_no_of_weekly_test),
                           new  SqlParameter("@sims_syllabus_lesson_plan_addl_requirement",data.sims_syllabus_lesson_plan_addl_requirement),
                           new  SqlParameter("@sims_syllabus_lesson_plan_creation_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_creation_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_created_by",data.sims_syllabus_lesson_plan_created_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_date",db.DBYYYYMMDDformat(data.sims_syllabus_lesson_plan_approval_date)),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approved_by",data.sims_syllabus_lesson_plan_approved_by),
                           new  SqlParameter("@sims_syllabus_lesson_plan_approval_remark",data.sims_syllabus_lesson_plan_approval_remark),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr1",data.sims_syllabus_lesson_plan_attr1),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr2",data.sims_syllabus_lesson_plan_attr2),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr3",data.sims_syllabus_lesson_plan_attr3),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr4",data.sims_syllabus_lesson_plan_attr4),
                           new  SqlParameter("@sims_syllabus_lesson_plan_attr5",data.sims_syllabus_lesson_plan_attr5),
                           new  SqlParameter("@sims_syllabus_lesson_plan_status",data.sims_syllabus_lesson_plan_status.Equals(true)?"A":"I")

                              });
                        if (dr > 0)
                        {
                             
                            int dr1 = db.ExecuteStoreProcedureforInsert("[sims].[sims_syllabus_creation_proc]",
                           new List<SqlParameter>()
                             {
                           new  SqlParameter("@opr", "LU"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_code",data.sims_syllabus_lesson_plan_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_path",data.sims_syllabus_lesson_plan_doc_path),
                           new  SqlParameter("@sims_syllabus_lesson_plan_doc_uploaded_by",data.sims_syllabus_lesson_plan_doc_uploaded_by)
                             });
                            if (dr1 > 0)
                            {
                                inserted = true;
                            }
                            
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

        [Route("CUD_syllabus_Units")]
        public HttpResponseMessage CUD_syllabus_Units(List<syllabus_unit> data1)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_unit data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", data.opr),
                           new SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_name",data.sims_syllabus_unit_name),
                           new  SqlParameter("@sims_syllabus_unit_short_desc",data.sims_syllabus_unit_short_desc),
                           new  SqlParameter("@sims_syllabus_unit_creation_date", db.DBYYYYMMDDformat(data.sims_syllabus_unit_creation_date)),
                           new  SqlParameter("@sims_syllabus_unit_created_by",data.sims_syllabus_unit_created_by), 
                           new  SqlParameter("@sims_syllabus_unit_status",data.sims_syllabus_unit_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("CUD_syllabus_Topics")]
        public HttpResponseMessage CUD_syllabus_Topics(List<syllabus_unit_topic> data1)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_unit_topic data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", data.opr),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_code",data.sims_syllabus_unit_topic_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_name",data.sims_syllabus_unit_topic_name),
                           new  SqlParameter("@sims_syllabus_unit_topic_short_desc",data.sims_syllabus_unit_topic_short_desc),
                           new  SqlParameter("@sims_syllabus_unit_topic_creation_date",db.DBYYYYMMDDformat(data.sims_syllabus_unit_topic_creation_date)),
                           new  SqlParameter("@sims_syllabus_unit_topic_created_by",data.sims_syllabus_unit_topic_created_by), 
                           new  SqlParameter("@sims_syllabus_unit_topic_status",data.sims_syllabus_unit_topic_status.Equals(true)?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("CUD_syllabus_Sub_Topics")]
        public HttpResponseMessage CUD_syllabus_Sub_Topics(syllabus_unit_sub_topic data)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", data.opr),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code), 
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_code",data.sims_syllabus_unit_topic_code),
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_code",data.sims_syllabus_unit_sub_topic_code),
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_name",data.sims_syllabus_unit_sub_topic_name),
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_short_desc",data.sims_syllabus_unit_sub_topic_short_desc),
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_creation_date", db.DBYYYYMMDDformat(data.sims_syllabus_unit_sub_topic_creation_date)),
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_created_by",data.sims_syllabus_unit_sub_topic_created_by), 
                           new  SqlParameter("@sims_syllabus_unit_sub_topic_status",data.sims_syllabus_unit_sub_topic_status.Equals(true)?"A":"I")
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        insrted = true;
                    }
                    dr.Close();
                }

            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("CUDsyllabuslesson")]
        public HttpResponseMessage CUDsyllabuslesson(List<syllabus_and_lesson> data1)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_and_lesson data in data1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                             {
                           new SqlParameter("@opr", data.opr),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_start_date", data.sims_syllabus_start_date),
                           new  SqlParameter("@sims_syllabus_end_date", data.sims_syllabus_end_date),
                           new  SqlParameter("@sims_syllabus_remark",data.sims_syllabus_remark),
                           new  SqlParameter("@sims_syllabus_creation_date", db.DBYYYYMMDDformat(data.sims_syllabus_creation_date)),
                           new  SqlParameter("@sims_syllabus_created_by",data.sims_syllabus_created_by),
                           new  SqlParameter("@sims_syllabus_status",data.sims_syllabus_status.Equals(true)?"A":"I")
                             });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("CUDUpdatesyllabuslesson")]
        public HttpResponseMessage CUDUpdatesyllabuslesson(syllabus_and_lesson data)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                   
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                             {
                           new SqlParameter("@opr", data.opr),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data.sims_syllabus_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                           new  SqlParameter("@sims_syllabus_start_date",db.DBYYYYMMDDformat(data.sims_syllabus_start_date)),
                           new  SqlParameter("@sims_syllabus_end_date", db.DBYYYYMMDDformat(data.sims_syllabus_end_date)),
                           new  SqlParameter("@sims_syllabus_remark",data.sims_syllabus_remark),
                           new  SqlParameter("@sims_syllabus_creation_date", db.DBYYYYMMDDformat(data.sims_syllabus_creation_date)),
                           new  SqlParameter("@sims_syllabus_created_by",data.sims_syllabus_created_by),
                           new  SqlParameter("@sims_syllabus_status",data.sims_syllabus_status.Equals(true)?"A":"I")
                             });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                        dr.Close();
                    }
                
            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }


        [Route("CUDeletesyllabuslesson")]
        public HttpResponseMessage CUDeletesyllabuslesson(List<syllabus_and_lesson> data)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_and_lesson data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "D"),
                           new  SqlParameter("@sims_cur_code",data1.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data1.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data1.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data1.sims_section_code), 
                           new  SqlParameter("@sims_subject_code",data1.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data1.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_created_by",data1.sims_syllabus_created_by),
                         
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
               // insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("CUDeletesyllabuslessonUnit")]
        public HttpResponseMessage CUDeletesyllabuslessonUnit(List<syllabus_unit> data)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_unit data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "UD"),
                           new  SqlParameter("@sims_cur_code",data1.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data1.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data1.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data1.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data1.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data1.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data1.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_created_by",data1.sims_syllabus_unit_created_by)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
               // insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }


        [Route("CUDeletesyllabuslessonUnitTopic")]
        public HttpResponseMessage CUDeletesyllabuslessonUnitTopic(List<syllabus_unit_topic> data)
        {
            bool insrted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (syllabus_unit_topic data1 in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                            new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "TD"),
                           new  SqlParameter("@sims_cur_code",data1.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data1.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data1.sims_grade_code),
                           new  SqlParameter("@sims_section_code",data1.sims_section_code),
                           new  SqlParameter("@sims_subject_code",data1.sims_subject_code),
                           new  SqlParameter("@sims_syllabus_code",data1.sims_syllabus_code),
                           new  SqlParameter("@sims_syllabus_unit_code",data1.sims_syllabus_unit_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_code",data1.sims_syllabus_unit_topic_code),
                           new  SqlParameter("@sims_syllabus_unit_topic_created_by",data1.sims_syllabus_unit_topic_created_by)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }


        #endregion

        [Route("syllabus_Topic_Sub_Topics_lesson_for_compose")]
        public HttpResponseMessage syllabus_Topic_Sub_Topics_lesson_for_compose(syllabus_and_lesson data)
        {
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "CC"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new SqlParameter("@lesson_user",data.lesson_user)
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            compose_lesson_plan obj = new compose_lesson_plan();

                                
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.month_name = dr["month_name"].ToString();
                            obj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            obj.sims_section_name = dr["sims_section_name"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_cur_name_en = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.checkedvalues = dr["checkedvalues"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }




        #region (Syllabus Unit Topic Creation)
        #endregion


        #region (Syllabus Unit Topic Sub Topic Creation)
        #endregion


    }
}