﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.CalendarExceptionStudentController
{
    [RoutePrefix("api/common/CalendarExceptionStudent")]
    [BasicAuthentication]
    public class CalendarExceptionStudentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("GetSimsHolidayStudent")]
        public HttpResponseMessage GetSimsHolidayStudent()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims027> list = new List<Sims027>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_holiday_student_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims027 simsobj = new Sims027();

                            simsobj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj._sims_cur_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj._sims_holiday_holiday_desc = dr["sims_holiday_desc"].ToString();
                            simsobj._sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj._sims_section_name = dr["sims_section_name_en"].ToString();
                            if (dr["sims_status"].ToString() == "A")
                                simsobj._sims_doc_status = true;
                            else if (dr["sims_status"].ToString() == "I")
                                simsobj._sims_doc_status = false;

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("GetHolidayDesc")]
        public HttpResponseMessage GetHolidayDesc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineAlert(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineAlert"));

            List<Sims027> list = new List<Sims027>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_holiday_student_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                                           new SqlParameter("@opr", "A")

                
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims027 simsobj = new Sims027();
                            simsobj._sims_holiday_holiday_desc = dr["sims_calendar_exception_description"].ToString();
                            simsobj.sims_calendar_exception_number = dr["sims_calendar_exception_number"].ToString();
                   
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("InsertSimsHolidayStudent")]
        public HttpResponseMessage InsertSimsHolidayStudent(Sims027 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_holiday_student_proc]",
                        new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", simsobj.opr),
                new SqlParameter("@sims_holiday_number", simsobj._sims_holiday_holiday_desc),
                new SqlParameter("@sims_cur_code", simsobj._sims_cur_cur_name),
                new SqlParameter("@sims_grade_code", simsobj._sims_grade_name),
                new SqlParameter("@sims_section_code", simsobj._sims_section_name),
                 new SqlParameter("@sims_status", simsobj._sims_doc_status ? "A" : "I")
                
                 

                             
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


      
       


    }
}