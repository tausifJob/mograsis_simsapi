﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.CalendarExceptionEmployeeController
{
    [RoutePrefix("api/common/CalendarExceptionEmployee")]
    [BasicAuthentication]
    public class CalendarExceptionEmployeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("GetAll_Sims_Holiday_Employee")]
        public HttpResponseMessage GetAll_Sims_Holiday_Employee()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMedicineType(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "STUDENT", "GetMedicineType"));

            List<Sims026> list = new List<Sims026>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_holiday_employee_proc]",
                        new List<SqlParameter>() 
                         { 
                         
                new SqlParameter("@opr", "S"),
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims026 simsobj = new Sims026();
                            simsobj.sims_holiday_number = dr["sims_calendar_exception_number"].ToString();
                            simsobj.sims_holiday_desc = dr["sims_calendar_exception_description"].ToString();
                            simsobj.sims_employee_group = dr["sims_employee_group"].ToString();
                            simsobj.sims_employee_group_nm = dr["sims_employee_group_nm"].ToString();

                            if (dr["sims_status"].ToString() == "A")
                                simsobj.sims_status = true;
                            else if (dr["sims_status"].ToString() == "I")
                                simsobj.sims_status = false;

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("InsertSims_Holiday_Employee")]
        public HttpResponseMessage InsertSims_Holiday_Employee(Sims026 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_MedicalImmunization()";
            Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_holiday_employee_proc]",
                        new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", simsobj.opr),
                    new SqlParameter("@sims_holiday_number", simsobj.sims_holiday_number),
                    new SqlParameter("@sims_employee_group", simsobj.sims_employee_group),
                    new SqlParameter("@sims_status", simsobj.sims_status),

                 

                             
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


      
       


    }
}