﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/house")]
    public class HouseController : ApiController
    {
        public string sims_house_color = "";
        public string section_term_section_code { get; set; }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getHouseNumber")]
        public HttpResponseMessage getHouseNumber(string curCode, string curYear)
        {
            string houseNumber = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_house_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "M"),
                            new SqlParameter("@sims_cur_code", curCode),
                            new SqlParameter("@sims_cur_year", curYear)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            houseNumber = "0" + dr["houseNumber"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, houseNumber);
        }

        //Insert Records

        [Route("HouseCUD")]
        public HttpResponseMessage HouseCUD(List<Sims029> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "HouseCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims029 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_house_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_cur_year",simsobj.sims_academic_year),
                          new SqlParameter("@sims_house_code", simsobj.sims_house_code),
                          new SqlParameter("@sims_house_name",simsobj.sims_house_name),
                          new SqlParameter ("@sims_house_color",simsobj.sims_house_color),
                          new SqlParameter ("@sims_house_objective",simsobj.sims_house_objective),
                          new SqlParameter ("@sims_house_pledge",simsobj.sims_house_pledge),
                          new SqlParameter("@sims_house_status",simsobj.sims_house_status==true?"A":"I")

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //Get Records

        [Route("getHouse")]
        public HttpResponseMessage getHouse()
        {
            List<Sims029> house = new List<Sims029>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_house_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims029 obj = new Sims029();


                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_cur_year"].ToString();
                            obj.sims_house_code = dr["sims_house_code"].ToString();
                            obj.sims_house_name = dr["sims_house_name"].ToString();
                            obj.sims_house_color = dr["sims_house_color"].ToString();
                            obj.sims_house_objective = dr["sims_house_objective"].ToString();
                            obj.sims_house_pledge = dr["sims_house_pledge"].ToString();
                            obj.sims_house_status = dr["sims_house_status"].Equals("A") ? true : false;
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();

                            house.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("chechcolor")]
        public HttpResponseMessage chechcolor(string curCode, string curYear)
        {

            List<color> house = new List<color>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_house_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "C"),
                            new SqlParameter("@sims_cur_code", curCode),
                            new SqlParameter("@sims_cur_year", curYear)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            color col = new color();
                            col.sims_house_color = dr["sims_house_color"].ToString();
                            col.sims_house_code = dr["sims_house_code"].ToString();
                            house.Add(col);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

    }

}