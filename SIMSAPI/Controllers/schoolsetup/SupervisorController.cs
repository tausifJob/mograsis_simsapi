﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
     [RoutePrefix("api/ERP/SupervisorMapping")]
    public class SupervisorController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("getSupervisorDetails")]
         public HttpResponseMessage getSupervisorDetails(string supervisor_code)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSupervisorDetails()";
             Log.Debug(string.Format(debug, "PP", "getSupervisorDetails"));

             List<Sims187> Subject_lst = new List<Sims187>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_supervisor_proc]",
                         new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'T'),
                             new SqlParameter("@sims_supervisor_code", supervisor_code),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims187 Simsobj = new Sims187();
                             Simsobj.sims_supervisor_code = dr["em_login_code"].ToString();
                             Simsobj.sims_supervisor_name = dr["emp_name"].ToString();
                             Subject_lst.Add(Simsobj);
                         }
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
             }
             return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
         }

        [Route("GetAllSims_Supervisor")]
         public HttpResponseMessage GetAllSims_Supervisor(string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllSims_Supervisor()";
            Log.Debug(string.Format(debug, "PP", "GetAllSims_Supervisor"));
            int cnt = 1;

            List<Sims187> Subject_lst = new List<Sims187>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_supervisor_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),
                             new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year", acad_yr),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims187 simsobj = new Sims187();
                            simsobj.myid = cnt;
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_supervisor_code = dr["sims_supervisor_code"].ToString();
                            simsobj.sims_supervisor_name = dr["sims_supervisor_name"].ToString();
                            if (dr["sims_supervisor_status"].ToString().Equals("A"))
                                simsobj.sims_supervisor_status = true;
                            else
                                simsobj.sims_supervisor_status = false;
                            Subject_lst.Add(simsobj);
                            cnt++;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
        }

        [Route("CUD_Supervisor_Mapping")]
        public HttpResponseMessage CUD_Supervisor_Mapping(List<Sims187> lst_simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Supervisor_Mapping(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Supervisor/", "CUD_Supervisor_Mapping"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (lst_simsobj != null)
                {

                    using (DBConnection db = new DBConnection())
                    {

                        db.Open();

                        // SqlDataReader dr;
                        foreach (Sims187 simsobj in lst_simsobj)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_supervisor_proc]",
                            new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_supervisor_code", simsobj.sims_supervisor_code),
                                new SqlParameter("@sims_supervisor_status", simsobj.sims_supervisor_status==true?"A":"I")
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}