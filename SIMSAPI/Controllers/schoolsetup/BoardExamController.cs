﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/BoardExamDetail")]
    public class BoardExamController : ApiController
    {
        [Route("getBoardExamDetail")]
        public HttpResponseMessage getBoardExamDetail()
        {
            List<Sims184> BoardExam = new List<Sims184>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_exam_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims184 obj = new Sims184();


                            obj.sims_board_code = dr["sims_board_code"].ToString();
                            obj.sims_board_exam_code = dr["sims_board_exam_code"].ToString();
                            obj.sims_board_exam_short_name = dr["sims_board_exam_short_name"].ToString();
                            obj.sims_board_exam_name = dr["sims_board_exam_name"].ToString();
                            obj.sims_board_exam_year = dr["sims_board_exam_year"].ToString();
                            obj.sims_board_exam_fee = dr["sims_board_exam_fee"].ToString();

                            if (dr["sims_board_exam_status"].ToString() == "A")
                                obj.sims_board_exam_status = true;
                            else
                                obj.sims_board_exam_status = false;
                            BoardExam.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, BoardExam);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, BoardExam);
        }

        [Route("getBoardCode")]
        public HttpResponseMessage getBoardCode()
        {
            List<Sims184> BoardExam = new List<Sims184>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_board_exam_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims184 obj = new Sims184();


                            obj.sims_board_code = dr["sims_board_code"].ToString();
                            obj.sims_board_short_name = dr["sims_board_short_name"].ToString();
                            BoardExam.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, BoardExam);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, BoardExam);
        }

        [Route("BoardExamCUD")]
        public HttpResponseMessage BoardExamCUD(List<Sims184> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims184 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_board_exam_proc]",
                            new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_board_code", simsobj.sims_board_code),
                                 new SqlParameter("@sims_board_exam_code", simsobj.sims_board_exam_code),
                                 new SqlParameter("@sims_board_exam_short_name", simsobj.sims_board_exam_short_name),
                                 new SqlParameter("@sims_board_exam_name", simsobj.sims_board_exam_name),
                                 new SqlParameter("@sims_board_exam_year", simsobj.sims_board_exam_year),
                                  new SqlParameter("@sims_board_exam_fee", simsobj.sims_board_exam_fee),
                                 new SqlParameter("@sims_board_exam_status", simsobj.sims_board_exam_status==true?"A":"I")

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
