﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/ERP/Grade")]

    public class GradeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCuriculum()PARAMETERS ::Username";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fetch_cur_by_emp_id]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@sims_employee_code", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_full_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getCuriculum_goa")]
        public HttpResponseMessage getCuriculum_goa(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCuriculum()PARAMETERS ::Username";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fetch_cur_for_attendance]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@sims_employee_code", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_full_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCuriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims024> lstCuriculum = new List<Sims024>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims024 sequence = new Sims024();
                            sequence.grade_cur_code = dr["sims_cur_code"].ToString();
                            sequence.grade_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getCuriculum_new")]
        public HttpResponseMessage getCuriculum_new()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCuriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_for_specific_user]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getCuriculumfor_Company")]
        public HttpResponseMessage getCuriculumfor_Company(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCuriculumfor_Company()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_for_specific_user]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", "B"),
                           new SqlParameter("@comp_code", comp_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }



        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims024> lstModules = new List<Sims024>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_head_teacher_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code",curCode),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims024 sequence = new Sims024();
                            sequence.grade_cur_code = dr["sims_cur_code"].ToString();
                            sequence.grade_academic_year = dr["sims_academic_year"].ToString();
                            sequence.grade_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getGrade")]
        public HttpResponseMessage getGrade()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            //int total = 0, skip = 0, cnt = 1;
            int cnt = 1;
            List<Sims024> lstgrades = new List<Sims024>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_grade_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims024 cur = new Sims024();
                            cur.myid = cnt;
                            cur.grade_cur_code = dr["sims_cur_code"].ToString();
                            cur.grade_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            cur.grade_academic_year = dr["sims_academic_year"].ToString();
                            cur.grade_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            cur.grade_code = dr["sims_grade_code"].ToString();
                            cur.grade_name_en = dr["sims_grade_name_en"].ToString();
                            cur.grade_name_ar = dr["sims_grade_name_ar"].ToString();
                            cur.grade_name_fr = dr["sims_grade_name_fr"].ToString();
                            cur.grade_name_ot = dr["sims_grade_name_ot"].ToString();
                            cur.comp_code = dr["sims_comp_code"].ToString();
                            cur.comp_name = dr["sims_comp_name"].ToString();

                            cur.sims_grade_status = dr["sims_grade_status"].Equals("A") ? true : false;
                            try
                            {
                                cur.sims_display_order = dr["sims_display_order"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            lstgrades.Add(cur);
                            // total = lstgrades.Count;
                            //skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstgrades);
        }

        [Route("CUDInsertGrade")]
        public HttpResponseMessage CUDInsertGrade(List<Sims024> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims024 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_grade_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.grade_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.grade_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_grade_name_en", simsobj.grade_name_en),
                            new SqlParameter("@sims_grade_name_ar", simsobj.grade_name_ar),
                            new SqlParameter("@sims_grade_name_fr", simsobj.grade_name_fr),
                            new SqlParameter("@sims_grade_name_ot", simsobj.grade_name_ot),
                            new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                            new SqlParameter("@sims_comp_code", simsobj.comp_code),
                            new SqlParameter("@sims_grade_status",simsobj.sims_grade_status.Equals(true)?"A":"I")
                         });
                            while (dr.Read())
                            {
                                string cnt = dr["grade_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDGradeUpdate")]
        public HttpResponseMessage CUDGradeUpdate(List<Sims024> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims024 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_grade_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.grade_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.grade_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_grade_name_en", simsobj.grade_name_en),
                            new SqlParameter("@sims_grade_name_ar", simsobj.grade_name_ar),
                            new SqlParameter("@sims_grade_name_fr", simsobj.grade_name_fr),
                            new SqlParameter("@sims_grade_name_ot", simsobj.grade_name_ot),
                            new SqlParameter("@sims_display_order", simsobj.sims_display_order),
                            new SqlParameter("@sims_comp_code", simsobj.comp_code),
                            new SqlParameter("@sims_grade_status",simsobj.sims_grade_status.Equals(true)?"A":"I")
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCompany()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            List<Sims024> lstModules = new List<Sims024>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_grade_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims024 sequence = new Sims024();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAccecssUser")]
        public HttpResponseMessage getAccecssUser(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAccecssUser()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));

            bool count = false;
          //  List<Sims02422> lstModules = new List<Sims02422>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_for_specific_user]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_user_name", user)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           // Sims02422 sequence = new Sims02422();
                            if (int.Parse(dr["cnt"].ToString()) > 0)
                            {
                                count = true;

                            }
                            
                          //  lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, count);
        }



    }
}