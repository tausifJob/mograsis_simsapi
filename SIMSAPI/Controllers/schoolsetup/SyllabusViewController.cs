﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/syllabus_lesson_PP")]
    public class SyllabusViewController:ApiController
    {

        [Route("get_syllabus_lesson_for_PP")]
        public HttpResponseMessage get_syllabus_lesson_for_PP()
        {
            List<syllabus_and_lesson> lst = new List<syllabus_and_lesson>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "TS")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            syllabus_and_lesson obj = new syllabus_and_lesson();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            //obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_creation_date = dr["sims_syllabus_creation_date"].ToString();
                            obj.sims_syllabus_created_by = dr["sims_syllabus_created_by"].ToString();
                            obj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            obj.sims_syllabus_start_date = dr["sims_syllabus_start_date"].ToString();
                            obj.sims_syllabus_end_date = dr["sims_syllabus_end_date"].ToString();
                            obj.sims_syllabus_lesson_plan_doc_path = dr["sims_syllabus_lesson_plan_doc_path"].ToString();
                            obj.Approved_status = dr["Approved_status"].ToString();
                            obj.sims_syllabus_created_name = dr["sims_syllabus_created_by1"].ToString();
                            obj.sims_syllabus_approved_name = dr["sims_syllabus_lesson_plan_approved_name"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                            obj.month_name = dr["month_name"].ToString();
                            lst.Add(obj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("syllabus_Approve_PP")]
        public HttpResponseMessage syllabus_Approve_PP(syllabus_and_lesson data)
        {
            List<compose_lesson_plan> lst = new List<compose_lesson_plan>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_syllabus_creation_proc]",
                        new List<SqlParameter>()
                         {
                           new  SqlParameter("@opr", "PP"),
                           new  SqlParameter("@sims_cur_code",data.sims_cur_code),
                           new  SqlParameter("@sims_academic_year",data.sims_academic_year),
                           new  SqlParameter("@sims_grade_code",data.sims_grade_code),
                           new  SqlParameter("@sims_subject_code",data.sims_subject_code),
                           new  SqlParameter("@sims_month_code",data.sims_month_code),
                            new  SqlParameter("@sims_section_code",data.sims_section_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            compose_lesson_plan obj = new compose_lesson_plan();

                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_syllabus_code = dr["sims_syllabus_code"].ToString();
                            obj.sims_syllabus_unit_name = dr["sims_syllabus_unit_name"].ToString();
                            obj.sims_syllabus_unit_topic_name = dr["sims_syllabus_unit_topic_name"].ToString();
                            obj.sims_month_code = dr["sims_month_code"].ToString();
                           
                            
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

    }
}