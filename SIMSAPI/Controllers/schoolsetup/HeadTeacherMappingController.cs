﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.schoolsetup
{
    [RoutePrefix("api/ERP/HeadTeacherMapping")]
    public class HeadTeacherMappingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSubjects")]
        public HttpResponseMessage getSubjects(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjects()";
            Log.Debug(string.Format(debug, "PP", "getSubjects"));

            List<Sims186> Subject_lst = new List<Sims186>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_head_teacher_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'H'),
                             new SqlParameter("@sims_cur_code", cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims186 Simsobj = new Sims186();
                            Simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            Simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            Subject_lst.Add(Simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
        }

        [Route("getTeacherDetails")]
        public HttpResponseMessage getTeacherDetails(string head_teacher_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjects()";
            Log.Debug(string.Format(debug, "PP", "getSubjects"));

            List<Sims186> Subject_lst = new List<Sims186>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_head_teacher_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'T'),
                             new SqlParameter("@sims_head_teacher_code", head_teacher_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims186 Simsobj = new Sims186();
                            Simsobj.sims_head_teacher_code = dr["sims_teacher_code"].ToString();
                            Simsobj.sims_head_teacher_name = dr["sims_teacher_name"].ToString();
                            Subject_lst.Add(Simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
        }

        [Route("getSims_Head_Teacher")]
        public HttpResponseMessage getSims_Head_Teacher(string cur_code, string acad_yr, string subject_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSims_Head_Teacher()";
            Log.Debug(string.Format(debug, "PP", "getSims_Head_Teacher"));
            int cnt = 1;

            List<Sims186> Subject_lst = new List<Sims186>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_head_teacher_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'S'),
                             new SqlParameter("@sims_cur_code",cur_code),
                             new SqlParameter("@sims_academic_year", acad_yr),
                             new SqlParameter("@sims_subject_code", subject_code)
                             // cmd.Parameters.AddWithValue("@sims_head_teacher_code", teacher_cd);
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims186 simsobj = new Sims186();
                            simsobj.myid = cnt;
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_head_teacher_code = dr["sims_head_teacher_code"].ToString();
                            simsobj.sims_head_teacher_name = dr["sims_head_teacher_name"].ToString();
                            if (dr["sims_head_teacher_status"].ToString().Equals("A"))
                                simsobj.sims_head_teacher_status = true;
                            else
                                simsobj.sims_head_teacher_status = false;
                            Subject_lst.Add(simsobj);
                            cnt++;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Subject_lst);
        }

        [Route("CUD_Head_Teacher_Mapping")]
        public HttpResponseMessage CUD_Head_Teacher_Mapping(List<Sims186> lst_simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUD_Head_Teacher_Mapping(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Admission/", "CUD_Head_Teacher_Mapping"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (lst_simsobj != null)
                {

                    using (DBConnection db = new DBConnection())
                    {

                        db.Open();

                        // SqlDataReader dr;
                        foreach (Sims186 simsobj in lst_simsobj)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_head_teacher_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                                new SqlParameter("@sims_head_teacher_code", simsobj.sims_head_teacher_code),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("sims_head_teacher_status", simsobj.sims_head_teacher_status==true?"A":"I")
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            // dr.Close();

                        }
                    }
                }

            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}