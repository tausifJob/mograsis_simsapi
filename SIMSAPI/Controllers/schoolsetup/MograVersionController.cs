﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.SchoolLicenseController
{
    [RoutePrefix("api/MograVersionDetails")]
    public class MograVersionController : ApiController
    {
        /// <summary>
        /// Priyanka Application
        /// </summary>
        /// <returns></returns>
        [Route("GetProjectNames")]
        public HttpResponseMessage GetProjectNames()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() { new SqlParameter("@opr", "P") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_appl_project_code = dr["sims_appl_parameter"].ToString();
                            pn.comn_appl_project_name = dr["sims_appl_form_field_value1"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetProjectModules")]
        public HttpResponseMessage GetProjectModules()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() { new SqlParameter("@opr", "M") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_mod_code = dr["comn_mod_code"].ToString();
                            pn.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetProjectApplications")]
        public HttpResponseMessage GetProjectApplications(string mod_code)
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "MA"),
                        new SqlParameter("@ModuleCode", mod_code),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_appl_code = dr["comn_appl_code"].ToString();
                            pn.comn_appl_name = dr["comn_appl_name_en"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetMograVersionsDetails")]
        public HttpResponseMessage GetMograVersionsDetails()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "SM"),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_appl_code = dr["ChangesetNo"].ToString();
                            pn.comn_appl_name = dr["ProjectName"].ToString();
                            pn.comn_appl_name = dr["DeveloperName"].ToString();
                            pn.comn_appl_name = dr["DevelopedDate"].ToString();
                            pn.comn_appl_name = dr["DeployedDate"].ToString();
                            if (pn.mogra_details == null)
                                pn.mogra_details = new List<Mogra_Version_Details>();
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                                new List<SqlParameter>() {
                                    new SqlParameter("@opr", "SD"),
                                    new SqlParameter("@ChangesetNo", pn.changeset_code),
                            });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_Version_Details vd = new Mogra_Version_Details();
                                        vd.changeset_code = pn.changeset_code;
                                        vd.change_id = dr1["ChangeID"].ToString();
                                        vd.comn_mod_code = dr1["ModuleName"].ToString();
                                        vd.comn_appl_code = dr1["ApplicationName"].ToString();
                                        vd.comn_mod_name = dr1["module_name_en"].ToString();
                                        vd.comn_appl_name = dr1["appl_name_en"].ToString();
                                        vd.is_doc_done = dr1["DocDone"].ToString().Equals("A") ? true : false;
                                        vd.is_test_case_design = dr1["TestCaseDesign"].ToString().Equals("A") ? true : false;
                                        vd.comn_version_remark = dr1["Remark"].ToString();
                                        vd.sp_name = dr1["spName"].ToString();
                                        vd.sp_remark = dr1["spremark"].ToString();

                                        pn.mogra_details.Add(vd);
                                    }
                                }
                                lst.Add(pn);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUD_SaveMograVersionsDetails")]
        public HttpResponseMessage CUD_SaveMograVersionsDetails(Mogra_Version v)
        {
            string result = "true";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "IM"),
                        new SqlParameter("@ChangesetNo", v.changeset_code),
                        new SqlParameter("@ProjectName", v.comn_appl_project_name),
                        new SqlParameter("@DeveloperName", v.comn_developer_name),
                        new SqlParameter("@DevelopedDate", v.comn_developed_date),
                        new SqlParameter("@DeployedDate", v.comn_deployed_date),
                        });
                    int r = dr.RecordsAffected;

                    dr.Close();
                    if (r > 0)
                    {
                        foreach (Mogra_Version_Details vd in v.mogra_details)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                            new List<SqlParameter>() {
                                new SqlParameter("@opr", "ID"),
                                new SqlParameter("@ChangesetNo", vd.changeset_code),
                                new SqlParameter("@ChangeID", null),
                                new SqlParameter("@ModuleCode", vd.comn_mod_name_e),
                                new SqlParameter("@Applicationcode", vd.comn_appl_name),
                                new SqlParameter("@DocDone", vd.is_doc_done==true?"A":"I"),
                                new SqlParameter("@TestCaseDesign", vd.is_test_case_design==true?"A":"I"),
                                new SqlParameter("@Remark", vd.comn_version_remark),
                                new SqlParameter("@sp_name", vd.sp_name),
                                new SqlParameter("@sp_remark", vd.sp_remark)
                                });
                            dr1.Close();
                        }
                    }
                    else
                    {
                        result = "exists";
                    }
                }
            }
            catch (Exception ex)
            {
                result = "false";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        /// <summary>
        /// Amruta Applications
        /// </summary>
        /// <param name="change_set_no"></param>
        /// <returns></returns>
        [Route("getSearchMograVersionsDetails")]
        public HttpResponseMessage getSearchMograVersionsDetails(string change_set_no)
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                        new List<SqlParameter>() {
                        new SqlParameter("@opr", "SM"),
                        new SqlParameter("@ChangesetNo",change_set_no),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.changeset_code = dr["ChangesetNo"].ToString();
                            pn.comn_appl_project_name = dr["ProjectName"].ToString();
                            pn.comn_developer_name = dr["DeveloperName"].ToString();
                            pn.comn_developed_date = dr["DevelopedDate"].ToString();
                            pn.comn_deployed_date = dr["DeployedDate"].ToString();
                            if (pn.mogra_details == null)
                                pn.mogra_details = new List<Mogra_Version_Details>();
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                                new List<SqlParameter>() {
                                    new SqlParameter("@opr", "ED"),
                                    new SqlParameter("@ChangesetNo", pn.changeset_code),
                            });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_Version_Details vd = new Mogra_Version_Details();
                                        vd.changeset_code = pn.changeset_code;
                                        vd.change_id = dr1["ChangeID"].ToString();
                                        vd.comn_mod_code = dr1["ModuleName"].ToString();
                                        vd.comn_mod_name_e = dr1["module_name_en"].ToString();
                                        vd.comn_appl_name = dr1["appl_name_en"].ToString();
                                        vd.comn_appl_code = dr1["ApplicationName"].ToString();
                                        vd.is_doc_done = dr1["DocDone"].ToString().Equals("A") ? true : false;
                                        vd.is_test_case_design = dr1["TestCaseDesign"].ToString().Equals("A") ? true : false;
                                        vd.comn_version_remark = dr1["Remark"].ToString();
                                        vd.sp_name = dr1["spName"].ToString();
                                        vd.sp_remark = dr1["spremark"].ToString();

                                        pn.mogra_details.Add(vd);
                                    }
                                }
                            }
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }
        //[Route("SearchMograVersionsDetailsNew")]
        //public HttpResponseMessage SearchMograVersionsDetailsNew(string ProjectName, string startDate, string endDate)
        //{
        //    List<Mogra_Version> lst = new List<Mogra_Version>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
        //                new List<SqlParameter>() {
        //                new SqlParameter("@opr", "SM"),
        //                new SqlParameter("@ProjectName",ProjectName),
        //                new SqlParameter("@startDate",startDate),
        //                new SqlParameter("@endDate",endDate)
        //            });

        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Mogra_Version pn = new Mogra_Version();
        //                    pn.comn_appl_code = dr["ChangesetNo"].ToString();
        //                    pn.comn_appl_name = dr["ProjectName"].ToString();
        //                    pn.comn_appl_name = dr["DeveloperName"].ToString();
        //                    pn.comn_appl_name = dr["DevelopedDate"].ToString();
        //                    pn.comn_appl_name = dr["DeployedDate"].ToString();
        //                    if (pn.mogra_details == null)
        //                        pn.mogra_details = new List<Mogra_Version_Details>();
        //                    using (DBConnection db1 = new DBConnection())
        //                    {
        //                        db1.Open();
        //                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
        //                        new List<SqlParameter>() {
        //                            new SqlParameter("@opr", "ED"),
        //                            new SqlParameter("@ChangesetNo", pn.changeset_code),
        //                    });
        //                        if (dr1.HasRows)
        //                        {
        //                            while (dr1.Read())
        //                            {
        //                                Mogra_Version_Details vd = new Mogra_Version_Details();
        //                                vd.changeset_code = pn.changeset_code;
        //                                vd.change_id = dr1["ChangeID"].ToString();
        //                                vd.comn_mod_code = dr1["ModuleName"].ToString();
        //                                vd.comn_appl_code = dr1["ApplicationName"].ToString();
        //                                vd.is_doc_done = dr1["DocDone"].ToString().Equals("A") ? true : false;
        //                                vd.is_test_case_design = dr1["TestCaseDesign"].ToString().Equals("A") ? true : false;
        //                                vd.comn_version_remark = dr1["Remark"].ToString();
        //                                vd.sp_name = dr1["spName"].ToString();
        //                                vd.sp_remark = dr1["spremark"].ToString();

        //                                pn.mogra_details.Add(vd);
        //                            }
        //                        }
        //                        lst.Add(pn);
        //                    }
        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, lst);
        //            }
        //        }
        //    }

        [Route("UpdateMograVersionsDetails")]
        public HttpResponseMessage UpdateMograVersionsDetails(List<Mogra_Version> lst)
        {
            bool res = true;
            try
            {
                foreach (Mogra_Version v in lst)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        foreach (Mogra_Version_Details vd in v.mogra_details)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                            new List<SqlParameter>() {
                                    new SqlParameter("@opr", "UD"),
                                    new SqlParameter("@ChangesetNo", v.changeset_code),
                                    new SqlParameter("@ChangeID", vd.change_id),
                                    new SqlParameter("@ModuleCode", vd.comn_mod_code),
                                    new SqlParameter("@Applicationcode", vd.comn_appl_code),
                                    new SqlParameter("@DocDone", vd.is_doc_done==true?"A":"I"),
                                    new SqlParameter("@TestCaseDesign", vd.is_test_case_design==true?"A":"I"),
                                    new SqlParameter("@Remark", vd.comn_version_remark),
                                    new SqlParameter("@sp_name", vd.sp_name),
                                    new SqlParameter("@sp_remark", vd.sp_remark)
                                    });
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
        }


        [Route("getsearchMograVersionsDetailsNew")]
        public HttpResponseMessage getsearchMograVersionsDetailsNew(string ProjectName, string startDate, string endDate)
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                        new List<SqlParameter>() {
                        new SqlParameter("@opr", "SM"),
                        new SqlParameter("@ProjectName", ProjectName),
                        new SqlParameter("@startDate", startDate),
                        new SqlParameter("@endDate", endDate),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.changeset_code = dr["ChangesetNo"].ToString();
                            pn.comn_appl_project_name = dr["ProjectName"].ToString();
                            pn.comn_developer_name = dr["DeveloperName"].ToString();
                            pn.comn_developed_date = dr["DevelopedDate"].ToString();
                            pn.comn_deployed_date = dr["DeployedDate"].ToString();
                            if (pn.mogra_details == null)
                                pn.mogra_details = new List<Mogra_Version_Details>();
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                                new List<SqlParameter>() {
                                    new SqlParameter("@opr", "ED"),
                                    new SqlParameter("@ChangesetNo", pn.changeset_code),
                            });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_Version_Details vd = new Mogra_Version_Details();
                                        vd.changeset_code = pn.changeset_code;
                                        vd.change_id = dr1["ChangeID"].ToString();
                                        vd.comn_mod_code = dr1["ModuleName"].ToString();
                                        vd.comn_mod_name_e = dr1["module_name_en"].ToString();
                                        vd.comn_appl_name = dr1["appl_name_en"].ToString();
                                        vd.comn_appl_code = dr1["ApplicationName"].ToString();
                                        vd.is_doc_done = dr1["DocDone"].ToString().Equals("A") ? true : false;
                                        vd.is_test_case_design = dr1["TestCaseDesign"].ToString().Equals("A") ? true : false;
                                        vd.comn_version_remark = dr1["Remark"].ToString();
                                        vd.sp_name = dr1["spName"].ToString();
                                        vd.sp_remark = dr1["spremark"].ToString();

                                        pn.mogra_details.Add(vd);
                                    }
                                }
                            }
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
           /* List<Mogra_Version_Details> lst = new List<Mogra_Version_Details>();
            try
            {
                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("[mogra].[mograVersion_proc]",
                    new List<SqlParameter>() {
                                    new SqlParameter("@opr", "ED"),
                                    new SqlParameter("@ProjectName", ProjectName),
                                    new SqlParameter("@startDate", startDate),
                                    new SqlParameter("@endDate", endDate),
                            });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            Mogra_Version_Details vd = new Mogra_Version_Details();
                            vd.changeset_code = dr1["ChangesetNo"].ToString(); ;
                            vd.change_id = dr1["ChangeID"].ToString();
                            vd.comn_mod_code = dr1["ModuleName"].ToString();
                            vd.comn_mod_name_e = dr1["module_name_en"].ToString();
                            vd.comn_appl_name = dr1["appl_name_en"].ToString();
                            vd.comn_appl_code = dr1["ApplicationName"].ToString();
                            vd.is_doc_done = dr1["DocDone"].ToString().Equals("A") ? true : false;
                            vd.is_test_case_design = dr1["TestCaseDesign"].ToString().Equals("A") ? true : false;
                            vd.comn_version_remark = dr1["Remark"].ToString();
                            vd.sp_name = dr1["spName"].ToString();
                            vd.sp_remark = dr1["spremark"].ToString();
                            lst.Add(vd);
                        }
                    }
                }
            }//
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
            * */
        }
    
    
    }///Class END
}// Namespace END

    
