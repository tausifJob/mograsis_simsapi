﻿using SIMSAPI.Helper;
using SIMSAPI.Models;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using log4net;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Term
{
    [RoutePrefix("api/StudLeaveApprove")]
    public class StudLeaveApproveController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("ClassDetails")]
        public HttpResponseMessage ClassDetails(string empid, string cur_code, string academic_year)
        {
            List<Sims548> mod_list = new List<Sims548>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_leave_application_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                              new SqlParameter("@emp_no", empid),
                              new SqlParameter("@sims_cur_code", cur_code),
                              new SqlParameter("@sims_academic_year", academic_year),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims548 hrmsObj = new Sims548();
                            hrmsObj.sims_grade_code = dr["sims_grade_code"].ToString();
                            hrmsObj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            hrmsObj.sims_section_code = dr["sims_section_code"].ToString();
                            hrmsObj.sims_section_name = dr["sims_section_name_en"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetLeaveCode")]
        public HttpResponseMessage GetLeaveCode()
        {
            List<Sims548> mod_list = new List<Sims548>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_leave_application_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "L")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims548 hrmsObj = new Sims548();
                            hrmsObj.sims_leave_code = dr["sims_appl_parameter"].ToString();
                            hrmsObj.sims_leave_type = dr["sims_appl_form_field_value1"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("LeaveDetails")]
        public HttpResponseMessage LeaveDetails(Sims548 obj)
        {
            List<Sims548> mod_list = new List<Sims548>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_leave_application_proc_ADISW]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@emp_no", obj.emp_id),
                new SqlParameter("@sims_leave_code", obj.sims_leave_code),
                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                new SqlParameter("@sims_section_code", obj.sims_section_code),
                new SqlParameter("@sims_leave_start_date",db.DBYYYYMMDDformat( obj.sims_leave_start_date)),
                new SqlParameter("@sims_leave_end_date", db.DBYYYYMMDDformat(obj.sims_leave_end_date)),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims548 simsobj = new Sims548();
                            simsobj.sims_leave_number = dr["sims_leave_number"].ToString();
                            simsobj.class_details = dr["Class_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_Stud_name = dr["Stud_name"].ToString();
                            simsobj.sims_leave_code = dr["sims_leave_code"].ToString();
                            simsobj.sims_leave_type = dr["Leave_type"].ToString();
                            simsobj.sims_leave_start_date = db.UIDDMMYYYYformat(dr["sims_leave_start_date"].ToString());
                            simsobj.sims_leave_end_date = db.UIDDMMYYYYformat(dr["sims_leave_end_date"].ToString());
                            simsobj.sims_leave_reason = dr["sims_leave_reason"].ToString();
                            simsobj.sims_leave_application_date = db.UIDDMMYYYYformat(dr["sims_leave_application_date"].ToString());
                            simsobj.sims_leave_status = dr["sims_leave_status"].ToString();
                            simsobj.sims_leave_status_name = dr["leave_status"].ToString();
                            simsobj.sims_leave_remark_new = dr["remark_new"].ToString();
                            simsobj.sims_leave_remark_old = dr["remark_old"].ToString();
                            simsobj.ord1 = dr["odr1"].ToString();
                            simsobj.ord2 = dr["odr2"].ToString();
                            simsobj.sims_leave_address = dr["sims_leave_address"].ToString();
                            simsobj.sims_leave_phone = dr["sims_leave_phone"].ToString();
                            simsobj.sims_leave_mobile = dr["sims_leave_mobile"].ToString();
                            simsobj.sims_leave_email = dr["sims_leave_email"].ToString();
                            simsobj.sims_leave_attachment = dr["sims_leave_attachment"].ToString();
                            if (dr["sims_leave_missed_cycle_test_term_exam"].ToString() == "Y")
                                simsobj.sims_leave_missed_cycle_test_term_exam = true;
                            else
                                simsobj.sims_leave_missed_cycle_test_term_exam = false;
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("LeaveUpdate")]
        public HttpResponseMessage LeaveUpdate(Sims548 item)
        {
            bool chk = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_leave_application_proc]",
                        new List<SqlParameter>() 
                         { 
                             
                new SqlParameter("@opr", 'U'),
                new SqlParameter("@sims_leave_number", item.sims_leave_number),
                new SqlParameter("@sims_enroll_number", item.sims_enroll_number),
                new SqlParameter("@emp_no",item.emp_id ),
                new SqlParameter("@sims_leave_status", item.sims_leave_status),
                new SqlParameter("@sims_leave_remark", item.sims_leave_remark_new),
                 new SqlParameter("@sims_leave_remark_old", item.sims_leave_remark_old),
                new SqlParameter("@sims_leave_start_date",db.DBYYYYMMDDformat(item.sims_leave_start_date)),
                new SqlParameter("@sims_leave_end_date",db.DBYYYYMMDDformat(item.sims_leave_end_date)),
              });
                    if (dr.RecordsAffected > 0)
                    {
                        chk = true;
                    }


                    dr.Close();
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }


    }
}