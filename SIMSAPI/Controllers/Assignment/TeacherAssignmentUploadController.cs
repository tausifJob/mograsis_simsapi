﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;

using System.Web;
using System.IO;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/AssignmentUpload")]
    public class TeacherAssignmentUploadController : ApiController
    {

        [Route("GetAcademicYear")]
        public HttpResponseMessage GetAcademicYear()
        {
            List<Sim508> mod_list = new List<Sim508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "O")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 obj = new Sim508();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            obj.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());


                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetTextFormat")]
        public HttpResponseMessage GetTextFormat()
        {
            List<Sim508> mod_list = new List<Sim508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "L")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 obj = new Sim508();
                            obj.file_format = dr["sims_appl_parameter"].ToString();


                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getAssignmentUpload")]
        public HttpResponseMessage getAssignmentUpload(string User_Code)
        {

            List<Sim508> list = new List<Sim508>();
            List<Sim508> rglist = new List<Sim508>();
            List<Sim508> rslist = new List<Sim508>();
            List<Sim508> sslist = new List<Sim508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "Y"),
                            new SqlParameter("@sims_assignment_teacher_code",User_Code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 ob = new Sim508();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            ob.sims_section_name = dr["sims_section_name_en"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();

                            //ob.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            //ob.sims_assignment_start_date = dr["sims_assignment_start_date"].ToString();
                            //ob.sims_assignment_submission_date = dr["sims_assignment_submission_date"].ToString();
                            //ob.sims_assignment_freeze_date = dr["sims_assignment_freeze_date"].ToString();


                            ob.sims_subject_Code = dr["sims_subject_code"].ToString();
                            ob.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            list.Add(ob);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj = new Sim508();
                            obj.sims_grade_code = list[i].sims_grade_code;
                            obj.sims_grade_name = list[i].sims_grade_name;
                            rglist.RemoveAll(q => q.sims_grade_code == obj.sims_grade_code);
                            rglist.Add(obj);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj_1 = new Sim508();
                            obj_1.sims_section_code = list[i].sims_section_code;
                            obj_1.sims_section_name = list[i].sims_section_name;
                            rglist.RemoveAll(q => q.sims_section_code == obj_1.sims_section_code);
                            rglist.Add(obj_1);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj_2 = new Sim508();
                            obj_2.sims_subject_Code = list[i].sims_subject_Code;
                            obj_2.sims_subject_name_en = list[i].sims_subject_name_en;
                            sslist.RemoveAll(q => q.sims_subject_Code == obj_2.sims_subject_Code);
                            sslist.Add(obj_2);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAssignmentDetail")]
        public HttpResponseMessage getAssignmentDetail(string User_Code)
        {

            List<Sim508> list = new List<Sim508>();
            List<Sim508> rglist = new List<Sim508>();
            List<Sim508> rslist = new List<Sim508>();
            List<Sim508> sslist = new List<Sim508>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "K"),
                            new SqlParameter("@sims_assignment_teacher_code",User_Code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 ob = new Sim508();
                            ob.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            ob.sims_grade_name = dr["sims_grade_name"].ToString();
                            ob.sims_section_name = dr["sims_section_name"].ToString();
                            ob.sims_section_code = dr["sims_bell_section_code"].ToString();
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_assignment_start_date"].ToString()))
                                ob.sims_assignment_start_date = db.UIDDMMYYYYformat( dr["sims_assignment_start_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                                ob.sims_assignment_submission_date =db.UIDDMMYYYYformat( dr["sims_assignment_submission_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                                ob.sims_assignment_freeze_date = db.UIDDMMYYYYformat(dr["sims_assignment_freeze_date"].ToString());
                            ob.sims_assignment_status = dr["sims_assignment_status"].ToString().Equals("A") ? true : false;
                            ob.sims_subject_Code = dr["sims_subject_code"].ToString();
                            ob.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            list.Add(ob);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj = new Sim508();
                            obj.sims_grade_code = list[i].sims_grade_code;
                            obj.sims_grade_name = list[i].sims_grade_name;
                            rglist.RemoveAll(q => q.sims_grade_code == obj.sims_grade_code);
                            rglist.Add(obj);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj_1 = new Sim508();
                            obj_1.sims_section_code = list[i].sims_section_code;
                            obj_1.sims_section_name = list[i].sims_section_name;
                            rglist.RemoveAll(q => q.sims_section_code == obj_1.sims_section_code);
                            rglist.Add(obj_1);
                        }

                        for (int i = 1; i < list.Count; i++)
                        {
                            Sim508 obj_2 = new Sim508();
                            obj_2.sims_subject_Code = list[i].sims_subject_Code;
                            obj_2.sims_subject_name_en = list[i].sims_subject_name_en;
                            sslist.RemoveAll(q => q.sims_subject_Code == obj_2.sims_subject_Code);
                            sslist.Add(obj_2);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("insert_Assignment")]
        public HttpResponseMessage insert_Assignment(List<List<Sim508>> data)
        {
            List<Sim508> data_lst = new List<Sim508>();
            List<Sim508> doc_list = new List<Sim508>();
            List<Sim508> stud_list = new List<Sim508>();

            data_lst = data[0];
            doc_list = data[1];
            stud_list = data[2];

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim508 simsobj in data_lst)
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","I"),

                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                          new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                          new SqlParameter("@sims_assignment_subject_code",simsobj.sims_subject_Code),
                          new SqlParameter("@sims_assignment_title", simsobj.sims_assignment_title),
                          new SqlParameter("@sims_assignment_desc",simsobj.sims_assignment_desc),
                          new SqlParameter("@sims_assignment_teacher_code",simsobj.sims_bell_teacher_code),
                          new SqlParameter("@sims_assignment_start_date", db.DBYYYYMMDDformat( simsobj.sims_assignment_start_date)),
                          new SqlParameter("@sims_assignment_submission_date",db.DBYYYYMMDDformat(simsobj.sims_assignment_submission_date)),
                          new SqlParameter("@sims_assignment_freeze_date",db.DBYYYYMMDDformat( simsobj.sims_assignment_freeze_date)),
                          new SqlParameter("@sims_assignment_status",simsobj.sims_assignment_status==true?"A":"I")

                        });

                        while (dr.Read())
                        {
                            string abc = dr["assignment_number"].ToString();
                            inserted = insert_Assignment_Doc(doc_list, simsobj, abc);
                            List<Sim508> tempobj = new List<Sim508>();
                            foreach (var item in stud_list)
                            {
                                if (item.sims_cur_code == simsobj.sims_cur_code && item.sims_academic_year == simsobj.sims_academic_year
                                    && item.sims_grade_code + item.sims_section_code == simsobj.sims_section_code)
                                {
                                    tempobj.Add(item);
                                }
                            }

                            inserted = insert_Asignment_student(tempobj, abc);
                        }
                        dr.Close();


                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        public bool insert_Assignment_Doc(List<Sim508> data, Sim508 obj, string assgn_number)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim508 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","Z"),

                          new SqlParameter("@sims_assignment_number",assgn_number),
                          new SqlParameter("@sims_cur_code",obj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",obj.sims_grade_code),
                          new SqlParameter("@sims_section_code", obj.sims_section_code),                         
                          new SqlParameter("@sims_assignment_doc_line_no",simsobj.sims_assignment_doc_line_no),
                          new SqlParameter("@sims_assignment_doc_path",simsobj.sims_assignment_doc_path),                                          
                          new SqlParameter("@sims_assignment_doc_path_name",simsobj.sims_assignment_doc_path_name),                
                          

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return inserted;
            }
            return inserted;
        }

        public bool insert_Asignment_student(List<Sim508> data, string assgn_number)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim508 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","B"),
                          new SqlParameter("@sims_assignment_number",assgn_number),
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                          new SqlParameter("@sims_section_code", simsobj.sims_section_code),                        
                          new SqlParameter("@sims_student_enroll_number",simsobj.sims_student_enroll_number),

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return inserted;
            }
            return inserted;
        }

        [Route("getStudentsList")]
        public HttpResponseMessage getStudentsList(string Cur_Code, string Grade_Code, string Section_Code, string Sub_Code, string year)
        {

            List<Sim508> std_list = new List<Sim508>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "H"),
                            new SqlParameter("@sims_cur_code",Cur_Code),
                            new SqlParameter("@sims_grade_code",Grade_Code),
                            new SqlParameter("@sims_section_code",Section_Code),
                            new SqlParameter("@sims_assignment_subject_code",Sub_Code),
                            new SqlParameter("@sims_academic_year",year),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim508 obj = new Sim508();
                            obj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            obj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString() + dr["sims_student_passport_middle_name_en"].ToString() + dr["sims_student_passport_last_name_en"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();

                            std_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, std_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, std_list);
        }

        [Route("getGradeByUsername")]
        public HttpResponseMessage getGradeByUsername(string username, string curcode, string acadyear)
        {
            List<Sim509> grade_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'N'),
                                new SqlParameter("@sims_assignment_teacher_code", username),
                                new SqlParameter("@sims_cur_code", curcode),
                                new SqlParameter("@sims_academic_year", acadyear)

                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim509 simsobj = new Sim509();
                            simsobj.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();

                            grade_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("getSectionByUserGrade")]
        public HttpResponseMessage getSectionByUserGrade(string curcode, string acadyear, string grade, string username)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'M'),
                                 new SqlParameter("@sims_cur_code", curcode),
                                  new SqlParameter("@sims_academic_year", acadyear),
                                new SqlParameter("@sims_grade_code", grade),
                                new SqlParameter("@sims_assignment_teacher_code", username)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_section_code = dr["sims_bell_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("get_subject_code")]
        public HttpResponseMessage get_subject_code(string teacher, string section, string year, string grade)
        {
            List<Sim509> list = new List<Sim509>();
            //string fl;
            try
            {
                //if (flag)
                //{
                //    fl = "1";
                //}
                //else
                //{
                //    fl = "0";
                //}

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_proc]",
                         new List<SqlParameter>()
                             {

                                  new SqlParameter("@opr", 'S'),
                                  new SqlParameter("@sims_assignment_teacher_code", teacher),
                                  new SqlParameter("@sims_academic_year", year),
                                  new SqlParameter("@sims_section_code", section),
                                  new SqlParameter("@sims_grade_code", grade)

                });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim509 simsobj = new Sim509();
                            simsobj.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();

                            list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Insert_student_details")]
        public HttpResponseMessage Insert_student_details(Sim508 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_student_details(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "AssignmentUploadCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_assignment]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@opr", 'B'),
                          new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                          new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                          new SqlParameter("@sims_assignment_enroll_number", simsobj.stud_code)

                });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Assignment_details")]
        public HttpResponseMessage Insert_Assignment_details(Sim508 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Assignment_details(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "AssignmentUploadCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_assignment",
                        new List<SqlParameter>()
                        {
                            //new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                            //new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            //new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                            //new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            //new SqlParameter("@sims_assignment_subject_code",simsobj.sims_subject_Code),
                            //new SqlParameter("@sims_gb_term_code",simsobj.sims_term_code),
                            //new SqlParameter("@sims_assignment_title", simsobj.sims_assignment_title),
                            //new SqlParameter("@sims_assignment_desc",simsobj.sims_assignment_desc),
                            //new SqlParameter("@sims_assignment_doc_line_no",simsobj.sims_assignment_doc_line_no),
                            //new SqlParameter("@sims_assignment_teacher_code",simsobj.sims_bell_teacher_code),
                            //new SqlParameter("@sims_assignment_start_date", simsobj.sims_assignment_start_date),
                            //new SqlParameter("@sims_assignment_submission_date",simsobj.sims_assignment_submission_date),
                            //new SqlParameter("@sims_assignment_freeze_date",simsobj.sims_assignment_freeze_date),
                            //new SqlParameter("@sims_assignment_doc_path",simsobj.sims_assignment_doc_path),
                            //new SqlParameter("@sims_student_enroll_number",simsobj.sims_student_enroll_number),
                            //new SqlParameter("@sims_assignment_status",simsobj.sims_assignment_status==true?"A":"I")
                               new SqlParameter("@opr",simsobj.opr),
                     new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                     new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                     new SqlParameter("@sims_gb_number", simsobj.sims_gb_number),
                     new SqlParameter("@sims_gb_cat_code", simsobj.sims_gb_cat_code),
                     new SqlParameter("@sims_gb_cat_assign_number", simsobj.sims_gb_cat_assign_number),
                     new SqlParameter("@sims_assignment_teacher_code", simsobj.sims_assignment_teacher_code),
                     new SqlParameter("@sims_assignment_subject_code", simsobj.sims_subject_code),
                     new SqlParameter("@sims_gb_term_code", simsobj.sims_term_code),
                     new SqlParameter("@sims_assignment_title", simsobj.sims_assignment_title),
                     new SqlParameter("@sims_assignment_desc", simsobj.sims_assignment_desc),
                     new SqlParameter("@sims_assignment_start_date", db.DBYYYYMMDDformat( simsobj.sims_assignment_start_date)),
                     new SqlParameter("@sims_assignment_submission_date",db.DBYYYYMMDDformat(simsobj.sims_assignment_submission_date)),
                     new SqlParameter("@sims_assignment_freeze_date",db.DBYYYYMMDDformat( simsobj.sims_assignment_freeze_date)),
                     new SqlParameter("@sims_assignment_status", simsobj.sims_assignment_status == true ? "A" : "I"),
                     new SqlParameter("@sims_assignment_doc_path", simsobj.sims_assignment_doc_path)


                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Assignment/TeacherDoc/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }
    }
}