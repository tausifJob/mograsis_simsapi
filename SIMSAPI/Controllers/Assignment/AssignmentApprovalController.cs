﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;

using System.Web;
using System.IO;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/AssingmentApproval")]
    public class AssignmentApprovalController : ApiController
    {

        [Route("cur")]
        public HttpResponseMessage cur()
        {
            List<Sim509> grade_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'C'),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim509 simsobj = new Sim509();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();

                            grade_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("aca")]
        public HttpResponseMessage aca(string cur)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_cur_code", cur)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_name = dr["sims_academic_year_description"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("grade")]
        public HttpResponseMessage grade(Sim509 obj)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_assignment_teacher_code", obj.sims_assignment_teacher_code)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("section")]
        public HttpResponseMessage section(Sim509 obj)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_assignment_teacher_code", obj.sims_assignment_teacher_code)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("subject")]
        public HttpResponseMessage subject(Sim509 obj)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'F'),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_assignment_teacher_code", obj.sims_assignment_teacher_code)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("AssignmentDetails")]
        public HttpResponseMessage AssignmentDetails(Sim509 obj)
        {
            List<Sim509> listassment = new List<Sim509>();
            List<sims509_doctdetails> docdetails = new List<sims509_doctdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                  new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_subject_code", obj.sims_subject_code),
                                new SqlParameter("@start_date", db.DBYYYYMMDDformat( obj.sims_assignment_start_date)),
                                new SqlParameter("@end_date", db.DBYYYYMMDDformat( obj.sims_assignment_freeze_date))
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 AssignmentEdit_list = new Sim509();

                            AssignmentEdit_list.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            AssignmentEdit_list.class_details = dr["class_details"].ToString();
                            AssignmentEdit_list.sims_assignment_teacher_name = dr["teacher_name"].ToString();
                            AssignmentEdit_list.subjectName = dr["sims_subject_name_en"].ToString();
                            AssignmentEdit_list.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            AssignmentEdit_list.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                            //AssignmentEdit_list.sims_assignment_start_date = DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_assignment_start_date"].ToString()).Day;
                            AssignmentEdit_list.sims_assignment_start_date = db.UIDDMMYYYYformat(dr["sims_assignment_start_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                                AssignmentEdit_list.sims_assignment_submission_date = db.UIDDMMYYYYformat(dr["sims_assignment_submission_date"].ToString());
                                AssignmentEdit_list.sims_assignment_freeze_date = db.UIDDMMYYYYformat(dr["sims_assignment_freeze_date"].ToString());

                            docdetails = Get_teacher_doc_Details(AssignmentEdit_list.sims_assignment_number);
                            AssignmentEdit_list.doc_details = docdetails;

                            listassment.Add(AssignmentEdit_list);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, listassment);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, listassment);
        }

        public List<sims509_doctdetails> Get_teacher_doc_Details(string assign_no)
        {
            List<sims509_doctdetails> group_list = new List<sims509_doctdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'D'),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims509_doctdetails obj = new sims509_doctdetails();
                            obj.sims509_doc = dr["sims_assignment_doc_path"].ToString();
                            obj.sims509_doc_name = dr["sims_assignment_doc_path_name"].ToString();
                            obj.sims_doc_line = dr["sims_assignment_doc_line_no"].ToString();

                            group_list.Add(obj);
                        }
                    }
                }
                return group_list;
            }
            catch (Exception x) { }
            return group_list;
        }

        [Route("StudentAssignment")]
        public HttpResponseMessage StudentAssignment(string assign_no)
        {
            List<Sim509> StdAssign_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_assignment_approv_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'B'),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_assignment_number = dr["sims_assignment_enroll_number"].ToString();
                            simsobj.sims_assignment_enroll_number = dr["stud_name"].ToString();
                            
                            StdAssign_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, StdAssign_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StdAssign_list);
        }

        [Route("ApproveRejectAssignment")]
        public HttpResponseMessage ApproveRejectAssignment(Sim509 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_assignment_approv_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","I"),
                          new SqlParameter("@sims_assignment_status", data.sims_assignment_status),
                          new SqlParameter("@sims_assignment_reject_remark", data.reject_remark),
                          new SqlParameter("@sims_assignment_number", data.sims_assignment_number)

                    });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}