﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;

using System.Web;
using System.IO;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/TeacherAssignmentApprovalView")]
    public class TeacherAssignmentViewWithApprovalController : ApiController
    {

        [Route("getGradeByUsername")]
        public HttpResponseMessage getGradeByUsername(string aca,string username)
        {
            List<Sim509> grade_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@user_name", username),
                                new SqlParameter("@sims_academic_year", aca)

                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim509 simsobj = new Sim509();
                            simsobj.sims_grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["grade"].ToString();

                            grade_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("getSectionByUserGrade")]
        public HttpResponseMessage getSectionByUserGrade(string aca,string grade, string username)
        {
            List<Sim509> sec_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'T'),
                                new SqlParameter("@grade_code", grade),
                                new SqlParameter("@sims_academic_year", aca),
                                new SqlParameter("@user_name", username)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_section_code = dr["sims_bell_section_code"].ToString();
                            simsobj.sims_section_name = dr["section"].ToString();

                            sec_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sec_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, sec_list);
        }

        [Route("getAssignmentView")]
        public HttpResponseMessage getAssignmentView(string username, string grade, string section, string ass_name,string sims_academic_year)
        {
            if (ass_name == "null")
                ass_name = null;

            List<Sim509> view_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'M'),
                            new SqlParameter("@user_name", username),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                            new SqlParameter("@sims_assignment_name", ass_name),
                            new SqlParameter("@sims_academic_year", sims_academic_year)

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            simsobj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            simsobj.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                            simsobj.sims_subject_code = dr["sims_assignment_subject_code"].ToString();
                            simsobj.sims_subject_name = dr["SubjectName"].ToString();

                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["Grade"].ToString();

                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["Section"].ToString();
                            simsobj.submission = dr["submission"].ToString();
                            if (!string.IsNullOrEmpty((dr["sims_assignment_start_date"].ToString())))
                                simsobj.sims_assignment_start_date = db.UIDDMMYYYYformat(dr["sims_assignment_start_date"].ToString());
                            //  simsobj.assign_date = dr["sims509_assign_date"].ToString();
                            if (!string.IsNullOrEmpty((dr["sims_assignment_submission_date"].ToString())))
                                simsobj.sims_assignment_submission_date = db.UIDDMMYYYYformat(dr["sims_assignment_submission_date"].ToString());
                            //  simsobj.sims509_due_date = dr["sims509_due_date"].ToString();
                            if (!string.IsNullOrEmpty((dr["sims_assignment_freeze_date"].ToString())))
                                simsobj.sims_assignment_freeze_date = db.UIDDMMYYYYformat(dr["sims_assignment_freeze_date"].ToString());

                            simsobj.sims_assignment_status = dr["sims_assignment_status"].ToString();
                            simsobj.sims_assignment_status_name = dr["ass_status"].ToString();
                            simsobj.reject_remark = dr["sims_assignment_reject_remark"].ToString();

                            view_list.Add(simsobj);
                        }


                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, view_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, view_list);
        }


        [Route("getStudentAssignment")]
        public HttpResponseMessage getStudentAssignment(string username, string assign_no)
        {
            List<Sim509> StdAssign_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'Q'),
                                new SqlParameter("@user_name", username),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            simsobj.sims_assignment_enroll_number = dr["sims_assignment_enroll_number"].ToString();
                            simsobj.StudentName = dr["StudentName"].ToString();
                            simsobj.count = dr["count"].ToString();
                            simsobj.sims_assignment_student_doc_date =  db.UIDDMMYYYYformat(dr["sims_assignment_student_doc_date"].ToString());
                            simsobj.sims_assignment_status = dr["sims_assignment_status"].ToString();

                            StdAssign_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, StdAssign_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StdAssign_list);
        }


        [Route("getSubmissionDetails")]
        public HttpResponseMessage getSubmissionDetails(string assign_no, string username)
        {
            List<Sim509> Sub_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@sims_assignment_number", assign_no),
                                new SqlParameter("@user_name", username)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.StudentName = dr["StudentName"].ToString();
                            simsobj.sims_assignment_enroll_number = dr["sims_assignment_enroll_number"].ToString();
                            simsobj.sims_assignment_status = dr["sims_assignment_status"].ToString();

                            Sub_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Sub_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Sub_list);
        }

        [Route("getUnassignSubmissionDetails")]
        public HttpResponseMessage getUnassignSubmissionDetails(string username, string grade, string section, string sims_academic_year, string subject, string sims_assignment_number)
        {
            List<Sim509> Sub_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", "O"),
                            new SqlParameter("@user_name", username),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                            new SqlParameter("@sims_academic_year", sims_academic_year),
                            new SqlParameter("@sims_subject_code", subject),
                            new SqlParameter("@sims_assignment_number", sims_assignment_number),
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 simsobj = new Sim509();
                            simsobj.stud_name = dr["StudentName"].ToString();
                            simsobj.sims_assignment_enroll_number = dr["sims_assignment_enroll_number"].ToString();
                            Sub_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Sub_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Sub_list);
        }

        [Route("getDocumentDetails")]
        public HttpResponseMessage getDocumentDetails(string assign_no)
        {
            List<Sim509> Doc_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_assignment_number", assign_no)

                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim509 simsobj = new Sim509();
                            simsobj.sims_assignment_student_doc_path = dr["sims_assignment_student_doc_path"].ToString();
                            simsobj.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            simsobj.sims_assignment_submission_date =  db.UIDDMMYYYYformat(dr["sims_assignment_submission_date"].ToString());
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.stud_name = dr["stud_name"].ToString();
                            Doc_list.Add(simsobj);
                        }


                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Doc_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Doc_list);
        }

        [Route("getStudentDocumentDetails")]
        public HttpResponseMessage getStudentDocumentDetails(string username, string assign_no,string enroll)
        {
            Sim509 Stddoc_list = new Sim509();
            List<sims509_doctdetails> docdetails = new List<sims509_doctdetails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'R'),
                                new SqlParameter("@user_name", username),
                                new SqlParameter("@enroll_no", enroll),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Stddoc_list.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            Stddoc_list.sims_assignment_enroll_number = dr["sims_assignment_enroll_number"].ToString();
                            Stddoc_list.StudentName = dr["StudentName"].ToString();
                            Stddoc_list.sims_assignment_teacher_remark = dr["sims_assignment_teacher_remark"].ToString();
                            Stddoc_list.sims_assignment_student_remark = dr["sims_assignment_student_remark"].ToString();
                            docdetails = new List<sims509_doctdetails>();
                            docdetails = Get_stud_doc_Details(dr["sims_assignment_enroll_number"].ToString(), assign_no);
                            Stddoc_list.doc_details = docdetails;

                           
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Stddoc_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Stddoc_list);
        }

        public List<sims509_doctdetails> Get_stud_doc_Details(string enroll_no,  string assign_no)
        {
            List<sims509_doctdetails> group_list = new List<sims509_doctdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'N'),
                                new SqlParameter("@enroll_no", enroll_no),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims509_doctdetails obj = new sims509_doctdetails();
                            obj.sims509_doc = dr["sims_assignment_student_doc_path"].ToString();
                            obj.sims_doc_line = dr["sims_assignment_student_line_no"].ToString();
                            obj.sims509_doc_remark = dr["sims_assignment_student_doc_teacher_remark"].ToString();

                            group_list.Add(obj);
                        }
                    }
                }
                return group_list;
            }
            catch (Exception x) { }
            return group_list;
        }


        [Route("saveTeacherRemark")]
        public HttpResponseMessage saveTeacherRemark(List<Sim509> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim509 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Teacher_assign_view_with_approval_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",'B'),
                          new SqlParameter("@enroll_no",simsobj.sims_assignment_enroll_number),
                          new SqlParameter("@sims_assignment_number",simsobj.sims_assignment_number),
                          new SqlParameter("@sims_line_no",simsobj.sims_doc_line),
                          new SqlParameter("@sims_assignment_status_doc_teacher_remark",simsobj.sims509_doc_remark),
                          new SqlParameter("@sims_assignment_teacher_remark",simsobj.sims_assignment_teacher_remark),
                    });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateStudentDoc")]
        public HttpResponseMessage UpdateStudentDoc(List<Sim509> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim509 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Teacher_assign_view_with_approval_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@enroll_no", simsobj.sims_assignment_enroll_number),
                          new SqlParameter("@sims_assignment_number", simsobj.assign_no),
                          new SqlParameter("@sims_assignment_teacher_remark", simsobj.sims_assignment_teacher_remark)

                    });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


      

        [Route("getStudentDetails")]
        public HttpResponseMessage getStudentDetails(string grade, string section, string assign_no)
        {

            List<Sim509> std_list = new List<Sim509>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@grade_code",grade),
                            new SqlParameter("@section_code",section),
                            new SqlParameter("@sims_assignment_number", assign_no)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim509 obj = new Sim509();
                            obj.sims_assignment_status = dr["sims_assignment_status"].ToString();
                            obj.sims_assignment_status_name = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            if (!string.IsNullOrEmpty(obj.sims_assignment_number))
                                obj.isselected = true;
                            else
                                obj.isselected = false;
                            obj.stud_name = dr["stud_name"].ToString();
                            obj.sims_assignment_enroll_number = dr["sims_enroll_number"].ToString();
                            std_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, std_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, std_list);
        }

        [Route("getEditAssignmentDetails")]
        public HttpResponseMessage getEditAssignmentDetails(string assign_no)
        {
            Sim509 AssignmentEdit_list = new Sim509();
            List<sims509_doctdetails> docdetails = new List<sims509_doctdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            AssignmentEdit_list.sims_academic_year = dr["sims_academic_year"].ToString();
                            AssignmentEdit_list.sims_cur_code = dr["sims_cur_code"].ToString();
                            AssignmentEdit_list.sims_grade_code = dr["sims_grade_code"].ToString();
                            AssignmentEdit_list.sims_section_code = dr["sims_section_code"].ToString();
                            AssignmentEdit_list.sims_assignment_number = dr["sims_assignment_number"].ToString();
                            AssignmentEdit_list.sims_gb_number = dr["sims_gb_number"].ToString();
                            AssignmentEdit_list.sims_gb_name = dr["sims_gb_name"].ToString();
                            AssignmentEdit_list.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            AssignmentEdit_list.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            AssignmentEdit_list.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            AssignmentEdit_list.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                            AssignmentEdit_list.sims_assignment_teacher_code = dr["sims_assignment_teacher_code"].ToString();
                            AssignmentEdit_list.sims_subject_code = dr["sims_assignment_subject_code"].ToString();
                            AssignmentEdit_list.sims_assignment_title = dr["sims_assignment_title"].ToString();
                            AssignmentEdit_list.sims_assignment_desc = dr["sims_assignment_desc"].ToString();
                            AssignmentEdit_list.sims_assignment_start_date = db.UIDDMMYYYYformat(dr["sims_assignment_start_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_assignment_submission_date"].ToString()))
                                AssignmentEdit_list.sims_assignment_submission_date = db.UIDDMMYYYYformat(dr["sims_assignment_submission_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_assignment_freeze_date"].ToString()))
                                AssignmentEdit_list.sims_assignment_freeze_date =db.UIDDMMYYYYformat((dr["sims_assignment_freeze_date"].ToString()));
                            AssignmentEdit_list.sims_archive_status = dr["sims_archive_status"].Equals("Y") ? true : false;
                            AssignmentEdit_list.sims_term_code = dr["sims_assignment_term_code"].ToString();
                            docdetails = new List<sims509_doctdetails>();
                            docdetails = Get_teacher_doc_Details(assign_no);

                            AssignmentEdit_list.sims_assignment_status = dr["sims_assignment_status"].ToString();
                            AssignmentEdit_list.sims_assignment_status_name = dr["status_name"].ToString();
                            AssignmentEdit_list.reject_remark = dr["sims_assignment_reject_remark"].ToString();

                            AssignmentEdit_list.doc_details = docdetails;
                            //if (dr["sims_assignment_status"].Equals("A"))
                            //    simsobj.sims_assignment_status = true;
                            //else
                            //    simsobj.sims_assignment_status = false;
                            //if (dr["sims_assignment_status"].ToString() == "Y")
                            //    simsobj.sims_archive_status = true;
                            //else
                            //    simsobj.sims_archive_status = false;

                            
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, AssignmentEdit_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, AssignmentEdit_list);
        }

        public List<sims509_doctdetails> Get_teacher_doc_Details( string assign_no)
        {
            List<sims509_doctdetails> group_list = new List<sims509_doctdetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                         new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'V'),
                                new SqlParameter("@sims_assignment_number", assign_no)
                             });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims509_doctdetails obj = new sims509_doctdetails();
                            obj.sims509_doc = dr["sims_assignment_doc_path"].ToString();
                            obj.sims509_doc_name = dr["sims_assignment_doc_path_name"].ToString();
                            obj.sims_doc_line = dr["sims_assignment_doc_line_no"].ToString();

                            group_list.Add(obj);
                        }
                    }
                }
                return group_list;
            }
            catch (Exception x) { }
            return group_list;
        }


        [Route("UpdateStudentDetails")]
        public HttpResponseMessage UpdateStudentDetails(List<Sim509> stud_info)
        {

            bool inserted = false;

            //Sim509 data = Newtonsoft.Json.JsonConvert.DeserializeObject<Sim509>(obj);

            //            if(obj)
            foreach (Sim509 item in stud_info)
            {
                if (item.isselected)
                {
                  
                        try
                        {
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();
                                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                            new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'G'),
                            new SqlParameter("@sims_academic_year", item.sims_academic_year),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@grade_code", item.sims_grade_code),
                            new SqlParameter("@section_code", item.sims_section_code),
                            new SqlParameter("@sims_assignment_number", item.sims_assignment_number),
                            new SqlParameter("@enroll_no", item.sims_assignment_enroll_number)
                            });
                                if (dr.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                                dr.Close();
                                
                            }
                        }
                        catch (Exception ecx) { }
                }
                else
                {
                    try
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                                new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@sims_assignment_number", item.sims_assignment_number),
                                new SqlParameter("@enroll_no", item.sims_assignment_enroll_number)
                        });

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            dr.Close();
                        }
                    }
                    catch (Exception ex) { }

                }
                
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateAssignmentDetails")]
        public HttpResponseMessage UpdateAssignmentDetails(Sim509 item)
        {

            bool inserted = false;
           
                    try
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                        new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'K'),
                            new SqlParameter("@sims_assignment_name", item.sims_assignment_title),
                            new SqlParameter("@sims_assignment_desc", item.sims_assignment_desc),
                            new SqlParameter("@sims_assignment_freeze_date", db.DBYYYYMMDDformat(item.sims_assignment_freeze_date)),
                            new SqlParameter("@sims_assignment_submission_date", db.DBYYYYMMDDformat(item.sims_assignment_submission_date)),
                            new SqlParameter("@sims_assignment_status", "P"),
                            new SqlParameter("@sims_assignment_number", item.sims_assignment_number)
                            });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                           

                        }
                    }
                    catch (Exception ecx) { }
              

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateAssignmentDocDetails")]
        public HttpResponseMessage UpdateAssignmentDocDetails(List<Sim509> item_list)
        {

            bool inserted = false;
            foreach (var item in item_list)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                    new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'J'),
                            new SqlParameter("@sims_academic_year", item.sims_academic_year),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@grade_code", item.sims_grade_code),
                            new SqlParameter("@section_code", item.sims_section_code),
                            new SqlParameter("@sims_assignment_number", item.sims_assignment_number),
                            new SqlParameter("@sims_line_no", item.sims_doc_line),
                            new SqlParameter("@sims_assignment_doc", item.sims509_desc),
                            new SqlParameter("@sims_assignment_doc_name", item.sims509_desc_name),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();

                    }
                }
                catch (Exception ecx) { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("UpdateDeleteAssignment")]
        public HttpResponseMessage UpdateDeleteAssignment(List<Sim509> item_list)
        {

            bool inserted = false;
            foreach (var item in item_list)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                    new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'L'),
                            new SqlParameter("@sims_assignment_number", item.sims_assignment_number),
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();

                    }
                }
                catch (Exception ecx) { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Assignment/TeacherDoc/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }


        [Route("CUDinsertunassingstudent")]
        public HttpResponseMessage CUDinsertunassingstudent(List<Sim509> item_list)
        {

            bool inserted = false;
            foreach (var item in item_list)
            {

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Teacher_assign_view_with_approval_proc]",
                    new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'W'),
                            new SqlParameter("@sims_academic_year", item.sims_academic_year),
                            new SqlParameter("@sims_cur_code", item.sims_cur_code),
                            new SqlParameter("@grade_code", item.sims_grade_code),
                            new SqlParameter("@section_code", item.sims_section_code),
                            new SqlParameter("@enroll_no", item.sims_assignment_enroll_number), 
                            new SqlParameter("@sims_assignment_number", item.sims_assignment_number),

                            });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();

                    }
                }
                catch (Exception ecx) { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}