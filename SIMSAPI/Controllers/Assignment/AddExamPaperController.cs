﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;

using System.Web;
using System.IO;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.Assignment_EXAM
{
    [RoutePrefix("api/AddExamPaper")]
    [BasicAuthentication]
    public class AddExamPaperController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getsubject")]
        public HttpResponseMessage getsubject(string cur, string aca, string grd, string section)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudent"));
            Sims685 simobj = new Sims685();
            //if (data == "undefined" || data == "\"\"" || data == "[]")
            //{
            //    data = null;
            //}
            //else
            //{
            //    simobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims685>(data);
            //}

            List<Sims685> search_list = new List<Sims685>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_exam_reference_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),                            
                            new SqlParameter("@sims_grade_code", grd),
                            new SqlParameter("@sims_section_code",section),                         
                            new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", aca),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims685 objNew = new Sims685();
                            objNew.sims_subject_code = dr["sims_subject_code"].ToString();
                            objNew.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getExamCode")]
        public HttpResponseMessage getExamCode(string cur, string year)
        {
            List<Sims685> lstcertificate = new List<Sims685>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_exam_reference_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@sims_cur_code", cur),
                            new SqlParameter("@sims_academic_year", year),  

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims685 obj = new Sims685();
                            obj.sims_exam_code = dr["sims_exam_code"].ToString();
                            obj.sims_exam_desc = dr["sims_exam_desc"].ToString();
                            lstcertificate.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstcertificate);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/ExamPapers/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [Route("insert_Assignment")]
        public HttpResponseMessage insert_Assignment(List<Sims685> data)
        {
            //  string debug = "MODULE :{0},APPLICATION :{1},METHOD : insert_Assignment_Detail(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "AssignmentUploadCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims685 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_exam_reference_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","I"),

                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                          new SqlParameter("@sims_grade_code",simsobj.sims_grade_code),
                          new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                          new SqlParameter("@sims_subject_code",simsobj.sims_subject_code),
                          new SqlParameter("@sims_exam_code", simsobj.sims_exam_code),
                          new SqlParameter("@sims_exam_attach_refno",simsobj.sims_exam_attach_refno),
                          new SqlParameter("@sims_exam_paper_title",simsobj.sims_doc_desc),
                          new SqlParameter("@sims_exam_paper_status",simsobj.sims_exam_paper_status== true ? "A" : "I"),
                          new SqlParameter("@sims_exam_paper_path",simsobj.sims_exam_paper_path),
                          new SqlParameter("@sims_exam_paper_display_name",simsobj.sims_exam_paper_display_name),
                          new SqlParameter("@sims_exam_paper_created_by", simsobj.sims_exam_paper_created_by),
                          new SqlParameter("@trans",simsobj.trans_type)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAllPapers")]
        public HttpResponseMessage getAllPapers(string cur, string aca, string grd, string sec, string exam, string sub)
        {
            Message message = new Message();
            List<Sims685> mod_list = new List<Sims685>();

            Sims685 simobj = new Sims685();
            //List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (grd == "undefined" || grd == "\"\"")
            {
                grd = null;
            }
            if (sec == "undefined" || sec == "\"\"")
            {
                sec = null;
            }
            if (exam == "undefined" || exam == "\"\"")
            {
                exam = null;
            }
            if (sub == "undefined" || sub == "\"\"")
            {
                sub = null;
            }
            //else
            //{
            //    simobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims685>(data);
            //}

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_exam_reference_proc]",
                    new List<SqlParameter>(){               
                    new SqlParameter("@opr", 'S'),
                    new SqlParameter("@sims_grade_code", grd),
                    new SqlParameter("@sims_section_code", sec),                         
                    new SqlParameter("@sims_cur_code", cur),
                    new SqlParameter("@sims_academic_year", aca),
                    new SqlParameter("@sims_exam_code", exam),
                    new SqlParameter("@sims_subject_code",sub)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims685 simsobj = new Sims685();
                            simsobj.sims_exam_paper_srl_no = dr["sims_exam_paper_srl_no"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //simsobj.sims_cur_code_name = dr["sims_cur_code_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_code_name = dr["sims_grade_code_name"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            // simsobj.sims_section_code_name = dr["sims_section_code_name"].ToString();
                            simsobj.sims_section_code_name = dr["section_name"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_code_name = dr["sims_subject_code_name"].ToString();
                            simsobj.sims_exam_code = dr["sims_exam_code"].ToString();
                            simsobj.sims_exam_code_name = dr["sims_exam_code_name"].ToString();
                            simsobj.sims_exam_paper_path = dr["sims_exam_paper_path"].ToString();
                            simsobj.sims_exam_paper_display_name = dr["sims_exam_paper_display_name"].ToString();
                            simsobj.sims_exam_paper_status = dr["sims_exam_paper_status"].Equals("A") ? true : false;
                            simsobj.sims_exam_paper_created_by = dr["sims_exam_paper_created_by"].ToString();
                            simsobj.sims_exam_paper_title = dr["sims_exam_paper_title"].ToString();
                            simsobj.sims_exam_attach_refno = dr["sims_exam_attach_refno"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getOldPapers")]
        public HttpResponseMessage getOldPapers(string cur, string aca, string grd, string sec, string exam)
        {
            Message message = new Message();
            List<Sims685> mod_list = new List<Sims685>();

            Sims685 simobj = new Sims685();
            //List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (grd == "undefined" || grd == "\"\"")
            {
                grd = null;
            }
            if (sec == "undefined" || sec == "\"\"")
            {
                sec = null;
            }
            if (exam == "undefined" || exam == "\"\"")
            {
                exam = null;
            }
            //else
            //{
            //    simobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims685>(data);
            //}

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_exam_reference_proc]",
                    new List<SqlParameter>(){               
                    new SqlParameter("@opr", 'K'),
                    new SqlParameter("@sims_grade_code", grd),
                    new SqlParameter("@sims_section_code", sec),                         
                    new SqlParameter("@sims_cur_code", cur),
                    new SqlParameter("@sims_academic_year", aca),
                    new SqlParameter("@sims_exam_code", exam)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims685 simsobj = new Sims685();
                            simsobj.sims_exam_paper_srl_no = dr["sims_exam_paper_srl_no"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //simsobj.sims_cur_code_name = dr["sims_cur_code_name"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_code_name = dr["sims_grade_code_name"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_code_name = dr["sims_section_code_name"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_code_name = dr["sims_subject_code_name"].ToString();
                            simsobj.sims_exam_code = dr["sims_exam_code"].ToString();
                            simsobj.sims_exam_code_name = dr["sims_exam_code_name"].ToString();
                            simsobj.sims_exam_paper_path = dr["sims_exam_paper_path"].ToString();
                            simsobj.sims_exam_paper_display_name = dr["sims_exam_paper_display_name"].ToString();
                            simsobj.sims_exam_paper_status = dr["sims_exam_paper_status"].Equals("A") ? true : false;
                            simsobj.sims_exam_paper_created_by = dr["sims_exam_paper_created_by"].ToString();
                            simsobj.sims_exam_paper_title = dr["sims_exam_paper_title"].ToString();
                            simsobj.sims_exam_attach_refno = dr["sims_exam_attach_refno"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Inactivate")]
        public HttpResponseMessage Inactivate(List<Sims685> data)
        {
            // List<Sims685> lstcertificate = new List<Sims685>();
            bool del = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims685 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_exam_reference_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                           new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                           new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),  
                           new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),                           
                           new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                           new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                           new SqlParameter("@sims_exam_code", simsobj.sims_exam_code),
                           new SqlParameter("@sims_exam_paper_srl_no",simsobj.sims_exam_paper_srl_no),
                           new SqlParameter("@sims_exam_attach_refno",simsobj.sims_exam_attach_refno)
                         });
                        if (ins > 0)
                        {
                            del = true;
                        }
                        else
                        {
                            del = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, del);
            }
            return Request.CreateResponse(HttpStatusCode.OK, del);
        }

        [Route("StatusDelete")]
        public HttpResponseMessage StatusDelete(List<Sims685> data)
        {
            // List<Sims685> lstcertificate = new List<Sims685>();
            bool del = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims685 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_exam_reference_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                           new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                           new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),  
                           new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),                           
                           new SqlParameter("@sims_section_code", simsobj.sims_section_code),  
                           new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                           new SqlParameter("@sims_exam_code", simsobj.sims_exam_code),
                           new SqlParameter("@sims_exam_paper_srl_no",simsobj.sims_exam_paper_srl_no),
                           new SqlParameter("@sims_exam_attach_refno",simsobj.sims_exam_attach_refno)
                         });
                        if (ins > 0)
                        {
                            del = true;
                        }
                        else
                        {
                            del = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, del);
            }
            return Request.CreateResponse(HttpStatusCode.OK, del);
        }
    }
}