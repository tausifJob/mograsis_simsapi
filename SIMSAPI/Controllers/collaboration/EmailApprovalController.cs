﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.collaboration
{

      [RoutePrefix("api/EmailApproval")]
    public class EmailApprovalController : ApiController
    {

          private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
          [Route("getEmailApprovalDetails")]
          public HttpResponseMessage getEmailApprovalDetails()
          {
              string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmailDetailsDatewise(),PARAMETERS :: NA";
              Log.Debug(string.Format(debug, "ERP", "SETUP"));

              List<emailApproval> lst_emaildet = new List<emailApproval>();

              try
              {
                  using (DBConnection db = new DBConnection())
                  {

                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_approve_proc]",
                          new List<SqlParameter>() 
                       { 
                          new SqlParameter("@opr", 'S'),                          
                          //new SqlParameter("@fromdate", db.DBYYYYMMDDformat( fromdate)),                          
                         // new SqlParameter("@todate", db.DBYYYYMMDDformat(todate)),                          
                       });
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              emailApproval simsobj = new emailApproval();
                              simsobj.sims_email_number = dr["sims_email_number"].ToString();

                              
                              simsobj.receiveMail = dr["sims_recepient_id"].ToString();
                              simsobj.subject = dr["sims_email_subject"].ToString();
                              simsobj.message = dr["sims_email_message"].ToString();
                              simsobj.mailDate = dr["sims_email_date"].ToString();
                              lst_emaildet.Add(simsobj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
                      }
                  }
              }
              catch (Exception x)
              {
              }

              return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
          }

          [Route("Get_search_email_deatils")]
          public HttpResponseMessage Get_search_email_deatils(string startDate, string endDate, string email , string subject )
          {
              List<emailApproval> search_list = new List<emailApproval>();

              if (startDate == "undefined")
                  startDate = null;
              if (endDate == "undefined")
                  endDate = null;
              if (email == "undefined")
                  email = null;
              if (subject == "undefined")
                  subject = null;
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_approve_proc]",
                          new List<SqlParameter>()
                         {

                new SqlParameter("@opr", 's'),
                new SqlParameter("@sims_recepient_id", email),
                new SqlParameter("@sims_email_subject", subject),
                new SqlParameter("@from_date", db.DBYYYYMMDDformat(startDate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(endDate)),

                //new SqlParameter("@from_date", DateTime.Parse(sdate).Year + "-" + DateTime.Parse(sdate).Month + "-" + DateTime.Parse(sdate).Day),
                //new SqlParameter("@to_date", DateTime.Parse(edate).Year + "-" + DateTime.Parse(edate).Month + "-" + DateTime.Parse(edate).Day),
                         }); if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              emailApproval simsobj = new emailApproval();
                              simsobj.sims_email_number = dr["sims_email_number"].ToString();


                              simsobj.receiveMail = dr["sims_recepient_id"].ToString();
                              simsobj.subject = dr["sims_email_subject"].ToString();
                              simsobj.message = dr["sims_email_message"].ToString();
                              simsobj.mailDate = dr["sims_email_date"].ToString();
                              search_list.Add(simsobj);
                          }
                          return Request.CreateResponse(HttpStatusCode.OK, search_list);
                      }
                  }
                  return Request.CreateResponse(HttpStatusCode.OK, search_list);
              }
              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, x.Message);
              }
              //return Request.CreateResponse(HttpStatusCode.OK, trans_list);

          }


          [Route("approve_email")]
          public HttpResponseMessage approve_email(string lst)
          {
              bool insert = false;
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();


                      int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_email_approve_proc]",
                          new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_email_number", lst),
                                
                     });
                          if (ins > 0)
                          {
                              insert = true;
                          }


                          else
                          {
                              insert = false;
                          }
                      
                      return Request.CreateResponse(HttpStatusCode.OK, insert);
                  }

              }


              catch (Exception x)
              {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
              }
           }

          [Route("reject_email")]
          public HttpResponseMessage reject_email(string lst)
          {
              bool insert = false;
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();


                      int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_email_approve_proc]",
                          new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@sims_email_number", lst),
                                
                     });
                      if (ins > 0)
                      {
                          insert = true;
                      }


                      else
                      {
                          insert = false;
                      }

                      return Request.CreateResponse(HttpStatusCode.OK, insert);
                  }

              }


              catch (Exception x)
              {
                  return Request.CreateResponse(HttpStatusCode.OK, x.Message);
              }
          }

    }
}