﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/CollaborationDetails")]
    public class CollaborationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCollaborationdetails")]
        public HttpResponseMessage getCollaborationdetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCollaborationdetails()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<acoll_det> curriculum = new List<acoll_det>();
            // int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            acoll_det simsobj = new acoll_det();
                            simsobj.appl_srno = dr["sims_appl_email_srno"].ToString();
                            simsobj.modcode = dr["sims_mod_code"].ToString();
                            simsobj.module_name = dr["ModuleName"].ToString();
                            simsobj.applcode = dr["sims_appl_code"].ToString();
                            simsobj.application_name = dr["AppName"].ToString();
                            simsobj.SmtpID = dr["sims_smtp_id"].ToString();
                            simsobj.form_control = dr["sims_form_field"].ToString();
                            simsobj.ifemail = dr["sims_email_flag"].ToString().Equals("1") ? true : false;
                            simsobj.email_recipients = dr["sims_email_receipient_member"].ToString();
                            simsobj.email_template = dr["sims_email_templ_no"].ToString();
                            simsobj.ifsms = dr["sims_sms_flag"].ToString().Equals("1") ? true : false;
                            simsobj.sms_recipients = dr["sims_sms_receipient_member"].ToString();
                            simsobj.sms_template = dr["sims_sms_templ_no"].ToString();
                            simsobj.ifalert = dr["sims_alert_flag"].ToString().Equals("1") ? true : false;
                            simsobj.alert_template = dr["sims_alert_templ_no"].ToString();
                            simsobj.ifmalert = dr["sims_mobile_alert_flag"].ToString().Equals("1") ? true : false;
                            simsobj.ifstatus = dr["sims_status"].ToString().Equals("A") ? true : false;
                            simsobj.email_temp_srno = dr["sims_email_templ_no"].ToString();
                            simsobj.sms_temp_srno = dr["sims_sms_templ_no"].ToString();
                            simsobj.alert_temp_srno = dr["sims_alert_templ_no"].ToString();

                            if (simsobj.appl_srno == "1053")
                            {
                                int f = 1;
                            }

                            if (simsobj.email_recipients.Contains("F") == true)                            
                                simsobj.ifemailfather = true;
                            else
                                simsobj.ifemailfather = false;
                            
                            if (simsobj.email_recipients.Contains("M") == true)
                                simsobj.ifemailmother = true;
                            else
                                simsobj.ifemailmother = false;
                            
                            if (simsobj.email_recipients.Contains("G") == true)
                                simsobj.ifemailguardian = true;
                            else
                                simsobj.ifemailguardian = false;

                            if (simsobj.email_recipients.Contains("A") == true)                            
                                simsobj.ifemailactive = true;
                            else
                                simsobj.ifemailactive = false;

                            if (simsobj.sms_recipients.Contains("F") == true)
                                simsobj.ifsmsfather = true;
                            else
                                simsobj.ifsmsfather = false;

                            if (simsobj.sms_recipients.Contains("M") == true)                            
                                simsobj.ifsmsmother = true;
                            else
                                simsobj.ifsmsmother = false;

                            if (simsobj.sms_recipients.Contains("G") == true)
                                simsobj.ifsmsguardian = true;
                            else
                                simsobj.ifsmsguardian = false;

                            if (simsobj.sms_recipients.Contains("A") == true)
                                simsobj.ifsmsactive = true;
                            else
                                simsobj.ifsmsactive = false;
                            
                            curriculum.Add(simsobj);
                            // total = curriculum.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, curriculum);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //  return Request.CreateResponse(HttpStatusCode.OK, curriculum);
        }       

        [Route("getModules")]
        public HttpResponseMessage getModules()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getModules()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<comn_modules> module_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_modules sequence = new comn_modules();
                            sequence.module_code = dr["comn_mod_code"].ToString();
                            sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                            module_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, module_list);
        }

        [Route("getApplications")]
        public HttpResponseMessage getApplications(string modcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getApplications()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<acoll_det> appl_list = new List<acoll_det>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'B'),
                             new SqlParameter("@comn_appl_mod_code", modcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            acoll_det sequence = new acoll_det();
                            sequence.applcode = dr["comn_appl_code"].ToString();
                            sequence.application_name = dr["comn_appl_name_en"].ToString();
                            appl_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, appl_list);
        }

        [Route("getSMTPIds")]
        public HttpResponseMessage getSMTPIds()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSMTPIds()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<acoll_det> smtp_list = new List<acoll_det>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            acoll_det sequence = new acoll_det();
                            sequence.smtpuser = dr["sims_smtp_code"].ToString();
                            sequence.smtpids = dr["sims_smtp_from_email"].ToString();
                            smtp_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, smtp_list);
        }

        [Route("getEmailSMSAlertTemplates")]
        public HttpResponseMessage getEmailSMSAlertTemplates(string modcode,string msgtype)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getEmailSMSAlertTemplates()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<acoll_det> emailtemp_list = new List<acoll_det>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'D'),
                             new SqlParameter("@comn_appl_mod_code", modcode),
                             new SqlParameter("@msgtype", msgtype),
                         });
                     while (dr.Read())
                        {
                            acoll_det sequence = new acoll_det();
                            sequence.msg_srno = dr["sims_msg_sr_no"].ToString();
                            sequence.msg_email_subject = dr["sims_msg_subject"].ToString();
                            sequence.msg_email_body = dr["sims_msg_body"].ToString();
                            sequence.msg_email_signature = dr["sims_msg_signature"].ToString();
                            emailtemp_list.Add(sequence);
                        }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, emailtemp_list);
        }

        [Route("getBodyTemplates")]
        public HttpResponseMessage getBodyTemplates(string srno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getBodyTemplates()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            string body="";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'E'),
                             new SqlParameter("@srno", srno),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            body = dr["sims_msg_body"].ToString();
                            
                        }
                    }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, body);
        }

        [Route("getSigTemplates")]
        public HttpResponseMessage getSigTemplates(string srno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSigTemplates()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<acoll_det> emailtemp_list = new List<acoll_det>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'F'),
                             new SqlParameter("@srno", srno),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            acoll_det sequence = new acoll_det();
                            sequence.msg_email_signature = dr["sims_msg_signature"].ToString();
                            emailtemp_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception dee)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dee.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, emailtemp_list);
        }

        [Route("CUDCollaborationdetails")]
        public HttpResponseMessage CUDCollaborationdetails(List<acoll_det> lstsimsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDCollaborationdetails(),PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/CollaborationDetails", "CUDCollaborationdetails"));
            bool inserted = false;
            Message message = new Message();
            List<acoll_det> lst = new List<acoll_det>();
            lst = lstsimsobj;

            try
            {
                foreach (acoll_det simsobj in lst)
                {
                    if (simsobj != null)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_collaboration_details]",
                                new List<SqlParameter>() 
                         {                              
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_mod_code", simsobj.modcode),
                            new SqlParameter("@sims_appl_code", simsobj.applcode), 
                            new SqlParameter("@sims_form_field",simsobj.form_control),
                            new SqlParameter("@sims_smtp_id", simsobj.SmtpID),
                            new SqlParameter("@sims_email_flag", simsobj.ifemail== true?"1":"0"),
                            new SqlParameter("@sims_email_templ_no", simsobj.email_temp_srno),
                            new SqlParameter("@sims_email_receipient_member", simsobj.email_recipients),
                            new SqlParameter("@sims_sms_flag", simsobj.ifsms== true?"1":"0"),
                            new SqlParameter("@sims_sms_templ_no", simsobj.sms_temp_srno),
                            new SqlParameter("@sims_sms_receipient_member", simsobj.sms_recipients),
                            new SqlParameter("@sims_alert_flag", simsobj.ifalert== true?"1":"0"),
                            new SqlParameter("@sims_alert_templ_no", simsobj.alert_temp_srno),
                            new SqlParameter("@sims_status", "A"),
                            new SqlParameter("@sims_mobile_alert_flag", simsobj.ifmalert== true?"1":"0"),
                            new SqlParameter("@sims_appl_email_srno", simsobj.appl_srno)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetExistingCollaborationdetails")]
        public HttpResponseMessage GetExistingCollaborationdetails(string modcode, string appcode, string formcontrol)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckTypeCode(),PARAMETERS :: GetExistingCollaborationdetails{2}";
            //Log.Debug(string.Format(debug, "STUDENT", "GetExistingCollaborationdetails"));

            bool msgstatus = false;
            bool ifexists = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_collaboration_details]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'J'),
                                new SqlParameter("@sims_mod_code", modcode),
                                new SqlParameter("@sims_appl_code", appcode),
                                new SqlParameter("@sims_form_field", formcontrol)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {
                    msgstatus = true;
                }
                else
                {
                    msgstatus = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, msgstatus);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}