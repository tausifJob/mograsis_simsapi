﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.FEE;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using System.Collections;
using System.IO;
using System.Web;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/CommDefaulter")]
    [BasicAuthentication]
    public class CommDefaulterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
              

        #region DefaulterEmail

        [Route("GetAllCurName")]
        public HttpResponseMessage GetAllCurName()
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllAcademicYears")]
        public HttpResponseMessage GetAllAcademicYears(string cur_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_academic_year_start_date = dr["sims_academic_year_start_date"].ToString();
                            objNew.sims_academic_year_end_date = dr["sims_academic_year_end_date"].ToString();
                            objNew.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllGrades")]
        public HttpResponseMessage GetAllGrades(string cur_code, string ac_year)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllSections")]
        public HttpResponseMessage GetAllSections(string cur_code, string ac_year, string g_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                                new SqlParameter("@sims_g_code", g_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        
        [Route("GetDefaulterSection")]
        public HttpResponseMessage GetDefaulterSection(string cur_code, string ac_year, string g_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SC"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", ac_year),
                                new SqlParameter("@grade_code", g_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        
        [Route("GetDefaulterfeeTypes")]
        public HttpResponseMessage GetDefaulterfeeTypes()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FT")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        
        [Route("GetDefaulterviewList")]
        public HttpResponseMessage GetDefaulterviewList()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "VL")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_view_list_code = dr["comn_appl_parameter"].ToString();
                            objNew.sims_view_list_description = dr["comn_appl_form_field_value1"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetDefaulterviewMode")]
        public HttpResponseMessage GetDefaulterviewMode()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "VM")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_view_mode_code = dr["param_type_code"].ToString();
                            objNew.sims_view_mode_description = dr["param_type_desc"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }
        
        [Route("GetDefaulterEmailMessage")]
        public HttpResponseMessage GetDefaulterEmailMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "F"),
                                
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();
                            
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        [Route("GetDefaulterTerms")]
        public HttpResponseMessage GetDefaulterTerms(string cur_code, string academic_year)
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year",academic_year),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_term_code = dr["sims_Fee_term_code"].ToString();
                            objNew.sims_term_description = dr["sims_fee_term_desc_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("DefaultersList")]
        public HttpResponseMessage DefaultersList(sims_defaulter obj)
        {
            List<sims_defaulter>[] arr = new List<sims_defaulter>[2];
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            List<sims_defaulter> summary = new List<sims_defaulter>();
            arr[0] = new List<sims_defaulter>();
            arr[0] = mod_list;
            arr[1] = new List<sims_defaulter>();
            arr[1] = summary;

            try
            {
                if (obj.sims_student_status == "" || obj.sims_student_status == "undefined")
                    obj.sims_student_status = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SS"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@grade_code", obj.sims_grade_code),
                                new SqlParameter("@section_code", obj.sims_section_code),
                                new SqlParameter("@fee_types", obj.sims_fee_code),
                                new SqlParameter("@view_list_opt", obj.sims_view_list_code),
                                new SqlParameter("@amount_param", string.IsNullOrEmpty(obj.sims_amount)?null:obj.sims_amount),
                                new SqlParameter("@search", string.IsNullOrEmpty(obj.sims_search)?null:obj.sims_search),
                                new SqlParameter("@view_mode_opt", string.IsNullOrEmpty(obj.sims_view_mode_code)?null:obj.sims_view_mode_code),
                                new SqlParameter("@term_code", obj.sims_term_code),
                                new SqlParameter("@sims_student_status", obj.sims_student_status)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                        //    sims_defaulter objNew = new sims_defaulter();
                        //    objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                        //    objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                        //    objNew.sims_fee_paid = dr["sims_fee_paid"].ToString();
                        //    objNew.student_name = dr["student_name"].ToString();
                        //    objNew.grade_name = dr["grade_name"].ToString();
                        //    objNew.section_name = dr["section_name"].ToString();
                        //    objNew.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                        //    objNew.sims_father_name = dr["father_name"].ToString();
                        //    objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                        //    objNew.sims_paid_amount = dr["sims_fee_paid"].ToString();
                        //    objNew.sims_balance_amount = (decimal.Parse(objNew.sims_fee_amount) - decimal.Parse(objNew.sims_paid_amount)).ToString();
                        //    objNew.sims_fee_type_desc = dr["sims_fee_type_desc"].ToString();
                        //    objNew.sims_fee_amount_total = dr["sims_fee_amount_total"].ToString();
                        //    objNew.sims_paid_amount_total = dr["sims_fee_paid_total"].ToString();
                        //    objNew.sims_balance_amount_total = (decimal.Parse(objNew.sims_fee_amount_total) - decimal.Parse(objNew.sims_paid_amount_total)).ToString();
                        //    objNew.parent_email = dr["parent_email"].ToString();
                        //    objNew.isSelected = false;
                        //    mod_list.Add(objNew);
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            //objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //objNew.sims_fee_paid = dr["sims_fee_paid"].ToString();
                            objNew.student_name = dr["student_name"].ToString();
                            objNew.grade_name = dr["grade_name"].ToString();
                            objNew.section_name = dr["section_name"].ToString();
                            objNew.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            objNew.sims_father_name = dr["father_name"].ToString();
                            //objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //objNew.sims_paid_amount = dr["sims_fee_paid"].ToString();
                            objNew.sims_balance_amount = dr["sims_balance_amount"].ToString();//(decimal.Parse(objNew.sims_fee_amount) - decimal.Parse(objNew.sims_paid_amount)).ToString();
                            objNew.sims_fee_type_desc = dr["sims_fee_type_desc"].ToString();
                            //objNew.sims_fee_amount_total = dr["sims_fee_amount_total"].ToString();
                            //objNew.sims_paid_amount_total = dr["sims_fee_paid_total"].ToString();
                            objNew.sims_balance_amount_total = dr["sims_fee_amount_total"].ToString(); //(decimal.Parse(objNew.sims_fee_amount_total) - decimal.Parse(objNew.sims_paid_amount_total)).ToString();
                            objNew.parent_email = dr["parent_email"].ToString();
                            objNew.parent_mobile = dr["parent_mobile"].ToString();
                            objNew.sims_term_code = dr["sims_fee_period_term_code"].ToString();
                            objNew.sims_term_description = dr["term_desc"].ToString();

                            objNew.isSelected = false;
                            mod_list.Add(objNew);
                        }
                    }
                    foreach (sims_defaulter t in mod_list)
                    {
                        sims_defaulter to = new sims_defaulter();
                        to.sims_enroll_number = t.sims_enroll_number;
                        to.student_name = t.student_name;
                        to.grade_name = t.grade_name;
                        to.section_name = t.section_name;
                        to.sims_sibling_parent_number = t.sims_sibling_parent_number;
                        to.sims_father_name = t.sims_father_name;
                        to.sims_balance_amount_total = t.sims_balance_amount_total;
                        to.parent_email = t.parent_email;
                        to.parent_mobile = t.parent_mobile;
                        to.sims_term_code = t.sims_term_code;
                        to.sims_term_description = t.sims_term_description;
                        int cnt = summary.Where(x => x.sims_enroll_number == t.sims_enroll_number).Count();
                        if (cnt == 0)
                            summary.Add(to);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
        }

        [Route("DefaultersList1")]
        public HttpResponseMessage DefaultersList1(sims_defaulter obj)
        {
            List<sims_defaulter>[] arr = new List<sims_defaulter>[2];
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            List<sims_defaulter> summary = new List<sims_defaulter>();
            arr[0] = new List<sims_defaulter>();
            arr[0] = mod_list;
            arr[1] = new List<sims_defaulter>();
            arr[1] = summary;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SS"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@grade_code", obj.sims_grade_code),
                                new SqlParameter("@section_code", obj.sims_section_code),
                                new SqlParameter("@fee_types", obj.sims_fee_code),
                                new SqlParameter("@view_list_opt", obj.sims_view_list_code),
                                new SqlParameter("@amount_param", string.IsNullOrEmpty(obj.sims_amount)?null:obj.sims_amount),
                                new SqlParameter("@search", string.IsNullOrEmpty(obj.sims_search)?null:obj.sims_search),
                                new SqlParameter("@view_mode_opt", '4'),
                                //new SqlParameter("@view_mode_opt", string.IsNullOrEmpty(obj.sims_view_mode_code)?null:obj.sims_view_mode_code),
                                new SqlParameter("@term_code", obj.sims_term_code),
                                new SqlParameter("@sims_student_status", obj.sims_student_status)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            objNew.student_name = dr["student_name"].ToString();
                            objNew.grade_name = dr["grade_name"].ToString();
                            objNew.section_name = dr["section_name"].ToString();
                            objNew.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            objNew.sims_father_name = dr["father_name"].ToString();
                            objNew.sims_balance_amount = dr["sims_balance_amount"].ToString();//(decimal.Parse(objNew.sims_fee_amount) - decimal.Parse(objNew.sims_paid_amount)).ToString();
                            objNew.sims_fee_type_desc = dr["sims_fee_type_desc"].ToString();
                            objNew.sims_expected_fee = dr["expected_fee"].ToString();
                            objNew.sims_paid_fee = dr["paid_fee"].ToString();
                            objNew.sims_balance_amount_total = dr["sims_fee_amount_total"].ToString(); //(decimal.Parse(objNew.sims_fee_amount_total) - decimal.Parse(objNew.sims_paid_amount_total)).ToString();
                            objNew.parent_email = dr["parent_email"].ToString();
                            objNew.parent_mobile = dr["parent_mobile"].ToString();
                            objNew.sims_term_code = dr["sims_fee_period_term_code"].ToString();
                            objNew.sims_term_description = dr["term_desc"].ToString();

                            objNew.isSelected = false;
                            mod_list.Add(objNew);
                        }
                    }
                    foreach (sims_defaulter t in mod_list)
                    {
                        sims_defaulter to = new sims_defaulter();
                        to.sims_enroll_number = t.sims_enroll_number;
                        to.student_name = t.student_name;
                        to.grade_name = t.grade_name;
                        to.section_name = t.section_name;
                        to.sims_sibling_parent_number = t.sims_sibling_parent_number;
                        to.sims_father_name = t.sims_father_name;
                        to.sims_paid_fee = t.sims_paid_fee;
                        to.sims_expected_fee = t.sims_expected_fee;
                        to.sims_balance_amount_total = t.sims_balance_amount_total;
                        to.parent_email = t.parent_email;
                        to.parent_mobile = t.parent_mobile;
                        to.sims_term_code = t.sims_term_code;
                        to.sims_term_description = t.sims_term_description;
                        int cnt = summary.Where(x => x.sims_enroll_number == t.sims_enroll_number).Count();
                        if (cnt == 0)
                            summary.Add(to);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
        }

        [Route("emailtoDefaulters")]
        public HttpResponseMessage DefaultersList(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@email_user_code", obj.sims_sibling_parent_number),
                                new SqlParameter("@recepient_id", obj.parent_email),
                                new SqlParameter("@email_subject", obj.email_subject),
                                new SqlParameter("@email_message", obj.email_message)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }

        [Route("emailtoDefaultersNew")]
        public HttpResponseMessage DefaultersListNew(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                            new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "V"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@email_user_code", obj.sims_sibling_parent_number),
                                new SqlParameter("@recepient_id", obj.parent_email),
                                new SqlParameter("@email_subject", obj.email_subject),
                                new SqlParameter("@email_message", obj.email_message),
                                new SqlParameter("@email_cc", obj.email_cc),
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }


        //New SMS/Alert Methods
        [Route("GetDefaulterSMSMessage")]
        public HttpResponseMessage GetDefaulterSMSMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        [Route("GetDefaulterAlertMessage")]
        public HttpResponseMessage GetDefaulterAlertMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B"),
                                
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        [Route("GetDefaulterBlockPPNMessage")]
        public HttpResponseMessage GetDefaulterBlockPPNMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        [Route("smstoDefaulters")]
        public HttpResponseMessage smstoDefaulters(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@sms_user_code", obj.sims_sibling_parent_number),
                                new SqlParameter("@recepient_id", obj.parent_mobile),
                                new SqlParameter("@sms_subject", obj.email_subject),
                                new SqlParameter("@sms_message", obj.email_message)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }

        [Route("alerttoDefaulters")]
        public HttpResponseMessage alerttoDefaulters(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@sms_user_code", obj.sims_sibling_parent_number),
                                new SqlParameter("@recepient_id", obj.parent_mobile),
                                new SqlParameter("@sms_subject", obj.email_subject),
                                new SqlParameter("@sms_message", obj.email_message)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }

        [Route("BlockDefaulters")]
        public HttpResponseMessage BlockDefaulters(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sms_user_code", obj.sims_sibling_parent_number),
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }

        [Route("Year_month")]
        public HttpResponseMessage Year_month(string year)
        {
            List<sims_defaulter> employee_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M"),
                            new SqlParameter("@year1",year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            //ot_hour simsobj = new ot_hour();
                            //simsobj.year_month = dr["year_month"].ToString();
                            //simsobj.year_month_name = dr["year_month_name"].ToString();

                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_term_code = dr["year_month"].ToString();
                            objNew.sims_term_description = dr["year_month_name"].ToString();
                            employee_list.Add(objNew);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("GetStudentStatus")]
        public HttpResponseMessage GetStudentStatus()
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","SA")                                
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year_status = dr["sims_appl_parameter"].ToString();                            
                            objNew.sims_academic_year_status_desc = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK,e.Message);
            }
        }

        [Route("GetAdmission_EmailIds")]
        public HttpResponseMessage GetAdmission_EmailIds(string admission_nos)
        {
            List<simsClass> email_list = new List<simsClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_defaulter_rpt_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Z'),
                            new SqlParameter("@adm_nos", admission_nos),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();
                            simsobj.emailid = dr["EmailID"].ToString();
                            simsobj.chk_email = true;
                            email_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, email_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, email_list);
            }
        }
        #endregion

    }
}