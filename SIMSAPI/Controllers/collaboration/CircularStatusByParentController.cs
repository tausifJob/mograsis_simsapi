﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COLLABORATION;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/CircularStatusByParent")]
    public class CircularStatusByParentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("GetCircularstatusbyparent")]
        public HttpResponseMessage GetCircularstatusbyparent(string cur_code,string acad_year,string circular_status,string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            if (search == "undefined")
            {
                search = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Circular_status_By_Parent]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@circular_status", circular_status),
                            new SqlParameter("@search", search)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                            simsobj.sims_circular_number = dr["sims_circular_number"].ToString();
                            simsobj.sims_circular_title = dr["sims_circular_title"].ToString();
                            simsobj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            simsobj.sims_circular_user_group_code = dr["sims_circular_user_group_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.parent_name = dr["parent_name"].ToString();
                            simsobj.sims_circular_user_name = dr["sims_circular_user_name"].ToString();
                            simsobj.circular_status = dr["circular_status"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetCircularuserstatus")]
        public HttpResponseMessage GetCircularuserstatus(string cur_code, string acad_year, string circular_status, string user_group, string from, string to, string search, string report_view)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            if (search == "undefined")
            {
                search = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_crl_Circular_User_Status]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@circular_status", circular_status),
                            new SqlParameter("@user_group_code", user_group),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to)),
                            new SqlParameter("@search_circular", search),
                            new SqlParameter("@report_view", report_view),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                             simsobj.sims_circular_date = dr["sims_circular_date"].ToString();
                             simsobj.sims_circular_publish_date = dr["sims_circular_publish_date"].ToString();
                             simsobj.sims_circular_expiry_date = dr["sims_circular_expiry_date"].ToString();
                             simsobj.sims_circular_title = dr["sims_circular_title"].ToString();
                             simsobj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                             simsobj.sims_circular_user_group_code = dr["sims_circular_user_group_code"].ToString();
                             simsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             simsobj.sims_circular_user_name = dr["sims_circular_user_name"].ToString();
                             simsobj.sims_circular_user_alias_name = dr["sims_circular_user_alias_name"].ToString();
                             simsobj.sims_circular_status = dr["sims_circular_status"].ToString();
                             simsobj.sims_circular_status_desc = dr["sims_circular_status_desc"].ToString();
                           
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Getcommunicationreport")]
        public HttpResponseMessage Getcommunicationreport(string cur_code, string acad_year, string circular_status, string user_group, string from, string to, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            if (search == "undefined")
            {
                search = null;
            }
            if (from == "undefined")
            {
                from = null;
            }
            if (to == "undefined")
            {
                to = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_crl_Communication_Report]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@circular_status", circular_status),
                            new SqlParameter("@user_group_code", user_group),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to)),
                            new SqlParameter("@search_circular", search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            circularByParent simsobj = new circularByParent();
                            simsobj.sender_id = dr["sender_id"].ToString();
                            simsobj.sender_name = dr["sender_name"].ToString();
                            simsobj.rece_id = dr["rece_id"].ToString();
                            simsobj.receiver_name = dr["receiver_name"].ToString();
                            simsobj.sims_comm_message = dr["sims_comm_message"].ToString();
                            simsobj.sims_comm_tran_date = dr["sims_comm_tran_date"].ToString();
                            simsobj.sims_comm_subject = dr["sims_comm_subject"].ToString();

                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Getemailstatusdetails")]
        public HttpResponseMessage Getemailstatusdetails(string from, string to, string status,string view_as)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_crl_email_status_report]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to)),
                            new SqlParameter("@status", status),
                            new SqlParameter("@view_as", view_as),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                            simsobj.sender = dr["sender"].ToString();
                            simsobj.date1 = dr["date1"].ToString();
                            simsobj.subject1 = dr["subject1"].ToString();
                            simsobj.status1 = dr["status1"].ToString();
                            simsobj.status = dr["status"].ToString();
                            simsobj.receiver = dr["receiver"].ToString();
                            simsobj.parent_id = dr["parent_id"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();

                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("Getnewbyuser")]
        public HttpResponseMessage Getnewbyuser(string cur_code, string user_group, string from, string to, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            if (search == "undefined")
            {
                search = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_crl_Communication_Report]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@user_group_code", user_group),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to)),
                            new SqlParameter("@search_circular", search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                            simsobj.group_name = dr["group_name"].ToString();
                            simsobj.sims_news_number = dr["sims_news_number"].ToString();
                            simsobj.sims_news_title = dr["sims_news_title"].ToString();
                            simsobj.sims_news_desc = dr["sims_news_desc"].ToString();
                            simsobj.sims_news_date = dr["sims_news_date"].ToString();
                            simsobj.sims_news_expiry_date = dr["sims_news_expiry_date"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Gethouselistbyage")]
        public HttpResponseMessage Gethouselistbyage(string cur_code, string acad_year, string grade, string section, string age_cal_date, string dob_less, string dob_greater)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_evt_StudentHouseListReportforVariousAgeGroup]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                            new SqlParameter("@dob_less_than", db.DBYYYYMMDDformat(dob_less)),
                            new SqlParameter("@dob_greater", db.DBYYYYMMDDformat(dob_greater)),
                            new SqlParameter("@age_cal_date", db.DBYYYYMMDDformat(age_cal_date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            simsobj.house_name = dr["house_name"].ToString();
                            simsobj.age_calculate = dr["age_calculate"].ToString();

                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("Getenrolllist")]
        public HttpResponseMessage Getenrolllist(string cur_code, string acad_year, string grade, string section, string status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<circularByParent> doc_list = new List<circularByParent>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_enroll_student_in_transport]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", grade+section),
                            new SqlParameter("@status", status),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            circularByParent simsobj = new circularByParent();
                             simsobj.grade_name = dr["grade_name"].ToString();
                             simsobj.section_name = dr["section_name"].ToString();
                             simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                             simsobj.student_name = dr["student_name"].ToString(); 
                             simsobj.address_details = dr["address_details"].ToString();
                             simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                             simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                             simsobj.bus_no = dr["bus_no"].ToString();

                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
    }
}