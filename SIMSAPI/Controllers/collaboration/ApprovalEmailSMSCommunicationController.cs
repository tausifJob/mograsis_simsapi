﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/ApprovalEmailSMSCommunicationController")]
    public class ApprovalEmailSMSCommunicationController : ApiController
    {
        [HttpGet]        
        [Route("getApprovalData")]
        public HttpResponseMessage getApprovalData(string from_date,string to_date, string sims_sms_subject,string sims_recepient_id,string type,string operand)
        {
            List<ApprovalESC> list_Approval_data = new List<ApprovalESC>();
            try
            {
                if(string.IsNullOrEmpty(from_date) || from_date.Equals("undefined"))
                {
                    from_date = null;
                }
                if (string.IsNullOrEmpty(to_date) || to_date.Equals("undefined"))
                {
                    to_date = null;
                }
                if (string.IsNullOrEmpty(sims_sms_subject) || sims_sms_subject.Equals("undefined"))
                {
                    sims_sms_subject = null;
                }
                if (string.IsNullOrEmpty(sims_recepient_id) || sims_recepient_id.Equals("undefined"))
                {
                    sims_recepient_id = null;
                }

                if (type.Equals("S"))
                {
                    try
                    {
                        #region SMS 

                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Email_Sms_Communication_proc]",
                                new List<SqlParameter>()
                                 {
                            new SqlParameter("@opr", operand),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@sims_sms_subject",sims_sms_subject),
                            new SqlParameter("@sims_recepient_id", sims_recepient_id),
                                 }
                                 );
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    ApprovalESC obj_ApprovalESC = new ApprovalESC();
                                    #region SMS
                                    obj_ApprovalESC.sims_recepient_id = dr["sims_recepient_id"].ToString(); 
                                    obj_ApprovalESC.sims_sms_number = dr["sims_sms_number"].ToString(); 
                                    obj_ApprovalESC.sims_sms_date = db.UIDDMMYYYYformat(dr["sims_sms_date"].ToString());
                                    obj_ApprovalESC.sims_sms_message = dr["sims_sms_message"].ToString();
                                    #endregion
                                    list_Approval_data.Add(obj_ApprovalESC);
                                }
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list_Approval_data);

                        #endregion
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (type.Equals("E"))
                {
                    try
                    {
                        #region Email 
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Email_Sms_Communication_proc]",
                                new List<SqlParameter>()
                                 {
                            new SqlParameter("@opr", operand),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@sims_email_subject",sims_sms_subject),
                            new SqlParameter("@sims_recepient_id", sims_recepient_id),
                                 }
                                 );
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    ApprovalESC obj_ApprovalESC = new ApprovalESC();
                                    
                                    obj_ApprovalESC.sims_recepient_id = dr["sims_recepient_id"].ToString();
                                    obj_ApprovalESC.sims_email_number = dr["sims_email_number"].ToString();                                    
                                    obj_ApprovalESC.sims_email_subject = dr["sims_email_subject"].ToString();                                    
                                    obj_ApprovalESC.sims_email_date = (dr["sims_email_date"].ToString());
                                    obj_ApprovalESC.sims_email_message = dr["sims_email_message"].ToString();
                                    
                                    list_Approval_data.Add(obj_ApprovalESC);
                                }
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list_Approval_data);
                        #endregion
                    }
                    catch (Exception ex)
                    {                        
                    }
                }
                if (type.Equals("C"))
                {
                    try
                    {
                        #region Communication 
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();

                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Email_Sms_Communication_proc]",
                                new List<SqlParameter>()
                                 {
                            new SqlParameter("@opr",operand),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@sims_comm_message",sims_recepient_id),
                            new SqlParameter("@sims_comm_recepient_id", sims_sms_subject),
                                 }
                                 );
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    ApprovalESC obj_ApprovalESC = new ApprovalESC();                                    
                                    obj_ApprovalESC.sims_comm_number = dr["sims_comm_number"].ToString();
                                    obj_ApprovalESC.sims_comm_tran_date = db.UIDDMMYYYYformat(dr["sims_comm_tran_date"].ToString());
                                    obj_ApprovalESC.sims_comm_message = dr["sims_comm_message"].ToString();                                    
                                    list_Approval_data.Add(obj_ApprovalESC);
                                }
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list_Approval_data);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list_Approval_data);
        }

        [HttpPost]
        [Route("approvalSelectedDataEmail")]
        public HttpResponseMessage approvalSelectedDataEmail(List<string> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    foreach(string dataActual in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Email_Sms_Communication_proc]",
                                                        new List<SqlParameter>()
                                                        {
                             #region Adding data into database
                             new SqlParameter("@opr", "Q"),
                             new SqlParameter("@sims_email_number",dataActual),
                                                            #endregion
                                                        });

                        #region Sending Response
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                        #endregion


                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [HttpPost]
        [Route("approvalSelectedDataSMS")]
        public HttpResponseMessage approvalSelectedDataSMS(List<string> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (string dataActual in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Email_Sms_Communication_proc]",
                                                        new List<SqlParameter>()
                                                        {
                             #region Adding data into database
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@sims_sms_number",dataActual),
                                                            #endregion
                                                        });

                        #region Sending Response
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                        #endregion


                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        [HttpPost]
        [Route("approvalSelectedDataCommunication")]
        public HttpResponseMessage approvalSelectedDataCommunication(List<string> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (string dataActual in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Email_Sms_Communication_proc]",
                                                        new List<SqlParameter>()
                                                        {
                             #region Adding data into database
                             new SqlParameter("@opr", "P"),
                             new SqlParameter("@sims_comm_number",dataActual),
                                                            #endregion
                                                        });

                        #region Sending Response
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                        #endregion


                    }



                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}