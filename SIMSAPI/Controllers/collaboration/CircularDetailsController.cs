﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/common/CircularDetailsController")]
    public class CircularDetailsController : ApiController
    {
        [HttpGet]
        [Route("getDetailsOfCircular")]
        public HttpResponseMessage getDetailsOfCircular( string sims_academic_year,string sims_cur_code ,string from_date,string to_date,string sims_circular_user_group_code, string sims_circular_title)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularDetails(),PARAMETERS :: NA";         

            List<ComnCircularDetails> lst_circularnum = new List<ComnCircularDetails>();

            if (sims_academic_year.Equals("undefined"))
            {
                sims_academic_year = null;
            }
            if (sims_cur_code.Equals("undefined"))
            {
                sims_cur_code = null;
            }
            if (from_date.Equals("undefined"))
            {
                from_date = null;
            }
            if (to_date.Equals("undefined"))
            {
                to_date = null;
            }
            //if (sims_circular_title.Equals("undefined"))
            //{
            //    sims_circular_title = null;
            //}
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),                            
                            new SqlParameter("@sims_academic_year", sims_academic_year),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@sims_circular_user_group_code", sims_circular_user_group_code),
                            new SqlParameter("@sims_circular_title", sims_circular_title),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnCircularDetails obj_ComnCircularDetails=new    ComnCircularDetails();
                            obj_ComnCircularDetails.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj_ComnCircularDetails.sims_circular_title = dr["sims_circular_title"].ToString(); 
                            obj_ComnCircularDetails.sims_circular_date = db.UIDDMMYYYYformat(dr["sims_circular_date"].ToString());
                            obj_ComnCircularDetails.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                            obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                            obj_ComnCircularDetails.sims_acknowledgment_count = dr["sims_acknowledgment_count"].ToString();
                            obj_ComnCircularDetails.sims_sent_count = dr["sims_sent_count"].ToString();
                            obj_ComnCircularDetails.sims_comn_user_group_name = dr["comn_user_group_name"].ToString();
                            lst_circularnum.Add(obj_ComnCircularDetails);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
        }

        [HttpGet]
        [Route("newsDetails")]
        public HttpResponseMessage newsDetails(string sims_academic_year, string sims_cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : newsDetails(),PARAMETERS :: NA";

            List<ComnCircularDetails> lst_circularnum = new List<ComnCircularDetails>();
            if (sims_academic_year.Equals("undefined"))
            {
                sims_academic_year = null;
            }
            if (sims_cur_code.Equals("undefined"))
            {
                sims_cur_code = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'N'),
                            new SqlParameter("@sims_academic_year", sims_academic_year),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnCircularDetails obj_ComnCircularDetails = new ComnCircularDetails();
                            obj_ComnCircularDetails.sims_news_title = dr["sims_news_title"].ToString();                          
                            lst_circularnum.Add(obj_ComnCircularDetails);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
        }
        [HttpGet]
        [Route("circularDetails")]
        public HttpResponseMessage circularDetails(string sims_academic_year, string sims_cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : circularDetails(),PARAMETERS :: NA";

            List<ComnCircularDetails> lst_circularnum = new List<ComnCircularDetails>();
            if (sims_academic_year.Equals("undefined"))
            {
                sims_academic_year = null;
            }
            if (sims_cur_code.Equals("undefined"))
            {
                sims_cur_code = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@sims_academic_year", sims_academic_year),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnCircularDetails obj_ComnCircularDetails = new ComnCircularDetails();
                            obj_ComnCircularDetails.sims_news_title = dr["sims_circular_title"].ToString();
                            lst_circularnum.Add(obj_ComnCircularDetails);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
        }

        [HttpGet]
        [Route("deailsacknowledgment")]
        public HttpResponseMessage deailsacknowledgment(string sims_circular_number,string type,string status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : deailsacknowledgment(),PARAMETERS :: NA";
            string oper = string.Empty, sims_circular_status=string.Empty,acadamicYear=string.Empty,cur_cod=string.Empty;
            List<ComnCircularDetails> lst_circularnum = new List<ComnCircularDetails>();
            if (sims_circular_number.Equals("undefined"))
            {
                sims_circular_number = null;
            }
            if (type.Equals("undefined"))
            {
                oper = "X";
            }
            else
            {
                oper = type;
            }           
            if(status.Equals("A"))
            {
                sims_circular_status = "R";
            }
            else
            {
                sims_circular_status = null;
            }
            if (status.Contains("/"))
            {
                string[] spilitData = status.Split('/');
                try
                {
                    sims_circular_status = null;
                    acadamicYear = spilitData[2];
                    cur_cod = spilitData[1];
                }
                catch (Exception ex)
                {

                }

            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = null;
                    if (oper!="P" && oper!="Q")/// If user select parent or student it will not going this option
                    {
                       dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                                       new List<SqlParameter>()
                                        {
                            new SqlParameter("@opr",oper),
                            new SqlParameter("@sims_circular_number", sims_circular_number),
                            new SqlParameter("@sims_circular_status", sims_circular_status),
                                        }); 
                    }
                    else
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                                       new List<SqlParameter>()
                                        {
                            new SqlParameter("@opr",oper),
                            new SqlParameter("@sims_circular_number", sims_circular_number),
                            new SqlParameter("@sims_circular_status", sims_circular_status),
                            new SqlParameter("@sims_cur_code", cur_cod),
                            new SqlParameter("@sims_academic_year", acadamicYear),
                                        });
                    }
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnCircularDetails obj_ComnCircularDetails = new ComnCircularDetails();
                            if (type.Equals("P"))
                            {
                                obj_ComnCircularDetails.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                                obj_ComnCircularDetails.username = dr["UserName"].ToString();
                                obj_ComnCircularDetails.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                obj_ComnCircularDetails.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            }
                            else if (type.Equals("Q"))
                            {
                                obj_ComnCircularDetails.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                                obj_ComnCircularDetails.parent_username = dr["Parent_UserName"].ToString();
                                obj_ComnCircularDetails.sims_sibling_student_enroll_number = dr["sims_sibling_student_enroll_number"].ToString();
                                obj_ComnCircularDetails.student_username = dr["Student_UserName"].ToString();
                                obj_ComnCircularDetails.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                obj_ComnCircularDetails.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            }
                            else if (type.Equals("R"))
                            {
                                obj_ComnCircularDetails.sims_circular_user_name = dr["sims_circular_user_name"].ToString();
                                obj_ComnCircularDetails.employee_UserName = dr["employee_UserName"].ToString();
                                obj_ComnCircularDetails.em_email = dr["em_email"].ToString();
                            }
                            else if (type.Equals("T"))
                            {
                                obj_ComnCircularDetails.comn_user_code = dr["comn_user_code"].ToString();
                                obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                                obj_ComnCircularDetails.comn_user_email = dr["comn_user_email"].ToString();

                            }
                            else if (type.Equals("U"))
                            {
                                obj_ComnCircularDetails.comn_user_code = dr["comn_user_code"].ToString();
                                obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                                obj_ComnCircularDetails.comn_user_email = dr["comn_user_email"].ToString();
                            }
                            else if (type.Equals("V"))
                            {
                                obj_ComnCircularDetails.comn_user_code = dr["comn_user_code"].ToString();
                                obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                                obj_ComnCircularDetails.comn_user_email = dr["comn_user_email"].ToString();

                            }
                            else if (type.Equals("W"))
                            {
                                obj_ComnCircularDetails.comn_user_code = dr["comn_user_code"].ToString();
                                obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                                obj_ComnCircularDetails.comn_user_email = dr["comn_user_email"].ToString();
                            }
                            else if (type.Equals("X"))
                            {
                                obj_ComnCircularDetails.comn_user_name = dr["comn_user_name"].ToString();
                                obj_ComnCircularDetails.comn_user_alias = dr["comn_user_alias"].ToString();
                                obj_ComnCircularDetails.comn_user_group_name= dr["comn_user_group_name"].ToString();
                            }
                            lst_circularnum.Add(obj_ComnCircularDetails);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
        }

        [Route("getcirculartype")]
        public HttpResponseMessage getcirculartype()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Circular_Details_Proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "K"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_circular_type = dr["sims_circular_type"].ToString();
                            sequence.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

    }
}