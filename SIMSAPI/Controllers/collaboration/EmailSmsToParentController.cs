﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/common/EmailSms")]
    public class EmailSmsToParentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private string sims_smtp_address = "", sims_smtp_port = "", sims_smtp_password = "", sims_sslReq = "", sims_senderid = "";


        [Route("GetEmployeesEmailIds")]
        public HttpResponseMessage GetEmployeesEmailIds(string desg, string dept)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeesEmailIds(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentEmailId", "ParentEmailId"));

            List<Comn_email> email = new List<Comn_email>();
            Comn_email comnObj;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "E"),
                            new SqlParameter("@desg",desg),
                            new SqlParameter("@dept",dept),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comnObj = new Comn_email();
                            comnObj.username = dr["em_login_code"].ToString();
                            comnObj.emp_emailid = dr["em_email"].ToString();
                            comnObj.name = dr["emp_name"].ToString();
                            comnObj.subject = "";
                            comnObj.body = "";
                            comnObj.emailsendto = dr["em_email"].ToString();
                            comn_email_attachments Fm = new comn_email_attachments();
                            Fm.attFilePath = string.Empty;
                            Fm.attFilename = string.Empty;
                            List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                            lst_fm.Add(Fm);
                            comnObj.comn_email_attachments = lst_fm;
                            email.Add(comnObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, email);
            }
        }

        [Route("GetEmployeesContacts")]
        public HttpResponseMessage GetEmployeesContacts(string desg, string dept)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeesContacts(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EmployeesContacts", "EmployeesContacts"));

            List<Comn_SMS> sms = new List<Comn_SMS>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "N"),
                            new SqlParameter("@desg",desg),
                            new SqlParameter("@dept",dept),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn_SMS comnObj = new Comn_SMS();
                            comnObj.username = dr["em_login_code"].ToString();
                            comnObj.sms_contact_number = dr["em_mobile"].ToString();
                            comnObj.name = dr["emp_name"].ToString();
                            comnObj.sms_smstext = "";
                            comnObj.sms_smssendto = dr["em_mobile"].ToString();
                            sms.Add(comnObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sms);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, sms);
            }
        }

        [Route("GetSpecificParentEmailIds")]
        public HttpResponseMessage GetSpecificParentEmailIds(bool iffather, bool ifmother, bool ifguardian, bool active, string sections)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificParentEmailIds(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentEmailId", "ParentEmailId"));

            List<Comn_email> email = new List<Comn_email>();
            Comn_email comnObj;
            string subopr = "", uperOperand = string.Empty;
            try
            {
                if (sections.Contains("true"))
                {
                    sections = sections.Replace("true", "").Trim();
                    uperOperand = "Z";
                }
                else
                {
                    uperOperand = "S";
                }
                #region FATHER EMAILS
                if (iffather == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'F'),
                                new SqlParameter("@sections",sections)
                            });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr["sims_parent_number"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.name = dr["father_name"].ToString();
                                    comnObj.father_emailid = dr["sims_parent_father_email"].ToString();
                                    comnObj.fflag = "1";

                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr["sims_parent_father_email"].ToString();
                                    comnObj.mother_emailid = "";
                                    comnObj.mflag = "0";
                                    comnObj.guardian_emailid = "";
                                    comnObj.gflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr.Close();
                        }
                    }
                }

                #endregion

                #region MOTHER EMAILS
                if (ifmother == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();


                            SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",uperOperand),
                                new SqlParameter("@subopr",'M'),
                                new SqlParameter("@sections",sections)
                            });
                            if (dr2.HasRows)
                            {
                                while (dr2.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr2["sims_parent_number"].ToString();
                                    comnObj.name = dr2["mother_name"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr2["sims_student_enroll_number"].ToString();
                                    }

                                    comnObj.mother_emailid = dr2["sims_parent_mother_email"].ToString();
                                    comnObj.mflag = "1";

                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr2["sims_parent_mother_email"].ToString();
                                    comnObj.father_emailid = "";
                                    comnObj.fflag = "0";
                                    comnObj.guardian_emailid = "";
                                    comnObj.gflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr2.Close();
                        }
                    }
                }
                #endregion

                #region GUARDIAN EMAILS
                if (ifguardian == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db3 = new DBConnection())
                        {
                            db3.Open();
                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'G'),
                                new SqlParameter("@sections",sections)
                            });
                            if (dr3.HasRows)
                            {
                                while (dr3.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr3["sims_parent_number"].ToString();
                                    comnObj.name = dr3["guardian_name"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr3["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.guardian_emailid = dr3["sims_parent_guardian_email"].ToString();
                                    comnObj.gflag = "1";
                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr3["sims_parent_guardian_email"].ToString();
                                    comnObj.mother_emailid = "";
                                    comnObj.mflag = "0";
                                    comnObj.father_emailid = "";
                                    comnObj.fflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr3.Close();
                        }
                    }
                }
                #endregion

                #region Active EMAILS
                if (active == true)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[GetActiveMem_Email_proc]",
                         new List<SqlParameter>()
                            {
                               new SqlParameter("@sections",sections)
                            });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                comnObj = new Comn_email();
                                comnObj.username = dr1["sims_parent_number"].ToString();
                                comnObj.name = dr1["ParentName"].ToString();
                                comnObj.enrollnumber = dr1["sims_student_enroll_number"].ToString();
                                comnObj.emailsendto = dr1["EmailID"].ToString();
                                if (dr1[1].ToString() == "F")
                                {
                                    comnObj.father_emailid = dr1["EmailID"].ToString();
                                    comnObj.fflag = "1";
                                }
                                else
                                {
                                    comnObj.father_emailid = "";
                                    comnObj.fflag = "0";
                                }

                                if (dr1[1].ToString() == "M")
                                {
                                    comnObj.mother_emailid = dr1["EmailID"].ToString();
                                    comnObj.mflag = "1";
                                }
                                else
                                {
                                    comnObj.mother_emailid = "";
                                    comnObj.mflag = "0";
                                }

                                if (dr1[1].ToString() == "G")
                                {
                                    comnObj.guardian_emailid = dr1["EmailID"].ToString();
                                    comnObj.gflag = "1";
                                }
                                else
                                {
                                    comnObj.guardian_emailid = "";
                                    comnObj.gflag = "0";
                                }
                                comn_email_attachments Fm = new comn_email_attachments();
                                Fm.attFilePath = string.Empty;
                                Fm.attFilename = string.Empty;
                                List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                lst_fm.Add(Fm);
                                comnObj.comn_email_attachments = lst_fm;
                                email.Add(comnObj);
                            }
                        }
                        dr1.Close();
                    }
                }
                #endregion


                return Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, email);

            }
        }

        [Route("GetSpecificParentEmailIdsByBUS")]
        public HttpResponseMessage GetSpecificParentEmailIdsByBUS(bool iffather, bool ifmother, bool ifguardian, bool active, string curCode, string route_code, string route_direction,string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificParentEmailIdsByBUS(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EmailSmsToParentController", "GetSpecificParentEmailIdsByBUS"));

            List<Comn_email> email = new List<Comn_email>();
            Comn_email comnObj;
            string uperOperand = string.Empty;

            try
            {
                #region For removing zero
                try
                {
                    if (curCode.Contains("true"))
                    {
                        curCode = curCode.Replace("true", "").Trim();
                        uperOperand = "Z";
                    }
                    else
                    {
                        uperOperand = "S";
                    }
                }
                catch (Exception ex1)
                {
                    Log.Error(ex1);
                }
                #endregion

                #region FATHER EMAILS
                if (iffather == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'A'),
                                new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),

                            });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr["sims_parent_number"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.name = dr["father_name"].ToString();
                                    comnObj.father_emailid = dr["sims_parent_father_email"].ToString();
                                    comnObj.fflag = "1";

                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr["sims_parent_father_email"].ToString();
                                    comnObj.mother_emailid = "";
                                    comnObj.mflag = "0";
                                    comnObj.guardian_emailid = "";
                                    comnObj.gflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr.Close();
                        }
                    }
                }

                #endregion

                #region MOTHER EMAILS
                if (ifmother == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();


                            SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'B'),
                                new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),
                            });
                            if (dr2.HasRows)
                            {
                                while (dr2.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr2["sims_parent_number"].ToString();
                                    comnObj.name = dr2["mother_name"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr2["sims_student_enroll_number"].ToString();
                                    }

                                    comnObj.mother_emailid = dr2["sims_parent_mother_email"].ToString();
                                    comnObj.mflag = "1";

                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr2["sims_parent_mother_email"].ToString();
                                    comnObj.father_emailid = "";
                                    comnObj.fflag = "0";
                                    comnObj.guardian_emailid = "";
                                    comnObj.gflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr2.Close();
                        }
                    }
                }
                #endregion

                #region GUARDIAN EMAILS
                if (ifguardian == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db3 = new DBConnection())
                        {
                            db3.Open();
                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'C'),
                                new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),
                            });
                            if (dr3.HasRows)
                            {
                                while (dr3.Read())
                                {
                                    comnObj = new Comn_email();
                                    //comnObj.username = "";
                                    comnObj.username = dr3["sims_parent_number"].ToString();
                                    comnObj.name = dr3["guardian_name"].ToString();
                                    if (uperOperand.Equals("Z"))
                                    {

                                    }
                                    else {
                                        comnObj.enrollnumber = dr3["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.guardian_emailid = dr3["sims_parent_guardian_email"].ToString();
                                    comnObj.gflag = "1";
                                    comnObj.subject = "";
                                    comnObj.body = "";
                                    comnObj.emailsendto = dr3["sims_parent_guardian_email"].ToString();
                                    comnObj.mother_emailid = "";
                                    comnObj.mflag = "0";
                                    comnObj.father_emailid = "";
                                    comnObj.fflag = "0";
                                    comn_email_attachments Fm = new comn_email_attachments();
                                    Fm.attFilePath = string.Empty;
                                    Fm.attFilename = string.Empty;
                                    List<comn_email_attachments> lst_fm = new List<comn_email_attachments>();
                                    lst_fm.Add(Fm);
                                    comnObj.comn_email_attachments = lst_fm;
                                    email.Add(comnObj);
                                }
                            }
                            dr3.Close();
                        }
                    }
                }
                #endregion

                return Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, email);

            }
        }

        [Route("GetSpecificParentMobileByBUS")]
        public HttpResponseMessage GetSpecificParentMobileByBUS(bool iffather, bool ifmother, bool ifguardian, bool active, bool ifemergency, string curCode, string route_code, string route_direction,string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificParentMobileByBUS(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentMobile", "ParentMobile"));

            List<Comn_SMS> sms = new List<Comn_SMS>();
            Comn_SMS comnObj;
            string subopr = "", uperOperand = string.Empty;
            try
            {
                try
                {
                    if (curCode.Contains("true"))
                    {
                        curCode = curCode.Replace("true", "").Trim();
                        uperOperand = "P";
                    }
                    else
                    {
                        uperOperand = "M";
                    }
                }
                catch (Exception ex1)
                {
                    Log.Error(ex1);
                }
                #region FATHER CONTACTS
                if (iffather == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'T'),
                                new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),

                            });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr["sims_parent_number"].ToString();
                                    comnObj.name = dr["father_name"].ToString();
                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr["sims_student_enroll_number"].ToString();
                                    }

                                    comnObj.sms_father_contact_number1 = dr["sims_parent_father_mobile"].ToString();
                                    comnObj.sms_father_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr["sims_parent_father_mobile"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr.Close();
                        }
                    }
                }

                #endregion

                #region MOTHER EMAILS
                if (ifmother == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();
                            SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'U'),
                                new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),


                            });
                            if (dr2.HasRows)
                            {
                                while (dr2.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr2["sims_parent_number"].ToString();
                                    comnObj.name = dr2["mother_name"].ToString();
                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr2["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.sms_mother_contact_number2 = dr2["sims_parent_mother_mobile"].ToString();
                                    comnObj.sms_mother_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr2["sims_parent_mother_mobile"].ToString();
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr2.Close();
                        }
                    }
                }
                #endregion

                #region GUARDIAN EMAILS
                if (ifguardian == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db3 = new DBConnection())
                        {
                            db3.Open();
                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",uperOperand),
                                new SqlParameter("@subopr",'V'),
                                      new SqlParameter("@cur_code",curCode),
                                new SqlParameter("@route_code ",route_code),
                                new SqlParameter("@route_direction",route_direction),
                                new SqlParameter("@sims_transport_academic_year",ac_year),


                            });
                            if (dr3.HasRows)
                            {
                                while (dr3.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr3["sims_parent_number"].ToString();
                                    comnObj.name = dr3["guardian_name"].ToString();
                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr3["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.sms_guardian_contact_number3 = dr3["sims_parent_guardian_mobile"].ToString();
                                    comnObj.sms_guardian_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr3["sims_parent_guardian_mobile"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr3.Close();
                        }
                    }
                }
                #endregion

           
                return Request.CreateResponse(HttpStatusCode.OK, sms);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.OK, sms);
            }
        }

        [Route("GetSpecificParentMobile")]
        public HttpResponseMessage GetSpecificParentMobile(bool iffather, bool ifmother, bool ifguardian, bool active, bool ifemergency, string sections, string ac_year)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificParentMobile(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentMobile", "ParentMobile"));

            List<Comn_SMS> sms = new List<Comn_SMS>();
            Comn_SMS comnObj;
            string subopr = "", uperOperand = string.Empty;
            try
            {
                if (sections.Contains("true"))
                {
                    sections = sections.Replace("true", "").Trim();
                    uperOperand = "P";
                }
                else
                {
                    uperOperand = "M";
                }
                #region FATHER CONTACTS
                if (iffather == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'F'),
                                new SqlParameter("@sections",sections),
                                new SqlParameter("@sims_transport_academic_year",ac_year)
                            });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr["sims_parent_number"].ToString();
                                    comnObj.name = dr["father_name"].ToString();

                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.sms_father_contact_number1 = dr["sims_parent_father_mobile"].ToString();
                                    comnObj.sms_father_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr["sims_parent_father_mobile"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr.Close();
                        }
                    }
                }

                #endregion

                #region MOTHER EMAILS
                if (ifmother == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db2 = new DBConnection())
                        {
                            db2.Open();
                            SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", uperOperand),
                                new SqlParameter("@subopr",'M'),
                                 new SqlParameter("@sections",sections),
                                 new SqlParameter("@sims_transport_academic_year",ac_year)
                            });
                            if (dr2.HasRows)
                            {
                                while (dr2.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr2["sims_parent_number"].ToString();
                                    comnObj.name = dr2["mother_name"].ToString();
                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr2["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.sms_mother_contact_number2 = dr2["sims_parent_mother_mobile"].ToString();
                                    comnObj.sms_mother_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr2["sims_parent_mother_mobile"].ToString();
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr2.Close();
                        }
                    }
                }
                #endregion

                #region GUARDIAN EMAILS
                if (ifguardian == true)
                {
                    if (active != true)
                    {
                        using (DBConnection db3 = new DBConnection())
                        {
                            db3.Open();
                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",uperOperand),
                                new SqlParameter("@subopr",'G'),
                                 new SqlParameter("@sections",sections),
                                 new SqlParameter("@sims_transport_academic_year",ac_year)
                                 
                            });
                            if (dr3.HasRows)
                            {
                                while (dr3.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    // comnObj.user_name = "";
                                    comnObj.username = dr3["sims_parent_number"].ToString();
                                    comnObj.name = dr3["guardian_name"].ToString();
                                    if (uperOperand.Equals("P"))
                                    {

                                    }
                                    else {
                                        comnObj.enroll_number = dr3["sims_student_enroll_number"].ToString();
                                    }
                                    comnObj.sms_guardian_contact_number3 = dr3["sims_parent_guardian_mobile"].ToString();
                                    comnObj.sms_guardian_flag = "1";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr3["sims_parent_guardian_mobile"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr3.Close();
                        }
                    }
                }
                #endregion

                #region EMERGENCY CONTACTS
                if (ifemergency == true)
                {
                    if (active != true)
                    {
                        #region Get Student Emergency Contacts1
                        using (DBConnection db4 = new DBConnection())
                        {
                            db4.Open();
                            SqlDataReader dr4 = db4.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@subopr",'C'),
                                 new SqlParameter("@sections",sections),
                                 new SqlParameter("@sims_transport_academic_year",ac_year)

                            });
                            if (dr4.HasRows)
                            {
                                while (dr4.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    comnObj.username = dr4["sims_sibling_parent_number"].ToString();
                                    comnObj.name = dr4["ParentName"].ToString();
                                    comnObj.enroll_number = dr4["sims_student_enroll_number"].ToString();
                                    comnObj.sms_emergency_contact_number4 = dr4["sims_student_emergency_contact_number1"].ToString();
                                    comnObj.sms_emergency_flag = "0";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr4["sims_student_emergency_contact_number1"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr4.Close();
                        }
                        #endregion

                        #region Get Student Emergency Contacts2
                        using (DBConnection db5 = new DBConnection())
                        {
                            db5.Open();
                            SqlDataReader dr5 = db5.ExecuteStoreProcedure("sims.sims_email_sms_to_parent_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@subopr",'D'),
                                 new SqlParameter("@sections",sections),
                                 new SqlParameter("@sims_transport_academic_year",ac_year)

                            });
                            if (dr5.HasRows)
                            {
                                while (dr5.Read())
                                {
                                    comnObj = new Comn_SMS();
                                    comnObj.username = dr5["sims_sibling_parent_number"].ToString();
                                    comnObj.name = dr5["ParentName"].ToString();
                                    comnObj.enroll_number = dr5["sims_student_enroll_number"].ToString();
                                    comnObj.sms_emergency_contact_number44 = dr5["sims_student_emergency_contact_number2"].ToString();
                                    comnObj.sms_emergency_flag = "0";
                                    comnObj.sms_smstext = "";
                                    comnObj.sms_smssendto = dr5["sims_student_emergency_contact_number2"].ToString();
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                    sms.Add(comnObj);
                                }
                            }
                            dr5.Close();
                        }
                        #endregion
                    }
                }
                #endregion

                #region Active Contacts
                if (active == true)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("[sims].[GetActiveMem_SMS_proc]",
                         new List<SqlParameter>()
                            {
                                new SqlParameter("@sections",sections),
                            });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                comnObj = new Comn_SMS();
                                comnObj.username = dr1["sims_parent_number"].ToString();
                                comnObj.name = dr1["ParentName"].ToString();
                                comnObj.enroll_number = dr1["sims_student_enroll_number"].ToString();
                                comnObj.sms_smssendto = dr1["ContactNum"].ToString();
                                if (dr1[1].ToString() == "F")
                                {
                                    comnObj.sms_father_contact_number1 = dr1["ContactNum"].ToString();
                                    comnObj.sms_father_flag = "1";
                                }
                                else
                                {
                                    comnObj.sms_father_contact_number1 = "";
                                    comnObj.sms_father_flag = "0";
                                }

                                if (dr1[1].ToString() == "M")
                                {
                                    comnObj.sms_mother_contact_number2 = dr1["ContactNum"].ToString();
                                    comnObj.sms_mother_flag = "1";
                                }
                                else
                                {
                                    comnObj.sms_mother_contact_number2 = "";
                                    comnObj.sms_mother_flag = "0";
                                }

                                if (dr1[1].ToString() == "G")
                                {
                                    comnObj.sms_guardian_contact_number3 = dr1["ContactNum"].ToString();
                                    comnObj.sms_guardian_flag = "1";
                                }
                                else
                                {
                                    comnObj.sms_guardian_contact_number3 = "";
                                    comnObj.sms_guardian_flag = "0";
                                }
                                sms.Add(comnObj);
                            }
                        }
                        dr1.Close();
                    }
                }
                #endregion

                return Request.CreateResponse(HttpStatusCode.OK, sms);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, sms);

            }
        }

        [Route("GetcheckEmailProfile")]
        public HttpResponseMessage GetcheckEmailProfile()
        {
            Comn_email em = new Comn_email();
            List<Comn_email> lstEmails = new List<Comn_email>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'T'),
                         //   new SqlParameter("@sims_msg_sr_no",sr_no)
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            em.username = dr["sims_smtp_username"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, em.username);
            }
            return Request.CreateResponse(HttpStatusCode.OK, em.username);
        }

        [Route("ScheduleMails")]
        public HttpResponseMessage ScheduleMails(List<Comn_email> emailList)//, List<comn_email_attachments> attFiles
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ScheduleMails()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Mails/", "ScheduleMails"));
            List<comn052> feeType = new List<comn052>();
            string attachment = string.Empty;
            bool inserted = false;

            Message message = new Message();
            try
            {
                foreach (Comn_email email in emailList)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        string attachments = string.Empty;
                        if (emailList[0].comn_email_attachments != null)
                        {
                            foreach (comn_email_attachments att in emailList[0].comn_email_attachments)
                            {
                                // attachments="Docs/Attachments/";
                                attachments = attachments + ',' + att.attFilename;
                            }
                        }

                        if (!string.IsNullOrEmpty(attachments))
                        {
                            attachment = attachments.Substring(1);
                            // attachment = "Docs/Attachments/" + attachment;
                        }
                        else
                            attachment = email.sims_email_attachment;

                        int ins = db.ExecuteStoreProcedureforInsert("sims.Insert_Sims_Email_Schedule_proc",
                           new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_user_name", email.username),
                            new SqlParameter("@sims_enroll_number", email.enrollnumber),
                            new SqlParameter("@sims_email_id", email.emp_emailid),
                            new SqlParameter("@sims_sender_email_id", email.sender_emailid),
                            new SqlParameter("@sims_email_father_flag", email.fflag),
                            new SqlParameter("@sims_email_father_email_id1", email.emailsendto),
                            new SqlParameter("@sims_email_father_email_id11", ""),
                            new SqlParameter("@sims_email_father_email_id111", ""),
                            new SqlParameter("@sims_email_mother_flag", email.mflag),
                            new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                            new SqlParameter("@sims_email_mother_email_id22", ""),
                            new SqlParameter("@sims_email_mother_email_id222", ""),
                            new SqlParameter("@sims_email_guardian_flag", email.gflag),
                            new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                            new SqlParameter("@sims_email_guardian_email_id33", ""),
                            new SqlParameter("@sims_email_guardian_email_id333", ""),
                            new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_error_code", email.errorcode),
                            new SqlParameter("@sims_email_status", "U"),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                            new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                            new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                            new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                            new SqlParameter("@sims_email_subject", email.subject),
                            new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                            new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                            new SqlParameter("@sims_email_attachment",attachment)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        //if (dr.RecordsAffected > 0)
                        //{
                        //    // if (simsobj.opr.Equals("U"))
                        //    message.strMessage = "Schedule Mail Information Added Sucessfully";
                        //    message.systemMessage = string.Empty;
                        //    message.messageType = MessageType.Success;
                        //}
                        //GetScheduledMails();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
        }

        private int GetScheduledMails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificParentEmailIds(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentEmailId", "ParentEmailId"));

            List<Comn_email> email = new List<Comn_email>();
            int cnt = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            cnt = int.Parse(dr[0].ToString());
                        }
                    }
                    if (cnt > 0)
                    {
                        GetSpecificScheduledMails();
                    }
                }
                return cnt;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return cnt;
            }
        }

        private void GetSpecificScheduledMails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSpecificScheduledMails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ParentEmailId", "ParentEmailId"));

            List<Comn_email> lstEmails = new List<Comn_email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "A")
                    });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            Comn_email em = new Comn_email();
                            em.emaildate = dr["sims_email_date"].ToString();
                            em.username = dr["sims_email_user_code"].ToString();
                            if (!string.IsNullOrEmpty(em.username))
                                em.emp_emailid = dr["sims_recepient_id"].ToString();
                            em.emailnumber = dr["sims_email_number"].ToString();
                            em.emailsendto = dr["sims_recepient_id"].ToString();
                            em.sender_emailid = dr["sims_sender_id"].ToString();
                            em.sims_email_attachment = dr["sims_email_attachment"].ToString();
                            em.sims_email_message = dr["sims_email_message"].ToString();
                            em.sims_email_recurrance_id = dr["sims_email_recurrance_id"].ToString();
                            em.sims_email_schedule_date = dr["sims_email_schedule_date"].ToString();
                            em.sims_email_subject = dr["sims_email_subject"].ToString();
                            // em.sims_recepient_bcc_id = dr[""].ToString();
                            em.sims_recepient_cc_id = dr["sims_recepient_cc_id"].ToString();
                            em.sims_recepient_id = dr["sims_recepient_id"].ToString();
                            em.sims_recepient_search_id = dr["sims_recepient_id"].ToString();
                            em.status = dr["sims_email_status"].ToString();
                            //em.comn_email_attachments = atts;
                            em.subject = dr["sims_email_subject"].ToString();
                            em.body = dr["sims_email_message"].ToString();
                            em.sims_email_attachment = dr["sims_email_attachment"].ToString();
                            List<comn_email_attachments> lst_stratt = new List<comn_email_attachments>();
                            if (!string.IsNullOrEmpty(em.sims_email_attachment))
                            {
                                string[] stringSeparators = new string[] { "," };
                                var result = em.sims_email_attachment.Split(stringSeparators, StringSplitOptions.None);

                                foreach (string Str in result)
                                {
                                    comn_email_attachments eatt = new comn_email_attachments();
                                    eatt.attFilename = Str;
                                    eatt.attFilePath = "Docs\\Attachments\\";
                                    lst_stratt.Add(eatt);
                                }
                            }
                            em.comn_email_attachments = lst_stratt;
                            lstEmails.Add(em);
                        }
                    }

                }
                //return Request.CreateResponse(HttpStatusCode.OK, lstEmails);

                GetSenderEID(lstEmails);
                InsertEmailTransaction(lstEmails);
                UpdateProc(lstEmails);
                SendScheduledEmail(lstEmails);
                GetScheduledMails();
            }
            catch (Exception e)
            {
                Log.Error(e);
                //return Request.CreateResponse(HttpStatusCode.OK, lstEmails);
            }
        }

        private void GetSenderEID(List<Comn_email> emailList)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSenderEID(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "SenderEID", "SenderEID"));

            string senderId = string.Empty;
            Comn_email mail = new Comn_email();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "G")
                    });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            sims_smtp_address = dr["sims_smtp_address"].ToString();
                            sims_smtp_port = dr["sims_smtp_port"].ToString();
                            sims_smtp_password = dr["sims_smtp_password"].ToString();
                            sims_sslReq = dr["sims_smtp_ssl_required"].ToString();
                            sims_senderid = dr["sims_smtp_from_email"].ToString();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                Log.Error(e);
            }

        }

        private void InsertEmailTransaction(List<Comn_email> emailList)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSenderEID(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "SenderEID", "SenderEID"));

            string senderId = string.Empty;
            string attachment = string.Empty;
            Comn_email_smtp smtp = new Comn_email_smtp();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Comn_email email in emailList)
                    {
                        try
                        {
                            string attachments = string.Empty;
                            foreach (comn_email_attachments att in emailList[0].comn_email_attachments)
                            {
                                attachments = attachments + ',' + att.attFilename;
                            }

                            if (!string.IsNullOrEmpty(attachments))
                                attachment = attachments.Substring(1);
                            else
                                attachment = email.sims_email_attachment;

                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", "B"),
                                new SqlParameter("@sims_user_name", email.username),
                                new SqlParameter("@email_number", email.emailnumber),
                                new SqlParameter("@sims_enroll_number", email.enrollnumber),
                                new SqlParameter("@sims_email_id", email.emp_emailid),
                                new SqlParameter("@sims_sender_email_id", sims_senderid),
                                new SqlParameter("@sims_email_father_flag", email.fflag),
                                new SqlParameter("@sims_email_father_email_id1", email.sims_recepient_id),
                                new SqlParameter("@sims_email_father_email_id11", ""),
                                new SqlParameter("@sims_email_father_email_id111", ""),
                                new SqlParameter("@sims_email_mother_flag", email.mflag),
                                new SqlParameter("@sims_email_mother_email_id2", email.mother_emailid),
                                new SqlParameter("@sims_email_mother_email_id22", ""),
                                new SqlParameter("@sims_email_mother_email_id222", ""),
                                new SqlParameter("@sims_email_guardian_flag", email.gflag),
                                new SqlParameter("@sims_email_guardian_email_id3", email.guardian_emailid),
                                new SqlParameter("@sims_email_guardian_email_id33", ""),
                                new SqlParameter("@sims_email_guardian_email_id333", ""),
                                new SqlParameter("@sims_email_date", System.DateTime.Now),
                                new SqlParameter("@sims_email_error_code", email.errorcode),
                                new SqlParameter("@sims_email_status", "U"),
                                new SqlParameter("@sims_recepient_id", email.sims_recepient_id),
                                new SqlParameter("@sims_recepient_bcc_id", email.sims_recepient_bcc_id),
                                new SqlParameter("@sims_recepient_cc_id", email.sims_recepient_cc_id),
                                new SqlParameter("@sims_recepient_search_id", email.sims_recepient_search_id),
                                new SqlParameter("@sims_email_subject", email.subject),
                                new SqlParameter("@sims_email_attachment", attachment),
                                new SqlParameter("@sims_email_schedule_date", System.DateTime.Now),
                                new SqlParameter("@sims_email_recurrance_id", email.sims_email_recurrance_id),
                                new SqlParameter("@sims_email_message", email.sims_email_message)

                            });
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private void UpdateProc(List<Comn_email> emailList)
        {
            try
            {
                foreach (Comn_email email in emailList)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.Insert_Sims_Email_Schedule_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@senderid",sims_senderid),
                            new SqlParameter("@email_number", email.emailnumber),
                            new SqlParameter("@updCnt", emailList.Count)
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            // message.strMessage = "Fee Category Information Added Sucessfully";
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private int SendScheduledEmail(List<Comn_email> emailList)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSenderEID(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "SenderEID", "SenderEID"));

            string senderId = string.Empty;
            int result = 0;
            string emailStatus = string.Empty;
            Comn_email_smtp comn_smtp = new Comn_email_smtp();
            Comn_email email = new Comn_email();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    string attachments = string.Empty;
                    try
                    {

                        #region SEND EMAIL CODE

                        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                        mail.IsBodyHtml = true;
                        mail.BodyEncoding = Encoding.UTF8;
                        mail.SubjectEncoding = Encoding.UTF8;
                        List<comn_email_attachments> lstAtt = new List<comn_email_attachments>();
                        foreach (Comn_email obj in emailList)
                        {
                            obj.sender_emailid = sims_senderid;
                            if (obj.sender_emailid != "")
                            {
                                // mail.Bcc.Add(obj.emailsendto);
                                mail.To.Add(obj.emailsendto);
                                mail.Subject = obj.subject;
                                mail.Body = obj.body;

                                //  string st = AppDomain.CurrentDomain.BaseDirectory;
                                string path = "~/Content/";

                                string st = HttpContext.Current.Server.MapPath(path);

                                foreach (comn_email_attachments a_str in emailList[0].comn_email_attachments)
                                {
                                    string str = st + "Docs\\Attachments\\" + a_str.attFilename;
                                    Attachment att = new Attachment(str);
                                    mail.Attachments.Add(att);
                                }
                            }
                        }


                        //Get School Name
                        string Schoolname = "Mograsys Support";
                        Schoolname = GetSchoolName();
                        if (string.IsNullOrEmpty(Schoolname))
                            Schoolname = "Mograsys Support";

                        mail.From = new MailAddress(sims_senderid, Schoolname, System.Text.Encoding.UTF8);
                        //"This is a test mail for Mail Integration Service of Mograsys.Thank You.";
                        mail.Priority = MailPriority.High;


                        //Attachment att = new Attachment("D:\\Alerts_Commit.txt");

                        //mail.Attachments.Add(att);
                        SmtpClient client = new SmtpClient();
                        // client.UseDefaultCredentials = false;
                        //  client.Credentials = new System.Net.NetworkCredential(sims_senderid.ToString(), sims_smtp_password, "login.microsoftonline.com");
                        client.Credentials = new System.Net.NetworkCredential(sims_senderid.ToString(), sims_smtp_password);
                        client.Port = Convert.ToInt32(sims_smtp_port);//587;
                        client.Host = sims_smtp_address;//"smtp.gmail.com";
                        if (client.Host == "smtp.office365.com")
                            client.TargetName = "STARTTLS/smtp.office365.com";
                        if (!string.IsNullOrEmpty(sims_sslReq))
                        {
                            if (sims_sslReq == "Y" || sims_sslReq == "y")
                                client.EnableSsl = true;
                            else if (sims_sslReq == "N" || sims_sslReq == "n")
                                client.EnableSsl = false;
                            else
                                client.EnableSsl = true;
                        }

                        client.Timeout = 300000;

                        try
                        {
                            client.Send(mail);
                            Thread.Sleep(5000);
                            emailStatus = "S";
                        }
                        catch (Exception ex)
                        {
                            Exception ex2 = ex;

                            string errorMessage = string.Empty;
                            while (ex2 != null)
                            {
                                errorMessage += ex2.ToString();
                                ex2 = ex2.InnerException;
                            }
                            emailStatus = "F";
                            mail.Dispose();
                            client.Dispose();
                        }

                        mail.Dispose();
                        client.Dispose();

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }

                }
                result = 0;
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return result;
            }
        }

        private string GetSchoolName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDetails(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "MAIL", "GetSchoolName"));
            string name = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_send_mail]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'M')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            name = dr["lic_school_name"].ToString();
                        }
                    }

                    return name;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return name;
            }
        }

        [Route("SendSMS")]
        public HttpResponseMessage SendSMS(List<Comn_SMS> obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ScheduleMails()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Mails/", "ScheduleMails"));
            string attachment = string.Empty;
            Comn_SMS comnObj = new Comn_SMS();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Insert_sims_sms_schedule_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comnObj = new Comn_SMS();
                            comnObj.sms_httpstring = dr["sims_sms_http_string"].ToString();
                            comnObj.user_name = dr["sims_sms_user"].ToString();
                            comnObj.sms_password = dr["sims_sms_password"].ToString();
                            comnObj.sms_sender = dr["sims_sms_sender_id"].ToString();
                        }
                    }

                    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(comnObj.sms_httpstring + "username=" + comnObj.user_name + "&pin=" + comnObj.sms_password + "&message=" + comnObj.sms_smstext + "&mnumber=" + comnObj.sms_smssendto + "&signature=" + comnObj.sms_sender + "");
                    //string mno = "+971552314490,+971507014581,+971507014726,+971507014692";
                    //string mno = "+971552314490,+971507014581";
                    //HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(sms_httpstring + "username=" + sms_username + "&pin=" + sms_password + "&message=" + smstext + "&mnumber=" + mno + "&signature=" + sms_sender + "&msgType=UC");
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    string responseString = respStreamReader.ReadToEnd();
                    respStreamReader.Close();

                    List<string> sms_num = new List<string>();
                    sms_num = comnObj.sms_smssendto.Split(',').ToList();
                    int i = 0;
                    //Insert SMS Transactions into database.
                    //string smsnum = Insert_SMS_Schedule("admin", sms_sender, smssendto,"", smstext,DateTime.Now.ToString(), "", "S");
                    foreach (Comn_SMS sms in obj)
                    {
                        string smsnum = Insert_SMS_Schedule1("admin", sms.sms_sender, sms.sms_smssendto, "", sms.sims_smstext, DateTime.Now.ToString(), "", "S");
                        Insert_SMS_Transaction(sms, smsnum);
                        i++;
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Adding Schedule Sms Information";
                message.systemMessage = x.Message;
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
        }

        private bool Insert_SMS_Transaction(Comn_SMS sms, string smsnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_SMS_Transaction(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Insert_SMS_Transaction", "Insert_SMS_Transaction"));

            bool inserted = false;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    try
                    {
                        #region
                        int ins = db.ExecuteStoreProcedureforInsert("Insert_sims_sms_transaction",
                           new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "I"),
                       new SqlParameter("@sims_sms_number ", smsnum),
                       new SqlParameter("@sims_user_name ", sms.user_name),
                       new SqlParameter("@sims_enroll_number", sms.enroll_number),
                       new SqlParameter("@sims_sms_contact_number", sms.sms_contact_number),
                       new SqlParameter("@sims_sms_father_flag", sms.sms_father_flag),
                       new SqlParameter("@sims_sms_father_contact_number1", sms.sms_father_contact_number1),
                       new SqlParameter("@sims_sms_father_contact_number11", ""),
                       new SqlParameter("@sims_sms_father_contact_number111", ""),
                       new SqlParameter("@sims_sms_mother_flag", sms.sms_mother_flag),
                       new SqlParameter("@sims_sms_mother_contact_number2", sms.sms_mother_contact_number2),
                       new SqlParameter("@sims_sms_mother_contact_number22", ""),
                       new SqlParameter("@sims_sms_mother_contact_number222", ""),
                       new SqlParameter("@sims_sms_guardian_flag", sms.sms_guardian_flag),
                       new SqlParameter("@sims_sms_guardian_contact_number3", sms.sms_guardian_contact_number3),
                       new SqlParameter("@sims_sms_guardian_contact_number33", ""),
                       new SqlParameter("@sims_sms_guardian_contact_number333", ""),
                       new SqlParameter("@sims_sms_emergency_flag", sms.sms_emergency_flag),
                       new SqlParameter("@sims_sms_emergency_contact_number4", sms.sms_emergency_contact_number4),
                       new SqlParameter("@sims_sms_emergency_contact_number44", sms.sms_emergency_contact_number44),
                       new SqlParameter("@sims_sms_emergency_contact_number444", ""),
                       new SqlParameter("@sims_sms_date", System.DateTime.Now),
                       new SqlParameter("@sims_sms_error_code", sms.sms_error_code),
                       new SqlParameter("@sims_sms_status", "S"),
                    });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {
                        Log.Error(e);
                    }
                }
                return inserted;
            }
            catch (Exception t)
            {
                Log.Error(t);
                return inserted;
            }
        }

        private string Insert_SMS_Schedule1(string usercode, string senderid, string recepient_id, string recepient_searchid, object sms_message, string scheduledate, string balance, string status)
        {
            bool inserted = false;
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ScheduleMails()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Mails/", "ScheduleMails"));
            string sms_number = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    try
                    {
                        #region
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Insert_sims_sms_schedule_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_sms_user_code ", usercode),
                            new SqlParameter("@sims_sender_id", senderid),
                            new SqlParameter("@sims_recepient_id", recepient_id),
                            new SqlParameter("@sims_recepient_search_id", recepient_searchid),
                            new SqlParameter("@sims_sms_message", sms_message),
                            new SqlParameter("@sims_sms_schedule_date", DateTime.Parse(scheduledate)),
                            new SqlParameter("@sims_sms_recurrance_id", ""),
                            new SqlParameter("@sims_sms_balance", balance),
                            new SqlParameter("@sims_sms_status", status)
                        });
                        if (dr.HasRows)
                        {
                            dr.Read();
                            sms_number = dr["SMS_Number"].ToString();
                            dr.Close();
                        }
                        return sms_number;
                        #endregion
                    }
                    catch (Exception e)
                    {
                        Log.Error(e);
                        return sms_number;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return sms_number;
            }
        }

        [Route("CUD_Insert_SMS_Schedule")]
        public HttpResponseMessage CUD_Insert_SMS_Schedule(List<Comn_SMS> obj)
        {
            bool inserted = false;
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ScheduleMails()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/Mails/", "ScheduleMails"));

            try
            {
                if (obj != null)
                {
                    string recurid = "0";
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        try
                        {
                            foreach (Comn_SMS sms in obj)
                            {
                                if (sms.arabicsms == true)
                                {

                                    if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "deps" || HttpContext.Current.Request.Headers["schoolId"].ToString() == "portal")
                                    {
                                        recurid = "1";
                                        //StringBuilder s = new StringBuilder();
                                        //foreach (char chr in sms.sims_smstext)
                                        //{
                                        //    s.Append(Convert.ToString(chr, 16).PadLeft(4, '0'));
                                        //}
                                        //sms.sims_smstext = s.ToString();
                                    }
                                    else {
                                        recurid = "1";
                                    }
                                }
                                #region
                                int ins = db.ExecuteStoreProcedureforInsert("[sims].[Insert_sims_sms_schedule_proc]",
                                new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", "I"),
                                    new SqlParameter("@sims_sms_user_code ", sms.usercode),
                                    new SqlParameter("@sims_sender_id", ""),
                                    new SqlParameter("@sims_recepient_id", sms.smssendto),
                                    new SqlParameter("@sims_recepient_cc_id", sms.sims_recepient_cc_id),
                                    new SqlParameter("@sims_recepient_search_id", sms.sims_recepient_search_id),
                                    new SqlParameter("@sims_sms_message", sms.sims_smstext),
                                    new SqlParameter("@sims_sms_schedule_date", DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                    new SqlParameter("@sims_sms_recurrance_id", recurid),
                                    new SqlParameter("@sims_sms_balance", ""),
                                    new SqlParameter("@sims_sms_status", "S")
                                });
                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                                #endregion
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error(e);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }

 
        [Route("Get_Grade_CodebyCuriculum")]
        public HttpResponseMessage Get_Grade_CodebyCuriculum(string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Grade_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Grade_Section_Code"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Search",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_mem_code", "L"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@academic_year", acad_yr)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


    }
}