﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/EmailDetails")]
    public class EmailDetailsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getEmailDetailsDatewise")]
        public HttpResponseMessage getEmailDetailsDatewise(string fromdate,string todate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmailDetailsDatewise(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<emaildetails> lst_emaildet = new List<emaildetails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                          
                            new SqlParameter("@fromdate", db.DBYYYYMMDDformat( fromdate)),                          
                            new SqlParameter("@todate", db.DBYYYYMMDDformat(todate)),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            emaildetails simsobj = new emaildetails();
                            simsobj.e_date = db.UIDDMMYYYYformat( dr["emaildate"].ToString());
                            simsobj.e_details = getEmailSubjects(simsobj.e_date);
                            lst_emaildet.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
        }


        public List<lst_emails> getEmailSubjects(string edate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmailSubjects(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<lst_emails> lst_emaildets = new List<lst_emails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),                          
                            new SqlParameter("@fromdate", db.DBYYYYMMDDformat( edate)),                         
                                                    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lst_emails simsobj = new lst_emails();
                            simsobj.email_subject = dr["sims_email_subject"].ToString();
                            simsobj.email_count = dr["emailcnt"].ToString();
                            lst_emaildets.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return lst_emaildets;
        }


        [Route("getEmailDetailsbody_ids")]
        public HttpResponseMessage getEmailDetailsbody_ids(string edate, string esubject)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmailDetailsbody_ids(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<emaildetails> lst_emaildet = new List<emaildetails>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),                          
                            new SqlParameter("@fromdate", db.DBYYYYMMDDformat(edate)),                          
                            new SqlParameter("@esubject", esubject),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            emaildetails simsobj = new emaildetails();
                            simsobj.email_body = dr["sims_email_message"].ToString();
                            simsobj.email_id = dr["sims_recepient_id"].ToString();
                            simsobj.status = dr["sims_email_status"].ToString();
                            lst_emaildet.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_emaildet);
        }


    }
}
