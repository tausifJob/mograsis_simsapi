﻿using log4net;
using System;
using System.Collections.Generic;
using SIMSAPI.Models.SIMS.simsClass;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Net;

namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/SmsApproval")]
    public class SmsApprovalController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getSmsApprovalDetails")]
        public HttpResponseMessage getSmsApprovalDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSmsDetailsDatewise(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<smsApproval> lst_smsdet = new List<smsApproval>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sms_approve_proc]",
                        new List<SqlParameter>() 
                       { 
                          new SqlParameter("@opr", 'S'),                          
                          //new SqlParameter("@fromdate", db.DBYYYYMMDDformat( fromdate)),                          
                         // new SqlParameter("@todate", db.DBYYYYMMDDformat(todate)),                          
                       });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            smsApproval simsobj = new smsApproval();
                            simsobj.sims_sms_number = dr["sims_sms_number"].ToString();


                            simsobj.receivesms = dr["sims_recepient_id"].ToString();
                            simsobj.subject = dr["sims_sms_subject"].ToString();
                            simsobj.message = dr["sims_sms_message"].ToString();
                            simsobj.smsDate = dr["sims_sms_date"].ToString();
                            lst_smsdet.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_smsdet);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_smsdet);
        }

        [Route("Get_search_sms_deatils")]
        public HttpResponseMessage Get_search_email_deatils(string startDate, string endDate, string sms, string subject)
        {
            List<smsApproval> search_list = new List<smsApproval>();

            if (startDate == "undefined")
                startDate = null;
            if (endDate == "undefined")
                endDate = null;
            if (sms == "undefined")
                sms = null;
            if (subject == "undefined")
                subject = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sms_approve_proc]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", 's'),
                new SqlParameter("@sims_recepient_id", sms),
                new SqlParameter("@sims_sms_subject", subject),
                new SqlParameter("@from_date", db.DBYYYYMMDDformat(startDate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(endDate)),

                //new SqlParameter("@from_date", DateTime.Parse(sdate).Year + "-" + DateTime.Parse(sdate).Month + "-" + DateTime.Parse(sdate).Day),
                //new SqlParameter("@to_date", DateTime.Parse(edate).Year + "-" + DateTime.Parse(edate).Month + "-" + DateTime.Parse(edate).Day),
                         }); if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            smsApproval simsobj = new smsApproval();
                            simsobj.sims_sms_number = dr["sims_sms_number"].ToString();


                            simsobj.receivesms = dr["sims_recepient_id"].ToString();
                            simsobj.subject = dr["sims_sms_subject"].ToString();
                            simsobj.message = dr["sims_sms_message"].ToString();
                            simsobj.smsDate = dr["sims_sms_date"].ToString();
                            search_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, search_list);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, search_list);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }


        [Route("approve_sms")]
        public HttpResponseMessage approve_sms(string lst)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sms_approve_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_sms_number", lst),
                                
                     });
                    if (ins > 0)
                    {
                        insert = true;
                    }


                    else
                    {
                        insert = false;
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("reject_sms")]
        public HttpResponseMessage reject_sms(string lst)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sms_approve_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@sims_sms_number", lst),
                                
                     });
                    if (ins > 0)
                    {
                        insert = true;
                    }


                    else
                    {
                        insert = false;
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


    }
}