﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/QueryReportController")]
    public class QueryReportController : ApiController
    {

        //Home room Report
        [Route("getQueryReport")]
        public HttpResponseMessage getHomeRoomReport(string cur_code, string acad_year, string grade_code, string section_code,string from_date,string to_date)
        {
            List<CQRQC> lstCuriculum = new List<CQRQC>();
            if (from_date == "undefined" || from_date == "")
            {
                from_date = null;
            }
            if (to_date == "undefined" || to_date == "")
            {
                to_date = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Communication_Query_report_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Z"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", acad_year),
                             new SqlParameter("@grade_code", grade_code),
                              new SqlParameter ("@section_code",section_code),
                             new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CQRQC sequence = new CQRQC();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();

                            sequence.sims_comm_status = dr["sims_comm_status"].ToString();

                            sequence.sims_comm_number = dr["sims_comm_number"].ToString();
                            sequence.sims_comm_tran_number = dr["sims_comm_tran_number"].ToString();
                            sequence.sims_comm_date = dr["sims_comm_date"].ToString();

                            sequence.sims_comm_tran_date = dr["sims_comm_tran_date"].ToString();
                            sequence.sims_comm_category = dr["sims_comm_category"].ToString();

                            sequence.sims_comm_user_code = dr["sims_comm_user_code"].ToString();
                            sequence.sims_subject_id = dr["sims_subject_id"].ToString();
                            sequence.sims_comm_subject = dr["sims_comm_subject"].ToString();

                            sequence.sender_id = dr["sender_id"].ToString();
                            sequence.rece_id = dr["rece_id"].ToString();
                            sequence.sims_comm_message = dr["sims_comm_message"].ToString();

                            sequence.Initial_sender_name = dr["Initial_sender_name"].ToString();
                            sequence.Initial_receiver_name = dr["Initial_receiver_name"].ToString();
                            sequence.sender_name = dr["sender_name"].ToString();

                            sequence.receiver_name = dr["receiver_name"].ToString();
                            sequence.initial_send = dr["initial_send"].ToString();
                            sequence.initial_receive = dr["initial_receive"].ToString();

                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_father_email = dr["sims_parent_father_email"].ToString();

                            lstCuriculum.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

    }
}