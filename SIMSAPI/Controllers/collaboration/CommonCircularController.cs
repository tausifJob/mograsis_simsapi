﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.COLLABORATION;
using SIMSAPI.Models.ParentClass;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.collaboration
{
     [RoutePrefix("api/commoncircular")]
    public class CommonCircularController : ApiController
    {
        // GET: CommonCircular
         [Route("GetCircularstatusbyparent")]
         public HttpResponseMessage GetCircularstatusbyparent()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_common_parameter",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("Getgroupcode")]
         public HttpResponseMessage Getgroupcode()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_common_parameter",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                             simsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("Getcomstatus")]
         public HttpResponseMessage Getcomstatus()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_common_parameter",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("Getemailtransactionstatus")]
         public HttpResponseMessage Getemailtransactionstatus()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_common_parameter",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'D'),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("GetViewasreport")]
         public HttpResponseMessage GetViewasreport()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_common_parameter",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }

         [Route("Gettransportstatus")]
         public HttpResponseMessage Gettransportstatus(string opr)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<circularByParent> doc_list = new List<circularByParent>();

             Message message = new Message();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_common_fleet_parameter]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", opr),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             circularByParent simsobj = new circularByParent();
                             simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             doc_list.Add(simsobj);

                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }
    }
}