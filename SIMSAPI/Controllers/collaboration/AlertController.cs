﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Alert")]
    public class AlertController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getModule")]
        public HttpResponseMessage getModule(string username)
        {

            List<alertclass> lst_module = new List<alertclass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'M'),                          
                            new SqlParameter("@userName", username),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alertclass simsobj = new alertclass();
                            simsobj.module_code = dr["comn_mod_code"].ToString();
                            simsobj.module_name = dr["comn_mod_name_en"].ToString();

                            lst_module.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_module);

        }

        [Route("getAlertStatus")]
        public HttpResponseMessage getAlertStatus()
        {
            List<alertclass> lst_module = new List<alertclass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'R'),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alertclass simsobj = new alertclass();
                            simsobj.alert_status = dr["sims_appl_parameter"].ToString();
                            simsobj.alert_status_name = dr["sims_appl_form_field_value1"].ToString();

                            lst_module.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_module);

        }
        
        [Route("UpdateAlertUnAsRead")]
        public HttpResponseMessage UpdateAlertUnAsRead(string alert_number)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),                                
                                new SqlParameter("@comn_alert_number", alert_number),
                               
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("AlertDetails")]
        public HttpResponseMessage AlertDetails(alertclass obj)
        {
            List<alertclass> lst_alert = new List<alertclass>();
            try
            {
                string ch;
                if (obj.fromdate == "" || obj.fromdate == "undefined" || obj.fromdate==null)
                {
                    obj.fromdate = null;
                    ch = "S";
                }
                else
                {
                    ch = "SS";
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", ch),                          
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(obj.fromdate)),                          
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(obj.todate)),                          
                            new SqlParameter("@userName", obj.usercode),                          
                            new SqlParameter("@comn_alert_status", obj.alert_status),                          
                            new SqlParameter("@comn_alert_mod_code", obj.module_code),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alertclass simsobj = new alertclass();
                            simsobj.module_name = dr["comn_mod_name_en"].ToString();
                            simsobj.alert_date = dr["comn_alert_date"].ToString();
                            simsobj.alert_msg = dr["comn_alert_message"].ToString();
                            simsobj.alert_status = dr["comn_alert_status"].ToString();
                            simsobj.alert_status_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.usercode = dr["comn_alert_user_code"].ToString();
                            simsobj.alert_number= dr["comn_alert_number"].ToString();
                            simsobj.module_code = dr["comn_alert_mod_code"].ToString();
                            try
                            {
                                simsobj.comn_alert_appl_code = dr["comn_alert_appl_code"].ToString();
                            }
                            catch (Exception ex) { }

                            
                            lst_alert.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_alert);

        }

        [Route("getUserCount")]
        public HttpResponseMessage getUserCount(string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserCount(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAlertType"));

            List<alertclass> goaltarget_list = new List<alertclass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_alert_transaction_view_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'V'),
                            new SqlParameter("@userName",user),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alertclass obj = new alertclass();
                            obj.unread_status = dr["unread_status"].ToString();


                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAlertBynumber")]
        public HttpResponseMessage getAlertBynumber(string alertnum)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlertType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAlertType"));
            alertclass obj = new alertclass();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'F'),
                            new SqlParameter("@comn_alert_number",alertnum),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            obj.alert_date = dr["comn_alert_date"].ToString();
                            obj.module_name = dr["comn_mod_name_en"].ToString();
                            obj.alert_msg = dr["comn_alert_message"].ToString();
                            obj.alert_number = dr["comn_alert_number"].ToString();
                            obj.alert_status = dr["comn_alert_status"].ToString();

                            try{
                                obj.comn_alert_appl_code = dr["comn_alert_appl_code"].ToString();
                               }catch(Exception ex){}
                        }
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        [Route("getTop10Alert")]
        public HttpResponseMessage getTop10Alert(string username)
        {
            List<alertclass> lst_alert = new List<alertclass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),                          
                            new SqlParameter("@userName", username),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            alertclass simsobj = new alertclass();
                            simsobj.module_name = dr["comn_mod_name_en"].ToString();
                            simsobj.alert_date = dr["comn_alert_date"].ToString();
                            simsobj.alert_msg = dr["comn_alert_message"].ToString();
                            simsobj.alert_status = dr["comn_alert_status"].ToString();
                            simsobj.alert_status_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.usercode = dr["comn_alert_user_code"].ToString();
                            simsobj.alert_number = dr["comn_alert_number"].ToString();
                            simsobj.module_code = dr["comn_alert_mod_code"].ToString();
                            lst_alert.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst_alert);
        }


        [Route("UAlertStatus")]
        public HttpResponseMessage UAlertStatus(List<alertclass> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (alertclass simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@comn_alert_number",simsobj.alertno),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("UAlertAllStatus")]
        public HttpResponseMessage UAlertAllStatus(List<alertclass> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (alertclass simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("comn.comn_alert_transaction_view_proc",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@comn_alert_number",simsobj.user),

                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}
