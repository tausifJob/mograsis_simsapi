﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;
using System.Configuration;
using System.Data;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Circular")]
    public class Comn017Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCircularNumber")]
        public HttpResponseMessage getCircularNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularNumber(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circularnum = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'Q'),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_circular_number = dr["maxnum"].ToString();
                            //em_number = dr["maxnum"].ToString();
                            lst_circularnum.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_circularnum);
        }

        [Route("CCirculars")]
        public HttpResponseMessage CCirculars(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : CCirculars(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_circular_ifalert;
            if (simsobj.sims_circular_desg == "undefined")
                simsobj.sims_circular_desg = "";
            if (simsobj.sims_circular_dept == "undefined")
                simsobj.sims_circular_dept = "";
            if (simsobj.sims_circular_gradesections == "undefined")
                simsobj.sims_circular_gradesections = "";
            // simsobj.sims_circular_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        string edate = "";
                        //if (string.IsNullOrEmpty(simsobj.sims_circular_expiry_date))
                        //{
                        //    edate = null;
                        //}
                        //else
                        //    edate = db.DBYYYYMMDDformat(simsobj.sims_circular_expiry_date);

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year_desc),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_circular_publish_date", !string.IsNullOrEmpty(simsobj.sims_circular_publish_date)?db.DBYYYYMMDDformat(simsobj.sims_circular_publish_date):null),
                                new SqlParameter("@sims_circular_expiry_date",!string.IsNullOrEmpty(simsobj.sims_circular_expiry_date)?db.DBYYYYMMDDformat(simsobj.sims_circular_expiry_date):null),
                                //new SqlParameter("@sims_circular_publish_date", db.DBYYYYMMDDformat(simsobj.sims_circular_publish_date)),
                                //new SqlParameter("@sims_circular_expiry_date",edate),

                                new SqlParameter("@sims_circular_title", simsobj.sims_circular_title),
                                new SqlParameter("@sims_circular_short_desc", simsobj.sims_circular_short_desc),
                                new SqlParameter("@sims_circular_desc", simsobj.sims_circular_desc),
                                new SqlParameter("@sims_circular_file_path1", simsobj.sims_circular_file_path1),
                                new SqlParameter("@sims_circular_file_path2", simsobj.sims_circular_file_path2),
                                new SqlParameter("@sims_circular_file_path3", simsobj.sims_circular_file_path3),
                                new SqlParameter("@sims_circular_file_path1_en", simsobj.sims_circular_file_path1_en),
                                new SqlParameter("@sims_circular_file_path2_en", simsobj.sims_circular_file_path2_en),
                                new SqlParameter("@sims_circular_file_path3_en", simsobj.sims_circular_file_path3_en),
                                new SqlParameter("@sims_circular_type", simsobj.sims_circular_type),
                                new SqlParameter("@sims_circular_category", simsobj.sims_circular_category),
                                new SqlParameter("@sims_circular_created_user_code", simsobj.sims_circular_created_user),
                                new SqlParameter("@sims_circular_display_order", simsobj.sims_display_order),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com017"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "Circular Type"),
                                new SqlParameter("@appl_field_category", "Circular Category"),
                                new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                new SqlParameter("@desg", simsobj.sims_circular_desg),
                                new SqlParameter("@dept", simsobj.sims_circular_dept)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Inserted.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        if (inserted)
                        {
                            //Code for multiple usergroups.

                            foreach (string str in simsobj.lst_groups)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    if (simsobj.sims_circular_desg == "undefined")
                                        simsobj.sims_circular_desg = "";
                                    if (simsobj.sims_circular_dept == "undefined")
                                        simsobj.sims_circular_dept = "";

                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_circular_view_rights_proc", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                        new SqlParameter("@sims_circular_user_group_code", str),
                                        new SqlParameter("@sims_circular_ifalert", ifalert),
                                        new SqlParameter("@sims_circular_status", simsobj.sims_circular_status == true?"A":"I"),
                                        new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                        new SqlParameter("@desg", simsobj.sims_circular_desg),
                                        new SqlParameter("@dept", simsobj.sims_circular_dept)
                                     });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                }
                            }

                            if (inserted)
                            {
                                message.strMessage = "Circular Added Successfully.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Error In Parsing Information.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Inserting Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getCircularType")]
        public HttpResponseMessage getCircularType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularType(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circularType = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_circular_type = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_circular_type_code = dr["sims_appl_parameter"].ToString();
                            lst_circularType.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circularType);
        }

        [Route("getCircularCategory")]
        public HttpResponseMessage getCircularCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularCategory(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circularcat = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_circular_category = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_circular_cat_code = dr["sims_appl_parameter"].ToString();
                            lst_circularcat.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circularcat);
        }

        [Route("getCircular")]
        public HttpResponseMessage getCircular(string loggeduser)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircular(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circulars = new List<Comn015>();
            bool isenable = GetCircular_RolePer(loggeduser);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com017"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "Circular Type"),
                                new SqlParameter("@appl_field_category", "Circular Category"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_circular_date"].ToString()))
                                obj.sims_circular_date = null;
                            else
                            // obj.sims_circular_date = dr["sims_circular_date"].ToString();
                            {
                                string dt_cdate = db.UIDDMMYYYYformat(dr["sims_circular_date"].ToString());
                                obj.sims_circular_date = dt_cdate;
                            }


                            bool p = obj.publish_status;

                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()))
                            {
                                obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                                obj.publish_status = true;
                            }
                            else
                            {
                                obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                                obj.publish_status = false;
                            }

                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "Y";
                            if ((Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) < DateTime.Now.Date))
                                obj.sims_circular_pstatus = "E";

                            if (string.IsNullOrEmpty(dr["sims_circular_expiry_date"].ToString()))
                                obj.sims_circular_expiry_date = null;
                            else
                                obj.sims_circular_expiry_date = db.UIDDMMYYYYformat(dr["sims_circular_expiry_date"].ToString());

                            obj.sims_circular_title = dr["sims_circular_title"].ToString();
                            obj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            obj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            obj.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                            obj.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                            obj.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                            obj.sims_circular_file_path1_en = dr["sims_circular_file_path1_en"].ToString();
                            obj.sims_circular_file_path2_en = dr["sims_circular_file_path2_en"].ToString();
                            obj.sims_circular_file_path3_en = dr["sims_circular_file_path3_en"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_circular_file_path1_en))
                                obj.sims_circular_file_path1_en = obj.sims_circular_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path2_en))
                                obj.sims_circular_file_path2_en = obj.sims_circular_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path3_en))
                                obj.sims_circular_file_path3_en = obj.sims_circular_file_path3;

                            obj.sims_circular_type = dr["news_type"].ToString();
                            obj.sims_circular_category = dr["news_category"].ToString();
                            obj.sims_circular_created_user = dr["user_name1"].ToString();
                            obj.sims_display_order = dr["sims_circular_display_order"].ToString();
                            obj.sims_circular_ifalert = "Y";

                            try
                            {
                                obj.sims_circular_alert_status = dr["sims_circular_alert_status"].ToString();
                                obj.sims_circular_email_status = dr["sims_circular_email_status"].ToString();


                            }
                            catch (Exception ex) { }

                            obj.sims_circular_gradesections = string.Empty;
                            obj.sims_circular_grade = string.Empty;
                            obj.sims_circular_desg = string.Empty;
                            obj.sims_circular_dept = string.Empty;
                            obj.enablecircular = isenable;
                            //obj.enablecircular = true;
                            obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                            obj.sims_circular_expiry_date = db.UIDDMMYYYYformat(dr["sims_circular_expiry_date"].ToString());

                            #region Circular Status
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_circular_proc",
                                    new List<SqlParameter>() 
                                     { 
                                        new SqlParameter("@opr", "D"),
                                        new SqlParameter("@sims_circular_number", obj.sims_circular_number),
                                     });
                                //string str=null;
                                if (dr1.Read())
                                {
                                    int active_cnt = int.Parse(dr1[0].ToString());
                                    if (active_cnt > 0)
                                        obj.sims_circular_status = true;
                                    else if (active_cnt <= 0)
                                        obj.sims_circular_status = false;
                                }
                                else
                                {
                                    obj.sims_circular_status = false;
                                }
                            #endregion

                                obj.lst_groups = getCircularUserGroup(obj.sims_circular_number);
                                obj.lst_desg = GetDesignationNames(obj.sims_circular_number);
                                obj.lst_dept = GetDepartmentNames(obj.sims_circular_number);
                                obj.lst_grade = GetgradeNames(obj.sims_circular_number);
                                obj.lst_section = GetsectionNames(obj.sims_circular_number);
                                if (obj.sims_circular_status == false)
                                    obj.sims_circular_pstatus = "Y";
                                lst_circulars.Add(obj);

                            }
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
        }

        [Route("getCircularbysdate")]
        public HttpResponseMessage getCircularbysdate(string loggeduser, string sfromdate, string stodate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularbysdate(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circulars = new List<Comn015>();
            bool isenable = GetCircular_RolePer(loggeduser);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A3"),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com017"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "Circular Type"),
                                new SqlParameter("@appl_field_category", "Circular Category"),
                                new SqlParameter("@sfromdate",db.DBYYYYMMDDformat(sfromdate)),
                                new SqlParameter("@stodate", db.DBYYYYMMDDformat(stodate)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_circular_date"].ToString()))
                                obj.sims_circular_date = null;
                            else
                            // obj.sims_circular_date = dr["sims_circular_date"].ToString();
                            {
                                string dt_cdate = db.UIDDMMYYYYformat(dr["sims_circular_date"].ToString());
                                obj.sims_circular_date = dt_cdate;
                            }

                            bool p = obj.publish_status;

                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()))
                            {
                                obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                                obj.publish_status = true;
                            }
                            else
                            {
                                obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());

                                obj.publish_status = false;
                            }

                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "Y";
                            if ((Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) < DateTime.Now.Date))
                                obj.sims_circular_pstatus = "E";

                            if (string.IsNullOrEmpty(dr["sims_circular_expiry_date"].ToString()))
                                obj.sims_circular_expiry_date = null;
                            else
                                obj.sims_circular_expiry_date = db.UIDDMMYYYYformat(dr["sims_circular_expiry_date"].ToString());

                            obj.sims_circular_title = dr["sims_circular_title"].ToString();
                            obj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            obj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            obj.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                            obj.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                            obj.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                            obj.sims_circular_file_path1_en = dr["sims_circular_file_path1_en"].ToString();
                            obj.sims_circular_file_path2_en = dr["sims_circular_file_path2_en"].ToString();
                            obj.sims_circular_file_path3_en = dr["sims_circular_file_path3_en"].ToString();

                            try
                            {
                                obj.sims_circular_alert_status = dr["sims_circular_alert_status"].ToString();
                                obj.sims_circular_email_status = dr["sims_circular_email_status"].ToString();


                            }
                            catch (Exception ex) { }

                            if (string.IsNullOrEmpty(obj.sims_circular_file_path1_en))
                                obj.sims_circular_file_path1_en = obj.sims_circular_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path2_en))
                                obj.sims_circular_file_path2_en = obj.sims_circular_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path3_en))
                                obj.sims_circular_file_path3_en = obj.sims_circular_file_path3;

                            obj.sims_circular_type = dr["news_type"].ToString();
                            obj.sims_circular_category = dr["news_category"].ToString();
                            obj.sims_circular_created_user = dr["user_name1"].ToString();
                            obj.sims_display_order = dr["sims_circular_display_order"].ToString();
                            obj.sims_circular_ifalert = "Y";

                            obj.sims_circular_gradesections = string.Empty;
                            obj.sims_circular_grade = string.Empty;
                            obj.sims_circular_desg = string.Empty;
                            obj.sims_circular_dept = string.Empty;
                            obj.enablecircular = isenable;
                            //obj.enablecircular = true;
                            obj.sims_circular_publish_date = db.UIDDMMYYYYformat(dr["sims_circular_publish_date"].ToString());
                            obj.sims_circular_expiry_date = db.UIDDMMYYYYformat(dr["sims_circular_expiry_date"].ToString());

                            #region Circular Status
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_circular_proc",
                                    new List<SqlParameter>() 
                                     { 
                                        new SqlParameter("@opr", "D"),
                                        new SqlParameter("@sims_circular_number", obj.sims_circular_number),
                                     });
                                //string str=null;
                                if (dr1.Read())
                                {
                                    int active_cnt = int.Parse(dr1[0].ToString());
                                    if (active_cnt > 0)
                                        obj.sims_circular_status = true;
                                    else if (active_cnt <= 0)
                                        obj.sims_circular_status = false;
                                }
                                else
                                {
                                    obj.sims_circular_status = false;
                                }
                            #endregion

                                obj.lst_groups = getCircularUserGroup(obj.sims_circular_number);
                                obj.lst_desg = GetDesignationNames(obj.sims_circular_number);
                                obj.lst_dept = GetDepartmentNames(obj.sims_circular_number);
                                obj.lst_grade = GetgradeNames(obj.sims_circular_number);
                                obj.lst_section = GetsectionNames(obj.sims_circular_number);
                                if (obj.sims_circular_status == false)
                                    obj.sims_circular_pstatus = "Y";
                                lst_circulars.Add(obj);

                            }
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
        }

        private bool GetCircular_RolePer(string username)
        {
            string str = string.Empty;
            bool flag = false;
            try
            {
                using (DBConnection db2 = new DBConnection())
                {
                    db2.Open();
                    SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() {
                                new SqlParameter("@opr", "Y"),
                                new SqlParameter("@user_name", username)
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            str = dr2["RolePer"].ToString();
                        }
                    }
                    if (str == "Y")
                        flag = true;
                    return flag;
                }
            }
            catch (Exception e)
            {
                return flag;
            }
        }

        [Route("DCirculars")]
        public HttpResponseMessage DCirculars(Comn015 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : DCirculars()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_view_rights_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "Circular Deleted Successfully.";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                    else
                    {
                        message.strMessage = "Circular Not Deleted.";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getUpdateCircularViewRights")]
        public HttpResponseMessage getUpdateCircularViewRights(Comn015 comnobj, string loggeduser)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUpdateCircularViewRights(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool updated = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_view_rights_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_circular_number", comnobj.sims_circular_number),
                                new SqlParameter("@sims_circular_user_group_code", ""),
                                new SqlParameter("@username", loggeduser),
                                new SqlParameter("@sims_circular_status", comnobj.sims_circular_status == true?"A":"I"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            updated = true;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, updated);
                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("getCircularUserGroup")]
        public HttpResponseMessage getCircularUserGroup(Comn015 comnobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircularUserGroup(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circularusergrps = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@sims_circular_number", comnobj.sims_circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_circular_user_group = dr["comn_user_group_name"].ToString();
                            lst_circularusergrps.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circularusergrps);
        }

        private List<string> getCircularUserGroup(string circular_number)
        {
            List<string> lst_circularusergrps = new List<string>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@sims_circular_number", circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //sims_circular_user_group = dr["comn_user_group_name"].ToString();
                            lst_circularusergrps.Add(dr["comn_user_group_code"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_circularusergrps;
        }

        [Route("getAllCircularUserGroup")]
        public HttpResponseMessage getAllCircularUserGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCircularUserGroup(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circularusergrps = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "H"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_circular_user_group = dr["comn_user_group_name"].ToString();
                            simsobj.sims_circular_user_group_code = dr["comn_user_group_code"].ToString();
                            lst_circularusergrps.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circularusergrps);
        }

        /* still 3 methods are remaining */

        [Route("UCirculars")]
        public HttpResponseMessage UCirculars(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UCirculars(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_circular_ifalert;
            if (simsobj.sims_circular_desg == "undefined")
                simsobj.sims_circular_desg = "";
            if (simsobj.sims_circular_dept == "undefined")
                simsobj.sims_circular_dept = "";
            if (simsobj.sims_circular_gradesections == "undefined")
                simsobj.sims_circular_gradesections = "";
            // simsobj.sims_circular_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),                                
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_circular_publish_date", db.DBYYYYMMDDformat( simsobj.sims_circular_publish_date)),
                                new SqlParameter("@sims_circular_expiry_date",db.DBYYYYMMDDformat( simsobj.sims_circular_expiry_date)),
                                new SqlParameter("@sims_circular_title", simsobj.sims_circular_title),
                                new SqlParameter("@sims_circular_short_desc", simsobj.sims_circular_short_desc),
                                new SqlParameter("@sims_circular_desc", simsobj.sims_circular_desc),
                                new SqlParameter("@sims_circular_file_path1", simsobj.sims_circular_file_path1),
                                new SqlParameter("@sims_circular_file_path2", simsobj.sims_circular_file_path2),
                                new SqlParameter("@sims_circular_file_path3", simsobj.sims_circular_file_path3),
                                new SqlParameter("@sims_circular_file_path1_en", simsobj.sims_circular_file_path1_en),
                                new SqlParameter("@sims_circular_file_path2_en", simsobj.sims_circular_file_path2_en),
                                new SqlParameter("@sims_circular_file_path3_en", simsobj.sims_circular_file_path3_en),
                                new SqlParameter("@sims_circular_type", simsobj.sims_circular_type),
                                new SqlParameter("@sims_circular_category", simsobj.sims_circular_category),
                                new SqlParameter("@sims_circular_created_user_code", simsobj.sims_circular_created_user),
                                new SqlParameter("@sims_circular_display_order", simsobj.sims_display_order),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com017"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "Circular Type"),
                                new SqlParameter("@appl_field_category", "Circular Category"),
                                new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                new SqlParameter("@desg", simsobj.sims_circular_desg),
                                new SqlParameter("@dept", simsobj.sims_circular_dept)
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Updated.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }



                        if (inserted)
                        {
                            //Delete all usercode in sims_circular_view_rights table.
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_circular_view_rights", new List<SqlParameter>() {
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                });

                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }
                            }
                        }
                        if (inserted)
                        {
                            //Code for multiple usergroups.
                            foreach (string str in simsobj.lst_groups)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    if (simsobj.sims_circular_desg == "undefined")
                                        simsobj.sims_circular_desg = "";
                                    if (simsobj.sims_circular_dept == "undefined")
                                        simsobj.sims_circular_dept = "";
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_circular_view_rights_proc", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                        new SqlParameter("@sims_circular_user_group_code", str),
                                        new SqlParameter("@sims_circular_ifalert", ifalert),
                                        new SqlParameter("@sims_circular_status", simsobj.sims_circular_status == true?"A":"I"),
                                        new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                        new SqlParameter("@desg", simsobj.sims_circular_desg),
                                        new SqlParameter("@dept", simsobj.sims_circular_dept)
                                     });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                }
                            }

                            if (inserted)
                            {
                                message.strMessage = "Circular Updated Successfully.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Error In Parsing Information.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UCirculars1")]
        public HttpResponseMessage UCirculars1(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UCirculars(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_circular_ifalert;
            if (simsobj.sims_circular_desg == "undefined")
                simsobj.sims_circular_desg = "";
            if (simsobj.sims_circular_dept == "undefined")
                simsobj.sims_circular_dept = "";
            if (simsobj.sims_circular_gradesections == "undefined")
                simsobj.sims_circular_gradesections = "";
            // simsobj.sims_circular_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U1"),                                
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_circular_publish_date", db.DBYYYYMMDDformat( simsobj.sims_circular_publish_date)),
                                new SqlParameter("@sims_circular_expiry_date",db.DBYYYYMMDDformat( simsobj.sims_circular_expiry_date)),
                                new SqlParameter("@sims_circular_title", simsobj.sims_circular_title),
                                new SqlParameter("@sims_circular_short_desc", simsobj.sims_circular_short_desc),
                                new SqlParameter("@sims_circular_desc", simsobj.sims_circular_desc),
                                new SqlParameter("@sims_circular_file_path1", simsobj.sims_circular_file_path1),
                                new SqlParameter("@sims_circular_file_path2", simsobj.sims_circular_file_path2),
                                new SqlParameter("@sims_circular_file_path3", simsobj.sims_circular_file_path3),
                                new SqlParameter("@sims_circular_file_path1_en", simsobj.sims_circular_file_path1_en),
                                new SqlParameter("@sims_circular_file_path2_en", simsobj.sims_circular_file_path2_en),
                                new SqlParameter("@sims_circular_file_path3_en", simsobj.sims_circular_file_path3_en),
                                new SqlParameter("@sims_circular_type", simsobj.sims_circular_type),
                                new SqlParameter("@sims_circular_category", simsobj.sims_circular_category),
                                new SqlParameter("@sims_circular_created_user_code", simsobj.sims_circular_created_user),
                                new SqlParameter("@sims_circular_display_order", simsobj.sims_display_order),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com017"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "Circular Type"),
                                new SqlParameter("@appl_field_category", "Circular Category"),
                                new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                new SqlParameter("@desg", simsobj.sims_circular_desg),
                                new SqlParameter("@dept", simsobj.sims_circular_dept)
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Updated.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }



                        if (inserted)
                        {
                            //Delete all usercode in sims_circular_view_rights table.
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_circular_view_rights", new List<SqlParameter>() {
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                });

                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }
                            }
                        }
                        if (inserted)
                        {
                            //Code for multiple usergroups.
                            foreach (string str in simsobj.lst_groups)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    if (simsobj.sims_circular_desg == "undefined")
                                        simsobj.sims_circular_desg = "";
                                    if (simsobj.sims_circular_dept == "undefined")
                                        simsobj.sims_circular_dept = "";
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_circular_view_rights_proc", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                                        new SqlParameter("@sims_circular_user_group_code", str),
                                        new SqlParameter("@sims_circular_ifalert", ifalert),
                                        new SqlParameter("@sims_circular_status", simsobj.sims_circular_status == true?"A":"I"),
                                        new SqlParameter("@gradesections", simsobj.sims_circular_gradesections),
                                        new SqlParameter("@desg", simsobj.sims_circular_desg),
                                        new SqlParameter("@dept", simsobj.sims_circular_dept)
                                     });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                }
                            }

                            if (inserted)
                            {
                                message.strMessage = "Circular Updated Successfully.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Error In Parsing Information.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }



        [Route("UpdateCircularPublishDate")]
        public HttpResponseMessage UpdateCircularPublishDate(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateCircularPublishDate(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_circular_ifalert;
            // simsobj.sims_circular_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A4"),                                
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number)                                
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                        if (inserted)
                        {
                            message.strMessage = "The Selected Circular Published Today.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Error In Parsing Information.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UpdateCircularStatus")]
        public HttpResponseMessage UpdateCircularStatus(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateCircularStatus(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_circular_ifalert;
            // simsobj.sims_circular_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_view_rights_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_circular_number", simsobj.sims_circular_number),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Updated.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        if (inserted)
                        {
                            message.strMessage = "Circular Unpublished Successfully.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Error In Parsing Information.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getUserDetails")]
        public HttpResponseMessage getUserDetails(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserDetails(),PARAMETERS :: :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));
            Comn005 UserDet = new Comn005();

            //List<Comn005> lst_UserDet = new List<Comn005>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@comn_user_name", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            UserDet.User_Name = dr["comn_user_name"].ToString();
                            UserDet.User_Code = dr["comn_user_code"].ToString();
                            UserDet.Email_id = dr["Mail_Id"].ToString();
                            UserDet.Expiry_Date = dr["comn_user_expiry_date"].ToString();
                            UserDet.Group_Code = dr["comn_user_group_code"].ToString();
                            UserDet.Last_Login = dr["comn_user_last_login"].ToString();
                            UserDet.user_image = dr["user_image"].ToString();

                            UserDet.User_password = dr["comn_user_password"].ToString();
                            UserDet.UserAlertCount = dr["AlertCount"].ToString();
                            UserDet.UserCircularCount = dr["CircularCount"].ToString();
                            UserDet.UserNewsCount = dr["NewsCount"].ToString();
                            UserDet.messageCount = dr["MessageCount"].ToString();
                            UserDet.Designation = dr["Desig"].ToString();
                            UserDet.isAdmin = dr["ADMINuser"].ToString().Equals("1");
                            string[] str = UserDet.Designation.Split(' ');
                            if (str.Length > 1)
                                UserDet.DesigLtr = str[0][0] + "" + str[1][0];
                            else
                                UserDet.DesigLtr = str[0][0] + "";

                            //lst_UserDet.Add(UserDet);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, UserDet);
        }

        [Route("GetUserCirculars")]
        public HttpResponseMessage GetUserCirculars(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserCirculars(),PARAMETERS :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

            List<Comn015> lst_circulars = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_circular_number", ""),
                                new SqlParameter("@sims_academic_year", ""),
                                new SqlParameter("@sims_cur_code", ""),
                                new SqlParameter("@sims_circular_date", ""),
                                new SqlParameter("@sims_circular_publish_date", ""),
                                new SqlParameter("@sims_circular_expiry_date", ""),
                                new SqlParameter("@sims_circular_title", ""),
                                new SqlParameter("@sims_circular_short_desc", ""),
                                new SqlParameter("@sims_circular_desc", ""),
                                new SqlParameter("@sims_circular_file_path1", ""),
                                new SqlParameter("@sims_circular_file_path2", ""),
                                new SqlParameter("@sims_circular_file_path3", ""),
                                new SqlParameter("@sims_circular_type", ""),
                                new SqlParameter("@sims_circular_category", ""),
                                new SqlParameter("@sims_circular_created_user_code", "01"),
                                new SqlParameter("@username", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj.sims_circular_user_status = dr["UserCStatus"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_circular_date"].ToString()))
                                // obj.sims_circular_date = Convert.ToDateTime(dr["sims_circular_date"].ToString());
                                obj.sims_circular_date = dr["sims_circular_date"].ToString();

                            if (dr["sims_circular_publish_date"].ToString() == "")
                            {
                                obj.sims_circular_publish_date = null;
                                obj.publish_status = true;
                            }
                            else
                            {
                                // obj.sims_circular_publish_date = Convert.ToDateTime(dr["sims_circular_publish_date"].ToString());
                                obj.sims_circular_publish_date = dr["sims_circular_publish_date"].ToString();
                                obj.publish_status = false;
                            }

                            obj.sims_circular_title = dr["sims_circular_title"].ToString();
                            obj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            obj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            obj.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                            obj.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                            obj.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                            obj.sims_circular_file_path1_en = dr["sims_circular_file_path1_en"].ToString();
                            obj.sims_circular_file_path2_en = dr["sims_circular_file_path2_en"].ToString();
                            obj.sims_circular_file_path3_en = dr["sims_circular_file_path3_en"].ToString();
                            obj.sims_circular_category = dr["sims_circular_category"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_circular_file_path1_en))
                                obj.sims_circular_file_path1_en = obj.sims_circular_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path2_en))
                                obj.sims_circular_file_path2_en = obj.sims_circular_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path3_en))
                                obj.sims_circular_file_path3_en = obj.sims_circular_file_path3;

                            if (dr["sims_circular_status"].ToString().Equals("A"))
                                obj.sims_circular_status = true;
                            else
                                obj.sims_circular_status = false;

                            obj.sims_circular_remark = dr["sims_circular_remark"].ToString();

                            lst_circulars.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
        }

        [Route("GetUserCircularsBYDate")]
        public HttpResponseMessage GetUserCircularsBYDate(string username, string fromDate, string toDate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserCircularsBYDate(),PARAMETERS :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

            List<Comn015> lst_circulars = new List<Comn015>();


            if (fromDate == "undefined")
            {
                fromDate = null;
            }

            if (toDate == "undefined")
            {
                toDate = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "K"),
                                new SqlParameter("@sims_circular_number", ""),
                                new SqlParameter("@sims_academic_year", ""),
                                new SqlParameter("@sims_cur_code", ""),
                                new SqlParameter("@sims_circular_date", ""),
                                new SqlParameter("@sims_circular_publish_date", ""),
                                new SqlParameter("@sims_circular_expiry_date", ""),
                                new SqlParameter("@sims_circular_title", ""),
                                new SqlParameter("@sims_circular_short_desc", ""),
                                new SqlParameter("@sims_circular_desc", ""),
                                new SqlParameter("@sims_circular_file_path1", ""),
                                new SqlParameter("@sims_circular_file_path2", ""),
                                new SqlParameter("@sims_circular_file_path3", ""),
                                new SqlParameter("@sims_circular_type", ""),
                                new SqlParameter("@sims_circular_category", ""),
                                new SqlParameter("@sims_circular_created_user_code", "01"),
                                new SqlParameter("@username", username),
                                new SqlParameter("@from_date",db.DBYYYYMMDDformat(fromDate)),
                                new SqlParameter("@to_date", db.DBYYYYMMDDformat(toDate))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj.sims_circular_user_status = dr["UserCStatus"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_circular_date"].ToString()))
                                // obj.sims_circular_date = Convert.ToDateTime(dr["sims_circular_date"].ToString());
                                obj.sims_circular_date = dr["sims_circular_date"].ToString();

                            if (dr["sims_circular_publish_date"].ToString() == "")
                            {
                                obj.sims_circular_publish_date = null;
                                obj.publish_status = true;
                            }
                            else
                            {
                                // obj.sims_circular_publish_date = Convert.ToDateTime(dr["sims_circular_publish_date"].ToString());
                                obj.sims_circular_publish_date = dr["sims_circular_publish_date"].ToString();
                                obj.publish_status = false;
                            }

                            obj.sims_circular_title = dr["sims_circular_title"].ToString();
                            obj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            obj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            obj.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                            obj.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                            obj.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                            obj.sims_circular_file_path1_en = dr["sims_circular_file_path1_en"].ToString();
                            obj.sims_circular_file_path2_en = dr["sims_circular_file_path2_en"].ToString();
                            obj.sims_circular_file_path3_en = dr["sims_circular_file_path3_en"].ToString();
                            obj.sims_circular_category = dr["sims_circular_category"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_circular_file_path1_en))
                                obj.sims_circular_file_path1_en = obj.sims_circular_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path2_en))
                                obj.sims_circular_file_path2_en = obj.sims_circular_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path3_en))
                                obj.sims_circular_file_path3_en = obj.sims_circular_file_path3;

                            if (dr["sims_circular_status"].ToString().Equals("A"))
                                obj.sims_circular_status = true;
                            else
                                obj.sims_circular_status = false;

                            obj.sims_circular_remark = dr["sims_circular_remark"].ToString();

                            lst_circulars.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
        }

        [Route("UpdateCircularAsRead")]
        public HttpResponseMessage UpdateCircularAsRead(string circular_number, string loggeduser, string remark)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateCircularAsRead(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_view_rights_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),                                
                                new SqlParameter("@sims_circular_number", circular_number),
                                new SqlParameter("@sims_circular_user_group_code", ""),
                                new SqlParameter("@username", loggeduser),
                                new SqlParameter("@sims_circular_status", "A"),
                                new SqlParameter("@remark", remark),
                               
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating Circular Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("GetAllDesignationNames")]
        public HttpResponseMessage GetAllDesignationNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<CommonUserControlClass> lst_designames = new List<CommonUserControlClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "L"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.designation = dr["dg_code"].ToString();
                            objNew.designation_name = dr["dg_desc"].ToString();
                            lst_designames.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_designames);
        }

        [Route("GetAllDepartments")]
        public HttpResponseMessage GetAllDepartments()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllDepartments()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Pers099> lst_departments = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "N"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 simsobj = new Pers099();
                            simsobj.code = dr["codp_dept_no"].ToString();
                            simsobj.em_dept_name = dr["codp_dept_name"].ToString();
                            lst_departments.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_departments);
        }

        [Route("getAllSection")]
        public HttpResponseMessage getAllSection(string cur_code, string ac_year, string grade)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSection(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Section", "Section"));

            List<Sim035> mod_list = new List<Sim035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@IGNORED_SECTION_LIST", grade)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim035 simsobj = new Sim035();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["section"].ToString();
                            simsobj.sims_grade_sec = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        private List<string> GetDesignationNames(string circular_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_designames = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "V"),
                                new SqlParameter("@sims_circular_number", circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_designames.Add(dr["sims_desg"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_designames;
        }

        private List<string> GetDepartmentNames(string circular_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_dept = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "W"),
                                new SqlParameter("@sims_circular_number", circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_dept.Add(dr["sims_dept"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_dept;
        }

        private List<string> GetgradeNames(string circular_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_grd = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@sims_circular_number", circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_grd.Add(dr["sims_gradecode"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_grd;
        }

        private List<string> GetsectionNames(string circular_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_section = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "O"),
                                new SqlParameter("@sims_circular_number", circular_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_section.Add(dr["grsec"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_section;
        }



        [Route("getCircularNew")]
        public HttpResponseMessage getCircularNew(string loggeduser)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCircular(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_circulars = new List<Comn015>();
            bool isenable = GetCircular_RolePer(loggeduser);
            try
            {

                var connectionString = ConfigurationManager.ConnectionStrings["sok"].ConnectionString;
         
                using (SqlConnection db = new SqlConnection(connectionString))
                {
                    db.Open();

                    SqlCommand cmd = new SqlCommand("sims_circular_proc");

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db;
                    cmd.Parameters.AddWithValue("@opr", "G");
                    cmd.Parameters.AddWithValue("@mod_code", "001");
                    cmd.Parameters.AddWithValue("@appl_code", "Com017");
                    cmd.Parameters.AddWithValue("@appl_form_field", "Setup");
                    cmd.Parameters.AddWithValue("@appl_field_type", "Circular Type");
                    cmd.Parameters.AddWithValue("@appl_field_category", "Circular Category");
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_circular_number = dr["sims_circular_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_circular_date"].ToString()))
                                obj.sims_circular_date = null;
                            else
                            // obj.sims_circular_date = dr["sims_circular_date"].ToString();
                            {
                                string dt_cdate = Convert.ToDateTime(dr["sims_circular_date"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_date"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_date"].ToString()).Day.ToString();
                                obj.sims_circular_date = dt_cdate;
                            }


                            bool p = obj.publish_status;

                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()))
                            {
                                obj.sims_circular_publish_date = DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Year.ToString();
                                obj.publish_status = true;
                            }
                            else
                            {
                                obj.sims_circular_publish_date = DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_circular_publish_date"].ToString()).Year.ToString();
                                obj.publish_status = false;
                            }

                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_circular_pstatus = "Y";
                            if ((Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()) < DateTime.Now.Date))
                                obj.sims_circular_pstatus = "E";

                            if (string.IsNullOrEmpty(dr["sims_circular_expiry_date"].ToString()))
                                obj.sims_circular_expiry_date = null;
                            else
                                obj.sims_circular_expiry_date = DateTime.Parse(dr["sims_circular_expiry_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_circular_expiry_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_circular_expiry_date"].ToString()).Year.ToString();

                            obj.sims_circular_title = dr["sims_circular_title"].ToString();
                            obj.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                            obj.sims_circular_desc = dr["sims_circular_desc"].ToString();
                            obj.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                            obj.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                            obj.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                            obj.sims_circular_file_path1_en = dr["sims_circular_file_path1_en"].ToString();
                            obj.sims_circular_file_path2_en = dr["sims_circular_file_path2_en"].ToString();
                            obj.sims_circular_file_path3_en = dr["sims_circular_file_path3_en"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_circular_file_path1_en))
                                obj.sims_circular_file_path1_en = obj.sims_circular_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path2_en))
                                obj.sims_circular_file_path2_en = obj.sims_circular_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_circular_file_path3_en))
                                obj.sims_circular_file_path3_en = obj.sims_circular_file_path3;

                            obj.sims_circular_type = dr["news_type"].ToString();
                            obj.sims_circular_category = dr["news_category"].ToString();
                            obj.sims_circular_created_user = dr["user_name1"].ToString();
                            obj.sims_display_order = dr["sims_circular_display_order"].ToString();
                            obj.sims_circular_ifalert = "Y";

                            obj.sims_circular_gradesections = string.Empty;
                            obj.sims_circular_grade = string.Empty;
                            obj.sims_circular_desg = string.Empty;
                            obj.sims_circular_dept = string.Empty;
                            obj.enablecircular = isenable;
                            //obj.enablecircular = true;
                            obj.sims_circular_publish_date = Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_publish_date"].ToString()).Day.ToString();
                            obj.sims_circular_expiry_date = Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["sims_circular_expiry_date"].ToString()).Day.ToString();

                            #region Circular Status
                            using (SqlConnection db1 = new SqlConnection(connectionString))
                            {
                                db1.Open();

                                SqlCommand cmd1 = new SqlCommand("sims_circular_proc");
                                cmd1.CommandType = CommandType.StoredProcedure;
                                cmd1.Connection = db1;
                                cmd1.Parameters.AddWithValue("@opr", "G");
                                cmd1.Parameters.AddWithValue("@sims_circular_number", obj.sims_circular_number);
                                SqlDataReader dr1 = cmd1.ExecuteReader();
                                                              //string str=null;
                                if (dr1.Read())
                                {
                                    int active_cnt = int.Parse(dr1[0].ToString());
                                    if (active_cnt > 0)
                                        obj.sims_circular_status = true;
                                    else if (active_cnt <= 0)
                                        obj.sims_circular_status = false;
                                }
                                else
                                {
                                    obj.sims_circular_status = false;
                                }
                            #endregion

                                obj.lst_groups = getCircularUserGroup(obj.sims_circular_number);
                                obj.lst_desg = GetDesignationNames(obj.sims_circular_number);
                                obj.lst_dept = GetDepartmentNames(obj.sims_circular_number);
                                obj.lst_grade = GetgradeNames(obj.sims_circular_number);
                                obj.lst_section = GetsectionNames(obj.sims_circular_number);
                                if (obj.sims_circular_status == false)
                                    obj.sims_circular_pstatus = "Y";
                                lst_circulars.Add(obj);

                            }
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_circulars);
        }
    }
}
