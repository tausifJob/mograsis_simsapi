﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;
using System.Data;

namespace SIMSAPI.Controllers.modules.AlertConfig
{
    [RoutePrefix("api/AlertConfig")]
    public class AlertConfigController : ApiController
    {

        //[Route("AlertConfigCommon")]
        //public HttpResponseMessage AlertConfigCommon(Dictionary<string, string> sf)
        //{
        //    object o = null;
        //    HttpStatusCode s = HttpStatusCode.OK;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            List<SqlParameter> sp = new List<SqlParameter>();
        //            sp.Clear();
        //            if (sf != null)
        //            {
        //                foreach (var p in sf.Keys)
        //                {
        //                    SqlParameter pr1 = new SqlParameter();
        //                    pr1.ParameterName = "@" + p;
        //                    pr1.Value = sf[p];
        //                    sp.Add(pr1);
        //                }
        //            }
        //            DataSet ds = db.ExecuteStoreProcedureDS("[sims].[AlertConfig]", sp);
        //            ds.DataSetName = "res";
        //            o = ds;
        //            s = HttpStatusCode.OK;
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        o = x;
        //        s = HttpStatusCode.InternalServerError;
        //    }
        //    return Request.CreateResponse(s, o);
        //}





        [Route("AlertConfigCommon")]
        public HttpResponseMessage getAllLibraryAttribute()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryAttribute(),PARAMETERS :: NO";
           

            List<AlertConfigObj> AlertConfig = new List<AlertConfigObj>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[AlertConfig]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AlertConfigObj simsobj = new AlertConfigObj();

                            simsobj.sims_scriptid= dr["sims_scriptid"].ToString();
                            simsobj.sims_scriptname= dr["sims_scriptname"].ToString();
                            simsobj.sims_startdate= dr["sims_startdate"].ToString();
                            simsobj.sims_nextdate= dr["sims_nextdate"].ToString();
                            simsobj.sims_alertdays= dr["sims_alertdays"].ToString();
                            simsobj.sims_status1 = dr["sims_status"].Equals("A") ? true : false;
                            simsobj.sims_control_field= dr["sims_control_field"].ToString();
                            simsobj.sims_alert_name= dr["sims_alert_name"].ToString();
                            simsobj.sims_alert_desc = dr["sims_alert_desc"].ToString();

                           

                            AlertConfig.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, AlertConfig);

            }
            return Request.CreateResponse(HttpStatusCode.OK, AlertConfig);
        }




        [Route("SaveAlertConfig")]
        public HttpResponseMessage SaveAlertConfig(AlertConfigObj data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[AlertConfig]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@sims_scriptname",data.sims_scriptname),
                    });


                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[AlertConfig]", new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@sims_scriptname",data.sims_scriptname),
                            new SqlParameter("@sims_alert_desc",data.sims_alert_desc),
                            new SqlParameter("@sims_alertdays", '1'),
                            new SqlParameter("@sims_control_field",data.sims_control_field),
                          //  new SqlParameter("@sims_status",data.status),
                             new SqlParameter("@sims_status", data.sims_status1==true?"A":"I"),
                            new SqlParameter("@sims_alert_name",data.sims_alert_name),
                            new SqlParameter("@sims_startdate",db.DBYYYYMMDDformat(data.sims_startdate)),
                            new SqlParameter("@sims_nextdate",db.DBYYYYMMDDformat(data.sims_nextdate))

                    });
                    return Request.CreateResponse(HttpStatusCode.OK, true);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("UpdateAlertConfig")]
        public HttpResponseMessage UpdateAlertConfig(AlertConfigObj data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[AlertConfig]", new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@sims_scriptid",data.sims_scriptid),
                            new SqlParameter("@sims_scriptname",data.sims_scriptname),
                            new SqlParameter("@sims_alert_desc",data.sims_alert_desc),
                            new SqlParameter("@sims_alertdays", data.sims_alertdays),
                            new SqlParameter("@sims_control_field",data.sims_control_field),
                            new SqlParameter("@sims_status", data.sims_status1==true?"A":"I"),
                            new SqlParameter("@sims_alert_name",data.sims_alert_name),
                            new SqlParameter("@sims_startdate",db.DBYYYYMMDDformat(data.sims_startdate)),
                            new SqlParameter("@sims_nextdate",db.DBYYYYMMDDformat(data.sims_nextdate))

                    });
                    return Request.CreateResponse(HttpStatusCode.OK, true);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("CUDAlertConfig")]
        public HttpResponseMessage CUDAlertConfig(List<AlertConfigObj> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (AlertConfigObj simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[AlertConfig]",

                        new List<SqlParameter>()
                     {

                               new SqlParameter("@opr", simsobj.opr),
                              
                               new SqlParameter("@sims_scriptid",simsobj.sims_scriptid),
                               new SqlParameter("@sims_scriptname",simsobj.sims_scriptname),
                               new SqlParameter("@sims_alert_desc",simsobj.sims_alert_desc),
                               new SqlParameter("@sims_alertdays", simsobj.sims_alertdays),
                               new SqlParameter("@sims_control_field",simsobj.sims_control_field),
                               new SqlParameter("@sims_status", simsobj.sims_status1==true?"A":"I"),
                               new SqlParameter("@sims_alert_name",simsobj.sims_alert_name),
                               new SqlParameter("@sims_startdate",db.DBYYYYMMDDformat(simsobj.sims_startdate)),
                               new SqlParameter("@sims_nextdate",db.DBYYYYMMDDformat(simsobj.sims_nextdate))
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}
