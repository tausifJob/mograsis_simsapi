﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
namespace SIMSAPI.Controllers.collaboration
{
    [RoutePrefix("api/communicationinitiateddetails")]
    public class CommunicationInitiatedDetailsReportController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //API Detention
        [Route("getCommunicationDetailsReport")]
        public HttpResponseMessage getCommunicationDetailsReport(string user_code, string from_date, string to_date, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDetention(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUENT", "getCommunicationDetailsReport"));
            if (search == "undefined") {
                search = null;
            }
            List<CIDR01> incidence_list = new List<CIDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Comm_initiated_detail_rpt_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@user_code",user_code),
                                new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                                new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                                new SqlParameter("@search", search),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CIDR01 Simsobj = new CIDR01();                           
                            
                            Simsobj.sims_comm_tran_date = dr["sims_comm_tran_date"].ToString();
                            Simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            Simsobj.sender_name = dr["sender_name"].ToString();
                            Simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            Simsobj.no_msg_sent = dr["no_msg_sent"].ToString();

                            incidence_list.Add(Simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, incidence_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, incidence_list);
        }
        
        


        [Route("getUserCode")]
        public HttpResponseMessage getUserCode()
        {
            List<CIDR01> lstCuriculum = new List<CIDR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Comm_initiated_detail_rpt_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "U"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CIDR01 sequence = new CIDR01();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getmessagedetails")]
        public HttpResponseMessage getmessagedetails(string user_code,string sender_id, string from_date, string to_date, string search)
        {
            if (search == "undefined")
            {
                search = null;
            }
            List<CIDR01> grade_list = new List<CIDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Comm_initiated_detail_rpt_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "V"),
                             new SqlParameter("@user_code",user_code),
                             new SqlParameter("@sender_id", sender_id),
                             new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                             new SqlParameter("@search", search),
                                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CIDR01 simsobj = new CIDR01();
                            simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
                            simsobj.reciver_name = dr["reciver_name"].ToString();
                            simsobj.sims_comm_message = dr["sims_comm_message"].ToString();
                            simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            simsobj.sender_name = dr["sender_name"].ToString();
                            simsobj.sims_comm_tran_date = dr["sims_comm_tran_date"].ToString();
                            simsobj.no_msg_sent = dr["no_msg_sent"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

    }
}