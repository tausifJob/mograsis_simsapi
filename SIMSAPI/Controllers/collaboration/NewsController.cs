﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using SIMSAPI.Models.Common;
//using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Helper;
//using System.Data.SqlClient;
//using log4net;
//using SIMSAPI.Models.SIMS.simsClass;

//namespace SIMSAPI.Controllers.modules.Common
//{
//    [RoutePrefix("api/common/News")]
//    public class NewsController : ApiController
//    {
//        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

//        [Route("getNewsNumber")]
//        public HttpResponseMessage getNewsNumber()
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsNumber(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_newsnum = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                            new SqlParameter("@opr", 'A'),                          
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 comnobj = new Comn015();
//                            comnobj.sims_news_number = dr["maxnum"].ToString();
//                            lst_newsnum.Add(comnobj);
//                        }
//                        return Request.CreateResponse(HttpStatusCode.OK, lst_newsnum);
//                    }
//                }
//            }
//            catch (Exception x)
//            {
//            }

//            return Request.CreateResponse(HttpStatusCode.OK, lst_newsnum);
//        }

//        [Route("CNews")]
//        public HttpResponseMessage CNews(Comn015 simsobj)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : CNews(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            Message message = new Message();
//            bool inserted = false;
//            string ifalert = simsobj.sims_news_ifalert;
//            // simsobj.sims_news_number = "0008";
//            try
//            {
//                if (simsobj != null)
//                {
//                    using (DBConnection db = new DBConnection())
//                    {
//                        db.Open();
//                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "I"),                                
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year_desc),
//                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
//                                new SqlParameter("@sims_news_publish_date", !string.IsNullOrEmpty(simsobj.sims_news_publish_date)?DateTime.Parse(simsobj.sims_news_publish_date):(DateTime?)null),
//                                new SqlParameter("@sims_news_expiry_date",!string.IsNullOrEmpty(simsobj.sims_news_expiry_date)?DateTime.Parse(simsobj.sims_news_expiry_date):(DateTime?)null),
//                                new SqlParameter("@sims_news_title", simsobj.sims_news_title),
//                                new SqlParameter("@sims_news_short_desc", simsobj.sims_news_short_desc),
//                                new SqlParameter("@sims_news_desc", simsobj.sims_news_desc),
//                                new SqlParameter("@sims_news_file_path1", simsobj.sims_news_file_path1),
//                                new SqlParameter("@sims_news_file_path2", simsobj.sims_news_file_path2),
//                                new SqlParameter("@sims_news_file_path3", simsobj.sims_news_file_path3),
//                                new SqlParameter("@sims_news_file_path1_en", simsobj.sims_news_file_path1_en),
//                                new SqlParameter("@sims_news_file_path2_en", simsobj.sims_news_file_path2_en),
//                                new SqlParameter("@sims_news_file_path3_en", simsobj.sims_news_file_path3_en),
//                                new SqlParameter("@sims_news_type", simsobj.sims_news_type),
//                                new SqlParameter("@sims_news_category", simsobj.sims_news_category),
//                                new SqlParameter("@sims_news_created_user_code", simsobj.sims_news_created_user),
//                                new SqlParameter("@sims_news_display_order", simsobj.sims_display_order),
//                                new SqlParameter("@mod_code", "001"),
//                                new SqlParameter("@appl_code", "Com015"),
//                                new SqlParameter("@appl_form_field", "Setup"),
//                                new SqlParameter("@appl_field_type", "News Type"),
//                                new SqlParameter("@appl_field_category", "News Category"),
//                         });
//                        if (dr.RecordsAffected > 0)
//                        {
//                            inserted = true;
//                        }
//                        if (dr.RecordsAffected < 0)
//                        {
//                            message.strMessage = "Record Not Inserted.";
//                            message.systemMessage = string.Empty;
//                            message.messageType = MessageType.Success;
//                        }

//                        if (inserted)
//                        {
//                            //Code for multiple usergroups.

//                            foreach (string str in simsobj.lst_news_groups)
//                            {
//                                using (DBConnection db1 = new DBConnection())
//                                {
//                                    db1.Open();
//                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>()
//                                     {
//                                        new SqlParameter("@opr", "I"),
//                                        new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                                        new SqlParameter("@sims_news_user_group_code", str),
//                                        new SqlParameter("@sims_news_ifalert", ifalert),
//                                        new SqlParameter("@sims_news_status", simsobj.sims_news_status == true?"A":"I"),
//                                     });
//                                    if (dr1.RecordsAffected > 0)
//                                    {
//                                        inserted = true;
//                                    }
//                                }
//                            }

//                            if (inserted)
//                            {
//                                message.strMessage = "News Added Successfully.";
//                                message.messageType = MessageType.Error;
//                                return Request.CreateResponse(HttpStatusCode.OK, message);
//                            }
//                            else
//                            {
//                                message.strMessage = "Error In Parsing Information.";
//                                message.messageType = MessageType.Error;
//                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                            }
//                        }

//                        return Request.CreateResponse(HttpStatusCode.OK, message);
//                    }
//                }
//                else
//                {
//                    message.strMessage = "Error In Parsing Information.";
//                    message.messageType = MessageType.Error;
//                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//                message.strMessage = "Error In Inserting News Information";
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }

//        [Route("getNewsType")]
//        public HttpResponseMessage getNewsType()
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsType(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_newsType = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                             new SqlParameter("@opr", "B"),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 simsobj = new Comn015();
//                            simsobj.sims_news_type = dr["sims_appl_form_field_value1"].ToString();
//                            simsobj.sims_news_type_code = dr["sims_appl_parameter"].ToString();
//                            lst_newsType.Add(simsobj);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_newsType);
//        }

//        [Route("getNewsCategory")]
//        public HttpResponseMessage getNewsCategory()
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsCategory(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_newscat = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                             new SqlParameter("@opr", "C"),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 simsobj = new Comn015();
//                            simsobj.sims_news_category = dr["sims_appl_form_field_value1"].ToString();
//                            simsobj.sims_news_cat_code = dr["sims_appl_parameter"].ToString();
//                            lst_newscat.Add(simsobj);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_newscat);
//        }

//        private bool GetCircular_RolePer(string username)
//        {
//            string str = string.Empty;
//            bool flag = false;
//            try
//            {
//                using (DBConnection db2 = new DBConnection())
//                {
//                    db2.Open();
//                    SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() {
//                                new SqlParameter("@opr", "Y"),
//                                new SqlParameter("@user_name", username)
//                        });
//                    if (dr2.HasRows)
//                    {
//                        while (dr2.Read())
//                        {
//                            str = dr2["RolePer"].ToString();
//                        }
//                    }
//                    if (str == "Y")
//                        flag = true;
//                    return flag;
//                }
//            }
//            catch (Exception e)
//            {
//                return flag;
//            }
//        }

//        [Route("getNews")]
//        public HttpResponseMessage getNews(string loggeduser)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNews(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_news = new List<Comn015>();
//            bool isenable = GetCircular_RolePer(loggeduser);
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "G"),
//                                new SqlParameter("@mod_code", "001"),
//                                new SqlParameter("@appl_code", "Com015"),
//                                new SqlParameter("@appl_form_field", "Setup"),
//                                new SqlParameter("@appl_field_type", "News Type"),
//                                new SqlParameter("@appl_field_category", "News Category"),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 obj = new Comn015();
//                            obj.sims_news_number = dr["sims_news_number"].ToString();
//                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
//                            obj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
//                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
//                            obj.sims_cur_name = dr["cur_name"].ToString();
//                            if (string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
//                                obj.sims_news_date = null;
//                            else
//                                obj.sims_news_date = dr["sims_news_date"].ToString();


//                            bool p = obj.publish_status;

//                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_news_publish_date"].ToString()))
//                            {
//                                obj.sims_news_publish_date = DateTime.Parse(dr["sims_news_publish_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_news_publish_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_news_publish_date"].ToString()).Year.ToString();
//                                obj.publish_status = true;
//                            }
//                            else
//                            {
//                                obj.sims_news_publish_date = DateTime.Parse(dr["sims_news_publish_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_news_publish_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_news_publish_date"].ToString()).Year.ToString();
//                                obj.publish_status = false;
//                            }

//                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
//                                obj.sims_news_pstatus = "N";
//                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
//                                obj.sims_news_pstatus = "N";
//                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
//                                obj.sims_news_pstatus = "Y";
//                            if ((Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) < DateTime.Now.Date))
//                                obj.sims_news_pstatus = "E";

//                            if (string.IsNullOrEmpty(dr["sims_news_expiry_date"].ToString()))
//                                obj.sims_news_expiry_date = null;
//                            else
//                                obj.sims_news_expiry_date = DateTime.Parse(dr["sims_news_expiry_date"].ToString()).Month.ToString() + "/" + DateTime.Parse(dr["sims_news_expiry_date"].ToString()).Day.ToString() + "/" + DateTime.Parse(dr["sims_news_expiry_date"].ToString()).Year.ToString();

//                            obj.sims_news_title = dr["sims_news_title"].ToString();
//                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
//                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
//                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
//                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
//                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
//                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
//                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
//                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();

//                            if (string.IsNullOrEmpty(obj.sims_news_file_path1_en))
//                                obj.sims_news_file_path1_en = obj.sims_news_file_path1;
//                            if (string.IsNullOrEmpty(obj.sims_news_file_path2_en))
//                                obj.sims_news_file_path2_en = obj.sims_news_file_path2;
//                            if (string.IsNullOrEmpty(obj.sims_news_file_path3_en))
//                                obj.sims_news_file_path3_en = obj.sims_news_file_path3;

//                            obj.sims_news_type = dr["news_type"].ToString();
//                            obj.sims_news_category = dr["news_category"].ToString();
//                            obj.sims_news_created_user = dr["user_name1"].ToString();
//                            obj.sims_display_order = dr["sims_news_display_order"].ToString();
//                            obj.sims_news_ifalert = "N";
//                            obj.enablenews = isenable;
//                            obj.sims_news_publish_date = Convert.ToDateTime(dr["sims_news_publish_date"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["sims_news_publish_date"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["sims_news_publish_date"].ToString()).Day.ToString();
//                            obj.sims_news_expiry_date = Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()).Year.ToString() + "-" + Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()).Month.ToString() + "-" + Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()).Day.ToString();

//                            #region News Status

//                            using (DBConnection db1 = new DBConnection())
//                            {
//                                db1.Open();
//                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_proc",
//                                    new List<SqlParameter>() 
//                                     { 
//                                        new SqlParameter("@opr", "D"),
//                                        new SqlParameter("@sims_news_number", obj.sims_news_number),
//                                     });
//                                //string str=null;
//                                if (dr1.Read())
//                                {
//                                    int active_cnt = int.Parse(dr1[0].ToString());
//                                    if (active_cnt > 0)
//                                        obj.sims_news_status = true;
//                                    else if (active_cnt <= 0)
//                                        obj.sims_news_status = false;
//                                }
//                                else
//                                {
//                                    obj.sims_news_status = false;
//                                }
//                            #endregion
//                            }
//                            if (obj.sims_news_status == false)
//                                obj.sims_news_pstatus = "Y";
//                            obj.lst_news_groups = getNewsUserGroup1(obj.sims_news_number);

//                            obj.news_doc_lst = GetNewsDocs(obj.sims_news_number);

//                            lst_news.Add(obj);

//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
//        }

//        [Route("getUpdateNewsViewRights")]
//        public HttpResponseMessage getUpdateNewsViewRights(Comn015 comnobj, string loggeduser)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUpdateNewsViewRights(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            bool updated = false;

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", 'U'),
//                                new SqlParameter("@sims_news_number", comnobj.sims_news_number),
//                                new SqlParameter("@sims_news_user_group_code", ""),
//                                new SqlParameter("@username", loggeduser),
//                                new SqlParameter("@sims_news_status", comnobj.sims_news_status == true?"A":"I"),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            updated = true;
//                        }
//                        return Request.CreateResponse(HttpStatusCode.OK, updated);
//                    }

//                }
//            }
//            catch (Exception x)
//            {
//                return Request.CreateResponse(HttpStatusCode.InternalServerError, updated);
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, updated);
//        }

//        [Route("getNewsUserGroup")]
//        public HttpResponseMessage getNewsUserGroup(Comn015 comnobj)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsUserGroup(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_newsusergrps = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                             new SqlParameter("@opr", "F"),
//                             new SqlParameter("@sims_news_number", comnobj.sims_news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 simsobj = new Comn015();
//                            simsobj.sims_news_user_group = dr["comn_user_group_name"].ToString();
//                            lst_newsusergrps.Add(simsobj);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_newsusergrps);
//        }

//        private List<string> getNewsUserGroup1(string news_number)
//        {
//            List<string> lst_newsusergrps = new List<string>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                             new SqlParameter("@opr", "F"),
//                             new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            //sims_news_user_group = dr["comn_user_group_name"].ToString();
//                            lst_newsusergrps.Add(dr["comn_user_group_code"].ToString());
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_newsusergrps;
//        }

//        [Route("getAllNewsUserGroup")]
//        public HttpResponseMessage getAllNewsUserGroup()
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllNewsUserGroup(),PARAMETERS :: NA";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<Comn015> lst_newsusergrps = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                             new SqlParameter("@opr", "H"),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 simsobj = new Comn015();
//                            simsobj.sims_news_user_group = dr["comn_user_group_name"].ToString();
//                            simsobj.sims_news_user_group_code = dr["comn_user_group_code"].ToString();
//                            lst_newsusergrps.Add(simsobj);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_newsusergrps);
//        }

//        /* still 3 methods are remaining */

//        [Route("DNews")]
//        public HttpResponseMessage DNews(Comn015 simsobj)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : DNews()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));
//            Message message = new Message();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "C"),
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                         });
//                    if (dr.RecordsAffected > 0)
//                    {
//                        message.strMessage = "News Deleted Successfully.";
//                        message.systemMessage = string.Empty;
//                        message.messageType = MessageType.Success;
//                    }
//                    else
//                    {
//                        message.strMessage = "News Not Deleted.";
//                        message.systemMessage = string.Empty;
//                        message.messageType = MessageType.Success;
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }


//        [Route("UNews")]
//        public HttpResponseMessage UNews(Comn015 simsobj)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UNews(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            Message message = new Message();
//            bool inserted = false;
//            string ifalert = simsobj.sims_news_ifalert;
//            // simsobj.sims_news_number = "0008";
//            try
//            {
//                if (simsobj != null)
//                {
//                    using (DBConnection db = new DBConnection())
//                    {
//                        db.Open();
//                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "U"),                                
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
//                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
//                                new SqlParameter("@sims_news_publish_date", simsobj.sims_news_publish_date),
//                                new SqlParameter("@sims_news_expiry_date",simsobj.sims_news_expiry_date),
//                                new SqlParameter("@sims_news_title", simsobj.sims_news_title),
//                                new SqlParameter("@sims_news_short_desc", simsobj.sims_news_short_desc),
//                                new SqlParameter("@sims_news_desc", simsobj.sims_news_desc),
//                                new SqlParameter("@sims_news_file_path1", simsobj.sims_news_file_path1),
//                                new SqlParameter("@sims_news_file_path2", simsobj.sims_news_file_path2),
//                                new SqlParameter("@sims_news_file_path3", simsobj.sims_news_file_path3),
//                                new SqlParameter("@sims_news_file_path1_en", simsobj.sims_news_file_path1_en),
//                                new SqlParameter("@sims_news_file_path2_en", simsobj.sims_news_file_path2_en),
//                                new SqlParameter("@sims_news_file_path3_en", simsobj.sims_news_file_path3_en),
//                                new SqlParameter("@sims_news_type", simsobj.sims_news_type),
//                                new SqlParameter("@sims_news_category", simsobj.sims_news_category),
//                                new SqlParameter("@sims_news_created_user_code", simsobj.sims_news_created_user),
//                                new SqlParameter("@sims_news_display_order", simsobj.sims_display_order),
//                                new SqlParameter("@mod_code", "001"),
//                                new SqlParameter("@appl_code", "Com015"),
//                                new SqlParameter("@appl_form_field", "Setup"),
//                                new SqlParameter("@appl_field_type", "News Type"),
//                                new SqlParameter("@appl_field_category", "News Category"),
//                         });
//                        if (dr.RecordsAffected > 0)
//                        {
//                            inserted = true;
//                        }
//                        if (dr.RecordsAffected < 0)
//                        {
//                            message.strMessage = "Record Not Updated.";
//                            message.systemMessage = string.Empty;
//                            message.messageType = MessageType.Success;
//                        }

//                        if (inserted)
//                        {
//                            //Delete all usercode in sims_news_view_rights table.
//                            using (DBConnection db2 = new DBConnection())
//                            {
//                                db2.Open();
//                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() {
//                                new SqlParameter("@opr", "D"),
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                                });

//                                if (dr2.RecordsAffected > 0)
//                                {
//                                    inserted = true;
//                                }
//                            }
//                        }
//                        if (inserted)
//                        {
//                            //Code for multiple usergroups.
//                            foreach (string str in simsobj.lst_news_groups)
//                            {
//                                using (DBConnection db1 = new DBConnection())
//                                {
//                                    db1.Open();
//                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>()
//                                     {
//                                        new SqlParameter("@opr", "I"),
//                                        new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                                        new SqlParameter("@sims_news_user_group_code", str),
//                                        new SqlParameter("@sims_news_ifalert", ifalert),
//                                        new SqlParameter("@sims_news_status", simsobj.sims_news_status == true?"A":"I"),
//                                     });
//                                    if (dr1.RecordsAffected > 0)
//                                    {
//                                        inserted = true;
//                                    }
//                                }
//                            }

//                            if (inserted)
//                            {
//                                message.strMessage = "News Updated Successfully.";
//                                message.messageType = MessageType.Error;
//                                return Request.CreateResponse(HttpStatusCode.OK, message);
//                            }
//                            else
//                            {
//                                message.strMessage = "Error In Parsing Information.";
//                                message.messageType = MessageType.Error;
//                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                            }
//                        }

//                        return Request.CreateResponse(HttpStatusCode.OK, message);
//                    }
//                }
//                else
//                {
//                    message.strMessage = "Error In Parsing Information.";
//                    message.messageType = MessageType.Error;
//                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//                message.strMessage = "Error In Updating News Information";
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }

//        [Route("UpdateNewsPublishDate")]
//        public HttpResponseMessage UpdateNewsPublishDate(Comn015 simsobj)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsPublishDate(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            Message message = new Message();
//            bool inserted = false;
//            string ifalert = simsobj.sims_news_ifalert;
//            // simsobj.sims_news_number = "0008";
//            try
//            {
//                if (simsobj != null)
//                {
//                    using (DBConnection db = new DBConnection())
//                    {
//                        db.Open();
//                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "A4"),                                
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),                                
//                         });
//                        if (dr.RecordsAffected > 0)
//                        {
//                            inserted = true;
//                        }                       

//                        if (inserted)
//                        {
//                            message.strMessage = "The Selected News Published Today.";
//                            message.messageType = MessageType.Error;
//                            return Request.CreateResponse(HttpStatusCode.OK, message);
//                        }
//                        else
//                        {
//                            message.strMessage = "Error In Parsing Information.";
//                            message.messageType = MessageType.Error;
//                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                        }
//                    }
//                }
//                else
//                {
//                    message.strMessage = "Error In Parsing Information.";
//                    message.messageType = MessageType.Error;
//                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//                message.strMessage = "Error In Updating News Information";
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }

//        [Route("UpdateNewsStatus")]
//        public HttpResponseMessage UpdateNewsStatus(Comn015 simsobj)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsStatus(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            Message message = new Message();
//            bool inserted = false;
//            string ifalert = simsobj.sims_news_ifalert;
//            // simsobj.sims_news_number = "0008";
//            try
//            {
//                if (simsobj != null)
//                {
//                    using (DBConnection db = new DBConnection())
//                    {
//                        db.Open();
//                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "A"),
//                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
//                         });
//                        if (dr.RecordsAffected > 0)
//                        {
//                            inserted = true;
//                        }
//                        if (dr.RecordsAffected < 0)
//                        {
//                            message.strMessage = "Record Not Updated.";
//                            message.systemMessage = string.Empty;
//                            message.messageType = MessageType.Success;
//                        }

//                        if (inserted)
//                        {
//                            message.strMessage = "News Unpublished Successfully.";
//                            message.messageType = MessageType.Error;
//                            return Request.CreateResponse(HttpStatusCode.OK, message);
//                        }
//                        else
//                        {
//                            message.strMessage = "Error In Parsing Information.";
//                            message.messageType = MessageType.Error;
//                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                        }
//                    }
//                }
//                else
//                {
//                    message.strMessage = "Error In Parsing Information.";
//                    message.messageType = MessageType.Error;
//                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//                message.strMessage = "Error In Updating News Information";
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }

//        [Route("getUserDetails")]
//        public HttpResponseMessage getUserDetails(string username)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserDetails(),PARAMETERS :: :: UserName{2}";
//            Log.Debug(string.Format(debug, "ERP", "SETUP", username));
//            Comn005 UserDet = new Comn005();

//            //List<Comn005> lst_UserDet = new List<Comn005>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "X"),
//                                new SqlParameter("@comn_user_name", username),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            UserDet.User_Name = dr["comn_user_name"].ToString();
//                            UserDet.User_Code = dr["comn_user_code"].ToString();
//                            UserDet.Email_id = dr["Mail_Id"].ToString();
//                            UserDet.Expiry_Date = dr["comn_user_expiry_date"].ToString();
//                            UserDet.Group_Code = dr["comn_user_group_code"].ToString();
//                            UserDet.Last_Login = dr["comn_user_last_login"].ToString();
//                            UserDet.user_image = dr["user_image"].ToString();
//                            UserDet.User_password = dr["comn_user_password"].ToString();
//                            UserDet.UserAlertCount = dr["AlertCount"].ToString();
//                            UserDet.UserCircularCount = dr["CircularCount"].ToString();
//                            UserDet.UserNewsCount = dr["NewsCount"].ToString();
//                            UserDet.messageCount = dr["MessageCount"].ToString();
//                            UserDet.Designation = dr["Desig"].ToString();
//                            UserDet.isAdmin = dr["ADMINuser"].ToString().Equals("1");
//                            string[] str = UserDet.Designation.Split(' ');
//                            if (str.Length > 1)
//                                UserDet.DesigLtr = str[0][0] + "" + str[1][0];
//                            else
//                                UserDet.DesigLtr = str[0][0] + "";

//                            //lst_UserDet.Add(UserDet);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, UserDet);
//        }

//        [Route("GetUserNews")]
//        public HttpResponseMessage GetUserNews(string username)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserNews(),PARAMETERS :: UserName{2}";
//            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

//            List<Comn015> lst_news = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "E"),
//                                new SqlParameter("@sims_news_number", ""),
//                                new SqlParameter("@sims_academic_year", ""),
//                                new SqlParameter("@sims_cur_code", ""),
//                                new SqlParameter("@sims_news_date", ""),
//                                new SqlParameter("@sims_news_publish_date", ""),
//                                new SqlParameter("@sims_news_expiry_date", ""),
//                                new SqlParameter("@sims_news_title", ""),
//                                new SqlParameter("@sims_news_short_desc", ""),
//                                new SqlParameter("@sims_news_desc", ""),
//                                new SqlParameter("@sims_news_file_path1", ""),
//                                new SqlParameter("@sims_news_file_path2", ""),
//                                new SqlParameter("@sims_news_file_path3", ""),
//                                new SqlParameter("@sims_news_type", ""),
//                                new SqlParameter("@sims_news_category", ""),
//                                new SqlParameter("@sims_news_created_user_code", ""),
//                                new SqlParameter("@username", username),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 obj = new Comn015();
//                            obj.sims_news_number = dr["sims_news_number"].ToString();
//                            // obj.sims_news_user_status = dr["UserCStatus"].ToString();
//                            if (!string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
//                                // obj.sims_news_date = Convert.ToDateTime(dr["sims_news_date"].ToString());
//                                obj.sims_news_date = dr["sims_news_date"].ToString();

//                            if (dr["sims_news_publish_date"].ToString() == "")
//                            {
//                                obj.sims_news_publish_date = null;
//                                obj.publish_status = true;
//                            }
//                            else
//                            {
//                                // obj.sims_news_publish_date = Convert.ToDateTime(dr["sims_news_publish_date"].ToString());
//                                obj.sims_news_publish_date = dr["sims_news_publish_date"].ToString();
//                                obj.publish_status = false;
//                            }

//                            obj.sims_news_title = dr["sims_news_title"].ToString();
//                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
//                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
//                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
//                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
//                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
//                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
//                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
//                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();
//                            obj.sims_news_category = dr["sims_news_category"].ToString();

//                            if (dr["sims_news_status"].ToString().Equals("A"))
//                                obj.sims_news_status = true;
//                            else
//                                obj.sims_news_status = false;

//                            lst_news.Add(obj);
//                        }
//                    }
//                }
//                return Request.CreateResponse(HttpStatusCode.OK, lst_news);
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
//        }

//        [Route("GetUserNewsBYDate")]
//        public HttpResponseMessage GetUserNewsBYDate(string username, string fromDate, string toDate)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserNewsBYDate(),PARAMETERS :: UserName{2}";
//            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

//            List<Comn015> lst_news = new List<Comn015>();

//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "K"),
//                                new SqlParameter("@sims_news_number", ""),
//                                new SqlParameter("@sims_academic_year", ""),
//                                new SqlParameter("@sims_cur_code", ""),
//                                new SqlParameter("@sims_news_date", ""),
//                                new SqlParameter("@sims_news_publish_date", ""),
//                                new SqlParameter("@sims_news_expiry_date", ""),
//                                new SqlParameter("@sims_news_title", ""),
//                                new SqlParameter("@sims_news_short_desc", ""),
//                                new SqlParameter("@sims_news_desc", ""),
//                                new SqlParameter("@sims_news_file_path1", ""),
//                                new SqlParameter("@sims_news_file_path2", ""),
//                                new SqlParameter("@sims_news_file_path3", ""),
//                                new SqlParameter("@sims_news_type", ""),
//                                new SqlParameter("@sims_news_category", ""),
//                                new SqlParameter("@sims_news_created_user_code", "01"),
//                                new SqlParameter("@username", username),
//                                new SqlParameter("@from_date",fromDate.Equals("undefined")?null:fromDate),
//                                new SqlParameter("@to_date", toDate.Equals("undefined")?null:toDate)
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            Comn015 obj = new Comn015();
//                            obj.sims_news_number = dr["sims_news_number"].ToString();
//                            obj.sims_news_user_status = dr["UserCStatus"].ToString();
//                            if (!string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
//                                // obj.sims_news_date = Convert.ToDateTime(dr["sims_news_date"].ToString());
//                                obj.sims_news_date = dr["sims_news_date"].ToString();

//                            if (dr["sims_news_publish_date"].ToString() == "")
//                            {
//                                obj.sims_news_publish_date = null;
//                                obj.publish_status = true;
//                            }
//                            else
//                            {
//                                // obj.sims_news_publish_date = Convert.ToDateTime(dr["sims_news_publish_date"].ToString());
//                                obj.sims_news_publish_date = dr["sims_news_publish_date"].ToString();
//                                obj.publish_status = false;
//                            }

//                            obj.sims_news_title = dr["sims_news_title"].ToString();
//                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
//                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
//                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
//                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
//                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
//                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
//                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
//                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();
//                            obj.sims_news_category = dr["sims_news_category"].ToString();

//                            if (dr["sims_news_status"].ToString().Equals("A"))
//                                obj.sims_news_status = true;
//                            else
//                                obj.sims_news_status = false;

//                            lst_news.Add(obj);
//                        }
//                    }
//                }
//                return Request.CreateResponse(HttpStatusCode.OK, lst_news);
//            }
//            catch (Exception x) { }
//            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
//        }

//        [Route("UpdateNewsAsRead")]
//        public HttpResponseMessage UpdateNewsAsRead(string news_number, string loggeduser)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsAsRead(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            Message message = new Message();
//            bool inserted = false;
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "U"),                                
//                                new SqlParameter("@sims_news_number", news_number),
//                                new SqlParameter("@sims_news_user_group_code", ""),
//                                new SqlParameter("@username", loggeduser),
//                                new SqlParameter("@sims_news_status", "A"),
                               
//                         });
//                    if (dr.RecordsAffected > 0)
//                    {
//                        inserted = true;
//                    }
//                    return Request.CreateResponse(HttpStatusCode.OK, message);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//                message.strMessage = "Error In Updating News Information";
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, message);
//        }

//        //----------------------------------------------------------------------

//        private List<string> GetDesignationNames(string news_number)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<string> lst_designames = new List<string>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "V"),
//                                new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            //CommonUserControlClass objNew = new CommonUserControlClass();
//                            // objNew.designation = dr["sims_desg"].ToString();
//                            lst_designames.Add(dr["sims_desg"].ToString());
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_designames;
//        }

//        private List<string> GetDepartmentNames(string news_number)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<string> lst_dept = new List<string>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "W"),
//                                new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            //CommonUserControlClass objNew = new CommonUserControlClass();
//                            // objNew.designation = dr["sims_desg"].ToString();
//                            lst_dept.Add(dr["sims_dept"].ToString());
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_dept;
//        }

//        private List<string> GetgradeNames(string news_number)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<string> lst_grd = new List<string>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "R"),
//                                new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            //CommonUserControlClass objNew = new CommonUserControlClass();
//                            // objNew.designation = dr["sims_desg"].ToString();
//                            lst_grd.Add(dr["sims_gradecode"].ToString());
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_grd;
//        }

//        private List<string> GetsectionNames(string news_number)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<string> lst_section = new List<string>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "O"),
//                                new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            //CommonUserControlClass objNew = new CommonUserControlClass();
//                            // objNew.designation = dr["sims_desg"].ToString();
//                            lst_section.Add(dr["grsec"].ToString());
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_section;
//        }

//        [Route("SaveNewsDocs")]
//        public HttpResponseMessage SaveNewsDocs(string news_number, string original_filename, string updatedfilename)
//        {
//            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsAsRead(), PARAMETERS ::No";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            bool inserted = false;
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "A5"),                                
//                                new SqlParameter("@sims_news_number", news_number),
//                                new SqlParameter("@sims_file_path", updatedfilename),
//                                new SqlParameter("@sims_file_path_en", original_filename),
                               
//                         });
//                    if (dr.RecordsAffected > 0)
//                    {
//                        inserted = true;
//                    }
//                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
//                }
//            }
//            catch (Exception x)
//            {
//                Log.Error(x);
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, inserted);
//        }

//        private List<news_docs> GetNewsDocs(string news_number)
//        {
//            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
//            Log.Debug(string.Format(debug, "ERP", "SETUP"));

//            List<news_docs> lst_docs = new List<news_docs>();
//            try
//            {
//                using (DBConnection db = new DBConnection())
//                {
//                    db.Open();
//                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
//                        new List<SqlParameter>() 
//                         { 
//                                new SqlParameter("@opr", "A7"),
//                                new SqlParameter("@sims_news_number", news_number),
//                         });
//                    if (dr.HasRows)
//                    {
//                        while (dr.Read())
//                        {
//                            news_docs nd = new news_docs();
//                            nd.sims_news_originalfile = dr["sims_file_path_en"].ToString();
//                            nd.sims_news_newfile = dr["sims_file_path"].ToString();
//                            lst_docs.Add(nd);
//                        }
//                    }
//                }
//            }
//            catch (Exception x) { }
//            return lst_docs;
//        }
//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/News")]
    public class NewsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getNewsNumber")]
        public HttpResponseMessage getNewsNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsNumber(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_newsnum = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 comnobj = new Comn015();
                            comnobj.sims_news_number = dr["maxnum"].ToString();
                            lst_newsnum.Add(comnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst_newsnum);
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, lst_newsnum);
        }

        [Route("CNews")]
        public HttpResponseMessage CNews(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : CNews(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_news_ifalert;
            if (simsobj.sims_news_desg == "undefined")
                simsobj.sims_news_desg = "";
            if (simsobj.sims_news_dept == "undefined")
                simsobj.sims_news_dept = "";
            if (simsobj.sims_news_gradesections == "undefined")
                simsobj.sims_news_gradesections = "";
            // simsobj.sims_news_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),                                
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year_desc),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            //    new SqlParameter("@sims_news_publish_date", !string.IsNullOrEmpty(simsobj.sims_news_publish_date)?DateTime.Parse(simsobj.sims_news_publish_date):(DateTime?)null),
                              //  new SqlParameter("@sims_news_expiry_date",!string.IsNullOrEmpty(simsobj.sims_news_expiry_date)?DateTime.Parse(simsobj.sims_news_expiry_date):(DateTime?)null),
                                new SqlParameter("@sims_news_publish_date", !string.IsNullOrEmpty(simsobj.sims_news_publish_date)?db.DBYYYYMMDDformat(simsobj.sims_news_publish_date):null),
                                new SqlParameter("@sims_news_expiry_date",!string.IsNullOrEmpty(simsobj.sims_news_expiry_date)?db.DBYYYYMMDDformat(simsobj.sims_news_expiry_date):null),
                           
                              new SqlParameter("@sims_news_title", simsobj.sims_news_title),
                                new SqlParameter("@sims_news_short_desc", simsobj.sims_news_short_desc),
                                new SqlParameter("@sims_news_desc", simsobj.sims_news_desc),
                                new SqlParameter("@sims_news_file_path1", simsobj.sims_news_file_path1),
                                new SqlParameter("@sims_news_file_path2", simsobj.sims_news_file_path2),
                                new SqlParameter("@sims_news_file_path3", simsobj.sims_news_file_path3),
                                new SqlParameter("@sims_news_file_path1_en", simsobj.sims_news_file_path1_en),
                                new SqlParameter("@sims_news_file_path2_en", simsobj.sims_news_file_path2_en),
                                new SqlParameter("@sims_news_file_path3_en", simsobj.sims_news_file_path3_en),
                                new SqlParameter("@sims_news_type", simsobj.sims_news_type),
                                new SqlParameter("@sims_news_category", simsobj.sims_news_category),
                                new SqlParameter("@sims_news_created_user_code", simsobj.sims_news_created_user),
                                new SqlParameter("@sims_news_display_order", simsobj.sims_display_order),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com015"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "News Type"),
                                new SqlParameter("@appl_field_category", "News Category"),
                                new SqlParameter("@gradesections", simsobj.sims_news_gradesections),
                                new SqlParameter("@desg", simsobj.sims_news_desg),
                                new SqlParameter("@dept", simsobj.sims_news_dept)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Inserted.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        if (inserted)
                        {
                            //Code for multiple usergroups.

                            foreach (string str in simsobj.lst_news_groups)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                                        new SqlParameter("@sims_news_user_group_code", str),
                                        new SqlParameter("@sims_news_ifalert", ifalert),
                                        new SqlParameter("@sims_news_status", simsobj.sims_news_status == true?"A":"I"),
                                        new SqlParameter("@gradesections", simsobj.sims_news_gradesections),
                                        new SqlParameter("@desg", simsobj.sims_news_desg),
                                        new SqlParameter("@dept", simsobj.sims_news_dept)
                                     });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                }
                            }

                            if (inserted)
                            {
                                message.strMessage = "News Added Successfully.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Error In Parsing Information.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Inserting News Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getNewsType")]
        public HttpResponseMessage getNewsType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsType(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_newsType = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_news_type = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_news_type_code = dr["sims_appl_parameter"].ToString();
                            lst_newsType.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_newsType);
        }

        [Route("getNewsCategory")]
        public HttpResponseMessage getNewsCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsCategory(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_newscat = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_news_category = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_news_cat_code = dr["sims_appl_parameter"].ToString();
                            lst_newscat.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_newscat);
        }

        private bool GetCircular_RolePer(string username)
        {
            string str = string.Empty;
            bool flag = false;
            try
            {
                using (DBConnection db2 = new DBConnection())
                {
                    db2.Open();
                    SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_circular_proc", new List<SqlParameter>() {
                                new SqlParameter("@opr", "Y"),
                                new SqlParameter("@user_name", username)
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            str = dr2["RolePer"].ToString();
                        }
                    }
                    if (str == "Y")
                        flag = true;
                    return flag;
                }
            }
            catch (Exception e)
            {
                return flag;
            }
        }

        [Route("getNews")]
        public HttpResponseMessage getNews(string loggeduser)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNews(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_news = new List<Comn015>();
            bool isenable = GetCircular_RolePer(loggeduser);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com015"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "News Type"),
                                new SqlParameter("@appl_field_category", "News Category"),
                                //new SqlParameter("@sfromdate", sfromdate),
                                //new SqlParameter("@stodate", stodate),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_news_number = dr["sims_news_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc =  dr["sims_academic_year_desc"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
                                obj.sims_news_date = null;
                            else
                                obj.sims_news_date = dr["sims_news_date"].ToString();


                            bool p = obj.publish_status;

                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_news_publish_date"].ToString()))
                            {
                                obj.sims_news_publish_date = db.UIDDMMYYYYformat((dr["sims_news_publish_date"].ToString()));
                                obj.publish_status = true;
                            }
                            else
                            {
                                    obj.sims_news_publish_date = db.UIDDMMYYYYformat((dr["sims_news_publish_date"].ToString()));
                               obj.publish_status = false;
                            }

                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "Y";
                            if ((Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) < DateTime.Now.Date))
                                obj.sims_news_pstatus = "E";

                            if (string.IsNullOrEmpty(dr["sims_news_expiry_date"].ToString()))
                                obj.sims_news_expiry_date = null;
                            else
                                obj.sims_news_expiry_date = db.UIDDMMYYYYformat(dr["sims_news_expiry_date"].ToString());

                            obj.sims_news_title = dr["sims_news_title"].ToString();
                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_news_file_path1_en))
                                obj.sims_news_file_path1_en = obj.sims_news_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_news_file_path2_en))
                                obj.sims_news_file_path2_en = obj.sims_news_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_news_file_path3_en))
                                obj.sims_news_file_path3_en = obj.sims_news_file_path3;

                            obj.sims_news_type = dr["news_type"].ToString();
                            obj.sims_news_category = dr["news_category"].ToString();
                            obj.sims_news_created_user = dr["user_name1"].ToString();
                            obj.sims_display_order = dr["sims_news_display_order"].ToString();
                            obj.sims_news_ifalert = "N";

                            obj.sims_news_gradesections = dr["sims_news_section"].ToString();
                            obj.sims_news_grade = dr["sims_news_grade"].ToString();
                            obj.sims_news_desg = dr["sims_news_desg"].ToString();
                            obj.sims_news_dept = dr["sims_news_dept"].ToString();

                            obj.enablenews = isenable;
                            obj.sims_news_publish_date = db.UIDDMMYYYYformat(dr["sims_news_publish_date"].ToString());
                            obj.sims_news_expiry_date = db.UIDDMMYYYYformat(dr["sims_news_expiry_date"].ToString());

                            #region News Status

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_proc",
                                    new List<SqlParameter>() 
                                     { 
                                        new SqlParameter("@opr", "D"),
                                        new SqlParameter("@sims_news_number", obj.sims_news_number),
                                     });
                                //string str=null;
                                if (dr1.Read())
                                {
                                    int active_cnt = int.Parse(dr1[0].ToString());
                                    if (active_cnt > 0)
                                        obj.sims_news_status = true;
                                    else if (active_cnt <= 0)
                                        obj.sims_news_status = false;
                                }
                                else
                                {
                                    obj.sims_news_status = false;
                                }
                                obj.sims_checkstatus = obj.sims_news_status;
                            }
                            #endregion

                            obj.lst_desg_news = GetDesignationNames(obj.sims_news_number);
                            obj.lst_dept_news = GetDepartmentNames(obj.sims_news_number);
                            obj.lst_grade_news = GetgradeNames(obj.sims_news_number);
                            obj.lst_section_news = GetsectionNames(obj.sims_news_number);

                            if (obj.sims_news_status == false)
                                obj.sims_news_pstatus = "Y";
                            obj.lst_news_groups = getNewsUserGroup1(obj.sims_news_number);
                            obj.news_doc_lst = new List<news_docs>();
                            obj.news_doc_lst = GetNewsDocs(obj.sims_news_number);

                            lst_news.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
        }

        [Route("getNews")]
        public HttpResponseMessage getNews(string loggeduser, string sfromdate, string stodate)
        {
            if (sfromdate == "undefined")
                sfromdate = "";
            if (stodate == "undefined")
                stodate = "";

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNews(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_news = new List<Comn015>();
            bool isenable = GetCircular_RolePer(loggeduser);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com015"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "News Type"),
                                new SqlParameter("@appl_field_category", "News Category"),
                                new SqlParameter("@sfromdate", db.DBYYYYMMDDformat( sfromdate)),
                                new SqlParameter("@stodate", db.DBYYYYMMDDformat(stodate)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_news_number = dr["sims_news_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_desc = dr["sims_academic_year_desc"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
                                obj.sims_news_date = null;
                            else
                                obj.sims_news_date = db.UIDDMMYYYYformat( dr["sims_news_date"].ToString());


                            bool p = obj.publish_status;

                            if (DateTime.Now.Date != Convert.ToDateTime(dr["sims_news_publish_date"].ToString()))
                            {
                                obj.sims_news_publish_date = db.UIDDMMYYYYformat(dr["sims_news_publish_date"].ToString());
                                obj.publish_status = true;
                            }
                            else
                            {
                                obj.sims_news_publish_date = db.UIDDMMYYYYformat(dr["sims_news_publish_date"].ToString());
                                obj.publish_status = false;
                            }

                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) < DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) == DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "N";
                            if ((Convert.ToDateTime(dr["sims_news_publish_date"].ToString()) > DateTime.Now.Date) && (Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) >= DateTime.Now.Date))
                                obj.sims_news_pstatus = "Y";
                            if ((Convert.ToDateTime(dr["sims_news_expiry_date"].ToString()) < DateTime.Now.Date))
                                obj.sims_news_pstatus = "E";

                            if (string.IsNullOrEmpty(dr["sims_news_expiry_date"].ToString()))
                                obj.sims_news_expiry_date = null;
                            else
                                obj.sims_news_expiry_date = db.UIDDMMYYYYformat(dr["sims_news_expiry_date"].ToString());

                            obj.sims_news_title = dr["sims_news_title"].ToString();
                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();

                            if (string.IsNullOrEmpty(obj.sims_news_file_path1_en))
                                obj.sims_news_file_path1_en = obj.sims_news_file_path1;
                            if (string.IsNullOrEmpty(obj.sims_news_file_path2_en))
                                obj.sims_news_file_path2_en = obj.sims_news_file_path2;
                            if (string.IsNullOrEmpty(obj.sims_news_file_path3_en))
                                obj.sims_news_file_path3_en = obj.sims_news_file_path3;

                            obj.sims_news_type = dr["news_type"].ToString();
                            obj.sims_news_category = dr["news_category"].ToString();
                            obj.sims_news_created_user = dr["user_name1"].ToString();
                            obj.sims_display_order = dr["sims_news_display_order"].ToString();
                            obj.sims_news_ifalert = "N";

                            obj.sims_news_gradesections = dr["sims_news_section"].ToString();
                            obj.sims_news_grade = dr["sims_news_grade"].ToString();
                            obj.sims_news_desg = dr["sims_news_desg"].ToString();
                            obj.sims_news_dept = dr["sims_news_dept"].ToString();

                            obj.enablenews = isenable;
                            obj.sims_news_publish_date = db.UIDDMMYYYYformat(dr["sims_news_publish_date"].ToString());
                            obj.sims_news_expiry_date = db.UIDDMMYYYYformat(dr["sims_news_expiry_date"].ToString());

                            #region News Status

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_proc",
                                    new List<SqlParameter>() 
                                     { 
                                        new SqlParameter("@opr", "D"),
                                        new SqlParameter("@sims_news_number", obj.sims_news_number),
                                     });
                                //string str=null;
                                if (dr1.Read())
                                {
                                    int active_cnt = int.Parse(dr1[0].ToString());
                                    if (active_cnt > 0)
                                        obj.sims_news_status = true;
                                    else if (active_cnt <= 0)
                                        obj.sims_news_status = false;
                                }
                                else
                                {
                                    obj.sims_news_status = false;
                                }
                                obj.sims_checkstatus = obj.sims_news_status;
                            }
                            #endregion

                            obj.lst_desg_news = GetDesignationNames(obj.sims_news_number);
                            obj.lst_dept_news = GetDepartmentNames(obj.sims_news_number);
                            obj.lst_grade_news = GetgradeNames(obj.sims_news_number);
                            obj.lst_section_news = GetsectionNames(obj.sims_news_number);

                            if (obj.sims_news_status == false)
                                obj.sims_news_pstatus = "Y";
                            obj.lst_news_groups = getNewsUserGroup1(obj.sims_news_number);
                            obj.news_doc_lst = new List<news_docs>();
                            obj.news_doc_lst = GetNewsDocs(obj.sims_news_number);

                            lst_news.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
        }

        [Route("getUpdateNewsViewRights")]
        public HttpResponseMessage getUpdateNewsViewRights(Comn015 comnobj, string loggeduser)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUpdateNewsViewRights(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool updated = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@sims_news_number", comnobj.sims_news_number),
                                new SqlParameter("@sims_news_user_group_code", ""),
                                new SqlParameter("@username", loggeduser),
                                new SqlParameter("@sims_news_status", comnobj.sims_news_status == true?"A":"I"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            updated = true;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, updated);
                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, updated);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("getNewsUserGroup")]
        public HttpResponseMessage getNewsUserGroup(Comn015 comnobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNewsUserGroup(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_newsusergrps = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@sims_news_number", comnobj.sims_news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_news_user_group = dr["comn_user_group_name"].ToString();
                            lst_newsusergrps.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_newsusergrps);
        }

        private List<string> getNewsUserGroup1(string news_number)
        {
            List<string> lst_newsusergrps = new List<string>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "F"),
                             new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //sims_news_user_group = dr["comn_user_group_name"].ToString();
                            lst_newsusergrps.Add(dr["comn_user_group_code"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_newsusergrps;
        }

        [Route("getAllNewsUserGroup")]
        public HttpResponseMessage getAllNewsUserGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllNewsUserGroup(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Comn015> lst_newsusergrps = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "H"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 simsobj = new Comn015();
                            simsobj.sims_news_user_group = dr["comn_user_group_name"].ToString();
                            simsobj.sims_news_user_group_code = dr["comn_user_group_code"].ToString();
                            lst_newsusergrps.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_newsusergrps);
        }

        /* still 3 methods are remaining */

        [Route("DNews")]
        public HttpResponseMessage DNews(Comn015 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : DNews()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        message.strMessage = "News Deleted Successfully.";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                    else
                    {
                        message.strMessage = "News Not Deleted.";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("UNews")]
        public HttpResponseMessage UNews(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UNews(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_news_ifalert;
            if (simsobj.sims_news_desg == "undefined")
                simsobj.sims_news_desg = "";
            if (simsobj.sims_news_dept == "undefined")
                simsobj.sims_news_dept = "";
            if (simsobj.sims_news_gradesections == "undefined")
                simsobj.sims_news_gradesections = "";

            // simsobj.sims_news_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),                                
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_news_publish_date", db.DBYYYYMMDDformat( simsobj.sims_news_publish_date)),
                                new SqlParameter("@sims_news_expiry_date",db.DBYYYYMMDDformat( simsobj.sims_news_expiry_date)),
                                new SqlParameter("@sims_news_title", simsobj.sims_news_title),
                                new SqlParameter("@sims_news_short_desc", simsobj.sims_news_short_desc),
                                new SqlParameter("@sims_news_desc", simsobj.sims_news_desc),
                                new SqlParameter("@sims_news_file_path1", simsobj.sims_news_file_path1),
                                new SqlParameter("@sims_news_file_path2", simsobj.sims_news_file_path2),
                                new SqlParameter("@sims_news_file_path3", simsobj.sims_news_file_path3),
                                new SqlParameter("@sims_news_file_path1_en", simsobj.sims_news_file_path1_en),
                                new SqlParameter("@sims_news_file_path2_en", simsobj.sims_news_file_path2_en),
                                new SqlParameter("@sims_news_file_path3_en", simsobj.sims_news_file_path3_en),
                                new SqlParameter("@sims_news_type", simsobj.sims_news_type),
                                new SqlParameter("@sims_news_category", simsobj.sims_news_category),
                                new SqlParameter("@sims_news_created_user_code", simsobj.sims_news_created_user),
                                new SqlParameter("@sims_news_display_order", simsobj.sims_display_order),
                                new SqlParameter("@mod_code", "001"),
                                new SqlParameter("@appl_code", "Com015"),
                                new SqlParameter("@appl_form_field", "Setup"),
                                new SqlParameter("@appl_field_type", "News Type"),
                                new SqlParameter("@appl_field_category", "News Category"),
                                new SqlParameter("@gradesections", simsobj.sims_news_gradesections),
                                new SqlParameter("@desg", simsobj.sims_news_desg),
                                new SqlParameter("@dept", simsobj.sims_news_dept)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Updated.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        if (inserted)
                        {
                            //Delete all usercode in sims_news_view_rights table.
                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() {
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                                });

                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }
                            }
                        }

                        if (inserted)
                        {
                            //Code for multiple usergroups.
                            foreach (string str in simsobj.lst_news_groups)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>()
                                     {
                                        new SqlParameter("@opr", "I"),
                                        new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                                        new SqlParameter("@sims_news_user_group_code", str),
                                        new SqlParameter("@sims_news_ifalert", ifalert),
                                        new SqlParameter("@sims_news_status", simsobj.sims_news_status == true?"A":"I"),
                                        new SqlParameter("@gradesections", simsobj.sims_news_gradesections),
                                        new SqlParameter("@desg", simsobj.sims_news_desg),
                                        new SqlParameter("@dept", simsobj.sims_news_dept)
                                     });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                }
                            }

                            if (inserted)
                            {
                                message.strMessage = "News Updated Successfully.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.OK, message);
                            }
                            else
                            {
                                message.strMessage = "Error In Parsing Information.";
                                message.messageType = MessageType.Error;
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating News Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UpdateNewsPublishDate")]
        public HttpResponseMessage UpdateNewsPublishDate(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsPublishDate(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_news_ifalert;
            // simsobj.sims_news_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A4"),                                
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),                                
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                        if (inserted)
                        {
                            message.strMessage = "The Selected News Published Today.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Error In Parsing Information.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating News Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UpdateNewsStatus")]
        public HttpResponseMessage UpdateNewsStatus(Comn015 simsobj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsStatus(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string ifalert = simsobj.sims_news_ifalert;
            // simsobj.sims_news_number = "0008";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_news_number", simsobj.sims_news_number),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Updated.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        if (inserted)
                        {
                            message.strMessage = "News Unpublished Successfully.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.OK, message);
                        }
                        else
                        {
                            message.strMessage = "Error In Parsing Information.";
                            message.messageType = MessageType.Error;
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating News Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getUserDetails")]
        public HttpResponseMessage getUserDetails(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserDetails(),PARAMETERS :: :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));
            Comn005 UserDet = new Comn005();

            //List<Comn005> lst_UserDet = new List<Comn005>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@comn_user_name", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            UserDet.User_Name = dr["comn_user_name"].ToString();
                            UserDet.User_Code = dr["comn_user_code"].ToString();
                            UserDet.Email_id = dr["Mail_Id"].ToString();
                            UserDet.Expiry_Date = dr["comn_user_expiry_date"].ToString();
                            UserDet.Group_Code = dr["comn_user_group_code"].ToString();
                            UserDet.Last_Login = dr["comn_user_last_login"].ToString();
                            UserDet.user_image = dr["user_image"].ToString();
                            UserDet.User_password = dr["comn_user_password"].ToString();
                            UserDet.UserAlertCount = dr["AlertCount"].ToString();
                            UserDet.UserCircularCount = dr["CircularCount"].ToString();
                            UserDet.UserNewsCount = dr["NewsCount"].ToString();
                            UserDet.messageCount = dr["MessageCount"].ToString();
                            UserDet.Designation = dr["Desig"].ToString();
                            UserDet.isAdmin = dr["ADMINuser"].ToString().Equals("1");
                            string[] str = UserDet.Designation.Split(' ');
                            if (str.Length > 1)
                                UserDet.DesigLtr = str[0][0] + "" + str[1][0];
                            else
                                UserDet.DesigLtr = str[0][0] + "";

                            //lst_UserDet.Add(UserDet);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, UserDet);
        }

        [Route("GetUserNews")]
        public HttpResponseMessage GetUserNews(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserNews(),PARAMETERS :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

            List<Comn015> lst_news = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_news_number", ""),
                                new SqlParameter("@sims_academic_year", ""),
                                new SqlParameter("@sims_cur_code", ""),
                                new SqlParameter("@sims_news_date", ""),
                                new SqlParameter("@sims_news_publish_date", ""),
                                new SqlParameter("@sims_news_expiry_date", ""),
                                new SqlParameter("@sims_news_title", ""),
                                new SqlParameter("@sims_news_short_desc", ""),
                                new SqlParameter("@sims_news_desc", ""),
                                new SqlParameter("@sims_news_file_path1", ""),
                                new SqlParameter("@sims_news_file_path2", ""),
                                new SqlParameter("@sims_news_file_path3", ""),
                                new SqlParameter("@sims_news_type", ""),
                                new SqlParameter("@sims_news_category", ""),
                                new SqlParameter("@sims_news_created_user_code", ""),
                                new SqlParameter("@username", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_news_number = dr["sims_news_number"].ToString();
                            // obj.sims_news_user_status = dr["UserCStatus"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
                                // obj.sims_news_date = Convert.ToDateTime(dr["sims_news_date"].ToString());
                                obj.sims_news_date = dr["sims_news_date"].ToString();

                            if (dr["sims_news_publish_date"].ToString() == "")
                            {
                                obj.sims_news_publish_date = null;
                                obj.publish_status = true;
                            }
                            else
                            {
                                // obj.sims_news_publish_date = Convert.ToDateTime(dr["sims_news_publish_date"].ToString());
                                obj.sims_news_publish_date = dr["sims_news_publish_date"].ToString();
                                obj.publish_status = false;
                            }

                            obj.sims_news_title = dr["sims_news_title"].ToString();
                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();
                            obj.sims_news_category = dr["sims_news_category"].ToString();

                            if (dr["sims_news_status"].ToString().Equals("A"))
                                obj.sims_news_status = true;
                            else
                                obj.sims_news_status = false;

                            lst_news.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst_news);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
        }

        [Route("GetUserNewsBYDate")]
        public HttpResponseMessage GetUserNewsBYDate(string username, string fromDate, string toDate)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserNewsBYDate(),PARAMETERS :: UserName{2}";
            Log.Debug(string.Format(debug, "ERP", "SETUP", username));

            List<Comn015> lst_news = new List<Comn015>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "K"),
                                new SqlParameter("@sims_news_number", ""),
                                new SqlParameter("@sims_academic_year", ""),
                                new SqlParameter("@sims_cur_code", ""),
                                new SqlParameter("@sims_news_date", ""),
                                new SqlParameter("@sims_news_publish_date", ""),
                                new SqlParameter("@sims_news_expiry_date", ""),
                                new SqlParameter("@sims_news_title", ""),
                                new SqlParameter("@sims_news_short_desc", ""),
                                new SqlParameter("@sims_news_desc", ""),
                                new SqlParameter("@sims_news_file_path1", ""),
                                new SqlParameter("@sims_news_file_path2", ""),
                                new SqlParameter("@sims_news_file_path3", ""),
                                new SqlParameter("@sims_news_type", ""),
                                new SqlParameter("@sims_news_category", ""),
                                new SqlParameter("@sims_news_created_user_code", "01"),
                                new SqlParameter("@username", username),
                                new SqlParameter("@from_date",fromDate.Equals("undefined")?null:fromDate),
                                new SqlParameter("@to_date", toDate.Equals("undefined")?null:toDate)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn015 obj = new Comn015();
                            obj.sims_news_number = dr["sims_news_number"].ToString();
                            obj.sims_news_user_status = dr["UserCStatus"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_news_date"].ToString()))
                                // obj.sims_news_date = Convert.ToDateTime(dr["sims_news_date"].ToString());
                                obj.sims_news_date = dr["sims_news_date"].ToString();

                            if (dr["sims_news_publish_date"].ToString() == "")
                            {
                                obj.sims_news_publish_date = null;
                                obj.publish_status = true;
                            }
                            else
                            {
                                // obj.sims_news_publish_date = Convert.ToDateTime(dr["sims_news_publish_date"].ToString());
                                obj.sims_news_publish_date = dr["sims_news_publish_date"].ToString();
                                obj.publish_status = false;
                            }

                            obj.sims_news_title = dr["sims_news_title"].ToString();
                            obj.sims_news_short_desc = dr["sims_news_short_desc"].ToString();
                            obj.sims_news_desc = dr["sims_news_desc"].ToString();
                            obj.sims_news_file_path1 = dr["sims_news_file_path1"].ToString();
                            obj.sims_news_file_path2 = dr["sims_news_file_path2"].ToString();
                            obj.sims_news_file_path3 = dr["sims_news_file_path3"].ToString();
                            obj.sims_news_file_path1_en = dr["sims_news_file_path1_en"].ToString();
                            obj.sims_news_file_path2_en = dr["sims_news_file_path2_en"].ToString();
                            obj.sims_news_file_path3_en = dr["sims_news_file_path3_en"].ToString();
                            obj.sims_news_category = dr["sims_news_category"].ToString();

                            if (dr["sims_news_status"].ToString().Equals("A"))
                                obj.sims_news_status = true;
                            else
                                obj.sims_news_status = false;

                            lst_news.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst_news);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_news);
        }

        [Route("UpdateNewsAsRead")]
        public HttpResponseMessage UpdateNewsAsRead(string news_number, string loggeduser)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsAsRead(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_view_rights_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),                                
                                new SqlParameter("@sims_news_number", news_number),
                                new SqlParameter("@sims_news_user_group_code", ""),
                                new SqlParameter("@username", loggeduser),
                                new SqlParameter("@sims_news_status", "A"),
                               
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "Error In Updating News Information";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        //----------------------------------------------------------------------

        private List<string> GetDesignationNames(string news_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_designames = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "V"),
                                new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_designames.Add(dr["sims_desg"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_designames;
        }

        private List<string> GetDepartmentNames(string news_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_dept = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "W"),
                                new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_dept.Add(dr["sims_dept"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_dept;
        }

        private List<string> GetgradeNames(string news_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_grd = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_grd.Add(dr["sims_gradecode"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_grd;
        }

        private List<string> GetsectionNames(string news_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<string> lst_section = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "O"),
                                new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //CommonUserControlClass objNew = new CommonUserControlClass();
                            // objNew.designation = dr["sims_desg"].ToString();
                            lst_section.Add(dr["grsec"].ToString());
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_section;
        }

        [Route("SaveNewsDocs")]
        public HttpResponseMessage SaveNewsDocs(string news_number, string original_filename, string updatedfilename)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : UpdateNewsAsRead(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A5"),                                
                                new SqlParameter("@sims_news_number", news_number),
                                new SqlParameter("@sims_file_path", updatedfilename),
                                new SqlParameter("@sims_file_path_en", original_filename),
                               
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        private List<news_docs> GetNewsDocs(string news_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationNames()";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<news_docs> lst_docs = new List<news_docs>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A7"),
                                new SqlParameter("@sims_news_number", news_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            news_docs nd = new news_docs();
                            nd.sims_news_originalfile = dr["sims_file_path_en"].ToString();
                            nd.sims_news_newfile = dr["sims_file_path"].ToString();
                            lst_docs.Add(nd);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return lst_docs;
        }

    }
}

