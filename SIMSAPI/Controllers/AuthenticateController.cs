﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models;
using SIMSAPI.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace SIMSAPI.Controllers
{
    [RoutePrefix("api/authenticate")]
    public class AuthenticateController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public IHttpActionResult Post([FromBody]User user)
        {
            Log.Debug("GET Request traced");
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    var param = new List<SqlParameter>() 
                    { 
                        new SqlParameter("@lic_school_code", CommonStaticClass.school_code),
                        new SqlParameter("@UserName", user.Username ),
                        new SqlParameter("@Password", user.Password)
                    };
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[CheckLogin_proc]", param);

                    string result = string.Empty;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = dr["Msg"].ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(result) && result.Equals("True"))
                    {
                        return Ok(new
                        {
                            success = true
                        });
                    }
                    else
                    {
                        return Unauthorized();


                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return InternalServerError(ex);
            }
        }




        [Route("GetGroups")]
        public HttpResponseMessage GetGroups()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGroups(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "PP", "REPORTCARD"));

            List<Uccw042> grp = new List<Uccw042>();
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'A')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw042 pc = new Uccw042();
                            pc.comn_user_group = dr["comn_user_group_name"].ToString();
                            pc.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            grp.Add(pc);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grp);
            }
            return Request.CreateResponse(HttpStatusCode.OK, grp);
        }



        [Route("GetGroupsNew")]
        public HttpResponseMessage GetGroupsNew()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetGroups(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "PP", "REPORTCARD"));

            List<Uccw042> grp = new List<Uccw042>();
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr","AA")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw042 pc = new Uccw042();
                            pc.comn_user_group = dr["comn_user_group_name"].ToString();
                            pc.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            grp.Add(pc);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grp);
            }
            return Request.CreateResponse(HttpStatusCode.OK, grp);
        }




        [Route("GetUserName")]
        public HttpResponseMessage GetUserName(string strEmailID, string strCode, string strgroupname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserName(),PARAMETERS :: strEmailID{2},strCode{3},strgroupname{4}";
            Log.Debug(string.Format(debug, "PP", "GetUserName", strEmailID, strCode, strgroupname));

            List<Uccw042> grp = new List<Uccw042>();
            string strUserName = string.Empty;
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'S'),
                           new SqlParameter("@comn_user_email", strEmailID),
                           new SqlParameter("@EEcode", strCode),
                           new SqlParameter("@groupCode", strgroupname)
                    });
                    // if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            strUserName = dr["comn_user_name"].ToString();
                        }
                        else
                        {
                            strUserName = "false";
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, strUserName);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strUserName);
        }

        [Route("getResetPassword")]
        public HttpResponseMessage getResetPassword(string strUsername, string strSecurityCode, string strSecurityAns)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ResetPassword(),PARAMETERS :: strUsername{2},strSecurityCode{3},strSecurityAns{4}";
            Log.Debug(string.Format(debug, "PP", "ResetPassword", strUsername, strSecurityCode, strSecurityAns));

            List<Uccw042> grp = new List<Uccw042>();
            int cnt = 0;
            string newpassword = string.Empty;
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'B'),
                           new SqlParameter("@comn_user_name", strUsername),
                           new SqlParameter("@comn_user_secret_question_code", strSecurityCode),
                           new SqlParameter("@comn_user_secret_answer", strSecurityAns)
                    });
                    //  if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            cnt = int.Parse(dr["cnt"].ToString());
                        }
                        if (cnt >= 1)
                        {
                            //Update Password
                            // newpassword = Membership.GeneratePassword(7, 0);
                            try
                            {
                                DBConnection db1 = new DBConnection();
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("[dbo].[GeneratePasswords]", new List<SqlParameter>()
                                    {
                                       new SqlParameter("@opr",'U'),
                                       new SqlParameter("@user_code", strUsername)
                                    });
                                    if (dr1.Read())
                                    {
                                        int str = dr1["emailPwd"].ToString().IndexOf('#');
                                        newpassword = dr1["emailPwd"].ToString().Substring(str + 1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex);
                                //throw;
                            }
                        }
                        else
                            newpassword = "false";
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, newpassword);
            }
            return Request.CreateResponse(HttpStatusCode.OK, newpassword);
        }

        [Route("GetSecurityQuestion")]
        public HttpResponseMessage GetSecurityQuestion(string strUsername)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSecurityQuestion(),PARAMETERS :: strUsername{2}";
            Log.Debug(string.Format(debug, "PP", "GetSecurityQuestion", strUsername));

            List<Uccw042> grp = new List<Uccw042>();
            string strSecurityQuestion = string.Empty;

            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'C'),
                           new SqlParameter("@comn_user_name", strUsername)
                    });
                    // if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            strSecurityQuestion = string.Format("{0}~{1}", dr["Question"].ToString(), dr["sims_appl_parameter"].ToString());
                        }
                        else
                        {
                            strSecurityQuestion = string.Empty;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, strSecurityQuestion);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strSecurityQuestion);
        }




        [Route("GetEmail")]
        public HttpResponseMessage GetEmail(string strUsername)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSecurityQuestion(),PARAMETERS :: strUsername{2}";
            Log.Debug(string.Format(debug, "PP", "GetSecurityQuestion", strUsername));

            List<Uccw042> grp = new List<Uccw042>();
            string strSecurityQuestion = string.Empty;

            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_erp_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'E'),
                           new SqlParameter("@comn_user_name", strUsername)
                    });
                    // if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            strSecurityQuestion = dr["email"].ToString();
                        }
                        else
                        {
                            strSecurityQuestion = string.Empty;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, strSecurityQuestion);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strSecurityQuestion);
        }



        [Route("GetMobile")]
        public HttpResponseMessage GetMobile(string strUsername)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSecurityQuestion(),PARAMETERS :: strUsername{2}";
            Log.Debug(string.Format(debug, "PP", "GetSecurityQuestion", strUsername));

            List<Uccw042> grp = new List<Uccw042>();
            string strSecurityQuestion = string.Empty;

            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_erp_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'E'),
                           new SqlParameter("@comn_user_name", strUsername)
                    });
                    // if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            strSecurityQuestion = dr["mobile"].ToString();
                        }
                        else
                        {
                            strSecurityQuestion = string.Empty;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, strSecurityQuestion);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strSecurityQuestion);
        }



        [Route("GetSendEmail")]
        public HttpResponseMessage GetSendEmail(string strUsername, string email)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSecurityQuestion(),PARAMETERS :: strUsername{2}";
            Log.Debug(string.Format(debug, "PP", "GetSecurityQuestion", strUsername));

            List<Uccw042> grp = new List<Uccw042>();
            string strSecurityQuestion = string.Empty;

            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_forgot_password_erp_proc", new List<SqlParameter>()
                    {
                           new SqlParameter("@opr",'F'),
                           new SqlParameter("@comn_user_name", strUsername),
                           new SqlParameter("@sims_admission_parent_email", email),
                           //new SqlParameter("@comn_user_name", strUsername)


                    });
                    // if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            strSecurityQuestion = dr["email"].ToString();
                        }
                        else
                        {
                            strSecurityQuestion = string.Empty;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, strSecurityQuestion);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strSecurityQuestion);
        }

        [Route("changePassword")]
        public HttpResponseMessage changePassword(string strUsername, string newpassword)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ResetPassword(),PARAMETERS :: strUsername{2}";
            Log.Debug(string.Format(debug, "PP", "ResetPassword", strUsername));

            bool ins = false;

            int cnt = 0;

            //Update Password
            // newpassword = Membership.GeneratePassword(7, 0);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[comn_user_change_password]", new List<SqlParameter>()
                                    {
                                       new SqlParameter("@opr",'R'),
                                       new SqlParameter("@comn_user_name", strUsername),
                                       new SqlParameter("@comn_user_new_password", newpassword)

                                    });
                    if (dr1.RecordsAffected > 0)
                    {
                        ins = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                //throw;
            }





            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }



    }
}
