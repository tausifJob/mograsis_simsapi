﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/GratuityCriteria")]
    public class GratuityCriteriaController : ApiController
    {

        [Route("Get_All_pays_grade")]
        public HttpResponseMessage Get_All_pays_grade()
        {

            List<Pers323> list = new List<Pers323>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_criteria_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers323 obj = new Pers323();
                            obj.gr_code = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAll_Account_Names")]
        public HttpResponseMessage GetAll_Account_Names(string comp_cd)
        {


            List<Finn003> mod_list = new List<Finn003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@comp_code",comp_cd),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn003 finnobj = new Finn003();
                            finnobj.glac_acct_code = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString();
                            finnobj.glac_acct_name = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString() + "_" + dr["glma_acct_name"].ToString();
                            finnobj.comp_inter_comp_acct = dr["glma_acct_name"].ToString();
                            finnobj.comp_against_inter_comp_acct = dr["glma_acct_name"].ToString();
                            mod_list.Add(finnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Get_paysGratuityCriteria")]
        public HttpResponseMessage Get_paysGratuityCriteria()
        {

            List<Pers323> list = new List<Pers323>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_criteria_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers323 obj = new Pers323();
                            // obj.srno = Convert.ToInt32(dr["srno"].ToString());
                            obj.grt_criteria_code = dr["grt_criteria_code"].ToString();
                            obj.grt_criteria_name = dr["grt_criteria_name"].ToString();
                            obj.grt_criteria_min_days = Convert.ToInt32(dr["grt_criteria_min_days"].ToString().Trim());
                            //if (obj.grt_criteria_min_days != null)
                            //{
                            obj.year = (obj.grt_criteria_min_days / 365);
                            obj.month = (((obj.grt_criteria_min_days) % 365) / 30);
                            obj.days = (((obj.grt_criteria_min_days) % 365) % 30);
                            //}
                            if (dr["grt_criteria_max_days"].ToString() != "-")
                            {
                                obj.grt_criteria_max_days = Convert.ToInt32(dr["grt_criteria_max_days"].ToString().Trim());
                            }
                            
                            obj.grt_working_days = dr["grt_working_days"].ToString();
                            
                            obj.grt_pay_grade = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();
                            obj.grt_acct_code = dr["accnt_code"].ToString();

                            if (dr["accnt_code"].ToString() != "_")
                            {
                                obj.grt_acct_name = dr["grt_acct_code"].ToString();
                            }

                            obj.grt_bank_acct_code = dr["bank_accnt_code"].ToString();
                            if (dr["bank_accnt_code"].ToString() != "_")
                            {
                                obj.grt_bank_acct_name = dr["grt_bank_acct_code"].ToString();
                            }

                            obj.grt_cash_acct_code = dr["cash_accnt_code"].ToString();
                            if (dr["cash_accnt_code"].ToString() != "_")
                            {
                                obj.grt_cash_acct_name = dr["grt_cash_acct_code"].ToString();
                            }

                            obj.grt_airfare_acct_code = dr["airfare_acct_code"].ToString();
                            if (dr["airfare_acct_code"].ToString() != "_")
                            {
                                obj.grt_airfare_acct_name = dr["grt_airfare_acct_code"].ToString();
                            }

                            obj.grt_prov_acct_code = dr["provision_acct_code"].ToString();
                            if (dr["provision_acct_code"].ToString() != "_")
                            {
                                obj.grt_prov_acct_name = dr["grt_provision_acct_code"].ToString();
                            }

                            if (dr["grt_status"].ToString() == "1")
                            {
                                obj.grt_status = true;
                            }
                            else
                            {
                                obj.grt_status = false;
                            }
                            obj.grt_formula_code = dr["grt_formula_code"].ToString();
                            obj.grt_criteria_order = dr["grt_criteria_order"].ToString();

                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CUDInsertpaysGratuityCriteria")]
        public HttpResponseMessage CUDInsertpaysGratuityCriteria(List<Pers323> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers323 hrmsobj in data)
                    {
                        if (hrmsobj.grt_pay_grade == "undefined")
                        {
                            hrmsobj.grt_pay_grade = null;
                        }
                        if (hrmsobj.grt_acct_code == "undefined")
                        {
                            hrmsobj.grt_acct_code = null;
                        }
                        if (hrmsobj.grt_bank_acct_code == "undefined")
                        {
                            hrmsobj.grt_bank_acct_code = null;
                        }
                        if (hrmsobj.grt_cash_acct_code == "undefined")
                        {
                            hrmsobj.grt_cash_acct_code = null;
                        }
                        if (hrmsobj.grt_airfare_acct_code == "undefined")
                        {
                            hrmsobj.grt_airfare_acct_code = null;
                        }
                        if (hrmsobj.grt_prov_acct_code == "undefined")
                        {
                            hrmsobj.grt_prov_acct_code = null;
                        }
                        if (hrmsobj.grt_formula_code == "undefined")
                        {
                            hrmsobj.grt_formula_code = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_criteria_proc]",
                                 new List<SqlParameter>()
                              {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@grt_criteria_code", hrmsobj.grt_criteria_code),
                                    new SqlParameter("@grt_criteria_name", hrmsobj.grt_criteria_name),
                                    new SqlParameter("@grt_criteria_min_days", hrmsobj.grt_criteria_min_days),
                                    new SqlParameter("@grt_pay_grade", hrmsobj.grt_pay_grade),
                                    new SqlParameter("@grt_acct_code", hrmsobj.grt_acct_code),
                                    new SqlParameter("@grt_bank_acct_code", hrmsobj.grt_bank_acct_code),
                                    new SqlParameter("@grt_cash_acct_code", hrmsobj.grt_cash_acct_code),
                                    new SqlParameter("@grt_airfare_acct_code", null),
                                    new SqlParameter("@grt_provision_acct_code", hrmsobj.grt_prov_acct_code),
                                    new SqlParameter("@grt_status", hrmsobj.grt_status?"1":"0"),
                                    new SqlParameter("@grt_formula_code", hrmsobj.grt_formula_code),
                                    new SqlParameter("@grt_criteria_max_days", hrmsobj.grt_criteria_max_days),
                                    new SqlParameter("@grt_working_days", hrmsobj.grt_working_days)
                             });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CUDDeleteGratuityCrite")]
        public HttpResponseMessage CUDDeleteGratuityCrite(List<Pers323> data)
        {

            string strMessage = "";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers323 hrmsobj in data)
                    {
                        if (hrmsobj.grt_pay_grade == "undefined")
                        {
                            hrmsobj.grt_pay_grade = null;
                        }
                        if (hrmsobj.grt_acct_code == "undefined")
                        {
                            hrmsobj.grt_acct_code = null;
                        }
                        if (hrmsobj.grt_bank_acct_code == "undefined")
                        {
                            hrmsobj.grt_bank_acct_code = null;
                        }
                        if (hrmsobj.grt_cash_acct_code == "undefined")
                        {
                            hrmsobj.grt_cash_acct_code = null;
                        }
                        if (hrmsobj.grt_airfare_acct_code == "undefined")
                        {
                            hrmsobj.grt_airfare_acct_code = null;
                        }
                        if (hrmsobj.grt_prov_acct_code == "undefined")
                        {
                            hrmsobj.grt_prov_acct_code = null;
                        }
                        if (hrmsobj.grt_formula_code == "undefined")
                        {
                            hrmsobj.grt_formula_code = null;
                        }
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_criteria_proc]",

                                 new List<SqlParameter>()
                              {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@grt_criteria_code", hrmsobj.grt_criteria_code),
                                    new SqlParameter("@grt_criteria_name", hrmsobj.grt_criteria_name),
                                    new SqlParameter("@grt_criteria_min_days", hrmsobj.grt_criteria_min_days),
                                    new SqlParameter("@grt_pay_grade", hrmsobj.grt_pay_grade),
                                    new SqlParameter("@grt_acct_code", hrmsobj.grt_acct_code),
                                    new SqlParameter("@grt_bank_acct_code", hrmsobj.grt_bank_acct_code),
                                    new SqlParameter("@grt_cash_acct_code", hrmsobj.grt_cash_acct_code),
                                    new SqlParameter("@grt_airfare_acct_code", hrmsobj.grt_airfare_acct_code),
                                    new SqlParameter("@grt_provision_acct_code", hrmsobj.grt_prov_acct_code),
                                    new SqlParameter("@grt_status", hrmsobj.grt_status?"1":"0"),
                                    new SqlParameter("@grt_formula_code", hrmsobj.grt_formula_code)
                             });

                        if (dr.Read())
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        else
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        dr.Close();

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, strMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strMessage);
        }

        [Route("CUDUpDelpaysGratuityCriteria")]
        public HttpResponseMessage CUDUpDelpaysGratuityCriteria(List<Pers323> data)
        {

            Message message = new Message();
            bool inserted = false;
            string cnt = "3";
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers323 hrmsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_criteria_proc]",
                                new List<SqlParameter>()
                             {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@srno", hrmsobj.srno),
                                    new SqlParameter("@grt_criteria_code", hrmsobj.grt_criteria_code),
                                    new SqlParameter("@grt_criteria_name", hrmsobj.grt_criteria_name),
                                    new SqlParameter("@grt_criteria_min_days", hrmsobj.grt_criteria_min_days),
                                    new SqlParameter("@grt_pay_grade", hrmsobj.grt_pay_grade),
                                    new SqlParameter("@grt_acct_code", hrmsobj.grt_acct_code),
                                    new SqlParameter("@grt_bank_acct_code", hrmsobj.grt_bank_acct_code),
                                    new SqlParameter("@grt_cash_acct_code", hrmsobj.grt_cash_acct_code),
                                    new SqlParameter("@grt_airfare_acct_code", hrmsobj.grt_airfare_acct_code),
                                    new SqlParameter("@grt_provision_acct_code", hrmsobj.grt_prov_acct_code),
                                    new SqlParameter("@grt_status", hrmsobj.grt_status),
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDInsertGratuityCalculation")]
        public HttpResponseMessage CUDInsertGratuityCalculation(List<Pers324> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers324 simsobj in data)
                    {
                        if (simsobj.grt_applicable_days == "undefined")
                        {
                            simsobj.grt_applicable_days = null;
                        }
                        if (simsobj.grt_working_days == "undefined")
                        {
                            simsobj.grt_working_days = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_calc_proc]",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_criteria_proc]",
                                 new List<SqlParameter>()
                              {
                                    new SqlParameter("@opr", simsobj.opr),
                                    new SqlParameter("@grt_criteria_code", simsobj.grt_criteria_code),
                                    new SqlParameter("@grt_applicable_days", simsobj.grt_applicable_days),
                                    new SqlParameter("@grt_working_days", simsobj.grt_working_days),
                                    new SqlParameter("@grt_status", simsobj.grt_status == true?"1":"0"),
                             });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getpaysGratuityCalculation")]
        public HttpResponseMessage getpaysGratuityCalculation(string criteria_code)
        {

            List<Pers324> list = new List<Pers324>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_calc_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@grt_criteria_code", criteria_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers324 obj = new Pers324();
                            obj.grt_criteria_code = dr["grt_criteria_code"].ToString();
                            obj.grt_criteria_name = dr["grt_criteria_name"].ToString();
                            obj.grt_applicable_days = dr["grt_applicable_days"].ToString();
                            obj.grt_working_days = dr["grt_working_days"].ToString();
                            string chk = dr["grt_status"].ToString();
                            if (chk == "1")
                            {
                                obj.grt_status = true;
                            }
                            else
                            {
                                obj.grt_status = false;
                            }

                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

    }
}