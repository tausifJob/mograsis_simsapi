﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/EmployeeLeave")]
    public class EmployeeLeaveController : ApiController
    {
        [Route("Get_EmpInfo")]
        public HttpResponseMessage Get_EmpInfo(string emp_id)
        {
            List<Pers300_le> emp_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@empid", emp_id)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.dg_desc = dr["Designation"].ToString();
                            hrmsobj.dept_no = dr["dept_no"].ToString();
                            hrmsobj.codp_dept_name = dr["Department"].ToString();
                            hrmsobj.gr_desc = dr["Grade"].ToString();
                            hrmsobj.comp_code = dr["em_company_code"].ToString();
                            hrmsobj.img_name = dr["em_img"].ToString();
                            hrmsobj.ndays = string.Empty;
                            hrmsobj.updated = false;
                            try
                            {
                                hrmsobj.em_staff_type = dr["em_staff_type"].ToString();
                            }
                            catch(Exception e) { }

                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Get_EMPLeaveDetails")]
        public HttpResponseMessage Get_EMPLeaveDetails(string compcode, string employee_id)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y"),
                                 new SqlParameter("@le_company_code",compcode),
                                  new SqlParameter("@em_login_code", employee_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.leave_type = dr["cl_desc"].ToString();
                            obj.leave_code = dr["le_leave_code"].ToString();
                            obj.maxleave_allowed = dr["le_max_days_allowed"].ToString();

                            try
                            {
                                obj.pays_leave_doc_code = dr["pays_leave_doc_code"].ToString();
                                obj.pays_leave_doc_name = dr["pays_leave_doc_name"].ToString();
                                obj.pays_leave_doc_is_mandatory = dr["pays_leave_doc_is_mandatory"].ToString();
                            }
                            catch (Exception ex) { };
                            
                            empleave_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, empleave_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, empleave_list);
        }

        [Route("Get_EMPLeaveDetails_SISO")]
        public HttpResponseMessage Get_EMPLeaveDetails_SISO(string compcode, string employee_id)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_siso_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "Y"),
                                 new SqlParameter("@le_company_code",compcode),
                                  new SqlParameter("@em_login_code", employee_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.leave_type = dr["cl_desc"].ToString();
                            obj.leave_code = dr["le_leave_code"].ToString();
                            obj.maxleave_allowed = dr["le_max_days_allowed"].ToString();

                            try
                            {
                                obj.pays_leave_doc_code = dr["pays_leave_doc_code"].ToString();
                                obj.pays_leave_doc_name = dr["pays_leave_doc_name"].ToString();
                                obj.pays_leave_doc_is_mandatory = dr["pays_leave_doc_is_mandatory"].ToString();
                            }
                            catch (Exception ex) { };

                            empleave_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, empleave_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, empleave_list);
        }


        [Route("Get_EMPLeaveMaxDays")]
        public HttpResponseMessage Get_EMPLeaveMaxDays(string le_leave_code, string compcode, string employee_id)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            string leave_max = string.Empty;
            string total_leave_taken = string.Empty;
            double allowed_leaves = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Z"),
                                 new SqlParameter("@le_company_code",compcode),
                                  new SqlParameter("@lt_leave_code", le_leave_code),
                                  new SqlParameter("@lt_number", employee_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            allowed_leaves = Convert.ToDouble(dr["el_maximum_days"].ToString());                            
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, allowed_leaves);

            }
            return Request.CreateResponse(HttpStatusCode.OK, allowed_leaves);
        }

        [Route("Get_EMPLeaveMaxDays_SISO")]
        public HttpResponseMessage Get_EMPLeaveMaxDays_SISO(string le_leave_code, string compcode, string employee_id)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            string leave_max = string.Empty;
            string total_leave_taken = string.Empty;
            double allowed_leaves = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_siso_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "Z"),
                                 new SqlParameter("@le_company_code",compcode),
                                  new SqlParameter("@lt_leave_code", le_leave_code),
                                  new SqlParameter("@lt_number", employee_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            allowed_leaves = Convert.ToDouble(dr["el_maximum_days"].ToString());
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, allowed_leaves);

            }
            return Request.CreateResponse(HttpStatusCode.OK, allowed_leaves);
        }

        [Route("GetLeaveDetails")]
        public HttpResponseMessage GetLeaveDetails(string emp_code, string comp_code)
        {
            List<Pers300_le> leave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                 new SqlParameter("@lt_number", emp_code),
                                  new SqlParameter("@lt_company_code", comp_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            //DateTime.Parse(dr["lt_start_date"].ToString()).ToShortDateString();
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());  //DateTime.Parse(dr["lt_end_date"].ToString()).ToShortDateString();
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = db.UIDDMMYYYYformat(dr["lt_modified_on"].ToString()); //DateTime.Parse(dr["lt_modified_on"].ToString()).ToShortDateString();
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.updated = false;
                            hrmsobj.lt_no = dr["lt_no"].ToString();
                            leave_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, leave_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, leave_list);
        }

        [Route("CUDInsertEMPLeaveDetails")]
        public HttpResponseMessage CUDInsertEMPLeaveDetails(List<Pers300_le> data)
        {
            //bool inserted = false;
            string transacid = string.Empty;
            try
            {
                if (data != null)
                {
                    string date = DateTime.Now.ToString();
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers300_le obj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr",'I'),
                                    new SqlParameter("@lt_number", obj.em_login_code),
                                    new SqlParameter("@lt_company_code", obj.comp_code),
                                    new SqlParameter("@lt_leave_code", obj.leave_code),
                                    //new SqlParameter("@lt_start_date", DateTime.Parse(obj.lt_start_date)),
                                    //new SqlParameter("@lt_end_date",DateTime.Parse(obj.lt_end_date)),
                                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lt_start_date)),
                                    new SqlParameter("@lt_end_date",db.DBYYYYMMDDformat(obj.lt_end_date)),
                                    new SqlParameter("@lt_days", obj.lt_days),
                                    new SqlParameter("@lt_hours",obj.lt_hours),
                                    new SqlParameter("@lt_mgrctag", obj.lt_mgr_confirm_tag),
                                    new SqlParameter("@lt_modified_on",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                    new SqlParameter("@lt_remarks", obj.lt_remarks),
                                    new SqlParameter("@lt_converted_hours", null),
                                    new SqlParameter("@lt_request_date", DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                    new SqlParameter("@lt_substitute_employee", obj.lt_substitute_employee),
                                    new SqlParameter("@em_doc_path", obj.em_doc_path),
                                    new SqlParameter("@pays_leave_doc_code", obj.pays_leave_doc_code)
                            });
                            while (dr.Read())
                            {
                                transacid = dr["TRANSACID"].ToString();
                            }
                          
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, transacid);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, transacid);
        }

        [Route("CUDInsertEMPLeaveDetailsSISO")]
        public HttpResponseMessage CUDInsertEMPLeaveDetailsSISO(List<Pers300_le> data)
        {
            //bool inserted = false;
            string transacid = string.Empty;
            List<string> list_transacid = new List<string>();
            try
            {
                if (data != null)
                {
                    string date = DateTime.Now.ToString();
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers300_le obj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_siso_proc]",
                                new List<SqlParameter>()
                             {
                                    new SqlParameter("@opr",'I'),
                                    new SqlParameter("@lt_number", obj.em_login_code),
                                    new SqlParameter("@lt_company_code", obj.comp_code),
                                    new SqlParameter("@lt_leave_code", obj.leave_code),                                    
                                    //new SqlParameter("@lt_end_date",DateTime.Parse(obj.lt_end_date)),
                                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lt_start_date)),
                                    new SqlParameter("@lt_end_date",db.DBYYYYMMDDformat(obj.lt_end_date)),
                                    new SqlParameter("@lt_days", obj.lt_days),
                                    new SqlParameter("@lt_hours",obj.lt_hours),
                                    new SqlParameter("@lt_mgrctag", obj.lt_mgr_confirm_tag),
                                    new SqlParameter("@lt_modified_on",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                    new SqlParameter("@lt_remarks", obj.lt_remarks),
                                    new SqlParameter("@lt_converted_hours", null),
                                    new SqlParameter("@lt_request_date", DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                    new SqlParameter("@lt_substitute_employee", obj.lt_substitute_employee),
                                    new SqlParameter("@em_doc_path", obj.em_doc_path),
                                    new SqlParameter("@pays_leave_doc_code", obj.pays_leave_doc_code)
                                    
                            });
                            while (dr.Read())
                            {
                                transacid = dr["TRANSACID"].ToString();
                                list_transacid.Add(transacid);
                            }

                            dr.Close();
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, list_transacid);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list_transacid);
        }

        [Route("CUDInsert_WorkflowDetails")]
        public HttpResponseMessage CUDInsert_WorkflowDetails(string transaction_id, string dept_code)
        {
            bool inserted = false;
            try
            {
                if (transaction_id != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Insert_Sims_Workflow_proc]",
                         new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr",'S'),
                                    new SqlParameter("@sims_workflow_transactionid", transaction_id),
                                    new SqlParameter("@dept", dept_code),
                                    new SqlParameter("@sims_workflow_mod_code", "004"),
                                    new SqlParameter("@sims_workflow_appl_code", "Per300"),
                                    new SqlParameter("@sims_workflow_transdate", DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString())
                            });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDInsert_WorkflowDetails_DMC")]
        public HttpResponseMessage CUDInsert_WorkflowDetails_DMC(string transaction_id, string dept_code, string workflow_type)
        {
            bool inserted = false;
            try
            {
                if (transaction_id != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[Insert_Sims_Workflow_DMC_proc]",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@sims_workflow_transactionid", transaction_id),
                            new SqlParameter("@dept", dept_code),
                            new SqlParameter("@sims_workflow_mod_code", "004"),
                            new SqlParameter("@sims_workflow_appl_code", "Per300"),
                            new SqlParameter("@sims_workflow_transdate", DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString()),
                            new SqlParameter("@sims_workflow_type", workflow_type)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Get_Current_AcademicYear")]
        public HttpResponseMessage Get_Current_AcademicYear()
        {
            //List<Pers300_le> Year_lst = new List<Pers300_le>();
            Pers300_le currentYear = new Pers300_le();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "J"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //currentYear.currentystart = DateTime.Parse(dr["sims_financial_year_start_date"].ToString());
                            //currentYear.currentyend = DateTime.Parse(dr["sims_financial_year_end_date"].ToString());
                            //  Pers300_le currentYear = new Pers300_le();
                            currentYear.yearcurrentstart = db.UIDDMMYYYYformat(dr["sims_financial_year_start_date"].ToString());
                            currentYear.yearcurrentyend = db.UIDDMMYYYYformat(dr["sims_financial_year_end_date"].ToString());
                            // Year_lst.Add(currentYear);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, currentYear);

            }
            return Request.CreateResponse(HttpStatusCode.OK, currentYear);
        }

        [Route("Get_EMPAllLeaveDates")]
        public HttpResponseMessage Get_EMPAllLeaveDates(string compcode, string employee_id)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "O"),
                                 new SqlParameter("@em_login_code", employee_id),
                                  new SqlParameter("@lt_company_code", compcode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            obj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("GetStatusofTransactionforEmployee")]
        public HttpResponseMessage GetStatusofTransactionforEmployee(string transactionId)
        {
            List<Pers318_workflow> lst_workflow = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "X"),
                             new SqlParameter("@transaction_id",transactionId),
                             new SqlParameter("@workflow_mod_code", "004"),
                             new SqlParameter("@workflow_appl_code", "Per300"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow pw = new Pers318_workflow();
                            pw.EName = dr["Name"].ToString();
                            pw.Eorder = dr["Order1"].ToString();
                            string stat = string.Empty;
                            if (dr["Status1"].ToString() == "A")
                                stat = "Approved";
                            if (dr["Status1"].ToString() == "R")
                                stat = "Rejected";
                            if (dr["Status1"].ToString() == "P")
                                stat = "Pending";
                            if (dr["Status1"].ToString() == "W")
                                stat = "Pending";
                            pw.EStatus = stat;
                            try
                            {
                                pw.Edate = db.UIDDMMYYYYformat(dr["adate"].ToString());    //dr["adate"].ToString().Substring(0, dr["adate"].ToString().IndexOf(" "));
                            }
                            catch (Exception deE)
                            {
                                pw.Edate = "";
                            }
                            pw.sims_workflow_remark = dr["remark"].ToString();
                            
                            lst_workflow.Add(pw);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);
        }

        [Route("Get_RoleDetails")]
        public HttpResponseMessage Get_RoleDetails()
        {
            List<Pers300_le> role_lst = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le role = new Pers300_le();
                            role.sims_param_role_id = dr["sims_appl_parameter"].ToString();
                            role.sims_param_role_field = dr["sims_appl_form_field_value1"].ToString();
                            role_lst.Add(role);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, role_lst);

            }
            return Request.CreateResponse(HttpStatusCode.OK, role_lst);
        }

        [Route("Get_empRoleDetails")]
        public HttpResponseMessage Get_empRoleDetails(string emp_id)
        {
            List<Pers300_le> role_lst = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "N"),
                              new SqlParameter("@empid", emp_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le role = new Pers300_le();
                            role.sims_param_role_id = dr["comn_user_role_id"].ToString();
                            role_lst.Add(role);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, role_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, role_lst);
        }

        [Route("Get_empHolidayDetails")]
        public HttpResponseMessage Get_empHolidayDetails(string emp_id)
        {
            List<Pers300_le> role_lst = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "K"),
                              new SqlParameter("@lt_number", emp_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le role = new Pers300_le();
                            role.lt_start_date = db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
                            role.lt_end_date = db.UIDDMMYYYYformat(dr["sims_to_date"].ToString());
                            role.lt_status = dr["LeaveStatus"].ToString();
                            role_lst.Add(role);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, role_lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, role_lst);
        }

        [Route("GetLeaveInfo")]
        public HttpResponseMessage GetLeaveInfo(string compcode, string employee_id, string leave_code)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "LD"),
                        new SqlParameter("@le_company_code",compcode),
                        new SqlParameter("@em_login_code", employee_id),
                        new SqlParameter("@lt_leave_code", leave_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.el_number = dr["el_number"].ToString();                            
                            obj.el_days_taken = dr["el_days_taken"].ToString();
                            obj.cl_code = dr["cl_code"].ToString();
                            obj.cl_parent_id = dr["cl_parent_id"].ToString();
                            obj.cl_display_order = dr["cl_display_order"].ToString();
                            obj.el_maximum_days = dr["el_maximum_days"].ToString();

                            //obj.el_company_code = dr["el_company_code"].ToString();
                            //obj.el_year = dr["el_year"].ToString();
                            //obj.el_hrs_taken = dr["el_hrs_taken"].ToString();
                            //obj.cl_desc = dr["cl_desc"].ToString();
                            empleave_list.Add(obj);
                            
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, empleave_list);
        }


        [Route("GetCheckLeaveExists")]
        public HttpResponseMessage GetCheckLeaveExists(string emp_id, string lt_start_date, string lt_end_date)
        {
            List<Pers300_le> role_lst = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","N"),
                        new SqlParameter("@lt_number", emp_id),
                        new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(lt_start_date)),
                        new SqlParameter("@lt_end_date", db.DBYYYYMMDDformat(lt_end_date))
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le role = new Pers300_le();                           
                            role.leave_exists = dr["leave_exists"].ToString();
                            role_lst.Add(role);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, role_lst);
        }


        [Route("GetSubstituteEmployee")]
        public HttpResponseMessage GetSubstituteEmployee(string comp_code,string emp_code)
        {
            List<Pers300_le> leave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "L"),
                            new SqlParameter("@lt_company_code", comp_code),
                            new SqlParameter("@lt_number", emp_code)                                
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["emp_name"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            leave_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, leave_list);
        }

        [Route("Get_AllowedLeaveStatus")]
        public HttpResponseMessage Get_AllowedLeaveStatus(string le_leave_code, string compcode, string employee_id)
        {
            List<Pers300_le> empleave_list = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "AD"),
                        new SqlParameter("@lt_leave_code", le_leave_code),
                        new SqlParameter("@le_company_code",compcode),
                        new SqlParameter("@lt_number", employee_id)                        
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();                          
                            obj.allowed_paid_leave = dr["cl_carry_forward"].ToString();                            
                            empleave_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, empleave_list);
        }

        [Route("GetCheckAnnualLeaveTaken")]
        public HttpResponseMessage GetCheckAnnualLeaveTaken(string emp_id, string lt_start_date, string lt_end_date, string le_leave_code)
        {
            List<Pers300_le> role_lst = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_siso_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","LM"),
                        new SqlParameter("@lt_number", emp_id),
                        new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(lt_start_date)),
                        new SqlParameter("@lt_end_date", db.DBYYYYMMDDformat(lt_end_date)),
                        new SqlParameter("@lt_leave_code", le_leave_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le role = new Pers300_le();
                            role.annual_leave_taken = dr["leave_exists"].ToString();
                            role_lst.Add(role);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, role_lst);
        }


    }
}