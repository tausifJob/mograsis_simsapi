﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/ShiftTemplate")]
    public class ShiftTemplateController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetShiftTemplateDetails")]
        public HttpResponseMessage GetShiftTemplateDetails(string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getShiftTemplate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getShiftTemplate"));

            List<HrmsClass> shift_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"), 
                            new SqlParameter("@sh_company_code", company_code),
                            new SqlParameter("@sh_template_id", ""),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", "")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_template_name = dr["sh_template_name"].ToString();
                            hrmsobj.sh_shift_day1 = dr["Sunday"].ToString();
                            hrmsobj.sh_shift_day2 = dr["Monday"].ToString();
                            hrmsobj.sh_shift_day3 = dr["Tuesday"].ToString();
                            hrmsobj.sh_shift_day4 = dr["Wednesday"].ToString();
                            hrmsobj.sh_shift_day5 = dr["Thursday"].ToString();
                            hrmsobj.sh_shift_day6 = dr["Friday"].ToString();
                            hrmsobj.sh_shift_day7 = dr["Saturday"].ToString();
                            shift_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shift_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        }

        [Route("getAllTemplateName")]
        public HttpResponseMessage getAllTemplateName(string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTemplateName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getAllTemplateName"));

            List<HrmsClass> temp_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),    
                            new SqlParameter("@sh_company_code", company_code),
                            new SqlParameter("@sh_template_id", ""),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", "")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_template_name = dr["sh_template_name"].ToString();
                            hrmsobj.sh_template_id = dr["sh_template_id"].ToString();
                            temp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, temp_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, temp_list);
        }

        [Route("getTemplateId")]
        public HttpResponseMessage getTemplateId()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTemplateId(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getTemplateId"));
            HrmsClass obj = new HrmsClass();
            List<HrmsClass> temp_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"), 
                            new SqlParameter("@sh_company_code", 1),
                            new SqlParameter("@sh_template_id", ""),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", "") 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            obj.sh_template_id = dr["maxcode"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, obj.sh_template_id);
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj.sh_template_id);
        }

        //[Route("GetAllShift")]
        //public HttpResponseMessage GetAllShift()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllShift(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "HRMS", "GetAllShift"));

        //    List<HrmsClass> temp_list = new List<HrmsClass>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[GetAllShift]",
        //                new List<SqlParameter>() 
        //                { 

        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    HrmsClass hrmsobj = new HrmsClass();
        //                    hrmsobj.sh_day_desc = dr["sims_appl_form_field_value1"].ToString();
        //                    hrmsobj.sh_day_code = dr["sims_appl_parameter"].ToString();
        //                    hrmsobj.sh_shift_id = dr["sh_shift_id"].ToString();
        //                    hrmsobj.sh_shift_desc = dr["sh_shift_desc"].ToString();
        //                    hrmsobj.sh_shift1_in = dr["sh_shift1_in"].ToString();
        //                    hrmsobj.sh_shift1_out = dr["sh_shift1_out"].ToString();
        //                    hrmsobj.sh_shift2_in = dr["sh_shift2_in"].ToString();
        //                    hrmsobj.sh_shift2_out = dr["sh_shift2_out"].ToString();
        //                    temp_list.Add(hrmsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, temp_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, temp_list);
        //}

        [Route("GetAllShift")]
        public HttpResponseMessage GetAllShift()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllShift(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Shift", "GetAllShift"));

            List<HrmsClass> hrmsobj = new List<HrmsClass>();
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[GetAllShift_proc]",
                        new List<SqlParameter>(){
                           new SqlParameter("@opr",'A')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass pc = new HrmsClass();
                            pc.sh_day_desc = dr["sims_appl_form_field_value1"].ToString();
                            pc.sh_day_code = dr["sims_appl_parameter"].ToString();

                            List<shift_day> day = new List<shift_day>();
                            #region  Shift_details
                            DBConnection db2 = new DBConnection();
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("[pays].[GetAllShift_proc]",
                                    new List<SqlParameter>(){
                                                   new SqlParameter("@sims_appl_parameter_day",pc.sh_day_code),
                                                   new SqlParameter("@opr",'S')
                                               });
                                if (dr2.HasRows)
                                {
                                    while (dr2.Read())
                                    {
                                        shift_day shift = new shift_day();
                                        shift.sh_shift_id = dr2["sh_shift_id"].ToString();
                                        shift.sh_shift_desc = dr2["sh_shift_desc"].ToString();
                                        shift.sh_shift1_in = dr2["sh_shift1_in"].ToString();
                                        shift.sh_shift1_out = dr2["sh_shift1_out"].ToString();
                                        shift.sh_shift2_in = dr2["sh_shift2_in"].ToString();
                                        shift.sh_shift2_out = dr2["sh_shift2_out"].ToString();
                                        day.Add(shift);


                                    }
                                }
                            }
                            #endregion
                            pc.assign = day;
                            hrmsobj.Add(pc);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, hrmsobj);
            }
        }

        //[Route("GetAllShiftEdit")]
        //public HttpResponseMessage GetAllShiftEdit(string company_code, string template_id)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllShift(),PARAMETERS :: No";
        //    Log.Debug(string.Format(debug, "Shift", "GetAllShift"));

        //    List<HrmsClass> hrmsobj = new List<HrmsClass>();
        //    try
        //    {
        //        DBConnection db = new DBConnection();
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
        //                new List<SqlParameter>(){
        //                   new SqlParameter("@opr",'F'),
        //                   new SqlParameter("@sh_serial_num", ""),
        //                   new SqlParameter("@sh_company_code", company_code),
        //                   new SqlParameter("@sh_template_id", template_id),
        //                   new SqlParameter("@sh_template_name", ""),
        //                   new SqlParameter("@sh_day_code", ""),
        //                   new SqlParameter("@sh_shift_id", ""),
        //                   new SqlParameter("@sh_template_status", ""),
        //                   new SqlParameter("@sh_created_by", ""),
        //               });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    HrmsClass pc = new HrmsClass();
        //                    pc.sh_day_desc = dr["sims_appl_form_field_value1"].ToString();
        //                    pc.sh_day_code = dr["sims_appl_parameter"].ToString();

        //                    List<shift_day> day = new List<shift_day>();
        //                    #region  Shift_details
        //                    DBConnection db2 = new DBConnection();
        //                    {
        //                        db2.Open();
        //                        SqlDataReader dr2 = db2.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
        //                            new List<SqlParameter>(){
        //                                       new SqlParameter("@sh_company_code", company_code),
        //                                       new SqlParameter("@sh_template_id", template_id),
        //                                       new SqlParameter("@sh_day_code",pc.sh_day_code),
        //                                       new SqlParameter("@opr",'U')
        //                                      });
        //                        if (dr2.HasRows)
        //                        {
        //                            while (dr2.Read())
        //                            {
        //                                shift_day shift = new shift_day();
        //                                shift.sh_shift_id = dr2["sh_shift_id"].ToString();
        //                                shift.sh_shift_desc = dr2["sh_shift_desc"].ToString();
        //                                shift.sh_shift1_in = dr2["sh_shift1_in"].ToString();
        //                                shift.sh_shift1_out = dr2["sh_shift1_out"].ToString();
        //                                shift.sh_shift2_in = dr2["sh_shift2_in"].ToString();
        //                                shift.sh_shift2_out = dr2["sh_shift2_out"].ToString();
        //                                if (dr2["sh_template_status"].ToString() == "A")
        //                                    shift.sh_template_status = true;
        //                                else
        //                                    shift.sh_template_status = false;
        //                               // if (dr2["sh_template_status"].ToString()=='A')
        //                               // shift.sh_template_status = dr2["sh_template_status"].ToString();
        //                                day.Add(shift);
        //                            }
        //                        }
        //                    }
        //                    #endregion
        //                    pc.assign = day;
        //                    hrmsobj.Add(pc);
        //                }
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, hrmsobj);
        //    }
        //}



        [Route("GetAllShiftEdit")]
        public HttpResponseMessage GetAllShiftEdit(string company_code, string template_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllShift(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Shift", "GetAllShift"));

            List<HrmsClass> hrmsobj = new List<HrmsClass>();
            try
            {
                DBConnection db = new DBConnection();
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[GetAllShift_proc]",
                        new List<SqlParameter>(){
                              new SqlParameter("@opr",'A')
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass pc = new HrmsClass();
                            pc.sh_day_desc = dr["sims_appl_form_field_value1"].ToString();
                            pc.sh_day_code = dr["sims_appl_parameter"].ToString();


                            List<shift_day> day = new List<shift_day>();

                            #region  Shift_details
                            DBConnection db2 = new DBConnection();
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("[pays].[GetAllShift_proc]",
                                    new List<SqlParameter>(){
                                                   new SqlParameter("@sims_appl_parameter_day",pc.sh_day_code),
                                                   new SqlParameter("@opr",'S')
                                               });
                                if (dr2.HasRows)
                                {
                                    while (dr2.Read())
                                    {
                                        shift_day shift = new shift_day();
                                        shift.sh_shift_id = dr2["sh_shift_id"].ToString();
                                        shift.sh_shift_desc = dr2["sh_shift_desc"].ToString();
                                        shift.sh_shift1_in = dr2["sh_shift1_in"].ToString();
                                        shift.sh_shift1_out = dr2["sh_shift1_out"].ToString();
                                        shift.sh_shift2_in = dr2["sh_shift2_in"].ToString();
                                        shift.sh_shift2_out = dr2["sh_shift2_out"].ToString();
                                        shift.sh_template_status = false;

                                        #region  Shift_details
                                        DBConnection db3 = new DBConnection();
                                        {
                                            db3.Open();
                                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                                                new List<SqlParameter>(){
                                                new SqlParameter("@sh_company_code", company_code),
                                                new SqlParameter("@sh_template_id", template_id),
                                                new SqlParameter("@sh_day_code",pc.sh_day_code),
                                                new SqlParameter("@opr",'U')
                                               });
                                            if (dr3.HasRows)
                                            {
                                                while (dr3.Read())
                                                {
                                                    shift_day shift_new = new shift_day();
                                                    // shift.sh_template_status = true;
                                                    //if (daychkflag==)
                                                    shift_new.sh_shift_id = dr3["sh_shift_id"].ToString();
                                                    shift_new.sh_template_id = dr3["sh_template_id"].ToString();
                                                    shift_new.sh_day_code = dr3["sh_day_code"].ToString();
                                                    if (template_id == shift_new.sh_template_id && pc.sh_day_code == shift_new.sh_day_code && shift.sh_shift_id == shift_new.sh_shift_id)
                                                    {
                                                        //shift_new.sh_shift_desc = dr3["sh_shift_desc"].ToString();
                                                        //shift_new.sh_shift1_in = dr3["sh_shift1_in"].ToString();
                                                        //shift_new.sh_shift1_out = dr3["sh_shift1_out"].ToString();
                                                        //shift_new.sh_shift2_in = dr3["sh_shift2_in"].ToString();
                                                        //shift_new.sh_shift2_out = dr3["sh_shift2_out"].ToString();
                                                        //if (dr2["sh_template_status"].ToString() == "A")
                                                        shift.sh_template_status = true;
                                                    }
                                                    //else
                                                    //    shift.sh_template_status = false;
                                                    //day.Add(shift_new);
                                                }
                                            }
                                            db3.Dispose();
                                        }
                                        #endregion

                                        day.Add(shift);
                                    }
                                }
                                db2.Dispose();
                            }
                            #endregion

                            pc.assign = day;
                            hrmsobj.Add(pc);
                        }
                    }
                    db.Dispose();
                }
                return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, hrmsobj);
            }
        }

        //[Route("GetAllShiftEdit")]
        //public HttpResponseMessage GetAllShiftEdit(string company_code, string template_id)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllShiftEdit(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "HRMS", "GetAllShiftEdit"));

        //    List<HrmsClass> shift_list = new List<HrmsClass>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();

        //            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
        //                new List<SqlParameter>() 
        //                { 

        //                   new SqlParameter("@opr", "U"),
        //                   new SqlParameter("@sh_serial_num", ""),
        //                   new SqlParameter("@sh_company_code", company_code),
        //                   new SqlParameter("@sh_template_id", template_id),
        //                   new SqlParameter("@sh_template_name", ""),
        //                   new SqlParameter("@sh_day_code", ""),
        //                   new SqlParameter("@sh_shift_id", ""),
        //                   new SqlParameter("@sh_template_status", ""),
        //                   new SqlParameter("@sh_created_by", ""),
        //                });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    HrmsClass hrmsobj = new HrmsClass();
        //                    hrmsobj.sh_day_desc = dr["sims_appl_form_field_value1"].ToString();
        //                    hrmsobj.sh_day_code = dr["sims_appl_parameter"].ToString();
        //                    hrmsobj.sh_shift_id = dr["sh_shift_id"].ToString();
        //                    hrmsobj.sh_shift_desc = dr["sh_shift_desc"].ToString();
        //                    hrmsobj.sh_shift1_in = dr["sh_shift1_in"].ToString();
        //                    hrmsobj.sh_shift1_out = dr["sh_shift1_out"].ToString();
        //                    hrmsobj.sh_shift2_in = dr["sh_shift2_in"].ToString();
        //                    hrmsobj.sh_shift2_out = dr["sh_shift2_out"].ToString();
        //                    if (dr["sh_template_status"].ToString() == "A")
        //                        hrmsobj.sh_day_status = true;
        //                    else
        //                        hrmsobj.sh_day_status = false;

        //                    shift_list.Add(hrmsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, shift_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        //}

        [Route("CUDshift_data")]
        public HttpResponseMessage CUDshift_data(List<HrmsClass> data)
        {
            // string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDshift_data(),PARAMETERS :: No";
            //Log.Debug(string.Format(debug, "STUDENT", "CUDshift_data"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (HrmsClass hrmsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_shift_template_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", hrmsobj.opr),
                                new SqlParameter("@sh_serial_num", ""),
                                new SqlParameter("@sh_company_code", hrmsobj.sh_company_code),
                                new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
                                new SqlParameter("@sh_template_name", hrmsobj.sh_template_name),
                                new SqlParameter("@sh_day_code", hrmsobj.sh_day_code),
                                new SqlParameter("@sh_shift_id", hrmsobj.sh_shift_id),
                                new SqlParameter("@sh_template_status",hrmsobj.sh_template_status == true? "A":"I"),
                                new SqlParameter("@sh_created_by", "01")
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDDelete_Shift_template")]
        public HttpResponseMessage CUDDelete_Shift_template(List<HrmsClass> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDModerator(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "CUDshift_data"));

            Message message = new Message();
            bool deleted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (HrmsClass hrmsobj in data)
                        {
                            int del = db.ExecuteStoreProcedureforInsert("[pays].[pays_shift_template_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", hrmsobj.opr),
                                new SqlParameter("@sh_template_name", hrmsobj.sh_template_name),
                                
                             });
                            if (del > 0)
                            {
                                deleted = true;
                            }
                            else
                            {
                                deleted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, deleted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, deleted);
        }

        [Route("getCheckedTemplateId")]
        public HttpResponseMessage getCheckedTemplateId(string TemplateId, string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCheckedTemplateId(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getCheckedTemplateId"));
            HrmsClass obj = new HrmsClass();
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                   new List<SqlParameter>() 
                    { 
                        new SqlParameter("@opr", "X"), 
                        new SqlParameter("@sh_template_id",TemplateId),
                        new SqlParameter("@sh_company_code",company_code),
                    });
                    if (dr.HasRows)
                    {
                        flag = true;
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        [Route("GetIdOfTemplateName")]
        public HttpResponseMessage GetIdOfTemplateName(string templateName, string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Check_TemplateId(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Check_TemplateId"));
            HrmsClass obj = new HrmsClass();
            bool flag = false;
            List<HrmsClass> shift_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                   new List<SqlParameter>() 
                    { 
                        new SqlParameter("@opr", "Y"), 
                        new SqlParameter("@sh_template_name",templateName),
                         new SqlParameter("@sh_company_code",company_code),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_template_id = dr["sh_template_id"].ToString();
                            if (dr["sh_template_status"].ToString() == "A")
                                hrmsobj.sh_template_status = true;
                            else
                                hrmsobj.sh_template_status = false;
                            shift_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shift_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        }

        [Route("GetStatusTemplate")]
        public HttpResponseMessage GetStatusTemplate(string company_code, string template_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetStatusTemplate(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "GetStatusTemplate"));
            int count = 0;
            List<HrmsClass> shift_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sh_serial_num", ""),
                            new SqlParameter("@sh_company_code", company_code),
                            new SqlParameter("@sh_template_id", template_id),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            count = int.Parse(dr[0].ToString());
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, count);

            }
            return Request.CreateResponse(HttpStatusCode.OK, count);
        }

        [Route("GetTemplateDetails")]
        public HttpResponseMessage GetTemplateDetails(string company_code, string template_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetTemplateDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "GetTemplateDetails"));

            List<HrmsClass> shift_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sh_serial_num", ""),
                            new SqlParameter("@sh_company_code", company_code),
                            new SqlParameter("@sh_template_id", template_id),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_template_name = dr["sh_template_name"].ToString();
                            hrmsobj.sh_shift_day1 = dr["Sunday"].ToString();
                            hrmsobj.sh_shift_day2 = dr["Monday"].ToString();
                            hrmsobj.sh_shift_day3 = dr["Tuesday"].ToString();
                            hrmsobj.sh_shift_day4 = dr["Wednesday"].ToString();
                            hrmsobj.sh_shift_day5 = dr["Thursday"].ToString();
                            hrmsobj.sh_shift_day6 = dr["Friday"].ToString();
                            hrmsobj.sh_shift_day7 = dr["Saturday"].ToString();
                            shift_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shift_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        }

        [Route("getCheckEmployeeMapped")]
        public HttpResponseMessage getCheckEmployeeMapped(string company_code, string template_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckEmployeeMapped(),PARAMETERS :: company_code{2},template_id{3}";
            Log.Debug(string.Format(debug, "HRMS", "CheckEmployeeMapped", company_code, template_id));

            List<HrmsClass> shift_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_template_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@sh_serial_num", ""),
                            new SqlParameter("@sh_company_code", company_code),
                            new SqlParameter("@sh_template_id", template_id),
                            new SqlParameter("@sh_template_name", ""),
                            new SqlParameter("@sh_day_code", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_template_status", ""),
                            new SqlParameter("@sh_created_by", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_shift_id = dr["sh_shift_id"].ToString();
                            hrmsobj.sh_template_id = dr["sh_template_id"].ToString();
                            hrmsobj.sh_company_code = dr["sh_company_code"].ToString();
                            shift_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shift_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        }

    }
}