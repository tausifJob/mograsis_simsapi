﻿using log4net;
using SIMSAPI.Helper;

using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/MarkEmpResignation")]

    public class MarkEmpResignationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        [Route("getEmployeDetails")]
        public HttpResponseMessage getEmployeDetails(string login_code, string com_code, string dep_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getEmployeDetails"));
            List<Pers306> emp_list = new List<Pers306>();

            string[] str = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sup", "Oct", "Nov", "Dec" };

            if (login_code == "undefined")
            {
                login_code = null;
            }
            if (com_code == "undefined")
            {
                com_code = null;
            }
            if (dep_code == "undefined")
            {
                dep_code = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.Pays_Mark_Employee_Resignation_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@em_login_code",login_code),
                                new SqlParameter("@em_company_code",com_code),
                                new SqlParameter("@em_dept_code", dep_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 invsobj = new Pers306();
                            string ac = dr["st"].ToString();
                            if (dr["st"].ToString() == "R" || dr["st"].ToString() == "N")
                            {
                                invsobj.com_code = dr["st"].ToString();

                            }
                            else
                            {
                                invsobj.em_number = dr["em_number"].ToString();
                                invsobj.em_login_code = dr["em_login_code"].ToString();
                                string year = string.Empty, month = string.Empty;
                                string abc = dr["sd_year_month"].ToString();
                                if (!string.IsNullOrEmpty(dr["sd_year_month"].ToString()))
                                {
                                    year = dr["sd_year_month"].ToString().Substring(0, 4);
                                    month = str[int.Parse(dr["sd_year_month"].ToString().Substring(4).ToString()) - 1];
                                    year = string.Format("{0}-{1}", year, month);

                                    invsobj.sd_year_month = year;
                                }

                                invsobj.em_full_name = dr["em_full_name"].ToString();
                               // invsobj.em_date_of_join = DateTime.Parse(dr["em_date_of_join"].ToString()).ToShortDateString(); ;
                                invsobj.em_date_of_join =db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                                invsobj.dg_desc = dr["dg_desc"].ToString();
                                invsobj.com_code = dr["com_code"].ToString();
                                invsobj.com_name = dr["com_name"].ToString();
                                invsobj.dep_code = dr["dep_code"].ToString();
                                invsobj.dep_name = dr["dep_name"].ToString();
                                if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                                    //invsobj.reg_date = DateTime.Parse(dr["em_left_date"].ToString()).Year + "-" + DateTime.Parse(dr["em_left_date"].ToString()).Month + "-" + DateTime.Parse(dr["em_left_date"].ToString()).Day;
                                    invsobj.reg_date =db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                                invsobj.reg_resion = dr["em_left_reason"].ToString();
                                if (dr["em_status"].ToString() == "Y")
                                    invsobj.em_status = true;
                                else
                                    invsobj.em_status = false;
                                if (dr["gratuaty_status"].ToString() == "Y")
                                    invsobj.gratuaty_status = true;
                                else
                                    invsobj.gratuaty_status = false;

                                invsobj.em_disengagement_type = dr["em_disengagement_type"].ToString();

                            }


                            //Pers306 invsobj = new Pers306();
                            //string ac = dr["st"].ToString();
                            //if (dr["st"].ToString() == "R" || dr["st"].ToString() == "N")
                            //{
                            //    invsobj.com_code = dr["st"].ToString();

                            //}
                            //else
                            //{
                            //    invsobj.em_number = dr["em_number"].ToString();
                            //    invsobj.em_login_code = dr["em_login_code"].ToString();
                            //    string year = string.Empty, month = string.Empty;
                            //    string abc = dr["sd_year_month"].ToString();
                            //    if (!string.IsNullOrEmpty(dr["sd_year_month"].ToString()))
                            //    {
                            //        year = dr["sd_year_month"].ToString().Substring(0, 4);
                            //        month = str[int.Parse(dr["sd_year_month"].ToString().Substring(4).ToString()) - 1];
                            //        year = string.Format("{0}-{1}", year, month);

                            //        invsobj.sd_year_month = year;
                            //    }

                            //    invsobj.em_full_name = dr["em_full_name"].ToString();
                            //    invsobj.em_date_of_join = DateTime.Parse(dr["em_date_of_join"].ToString()).ToShortDateString(); ;
                            //    invsobj.dg_desc = dr["dg_desc"].ToString();
                            //    invsobj.com_code = dr["com_code"].ToString();
                            //    invsobj.com_name = dr["com_name"].ToString();
                            //    invsobj.dep_code = dr["dep_code"].ToString();
                            //    invsobj.dep_name = dr["dep_name"].ToString();
                            //    invsobj.reg_date = "";
                            //    invsobj.reg_resion = "";
                            //}

                            emp_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("getCompanydetails")]
        public HttpResponseMessage getCompanydetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanydetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getCompanydetails"));

            List<Pers306> company_list = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 objot = new Pers306();
                            objot.com_code = dr["co_company_code"].ToString();
                            objot.com_name = dr["co_desc"].ToString();
                            company_list.Add(objot);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, company_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, company_list);
        }

        [Route("getCompanyDepartments")]
        public HttpResponseMessage getCompanyDepartments(string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanyDepartments(),PARAMETERS :: COMPCODE{2}";
            Log.Debug(string.Format(debug, "HRMS", "getCompanyDepartments", company_code));

            List<Pers306> code = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@opr_mem", ""),
                            new SqlParameter("@tbl_cond", company_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 objot = new Pers306();
                            objot.dep_code = dr["codp_dept_no"].ToString();
                            objot.dep_name = dr["codp_dept_name"].ToString();
                            code.Add(objot);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, code);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code);
        }

        [Route("MarkEmployeeDetails")]
        public HttpResponseMessage MarkEmployeeDetails(Pers306 persobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : MarkEmployeeDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "MarkEmployeeDetails"));

            // Pers306 persobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Pers306>(data);
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("pays.Pays_Mark_Employee_Resignation_proc",
                    new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr",persobj.opr),
                            new SqlParameter("@em_login_code", persobj.em_login_code),
                            new SqlParameter("@em_number", persobj.em_number),
                            new SqlParameter("@em_left_date",db.DBYYYYMMDDformat(persobj.reg_date)),
                            new SqlParameter("@em_left_reason",persobj.reg_resion),
                            new SqlParameter("@em_disengagement_type",persobj.em_disengagement_type)
                        });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getAmt_ApplandWorkingDays")]
        public HttpResponseMessage getAmt_ApplandWorkingDays(string empl_code)
        {

            List<Pers306> desg_list = new List<Pers306>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),    
                            new SqlParameter("@grt_emp_code",empl_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 simsobj = new Pers306();
                            simsobj.grt_amount = dr["grt_amount"].ToString();
                            simsobj.grt_applicable_days = dr["grt_applicable_days"].ToString();
                            simsobj.grt_working_days = dr["grt_working_days"].ToString();
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }


        [Route("CUDInsert_pays_gratuity_empl")]
        public HttpResponseMessage CUDInsert_pays_gratuity_empl(List<Pers306> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsert_pays_gratuity_empl(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "Payroll_emp"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers306 simsobj in data)
                        {
                            //int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_empl_proc]",
                           SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_empl_proc]",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@grt_emp_leave_days", simsobj.grt_emp_leave_days),
                            new SqlParameter("@grt_emp_code", simsobj.em_number),
                              //new SqlParameter("@em_login_code", simsobj.em_login_code),
                            new SqlParameter("@grt_amount",simsobj.grt_amount),
                            new SqlParameter("@grt_emp_applicable_days",simsobj.grt_applicable_days),
                            new SqlParameter("@grt_emp_working_days",simsobj.grt_working_days),
               	
                            new SqlParameter("@grt_airfare_amount", simsobj.grt_airfare_amount),
                            new SqlParameter("@grt_leave_include_flag",simsobj.grt_leave_include_flag == true? "1":"0"),
                            new SqlParameter("@grt_cash_bank_flag",simsobj.grt_cash_bank_flag == true? "C":"B"),
                            new SqlParameter("@grt_airfare_flag", simsobj.grt_airfare_flag == true?"1":"0"),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            dr.Close();
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("calculateGratuity")]
        public HttpResponseMessage calculateGratuity(Pers306 data)
        {            
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        //int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_empl_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_empl_proc]",

                        new List<SqlParameter>()
                          {
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@reg_date", db.DBYYYYMMDDformat(data.reg_date)),
                            new SqlParameter("@em_date_of_join", db.DBYYYYMMDDformat(data.em_date_of_join)),                            
                            new SqlParameter("@em_number",data.em_number),
                            new SqlParameter("@grt_comp_code",data.com_code),
                            new SqlParameter("@grt_updated_user",data.grt_updated_user),                            
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        dr.Close();
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {                
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getDisenagementType")]
        public HttpResponseMessage getDisenagementType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDisenagementType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getDisenagementType"));

            List<Pers306> company_list = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[Pays_Mark_Employee_Resignation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "A")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 objot = new Pers306();
                            objot.pays_appl_parameter = dr["pays_appl_parameter"].ToString();
                            objot.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                            company_list.Add(objot);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, company_list);
        }

    }
}