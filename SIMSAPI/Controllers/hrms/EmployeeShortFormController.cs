﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Newtonsoft.Json;

namespace SIMSAPI.Controllers.modules.HRMS
{
    [RoutePrefix("api/HRMS/empShortFrm")]
    [BasicAuthentication]
    public class EmployeeShortFormController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getEmpData")]
        public HttpResponseMessage getEmpData()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));

            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetEMPData_Temp", new List<SqlParameter>());
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            //obj.em_Company_name = dr["Company_Name"].ToString();
                            //obj.em_Company_Code = dr["Company_Code"].ToString();
                            //obj.em_Company_Short_Name = dr["Company_ShortName"].ToString();

                            obj.em_Designation_Code = dr["Designation_Code"].ToString();
                            obj.em_Designation_name = dr["Designation_Desc"].ToString();
                            obj.em_Designation_Company_Code = dr["Designation_Company_code"].ToString();

                            obj.em_Grade_Code = dr["Grade_Code"].ToString();
                            obj.em_Grade_Company_Code = dr["Grade_Company_code"].ToString();
                            obj.em_Grade_name = dr["Grade_Desc"].ToString();

                            obj.em_Dept_Code = dr["Department_Code"].ToString();
                            obj.em_Dept_Company_Code = dr["Department_Company_Code"].ToString();
                            obj.em_Dept_name = dr["Department_Name"].ToString();
                            obj.em_Dept_Short_Name = dr["Department_Short_Name"].ToString();

                            obj.em_Staff_Type_name = dr["Staff_Type_Value"].ToString();
                            obj.em_Staff_Type_Code = dr["Staff_Type_Code"].ToString();

                            obj.em_Religion_Code = dr["sims_religion_code"].ToString();
                            obj.em_Religion_name = dr["sims_religion_name_en"].ToString();

                            obj.em_Sex_name = dr["sims_appl_form_field_value1_gender"].ToString();
                            obj.em_Sex_Code = dr["sims_appl_parameter_gender"].ToString();

                            obj.em_Salutation_name = dr["sims_appl_form_field_value1_salut"].ToString();
                            obj.em_Salutation_Code = dr["sims_appl_parameter_salut"].ToString();

                            //obj.em_Country_name = dr["sims_country_name_en"].ToString();
                            //obj.em_Country_Code = dr["sims_country_code"].ToString();

                            obj.em_Nation_Code = dr["sims_nationality_code"].ToString();
                            obj.em_Nation_name = dr["sims_nationality_name_en"].ToString();

                            //obj.em_Dest_Code = dr["Pays_Destination_Code"].ToString();
                            //obj.em_Dest_name = dr["Pays_Destination_Desc"].ToString();

                            obj.em_Marital_Status_name = dr["sims_appl_form_field_value1_Married_Status"].ToString();
                            obj.em_Marital_Status_Code = dr["sims_appl_parameter_Married_Status"].ToString();

                            obj.em_blood_group_name = dr["sims_appl_form_field_value1_blood"].ToString();
                            obj.em_blood_group_code = dr["sims_appl_parameter_blood"].ToString();

                            obj.em_ethnicity_name = dr["ethnicity_name_en"].ToString();
                            obj.em_ethnicity_code = dr["ethnicity_code"].ToString();


                            obj.em_Bank_Code = dr["bank_code"].ToString();
                            obj.em_Bank_name = dr["bank_name"].ToString();

                            obj.em_visa_type_code = dr["visatype_code"].ToString();
                            obj.em_visa_type = dr["visatype_name"].ToString();

                            obj.em_service_status_code = dr["servicestatus_code"].ToString();
                            obj.em_service_status = dr["servicestatus_name"].ToString();

                            obj.em_secret_question_code = dr["secretquestion_code"].ToString();
                            obj.em_secret_question = dr["secretquestion"].ToString();

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("CUCreateEditEmployee")]//insert
        public HttpResponseMessage CUCreateEditEmployee(Pers099 data)
        {
            Pers099 st = new Pers099();
            Pers099 simsobj = data;
            //string empid = "";
            //bool inserted = false;
            string strMessage = "";
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",
                            new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",simsobj.opr),
                                    new SqlParameter("@em_login_code",simsobj.em_login_code),
                                    new SqlParameter("@em_salutation", simsobj.em_Salutation_Code),
                                    new SqlParameter("@em_first_name",simsobj.em_first_name),
                                    new SqlParameter("@em_middle_name", simsobj.em_middle_name),
                                    new SqlParameter("@em_last_name", simsobj.em_last_name),
                                    new SqlParameter("@em_name_ot", simsobj.em_name_ot),
                                    new SqlParameter("@em_summary_address", simsobj.em_summary_address),
                                    new SqlParameter("@em_country_name", simsobj.em_Country_Code),
                                    new SqlParameter("@em_sex", simsobj.em_Sex_Code),
                                    new SqlParameter("@em_marital_status", simsobj.em_Marital_Status_Code),
                                    new SqlParameter("@em_company_name", simsobj.em_Company_Code),
                                    new SqlParameter("@em_desg_name", simsobj.em_Designation_Code),
                                    new SqlParameter("@em_staff_type",simsobj.em_Staff_Type_Code),
                                    new SqlParameter("@em_nation_name",simsobj.em_Nation_Code),
                                    new SqlParameter("@em_dest_name", simsobj.em_Dest_Code),
                                    new SqlParameter("@em_email", simsobj.em_email),
                                    new SqlParameter("@em_mobile", simsobj.em_mobile),
                                    new SqlParameter("@em_emergency_contact_name1", simsobj.em_emergency_contact_name1),
                                    new SqlParameter("@em_emergency_contact_number1", simsobj.em_emergency_contact_number1),
                                    new SqlParameter("@em_religion_name", simsobj.em_Religion_Code), 
                                    new SqlParameter("@em_date_of_join", db.DBYYYYMMDDformat(simsobj.em_date_of_join)), 
                                    new SqlParameter("@em_blood_group_code", simsobj.em_blood_group_code), 
                                    new SqlParameter("@em_passport_number", simsobj.em_passport_no),
                                    new SqlParameter("@em_passport_issue_date", db.DBYYYYMMDDformat(simsobj.em_passport_issue_date)),
                                    new SqlParameter("@em_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.em_passport_exp_date)),
                                    new SqlParameter("@em_visa_number", simsobj.em_visa_no),
                                    new SqlParameter("@em_visa_type",simsobj.em_visa_type_code),
                                    new SqlParameter("@em_visa_issue_date", db.DBYYYYMMDDformat(simsobj.em_visa_issue_date)),
                                    new SqlParameter("@em_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.em_visa_exp_date)),
                                    new SqlParameter("@em_dept_name", simsobj.em_Dept_Code),
                                    new SqlParameter("@em_dept_effect_from", db.DBYYYYMMDDformat(simsobj.em_dept_effect_from)),
                                    new SqlParameter("@em_grade_name", simsobj.em_Grade_Code),
                                    new SqlParameter("@em_grade_effect_from", db.DBYYYYMMDDformat(simsobj.em_grade_effect_from)),
                                    new SqlParameter("@em_national_id", simsobj.em_national_id),
                                    new SqlParameter("@em_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.em_national_id_issue_date)),
                                    new SqlParameter("@em_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.em_national_id_expiry_date)),
                                    new SqlParameter("@em_date_of_birth", db.DBYYYYMMDDformat(simsobj.em_date_of_birth)),
                                    new SqlParameter("@em_bank_ac_no", simsobj.em_bank_ac_no),
                                    new SqlParameter("@em_iban_no", simsobj.em_iban_no), 
                                    new SqlParameter("@Role_Code",simsobj.comn_role_code),
                                    new SqlParameter("@em_img", simsobj.em_img),
                                 });
                       
                            if (dr.Read())
                            {
                                strMessage = dr[0].ToString()+"/"+dr[1].ToString();
                            }
                            dr.Close();
                        }
                    
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, strMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strMessage);
        }

        [Route("getSearchEmpMaster")]//search
        public HttpResponseMessage getSearchEmpMaster(string em_login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchEmp(),PARAMETERS ::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getSearchEmp", em_login_code));

            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@em_login_code",em_login_code),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_Salutation_Code = dr["em_salutation"].ToString();
                            //obj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            obj.em_login_code = dr["em_login_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_middle_name = dr["em_middle_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.em_family_name = dr["em_family_name"].ToString();
                            obj.em_apartment_number = dr["em_appartment_number"].ToString();
                            obj.em_building_number = dr["em_building_number"].ToString();
                            obj.em_street_number = dr["em_street_number"].ToString();
                            obj.em_area_number = dr["em_area_number"].ToString();
                            obj.em_phone = dr["em_phone"].ToString();
                            obj.em_fax = dr["em_fax"].ToString();
                            obj.em_post = dr["em_po_box"].ToString();
                            obj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            obj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            obj.em_joining_ref = dr["em_joining_ref"].ToString();
                            obj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();

                            if (dr["em_habdicap_status"].ToString() == "A")
                                obj.em_handicap_status = true;
                            else
                                obj.em_handicap_status = false;

                            if (dr["em_stop_salary_indicator"].ToString() == "Y")
                                obj.em_stop_salary_indicator = true;
                            else
                                obj.em_stop_salary_indicator = false;

                            if (dr["em_agreement"].ToString() == "A")
                                obj.em_agreement = true;
                            else
                                obj.em_agreement = false;

                            if (dr["em_punching_status"].ToString() == "A")
                                obj.em_punching_status = true;
                            else
                                obj.em_punching_status = false;

                            if (dr["em_bank_cash_tag"].ToString() == "B")
                                obj.em_bank_cash_tag = true;
                            else
                                obj.em_bank_cash_tag = false;

                            if (dr["em_citi_exp_tag"].ToString() == "C")
                                obj.em_citi_exp_tag = true;
                            else
                                obj.em_citi_exp_tag = false;

                            if (dr["em_leave_tag"].ToString() == "Y")
                                obj.em_leave_tag = true;
                            else
                                obj.em_leave_tag = false;
                            obj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());
                            obj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            obj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            obj.em_pssport_exp_rem_date = db.UIDDMMYYYYformat(dr["em_passport_remember_expiry_date"].ToString());
                            obj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            obj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            obj.em_visa_exp_rem_date = dr["em_visa_remember_expiry_date"].ToString();
                            obj.em_visa_type_code = dr["em_visa_type"].ToString();
                            obj.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());
                            obj.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());
                            obj.em_agreemet_exp_rem_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_rem_date"].ToString());
                            obj.em_punching_id = dr["em_punching_id"].ToString();
                            obj.em_dependant_full = dr["em_dependant_full"].ToString();
                            obj.em_dependant_half = dr["em_dependant_half"].ToString();
                            obj.em_dependant_infant = dr["em_dependant_infant"].ToString();
                            obj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            obj.em_Bank_Code = dr["em_Bank_Code"].ToString();
                            obj.em_iban_no = dr["em_iban_no"].ToString();
                            obj.em_route_code = dr["em_route_code"].ToString();
                            obj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            obj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            obj.em_pan_no = dr["em_pan_no"].ToString();

                            obj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            obj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            //obj.em_gosi_start_date = dr["em_gosi_start_date"].ToString();
                            obj.em_national_id = dr["em_national_id"].ToString();
                            obj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            obj.em_secret_answer = dr["em_secret_answer"].ToString();
                            obj.em_name_ot = dr["em_name_ot"].ToString();
                            obj.em_summary_address = dr["em_summary_address"].ToString();
                            obj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            obj.em_Dept_Code = dr["em_dept_code"].ToString();
                            obj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            obj.em_Grade_Code = dr["em_grade_code"].ToString();
                            obj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            obj.em_Country_Code = dr["em_country_code"].ToString();
                            obj.em_State_Code = dr["em_state"].ToString();
                            obj.em_City_Code = dr["em_city"].ToString();
                            obj.em_service_status_code = dr["em_service_status"].ToString();
                            obj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            obj.em_Sex_Code = dr["em_sex"].ToString();
                            obj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            obj.em_Company_Code = dr["em_company_code"].ToString();
                            obj.em_Designation_Code = dr["em_desg_code"].ToString();
                            obj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            obj.em_Nation_Code = dr["em_nation_code"].ToString();
                            obj.em_Dest_Code = dr["em_dest_code"].ToString();
                            obj.em_email = dr["em_email"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();
                            obj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            obj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            obj.em_Religion_Code = dr["em_religion_code"].ToString();
                            obj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            obj.em_blood_group_code = dr["em_blood_group_code"].ToString();
                            obj.em_passport_no = dr["em_passport_number"].ToString();

                            obj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());
                            obj.em_visa_no = dr["em_visa_number"].ToString();
                            obj.em_visa_type_code = dr["em_visa_type"].ToString();
                            obj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());
                            obj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            obj.em_Nation_name = dr["em_national_id"].ToString();
                            obj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());
                            obj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());
                            obj.em_img = dr["em_img"].ToString();
                            obj.em_status_code = dr["em_status"].ToString();
                            obj.comn_role_code = dr["comn_role_code"].ToString();
                            obj.comn_role_name = dr["comn_role_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);

            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
        }

        [Route("CUMasterEmployee")]//update
        public HttpResponseMessage CUMasterEmployee(Pers099 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUMasterEmployee(),PARAMETERS::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CUMasterEmployee", data));

            Pers099 simsobj = data;


            string empid = "";
            bool flag = false;
            Message message = new Message();

            try
            {
                string handi = null;
                string agree = null;
                string punch = null;
                string bank = null;
                string citi = null;
                string leave = null;
                string sal = null;

                if (simsobj != null)
                {

                    if (data.em_handicap_status)
                    { handi = "A"; }
                    else
                    { handi = "I"; }

                    if (data.em_agreement)
                    { agree = "A"; }
                    else
                    { agree = "I"; }

                    if (data.em_punching_status)
                    { punch = "A"; }
                    else
                    { punch = "I"; }

                    if (data.em_bank_cash_tag)
                    { bank = "B"; }
                    else
                    { bank = "C"; }

                    if (data.em_citi_exp_tag)
                    { citi = "C"; }
                    else
                    { citi = "E"; }

                    if (data.em_leave_tag)
                    { leave = "Y"; }
                    else
                    { leave = "N"; }

                    if (data.em_stop_salary_indicator)
                    { sal = "Y"; }
                    else
                    { sal = "N"; }

                    simsobj.em_img = simsobj.em_login_code + simsobj.em_img;
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",
                            new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",simsobj.opr),
                                    new SqlParameter("@em_login_code",simsobj.em_login_code),
                                    new SqlParameter ("@em_number",simsobj.em_number),
                                    new SqlParameter("@em_salutation", simsobj.em_Salutation_Code),
                                    new SqlParameter("@em_first_name",simsobj.em_first_name),
                                    new SqlParameter("@em_middle_name", simsobj.em_middle_name),
                                    new SqlParameter("@em_last_name", simsobj.em_last_name),
                                    //new SqlParameter("@em_family_name",simsobj.em_family_name),
                                    new SqlParameter("@em_name_ot", simsobj.em_name_ot),
                                    new SqlParameter("@em_country_code", simsobj.em_Country_Code),
                                    //new SqlParameter("@em_state_name", simsobj.em_State_Code),
                                    new SqlParameter("@em_dest_code", simsobj.em_Dest_Code),
                                    new SqlParameter("@em_date_of_birth", db.DBYYYYMMDDformat(simsobj.em_date_of_birth)),
                                    new SqlParameter("@em_sex", simsobj.em_Sex_Code),
                                    new SqlParameter("@em_marital_status", simsobj.em_Marital_Status_Code),
                                    new SqlParameter("@em_company_code", simsobj.em_Company_Code),
                                    new SqlParameter("@em_desg_code", simsobj.em_Designation_Code),
                                    new SqlParameter("@em_service_status", simsobj.em_service_status_code), 
                                    new SqlParameter("@em_dept_code", simsobj.em_Dept_Code),
                                    new SqlParameter("@em_dept_effect_from", db.DBYYYYMMDDformat(simsobj.em_dept_effect_from)),
                                    new SqlParameter("@em_grade_code", simsobj.em_Grade_Code),
                                    new SqlParameter("@em_grade_effect_from", db.DBYYYYMMDDformat(simsobj.em_grade_effect_from)),
                                    new SqlParameter("@em_leave_tag",leave), 
                                    new SqlParameter("@em_stop_salary_indicator",sal),
                                    new SqlParameter("@em_bank_cash_tag",bank), 
                                    new SqlParameter("@em_date_of_join", simsobj.em_date_of_join), 
                                    new SqlParameter("@em_citi_exp_tag",citi), 
                                    new SqlParameter("@em_ledger_name"," "), 
                                    new SqlParameter("@em_ledger_ac_no"," "), 
                                    new SqlParameter("@em_modified_on"," "), 
                                    new SqlParameter("@em_leave_resume_date",DateTime.Now), 
                                    //new SqlParameter("@em_pan_no", simsobj.em_pan_no), 
                                    //new SqlParameter("@em_city_name", simsobj.em_City_Code),
                                    new SqlParameter("@em_staff",simsobj.em_Staff_Type_Code),
                                    new SqlParameter("@em_nation_code",simsobj.em_Nation_Code),
                                    //new SqlParameter("@em_appartment_number", simsobj.em_apartment_number),
                                    //new SqlParameter("@em_building_number", simsobj.em_building_number),
                                    //new SqlParameter("@em_street_number", simsobj.em_street_number),
                                    //new SqlParameter("@em_area_number", simsobj.em_area_number),
                                    new SqlParameter("@em_summary_address", simsobj.em_summary_address),
                                    //new SqlParameter("@em_summary_address_local_language", simsobj.em_summary_address_local_language),
                                    new SqlParameter("@em_email", simsobj.em_email),
                                    //new SqlParameter("@em_phone", simsobj.em_phone),
                                    new SqlParameter("@em_mobile", simsobj.em_mobile),
                                    //new SqlParameter("@em_fax", simsobj.em_fax),
                                    //new SqlParameter("@em_po_box", simsobj.em_post),
                                    new SqlParameter("@em_emergency_contact_name1", simsobj.em_emergency_contact_name1),
                                    //new SqlParameter("@em_emergency_contact_name2", simsobj.em_emergency_contact_name2),
                                    new SqlParameter("@em_emergency_contact_number1", simsobj.em_emergency_contact_number1),
                                    //new SqlParameter("@em_emergency_contact_number2", simsobj.em_emergency_contact_number2),
                                    new SqlParameter("@em_religion_code", simsobj.em_Religion_Code), 
                                    //new SqlParameter("@em_joining_ref", simsobj.em_joining_ref), 
                                    //new SqlParameter("@em_ethnicity_name", simsobj.em_ethnicity_code), 
                                    new SqlParameter("@em_blood_group_code", simsobj.em_blood_group_code), 
                                    new SqlParameter("@em_habdicap_status", handi), 
                                    new SqlParameter("@em_passport_number", simsobj.em_passport_no),
                                    //new SqlParameter("@em_passport_issue_place", simsobj.em_passport_issue_place),
                                    //new SqlParameter("@em_passport_issuing_authority", simsobj.em_passport_issuing_authority),
                                    new SqlParameter("@em_passport_issue_date", db.DBYYYYMMDDformat(simsobj.em_passport_issue_date)),
                                    new SqlParameter("@em_passport_expiry_date",db.DBYYYYMMDDformat(simsobj.em_passport_exp_date)),
                                    //new SqlParameter("@em_passport_remember_expiry_date",simsobj.em_pssport_exp_rem_date),
                                    new SqlParameter("@em_visa_number", simsobj.em_visa_no),
                                     new SqlParameter("@em_visa_type",simsobj.em_visa_type_code),
                                    //new SqlParameter("@em_visa_issuing_place", simsobj.em_visa_issuing_place),
                                    //new SqlParameter("@em_visa_issuing_authority", simsobj.em_visa_issuing_authority),
                                    new SqlParameter("@em_visa_issue_date", db.DBYYYYMMDDformat(simsobj.em_visa_issue_date)),
                                    new SqlParameter("@em_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.em_visa_exp_date)),
                                      new SqlParameter("@em_national_id", simsobj.em_national_id),
                                    new SqlParameter("@em_national_id_issue_date", db.DBYYYYMMDDformat(simsobj.em_national_id_issue_date)),
                                    new SqlParameter("@em_national_id_expiry_date", db.DBYYYYMMDDformat(simsobj.em_national_id_expiry_date)), 
                                    
                        
                                     new SqlParameter("@em_img", simsobj.em_img),
                                    //new SqlParameter("@em_visa_remember_expiry_date", simsobj.em_visa_exp_rem_date),
                                    //new SqlParameter("@em_visa_type", simsobj.em_visa_type_code), 
                                    new SqlParameter("@em_agreement",agree), 
                                    //new SqlParameter("@em_agreement_start_date", simsobj.em_agreement_start_date), 
                                    //new SqlParameter("@em_agreement_end_date", simsobj.em_agreement_exp_date), 
                                    //new SqlParameter("@em_agreement_exp_rem_date", simsobj.em_agreemet_exp_rem_date), 
                                    //new SqlParameter("@em_punching_id", simsobj.em_punching_id),
                                    new SqlParameter("@em_punching_status",punch), 
                                    new SqlParameter("@em_status", simsobj.em_status_code), 
                                    //new SqlParameter("@em_labour_card_no",simsobj.em_labour_card_no),
                                    //new SqlParameter("@em_dependant_full", Convert.ToInt32( simsobj.em_dependant_full)), 
                                    //new SqlParameter("@em_dependant_half", Convert.ToInt32( simsobj.em_dependant_half)), 
                                    //new SqlParameter("@em_dependant_infant", Convert.ToInt32( simsobj.em_dependant_infant)), 
                                    //new SqlParameter("@em_bank_name", simsobj.em_Bank_Code), 
                                    new SqlParameter("@em_bank_ac_no", simsobj.em_bank_ac_no), 
                                    new SqlParameter("@em_iban_no", simsobj.em_iban_no), 
                                    //new SqlParameter("@em_route_code", simsobj.em_route_code),   
                                    //new SqlParameter("@em_bank_swift_code", simsobj.em_bank_swift_code), 
                                    //new SqlParameter("@em_gpf_ac_no", simsobj.em_gpf_ac_no), 
                                    //new SqlParameter("@em_gosi_ac_ac_no", simsobj.em_gosi_ac_no), 
                                    //new SqlParameter("@em_gosi_start_date", simsobj.em_gosi_start_date),  
                                  
                                   // new SqlParameter("@em_stop_salary_from", simsobj.em_stop_salary_from),  
                                    //new SqlParameter("@em_secret_question_code", simsobj.em_secret_question_code), 
                                    //new SqlParameter("@em_secret_answer", simsobj.em_secret_answer), 
                                     new SqlParameter("@Role_Code",simsobj.comn_role_code),
                                   
                                                     
                                 });
                        if (dr.RecordsAffected > 0)
                        {
                            //if (simsobj.opr.Equals("I"))
                            //{
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    empid = dr["LoginCode"].ToString();
                                }
                            }
                            flag = true;
                            //}
                        }
                        flag = true;
                    }
                    if (flag == true)
                    {
                        if (simsobj.opr.Equals("I"))
                            message.strMessage = "Employee ID= " + empid + " " + " Employee Data Inserted Successfully";
                        else if (simsobj.opr.Equals("U"))
                            message.strMessage = "Employee Updated Successfully";

                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (simsobj.opr.Equals("U"))
                    message.strMessage = "Error In Updating Employee Details";
                else if (simsobj.opr.Equals("I"))
                    message.strMessage = "Error In Adding Employee Details";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {

            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'M'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_Company_Code = dr["co_company_code"].ToString();
                            obj.em_Company_name = dr["co_desc"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getCountry")]
        public HttpResponseMessage getCountry()
        {

            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'F'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_Country_Code = dr["sims_country_code"].ToString();
                            obj.em_Country_name = dr["sims_country_name_en"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getDestination")]
        public HttpResponseMessage getDestination()
        {

            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'E'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_Dest_Code = dr["ds_code"].ToString();
                            obj.em_Dest_name = dr["ds_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getState")]
        public HttpResponseMessage getState(string em_Country_Code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getState(),PARAMETERS ::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getState", em_Country_Code));

            List<Pers099> code_list = new List<Pers099>();
            // Pers099 simsobj = data;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'L'),
                           new SqlParameter("@em_country_name",em_Country_Code),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_State_Code = dr["sims_state_code"].ToString();
                            obj.em_State_name = dr["sims_state_name_en"].ToString();
                            //obj.em_Country_name=dr["sims_country_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getSearchEmp")]
        public HttpResponseMessage getSearchEmp(string em_login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchEmp(),PARAMETERS ::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getSearchEmp", em_login_code));

            List<Pers099> code_list = new List<Pers099>();
            // Pers099 simsobj = data;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@em_login_code",em_login_code),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_Salutation_Code = dr["em_salutation"].ToString();
                            obj.em_login_code = dr["em_login_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_middle_name = dr["em_middle_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.em_name_ot = dr["em_name_ot"].ToString();
                            obj.em_summary_address = dr["em_summary_address"].ToString();
                            obj.em_Dept_Code = dr["em_dept_code"].ToString();
                            obj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            obj.em_Grade_Code = dr["em_grade_code"].ToString();
                            obj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            obj.em_Country_Code = dr["em_country_code"].ToString();
                            obj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            obj.em_Sex_Code = dr["em_sex"].ToString();
                            obj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            obj.em_Company_Code = dr["em_company_code"].ToString();
                            obj.em_Designation_Code = dr["em_desg_code"].ToString();
                            obj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            obj.em_Nation_Code = dr["em_nation_code"].ToString();
                            obj.em_Dest_Code = dr["em_dest_code"].ToString();
                            obj.em_email = dr["em_email"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();
                            obj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            obj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            obj.em_Religion_Code = dr["em_religion_code"].ToString();
                            obj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            obj.em_blood_group_code = dr["em_blood_group_code"].ToString();
                            obj.em_passport_no = dr["em_passport_number"].ToString();
                            obj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());
                            obj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());
                            obj.em_visa_no = dr["em_visa_number"].ToString();
                            obj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());
                            obj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            obj.em_Nation_name = dr["em_national_id"].ToString();
                            obj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());
                            obj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);

            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
        }

        [Route("getCity")]
        public HttpResponseMessage getCity(string em_State_Code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCity(),PARAMETERS ::data{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getCity", em_State_Code));

            List<Pers099> code_list = new List<Pers099>();
            // Pers099 simsobj = data;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'J'),
                           new SqlParameter("@em_state_name",em_State_Code),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.em_City_Code = dr["sims_city_code"].ToString();
                            obj.em_City_name = dr["sims_city_name_en"].ToString();
                            //obj.em_Country_name=dr["sims_country_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getRole")]
        public HttpResponseMessage getRole()
        {
            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'O'),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.comn_role_code = dr["comn_role_code"].ToString();
                            obj.comn_role_name = dr["comn_role_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getLicSchool")]
        public HttpResponseMessage getLicSchool()
        {
            List<Pers099> code_list = new List<Pers099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_short_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr", 'D'),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 obj = new Pers099();
                            obj.lic_school_code = dr["lic_school_code"].ToString();
                            obj.lic_school_name = dr["lic_school_name"].ToString();
                            obj.lic_school_country = dr["lic_school_country"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

    }
}





















