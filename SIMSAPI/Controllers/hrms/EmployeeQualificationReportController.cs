﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.EmployeeQualificationReportController
{
    [RoutePrefix("api/Qualification_Report")]
    [BasicAuthentication]
    public class EmployeeQualificationReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getcompany")]
        public HttpResponseMessage getcompany()
        {
            List<EQR001> lstCuriculum = new List<EQR001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_EmployeeQualificationReport_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EQR001 sequence = new EQR001();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string comp_code)
        {
            List<EQR001> lstModules = new List<EQR001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_EmployeeQualificationReport_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comp_Code",comp_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EQR001 sequence = new EQR001();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getDepartment")]
        public HttpResponseMessage getDepartment(string comp_code,string acad_year)
        {
            List<EQR001> lstModules = new List<EQR001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_EmployeeQualificationReport_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@comp_Code",comp_code),
                            new SqlParameter("@acad_year",acad_year)
                            

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EQR001 sequence = new EQR001();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getQualification")]
        public HttpResponseMessage getQualification()
        {
            List<EQR001> lstCuriculum = new List<EQR001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_EmployeeQualificationReport_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "D"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EQR001 sequence = new EQR001();
                            sequence.pays_qualification_code = dr["pays_qualification_code"].ToString();
                            sequence.pays_qualification_desc = dr["pays_qualification_desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getQualification_Details")]
        public HttpResponseMessage getQualification_Details(string comp_code, string qcode, string de_code,string emp_name_id)
        {
            List<EQR001> lstModules = new List<EQR001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_EmployeeQualificationReport_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_Code",comp_code),
                            new SqlParameter("@qcode",qcode),
                            new SqlParameter("@de_code",de_code),
                            new SqlParameter("@emp_name_id",emp_name_id)


                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EQR001 sequence = new EQR001();
                            sequence.pays_qualification_emp_id = dr["pays_qualification_emp_id"].ToString();
                            sequence.ename = dr["ename"].ToString();
                            sequence.co_desc = dr["co_desc"].ToString();
                            sequence.em_dept_code = dr["em_dept_code"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.pays_qualification_desc = dr["pays_qualification_desc"].ToString();
                            sequence.pays_qualification_code = dr["pays_qualification_code"].ToString();
                            sequence.pays_qualification_institude_name = dr["pays_qualification_institude_name"].ToString();
                            sequence.pays_qualification_type = dr["pays_qualification_type"].ToString();
                            sequence.qualification_type = dr["qualification_type"].ToString();
                            sequence.pays_qualification_year_start_year = dr["pays_qualification_year_start_year"].ToString();
                            sequence.pays_qualification_year = dr["pays_qualification_year"].ToString();
                            sequence.from_to_year = dr["from_to_year"].ToString();
                            sequence.pays_qualification_percentage = dr["pays_qualification_percentage"].ToString();
                            sequence.pays_qualification_remark = dr["pays_qualification_remark"].ToString();
                            sequence.pays_qualification_emp_status = dr["pays_qualification_emp_status"].ToString();
                           
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

    }
}

