﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Web;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/HRMS/EmpImgShow")]
    public class EmployeeImageViewController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getEmpMasterData")]
        public HttpResponseMessage getEmpMasterData()
        {


            List<Pers255> emp_list = new List<Pers255>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetEMPData_Temp", new List<SqlParameter>());
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.em_Grade_Code = dr["Grade_Code"].ToString();
                            obj.em_Grade_Company_Code = dr["Grade_Company_code"].ToString();
                            obj.em_Grade_name = dr["Grade_Desc"].ToString();
                            obj.em_Dept_Code = dr["Department_Code"].ToString();
                            obj.em_Dept_Company_Code = dr["Department_Company_Code"].ToString();
                            obj.em_Dept_name = dr["Department_Name"].ToString();
                            obj.em_Dept_Short_Name = dr["Department_Short_Name"].ToString();
                            obj.em_Staff_Type_name = dr["Staff_Type_Value"].ToString();
                            obj.em_Staff_Type_Code = dr["Staff_Type_Code"].ToString();
                            emp_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_list);
            }
        }

        [Route("getEmployeeDetail")]
        public HttpResponseMessage getEmployeeDetail(string codp_dept_no, string gr_code, string sims_appl_parameter, string pays_service_status_code)
        {
            if (codp_dept_no == "undefined")
            { codp_dept_no = null; }
            if (gr_code == "undefined")
            { gr_code = null; }
            if (sims_appl_parameter == "undefined")
            { sims_appl_parameter = null; }
            if (pays_service_status_code == "undefined")
            { pays_service_status_code = null; }

            string cur_code = string.Empty, str = string.Empty;
            List<Pers255> empdetail = new List<Pers255>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Q"),
                             new SqlParameter("@em_dept_code", codp_dept_no),
                             new SqlParameter("@em_grade_code", gr_code),
                             new SqlParameter("@em_staff_type", sims_appl_parameter),
                             new SqlParameter("@em_service_status_code",pays_service_status_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 sequence = new Pers255();

                            sequence.em_number = dr["em_number"].ToString();

                            sequence.em_first_name = dr["em_first_name"].ToString();

                            sequence.em_last_name = dr["em_last_name"].ToString();

                            sequence.em_full_name = dr["em_first_name"].ToString() + ' ' + dr["em_last_name"];

                            sequence.em_img = dr["em_img"].ToString();

                            empdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empdetail);
        }

        [Route("getEmployeeDetailForNIS")]
        public HttpResponseMessage getEmployeeDetailForNIS(string codp_dept_no, string gr_code, string sims_appl_parameter, string pays_service_status_code, string company_code)
        {
            if (company_code == "undefined")
            { company_code = null; }
            if (codp_dept_no == "undefined")
            { codp_dept_no = null; }
            if (gr_code == "undefined")
            { gr_code = null; }
            if (sims_appl_parameter == "undefined")
            { sims_appl_parameter = null; }
            if (pays_service_status_code == "undefined")
            { pays_service_status_code = null; }

            string cur_code = string.Empty, str = string.Empty;
            List<Pers255> empdetail = new List<Pers255>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Q"),
                             new SqlParameter("@em_dept_code", codp_dept_no),
                             new SqlParameter("@em_grade_code", gr_code),
                             new SqlParameter("@em_staff_type", sims_appl_parameter),
                             new SqlParameter("@em_service_status_code",pays_service_status_code),
                             new SqlParameter("@codp_comp_code",company_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 sequence = new Pers255();

                            sequence.em_number = dr["em_number"].ToString();

                            sequence.em_first_name = dr["em_first_name"].ToString();

                            sequence.em_last_name = dr["em_last_name"].ToString();

                            sequence.em_full_name = dr["em_first_name"].ToString() + ' ' + dr["em_last_name"];

                            sequence.em_img = dr["em_img"].ToString();

                            empdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empdetail);
        }

        [Route("getDepartmentName")]
        public HttpResponseMessage getDepartmentName()
        {
            List<Pers255> departname = new List<Pers255>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.codp_dept_no = dr["codp_dept_no"].ToString();
                            obj.codp_dept_name = dr["codp_dept_name"].ToString();
                            departname.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, departname);
        }

        [Route("getGradeName")]
        public HttpResponseMessage getGradeName()
        {
            List<Pers255> gradename = new List<Pers255>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.gr_code = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();
                            gradename.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, gradename);
        }

        [Route("getGradeNameForNIS")]
        public HttpResponseMessage getGradeNameForNIS(string company_code)
        {
            List<Pers255> gradename = new List<Pers255>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@codp_comp_code",company_code),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.gr_code = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();
                            gradename.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, gradename);
        }

        [Route("getStaffType")]
        public HttpResponseMessage getStaffType()
        {
            List<Pers255> staffname = new List<Pers255>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            staffname.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, staffname);
        }

        [Route("getServiceStatus")]
        public HttpResponseMessage getServiceStatus()
        {

            List<Pers255> staffname = new List<Pers255>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AA"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers255 obj = new Pers255();
                            obj.pays_service_status_code = dr["sims_appl_parameter"].ToString();
                            obj.pays_service_status_name = dr["sims_appl_form_field_value1"].ToString();
                            staffname.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, staffname);
        }

        [Route("CUDEmployeeDetails")]
        public HttpResponseMessage CUDEmployeeDetails(Pers255 simsobj)
        {
            Message msg = new Message();
            bool deleted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",simsobj.opr),
                             new SqlParameter("@em_number", simsobj.em_number),
                               new SqlParameter("@em_img", simsobj.em_img),



                         });
                    if (dr.RecordsAffected > 0)
                    {
                        deleted = true;
                    }
                    if (deleted == true)
                    {
                        msg.strMessage = "Image Deleted Successfully";
                    }
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, deleted);
        }

        [Route("CUDUpdateEmpPics")]
        public HttpResponseMessage CUDUpdateEmpPics(Pers255 simsobj)//string opr, string data
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_proc]",
                            new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@em_number", simsobj.em_number),
                            new SqlParameter("@em_img", simsobj.em_img),

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            insert = true;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, insert);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, insert);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Deleting image Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}