﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.EmpDashboardController
{
    [RoutePrefix("api/common/Empdashboard")]
    [BasicAuthentication]
    public class EmpDashboardController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string getAcademicYear()
        {
            string sims_academic_year = "";

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'O'),
                            
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_academic_year = dr["sims_academic_year"].ToString();


                        }

                    }
                }
            }
            catch (Exception x)
            {

            }
            return sims_academic_year;
        }

        [Route("getLeaveType")]
        public HttpResponseMessage getLeaveType(string att_emp_id)
        {


            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@att_emp_id",att_emp_id)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.emp_leave_code = dr["cl_code"].ToString();
                            simsobj.emp_leave_name = dr["cl_desc"].ToString();
                            simsobj.le_max_days_allowed = dr["le_max_days_allowed"].ToString();
                            simsobj.le_days_taken = dr["el_days_taken"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getLeaveTotal")]
        public HttpResponseMessage getLeaveTotal(string att_emp_id)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@att_emp_id",att_emp_id)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.emp_leave_code = dr["cl_code"].ToString();
                            simsobj.emp_leave_name = dr["cl_desc"].ToString();
                            simsobj.le_max_days_allowed = dr["le_max_days_allowed"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getEmpAttendance")]
        public HttpResponseMessage getEmpAttendance(string emp_att_date, string emp_att_emp_id)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                             new SqlParameter("@att_date",emp_att_date),
                            new SqlParameter("@att_emp_id",emp_att_emp_id),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            //simsobj.emp_atten_flag = dr["att_flag"].ToString();
                            //simsobj.em_first_name = dr["em_first_name"].ToString();
                            simsobj.em_check_in = dr["att_shift1_in"].ToString();
                            simsobj.em_check_out = dr["att_shift2_out"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getEmpGrade")]
        public HttpResponseMessage getEmpGrade(string emp_att_emp_id, string sims_grade_code, string emp_att_date, string sims_section_code)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            EmpDashboard simsobj = new EmpDashboard();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                             new SqlParameter("@sims_employee_code",emp_att_emp_id),
                             new SqlParameter("@sims_academic_year",getAcademicYear())
                           
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //EmpDashboard simsobj = new EmpDashboard();

                            simsobj.emp_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }

                    }

                    dr.Close();

                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[pays_attendance_daily]",
                       new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'H'),
                             new SqlParameter("@att_date",db.DBYYYYMMDDformat(emp_att_date)),
                             new SqlParameter("@sims_grade_code",sims_grade_code),
                             new SqlParameter("@sims_section_code",sims_section_code),
                             new SqlParameter("@sims_academic_year",getAcademicYear())
                         });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {

                            simsobj.emp_grade_percentage = dr1["Percentage"].ToString();
                            mod_list.Add(simsobj);
                        }

                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getEmpGradePercentage")]
        public HttpResponseMessage getEmpGradePercentage(string emp_att_emp_id)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                             new SqlParameter("@sims_employee_code",emp_att_emp_id),
                             new SqlParameter("@sims_academic_year",getAcademicYear())
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.emp_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.tardy_cnt = dr["tardy_count"].ToString();
                            simsobj.emp_grade_percentage = dr["Percentage"].ToString();
                            simsobj.emp_total_stud = dr["total_stud"].ToString();
                            simsobj.emp_present_stud = dr["present_count"].ToString();
                            simsobj.emp_absent_count = dr["absent_count"].ToString();
                            simsobj.emp_absent_stud_percentage = dr["Absent_Perentages"].ToString();
                            simsobj.emp_tardy_stud_percentage = dr["tardy_per"].ToString();
                            simsobj.emp_unmark_cnt = dr["unmark_count"].ToString();
                            simsobj.emp_unmark_percentage = dr["unmark_Perentages"].ToString();
                            simsobj.emp_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    else
                    {
                        message.strMessage = "No Data Available";

                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getEmpSubjectName")]
        public HttpResponseMessage getEmpSubjectName(string emp_att_emp_id)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                             new SqlParameter("@sims_employee_code",emp_att_emp_id),
                             new SqlParameter("@sims_academic_year",getAcademicYear())
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.emp_sub_code = dr["sims_subject_code"].ToString();
                            simsobj.emp_sub_name = dr["sims_subject_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getEmpgradebook")]
        public HttpResponseMessage getEmpgradebook(string emp_att_emp_id, string sub_code)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                             new SqlParameter("@sims_employee_code",emp_att_emp_id),
                             new SqlParameter("@sims_academic_year",getAcademicYear()),
                             new SqlParameter("@sub_code",sub_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_name = dr["sims_gb_name"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    else
                    {
                        message.strMessage = "No Data Available";
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getGradeSection")]
        public HttpResponseMessage getGradeSection(string emp_academic_year, string emp_att_emp_id, string sub_code)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'R'),
                             new SqlParameter("@sims_employee_code",emp_att_emp_id),
                             new SqlParameter("@sims_academic_year",emp_academic_year),
                             new SqlParameter("@sub_code",sub_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_gb_name = dr["gradesectionname"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    else
                    {
                        message.strMessage = "No Data Available";
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        //Male Female Count


        [Route("getStudPerEnrollNumber")]
        public HttpResponseMessage getStudPerEnrollNumber(string sims_grade_code, string sims_section_code, string code, string emp_code, string stud_att_date)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_proc]",

                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'M'),
                             new SqlParameter("@sims_employee_code",emp_code),
                             new SqlParameter("@sims_grade_code",sims_grade_code),
                             new SqlParameter("@sims_section_code",sims_section_code),
                             new SqlParameter("@sims_attendance_day_attendance_code",code),
                            new SqlParameter("@sims_attendance_Date",db.DBYYYYMMDDformat(stud_att_date))
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.sims_attendance_day_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();
                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            mod_list.Add(simsobj);
                        }

                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("Getdivisionmale")]
        public HttpResponseMessage Getdivisionmale(string co_code)
        {

            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                             new SqlParameter("@year",getAcademicYear()),
                            new SqlParameter("@co_code",co_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.em_sex = dr["em_sex"].ToString();
                            simsobj.counter = dr["counter"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("GetDashboardData")]
        public HttpResponseMessage GetDashboardData()
        {

            List<EmpDashboard> dashboard_list = new List<EmpDashboard>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Q'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();

                            simsobj.sims_field_code = dr["sims_field_code"].ToString();
                            simsobj.sims_field_name = dr["sims_field_name"].ToString();
                            simsobj.sims_field_iseditable = dr["sims_field_iseditable"].ToString();
                            simsobj.sims_field_isvisible = dr["sims_field_isvisible"].ToString();
                            simsobj.sims_field_attribute = dr["sims_field_attribute"].ToString();
                            simsobj.sims_field_attribute_html = dr["sims_field_attribute_html"].ToString();

                            dashboard_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }

        [Route("GetAllPerEmpDet")]
        public HttpResponseMessage GetAllPerEmpDet(string curr_date, string status)
        {

            List<EmpDashboard> dashboard_list = new List<EmpDashboard>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'V'),
                           new SqlParameter("@att_date",db.DBYYYYMMDDformat(curr_date)),
                           new SqlParameter("@status",status)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();

                            simsobj.fullname = dr["fullname"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.att_absent_flag = dr["att_absent_flag"].ToString();
                            simsobj.em_login_code = dr["em_login_code"].ToString();

                            dashboard_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }

        [Route("GetAllDetailsEmp")]
        public HttpResponseMessage GetAllDetailsEmp(string curr_date)
        {

            List<EmpDashboard> dashboard_list = new List<EmpDashboard>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'X'),
                           new SqlParameter("att_date",db.DBYYYYMMDDformat(curr_date))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();

                            simsobj.presents = dr["presents"].ToString();
                            simsobj.absents = dr["absents"].ToString();
                            simsobj.leavewithougthallowns = dr["leavewithougthallowns"].ToString();
                            simsobj.leave = dr["leave"].ToString();
                            simsobj.unmark = dr["unmark"].ToString();
                            simsobj.total = dr["total"].ToString();

                            dashboard_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }

        //GOOGLE MAP API
        [Route("GetCountryCountData")]
        public HttpResponseMessage GetCountryCountData()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCountryCountData(),PARAMETERS :: NO";

            List<EmpDashboard> dashboard_list = new List<EmpDashboard>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           new SqlParameter("@year",getAcademicYear()),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();

                            simsobj.student_count = dr["student_count"].ToString();
                            simsobj.sims_country_code = dr["sims_country_code"].ToString();
                            simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            simsobj.sims_country_longitude = dr["sims_country_longitude"].ToString();
                            simsobj.sims_country_latitude = dr["sims_country_latitude"].ToString();
                            simsobj.male_student_count = dr["male_student_count"].ToString();
                            simsobj.female_student_count = dr["female_student_count"].ToString();

                            dashboard_list.Add(simsobj);
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }

            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }


        [Route("GetCountryCountTotal")]
        public HttpResponseMessage GetCountryCountTotal()
        {

            List<EmpDashboard> dashboard_list = new List<EmpDashboard>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'O'),
                            new SqlParameter("@year",getAcademicYear())
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();

                            simsobj.total_count = dr["total_count"].ToString();
                            simsobj.male = dr["male"].ToString();
                            simsobj.female = dr["female"].ToString();
                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }


        [Route("getHRMSData")]
        public HttpResponseMessage getHRMSData()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'D'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.visa_type = dr["visa_type"].ToString();
                            simsobj.visa_type_count = dr["visa_type_count"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }


        [Route("getStudentData")]
        public HttpResponseMessage getStudentData()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),
                            new SqlParameter("@year",getAcademicYear()),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.gender = dr["gender"].ToString();
                            simsobj.gender_count = dr["gender_count"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }


        [Route("getStaffPersonalData")]
        public HttpResponseMessage getStaffPersonalData()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'T'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            simsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            simsobj.em_desg_code = dr["em_desg_code"].ToString();
                            simsobj.em_designation = dr["em_designation"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_gender = dr["em_gender"].ToString();
                            simsobj.emp_marital_status = dr["emp_marital_status"].ToString();
                            simsobj.em_division = dr["em_division"].ToString();
                            simsobj.em_national_id = dr["em_national_id"].ToString();
                            simsobj.em_dept_code = dr["em_dept_code"].ToString();
                            simsobj.em_department = dr["em_department"].ToString();
                            simsobj.em_nationality = dr["em_nationality"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();
                            simsobj.service_status = dr["service_status"].ToString();
                            simsobj.em_religion_code = dr["em_religion_code"].ToString();
                            simsobj.religion_code = dr["religion_code"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getStaffDivisionCount")]
        public HttpResponseMessage getStaffDivisionCount()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'U'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.gd_division_desc = dr["gd_division_desc"].ToString();
                            simsobj.staff_count = dr["staff_count"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("getStaffDepartmentCount")]
        public HttpResponseMessage getStaffDepartmentCount()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'Y'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.staff_count = dr["staff_count"].ToString();
                            simsobj.department = dr["department"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getResignedstaffData")]
        public HttpResponseMessage getResignedstaffData()
        {
            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_new_dashboard_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'J'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.em_national_id = dr["em_national_id"].ToString();
                            simsobj.em_desg_code = dr["em_desg_code"].ToString();
                            simsobj.em_designation = dr["em_designation"].ToString();
                            simsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            simsobj.em_left_reason = dr["em_left_reason"].ToString();
                            simsobj.em_left_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                            simsobj.division = dr["division"].ToString();
                            simsobj.department = dr["department"].ToString();
                            simsobj.grade_name = dr["grade_name"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }



        [Route("getApplication_Dash")]
        public HttpResponseMessage getApplication_Dash(string user)
        {


            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_employee_dashboard_erp]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@comn_user_name",user)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("remove_tile")]
        public HttpResponseMessage remove_tile(string user ,string status, string appl_code)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : EventDeleteById(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "CALENDAR"));

            //   Uccw008 events = Newtonsoft.Json.JsonConvert.DeserializeObject<Uccw008>(Udata);
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_employee_dashboard_erp]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@comn_user_name",user),
                            new SqlParameter("@comn_widget_appl_code",appl_code),
                            new SqlParameter("@comn_widget_appl_visible_flag",status),
                            
                        });

                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("get_removed_Application_Dash")]
        public HttpResponseMessage get_removed_Application_Dash(string user)
        {


            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_employee_dashboard_erp]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@comn_user_name",user)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }



        [Route("get_favirate_Application_Dash")]
        public HttpResponseMessage get_favirate_Application_Dash(string user)
        {


            List<EmpDashboard> mod_list = new List<EmpDashboard>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_employee_dashboard_erp]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@comn_user_name",user)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard simsobj = new EmpDashboard();
                            simsobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            simsobj.comn_appl_location_html = dr["comn_appl_location_html"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            simsobj.comn_appl_location = dr["comn_appl_location"].ToString();

                            try { simsobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString(); }
                            catch (Exception ex) { }
                            
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }



        [Route("getInventoryDataforOrders")]
        public HttpResponseMessage getInventoryDataforOrders()
        {
            List<EmpDashboard_invs> mod_list = new List<EmpDashboard_invs>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_dashboard_query_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard_invs simsobj = new EmpDashboard_invs();

                            simsobj.item_req = dr["item_req"].ToString();
                            simsobj.service_req = dr["service_req"].ToString();

                            simsobj.req_item = dr["req_item"].ToString();
                            simsobj.req_service = dr["req_service"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("getInventoryPendingReceived")]
        public HttpResponseMessage getInventoryPendingReceived()
        {
            List<EmpDashboard_invs> mod_list = new List<EmpDashboard_invs>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_dashboard_query_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'B'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpDashboard_invs simsobj = new EmpDashboard_invs();
                            simsobj.ord_no = dr["ord_no"].ToString();
                            simsobj.orderedQty = dr["orderedQty"].ToString();

                            simsobj.receivedQty = dr["receivedQty"].ToString();
                            simsobj.totalOrderedAmount = dr["totalOrderedAmount"].ToString();
                            simsobj.totalReceivedAmount = dr["totalReceivedAmount"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("getInventoryOrders")]
        public HttpResponseMessage getInventoryOrders()
        {
            List<EmpDashboard_invs> mod_list = new List<EmpDashboard_invs>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_dashboard_query_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            EmpDashboard_invs simsobj = new EmpDashboard_invs();
                            simsobj.year = dr["year"].ToString();
                            simsobj.month = dr["month"].ToString();
                            simsobj.month_Name = dr["month_Name"].ToString();
                            simsobj.total_Order_Count = dr["total_Order_Count"].ToString();
                            simsobj.ordered_Count = dr["ordered_Count"].ToString();
                            simsobj.received_Count = dr["received_Count"].ToString();

                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("getUname")]
        public HttpResponseMessage getUname(string name)
        {
            string uname = string.Empty;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[get_uname]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'C'),
                             new SqlParameter("@UserName",name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            uname = dr["comn_user_name"].ToString();
                            

                           
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, uname);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, uname);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, uname);
            }

        }


    }
}