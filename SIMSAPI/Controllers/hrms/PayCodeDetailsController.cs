﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/PayCodeDetails")]
    public class PayCodeDetailsController : ApiController
    {

        [Route("Get_gc_company_Code")]
        public HttpResponseMessage Get_gc_company_Code()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_company_master]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.pc_company_code = dr["co_company_code"].ToString();
                        objph.company_name = dr["co_desc"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        [Route("GetGradeCode")]
        public HttpResponseMessage GetGradeCode()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.pd_grade_code = dr["gr_code"].ToString();
                        objph.pd_grade_code_nm = dr["gr_desc"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        [Route("GetPayCode")]
        public HttpResponseMessage GetPayCode()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.pd_pay_code = dr["cp_code"].ToString();
                        objph.pd_pay_code_nm = dr["cp_desc"].ToString();
                        objph.paysEarnDed = dr["pc_earn_dedn_tag"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        [Route("GetAccountNo")]
        public HttpResponseMessage GetAccountNo()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.pd_credit_acno = dr["acc_code"].ToString();
                        objph.pd_credit_acno_nm = dr["acc_name"].ToString();
                        objph.pd_debit_acno = dr["acc_code"].ToString();
                        objph.pd_debit_acno_nm = dr["acc_name"].ToString();
                        objph.pd_cash_acno = dr["acc_code"].ToString();
                        objph.pd_cash_acno_nm = dr["acc_name"].ToString();
                        objph.pd_bank_acno = dr["acc_code"].ToString();
                        objph.pd_bank_acno_nm = dr["acc_name"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }


        [Route("getPayCodeDetails")]
        public HttpResponseMessage getPayCodeDetails()
        {
            List<Pers109> pay_code_Details = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers109 obj = new Pers109();

                            obj.pd_grade_code = dr["pd_grade_code"].ToString();
                            obj.pd_grade_code_nm = dr["pd_grade_code_nm"].ToString();

                            obj.pd_pay_code = dr["pd_pay_code"].ToString();
                            obj.pd_pay_code_nm = dr["pd_pay_code_nm"].ToString();

                            obj.pd_company_code = dr["pd_company_code"].ToString();
                            obj.company_name = dr["company_name"].ToString();

                            obj.pd_amount = dr["pd_amount"].ToString();

                            obj.pd_credit_acno = dr["pd_credit_acno"].ToString();
                            obj.pd_credit_acno_nm = dr["pd_credit_acno_nm"].ToString();

                            obj.pd_debit_acno = dr["pd_debit_acno"].ToString();
                            obj.pd_debit_acno_nm = dr["pd_debit_acno_nm"].ToString();

                            obj.pd_cash_acno = dr["pd_cash_acno"].ToString();
                            obj.pd_cash_acno_nm = dr["pd_cash_acno_nm"].ToString();

                            obj.pd_bank_acno = dr["pd_bank_acno"].ToString();
                            obj.pd_bank_acno_nm = dr["pd_bank_acno_nm"].ToString();


                            pay_code_Details.Add(obj);


                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, pay_code_Details);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, pay_code_Details);
        }

        [Route("PayCodeDetailsCUD")]
        public HttpResponseMessage PayCodeDetailsCUD(Pers109 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "PayCodeDetailsCUD", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //int ins = db.ExecuteStoreProcedureforInsert("pays_pay_code_detail",
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                       
                          new SqlParameter("@pd_grade_code", simsobj.pd_grade_code),
                          new SqlParameter("@pd_pay_code",simsobj.pd_pay_code),
                          new SqlParameter("@pd_company_code",simsobj.pd_company_code),
                          new SqlParameter("@pd_amount",simsobj.pd_amount),
                          new SqlParameter("@pd_credit_acno", simsobj.pd_credit_acno),
                          new SqlParameter("@pd_debit_acno",simsobj.pd_debit_acno),
                          new SqlParameter("@pd_cash_acno",simsobj.pd_cash_acno),
                          new SqlParameter("@pd_bank_acno",simsobj.pd_bank_acno),
                                              
                        });
                    if (dr.Read())
                    {
                        st = dr[0].ToString();
                        msg.strMessage = st;

                    }
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }

                    //if (ins > 0)
                    //{
                    //    inserted = true;
                    //}
                    //else
                    //{
                    //    inserted = false;
                    //}
                    //if (inserted == true)
                    //{
                    //    msg.strMessage = "Record Inserted Succesfully";
                    //}
                    //else
                    //{ 
                    //    msg.strMessage = "Record not Inserted";
                    //}

                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("PayCodeDetailsD")]
        public HttpResponseMessage PayCodeDetailsD(List<Pers109> obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "PayCodeDetailsCUD", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (Pers109 simsobj in obj)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        //int ins = db.ExecuteStoreProcedureforInsert("pays_pay_code_detail",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                       
                          new SqlParameter("@pd_grade_code", simsobj.pd_grade_code),
                          new SqlParameter("@pd_pay_code",simsobj.pd_pay_code),
                          new SqlParameter("@pd_company_code",simsobj.pd_company_code),
                          new SqlParameter("@pd_amount",simsobj.pd_amount),
                          new SqlParameter("@pd_credit_acno", simsobj.pd_credit_acno),
                          new SqlParameter("@pd_debit_acno",simsobj.pd_debit_acno),
                          new SqlParameter("@pd_cash_acno",simsobj.pd_cash_acno),
                          new SqlParameter("@pd_bank_acno",simsobj.pd_bank_acno),
                                              
                        });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
    }
}