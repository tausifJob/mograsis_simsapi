﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/ManagerConfirmation")]
    public class ManagerConfirmationController:ApiController
    {
        [Route("getMngrConfirmation")]
        public HttpResponseMessage getMngrConfirmation()
        {
            List<Per135> mngrConfirmation_list = new List<Per135>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_manager_cofirm_proc]", //pays_manager_cofirm
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per135 obj = new Per135();
                             
                            obj.mc_number = dr["mc_number"].ToString();
                            obj.employee_name = dr["employeeName"].ToString();
                            obj.co_name = dr["co_name"].ToString();
                            obj.mc_company_code = dr["mc_company_code"].ToString();
                            obj.mc_old_basic = dr["mc_old_basic"].ToString();
                            obj.mc_new_basic = dr["mc_new_basic"].ToString();
                            obj.mc_effect_from = db.UIDDMMYYYYformat(dr["mc_effect_from"].ToString());
                            obj.mc_retro_effect_from = db.UIDDMMYYYYformat(dr["mc_retro_effect_from"].ToString());
                            obj.mc_mgr_confirm_tag = dr["mc_mgr_confirm_tag"].Equals("A") ? true : false;

                            mngrConfirmation_list.Add(obj);

                        }
                    }
                }


                

                return Request.CreateResponse(HttpStatusCode.OK, mngrConfirmation_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mngrConfirmation_list);
        }

        [Route("ManagerConfirmationCUD")]
        public HttpResponseMessage ManagerConfirmationCUD(Per135 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "ManagerConfirmationCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_manager_cofirm_proc]", //pays_manager_cofirm
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@mc_number",simsobj.mc_number),
                          new SqlParameter("@mc_company_code",simsobj.mc_company_code),
                          new SqlParameter("@mc_old_basic", simsobj.mc_old_basic),
                          new SqlParameter("@mc_new_basic", simsobj.mc_new_basic),
                          new SqlParameter("@mc_effect_from",db.DBYYYYMMDDformat(simsobj.mc_effect_from)),
                          new SqlParameter("@mc_retro_effect_from",db.DBYYYYMMDDformat(simsobj.mc_retro_effect_from)),
                          new SqlParameter("@mc_mgr_confirm_tag",simsobj.mc_mgr_confirm_tag==true?"A":"I"),
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}