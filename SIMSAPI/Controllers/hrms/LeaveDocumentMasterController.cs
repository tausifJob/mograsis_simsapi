﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.LeaveDocumentMasterController
{
    [RoutePrefix("api/LeaveDocument")]
    [BasicAuthentication]
    public class LeaveDocumentMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllLeaveDocuments")] 
        public HttpResponseMessage getAllLeaveDocuments()
        {

            List<Per256> desg_list = new List<Per256>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per256 simsobj = new Per256();
                            simsobj.pays_leave_doc_code = dr["pays_leave_doc_code"].ToString();
                            simsobj.pays_leave_doc_name = dr["pays_leave_doc_name"].ToString();
                            simsobj.pays_leave_doc_desc = dr["pays_leave_doc_desc"].ToString();
                            simsobj.pays_leave_doc_is_mandatory = dr["pays_leave_doc_is_mandatory"].Equals("Y") ? true : false;
                            simsobj.pays_leave_doc_status = dr["pays_leave_doc_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("CUDLeaveDocuments")]
        public HttpResponseMessage CUDLeaveDocuments(List<Per256> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per256 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_leave_document_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_leave_doc_code", simsobj.pays_leave_doc_code),
                                new SqlParameter("@pays_leave_doc_name", simsobj.pays_leave_doc_name),
                                new SqlParameter("@pays_leave_doc_desc", simsobj.pays_leave_doc_desc),
                                new SqlParameter("@pays_leave_doc_is_mandatory", simsobj.pays_leave_doc_is_mandatory.Equals(true)?"Y":"N"),
                                 new SqlParameter("@pays_leave_doc_status", simsobj.pays_leave_doc_status.Equals(true)?"A":"I"),
                          
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
