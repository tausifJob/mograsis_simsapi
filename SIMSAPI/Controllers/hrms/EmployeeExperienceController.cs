﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.EmployeeExperienceController
{
    [RoutePrefix("api/EmpExp")]
    [BasicAuthentication]
    public class EmployeeExperienceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllEmployeeExp")]
        public HttpResponseMessage getAllEmployeeExp()
        {

            List<EmpExp> desg_list = new List<EmpExp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_experience_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpExp simsobj = new EmpExp();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_emp_full_name = dr["em_emp_full_name"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.company_name = dr["company_name"].ToString();
                            simsobj.em_previous_job_line_no = dr["em_previous_job_line_no"].ToString();
                            simsobj.em_previous_job_company = dr["em_previous_job_company"].ToString();
                            simsobj.em_previous_job_title = dr["em_previous_job_title"].ToString();
                            simsobj.em_previous_job_start_year_month = dr["em_previous_job_start_year_month"].ToString();
                            simsobj.em_previous_job_end_year_month = dr["em_previous_job_end_year_month"].ToString();
                            simsobj.em_previous_job_responsibilities = dr["em_previous_job_responsibilities"].ToString();
                            simsobj.em_previous_job_remark = dr["em_previous_job_remark"].ToString();
                            simsobj.em_previous_job_salary_details = dr["em_previous_job_salary_details"].ToString();
                            simsobj.em_previous_job_status = dr["em_previous_job_status"].Equals("A") ? true : false;
                            simsobj.total_experience = dr["total_experience"].ToString();
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getMonthName")]
        public HttpResponseMessage getMonthName()
        {

            List<EmpExp> mod_list = new List<EmpExp>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_experience_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpExp simsobj = new EmpExp();
                            simsobj.Month_Number = dr["MonthNumber"].ToString();
                            simsobj.Month_Name = dr["MonthName"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<EmpExp> mod_list = new List<EmpExp>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_experience_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpExp simsobj = new EmpExp();
                            simsobj.co_company_code = dr["co_company_code"].ToString();
                            simsobj.co_desc = dr["co_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDEmployeeExperience")]
        public HttpResponseMessage CUDEmployeeExperience(List<EmpExp> data)
        {

            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (EmpExp simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_experience_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@em_login_code", simsobj.enroll_number),
                                new SqlParameter("@em_company_code",simsobj.co_company_code),
                                new SqlParameter("@em_previous_job_company",simsobj.prev_company_name),
                                new SqlParameter("@em_previous_job_title", simsobj.prev_job_title),
                                new SqlParameter("@em_previous_job_start_year_month", simsobj.from_year + simsobj.from_month),
                                new SqlParameter("@em_previous_job_end_year_month",simsobj.to_year + simsobj.to_month),
                                new SqlParameter("@em_previous_job_responsibilities",simsobj.prev_job_responsibilities),
                                new SqlParameter("@em_previous_job_remark", simsobj.prev_job_remark),
                                new SqlParameter("@em_previous_job_salary_details", simsobj.prev_job_salary),
                                new SqlParameter("@em_previous_job_status", simsobj.job_status.Equals(true)?"A":"I"),
                                new SqlParameter("@em_previous_job_line_no", simsobj.em_previous_job_line_no)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getEmployeeExp")]
        public HttpResponseMessage getEmployeeExp(string em_login_code)
        {

            List<EmpExp> desg_list = new List<EmpExp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_experience_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@em_login_code", em_login_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpExp simsobj = new EmpExp();
                            simsobj.enroll_number = dr["em_login_code"].ToString();
                            simsobj.em_emp_full_name = dr["em_emp_full_name"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.company_name = dr["company_name"].ToString();
                            simsobj.em_previous_job_line_no = dr["em_previous_job_line_no"].ToString();
                            simsobj.prev_company_name = dr["em_previous_job_company"].ToString();
                            simsobj.prev_job_title = dr["em_previous_job_title"].ToString();
                            simsobj.em_previous_job_start_year_month = dr["em_previous_job_start_year_month"].ToString();
                            simsobj.em_previous_job_end_year_month = dr["em_previous_job_end_year_month"].ToString();
                            simsobj.prev_job_responsibilities = dr["em_previous_job_responsibilities"].ToString();
                            simsobj.prev_job_remark = dr["em_previous_job_remark"].ToString();
                            simsobj.prev_job_salary = dr["em_previous_job_salary_details"].ToString();
                            simsobj.job_status = dr["em_previous_job_status"].Equals("A") ? true : false;
                            simsobj.total_experience = dr["total_experience"].ToString();
                            simsobj.from_year = dr["from_year"].ToString();
                            simsobj.from_month = dr["from_month"].ToString();
                            simsobj.to_year = dr["to_year"].ToString();
                            simsobj.to_month = dr["to_month"].ToString();
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getEmployeeExpByApplicant")]
        public HttpResponseMessage getEmployeeExpByApplicant(string applicant_id)
        {

            List<EmpExp> desg_list = new List<EmpExp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_experience_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@em_applicant_id", applicant_id)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpExp simsobj = new EmpExp();
                            //simsobj.enroll_number = dr["em_login_code"].ToString();
                            //simsobj.em_emp_full_name = dr["em_emp_full_name"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.company_name = dr["company_name"].ToString();
                            simsobj.em_previous_job_line_no = dr["em_previous_job_line_no"].ToString();
                            simsobj.prev_company_name = dr["em_previous_job_company"].ToString();
                            simsobj.prev_job_title = dr["em_previous_job_title"].ToString();
                            simsobj.em_previous_job_start_year_month = dr["em_previous_job_start_year_month"].ToString();
                            simsobj.em_previous_job_end_year_month = dr["em_previous_job_end_year_month"].ToString();
                            simsobj.prev_job_responsibilities = dr["em_previous_job_responsibilities"].ToString();
                            simsobj.prev_job_remark = dr["em_previous_job_remark"].ToString();
                            simsobj.prev_job_salary = dr["em_previous_job_salary_details"].ToString();
                            simsobj.job_status = dr["em_previous_job_status"].Equals("A") ? true : false;
                            simsobj.total_experience = dr["total_experience"].ToString();
                            simsobj.from_year = dr["from_year"].ToString();
                            simsobj.from_month = dr["from_month"].ToString();
                            simsobj.to_year = dr["to_year"].ToString();
                            simsobj.to_month = dr["to_month"].ToString();
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("CUDEmployeeExperienceApplicant")]
        public HttpResponseMessage CUDEmployeeExperienceApplicant(List<EmpExp> data)
        {

            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (EmpExp simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_experience_proc]",
                           new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@em_applicant_id", simsobj.em_applicant_id),
                                new SqlParameter("@em_login_code", simsobj.enroll_number),
                                new SqlParameter("@em_company_code",simsobj.co_company_code),
                                new SqlParameter("@em_previous_job_company",simsobj.prev_company_name),
                                new SqlParameter("@em_previous_job_title", simsobj.prev_job_title),
                                new SqlParameter("@em_previous_job_start_year_month", simsobj.from_year + simsobj.from_month),
                                new SqlParameter("@em_previous_job_end_year_month",simsobj.to_year + simsobj.to_month),
                                new SqlParameter("@em_previous_job_responsibilities",simsobj.prev_job_responsibilities),
                                new SqlParameter("@em_previous_job_remark", simsobj.prev_job_remark),
                                new SqlParameter("@em_previous_job_salary_details", simsobj.prev_job_salary),
                                new SqlParameter("@em_previous_job_status", simsobj.job_status.Equals(true)?"A":"I"),
                                new SqlParameter("@em_previous_job_line_no", simsobj.em_previous_job_line_no)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}
