﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/TransLeaveApproval")]
    public class TransLeaveApprovalController : ApiController
    {
        [Route("GetStatusforApproval")]
        public HttpResponseMessage GetStatusforApproval()
        {
            List<Pers318_workflow> lst = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@Opr", "E")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow obj = new Pers318_workflow();
                            obj.sims_workflow_status_code = dr["sims_appl_parameter"].ToString();
                            obj.sims_workflow_status = dr["sims_appl_form_field_value1"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetTrasactionDetailsforApproval")]
        public HttpResponseMessage GetTrasactionDetailsforApproval(string emp_id)
        {
            List<Pers318_workflow> lst_workflow = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'G'),
                              new SqlParameter("@todaydate",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                              new SqlParameter("@emp_code", emp_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow pw = new Pers318_workflow();
                            pw.sims_workflow_no = dr["sims_workflow_no"].ToString();
                            pw.sims_workflow_modcode = dr["sims_workflow_mod_code"].ToString();
                            pw.sims_workflow_applcode = dr["sims_workflow_appl_code"].ToString();
                            pw.sims_workflow_description = dr["sims_workflow_description"].ToString();

                            pw.sims_workflow_det_empno = dr["Name"].ToString();
                            pw.sims_workflow_transdate = db.UIDDMMYYYYformat(dr["sims_workflow_transdate"].ToString());//.Substring(0, dr["sims_workflow_transdate"].ToString().IndexOf(' '));
                            //pw.sims_workflow_transdate = dr["sims_workflow_transdate"].ToString().Substring(0, dr["sims_workflow_transdate"].ToString().IndexOf(' '));
                            pw.sims_workflow_transactionid = dr["sims_workflow_transactionid"].ToString();
                            pw.sims_workflow_remark = dr["sims_workflow_remark"].ToString();

                            if (pw.sims_workflow_modcode == "004" && pw.sims_workflow_applcode == "Per300")
                            {
                                pw.stVal = "L";
                                pw.sims_workflow_status_code = "P";
                                pw.sims_workflow_status = "Pending";
                            }
                            if (pw.sims_workflow_modcode == "006" && pw.sims_workflow_applcode == "Inv058")
                            {
                                pw.stVal = "R";
                                pw.sims_workflow_status = GetRequestStatusforWorkflow(pw.sims_workflow_transactionid);
                            }

                            try
                            {
                                pw.sims_workflow_det_empName= dr["empName"].ToString();
                            }
                            catch(Exception ex)
                            {
                            }
                            try
                            {
                                pw.leave_leavedesc = dr["LeaveDescription"].ToString();
                                pw.leave_sdate = db.UIDDMMYYYYformat(dr["LeaveStartDate"].ToString());
                                pw.leave_edate = db.UIDDMMYYYYformat(dr["LeaveEndDate"].ToString());
                                pw.leave_days = dr["LeaveDays"].ToString();
                                pw.leave_remark = dr["Remarks"].ToString();
                                pw.sims_balanceleaves = dr["BalanceLeaves"].ToString();
                                pw.em_doc_path = dr["em_doc_path"].ToString();
                            }
                            catch(Exception e) { }


                            lst_workflow.Add(pw);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);
        }

        [Route("GetTrasactionDetailsforApprovalold")]
        public HttpResponseMessage GetTrasactionDetailsforApprovalold()
        {
            List<Pers318_workflow> lst_workflow = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", 'P'),
                              new SqlParameter("@todaydate",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString())
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow pw = new Pers318_workflow();
                            pw.sims_workflow_no = dr["sims_workflow_no"].ToString();
                            pw.sims_workflow_modcode = dr["sims_workflow_mod_code"].ToString();
                            pw.sims_workflow_applcode = dr["sims_workflow_appl_code"].ToString();
                            pw.sims_workflow_description = dr["sims_workflow_description"].ToString();
                            pw.sims_workflow_det_empno = dr["Name"].ToString();
                            pw.sims_workflow_transdate = db.UIDDMMYYYYformat(dr["sims_workflow_transdate"].ToString());//.Substring(0, dr["sims_workflow_transdate"].ToString().IndexOf(' '));
                            //pw.sims_workflow_transdate = dr["sims_workflow_transdate"].ToString().Substring(0, dr["sims_workflow_transdate"].ToString().IndexOf(' '));
                            pw.sims_workflow_transactionid = dr["sims_workflow_transactionid"].ToString();
                            pw.sims_workflow_remark = dr["sims_workflow_remark"].ToString();

                            if (pw.sims_workflow_modcode == "004" && pw.sims_workflow_applcode == "Per300")
                            {
                                pw.stVal = "L";
                                pw.sims_workflow_status_code = "P";
                                pw.sims_workflow_status = "Pending";
                            }
                            if (pw.sims_workflow_modcode == "006" && pw.sims_workflow_applcode == "Inv058")
                            {
                                pw.stVal = "R";
                                pw.sims_workflow_status = GetRequestStatusforWorkflow(pw.sims_workflow_transactionid);
                            }
                            lst_workflow.Add(pw);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);
        }

        [Route("GetLeaveDetailsinWorkflow")]
        public HttpResponseMessage GetLeaveDetailsinWorkflow(string transaction_id)
        {
            List<Pers318_workflow> lst_workflow = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'W'),
                              new SqlParameter("@todaydate",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                              new SqlParameter("@transaction_id", transaction_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow pw = new Pers318_workflow();
                            pw.leave_empname = dr["EmployeeName"].ToString();
                            pw.leave_compname = dr["CompanyName"].ToString();
                            pw.leave_leavedesc = dr["LeaveDescription"].ToString();
                            //DateTime dt_stdate = dr["LeaveStartDate"].ToString();
                            //pw.leave_sdate = dt_stdate.Year.ToString() + '-' + dt_stdate.Month.ToString() + '-' + dt_stdate.Day.ToString();

                            //DateTime dt_stdate = Convert.ToDateTime(dr["LeaveStartDate"].ToString());
                            //pw.leave_sdate = dt_stdate.Year.ToString() + '-' + dt_stdate.Month.ToString() + '-' + dt_stdate.Day.ToString();
                            pw.leave_sdate = db.UIDDMMYYYYformat(dr["LeaveStartDate"].ToString());
                            //DateTime dt_etdate = Convert.ToDateTime(dr["LeaveEndDate"].ToString());
                            //pw.leave_edate = dt_etdate.Year.ToString() + '-' + dt_etdate.Month.ToString() + '-' + dt_etdate.Day.ToString();
                            pw.leave_edate = db.UIDDMMYYYYformat(dr["LeaveEndDate"].ToString());
                            // pw.leave_sdate = dr["LeaveStartDate"].ToString();
                            // pw.leave_edate = dr["LeaveEndDate"].ToString();
                            pw.leave_days = dr["LeaveDays"].ToString();
                            pw.leave_remark = dr["Remarks"].ToString();
                            pw.sims_balanceleaves = dr["BalanceLeaves"].ToString();
                            try
                            {
                                pw.em_doc_path = dr["em_doc_path"].ToString();
                            }
                            catch (Exception) { }

                            lst_workflow.Add(pw);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst_workflow);
        }

        private string GetRequestStatusforWorkflow(string reqno)
        {
            string str_status = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_Workflow_Process_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", 'M'),
                              new SqlParameter("@req_no", reqno)
                         });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            str_status = dr["RequestStatus"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return str_status;

            }
            return str_status;
        }

        [Route("CUpdateTransactionDetails")]
        public HttpResponseMessage CUpdateTransactionDetails(List<Pers318_workflow> data)
        {
            bool inserted = false;
            string transacid = string.Empty;
            try
            {
                if (data != null)
                {
                    string date = DateTime.Now.ToString();
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers318_workflow obj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[Sims_Workflow_Process_proc]",
                                new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", 'Y'),
                                new SqlParameter("@todaydate",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                new SqlParameter("@sims_transworkflowno", obj.sims_workflow_no),
                                new SqlParameter("@workflow_mod_code", obj.sims_workflow_modcode),
                                new SqlParameter("@workflow_appl_code", obj.sims_workflow_applcode),
                                new SqlParameter("@trans_status", obj.sims_workflow_status_code),
                                new SqlParameter("@remark",obj.sims_workflow_remark)
                           });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}