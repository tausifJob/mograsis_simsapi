﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/PayableIncrement")]
    public class PayrollincrementController : ApiController
    {



        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 hrmsObj = new Pers072();
                            hrmsObj.dept_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 hrmsObj = new Pers072();
                            hrmsObj.desg_code = dr["dg_code"].ToString();
                            hrmsObj.desg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetGrade")]
        public HttpResponseMessage GetGrade()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.gr_code = dr["gr_code"].ToString();
                            simsobj.gr_name = dr["gr_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetPayCode")]
        public HttpResponseMessage GetPayCode()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'D'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.paycode_code = dr["pc_code"].ToString();
                            simsobj.paycode_name = dr["cp_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetinvType")]
        public HttpResponseMessage GetinvType()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.inc_type = dr["pays_appl_parameter"].ToString();
                            simsobj.inc_type_name = dr["pays_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("ALLEmpDetails")]
        public HttpResponseMessage AllEmpDetails(Pers072 varlst)
        {

            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                            new SqlParameter("@pa_pay_code", varlst.paycode_code),
                            new SqlParameter("@em_dept_code", varlst.dept_code),
                            new SqlParameter("@em_desg_code", varlst.desg_code),
                            new SqlParameter("@em_grade_code", varlst.gr_code),
                            new SqlParameter("@pai_number", varlst.em_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.paycode_name = dr["paycode_name"].ToString();
                            simsobj.gr_name = dr["Grade"].ToString();
                            simsobj.inc_type_name = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.dept_name = dr["Dep_name"].ToString();
                            simsobj.desg_name = dr["Desg_name"].ToString();
                            if (dr["pai_increment_type"].ToString() == "01")
                                simsobj.inc_fixed_amout = dr["pai_fixed"].ToString();
                            else
                                simsobj.inc_fixed_amout = dr["pai_percentage"].ToString();
                            mod_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("EmpDetails")]
        public HttpResponseMessage EmpDetails(Pers072 varlst)
        {

            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@pa_pay_code", varlst.paycode_code),
                            new SqlParameter("@em_dept_code", varlst.dept_code),
                            new SqlParameter("@em_desg_code", varlst.desg_code),
                            new SqlParameter("@em_grade_code", varlst.gr_code),
                            new SqlParameter("@pai_number", varlst.em_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.paycode_code = dr["pa_pay_code"].ToString();
                            simsobj.paycode_name = dr["paycode_name"].ToString();
                            simsobj.pay_amount = "0";
                            simsobj.inc_period = "0";
                            simsobj.inc_type = "01";

                            mod_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("UpdatePaysPayable")]
        public HttpResponseMessage UpdatePaysPayable(List<Pers072> obj)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers072 data in obj)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payable_increment_proc]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "I"),
                new SqlParameter("@pa_pay_code",data.paycode_code ),
	            new SqlParameter("@em_dept_code",data.dept_code),
	            new SqlParameter("@em_desg_code", data.desg_code),
	            new SqlParameter("@em_grade_code", data.gr_code),
	            new SqlParameter("@pai_number", data.em_number),
                new SqlParameter("@pai_increment_type", data.inc_type),
                new SqlParameter("@pai_fixed_amount", data.inc_fixed_amout),
                new SqlParameter("@pai_percentage", data.inc_per),
                new SqlParameter("@pai_period", data.inc_period)
               
                 });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}