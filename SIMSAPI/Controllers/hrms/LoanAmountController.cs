﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LoanAmountDetails")]
    public class LoanAmountController:ApiController
    {


        [Route("getLoanAmount")]
        public HttpResponseMessage getLoanAmount()
        {
            List<Per130> loanAmount_list = new List<Per130>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_amount_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per130 obj = new Per130();

                            obj.la_company_code = dr["la_company_code"].ToString();
                            obj.CompanyName = dr["CompanyName"].ToString();

                            obj.la_loan_code = dr["la_loan_code"].ToString();
                            obj.LoanName = dr["LoanName"].ToString();

                            obj.la_ledger_code = dr["la_ledger_code"].ToString();
                            obj.LedgerName = dr["LedgerName"].ToString();

                            obj.la_debit_acno = dr["la_debit_acno"].ToString();
                            obj.DebitAccName = dr["DebitAccName"].ToString();

                            obj.la_grade_code = dr["la_grade_code"].ToString();
                            obj.GradeName = dr["GradeName"].ToString();

                            obj.la_amount = dr["la_amount"].ToString();
                            obj.la_no_of_installment = dr["la_no_of_installment"].ToString();
                            obj.la_comp_emp_tag = dr["la_comp_emp_tag"].ToString();
                            loanAmount_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loanAmount_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loanAmount_list);
        }

        [Route("LoanAmountCUD")]
        public HttpResponseMessage LoanAmountCUD(List<Per130> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LoanCodeCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per130 simsobj in data)
                    { 
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_loan_amount_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@COMPANY_CODE",simsobj.la_company_code),
                          new SqlParameter("@LOAN_CODE", simsobj.la_loan_code),
                          new SqlParameter("@TOTAL_INSTALLMENT",simsobj.la_no_of_installment),
                          new SqlParameter("@COMANY_EMP_TAG", "E"),
                          new SqlParameter("@LEDGER_CODE",simsobj.la_ledger_code),
                          new SqlParameter("@DEBIT_ACC_NO", simsobj.la_debit_acno),
                          new SqlParameter("@GRADE_CODE", simsobj.la_grade_code),
                            new SqlParameter("@LOAN_AMMOUNT", simsobj.la_amount)
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        
    }
}