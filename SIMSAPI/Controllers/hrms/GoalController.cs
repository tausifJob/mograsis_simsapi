﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.GoalController
{
    [RoutePrefix("api/Goal")]
    [BasicAuthentication]
    public class GoalController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllGoal")]
        public HttpResponseMessage getAllGoal()
        {
            List<Ucw242> goaltarget_list = new List<Ucw242>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read()) 
                        {
                            Ucw242 simsobj = new Ucw242();
                            simsobj.sims_sip_academic_year = dr["sims_sip_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_sip_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_sip_goal_desc = dr["sims_sip_goal_desc"].ToString();
                            simsobj.sims_sip_goal_start_date = db.UIDDMMYYYYformat(dr["sims_sip_goal_start_date"].ToString());
                            simsobj.sims_sip_goal_end_date = db.UIDDMMYYYYformat(dr["sims_sip_goal_end_date"].ToString());
                            simsobj.sims_sip_goal_revision_type = dr["sims_sip_goal_revision_type"].ToString();
                            simsobj.sims_sip_goal_revision_type_period = dr["sims_sip_goal_revision_type_period"].ToString();
                            simsobj.sims_sip_goal_max_point =dr["sims_sip_goal_max_point"].ToString();
                            simsobj.sims_sip_goal_min_point =dr["sims_sip_goal_min_point"].ToString();
                            simsobj.sims_sip_grading_scale = dr["sims_sip_grading_scale"].ToString();
                            simsobj.sims_sip_goal_status = dr["sims_sip_goal_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("InsertUpdateGoal")]
        public HttpResponseMessage InsertUpdateGoal(List<Ucw242> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Ucw242 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_goal_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_academic_year", simsobj.sims_sip_academic_year),
                                new SqlParameter("@sims_sip_goal_code", simsobj.sims_sip_goal_code),
                                new SqlParameter("@sims_sip_goal_desc", simsobj.sims_sip_goal_desc),
                                new SqlParameter("@sims_sip_goal_start_date",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_start_date)),
                                new SqlParameter("@sims_sip_goal_end_date",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_end_date)),
                                new SqlParameter("@sims_sip_goal_revision_type",simsobj.sims_sip_goal_revision_type),
                                new SqlParameter("@sims_sip_goal_revision_type_period",simsobj.sims_sip_goal_revision_type_period),
                                new SqlParameter("@sims_sip_goal_max_point",simsobj.sims_sip_goal_max_point),
                                new SqlParameter("@sims_sip_goal_min_point",simsobj.sims_sip_goal_min_point),
                                new SqlParameter("@sims_sip_goal_status",simsobj.sims_sip_goal_status.Equals(true)?"A":"I"),
                                new SqlParameter("@sims_sip_grading_scale", simsobj.sims_sip_grading_scale),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("Get_revision_type")]
        public HttpResponseMessage Get_revision_type()
        {
            List<Ucw242> goaltarget_list = new List<Ucw242>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw242 simsobj = new Ucw242();
                            simsobj.sims_sip_goal_revision_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_sip_goal_revision_type_name = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        //[OperationContract]
        //public List<Uccw241> Get_revision_type()
        //{
        //    List<Uccw241> mod_list = new List<Uccw241>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("GetData", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("tbl_name", "[sims].[sims_parameter]");
        //        cmd.Parameters.AddWithValue("tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1");
        //        cmd.Parameters.AddWithValue("@tbl_cond", "[sims_appl_code]='Ucw241'");

        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            Uccw241 simsobj = new Uccw241();
        //            simsobj.sims_sip_goal_revision_type = dr["sims_appl_parameter"].ToString();
        //            simsobj.sims_sip_goal_revision_type_name = dr["sims_appl_form_field_value1"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return mod_list;
        //    }
        //}

       // select sims_appl_parameter,sims_appl_form_field_value1 from [sims].[sims_parameter] where [sims_appl_code]='Ucw241'





    }
}




