﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/HRMS")]
    [BasicAuthentication]
    public class MeetingManagementSchedulReport_RptController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getmeetingcode")]
        public HttpResponseMessage getmeetingcode()
        {
            List<MMSR20> lstmeetingcode = new List<MMSR20>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[MeetingScheduleReport_Rpt]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "T"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            MMSR20 sequence = new MMSR20();
                            sequence.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            sequence.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            lstmeetingcode.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstmeetingcode);
        }

         [Route("getstatus")]
        public HttpResponseMessage getstatus()
        {
            List<MMSR20> lststatus = new List<MMSR20>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[MeetingScheduleReport_Rpt]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "U"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            MMSR20 sequence = new MMSR20();
                            sequence.status = dr["status"].ToString();
                            sequence.status_desc = dr["status_desc"].ToString();
                            lststatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lststatus);
        }

        [Route("getMeetingManagementSchedulReport_Rpt")]
        public HttpResponseMessage getMeetingManagementSchedulReport_Rpt(string mom_type, string mom_start_date, string mom_end_date,string status)
        {
            List<MMSR20> lstmeetingcode = new List<MMSR20>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[MeetingScheduleReport_Rpt]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@mom_type", mom_type),
                                     new SqlParameter("@mom_start_date", db.DBYYYYMMDDformat(mom_start_date)),
                                     new SqlParameter("@mom_end_date", db.DBYYYYMMDDformat(mom_end_date)),
                                     new SqlParameter("@status", status)



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            MMSR20 sequence = new MMSR20();
                            sequence.sims_mom_reference_number = dr["sims_mom_reference_number"].ToString();
                            sequence.sims_mom_type_desc_en = dr["sims_mom_type_desc_en"].ToString();
                            sequence.sims_mom_subject = dr["sims_mom_subject"].ToString();
                            sequence.sims_mom_date = dr["sims_mom_date"].ToString();
                            sequence.requester = dr["Requester"].ToString();
                            sequence.chair_person = dr["Chair_person"].ToString();
                            sequence.no_of_attendees = int.Parse(dr["no_of_attendees"].ToString());
                            sequence.sims_mom_status = dr["sims_mom_status"].ToString();
                            sequence.sims_mom_type_code = dr["sims_mom_type_code"].ToString();
                            lstmeetingcode.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstmeetingcode);
        }

    }
}