﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.EmpGratuityViewController
{
    [RoutePrefix("api/EmpGrtview")]
    [BasicAuthentication]
    public class EmpGratuityViewController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'K'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.company_code = dr["co_company_code"].ToString();
                            simsobj.company_name = dr["co_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {

                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDeptName")]
        public HttpResponseMessage getDeptName(string comp_code)
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@comp_code",comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDesignation")]
        public HttpResponseMessage getDesignation()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'J'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dg_code = dr["dg_code"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getYear")]
        public HttpResponseMessage getYear()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_start_date = DateTime.Parse(dr["sims_academic_year_start_date"].ToString()).ToShortDateString();
                            simsobj.sims_academic_year_end_date = DateTime.Parse(dr["sims_academic_year_end_date"].ToString()).ToShortDateString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAllGratuityData")]
        public HttpResponseMessage getAllGratuityData(string comp_code, string dept_code, string desg_code, string emp_no, string yearmonth,string grtyearmonth)
        {

            if (comp_code == "undefined")
            { comp_code = null; }
            if (dept_code == "undefined")
            { dept_code = null; }
            if (desg_code == "undefined")
            { desg_code = null; }
            if (emp_no == "undefined")
            { emp_no = null; }
            if (yearmonth == "undefined")
            { yearmonth = null; }
            if (grtyearmonth == "undefined")
            { grtyearmonth = null; }

            List<PerGrt> desg_list = new List<PerGrt>();
            string str = "";
            decimal grt = 0;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@dept_code",dept_code),
                            new SqlParameter("@desg_code",desg_code),
                            new SqlParameter("@emp_no",emp_no),
                            new SqlParameter("@yearmonth",yearmonth),
                            new SqlParameter("@grtyearmonth",grtyearmonth)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["em_login_code"].ToString();
                            var v = (from p in desg_list where p.em_login_code == str select p);

                            if (v.Count() == 0)
                            {                               
                                PerGrt simsobj = new PerGrt();
                                simsobj.paycodeList = new List<payCodeList1>();
                                simsobj.em_login_code = dr["em_login_code"].ToString();                                
                                simsobj.emp_name = dr["emp_name"].ToString();
                                simsobj.experience = dr["experience_in_year"].ToString();
                                simsobj.desg_name = dr["desg_name"].ToString();
                                simsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());                                
                                simsobj.grt_amount = dr["grt_amount"].ToString();
                                /* Convert total amount into decimal */
                                //decimal temp_grt_amount = Convert.ToDecimal(dr["total_grt_amount"].ToString());                               
                                //temp_grt_amount = System.Math.Round(temp_grt_amount);
                                simsobj.total_grt_amount = dr["total_grt_amount"].ToString();
                                simsobj.grt_criteria_code = dr["grt_criteria_code"].ToString();
                                simsobj.grt_working_days = dr["grt_working_days"].ToString();
                                simsobj.em_company_code = dr["em_company_code"].ToString();
                                simsobj.current_month_amount = dr["current_month_amount"].ToString();
                                simsobj.year_number = Int32.Parse(dr["year_number"].ToString());
                                simsobj.month_number = Int32.Parse(dr["month_number"].ToString());
                                simsobj.year_month_days = Int32.Parse(dr["year_month_days"].ToString());
                                simsobj.grt_posting_date = db.UIDDMMYYYYformat(dr["grt_posting_date"].ToString());
                                simsobj.grt_posting_status = dr["grt_posting_status"].ToString();

                                payCodeList1 sb = new payCodeList1();
                                sb.pa_pay_code = dr["pa_pay_code"].ToString();
                                sb.pa_amount = dr["pa_amount"].ToString();
                                sb.pay_discription = dr["pay_discription"].ToString();
                               
                                simsobj.paycodeList.Add(sb);
                               
                                desg_list.Add(simsobj);
                            }
                            else
                            {
                                payCodeList1 sb = new payCodeList1();
                                sb.pa_pay_code = dr["pa_pay_code"].ToString();
                                sb.pa_amount = dr["pa_amount"].ToString();
                                sb.pay_discription = dr["pay_discription"].ToString();
                               
                                v.ElementAt(0).paycodeList.Add(sb);
                            }

                                                         
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("SaveAllGratuityData")]
        public HttpResponseMessage SaveAllGratuityData(string data1, List<PerGrt> data)
        {
            bool Updated = false;
            PerGrt grtObj = Newtonsoft.Json.JsonConvert.DeserializeObject<PerGrt>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (PerGrt obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_gratuity_view]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@em_login_code", obj.em_login_code),
                                new SqlParameter("@total_grt_amount", obj.total_grt_amount),
                                new SqlParameter("@em_company_code", obj.em_company_code),
                                new SqlParameter("@grt_working_days", obj.grt_working_days),
                                new SqlParameter("@grt_criteria_code", obj.grt_criteria_code),
                                new SqlParameter("@yearmonth", grtObj.yearmonth),
                                new SqlParameter("@grtyearmonth", grtObj.grtyearmonth),
                                new SqlParameter("@username", grtObj.username),
                                new SqlParameter("@current_month_amount", obj.current_month_amount),
                                new SqlParameter("@grt_posting_date", db.DBYYYYMMDDformat(obj.grt_posting_date))
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            Updated = true;
                        }
                        else
                        {
                            Updated = false;
                        }
                        dr.Close();
                    }
                }
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }

        [Route("PostingGratuity")]
        public HttpResponseMessage PostingGratuity(string year_month, string emp_id,string company_code,string username)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_posting_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'P'),
                                new SqlParameter("@year_month", year_month),
                                //new SqlParameter("@process_date", db.DBYYYYMMDDformat(finalize_date)),
                                new SqlParameter("@fins_emp_payroll_list", emp_id),
                                new SqlParameter("@company_code", company_code),
                                new SqlParameter("@grt_updated_user", username)
                        });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}
