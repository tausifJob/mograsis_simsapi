﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmpDocumentUp")]
    public class EmployeeDocumentUploadController:ApiController
    {
        static string root = "http://localhost/SIMSAPI/Content/databaseConnectionString/UploadedFiles/";

        [Route("getEmpDocumentDetail")]
        public HttpResponseMessage getEmpDocumentDetail()
        {
            List<Pers312> empDocDetail = new List<Pers312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers312 objone = new Pers312();

                            objone.pays_doc_code = dr["pays_doc_code"].ToString();
                            objone.pays_doc_mod_code = dr["pays_doc_mod_code"].ToString();
                            objone.pays_doc_srno = dr["pays_doc_srno"].ToString();
                            objone.pays_doc_empl_id = dr["pays_doc_empl_id"].ToString();
                            objone.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            objone.pays_doc_path = dr["pays_doc_path"].ToString();
                            objone.pays_doc_issue_date = db.UIDDMMYYYYformat(dr["pays_doc_issue_date"].ToString());
                            objone.pays_doc_expiry_date = db.UIDDMMYYYYformat(dr["pays_doc_expiry_date"].ToString());
                           
                            if (dr["pays_doc_status"].ToString() == "Y")
                                objone.pays_doc_status = true;
                            else
                                objone.pays_doc_status = false;

                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("EmpDocDetailCUD")]
        public HttpResponseMessage EmpDocDetailCUD(List<Pers312> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers312 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@pays_doc_code", simsobj.pays_doc_code),
                                 new SqlParameter("@pays_doc_mod_code", simsobj.pays_doc_mod_code),
                                 new SqlParameter("@pays_doc_srno", simsobj.pays_doc_code),
                                 new SqlParameter("@pays_doc_empl_id", simsobj.em_first_name),
                                 new SqlParameter("@pays_doc_desc", simsobj.pays_doc_desc),
                                 new SqlParameter("@pays_doc_path", simsobj.pays_doc_path),
                                 new SqlParameter("@pays_doc_issue_date", db.DBYYYYMMDDformat(simsobj.pays_doc_issue_date)),
                                 new SqlParameter("@pays_doc_expiry_date", db.DBYYYYMMDDformat(simsobj.pays_doc_expiry_date)),
                                 new SqlParameter("@pays_doc_status", simsobj.pays_doc_status==true?"Y":"N")


                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("searchEmployeeList")]
        public HttpResponseMessage searchEmployeeList()
        {
            List<Pers312> empDetail = new List<Pers312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers312 obj = new Pers312();

                            obj.em_login_code = dr["em_login_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.fullname = dr["fullname"].ToString();
                            obj.em_mobile = dr["em_mobile"].ToString();
                            obj.em_email = dr["em_email"].ToString();
                            obj.em_status = dr["em_status"].ToString();

                            empDetail.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDetail);
        }

        [Route("getEmpDocCode")]
        public HttpResponseMessage getEmpDocCode()
        {
            List<Pers312> empDocDetail = new List<Pers312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers312 obj = new Pers312();

                            obj.pays_doc_code = dr["pays_doc_code"].ToString();
                            obj.pays_doc_mod_code = dr["pays_doc_mod_code"].ToString();
                            empDocDetail.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }
    }
}