﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;
//using SIMSAPI.Controllers.EmpAttendance;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/EmpAttendanceController")]
    public class EmpAttendanceController : ApiController
    {
        //  public int sp_call_att_codes = 0;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);





        [Route("EDA355_Get_attednce_deatils")]
        public HttpResponseMessage EDA355_Get_attednce_deatils(string sdate, string edate, string dept = null, string desg = null)
        {
            List<Pers326> trans_list = new List<Pers326>();

            try
            {
                string[] spilitsdate = sdate.Split('-');
                string yeartemp = spilitsdate[0];
                if (yeartemp.Length >= 4)
                {
                    var day = sdate.Split('-')[2];
                    var month = sdate.Split('-')[1];
                    var year = sdate.Split('-')[0];

                    sdate = day + "-" + month + "-" + year;
                }
                string[] spilitedate = edate.Split('-');
                string yeartemp1 = spilitedate[0];
                if (yeartemp1.Length >= 4)
                {
                    var day = edate.Split('-')[2];
                    var month = edate.Split('-')[1];
                    var year = edate.Split('-')[0];

                    edate = day + "-" + month + "-" + year;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_attendance]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", 'Q'),
                new SqlParameter("@dep", dept),
                new SqlParameter("@desg", desg),
                 new SqlParameter("@from_date", db.DBYYYYMMDDformat(sdate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(edate)),
                //new SqlParameter("@from_date", DateTime.Parse(sdate).Year + "-" + DateTime.Parse(sdate).Month + "-" + DateTime.Parse(sdate).Day),
                //new SqlParameter("@to_date", DateTime.Parse(edate).Year + "-" + DateTime.Parse(edate).Month + "-" + DateTime.Parse(edate).Day),
                         });
                    while (dr.Read())
                    {
                        Pers326 simsobj = new Pers326();
                        simsobj.att_user_id = dr["att_user_id"].ToString();
                        simsobj.att_emp_id = dr["att_emp_id"].ToString();
                        simsobj.emp_name = dr["emp_name"].ToString();
                        simsobj.att_shift_no = dr["sh_shift_id"].ToString();
                        simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                        simsobj.dg_desc = dr["dg_desc"].ToString();

                        simsobj.total_hours = dr["total_hours"].ToString();


                        try
                        {
                            simsobj.att_remarks = dr["att_rem"].ToString();
                            simsobj.att_description = dr["att_description"].ToString();

                        }
                        catch (Exception ex)
                        {

                        }
                        simsobj.att_date = dr["att_date"].ToString(); //DateTime.Parse(dr["att_date"].ToString()).ToShortDateString();
                        simsobj.expected1In = dr["expected1In"].ToString();
                        simsobj.expected2In = dr["expected2In"].ToString();
                        simsobj.att_shift1_in = dr["att_shift1_in"].ToString();
                        simsobj.att_shift2_in = dr["att_shift2_in"].ToString();
                        simsobj.att1_in = (DateTime.Parse(simsobj.expected1In) - DateTime.Parse(simsobj.att_shift1_in)).ToString();
                        simsobj.att2_in = (DateTime.Parse(simsobj.expected2In) - DateTime.Parse(simsobj.att_shift2_in)).ToString();

                        var s1 = simsobj.att1_in.Substring(0, 1);
                        if (s1 == "-")
                            simsobj.s1_in = s1;
                        else
                            simsobj.s1_in = "+";
                        var s2 = simsobj.att2_in.Substring(0, 1);
                        if (s2 == "-")
                            simsobj.s2_in = s2;
                        else
                            simsobj.s2_in = "+";

                        simsobj.expected1Out = dr["expected1Out"].ToString();
                        simsobj.expected2Out = dr["expected2Out"].ToString();
                        simsobj.att_shift1_out = dr["att_shift1_out"].ToString();
                        simsobj.att_shift2_out = dr["att_shift2_out"].ToString();
                        simsobj.att1_out = (DateTime.Parse(simsobj.att_shift1_out) - DateTime.Parse(simsobj.expected1Out)).ToString();
                        simsobj.att2_out = (DateTime.Parse(simsobj.att_shift2_out) - DateTime.Parse(simsobj.expected2Out)).ToString();

                        var v1 = simsobj.att1_out.Substring(0, 1);
                        if (v1 == "-")
                            simsobj.s1_out = v1;
                        else
                            simsobj.s1_out = "+";
                        var v2 = simsobj.att2_out.Substring(0, 1);
                        if (v2 == "-")
                            simsobj.s2_out = v2;
                        else
                            simsobj.s2_out = "+";

                        simsobj.att1_delay = dr["att1_delay"].ToString();
                        simsobj.att2_delay = dr["att2_delay"].ToString();
                        simsobj.att1_early = dr["att1_early"].ToString();
                        simsobj.att2_early = dr["att2_early"].ToString();
                        simsobj.att_wrkd_hrs = dr["att_wrkd_hrs"].ToString();
                        simsobj.att_absent_flag = dr["att_absent_flag"].ToString();
                        try
                        {
                            // simsobj.leave_name = dr["leave_name"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }

                        trans_list.Add(simsobj);
                        simsobj.lst_leave = new List<leave_data>();

                    }
                    try
                    {
                        dr.NextResult();
                        List<leave_data> temp = new List<leave_data>();
                        while (dr.Read())
                        {
                            leave_data obj = new leave_data();
                            obj.emp_id = dr["em_login_code"].ToString();
                            var v = trans_list.Where(q => q.att_emp_id == obj.emp_id).ToList();
                            // var v = trans_list.ToList();
                            if (v.Count() > 0)
                            {
                                obj.leave_code = dr["el_leave_code"].ToString();
                                obj.leave_name = dr["cl_desc"].ToString();
                                obj.leave_pending = dr["pending_leave"].ToString();
                                temp.Add(obj);
                            }

                        }
                        List<string> empls = temp.Select(q => q.emp_id).Distinct().ToList();
                        foreach (var item in empls)
                        {
                            var v = temp.Where(q => q.emp_id == item).ToList();
                            trans_list.Where(q => q.att_emp_id == item).ToList().ForEach(q => q.lst_leave = v);
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [HttpGet]
        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode(string str)
        {
            List<Per313> list = new List<Per313>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_attendance_code = dr["pays_attendance_code"].ToString();
                            simsobj.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                            simsobj.pays_attendance_description = dr["pays_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["pays_attendance_color_code"].ToString()))
                            {
                                simsobj.pays_attendance_color = string.Format("#{0}", dr["pays_attendance_color_code"].ToString().Substring(4));
                            }
                            if (str.Contains(simsobj.pays_attendance_code))
                            {
                                list.Add(simsobj);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


    }
}


