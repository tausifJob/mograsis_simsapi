﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.GoalTargetKPIMeasureController
{
    [RoutePrefix("api/GoalTargetKPIMeasure")]
    [BasicAuthentication]
    public class GoalTargetKPIMeasureController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllGoalTargetKPIMeasure")]
        public HttpResponseMessage getAllGoalTargetKPIMeasure()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGoalTargetKPIMeasure(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllGoalTargetKPIMeasure"));

            List<Ucw244> goaltargetkpi_list = new List<Ucw244>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw244 simsobj = new Ucw244();
                            simsobj.sims_sip_academic_year = dr["sims_sip_academic_year"].ToString();
                            simsobj.sims_sip_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_sip_goal_desc = dr["sims_sip_goal_desc"].ToString();
                            simsobj.sims_sip_goal_target_code = dr["sims_sip_goal_target_code"].ToString();
                            simsobj.sims_sip_goal_target_desc = dr["sims_sip_goal_target_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_code = dr["sims_sip_goal_target_kpi_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_desc = dr["sims_sip_goal_target_kpi_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_auto_code = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_code = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_min_point = dr["sims_sip_goal_target_kpi_measure_min_point"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_max_point = dr["sims_sip_goal_target_kpi_measure_max_point"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_status = dr["sims_sip_goal_target_kpi_measure_status"].Equals("A") ? true : false;
                            goaltargetkpi_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltargetkpi_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltargetkpi_list);
        }

        [Route("getGoalTargetKPIMeasure")]
        public HttpResponseMessage getGoalTargetKPIMeasure(string sims_sip_goal_code, string sims_sip_goal_target_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGoalTargetKPIMeasure()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getGoalTargetKPIMeasure"));

            List<Ucw244> goalTarget_list = new List<Ucw244>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_sip_goal_code",sims_sip_goal_code),
                            new SqlParameter("@sims_sip_goal_target_code",sims_sip_goal_target_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw244 simsobj = new Ucw244();
                            simsobj.sims_sip_goal_target_kpi_code = dr["sims_sip_goal_target_kpi_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_desc = dr["sims_sip_goal_target_kpi_desc"].ToString();
                            goalTarget_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAutoGenerateCode")]
        public HttpResponseMessage getAutoGenerateCode()
        {

            List<Ucw244> srno_list = new List<Ucw244>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw244 simsobj = new Ucw244();
                            simsobj.sims_sip_goal_target_kpi_measure_auto_code = dr["Target_KPI_Measure_code"].ToString();
                            srno_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDGoalTargetKPIMeasure")]
        public HttpResponseMessage CUDGoalTargetKPIMeasure(List<Ucw244> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Ucw244 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_goal_target_kpi_measure_proc]",
                            new List<SqlParameter>() 
                         { 
		 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_academic_year", simsobj.sims_sip_academic_year),
                                new SqlParameter("@sims_sip_goal_code", simsobj.sims_sip_goal_code),
                                new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_sip_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_code", simsobj.sims_sip_goal_target_kpi_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_code",simsobj.sims_sip_goal_target_kpi_measure_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_code",simsobj.sims_sip_goal_target_kpi_measure_auto_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_desc",simsobj.sims_sip_goal_target_kpi_measure_desc),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_min_point",simsobj.sims_sip_goal_target_kpi_measure_min_point),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_max_point",simsobj.sims_sip_goal_target_kpi_measure_max_point),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_status",simsobj.sims_sip_goal_target_kpi_measure_status.Equals(true)?"A":"I"),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}




