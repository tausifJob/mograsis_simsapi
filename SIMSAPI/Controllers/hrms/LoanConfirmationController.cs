﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LoanCodeConfirmation")]
    public class LoanConfirmationController:ApiController
    {

        [Route("getAllLoanConfirmation")]
        public HttpResponseMessage getAllLoanConfirmation()
        {
            List<Per132> loanConfirmation_list = new List<Per132>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_confirmation_proc]",  //dbo.pays_loan_confirmation
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per132 obj = new Per132();

                         
                            obj.lm_company_code = dr["lm_company_code"].ToString();
                            obj.CompanyName = dr["CompanyName"].ToString();
                            obj.lm_year_month = dr["lm_year_month"].ToString();

                            obj.lm_number = dr["lm_number"].ToString();
                            obj.EmployeeName = dr["employeeName"].ToString();
                            obj.lm_loan_code = dr["lm_loan_code"].ToString();
                            obj.LoanName = dr["LoanName"].ToString();
                            obj.lm_sanction_date = db.UIDDMMYYYYformat(dr["lm_sanction_date"].ToString());
                            obj.lm_amount = dr["lm_amount"].ToString();
                            obj.lm_mgr_confirm_tag = dr["lm_mgr_confirm_tag"].ToString();
                           // obj.lm_mgr_confirm_tag = dr["lm_mgr_confirm_tag"].Equals("Y") ? true : false;
                            obj.managerConfirmTag = dr["managerConfirmTag"].ToString();
                            

                            loanConfirmation_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loanConfirmation_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loanConfirmation_list);
        }

        [Route("LoanCodeConfirmationCUD")]
        public HttpResponseMessage LoanCodeConfirmationCUD(Per132 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LoanCodeConfirmationCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_loan_confirmation_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@LoanNumber",simsobj.lm_number),
                          new SqlParameter("@CompanyCode",simsobj.lm_company_code),
                          new SqlParameter("@LoanCode", simsobj.lm_loan_code),
                          new SqlParameter("@SanctionDate", db.DBYYYYMMDDformat(simsobj.lm_sanction_date)),
                          new SqlParameter("@Ammount",simsobj.lm_amount),
                          new SqlParameter("@YearMonth",simsobj.lm_year_month),
                          new SqlParameter("@ManagerConfirmTag",simsobj.lm_mgr_confirm_tag),
                          //new SqlParameter("@ManagerConfirmTag",simsobj.lm_mgr_confirm_tag==true?"Y":"N"),
                        });
                    
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}