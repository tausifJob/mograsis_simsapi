﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/OverTimeRate")]
    public class OvertimeRateController:ApiController
    {
        [Route("getformuladesc")]
        public HttpResponseMessage getformuladesc()
        {
            List<Per138> formuladesc_list = new List<Per138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_ot_rate_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'F')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per138 obj = new Per138();
                            obj.fm_code = dr["fm_code"].ToString();
                            obj.fm_desc = dr["fm_desc"].ToString();

                            formuladesc_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, formuladesc_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, formuladesc_list);
        }


        [Route("getOverTimeRateDetails")]
        public HttpResponseMessage getOverTimeRateDetails()
        {
            List<Per138> OverTimeRate_list = new List<Per138>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_ot_rate_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S')

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per138 obj = new Per138();

                            obj.dr_company_code = dr["dr_company_code"].ToString();
                            obj.dr_company_name = dr["dr_company_name"].ToString();
                            obj.dr_dept_code = dr["dr_dept_code"].ToString();
                            obj.dr_dept_name = dr["dr_dept_name"].ToString();
                            obj.dr_grade_code = dr["dr_grade_code"].ToString();
                            obj.dr_grade_name = dr["dr_grade_name"].ToString();

                            obj.dr_ot_formula = dr["dr_ot_formula"].ToString();
                            obj.dr_ot_formula_name = dr["dr_ot_formula_name"].ToString();
                            obj.dr_ot_rate = dr["dr_ot_rate"].ToString();

                            obj.dr_holiday_ot_formula = dr["dr_holiday_ot_formula"].ToString();
                            obj.dr_holiday_ot_formula_name = dr["dr_holiday_ot_formula_name"].ToString();
                            obj.dr_holiday_ot_rate = dr["dr_holiday_ot_rate"].ToString();

                            obj.dr_special_ot_formula = dr["dr_special_ot_formula"].ToString();
                            obj.dr_special_ot_formula_name = dr["dr_special_ot_formula_name"].ToString();
                            obj.dr_special_ot_rate = dr["dr_special_ot_rate"].ToString();

                            obj.dr_special_holi_ot_formula = dr["dr_special_holi_ot_formula"].ToString();
                            obj.dr_special_holi_ot_formula_name = dr["dr_special_holi_ot_formula_name"].ToString();
                            obj.dr_special_holi_ot_rate = dr["dr_special_holi_ot_rate"].ToString();

                            obj.dr_travel_rate = dr["dr_travel_rate"].ToString();


                            OverTimeRate_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, OverTimeRate_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OverTimeRate_list);
        }


        [Route("OvertimeRateDetailsCUD")]
        public HttpResponseMessage OvertimeRateDetailsCUD(List<Per138> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LoanCodeCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per138 simsobj in data)
                    {
                        if (simsobj.dr_ot_formula == "")
                        {
                            simsobj.dr_ot_formula = null;
                        }
                        if (simsobj.dr_ot_rate == "")
                        {
                            simsobj.dr_ot_rate = null;
                        }
                        if (simsobj.dr_holiday_ot_formula == "")
                        {
                            simsobj.dr_holiday_ot_formula = null;
                        }
                        if (simsobj.dr_holiday_ot_rate == "")
                        {
                            simsobj.dr_holiday_ot_rate = null;
                        }
                        if (simsobj.dr_special_ot_formula == "")
                        {
                            simsobj.dr_special_ot_formula = null;
                        }
                        if (simsobj.dr_special_ot_rate == "")
                        {
                            simsobj.dr_special_ot_rate = null;
                        }
                        if (simsobj.dr_special_holi_ot_formula == "")
                        {
                            simsobj.dr_special_holi_ot_formula = null;
                        }
                        if (simsobj.dr_special_holi_ot_rate == "")
                        {
                            simsobj.dr_special_holi_ot_rate = null;
                        }
                        if (simsobj.dr_travel_rate == "")
                        {
                            simsobj.dr_travel_rate = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_ot_rate_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@dr_company_code",simsobj.dr_company_code),
                          new SqlParameter("@dr_dept_code", simsobj.dr_dept_code),
                          new SqlParameter("@dr_grade_code",simsobj.dr_grade_code),

                          new SqlParameter("@dr_ot_formula",simsobj.dr_ot_formula),
                          new SqlParameter("@dr_ot_rate",simsobj.dr_ot_rate),

                          new SqlParameter("@dr_holiday_ot_formula", simsobj.dr_holiday_ot_formula),
                          new SqlParameter("@dr_holiday_ot_rate", simsobj.dr_holiday_ot_rate),

                          new SqlParameter("@dr_special_holi_ot_formula", simsobj.dr_special_holi_ot_formula),
                          new SqlParameter("@dr_special_holi_ot_rate",simsobj.dr_special_holi_ot_rate),

                          new SqlParameter("@dr_special_ot_formula",simsobj.dr_special_ot_formula),
                          new SqlParameter("@dr_special_ot_rate",simsobj.dr_special_ot_rate),

                          new SqlParameter("@dr_travel_rate", simsobj.dr_travel_rate)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}