﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/VacancyMst")]
    [BasicAuthentication]
    public class VacancyViewController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region  API For Vacancy View...

        [Route("Getdesignation12")]
        public HttpResponseMessage Getdesignation12()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getdesignation12(),PARAMETERS :: NO";

            List<Per331> dashboard_list = new List<Per331>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_vacancy_master_proc]",
                        new List<SqlParameter>()
                        {
                           new SqlParameter("@opr", 'Q'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per331 simsobj = new Per331();
                            simsobj.dg_code = dr["dg_code"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }

        [Route("getAllvacancy")]
        public HttpResponseMessage getAllvacancy(string frmdate,string desig,string experience)
        {
            List<Per331> achieve_list = new List<Per331>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_vacancy_master_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@pays_vacancy_posting_date", frmdate),
                            new SqlParameter("@pays_vacancy_profile_desg_code", desig),
                            new SqlParameter("@pays_vacancy_min_experience", experience),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per331 simsobj = new Per331();
                            simsobj.pays_sr_no= dr["pays_sr_no"].ToString();
                            simsobj.pays_vacancy_id = dr["pays_vacancy_id"].ToString();
                            simsobj.pays_vacancy_profile_desg_code = dr["pays_vacancy_profile_desg_code"].ToString();
                            simsobj.pays_vacancy_desc = dr["pays_vacancy_desc"].ToString();
                            simsobj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            simsobj.pays_vacancy_level = dr["pays_vacancy_level"].ToString();
                            simsobj.pays_vacancy_type = dr["pays_vacancy_type"].ToString();
                            simsobj.pays_vacancy_min_experience = dr["pays_vacancy_min_experience"].ToString();
                            simsobj.pays_vacancy_doc = dr["pays_vacancy_doc"].ToString();
                            simsobj.pays_vacancy_company_code = dr["pays_vacancy_company_code"].ToString();
                            simsobj.pays_vacancy_dept_code = dr["pays_vacancy_dept_code"].ToString();
                            simsobj.pays_vacancy_status = dr["pays_vacancy_status"].ToString();
                            simsobj.pays_vacancy_posting_date = db.UIDDMMYYYYformat(dr["pays_vacancy_posting_date"].ToString());
                            simsobj.pays_vacancy_expiry_date =  db.UIDDMMYYYYformat(dr["pays_vacancy_expiry_date"].ToString());
                            simsobj.dept_name = dr["dept_name"].ToString();
                            simsobj.designation_name = dr["designation_name"].ToString();
                            simsobj.pays_vacancy_type_nm = dr["pays_vacancy_type_nm"].ToString();
                            simsobj.pays_vacancy_level_nm = dr["pays_vacancy_level_nm"].ToString();
                            achieve_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, achieve_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, achieve_list);
        }

        #endregion

        #region  API For Staff Time Table View...

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string ac_year, string log_code)
        {
            List<Sims028> mod_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_staff_time_table_view_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@sims_login_code", log_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            //simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            //simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string ac_year,string grade_code, string log_code)
        {
            List<Sims028> mod_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_staff_time_table_view_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_login_code", log_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            //simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("gettimetable")]
        public HttpResponseMessage gettimetable(string cur_code, string ac_year, string grade_code, string section_code, string log_code)
        {
            List<Sims171> mod_list = new List<Sims171>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_staff_time_table_view_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section_code),
                            new SqlParameter("@sims_login_code", log_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims171 simsobj = new Sims171();
                            simsobj.sims_timetable_number = dr["sims_timetable_number"].ToString();
                            simsobj.sims_timetable_cur = dr["sims_timetable_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_timetable_academic_year"].ToString();
                            simsobj.sims_grade = dr["sims_timetable_grade_code"].ToString();
                            simsobj.sims_section = dr["sims_timetable_section_code"].ToString();
                            simsobj.sims_timetable_filename = dr["sims_timetable_filename"].ToString();
                            if (dr["sims_timetable_status"].ToString().Equals("A"))
                                simsobj.sims_timetable_status = true;
                            else
                                simsobj.sims_timetable_status = false;
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        #endregion

    }
}
