﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;
//using SIMSAPI.Controllers.EmpAttendance;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/EmpAttendanceDailyController")]
    public class EmpAttendanceDailyController : ApiController
    {
        //  public int sp_call_att_codes = 0;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("MarkEmpDailyAttendance")]
        public HttpResponseMessage MarkEmpDailyAttendance(List<Pers326> data)
        {
            int res = 0;
            string status = "";
            Message message = new Message();
            //List<result> m = new List<result>();
            try
            {
                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        int dr;
                        foreach (var simsobj in data)
                        {
                            dr = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_move]",
                      new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                                new SqlParameter("@att_emp_id",simsobj.no),
                                new SqlParameter("@att_date",simsobj.att_date),
                                new SqlParameter("@att_shift1_in",simsobj.s1in),
                                new SqlParameter("@att_shift1_out",simsobj.s1out),
                                new SqlParameter("@att_shift2_in",simsobj.s2in),
                                new SqlParameter("@att_shift2_out",simsobj.s2out),
                                new SqlParameter("@att_flag","UM"),
                         });

                            if (dr > 0)
                            {
                                res = res + 1;
                            }
                            else
                            {
                                res = 0;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, res);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        [Route("PersEmnumber")]
        public HttpResponseMessage PersEmnumber(List<Pers326> data)
        {
            List<Pers326> trans_list = new List<Pers326>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var o in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@att_emp_id", o.att_emp_id),
                            new SqlParameter("@att_date", o.att_date),
                            new SqlParameter("@att_shift1_in", o.att_shift1_in),
                            new SqlParameter("@att_shift1_out", o.att_shift1_out),
                            new SqlParameter("@att_shift2_in", o.att_shift2_in),
                            new SqlParameter("@att_shift2_out", o.att_shift2_out),
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Pers326 obj = new Pers326();
                                obj.emp_id = dr["em_number"].ToString();
                                if (dr["exist_number"].ToString() == "Y")
                                    obj.emp_id_flag = true;
                                else
                                    obj.emp_id_flag = false;
                                obj.att_date = dr["att_date"].ToString();
                                obj.att_shift1_in = dr["att_shift1_in"].ToString();
                                obj.att_shift1_out = dr["att_shift1_out"].ToString();
                                obj.att_shift2_in = dr["att_shift2_in"].ToString();
                                obj.att_shift2_out = dr["att_shift2_out"].ToString();
                                // obj.color='red';
                                trans_list.Add(obj);
                            }
                            dr.Close();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }

        [Route("getPers326Dept")]
        public HttpResponseMessage getPers326Dept()
        {
            List<Pers326> trans_list = new List<Pers326>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "D"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers326 obj = new Pers326();
                            obj.dep_code = dr["codp_dept_no"].ToString();
                            obj.dep_name = dr["codp_dept_name"].ToString();

                            trans_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }



        [Route("getPers326desg")]
        public HttpResponseMessage getPers326desg()
        {
            List<Pers326> trans_list = new List<Pers326>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "E"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers326 obj = new Pers326();
                            obj.dg_code = dr["dg_code"].ToString();
                            obj.dg_name = dr["dg_desc"].ToString();
                            trans_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }

        [HttpGet]
        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode(string str)
        {
            List<Per313> list = new List<Per313>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_attendance_code = dr["pays_attendance_code"].ToString();
                            simsobj.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                            simsobj.pays_attendance_description = dr["pays_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["pays_attendance_color_code"].ToString()))
                            {
                                simsobj.pays_attendance_color = string.Format("#{0}", dr["pays_attendance_color_code"].ToString().Substring(4));
                            }
                            if (str.Contains(simsobj.pays_attendance_code))
                            {
                                list.Add(simsobj);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Pers326_Get_attednce_deatils")]
        public HttpResponseMessage Pers326_Get_attednce_deatils(string sdate, string edate, string dept = null, string desg = null)
        {
            List<Pers326> trans_list = new List<Pers326>();

            try
            {
                string[] spilitsdate = sdate.Split('-');
                string yeartemp = spilitsdate[0];
                if (yeartemp.Length >= 4)
                {
                    var day = sdate.Split('-')[2];
                    var month = sdate.Split('-')[1];
                    var year = sdate.Split('-')[0];

                    sdate = day + "-" + month + "-" + year;
                }
                string[] spilitedate = edate.Split('-');
                string yeartemp1 = spilitedate[0];
                if (yeartemp1.Length >= 4)
                {
                    var day = edate.Split('-')[2];
                    var month = edate.Split('-')[1];
                    var year = edate.Split('-')[0];

                    edate = day + "-" + month + "-" + year;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", 'Q'),
                new SqlParameter("@dep", dept),
                new SqlParameter("@desg", desg),
                 new SqlParameter("@from_date", db.DBYYYYMMDDformat(sdate)),
                new SqlParameter("@to_date", db.DBYYYYMMDDformat(edate)),
                //new SqlParameter("@from_date", DateTime.Parse(sdate).Year + "-" + DateTime.Parse(sdate).Month + "-" + DateTime.Parse(sdate).Day),
                //new SqlParameter("@to_date", DateTime.Parse(edate).Year + "-" + DateTime.Parse(edate).Month + "-" + DateTime.Parse(edate).Day),
                         });
                    while (dr.Read())
                    {
                        Pers326 simsobj = new Pers326();
                        simsobj.att_user_id = dr["att_user_id"].ToString();
                        simsobj.att_emp_id = dr["att_emp_id"].ToString();
                        simsobj.emp_name = dr["emp_name"].ToString();
                        simsobj.att_shift_no = dr["sh_shift_id"].ToString();
                        try
                        {
                            simsobj.att_remarks = dr["att_rem"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        simsobj.att_date = dr["att_date"].ToString(); //DateTime.Parse(dr["att_date"].ToString()).ToShortDateString();
                        simsobj.expected1In = dr["expected1In"].ToString();
                        simsobj.expected2In = dr["expected2In"].ToString();
                        simsobj.att_shift1_in = dr["att_shift1_in"].ToString();
                        simsobj.att_shift2_in = dr["att_shift2_in"].ToString();
                        simsobj.att1_in = (DateTime.Parse(simsobj.expected1In) - DateTime.Parse(simsobj.att_shift1_in)).ToString();
                        simsobj.att2_in = (DateTime.Parse(simsobj.expected2In) - DateTime.Parse(simsobj.att_shift2_in)).ToString();

                        var s1 = simsobj.att1_in.Substring(0, 1);
                        if (s1 == "-")
                            simsobj.s1_in = s1;
                        else
                            simsobj.s1_in = "+";
                        var s2 = simsobj.att2_in.Substring(0, 1);
                        if (s2 == "-")
                            simsobj.s2_in = s2;
                        else
                            simsobj.s2_in = "+";

                        simsobj.expected1Out = dr["expected1Out"].ToString();
                        simsobj.expected2Out = dr["expected2Out"].ToString();
                        simsobj.att_shift1_out = dr["att_shift1_out"].ToString();
                        simsobj.att_shift2_out = dr["att_shift2_out"].ToString();
                        simsobj.att1_out = (DateTime.Parse(simsobj.att_shift1_out) - DateTime.Parse(simsobj.expected1Out)).ToString();
                        simsobj.att2_out = (DateTime.Parse(simsobj.att_shift2_out) - DateTime.Parse(simsobj.expected2Out)).ToString();

                        var v1 = simsobj.att1_out.Substring(0, 1);
                        if (v1 == "-")
                            simsobj.s1_out = v1;
                        else
                            simsobj.s1_out = "+";
                        var v2 = simsobj.att2_out.Substring(0, 1);
                        if (v2 == "-")
                            simsobj.s2_out = v2;
                        else
                            simsobj.s2_out = "+";

                        simsobj.att1_delay = dr["att1_delay"].ToString();
                        simsobj.att2_delay = dr["att2_delay"].ToString();
                        simsobj.att1_early = dr["att1_early"].ToString();
                        simsobj.att2_early = dr["att2_early"].ToString();
                        simsobj.att_wrkd_hrs = dr["att_wrkd_hrs"].ToString();
                        simsobj.leave_code = dr["att_absent_flag"].ToString();
                        try
                        {
                            simsobj.leave_name = dr["leave_name"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        try
                        {
                            simsobj.pays_attendance_color_code = dr["pays_attendance_color_code"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }

                        trans_list.Add(simsobj);
                        simsobj.lst_leave = new List<leave_data>();

                    }
                    try
                    {
                        dr.NextResult();
                        List<leave_data> temp = new List<leave_data>();
                        while (dr.Read())
                        {
                            leave_data obj = new leave_data();
                            obj.emp_id = dr["em_login_code"].ToString();
                            var v = trans_list.Where(q => q.att_emp_id == obj.emp_id).ToList();
                            // var v = trans_list.ToList();
                            if (v.Count() > 0)
                            {
                                obj.leave_code = dr["el_leave_code"].ToString();
                                obj.leave_name = dr["cl_desc"].ToString();
                                obj.leave_pending = dr["pending_leave"].ToString();                                
                                temp.Add(obj);
                            }

                        }
                        List<string> empls = temp.Select(q => q.emp_id).Distinct().ToList();
                        foreach (var item in empls)
                        {
                            var v = temp.Where(q => q.emp_id == item).ToList();
                            trans_list.Where(q => q.att_emp_id == item).ToList().ForEach(q => q.lst_leave = v);
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("Pers326_update_att_data")]
        public HttpResponseMessage Pers326_update_att_data(List<Pers326> item)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var obj in item)
                    {
                        try
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                                                  new List<SqlParameter>()
                                           {
                    new SqlParameter("@opr", 'M'),
                    //new SqlParameter("@att_delay", obj.att1_delay),
                    //new SqlParameter("@att_early", obj.att1_early),
                    new SqlParameter("@att_shift_no", obj.att_shift_no),
                    //new SqlParameter("@att_wrkd_hrs", obj.att_wrkd_hrs),
                    new SqlParameter("@att_absent_flag", obj.leave_code),
                    new SqlParameter("@att_user_id", obj.att_user_id),
                    new SqlParameter("@att_emp_id", obj.att_emp_id),
                      new SqlParameter("@att_rem", obj.att_remarks),
                    new SqlParameter("@att_date",  DateTime.Parse(obj.att_date).Year + "-" + DateTime.Parse(obj.att_date).Month + "-" + DateTime.Parse(obj.att_date).Day),
                                        });

                            if (dr.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }
                            dr.Close();
                        }
                        catch (Exception)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_move]",
                                                 new List<SqlParameter>()
                                          {
                    new SqlParameter("@opr", 'M'),
                    //new SqlParameter("@att_delay", obj.att1_delay),
                    //new SqlParameter("@att_early", obj.att1_early),
                    new SqlParameter("@att_shift_no", obj.att_shift_no),
                    //new SqlParameter("@att_wrkd_hrs", obj.att_wrkd_hrs),
                    new SqlParameter("@att_absent_flag", obj.leave_code),
                    new SqlParameter("@att_user_id", obj.att_user_id),
                    new SqlParameter("@att_emp_id", obj.att_emp_id),
                    new SqlParameter("@att_date",  DateTime.Parse(obj.att_date).Year + "-" + DateTime.Parse(obj.att_date).Month + "-" + DateTime.Parse(obj.att_date).Day),
                                       });

                            if (dr.RecordsAffected > 0)
                            {
                                Inserted = true;
                            }
                            dr.Close();
                        }
                    }

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }


    }
}



