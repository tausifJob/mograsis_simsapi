﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.ERP.modules
{
    [RoutePrefix("api/EmplDependent")]
    public class EmplDependentController : ApiController
    {

        [Route("employee_dependent")]
        public HttpResponseMessage employee_dependent(string emp_id)
        {

            List<Per352> lstgender = new List<Per352>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_dependent_proc]",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "S"),
                               new SqlParameter("@em_login_code", emp_id),
                         }
                         );

                    while (dr.Read())
                    {
                        Per352 simsobj = new Per352();
                        simsobj.ed_srno = dr["ed_srno"].ToString();
                        simsobj.em_login_code = dr["em_login_code"].ToString();
                        simsobj.em_name = dr["em_name"].ToString();
                        simsobj.ed_sr_code = dr["ed_sr_code"].ToString();
                        simsobj.ed_first_name_en = dr["ed_first_name_en"].ToString();
                        simsobj.ed_middle_name_en = dr["ed_middle_name_en"].ToString();
                        simsobj.ed_last_name_en = dr["ed_last_name_en"].ToString();
                        simsobj.ed_dob = db.UIDDMMYYYYformat(dr["ed_dob"].ToString());
                        simsobj.ed_visa_sponsor_id = dr["ed_visa_sponsor_id"].ToString();
                        simsobj.ed_visa_sponsor_name = dr["ed_visa_sponsor_name"].ToString();
                        simsobj.ed_visa_number = dr["ed_visa_number"].ToString();
                        simsobj.ed_visa_issue_date = db.UIDDMMYYYYformat(dr["ed_visa_issue_date"].ToString());
                        simsobj.ed_visa_expiry_date = db.UIDDMMYYYYformat(dr["ed_visa_expiry_date"].ToString());
                        simsobj.ed_nation_id = dr["ed_nation_id"].ToString();
                        simsobj.ed_nation_id_issue_date = db.UIDDMMYYYYformat(dr["ed_nation_id_issue_date"].ToString());
                        simsobj.ed_nation_id_expiry_date = db.UIDDMMYYYYformat(dr["ed_nation_id_expiry_date"].ToString());
                        simsobj.ed_passport_id = dr["ed_passport_id"].ToString();
                        simsobj.ed_passport_id_issue_date = db.UIDDMMYYYYformat(dr["ed_passport_id_issue_date"].ToString());
                        simsobj.ed_passport_id_expiry_date = db.UIDDMMYYYYformat(dr["ed_passport_id_expiry_date"].ToString());
                        simsobj.ed_relation = dr["ed_relation"].ToString();
                        simsobj.ed_visa_exp_rem_date = db.UIDDMMYYYYformat(dr["ed_visa_exp_rem_date"].ToString());
                        simsobj.ed_health_insurance_issuing_authority = dr["ed_health_insurance_issuing_authority"].ToString();
                        simsobj.ed_health_insurance_no = dr["ed_health_insurance_no"].ToString();
                        simsobj.ed_health_insurance_effective_from_date = db.UIDDMMYYYYformat(dr["ed_health_insurance_effective_from_date"].ToString());
                        simsobj.ed_health_insurance_effective_upto_date = db.UIDDMMYYYYformat(dr["ed_health_insurance_effective_upto_date"].ToString());
                        simsobj.ed_health_insurance_status = dr["ed_health_insurance_status"].ToString().Equals("A") ? true:false;
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }

        [Route("CUDemployee_dependent")]
        public HttpResponseMessage CUDemployee_dependent(Per352 simsobj)
        {
            bool insert = false;
            try
            {
                    using (DBConnection db = new DBConnection())
                    {
                         db.Open();
                         SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_dependent_proc]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@em_login_code", simsobj.em_login_code),
                                new SqlParameter("@ed_sr_code", simsobj.ed_sr_code),
                                new SqlParameter("@ed_first_name_en", simsobj.ed_first_name_en),
                                new SqlParameter("@ed_middle_name_en", simsobj.ed_middle_name_en),
                                new SqlParameter("@ed_last_name_en", simsobj.ed_last_name_en),
                                new SqlParameter("@ed_dob", db.DBYYYYMMDDformat(simsobj.ed_dob)),
                                new SqlParameter("@ed_visa_sponsor_id", simsobj.ed_visa_sponsor_id),
                                new SqlParameter("@ed_visa_sponsor_name", simsobj.ed_visa_sponsor_name),
                                new SqlParameter("@ed_visa_number", simsobj.ed_visa_number),
                                new SqlParameter("@ed_visa_issue_date", db.DBYYYYMMDDformat(simsobj.ed_visa_issue_date)),
                                new SqlParameter("@ed_visa_expiry_date", db.DBYYYYMMDDformat(simsobj.ed_visa_expiry_date)),
                                new SqlParameter("@ed_nation_id", simsobj.ed_nation_id),
                                new SqlParameter("@ed_nation_id_issue_date", db.DBYYYYMMDDformat(simsobj.ed_nation_id_issue_date)),
                                new SqlParameter("@ed_nation_id_expiry_date", db.DBYYYYMMDDformat(simsobj.ed_nation_id_expiry_date)),
                                new SqlParameter("@ed_passport_id", simsobj.ed_passport_id),
                                new SqlParameter("@ed_passport_id_issue_date", db.DBYYYYMMDDformat(simsobj.ed_passport_id_issue_date)),
                                new SqlParameter("@ed_passport_id_expiry_date", db.DBYYYYMMDDformat(simsobj.ed_passport_id_expiry_date)),
                                new SqlParameter("@ed_relation", simsobj.ed_relation),
                                new SqlParameter("@ed_visa_exp_rem_date", db.DBYYYYMMDDformat(simsobj.ed_visa_exp_rem_date)),
                                new SqlParameter("@ed_health_insurance_issuing_authority", simsobj.ed_health_insurance_issuing_authority),
                                new SqlParameter("@ed_health_insurance_no", simsobj.ed_health_insurance_no),
                                new SqlParameter("@ed_health_insurance_effective_from_date", db.DBYYYYMMDDformat(simsobj.ed_health_insurance_effective_from_date)),
                                new SqlParameter("@ed_health_insurance_effective_upto_date", db.DBYYYYMMDDformat(simsobj.ed_health_insurance_effective_upto_date)),
                                new SqlParameter("@ed_health_insurance_status", simsobj.ed_health_insurance_status.Equals(true)?"A":"I")
                            });
                            if (dr.RecordsAffected > 0)
                            {
                                insert = true;
                            }
                            dr.Close();
                        }
                }
                
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getDependentRelationship")]
        public HttpResponseMessage getDependentRelationship()
        {
            List<Per352> lstgender = new List<Per352>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_dependent_proc]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "A")                               
                         });

                    while (dr.Read())
                    {
                        Per352 simsobj = new Per352();
                        simsobj.pays_appl_parameter = dr["pays_appl_parameter"].ToString();
                        simsobj.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { return Request.CreateResponse(HttpStatusCode.InternalServerError,x.Message); }            
        }
    }
}