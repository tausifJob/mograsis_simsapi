﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/UEmpDestination")]
    public class UpdateEmpDestinationController:ApiController
    {
      
        [Route("Department")]
        public HttpResponseMessage Department()
        {

            List<dest_update> employee_list = new List<dest_update>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","D"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            dest_update simsobj = new dest_update();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Designation")]
        public HttpResponseMessage Designation()
        {

            List<dest_update> employee_list = new List<dest_update>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","E"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            dest_update simsobj = new dest_update();
                            simsobj.desg_code = dr["dg_code"].ToString();
                            simsobj.desg_name = dr["dg_desc"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Grade")]
        public HttpResponseMessage Grade()
        {

            List<dest_update> employee_list = new List<dest_update>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            dest_update simsobj = new dest_update();
                            simsobj.grade_code = dr["gr_code"].ToString();
                            simsobj.grade_name = dr["gr_desc"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

     
        [Route("Destination")]
        public HttpResponseMessage Destination()
        {

            List<dest_update> employee_list = new List<dest_update>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            dest_update simsobj = new dest_update();
                            simsobj.dest_code = dr["ds_code"].ToString();
                            simsobj.dest_name = dr["ds_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("EmployeeDest")]
        public HttpResponseMessage EmployeeDest(dest_update obj)
        {

            List<dest_update> employee_list = new List<dest_update>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@emp_id",obj.emp_id),
                            new SqlParameter("@dept_code",obj.dept_code),
                            new SqlParameter("@desg_code",obj.desg_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            dest_update simsobj = new dest_update();
                            simsobj.emp_id = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.dest_code = dr["em_dest_code"].ToString();
                            simsobj.dest_name = dr["ds_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("UpdateDest")]
        public HttpResponseMessage UpdateDest(dest_update data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_empl_dest_proc]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "U"),
                new SqlParameter("@emp_id", data.emp_id),
                new SqlParameter("@dest_code", data.dest_code),
               
                 });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}