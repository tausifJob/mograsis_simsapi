﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;
namespace SIMSAPI.Controllers.hrms
{
    //leave details
    [RoutePrefix("api/leavedetails")]
    public class LeaveDetailsController : ApiController
    {
        List<Pers300_le> allDates = new List<Pers300_le>();
        List<DateTime> BlackDates = new List<DateTime>();

        #region Leave Details
        [Route("Getallemployee")]
        public HttpResponseMessage Getallemployee()
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'E'),
             
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrms = new Pers300_le();
                            hrms.EmpName = dr["empname"].ToString();
                            hrms.em_login_code = dr["em_login_code"].ToString();
                            hrms.em_email = dr["em_email"].ToString();
                            hrms.em_status = dr["em_status"].ToString();
                            hrmsobj.Add(hrms);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }

        [Route("Get_EmpInfo")]
        public HttpResponseMessage Get_EmpInfo(string empid)
        {
            Pers300_le hrmsobj = new Pers300_le();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'X'),
                new SqlParameter("@empid", empid)
                });
                    if (dr.Read())
                    {

                        hrmsobj.EmpName = dr["EmpName"].ToString();
                        hrmsobj.em_login_code = dr["EMP"].ToString();
                        hrmsobj.comp_name = dr["Company"].ToString();
                        hrmsobj.dg_desc = dr["Designation"].ToString();
                        hrmsobj.codp_dept_name = dr["Department"].ToString();
                        hrmsobj.gr_desc = dr["Grade"].ToString();
                        hrmsobj.comp_code = dr["em_company_code"].ToString();
                        hrmsobj.img_name = dr["em_img"].ToString();
                        hrmsobj.ndays = string.Empty;
                        hrmsobj.updated = false;
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }

        [Route("Get_EMPLeaveDetails")]
        public HttpResponseMessage Get_EMPLeaveDetails()
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("opr", "L"),
                 });
                    while (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        obj.leave_type = dr["cl_code"].ToString();
                        obj.leave_code = dr["cl_desc"].ToString();
                        //obj.maxleave_allowed = dr["le_max_days_allowed"].ToString();
                        hrmsobj.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }

        [Route("Get_EMPLeaveMaxDays")]
        public HttpResponseMessage Get_EMPLeaveMaxDays(string le_leave_code, string compcode, string empid)
        {
            string leave_max = string.Empty;
            string total_leave_taken = string.Empty;
            double allowed_leaves = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "AA"),
                new SqlParameter("@le_company_code", compcode),
                new SqlParameter("@lt_leave_code", le_leave_code )
                });
                    if (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        leave_max = dr["le_max_days_allowed"].ToString();
                    }
                    dr.Close();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "BB"),
                new SqlParameter("@em_login_code",empid),
                new SqlParameter("@lt_leave_code", le_leave_code)
                });
                    if (dr.Read())
                    {
                        total_leave_taken = dr["el_days_taken"].ToString();
                    }
                }
                allowed_leaves = Convert.ToDouble(leave_max) - Convert.ToDouble(total_leave_taken);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, allowed_leaves);

        }

        [Route("Insert_pay_leavedoc")]
        public HttpResponseMessage Insert_pay_leavedoc(Pers300_le pay)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_document_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'I'),
                new SqlParameter("@pays_leave_doc_code", pay.pays_leave_doc_code),
                new SqlParameter("@pays_leave_doc_name", pay.pays_leave_doc_name),
                new SqlParameter("@pays_leave_doc_desc", pay.pays_leave_doc_desc),
                new SqlParameter("@pays_leave_doc_is_mandatory", pay.pays_leave_doc_is_mandatory),
                new SqlParameter("@pays_leave_doc_status", pay.pays_leave_doc_status)
                });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }

                }

            }

            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        [Route("GetEmployeePendingLeaveDetails")]
        public HttpResponseMessage GetEmployeePendingLeaveDetails(string emp_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            bool Updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@Opr", "P"),
                new SqlParameter("@lt_number", emp_code),
                
                 });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat((dr["lt_start_date"].ToString()));
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat((dr["lt_end_date"].ToString()));
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = db.UIDDMMYYYYformat((dr["lt_modified_on"].ToString()));
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.updated = false;
                            hrmsobj.lt_no = dr["lt_no"].ToString();
                            hrmsobj.lt_admin_remark = dr["lt_admin_remark"].ToString();
                            hrmsobj.lt_approval_date = db.UIDDMMYYYYformat(dr["lt_approval_date"].ToString());
                            hrmsobj.lt_admin_cancel_remark = dr["lt_admin_cancel_remark"].ToString();
                            hrmsobj.lt_approved_by_user_code = dr["lt_approved_by_user_code"].ToString();
                            hrmsobj.Actiononleave = dr["Actiononleave"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }

        [Route("InsertEMPLeaveDetails")]
        public HttpResponseMessage InsertEMPLeaveDetails(Pers300_le obj)
        {
            bool res = false;
            string @transacid = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@Opr", "I"),
                new SqlParameter("@lt_number", obj.em_login_code),
                new SqlParameter("@lt_company_code", obj.comp_code),
                new SqlParameter("@lt_leave_code", obj.leave_code),
                new SqlParameter("@lt_start_date",db.DBYYYYMMDDformat(obj.lt_start_date)),
                new SqlParameter("@lt_end_date",db.DBYYYYMMDDformat(obj.lt_end_date)),
                new SqlParameter("@lt_days", obj.lt_days),
                new SqlParameter("@lt_hours", obj.lt_hours),
                new SqlParameter("@lt_mgrctag", obj.lt_mgr_confirm_tag),
                new SqlParameter("@lt_modified_on", db.DBYYYYMMDDformat(DateTime.Now.Date.ToString())),
                new SqlParameter("@lt_remarks", obj.lt_remarks),
                new SqlParameter("@lt_converted_hours", obj.lt_converted_hours),
                new SqlParameter("@lt_request_date", db.DBYYYYMMDDformat(DateTime.Now.Date.ToString()))
              });

                    if (dr.Read())
                    {
                        transacid = dr["TRANSACID"].ToString();
                        res = true;
                    }
                    //else
                    //    res = false;
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }

        [Route("GetAllEMPLeaveDetails")]
        public HttpResponseMessage GetAllEMPLeaveDetails(string comp_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            //
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", "Q"),
                            new SqlParameter("@lt_company_code", comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            DateTime stdate = DateTime.Parse(dr["lt_start_date"].ToString());
                            string st = stdate.Year.ToString() + "-" + stdate.Month.ToString() + "-" + stdate.Day.ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            DateTime enddate = DateTime.Parse(dr["lt_end_date"].ToString());
                            string ed = enddate.Year.ToString() + "-" + enddate.Month.ToString() + "-" + enddate.Day.ToString();
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = dr["lt_modified_on"].ToString();
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            string rq = string.Empty;
                            if (!string.IsNullOrEmpty(dr["lt_request_date"].ToString()))
                            {
                                DateTime rqdate = DateTime.Parse(dr["lt_request_date"].ToString());
                                rq = rqdate.Year.ToString() + "-" + rqdate.Month.ToString() + "-" + rqdate.Day.ToString();
                            }
                            hrmsobj.requestdate = db.UIDDMMYYYYformat(dr["lt_request_date"].ToString());
                            hrmsobj.updated = false;
                            hrmsobj.lt_admin_remark = dr["lt_admin_remark"].ToString(); //string.Empty;
                            hrmsobj.lt_admin_cancel_remark = string.Empty;
                            try
                            {
                                hrmsobj.em_doc_path = dr["em_doc_path"].ToString(); 
                            }
                            catch (Exception) { }
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }

        //[Route("GetAllEMPLeaveDetails")]
        //public HttpResponseMessage GetAllEMPLeaveDetails()
        //{
        //    List<string> hrmsobj = new List<string>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //        new SqlParameter("tbl_name", "pays.pays_parameter"),
        //        new SqlParameter("tbl_col_name1", "[pays_appl_form_field_value1]"),
        //        new SqlParameter("@tbl_cond", "[pays_appl_form_field]='ManagerConfirmTag'"),
        //        });
        //            while (dr.Read())
        //            {
        //                hrmsobj.Add(dr["pays_appl_form_field_value1"].ToString());
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        //}

        [Route("Update_pay_leavedoc")]
        public HttpResponseMessage Update_pay_leavedoc(List<Pers300_le> pay)
        {
            //bool updated = false;
            int updated = 0;

            foreach (Pers300_le obj in pay)
            {
                double daystoadd = 0;
                double extra = 0;
               
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        obj.lt_start_date = db.DBYYYYMMDDformat(obj.lt_start_date);
                        obj.lt_end_date = db.DBYYYYMMDDformat(obj.lt_end_date);

                        string mnth = string.Empty;
                        string day = string.Empty;

                        if (DateTime.Parse(obj.lt_start_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(obj.lt_start_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(obj.lt_start_date).Month.ToString();

                        if (DateTime.Parse(obj.lt_start_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(obj.lt_start_date).Day.ToString();
                        else
                            day = DateTime.Parse(obj.lt_start_date).Day.ToString();

                        obj.lt_start_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.lt_start_date).Year, mnth, day);

                        if (DateTime.Parse(obj.lt_end_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(obj.lt_end_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(obj.lt_end_date).Month.ToString();

                        if (DateTime.Parse(obj.lt_end_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(obj.lt_end_date).Day.ToString();
                        else
                            day = DateTime.Parse(obj.lt_end_date).Day.ToString();

                        obj.lt_end_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.lt_end_date).Year, mnth, day);

                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@Opr", 'R'),
                             new SqlParameter("@lt_number", obj.em_login_code),
                             new SqlParameter("@lt_company_code", obj.comp_code),
                             new SqlParameter("@lt_start_date", obj.lt_start_date),
                             new SqlParameter("@lt_end_date", obj.lt_end_date),
                             new SqlParameter("@lt_mgrctag", obj.lt_mgr_confirm_tag),                    
                             new SqlParameter("@lt_admin_remark", obj.lt_admin_remark),
                             new SqlParameter("@lt_leave_code",obj.leave_code)
                        }); 
                        dr.Close();
                        updated = 1;
                        if (obj.lt_mgr_confirm_tag == "C")
                        {
                            string leave_max = string.Empty;
                            string total_leave_taken = string.Empty;
                            double allowed_leaves = 0;
                            try
                            {
                                SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                                new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", "A1"),
                                    new SqlParameter("@le_company_code",obj.comp_code),
                                    new SqlParameter("@lt_leave_code",obj.leave_code),
                                    new SqlParameter("@lt_number", obj.em_login_code),
                                });
                                if (dr1.Read())
                                {
                                    leave_max = dr1["el_maximum_days"].ToString();
                                }
                                dr1.Close();
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                              new List<SqlParameter>()
                                 {
                                    new SqlParameter("@opr", "BB"),
                                    new SqlParameter("@em_login_code",obj.em_login_code),
                                    new SqlParameter("@lt_leave_code",  obj.leave_code)
                                });
                                if (dr2.Read())
                                {
                                    total_leave_taken = dr2["el_days_taken"].ToString();
                                }
                                else
                                {
                                    total_leave_taken = "0.0";
                                }
                                dr2.Close();
                                
                                /* dont delete // if leave taken greater than max allowed leave */
                                //if ((Convert.ToDouble(total_leave_taken) + Convert.ToDouble(obj.lt_days)) <= Convert.ToDouble(leave_max))
                                //{

                                allowed_leaves = Convert.ToDouble(leave_max) - Convert.ToDouble(total_leave_taken);
                                    //if (obj.lt_days.Contains(".5"))
                                    //{
                                    //    int ind = obj.lt_days.IndexOf('.');
                                    //    obj.lt_days = obj.lt_days.Substring(0, ind);
                                    //    obj.lt_days = (Double.Parse(obj.lt_days) + 1).ToString();
                                    //}
                                    //for (int i = 0; i < Double.Parse(obj.lt_days); i++)

                                for(DateTime date = DateTime.Parse(obj.lt_start_date); date <= DateTime.Parse(obj.lt_end_date); date = date.AddDays(1))
                                { 
                                    //for (int i = 0; i < Convert.ToDouble(obj.lt_days); i++)
                                    //{
                                    DateTime dt = DateTime.Parse(obj.lt_start_date);
                                        //DateTime dt_insert = dt.AddDays(i);
                                    empAttendance eatt = new empAttendance();

                                    string date1 = date.Year.ToString() + "-" + date.Month.ToString() + "-" + date.Day.ToString();
                                   
                                    eatt.emp_id = obj.em_login_code;
                                    //eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, dt_insert.Month, dt_insert.Day);
                                    eatt.att_date = date1;

                                    //if (i < allowed_leaves)
                                    //        eatt.att_flag = obj.leave_code;
                                    //    else
                                    //        eatt.att_flag = obj.leave_code;//"A";

                                    eatt.att_flag = obj.leave_code;

                                    //if (obj.lt_hours == "1.0")
                                    //    if (i == 0)
                                    //        eatt.att_flag = "HD";
                                    //if (obj.lt_hours == "0.1")
                                    //    if (i == Convert.ToDouble(obj.lt_days) - 1)
                                    //        eatt.att_flag = "HD";
                                    //if (obj.lt_hours == "1.1")
                                    //{
                                    //    if (i == Convert.ToDouble(obj.lt_days) - 1)
                                    //        eatt.att_flag = "HD";
                                    //    if (i == 0)
                                    //        eatt.att_flag = "HD";
                                    //}

                                        eatt.comment = String.Empty;
                                        eatt.s1in = String.Empty;
                                        eatt.s1out = String.Empty;
                                        eatt.s2in = String.Empty;
                                        eatt.s2out = String.Empty;
                                        bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                                    }

                                    if (allowed_leaves <= Convert.ToDouble(obj.lt_days))
                                    {
                                        extra = Convert.ToDouble(obj.lt_days) - allowed_leaves;
                                        daystoadd = allowed_leaves;
                                    }
                                    else
                                    {
                                        daystoadd = Convert.ToDouble(obj.lt_days);
                                        extra = 0;
                                    }

                                    SqlDataReader dr3 = db.ExecuteStoreProcedure("[pays_Leave_Register]",
                                    new List<SqlParameter>()
                                    {
                                                new SqlParameter("@Opr", 'U'),
                                                new SqlParameter("@el_number", obj.em_login_code),
                                                new SqlParameter("@el_leave_code", obj.leave_code),
                                                new SqlParameter("@el_company_code", obj.comp_code),
                                                new SqlParameter("@el_days_taken", daystoadd),
                                                new SqlParameter("@el_extra_days", extra),
                                                new SqlParameter("@lt_start_date", obj.lt_start_date),
                                                new SqlParameter("@lt_end_date", obj.lt_end_date)
                                   });
                                    if (dr3.RecordsAffected > 0)
                                    {
                                        updated = 1;
                                    }
                                /* dont delete // if leave taken greater than max allowed leave */
                                //}
                                //else
                                //{
                                //     // if leave taken greater than max allowed leave
                                //SqlDataReader dr3 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                                //new List<SqlParameter>()
                                // {
                                //     new SqlParameter("@Opr", 'R'),
                                //     new SqlParameter("@lt_number", obj.em_login_code),
                                //     new SqlParameter("@lt_company_code", obj.comp_code),
                                //     new SqlParameter("@lt_start_date", obj.lt_start_date),
                                //     new SqlParameter("@lt_end_date", obj.lt_end_date),
                                //     new SqlParameter("@lt_mgrctag", 'N'),
                                //     new SqlParameter("@lt_admin_remark", obj.lt_admin_remark),
                                //     new SqlParameter("@lt_leave_code",obj.leave_code)
                                //});
                                //dr3.Close();
                                //updated = 2;
                                //}
                                /* dont delete*/
                            }
                            catch (Exception de)
                            { }
                        }

                        using (DBConnection db1 = new DBConnection())
                        {
                            db.Open();

                            try
                            {
                                //For Requirement of OES / DPS MIS
                                //While applying leave / Absent check if absent or leave marked on Sunday then check for Thursday for attendance 
                                //status if its absent or leave mark Friday/ Saturday as Absent.

                                string if_includeholidays = "N";
                                SqlDataReader dr33_get = db1.ExecuteStoreProcedure("GetData",
                                     new List<SqlParameter>() 
                                    { 
                            new SqlParameter("tbl_name", "[pays].[pays_parameter]"),
                                    new SqlParameter("tbl_col_name1", "[pays_appl_parameter]"),
                                    new SqlParameter("@tbl_cond", "[pays_appl_code] ='Per304' and [pays_appl_form_field]='IncludeHolidays' "),
                                    });
                                if (dr33_get.RecordsAffected > 0)
                                {
                                    if (dr33_get.Read())
                                        if_includeholidays = dr33_get["pays_appl_parameter"].ToString();
                                }


                                if (if_includeholidays == "Y" || if_includeholidays == "y")
                                {
                                    bool r = false;
                                    DateTime startDate = DateTime.Parse(obj.lt_start_date);
                                    DateTime endDate = DateTime.Parse(obj.lt_end_date);

                                    while (endDate > startDate)
                                    {
                                        r = (endDate.DayOfWeek == DayOfWeek.Sunday);
                                        if (r)
                                        {
                                            //Sunday Present
                                            DateTime checkThur = endDate.AddDays(-3);
                                            string if_leave = "N";
                                            //Check if Thursday is Leave or Absent
                                            SqlDataReader dr333 = db1.ExecuteStoreProcedure("GetData",
                                     new List<SqlParameter>() 
                                    { 
                            new SqlParameter("@tbl_name", "[pays].[pays_attendance_daily]"),
                                                new SqlParameter("tbl_col_name1", "[att_flag]"),
                                                new SqlParameter("@tbl_cond", "[att_emp_id] ='" + obj.em_login_code + "' and [att_date]='" + checkThur + "'"),
                                                });
                                            if (dr333.RecordsAffected > 0)
                                            {
                                                if (dr333.Read())
                                                    if_leave = dr333["att_flag"].ToString();
                                            }


                                            if (if_leave == "A" || if_leave == "L")
                                            {
                                                for (int z = 1; z < 3; z++)
                                                {
                                                    empAttendance eatt = new empAttendance();

                                                    eatt.emp_id = obj.em_login_code;
                                                    eatt.att_date = string.Format("{0}-{1}-{2}", checkThur.AddDays(z).Year, checkThur.AddDays(z).Month, checkThur.AddDays(z).Day);
                                                    eatt.att_flag = "A";
                                                    eatt.comment = String.Empty;
                                                    eatt.s1in = String.Empty;
                                                    eatt.s1out = String.Empty;
                                                    eatt.s2in = String.Empty;
                                                    eatt.s2out = String.Empty;
                                                    bool r1 = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                                                }
                                            }
                                        }
                                        endDate = endDate.AddDays(-1);
                                    }
                                }
                            }
                            catch (Exception dee)
                            { }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("Update_pay_leavedoc_SISO")]
        public HttpResponseMessage Update_pay_leavedoc_SISO(List<Pers300_le> pay)
        {
            //bool updated = false;
            int updated = 0;

            foreach (Pers300_le obj in pay)
            {
                double daystoadd = 0;
                double extra = 0;

                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        obj.lt_start_date = db.DBYYYYMMDDformat(obj.lt_start_date);
                        obj.lt_end_date = db.DBYYYYMMDDformat(obj.lt_end_date);

                        string mnth = string.Empty;
                        string day = string.Empty;

                        if (DateTime.Parse(obj.lt_start_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(obj.lt_start_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(obj.lt_start_date).Month.ToString();

                        if (DateTime.Parse(obj.lt_start_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(obj.lt_start_date).Day.ToString();
                        else
                            day = DateTime.Parse(obj.lt_start_date).Day.ToString();

                        obj.lt_start_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.lt_start_date).Year, mnth, day);

                        if (DateTime.Parse(obj.lt_end_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(obj.lt_end_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(obj.lt_end_date).Month.ToString();

                        if (DateTime.Parse(obj.lt_end_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(obj.lt_end_date).Day.ToString();
                        else
                            day = DateTime.Parse(obj.lt_end_date).Day.ToString();

                        obj.lt_end_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.lt_end_date).Year, mnth, day);

                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_siso_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@Opr", 'R'),
                             new SqlParameter("@lt_number", obj.em_login_code),
                             new SqlParameter("@lt_company_code", obj.comp_code),
                             new SqlParameter("@lt_start_date", obj.lt_start_date),
                             new SqlParameter("@lt_end_date", obj.lt_end_date),
                             new SqlParameter("@lt_mgrctag", obj.lt_mgr_confirm_tag),
                             new SqlParameter("@lt_admin_remark", obj.lt_admin_remark),
                             new SqlParameter("@lt_leave_code",obj.leave_code)
                        });
                        dr.Close();
                        updated = 1;
                        if (obj.lt_mgr_confirm_tag == "C")
                        {
                            string leave_max = string.Empty;
                            string total_leave_taken = string.Empty;
                            double allowed_leaves = 0;
                            try
                            {
                                SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_siso_proc]",
                                new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", "A1"),
                                    new SqlParameter("@le_company_code",obj.comp_code),
                                    new SqlParameter("@lt_leave_code",obj.leave_code),
                                    new SqlParameter("@lt_number", obj.em_login_code),
                                });
                                if (dr1.Read())
                                {
                                    leave_max = dr1["el_maximum_days"].ToString();
                                }
                                dr1.Close();
                                SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_siso_proc]",
                              new List<SqlParameter>()
                                 {
                                    new SqlParameter("@opr", "BB"),
                                    new SqlParameter("@em_login_code",obj.em_login_code),
                                    new SqlParameter("@lt_leave_code",  obj.leave_code)
                                });
                                if (dr2.Read())
                                {
                                    total_leave_taken = dr2["el_days_taken"].ToString();
                                }
                                else
                                {
                                    total_leave_taken = "0.0";
                                }
                                dr2.Close();

                                /* dont delete // if leave taken greater than max allowed leave */
                                //if ((Convert.ToDouble(total_leave_taken) + Convert.ToDouble(obj.lt_days)) <= Convert.ToDouble(leave_max))
                                //{

                                allowed_leaves = Convert.ToDouble(leave_max) - Convert.ToDouble(total_leave_taken);
                                //if (obj.lt_days.Contains(".5"))
                                //{
                                //    int ind = obj.lt_days.IndexOf('.');
                                //    obj.lt_days = obj.lt_days.Substring(0, ind);
                                //    obj.lt_days = (Double.Parse(obj.lt_days) + 1).ToString();
                                //}
                                //for (int i = 0; i < Double.Parse(obj.lt_days); i++)

                                for (DateTime date = DateTime.Parse(obj.lt_start_date); date <= DateTime.Parse(obj.lt_end_date); date = date.AddDays(1))
                                {
                                    //for (int i = 0; i < Convert.ToDouble(obj.lt_days); i++)
                                    //{
                                    DateTime dt = DateTime.Parse(obj.lt_start_date);
                                    //DateTime dt_insert = dt.AddDays(i);
                                    empAttendance eatt = new empAttendance();

                                    string date1 = date.Year.ToString() + "-" + date.Month.ToString() + "-" + date.Day.ToString();

                                    eatt.emp_id = obj.em_login_code;
                                    //eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, dt_insert.Month, dt_insert.Day);
                                    eatt.att_date = date1;

                                    //if (i < allowed_leaves)
                                    //        eatt.att_flag = obj.leave_code;
                                    //    else
                                    //        eatt.att_flag = obj.leave_code;//"A";

                                    eatt.att_flag = obj.leave_code;

                                    //if (obj.lt_hours == "1.0")
                                    //    if (i == 0)
                                    //        eatt.att_flag = "HD";
                                    //if (obj.lt_hours == "0.1")
                                    //    if (i == Convert.ToDouble(obj.lt_days) - 1)
                                    //        eatt.att_flag = "HD";
                                    //if (obj.lt_hours == "1.1")
                                    //{
                                    //    if (i == Convert.ToDouble(obj.lt_days) - 1)
                                    //        eatt.att_flag = "HD";
                                    //    if (i == 0)
                                    //        eatt.att_flag = "HD";
                                    //}

                                    eatt.comment = String.Empty;
                                    eatt.s1in = String.Empty;
                                    eatt.s1out = String.Empty;
                                    eatt.s2in = String.Empty;
                                    eatt.s2out = String.Empty;
                                    bool r = MarkEmpDayAttedance_OnLeaveTransaction_SISO(eatt);
                                }

                                if (allowed_leaves <= Convert.ToDouble(obj.lt_days))
                                {
                                    extra = Convert.ToDouble(obj.lt_days) - allowed_leaves;
                                    daystoadd = allowed_leaves;
                                }
                                else
                                {
                                    daystoadd = Convert.ToDouble(obj.lt_days);
                                    extra = 0;
                                }

                                SqlDataReader dr3 = db.ExecuteStoreProcedure("[pays_Leave_Register]",
                                new List<SqlParameter>()
                                {
                                        new SqlParameter("@Opr", 'U'),
                                        new SqlParameter("@el_number", obj.em_login_code),
                                        new SqlParameter("@el_leave_code", obj.leave_code),
                                        new SqlParameter("@el_company_code", obj.comp_code),
                                        new SqlParameter("@el_days_taken", daystoadd),
                                        new SqlParameter("@el_extra_days", extra),
                                        new SqlParameter("@lt_start_date", obj.lt_start_date),
                                        new SqlParameter("@lt_end_date", obj.lt_end_date)
                                });
                                if (dr3.RecordsAffected > 0)
                                {
                                    updated = 1;
                                }
                                /* dont delete // if leave taken greater than max allowed leave */
                                //}
                                //else
                                //{
                                //     // if leave taken greater than max allowed leave
                                //SqlDataReader dr3 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                                //new List<SqlParameter>()
                                // {
                                //     new SqlParameter("@Opr", 'R'),
                                //     new SqlParameter("@lt_number", obj.em_login_code),
                                //     new SqlParameter("@lt_company_code", obj.comp_code),
                                //     new SqlParameter("@lt_start_date", obj.lt_start_date),
                                //     new SqlParameter("@lt_end_date", obj.lt_end_date),
                                //     new SqlParameter("@lt_mgrctag", 'N'),
                                //     new SqlParameter("@lt_admin_remark", obj.lt_admin_remark),
                                //     new SqlParameter("@lt_leave_code",obj.leave_code)
                                //});
                                //dr3.Close();
                                //updated = 2;
                                //}
                                /* dont delete*/
                            }
                            catch (Exception de)
                            { }
                        }

                        using (DBConnection db1 = new DBConnection())
                        {
                            db.Open();

                            try
                            {
                                //For Requirement of OES / DPS MIS
                                //While applying leave / Absent check if absent or leave marked on Sunday then check for Thursday for attendance 
                                //status if its absent or leave mark Friday/ Saturday as Absent.

                                string if_includeholidays = "N";
                                SqlDataReader dr33_get = db1.ExecuteStoreProcedure("GetData",
                                     new List<SqlParameter>()
                                    {
                            new SqlParameter("tbl_name", "[pays].[pays_parameter]"),
                                    new SqlParameter("tbl_col_name1", "[pays_appl_parameter]"),
                                    new SqlParameter("@tbl_cond", "[pays_appl_code] ='Per304' and [pays_appl_form_field]='IncludeHolidays' "),
                                    });
                                if (dr33_get.RecordsAffected > 0)
                                {
                                    if (dr33_get.Read())
                                        if_includeholidays = dr33_get["pays_appl_parameter"].ToString();
                                }


                                if (if_includeholidays == "Y" || if_includeholidays == "y")
                                {
                                    bool r = false;
                                    DateTime startDate = DateTime.Parse(obj.lt_start_date);
                                    DateTime endDate = DateTime.Parse(obj.lt_end_date);

                                    while (endDate > startDate)
                                    {
                                        r = (endDate.DayOfWeek == DayOfWeek.Sunday);
                                        if (r)
                                        {
                                            //Sunday Present
                                            DateTime checkThur = endDate.AddDays(-3);
                                            string if_leave = "N";
                                            //Check if Thursday is Leave or Absent
                                            SqlDataReader dr333 = db1.ExecuteStoreProcedure("GetData",
                                     new List<SqlParameter>()
                                    {
                            new SqlParameter("@tbl_name", "[pays].[pays_attendance_daily]"),
                                                new SqlParameter("tbl_col_name1", "[att_flag]"),
                                                new SqlParameter("@tbl_cond", "[att_emp_id] ='" + obj.em_login_code + "' and [att_date]='" + checkThur + "'"),
                                                });
                                            if (dr333.RecordsAffected > 0)
                                            {
                                                if (dr333.Read())
                                                    if_leave = dr333["att_flag"].ToString();
                                            }


                                            if (if_leave == "A" || if_leave == "L")
                                            {
                                                for (int z = 1; z < 3; z++)
                                                {
                                                    empAttendance eatt = new empAttendance();

                                                    eatt.emp_id = obj.em_login_code;
                                                    eatt.att_date = string.Format("{0}-{1}-{2}", checkThur.AddDays(z).Year, checkThur.AddDays(z).Month, checkThur.AddDays(z).Day);
                                                    eatt.att_flag = "A";
                                                    eatt.comment = String.Empty;
                                                    eatt.s1in = String.Empty;
                                                    eatt.s1out = String.Empty;
                                                    eatt.s2in = String.Empty;
                                                    eatt.s2out = String.Empty;
                                                    bool r1 = MarkEmpDayAttedance_OnLeaveTransaction_SISO(eatt);
                                                }
                                            }
                                        }
                                        endDate = endDate.AddDays(-1);
                                    }
                                }
                            }
                            catch (Exception dee)
                            { }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("Update_pay_empleavedoc")]
        public HttpResponseMessage Update_pay_empleavedoc(Pers300_le obj)
        {
            bool updated = false;
            string manager_tag = "E";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@Opr", 'R'),
                new SqlParameter("@lt_number", obj.em_login_code),
                new SqlParameter("@lt_company_code", obj.comp_code),
                new SqlParameter("@lt_start_date",db.DBYYYYMMDDformat(obj.lt_start_date)),
                new SqlParameter("@lt_end_date",db.DBYYYYMMDDformat(obj.lt_end_date)),
                new SqlParameter("@lt_mgrctag", "E"),
                new SqlParameter("@lt_modified_on",db.DBYYYYMMDDformat(DateTime.Now.Date.ToString())),
                new SqlParameter("@lt_remarks", obj.lt_remarks)
                });
                    if (dr.RecordsAffected > 0)
                    {
                        updated = true;
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("GetEMPLeaveDetails")]
        public HttpResponseMessage GetEMPLeaveDetails(string leave_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            //
            try
            {
                //  con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ToString());
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@Opr", "T"),
                new SqlParameter("@lt_number", leave_code),
               
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.comp_name = dr["CompanyName"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat((dr["lt_end_date"].ToString()));
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = db.UIDDMMYYYYformat((dr["lt_modified_on"].ToString()));
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            hrmsobj.requestdate = db.UIDDMMYYYYformat((dr["lt_request_date"].ToString()));
                            hrmsobj.updated = false;
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }

        private bool MarkEmpDayAttedance(empAttendance obj)
        {
            List<Pers300_le> hol_nwdays = new List<Pers300_le>();
            List<string> emps = new List<string>();

            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_attendance_daily_m]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "I"),
                new SqlParameter("@emp_id", obj.emp_id),
                new SqlParameter("@att_date", db.DBYYYYMMDDformat(obj.att_date)),
                new SqlParameter("@s1in", obj.s1in),
                new SqlParameter("@s1out", obj.s1out),
                new SqlParameter("@s2in", obj.s2in),
                new SqlParameter("@s2out", obj.s2out),
                new SqlParameter("@comment", obj.comment)

                         });
                    emps.Add(obj.emp_id);
                    hol_nwdays = Get_EMPHolidaysNonWorkingDays(emps);


                    DateTime dt = DateTime.Parse(obj.att_date);
                    int cnt = (from x in hol_nwdays where x.holiday == (obj.att_date).ToString() select x.leavest_ex).Count();

                    if (cnt > 0)
                    {
                        var pl = (from x in hol_nwdays where x.holiday == (obj.att_date) select x.leavest_ex).ToList();
                        List<string> hdd = new List<string>();
                        hdd.Add(pl.ElementAt(0));
                        string st = pl.ElementAt(0).ToString();
                        new SqlParameter("@att_flag", pl.ElementAt(0).ToString());
                    }
                    else
                        new SqlParameter("@att_flag", obj.att_flag);

                    if (dr.RecordsAffected > 0)
                        res = true;
                    else
                        res = false;
                }

            }
            catch (Exception x)
            { }

            return res;
        }

        [Route("Get_EMPAllLeaveDates")]
        public HttpResponseMessage Get_EMPAllLeaveDates(string compcode, string empid)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("tbl_col_name1", "[lt_start_date],[lt_end_date]"),
                new SqlParameter("@tbl_cond", "[lt_number]='" + empid + "' and ([lt_mgr_confirm_tag]='N' or [lt_mgr_confirm_tag]='C')"),
                 });
                    while (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        obj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                        obj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                        hrmsobj.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }

        [Route("Get_EMPBalanceLeaves")]
        public List<Pers300_le> Get_EMPBalanceLeaves(string compcode, string empid, string leave_code)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("tbl_name", "[pays].[pays_leave_register]"),
                new SqlParameter("tbl_col_name1", "[el_days_taken],[el_maximum_days],[el_extra_days]"),
                new SqlParameter("@tbl_cond", "[el_company_code]=" + compcode + " and  [el_number]='" + empid + "' and  [el_leave_code]='" + leave_code + "'")
                         });
                    while (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        obj.el_days_taken = dr["el_days_taken"].ToString();
                        obj.el_maximum_days = dr["el_maximum_days"].ToString();
                        obj.el_extra_days = dr["el_extra_days"].ToString();
                        hrmsobj.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {

            }

            return hrmsobj;
        }

        [Route("Get_EMPLDetails")]
        public HttpResponseMessage Get_EMPLDetails(List<string> EMPIDS, string leavecode)
        {
            string cond = string.Empty;
            for (int p = 0; p < EMPIDS.Count; p++)
            {
                cond = cond + " [lt_number]='" + EMPIDS[p] + "' OR";
            }
            cond = cond.Substring(0, cond.Length - 3);
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            List<Pers300_le> bal_leave = new List<Pers300_le>();
            List<Pers300_le> alldates = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@tbl_name", "[pays].[sims].[pays_leave_details_proc]"),
                new SqlParameter("@tbl_col_name1", "[lt_number] as EMP,lt_leave_code,(SELECT [comp_name] FROM [fins].[fins_company_master] WHERE [comp_code]=[lt_company_code]) As CompanyName,(SELECT COALESCE([em_first_name],'')+' '+ COALESCE([em_middle_name],'')+' '+ COALESCE([em_last_name],'') FROM [pays].[pays_employee] WHERE [em_login_code]=[lt_number]) AS EmpName,lt_company_code,(SELECT [cl_desc] FROM [pays].[pays_common_code] WHERE [cl_code]=[lt_leave_code])As LeaveDescription,[lt_start_date],[lt_end_date],[lt_days],[lt_hours],(Select [pays_appl_form_field_value1] from [pays].[pays_parameter] where [pays_appl_form_field]='ManagerConfirmTag' and [pays_appl_parameter]=[lt_mgr_confirm_tag]) as leavestatus,[lt_modified_on],[lt_remarks],[lt_converted_hours],[lt_request_date]"),
                new SqlParameter("@tbl_cond", "(" + cond + ")AND [lt_mgr_confirm_tag]='C' AND [lt_leave_code]=" + leavecode + "")
                });
                    while (dr.Read())
                    {
                        Pers300_le hrmsobj = new Pers300_le();
                        hrmsobj.EmpName = dr["EmpName"].ToString();
                        hrmsobj.em_login_code = dr["EMP"].ToString();
                        hrmsobj.comp_code = dr["lt_company_code"].ToString();
                        hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                        hrmsobj.comp_name = dr["CompanyName"].ToString();
                        hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                        DateTime dt;
                        DateTime.TryParse(dr["lt_end_date"].ToString(), out dt);
                        //hrmsobj.lt_end_dateD = dt;
                        hrmsobj.lt_end_dateD = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                        hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                        hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                        hrmsobj.lt_days = dr["lt_days"].ToString();
                        hrmsobj.lt_hours = dr["lt_hours"].ToString();
                        hrmsobj.lt_modified_on = db.UIDDMMYYYYformat(dr["lt_modified_on"].ToString());
                        hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                        hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                        hrmsobj.requestdate = db.UIDDMMYYYYformat((dr["lt_request_date"].ToString()));
                        hrmsobj.updated = false;
                        hrmsobj.lt_end_date_old = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                        hrmsobj.lt_end_date_new =db.UIDDMMYYYYformat( dr["lt_end_date"].ToString());
                        hrmsobj.lt_admin_remark = string.Empty;
                        hrmsobj.lt_admin_cancel_remark = string.Empty;

                        bal_leave = Get_EMPBalanceLeaves(hrmsobj.comp_code, hrmsobj.em_login_code, hrmsobj.leave_code);
                        if (bal_leave.Count > 0)
                        {
                            foreach (Pers300_le ple in bal_leave)
                            {
                                hrmsobj.el_days_taken = ple.el_days_taken;
                                hrmsobj.el_maximum_days = ple.el_maximum_days;
                                hrmsobj.el_extra_days = ple.el_extra_days;
                            }
                        }
                        else
                        {
                            hrmsobj.el_days_taken = "0";
                            hrmsobj.el_maximum_days = "0";
                            hrmsobj.el_extra_days = "0";
                        }

                        hrmsobj.emp_balance_leaves = (decimal.Parse(hrmsobj.el_maximum_days) - decimal.Parse(hrmsobj.el_days_taken)).ToString();

                        BlackDates = new List<DateTime>();
                        BlackDates = Get_EMPHolidaysNonWorkingPendConfirmedLeaves(hrmsobj.em_login_code);

                        for (DateTime date = DateTime.Parse(hrmsobj.lt_start_date); date <= DateTime.Parse(hrmsobj.lt_end_date); date = date.AddDays(1))
                        {
                            if (date != DateTime.Parse(hrmsobj.lt_start_date))
                                BlackDates.Remove(date);
                        }
                        hrmsobj.BlackOutDates = BlackDates;
                        lstEmp_leave.Add(hrmsobj);
                    }
                }

            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);


        }

        [Route("Update_endleave")]
        public HttpResponseMessage Update_endleave(List<Pers300_le> pay)
        {
            bool updated = false;
            foreach (Pers300_le obj in pay)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@Opr", 'V'),
                    new SqlParameter("@lt_number", obj.em_login_code),
                    new SqlParameter("@lt_company_code", obj.comp_code),
                    new SqlParameter("@lt_leave_code", obj.leave_code),
                    new SqlParameter("@lt_days", obj.lt_days),
                    new SqlParameter("@lt_hours", obj.lt_hours),
                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lt_start_date)),
                    new SqlParameter("@lt_end_date", db.DBYYYYMMDDformat(obj.lt_end_dateD)),
                    new SqlParameter("@lt_modified_on", db.DBYYYYMMDDformat(DateTime.Now.Date.ToString())),
                    new SqlParameter("@lt_mgrctag", 'C'),
                    new SqlParameter("@lt_request_date",db.DBYYYYMMDDformat( DateTime.Now.Date.ToString())),
                    new SqlParameter("@lt_end_date_new", db.DBYYYYMMDDformat(obj.lt_end_date))
                });
                        if (dr.RecordsAffected > 0)
                        {
                            updated = true;
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);

        }

        [Route("Get_Current_AcademicYear")]
        public HttpResponseMessage Get_Current_AcademicYear()
        {
            List<Pers300_le> currentYear = new List<Pers300_le>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@tbl_name", "[sims_financial_year]"),
                new SqlParameter("@tbl_col_name1", "[sims_financial_year_start_date],[sims_financial_year_end_date]"),
                new SqlParameter("@tbl_cond", "[sims_financial_year]=(Select [fins_appl_form_field_value1] from [fins].[fins_parameter] where [fins_appl_code]='Fin138' and [fins_appl_form_field]='Current Year' and [fins_appl_parameter]='C')"),
                         });
                    if (dr.Read())
                    {
                        Pers300_le per = new Pers300_le();
                        per.sims_financial_year_start_date = DateTime.Parse(dr["sims_financial_year_start_date"].ToString());
                        per.sims_financial_year_end_date = DateTime.Parse(dr["sims_financial_year_end_date"].ToString());
                        currentYear.Add(per);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, currentYear);

        }

        public class temp
        {
            public string str { get; set; }
            public string month { get; set; }

        }

        [Route("EMPAllHolidays")]
        public HttpResponseMessage EMPAllHolidays(List<temp> empids)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>(); string month1 = "0";
            try
            {
                foreach (temp emp in empids)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        month1 = emp.month;
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                        { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@lt_number", emp.str),
                            new SqlParameter("@month", emp.month)
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Pers300_le obj = new Pers300_le();
                                //obj.holidaystart = db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
                                //obj.holidayend = db.UIDDMMYYYYformat(dr["sims_to_date"].ToString());
                                obj.holidaystart = dr["sims_from_date"].ToString();
                                obj.holidayend = dr["sims_to_date"].ToString();

                                //DateTime dt1 = DateTime.Parse(DateTime.Parse(dr["sims_from_date"].ToString()).Year + "-" + DateTime.Parse(dr["sims_from_date"].ToString()).Month + "-" + DateTime.Parse(dr["sims_from_date"].ToString()).Day);
                                //obj.holiday = dt1;

                                obj.holiday = db.UIDDMMYYYYformat(dr["sims_from_date"].ToString());
                                obj.updated = false;
                                obj.em_login_code = emp.str;
                                hrmsobj.Add(obj);
                            }
                        }
                    }
                }

                foreach (Pers300_le lve in hrmsobj)
                {
                    for (DateTime date = DateTime.Parse(lve.holidaystart); date <= DateTime.Parse(lve.holidayend); date = date.AddDays(1))
                    //for(DateTime date = db.UIDDMMYYYYformat(lve.holidaystart.ToString()); date <= DateTime.Parse(lve.holidayend); date = date.AddDays(1))
                    {
                        Pers300_le hd = new Pers300_le();
                        if (date.Month.ToString() == month1)
                        {

                            string date1 = date.Year.ToString() + "-" + date.Month.ToString() + "-" + date.Day.ToString();
                            hd.holiday = date1;//DateTime.Parse(date1);
                            hd.holiday_flag = false;
                            hd.em_login_code = lve.em_login_code;
                            int cnt = (from x in allDates where x.holiday == date.ToString() select x).Count();
                            if (cnt <= 0)
                                if (date.Month == int.Parse(empids[0].month))
                                    allDates.Add(hd);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, allDates); 

        }

        public List<Pers300_le> Get_EMPHolidaysNonWorkingDays(List<string> empids)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                foreach (string emp in empids)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@Opr", "V1"),
                    new SqlParameter("@lt_number", emp)
                    });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Pers300_le obj = new Pers300_le();
                                obj.holidaystart = dr["sims_from_date"].ToString();
                                obj.holidayend = dr["sims_to_date"].ToString();
                                obj.holiday =db.UIDDMMYYYYformat((dr["sims_from_date"].ToString()));
                                obj.leavest_ex = dr["LeaveStatus"].ToString();
                                obj.updated = false;
                                obj.em_login_code = emp;
                                hrmsobj.Add(obj);
                            }
                        }

                        if (hrmsobj.Count > 0)
                        {
                            foreach (Pers300_le lve in hrmsobj)
                            {
                                for (DateTime date = DateTime.Parse(lve.holidaystart); date <= DateTime.Parse(lve.holidayend); date = date.AddDays(1))
                                {
                                    Pers300_le hd = new Pers300_le();
                                    string dte = string.Format("{0}-{1}-{2}", date.Year, date.Month, date.Day);
                                    hd.holiday = dte;//Convert.ToDateTime(dte);
                                    hd.em_login_code = lve.em_login_code;
                                    hd.leavest_ex = lve.leavest_ex;

                                    int cnt = (from x in allDates where x.holiday == date.ToString() select x).Count();
                                    if (cnt <= 0)
                                        allDates.Add(hd);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            return allDates;

        }

        [Route("Get_EMPHolidaysNonWorkingPendConfirmedLeaves")]
        public List<DateTime> Get_EMPHolidaysNonWorkingPendConfirmedLeaves(string empid)
        {
            BlackDates.Clear();
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@Opr", "V1"),
                new SqlParameter("@lt_number", empid)
                         });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.holidaystart = dr1["sims_from_date"].ToString();
                            obj.holidayend = dr1["sims_to_date"].ToString();
                            obj.holiday = db.UIDDMMYYYYformat((dr1["sims_from_date"].ToString()));
                            obj.leavest_ex = dr1["LeaveStatus"].ToString();
                            obj.updated = false;
                            obj.em_login_code = empid;
                            hrmsobj.Add(obj);
                        }
                    }
                    if (hrmsobj.Count > 0)
                    {
                        foreach (Pers300_le lve in hrmsobj)
                        {
                            for (DateTime date = DateTime.Parse(lve.holidaystart); date <= DateTime.Parse(lve.holidayend); date = date.AddDays(1))
                            {
                                BlackDates.Add(date);
                            }
                        }
                    }

                    List<Pers300_le> leaves = new List<Pers300_le>();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "K"),
                new SqlParameter("@lt_number", empid),
                new SqlParameter("@lt_mgr_confirm_tag",'C')
                });
                    while (dr1.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        obj.lt_start_date = db.UIDDMMYYYYformat(dr1["lt_start_date"].ToString());
                        obj.lt_end_date = db.UIDDMMYYYYformat(dr1["lt_end_date"].ToString());
                        leaves.Add(obj);
                    }
                    if (leaves.Count > 0)
                    {
                        foreach (Pers300_le lve in leaves)
                        {
                            for (DateTime date = DateTime.Parse(lve.lt_start_date); date <= DateTime.Parse(lve.lt_end_date); date = date.AddDays(1))
                            {
                                BlackDates.Add(date);
                            }
                        }
                    }
                }

            }
            catch (Exception ee)
            {

            }
            return BlackDates;
        }

        [Route("CAL_geteavesforEmp")]
        public HttpResponseMessage CAL_geteavesforEmp(string empid, string leave_code)
        {
            List<CAL> mod_list = new List<CAL>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_approved_leave_cancel_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_emp_code", empid),
                new SqlParameter("@sims_leave_code", leave_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CAL hrmsobj = new CAL();
                            hrmsobj.sims_emp_code = empid;
                            hrmsobj.sims_emp_name = dr["EmpName"].ToString();
                            hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                            hrmsobj.leave_desc = dr["LeaveType"].ToString();
                            hrmsobj.sims_start_date = db.UIDDMMYYYYformat((dr["lt_start_date"].ToString()));
                            hrmsobj.sims_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.sims_total_days = dr["TotalDays"].ToString();
                            hrmsobj.sims_remark = string.Empty;
                            hrmsobj.isActive = false;
                            mod_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CALeaves")]
        public HttpResponseMessage CALeaves(List<Pers300_le> coll)
        {
            bool returnval = false;
            foreach (Pers300_le data in coll)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {

                        data.lt_start_date = db.DBYYYYMMDDformat(data.lt_start_date);
                        data.lt_end_date = db.DBYYYYMMDDformat(data.lt_end_date);

                        string mnth = string.Empty;
                        string day = string.Empty;

                        if (DateTime.Parse(data.lt_start_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_start_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_start_date).Month.ToString();

                        if (DateTime.Parse(data.lt_start_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_start_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_start_date).Day.ToString();

                        data.lt_start_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_start_date).Year, mnth, day);

                        if (DateTime.Parse(data.lt_end_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_end_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_end_date).Month.ToString();

                        if (DateTime.Parse(data.lt_end_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_end_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_end_date).Day.ToString();

                        data.lt_end_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_end_date).Year, mnth, day);


                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_approved_leave_cancel_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", "U"),
                    new SqlParameter("@sims_emp_code", data.em_number),
                    new SqlParameter("@sims_leave_code", data.leave_code),
                    new SqlParameter("@days_taken", decimal.Parse(data.lt_days).ToString()),
                    new SqlParameter("@start_date",data.lt_start_date),
                    new SqlParameter("@end_date",data.lt_end_date),
                    new SqlParameter("@remarks", data.lt_remarks),
                    new SqlParameter("@lt_no", data.lt_no)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            returnval = true;
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, returnval);

        }

        [Route("Assign_LWPforEmployees")]
        public HttpResponseMessage Assign_LWPforEmployees(List<Pers300_le> pay)
        {
            DBConnection db1 = new DBConnection();
            bool insrted = false;
            string enddate = "";
            foreach (Pers300_le obj in pay)
            {
                int k = 0;
                for (k = 0; k < int.Parse(obj.ndays); k++)
                {

                    DateTime dt = DateTime.Parse(db1.DBYYYYMMDDformat(obj.lstart_date.ToString()));
                    DateTime dt_insert = dt.AddDays(k);
                    empAttendance eatt = new empAttendance();
                    
                    string mnth = string.Empty;
                    string day = string.Empty;

                    if (dt_insert.Month.ToString().Length == 1)
                        mnth = "0" + dt_insert.Month.ToString();
                    else
                        mnth = dt_insert.Month.ToString();

                    if (dt_insert.Day.ToString().Length == 1)
                        day = "0" + dt_insert.Day.ToString();
                    else
                        day = dt_insert.Day.ToString();

                    eatt.emp_id = obj.em_login_code;
                    eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, mnth, day);
                    enddate = eatt.att_date;
                    eatt.att_flag = "A";
                    eatt.comment = String.Empty;
                    eatt.s1in = String.Empty;
                    eatt.s1out = String.Empty;
                    eatt.s2in = String.Empty;
                    eatt.s2out = String.Empty;
                    bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                }
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@Opr", 'X'),
                    new SqlParameter("@lt_number", obj.em_login_code),
                    new SqlParameter("@lt_company_code", obj.comp_code),
                    new SqlParameter("@lt_leave_code", "A"),
                    new SqlParameter("@lt_days", obj.ndays),
                    new SqlParameter("@lt_hours", obj.lt_hours),
                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lstart_date)),
                    new SqlParameter("@lt_end_date",enddate),
                    new SqlParameter("@lt_mgrctag", 'C'),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    insrted = false;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);

        }

        [Route("Assign_LWPforEmployees_siserp")]
        public HttpResponseMessage Assign_LWPforEmployees_siserp(List<Pers300_le> pay)
        {
            DBConnection db1 = new DBConnection();
            bool insrted = false;
            string enddate = "";
            foreach (Pers300_le obj in pay)
            { 
                int k = 0;
                for (k = 0; k < int.Parse(obj.ndays); k++)
                {

                    DateTime dt = DateTime.Parse(db1.DBYYYYMMDDformat(obj.lstart_date.ToString()));
                    DateTime dt_insert = dt.AddDays(k);
                    empAttendance eatt = new empAttendance();

                    string mnth = string.Empty;
                    string day = string.Empty;

                    if (dt_insert.Month.ToString().Length == 1)
                        mnth = "0" + dt_insert.Month.ToString();
                    else
                        mnth = dt_insert.Month.ToString();

                    if (dt_insert.Day.ToString().Length == 1)
                        day = "0" + dt_insert.Day.ToString();
                    else
                        day = dt_insert.Day.ToString();

                    eatt.emp_id = obj.em_login_code;
                    eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, mnth, day);
                    enddate = eatt.att_date;
                    eatt.att_flag = "LP";
                    eatt.comment = String.Empty;
                    eatt.s1in = String.Empty;
                    eatt.s1out = String.Empty;
                    eatt.s2in = String.Empty;
                    eatt.s2out = String.Empty;
                    bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                }
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@Opr", 'X'),
                    new SqlParameter("@lt_number", obj.em_login_code),
                    new SqlParameter("@lt_company_code", obj.comp_code),
                    new SqlParameter("@lt_leave_code", "LP"),
                    new SqlParameter("@lt_days", obj.ndays),
                    new SqlParameter("@lt_hours", obj.lt_hours),
                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lstart_date)),
                    new SqlParameter("@lt_end_date",enddate),
                    new SqlParameter("@lt_mgrctag", 'C'),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    insrted = false;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);

        }

        private bool MarkEmpDayAttedance_OnLeaveTransaction(empAttendance obj)
        {
            List<Pers300_le> hol_nwdays = new List<Pers300_le>();
            List<string> emps = new List<string>();
            string flag = "";
            emps.Add(obj.emp_id);
            hol_nwdays = Get_EMPHolidaysNonWorkingDays(emps);

            DateTime dt = DateTime.Parse(obj.att_date);
            int cnt = (from x in hol_nwdays where x.holiday == DateTime.Parse(obj.att_date).ToString() select x.leavest_ex).Count();

            if (cnt > 0)
            {
                var pl = (from x in hol_nwdays where x.holiday == DateTime.Parse(obj.att_date).ToString() select x.leavest_ex).ToList();
                List<string> hdd = new List<string>();
                hdd.Add(pl.ElementAt(0));
                flag = pl.ElementAt(0).ToString();
            }
            else
                flag = obj.att_flag;

            string mnth = string.Empty;
            string day = string.Empty;

            if (DateTime.Parse(obj.att_date).Month.ToString().Length == 1)
                mnth = "0" + DateTime.Parse(obj.att_date).Month.ToString();
            else
                mnth = DateTime.Parse(obj.att_date).Month.ToString();

            if (DateTime.Parse(obj.att_date).Day.ToString().Length == 1)
                day = "0" + DateTime.Parse(obj.att_date).Day.ToString();
            else
                day = DateTime.Parse(obj.att_date).Day.ToString();

            obj.att_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.att_date).Year, mnth, day);

            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "CC"),
                new SqlParameter("@elnumber", obj.emp_id),
                new SqlParameter("@date_insert", obj.att_date),
                new SqlParameter("@attflag", obj.att_flag), 
                         });

                    if (dr.RecordsAffected > 0)
                        res = true;
                    else
                        res = false;

                    dr.Close();
                }
            }
            catch (Exception x)
            { }

            return res;
        }

        private bool MarkEmpDayAttedance_OnLeaveTransaction_SISO(empAttendance obj)
        {
            List<Pers300_le> hol_nwdays = new List<Pers300_le>();
            List<string> emps = new List<string>();
            string flag = "";
            emps.Add(obj.emp_id);
            hol_nwdays = Get_EMPHolidaysNonWorkingDays(emps);

            DateTime dt = DateTime.Parse(obj.att_date);
            int cnt = (from x in hol_nwdays where x.holiday == DateTime.Parse(obj.att_date).ToString() select x.leavest_ex).Count();

            if (cnt > 0)
            {
                var pl = (from x in hol_nwdays where x.holiday == DateTime.Parse(obj.att_date).ToString() select x.leavest_ex).ToList();
                List<string> hdd = new List<string>();
                hdd.Add(pl.ElementAt(0));
                flag = pl.ElementAt(0).ToString();
            }
            else
                flag = obj.att_flag;

            string mnth = string.Empty;
            string day = string.Empty;

            if (DateTime.Parse(obj.att_date).Month.ToString().Length == 1)
                mnth = "0" + DateTime.Parse(obj.att_date).Month.ToString();
            else
                mnth = DateTime.Parse(obj.att_date).Month.ToString();

            if (DateTime.Parse(obj.att_date).Day.ToString().Length == 1)
                day = "0" + DateTime.Parse(obj.att_date).Day.ToString();
            else
                day = DateTime.Parse(obj.att_date).Day.ToString();

            obj.att_date = string.Format("{0}-{1}-{2}", DateTime.Parse(obj.att_date).Year, mnth, day);

            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_siso_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "CC"),
                        new SqlParameter("@elnumber", obj.emp_id),
                        new SqlParameter("@date_insert", obj.att_date),
                        new SqlParameter("@attflag", obj.att_flag),
                    });

                    if (dr.RecordsAffected > 0)
                        res = true;
                    else
                        res = false;

                    dr.Close();
                }
            }
            catch (Exception x)
            { }

            return res;
        }

        [Route("GET_LWPforEmployees")]
        public HttpResponseMessage GET_EmployeesforLWP(string empcode, string date)
        {
            List<Pers300_le> lst = new List<Pers300_le>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@lt_start_date",date),
                new SqlParameter("@lt_number", empcode),
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrms = new Pers300_le();
                            hrms.EmpName = dr["EmployeeName"].ToString();
                            hrms.leave_code = dr["lt_leave_code"].ToString();
                            hrms.lt_no = dr["lt_number"].ToString();
                            hrms.comp_code = dr["lt_company_code"].ToString();
                            hrms.comp_name = dr["CompanyName"].ToString();
                            hrms.leave_type = dr["LeaveDescription"].ToString();
                            hrms.lt_start_date = db.UIDDMMYYYYformat((dr["lt_start_date"].ToString()));
                            hrms.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrms.lt_days = dr["lt_days"].ToString();
                            hrms.lt_hours = dr["lt_hours"].ToString();
                            hrms.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            hrms.lt_modified_on = db.UIDDMMYYYYformat(dr["lt_modified_on"].ToString());
                            hrms.lt_remarks = dr["lt_remarks"].ToString();
                            hrms.lt_converted_hours = dr["lt_converted_hours"].ToString();
                            hrms.updated = false;
                            lst.Add(hrms);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GET_ManagerConfirmTag")]
        public HttpResponseMessage GET_ManagerConfirmTag()
        {
            List<Pers300_le> lst = new List<Pers300_le>();
            string comp_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "MT")
                
               });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrms = new Pers300_le();
                            hrms.ConfirmTag_type = dr["pays_appl_form_field_value1"].ToString();
                            hrms.ConfirmTag_code = dr["pays_appl_parameter"].ToString();
                            lst.Add(hrms);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("Get_dept_Idcompany")]
        public HttpResponseMessage Get_dept_Idcompany(string compcode)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A3"),          
                            new SqlParameter("@lt_company_code", compcode)                
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.comp_code = dr["codp_dept_no"].ToString();
                            obj.comp_name = dr["codp_dept_name"].ToString();
                            hrmsobj.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("Get_dept_IdWithCompany")]
        public HttpResponseMessage Get_dept_IdWithCompany(string compcode)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),                
                            new SqlParameter("@lt_company_code", compcode)                
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.comp_code = dr["codp_dept_no"].ToString();
                            obj.comp_name = dr["codp_dept_name"].ToString();
                            hrmsobj.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("Get_dept_Id")]
        public HttpResponseMessage Get_dept_Id()
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),                
                            new SqlParameter("@opr", "B")                
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le obj = new Pers300_le();
                            obj.comp_code = dr["codp_dept_no"].ToString();
                            obj.comp_name = dr["codp_dept_name"].ToString();
                            hrmsobj.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("Get_Employee_leave_balance")]
        public HttpResponseMessage Get_Employee_leave_balance(string leave_code, string dept_code, string login_code, string compcode)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();

            if (leave_code == "undefined")
                leave_code = null;
            if (dept_code == "undefined")
                dept_code = null;
            if (login_code == "undefined")
                login_code = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),            
                            new SqlParameter("@lt_leave_code", leave_code),                
                            new SqlParameter("@em_dept_code", dept_code),               
                            new SqlParameter("@em_login_code", login_code),               
                            new SqlParameter("@lt_company_code", compcode),               
                        });
                    while (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();

                        obj.em_number = dr["el_number"].ToString();
                        obj.EmpName = dr["Emp_name"].ToString();
                        obj.leave_code = dr["el_leave_code"].ToString();
                        obj.leave_desc = dr["cl_desc"].ToString();
                        obj.el_days_taken = dr["el_days_taken"].ToString();
                        obj.el_maximum_days = dr["el_maximum_days"].ToString();
                        obj.el_extra_days = dr["el_extra_days"].ToString();
                        obj.emp_balance_leaves = dr["bal_leave"].ToString();

                        hrmsobj.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("Get_approed_employee_list")]
        public HttpResponseMessage Get_approed_employee_list(string leave_code, string dept_code, string login_code)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();

            if (leave_code == "undefined")
                leave_code = null;
            if (dept_code == "undefined")
                dept_code = null;
            if (login_code == "undefined")
                login_code = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_leave_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),            
                            new SqlParameter("@lt_leave_code", leave_code),                
                            new SqlParameter("@em_dept_code", dept_code),               
                            new SqlParameter("@em_login_code", login_code),               
                        });
                    while (dr.Read())
                    {
                        Pers300_le obj = new Pers300_le();
                        obj.lt_no = dr["lt_no"].ToString();
                        obj.em_login_code = dr["em_login_code"].ToString();
                        obj.em_number = dr["lt_number"].ToString();
                        obj.EmpName = dr["Emp_name"].ToString();
                        obj.leave_code = dr["lt_leave_code"].ToString();
                        obj.leave_desc = dr["cl_desc"].ToString();
                        obj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                        obj.leave_tag_desc = dr["pays_appl_form_field_value1"].ToString();
                        obj.lt_start_date =db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                        obj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                        obj.lt_admin_cancel_remark = dr["lt_admin_cancel_remark"].ToString();
                        obj.lt_days = dr["lt_days"].ToString();
                        obj.lt_no_cancel = dr["lt_no"].ToString();
                        obj.lt_no_update = dr["lt_no"].ToString();
                        obj.lt_end_date_new = "";
                        hrmsobj.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);
        }

        [Route("UpdateLeaves")]
        public HttpResponseMessage UpdateLeaves(List<Pers300_le> coll)
        {
            bool returnval = false;
            foreach (Pers300_le data in coll)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {

                        data.lt_start_date = db.DBYYYYMMDDformat(data.lt_start_date);
                        data.lt_end_date = db.DBYYYYMMDDformat(data.lt_end_date);

                        string mnth = string.Empty;
                        string day = string.Empty;

                        if (DateTime.Parse(data.lt_start_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_start_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_start_date).Month.ToString();

                        if (DateTime.Parse(data.lt_start_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_start_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_start_date).Day.ToString();

                        data.lt_start_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_start_date).Year, mnth, day);

                        if (DateTime.Parse(data.lt_end_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_end_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_end_date).Month.ToString();

                        if (DateTime.Parse(data.lt_end_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_end_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_end_date).Day.ToString();

                        data.lt_end_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_end_date).Year, mnth, day);

                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", "MO"),
                    new SqlParameter("@lt_no", data.lt_no),
                    new SqlParameter("@lt_number", data.em_number),
                    new SqlParameter("@lt_leave_code", data.leave_code),
                    new SqlParameter("@lt_days", decimal.Parse(data.lt_days).ToString()),
                    new SqlParameter("@lt_start_date",(data.lt_start_date.ToString())),
                    new SqlParameter("@lt_end_date", data.lt_end_date.ToString()),
                    new SqlParameter("@lt_remarks", data.lt_remarks),
                    new SqlParameter("@lt_end_date_new", db.DBYYYYMMDDformat(data.lt_end_date_new))//Need to write update code in above proc in OPR - MO
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            returnval = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, returnval);

        }

        [Route("AssignHolidaysforEmployees")]
        public HttpResponseMessage AssignHolidaysforEmployees(List<Pers300_le> pay)
        {
            bool insrted = false;
            foreach (Pers300_le obj in pay)
            {
                //int k = 0;
                //for (k = 0; k < int.Parse(obj.ndays); k++)
                //{
                //DateTime dt = DateTime.Parse(obj.lstart_date.ToString());
                DateTime dt_insert = DateTime.Parse(obj.holiday);
                empAttendance eatt = new empAttendance();

                string mnth = string.Empty;
                string day = string.Empty;

                if (dt_insert.Month.ToString().Length == 1)
                    mnth = "0" + dt_insert.Month.ToString();
                else
                    mnth = dt_insert.Month.ToString();

                if (dt_insert.Day.ToString().Length == 1)
                    day = "0" + dt_insert.Day.ToString();
                else
                    day = dt_insert.Day.ToString();

                eatt.emp_id = obj.em_login_code;
                eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, mnth, day);
                eatt.att_flag = "P";
                eatt.comment = String.Empty;
                eatt.s1in = String.Empty;
                eatt.s1out = String.Empty;
                eatt.s2in = String.Empty;
                eatt.s2out = String.Empty;
                bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                // }
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@Opr", 'X'),
                    new SqlParameter("@lt_number", obj.lt_no),
                    new SqlParameter("@lt_company_code", obj.comp_code),
                    new SqlParameter("@lt_leave_code", obj.leave_code),
                    new SqlParameter("@lt_days", obj.lt_days),
                    new SqlParameter("@lt_hours", obj.lt_hours),
                    new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lstart_date)),
                    new SqlParameter("@lt_end_date",db.DBYYYYMMDDformat(obj.lt_end_date)),
                    new SqlParameter("@lt_modified_on",  db.DBYYYYMMDDformat(DateTime.Now.Date.ToString())),
                    new SqlParameter("@lt_mgrctag", 'C'),
                    new SqlParameter("@lt_request_date",  db.DBYYYYMMDDformat(DateTime.Now.Date.ToString()))
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    insrted = false;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);

        }

        [Route("AssignHolidaysforEmployees_1")]
        public HttpResponseMessage AssignHolidaysforEmployees_1(List<Pers300_le> pay)
        {
            bool insrted = true;
            try {
                foreach (Pers300_le obj in pay)
                {
                    //int k = 0;
                    //for (k = 0; k < int.Parse(obj.ndays); k++)
                    //{
                    //DateTime dt = DateTime.Parse(obj.lstart_date.ToString());
                    DateTime dt_insert = DateTime.Parse(obj.holiday);
                    empAttendance eatt = new empAttendance();

                    string mnth = string.Empty;
                    string day = string.Empty;

                    if (dt_insert.Month.ToString().Length == 1)
                        mnth = "0" + dt_insert.Month.ToString();
                    else
                        mnth = dt_insert.Month.ToString();

                    if (dt_insert.Day.ToString().Length == 1)
                        day = "0" + dt_insert.Day.ToString();
                    else
                        day = dt_insert.Day.ToString();

                    eatt.emp_id = obj.em_login_code;
                    eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, mnth, day);
                    eatt.att_flag = "P";
                    eatt.comment = String.Empty;
                    eatt.s1in = String.Empty;
                    eatt.s1out = String.Empty;
                    eatt.s2in = String.Empty;
                    eatt.s2out = String.Empty;
                    bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                    // }
                    // try
                    //{
                    //using (DBConnection db = new DBConnection())
                    //{
                    //db.Open();
                    //SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                    //new List<SqlParameter>() 
                    //{ 
                    //new SqlParameter("@Opr", 'X'),
                    //new SqlParameter("@lt_number", obj.lt_no),
                    //new SqlParameter("@lt_company_code", obj.comp_code),
                    //new SqlParameter("@lt_leave_code", obj.leave_code),
                    //new SqlParameter("@lt_days", obj.lt_days),
                    //new SqlParameter("@lt_hours", obj.lt_hours),
                    //new SqlParameter("@lt_start_date", db.UIDDMMYYYYformat(obj.lstart_date)),
                    //new SqlParameter("@lt_end_date",db.UIDDMMYYYYformat(obj.lt_end_date)),
                    //new SqlParameter("@lt_modified_on",  db.UIDDMMYYYYformat(DateTime.Now.Date.ToString())),
                    //new SqlParameter("@lt_mgrctag", 'C'),
                    //new SqlParameter("@lt_request_date", db.UIDDMMYYYYformat(DateTime.Now.Date.ToString()))
                    //});
                    //if (dr.RecordsAffected > 0)
                    //{
                    //insrted = true;
                    //}
                    //}
                    //}
                    //catch (Exception e)
                    //{
                    //insrted = false;
                    //}
                }
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK,ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);

        }

        [Route("GetEmployeeData")]
        public HttpResponseMessage GetEmployeeData()
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A2"),             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrms = new Pers300_le();

                            hrms.em_name = dr["em_name"].ToString();
                            hrms.em_number1 = dr["em_number"].ToString();
                            
                            hrmsobj.Add(hrms);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }

        [Route("GetFinalizeSatus")]
        public HttpResponseMessage GetFinalizeSatus(string emp_id,string leave_start_date,string leave_end_date)
        {
            List<Pers300_le> hrmsobj = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "PS"),
                            new SqlParameter("@em_login_code", emp_id),
                            new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(leave_start_date)),
                            new SqlParameter("@lt_end_date", db.DBYYYYMMDDformat(leave_end_date))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrms = new Pers300_le();                    
                            hrms.pe_status = dr["pe_status"].ToString();                          
                            hrmsobj.Add(hrms);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK,e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

        }


        //lwp history details
        [Route("GetAllEMPLeaveHistoryDetails")]
        public HttpResponseMessage GetAllEMPLeaveHistoryDetails(string comp_code, string emp_id)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            //
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", "Q1"),
                            new SqlParameter("@lt_company_code", comp_code),
                            new SqlParameter("@lt_number", emp_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["Emp_Name"].ToString();
                            hrmsobj.em_login_code = dr["lt_number"].ToString();
                            DateTime stdate = DateTime.Parse(dr["lt_start_date"].ToString());
                            string st = stdate.Year.ToString() + "-" + stdate.Month.ToString() + "-" + stdate.Day.ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            DateTime enddate = DateTime.Parse(dr["lt_end_date"].ToString());
                            string ed = enddate.Year.ToString() + "-" + enddate.Month.ToString() + "-" + enddate.Day.ToString();
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.leave_type = dr["cl_desc"].ToString();
                            hrmsobj.maxLeaves = dr["el_maximum_days1"].ToString();
                            hrmsobj.balanceLeaves = dr["ballance_leave"].ToString();
                            hrmsobj.lt_leave_code = dr["lt_leave_code"].ToString();
                            try
                            {
                               hrmsobj.lt_no = dr["lt_no"].ToString();
                               hrmsobj.em_doc_path = dr["em_doc_path"].ToString();
                            }
                            catch(Exception e) { };

                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }

        [Route("CUDInsertEMPLeaveDetails")]
        public HttpResponseMessage CUDInsertEMPLeaveDetails(List<Pers300_le> pay)
        {
            DBConnection db1 = new DBConnection();
            bool insrted = false;
            string enddate = "";
            foreach (Pers300_le obj in pay)
            {
                int k = 0;
                for (k = 0; k < int.Parse(obj.ndays); k++)
                {
                    DateTime dt = DateTime.Parse(db1.DBYYYYMMDDformat(obj.lstart_date.ToString()));
                    DateTime dt_insert = dt.AddDays(k);
                    empAttendance eatt = new empAttendance();

                    string mnth = string.Empty;
                    string day = string.Empty;

                    if (dt_insert.Month.ToString().Length == 1)
                        mnth = "0" + dt_insert.Month.ToString();
                    else
                        mnth = dt_insert.Month.ToString();

                    if (dt_insert.Day.ToString().Length == 1)
                        day = "0" + dt_insert.Day.ToString();
                    else
                        day = dt_insert.Day.ToString();

                    eatt.emp_id = obj.em_login_code;
                    eatt.att_date = string.Format("{0}-{1}-{2}", dt_insert.Year, mnth, day);
                    enddate = eatt.att_date;
                    eatt.att_flag = "LP";
                    eatt.comment = String.Empty;
                    eatt.s1in = String.Empty;
                    eatt.s1out = String.Empty;
                    eatt.s2in = String.Empty;
                    eatt.s2out = String.Empty;
                   // bool r = MarkEmpDayAttedance_OnLeaveTransaction(eatt);
                }
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                         SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                           new List<SqlParameter>() 
                         { 
                           new SqlParameter("@Opr", "C1"),
                           new SqlParameter("@lt_number", obj.em_login_code),
                           new SqlParameter("@lt_company_code", obj.comp_code),
                           new SqlParameter("@lt_leave_code", "LWP"),
                           new SqlParameter("@lt_days", obj.ndays),
                           new SqlParameter("@lt_hours", obj.lt_hours),
                           new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lstart_date)),
                           new SqlParameter("@lt_end_date",enddate),
                           new SqlParameter("@lt_mgrctag", 'C'),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insrted = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    insrted = false;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("GetAllEMPLeaveLWPDetails")]
        public HttpResponseMessage GetAllEMPLeaveLWPDetails(string comp_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            //
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@Opr", "Q2"),
                            new SqlParameter("@lt_company_code", comp_code),
                            //new SqlParameter("@lt_number",emp_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            DateTime stdate = DateTime.Parse(dr["lt_start_date"].ToString());
                            string st = stdate.Year.ToString() + "-" + stdate.Month.ToString() + "-" + stdate.Day.ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            DateTime enddate = DateTime.Parse(dr["lt_end_date"].ToString());
                            string ed = enddate.Year.ToString() + "-" + enddate.Month.ToString() + "-" + enddate.Day.ToString();
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = dr["lt_modified_on"].ToString();
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            string rq = string.Empty;
                            if (!string.IsNullOrEmpty(dr["lt_request_date"].ToString()))
                            {
                                DateTime rqdate = DateTime.Parse(dr["lt_request_date"].ToString());
                                rq = rqdate.Year.ToString() + "-" + rqdate.Month.ToString() + "-" + rqdate.Day.ToString();
                            }
                            hrmsobj.requestdate = db.UIDDMMYYYYformat(dr["lt_request_date"].ToString());
                            hrmsobj.updated = false;
                            hrmsobj.lt_admin_remark = string.Empty;
                            hrmsobj.lt_admin_cancel_remark = string.Empty;
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }


        [Route("GetAllEMPLeaveDetailsForReschedule")]
        public HttpResponseMessage GetAllEMPLeaveDetailsForReschedule(string comp_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            //
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@Opr", "Q3"),
                        new SqlParameter("@lt_company_code", comp_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.EmpName = dr["EmpName"].ToString();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.leave_code = dr["lt_leave_code"].ToString();
                            hrmsobj.comp_name = dr["Company"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            DateTime stdate = DateTime.Parse(dr["lt_start_date"].ToString());
                            string st = stdate.Year.ToString() + "-" + stdate.Month.ToString() + "-" + stdate.Day.ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            DateTime enddate = DateTime.Parse(dr["lt_end_date"].ToString());
                            string ed = enddate.Year.ToString() + "-" + enddate.Month.ToString() + "-" + enddate.Day.ToString();
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = dr["lt_modified_on"].ToString();
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            string rq = string.Empty;
                            if (!string.IsNullOrEmpty(dr["lt_request_date"].ToString()))
                            {
                                DateTime rqdate = DateTime.Parse(dr["lt_request_date"].ToString());
                                rq = rqdate.Year.ToString() + "-" + rqdate.Month.ToString() + "-" + rqdate.Day.ToString();
                            }
                            hrmsobj.requestdate = db.UIDDMMYYYYformat(dr["lt_request_date"].ToString());                            
                            hrmsobj.lt_no = dr["lt_no"].ToString();
                            hrmsobj.updated = false;
                            hrmsobj.lt_admin_remark = dr["lt_admin_remark"].ToString();  //string.Empty;
                            hrmsobj.lt_admin_cancel_remark = string.Empty;
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

        }

        [Route("RescheduleLeaves")]
        public HttpResponseMessage RescheduleLeaves(List<Pers300_le> coll)
        {
            bool returnval = false;
            foreach (Pers300_le data in coll)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        data.lt_start_date = db.DBYYYYMMDDformat(data.lt_start_date);
                        data.lt_end_date = db.DBYYYYMMDDformat(data.lt_end_date);

                        string mnth = string.Empty;
                        string day = string.Empty;

                        if (DateTime.Parse(data.lt_start_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_start_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_start_date).Month.ToString();

                        if (DateTime.Parse(data.lt_start_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_start_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_start_date).Day.ToString();

                        data.lt_start_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_start_date).Year, mnth, day);

                        if (DateTime.Parse(data.lt_end_date).Month.ToString().Length == 1)
                            mnth = "0" + DateTime.Parse(data.lt_end_date).Month.ToString();
                        else
                            mnth = DateTime.Parse(data.lt_end_date).Month.ToString();

                        if (DateTime.Parse(data.lt_end_date).Day.ToString().Length == 1)
                            day = "0" + DateTime.Parse(data.lt_end_date).Day.ToString();
                        else
                            day = DateTime.Parse(data.lt_end_date).Day.ToString();

                        data.lt_end_date = string.Format("{0}-{1}-{2}", DateTime.Parse(data.lt_end_date).Year, mnth, day);

                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_leave_details_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "SO"),
                              new SqlParameter("@lt_no", data.lt_no),
                              new SqlParameter("@lt_number", data.em_login_code),
                              new SqlParameter("@lt_leave_code", data.leave_code),
                              new SqlParameter("@lt_days", decimal.Parse(data.lt_days).ToString()),
                              new SqlParameter("@lt_start_date",(data.lt_start_date.ToString())),
                              new SqlParameter("@lt_end_date", data.lt_end_date.ToString()),
                              new SqlParameter("@lt_remarks", data.lt_remarks),
                              new SqlParameter("@lt_admin_remark", data.lt_admin_remark),
                              new SqlParameter("@lt_mgr_confirm_tag", data.lt_mgr_confirm_tag),
                              new SqlParameter("@lt_start_date_new", db.DBYYYYMMDDformat(data.lt_start_date_new)),
                              new SqlParameter("@lt_end_date_new", db.DBYYYYMMDDformat(data.lt_end_date_new))//Need to write update code in above proc in OPR - MO
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            returnval = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, returnval);

        }

        #endregion
    }
}