﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/UpdateLeaveBal")]
    public class UpdateLeaveBalController:ApiController
    {



        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<Per314> mod_list = new List<Per314>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "M")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per314 hrmsObj = new Per314();
                            hrmsObj.dep_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dep_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<Per314> mod_list = new List<Per314>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per314 hrmsObj = new Per314();
                            hrmsObj.dg_code = dr["dg_code"].ToString();
                            hrmsObj.dg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Getleaves")]
        public HttpResponseMessage Getleaves()
        {
            List<Per314> mod_list = new List<Per314>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_update_employee_leave_balance]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per314 simsobj = new Per314();
                            simsobj.leave_code = dr["cl_code"].ToString();
                            simsobj.leave_name = dr["cl_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("LeaveDetails")]
        public HttpResponseMessage LeaveDetails(string dept, string desg, string l_type, string empcode, string gr_code,string company_code)
        {

             if (dept == "undefined")
                    dept = null;
             if (desg == "undefined")
                    desg = null;
             if (l_type == "undefined")
                    l_type = null;
             if (empcode == "undefined")
                    empcode = null;
            if (gr_code == "undefined")
                gr_code = null;
            if (company_code == "undefined")
                company_code = null;
            List<Per314> mod_list = new List<Per314>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_update_employee_leave_balance]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@depcode", dept),
                            new SqlParameter("@desgcode", desg),
                            new SqlParameter("@leave_code", l_type),
                            new SqlParameter("@emp_code", empcode),
                            //new SqlParameter("@gradecode", gr_code),
                            new SqlParameter("@company_code", company_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per314 simsobj = new Per314();
                            simsobj.emp_code = dr["el_number"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.leave_code = dr["el_leave_code"].ToString();
                            simsobj.leave_name = dr["cl_desc"].ToString();
                            simsobj.leave_taken = dr["el_days_taken"].ToString();
                            simsobj.leave_bal = dr["el_leave_bal"].ToString();
                            simsobj.leave_max = dr["el_maximum_days"].ToString();
                            simsobj.leave_max_old = dr["el_maximum_days"].ToString();
                            mod_list.Add(simsobj);

                        }
                    }
                  
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

         [Route("EmpUpdateLeaveBal")]
        public HttpResponseMessage EmpUpdateLeaveBal(List<Per314> lst)
        {
            bool Inserted =false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    
                    foreach (Per314 obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_update_employee_leave_balance]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'U'),
                                new SqlParameter("@emp_code", obj.emp_code),
                                new SqlParameter("@leave_code", obj.leave_code),
                                new SqlParameter("@max_days", obj.leave_max),
                                new SqlParameter("@user_name", obj.user_name)
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
              
            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

    }
}