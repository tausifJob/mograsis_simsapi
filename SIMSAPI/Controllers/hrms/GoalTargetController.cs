﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.GoalTargetController
{
    [RoutePrefix("api/GoalTarget")]
    [BasicAuthentication]
    public class GoalTargetController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllGoalTarget")]
        public HttpResponseMessage getAllGoalTarget()
        {
            List<Ucw242> goaltarget_list = new List<Ucw242>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw242 simsobj = new Ucw242();
                            simsobj.sims_sip_academic_year = dr["sims_sip_academic_year"].ToString();
                            simsobj.sims_sip_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_sip_goal_desc = dr["sims_sip_goal_desc"].ToString();
                            simsobj.sims_sip_goal_target_code = dr["sims_sip_goal_target_code"].ToString();
                            simsobj.sims_sip_goal_target_desc = dr["sims_sip_goal_target_desc"].ToString();
                            simsobj.sims_sip_goal_target_start_date =db.UIDDMMYYYYformat(dr["sims_sip_goal_target_start_date"].ToString());
                            simsobj.sims_sip_goal_target_end_date = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_end_date"].ToString());
                            simsobj.sims_sip_goal_target_min_point = dr["sims_sip_goal_target_min_point"].ToString();
                            simsobj.sims_sip_goal_target_max_point = dr["sims_sip_goal_target_max_point"].ToString();
                            simsobj.sims_sip_goal_target_status = dr["sims_sip_goal_target_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear() 
        {
            List<Ucw242> lstModules = new List<Ucw242>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "X"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw242 sequence = new Ucw242();
                            sequence.sims_sip_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_sip_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getGoalName")]
        public HttpResponseMessage getGoalName()
        {
            List<Ucw242> goal_list = new List<Ucw242>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw242 simsobj = new Ucw242();
                            simsobj.sims_sip_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_sip_goal_desc = dr["sims_sip_goal_desc"].ToString(); ;
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAutoGenerateCode")]
        public HttpResponseMessage getAutoGenerateCode()
        {
            List<Ucw242> srno_list = new List<Ucw242>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw242 simsobj = new Ucw242();
                            simsobj.sims_sip_goal_target_code = dr["Target_code"].ToString();
                            srno_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDGoalTarget")]
        public HttpResponseMessage CUDGoalTarget(List<Ucw242> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Ucw242 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_goal_target_proc]",
                            new List<SqlParameter>() 
                         { 
		 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_academic_year", simsobj.sims_sip_academic_year),
                                new SqlParameter("@sims_sip_goal_code", simsobj.sims_sip_goal_code),
                                new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_sip_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_desc", simsobj.sims_sip_goal_target_desc),
                                new SqlParameter("@sims_sip_goal_target_start_date",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_start_date)),
                                new SqlParameter("@sims_sip_goal_target_end_date",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_end_date)),
                                new SqlParameter("@sims_sip_goal_target_min_point",simsobj.sims_sip_goal_target_min_point),
                                new SqlParameter("@sims_sip_goal_target_max_point",simsobj.sims_sip_goal_target_max_point),
                                new SqlParameter("@sims_sip_goal_target_status",simsobj.sims_sip_goal_target_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}




