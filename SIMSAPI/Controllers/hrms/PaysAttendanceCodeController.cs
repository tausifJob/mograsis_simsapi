﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/PaysAttendanceCode")]
    [BasicAuthentication]
    public class PaysAttendanceCodeController : ApiController
    {
    private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("Getall_attendance_code")]
        public HttpResponseMessage Getall_attendance_code()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getall_attendance_code(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Getall_attendance_code"));

            List<Pers315> pay_list = new List<Pers315>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_attendance_code]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    while (dr.Read())
                    {
                        Pers315 pay = new Pers315();
                        pay.pays_attendance_code = dr["pays_attendance_code"].ToString();
                        pay.pays_attendance_code_numeric = int.Parse(dr["pays_attendance_code_numeric"].ToString());
                        pay.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                        pay.pays_attendance_description = dr["pays_attendance_description"].ToString();
                        pay.pays_attendance_type_value = dr["pays_attendance_type_value"].ToString();
                        pay.pays_attendance_color_code = dr["pays_attendance_color_code"].ToString();
                        pay.pays_attendance_status = dr["pays_attendance_status"].Equals("Y") ? true : false;

                       // pay.pays_attendance_status_inactive_date = DateTime.Parse(dr["pays_attendance_status_inactive_date"].ToString()).ToShortDateString();

                        pay_list.Add(pay);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, pay_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, pay_list);
            }
        }

        [Route("CUDPaysAttendanceCode")]
        public HttpResponseMessage CUDPaysAttendanceCode(List<Pers315> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers315 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_attendance_code]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@pays_attendance_code", obj.pays_attendance_code),
                                new SqlParameter("@pays_attendance_code_numeric", obj.pays_attendance_code_numeric),                                
                                new SqlParameter("@pays_attendance_short_desc", obj.pays_attendance_short_desc),
                                new SqlParameter("@pays_attendance_description", obj.pays_attendance_description),
                                new SqlParameter("@pays_attendance_type_value", obj.pays_attendance_type_value),
                                new SqlParameter("@pays_attendance_color_code", obj.pays_attendance_color_code),
                                new SqlParameter("@pays_attendance_status", obj.pays_attendance_status==true?"Y":"N")
                     });
                        if (ins > 0)
                        {
                            if (obj.opr.Equals("U"))
                                message.strMessage = "Record Updated Sucessfully!!";
                            else if (obj.opr.Equals("I"))
                                message.strMessage = "Record Added Sucessfully!!";
                            else if (obj.opr.Equals("D"))
                                message.strMessage = "Record Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        else
                        {
                            // message.strMessage = "";
                            message.strMessage = "Attendance Code is already present";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }

            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
    }
}