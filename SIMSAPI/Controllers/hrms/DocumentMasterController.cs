﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.DocumentMasterController
{
    [RoutePrefix("api/EmpDocumentUp")]
    [BasicAuthentication]
    public class DesignationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getEmpDocumentDetail")]
        public HttpResponseMessage getEmpDocumentDetail()
        {
            List<Sims020> empDocDetail = new List<Sims020>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_master_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims020 objone = new Sims020();

                            objone.pays_doc_mod_code = dr["pays_doc_mod_code"].ToString();
                            objone.pays_doc_code = dr["pays_doc_code"].ToString();
                            objone.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            //objone.pays_doc_path = dr["pays_doc_path"].ToString();
                            objone.pays_doc_create_date = db.UIDDMMYYYYformat(dr["pays_doc_create_date"].ToString());
                            objone.pays_doc_created_by = dr["pays_doc_created_by"].ToString();
                            if (dr["pays_doc_status"].ToString() == "A")
                                objone.pays_doc_status = true;
                            else
                                objone.pays_doc_status = false;

                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("CUDgetAllRecords")]
        public HttpResponseMessage CUDgetAllRecordsNew(List<Sims020> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims020 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_doc_master_proc]",
                        new List<SqlParameter>()
                                    {
                                             new SqlParameter("@opr", simsobj.opr),
                                             new SqlParameter("@pays_doc_code", simsobj.pays_doc_code),
                                             new SqlParameter("@pays_doc_desc", simsobj.pays_doc_desc),
                                             new SqlParameter("@pays_doc_created_by",simsobj.pays_doc_created_by)
                                             
                                    });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}
