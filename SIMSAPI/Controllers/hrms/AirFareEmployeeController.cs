﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Controllers;
using SIMSAPI.Models.SIMS.simsClass;



namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/AirFareEmp")]
    public class AirFareEmployeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllAirFareEmployee")]
        public HttpResponseMessage getAllAirFareEmployee()
        {


            List<PerAFEmp> airfare_list = new List<PerAFEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAFEmp invsobj = new PerAFEmp();
                            invsobj.afe_em_number = dr["afe_em_number"].ToString();
                            invsobj.afe_grade_code = dr["afe_grade_code"].ToString();
                            invsobj.gr_desc = dr["gr_desc"].ToString();
                            invsobj.afe_dest_code = dr["afe_dest_code"].ToString();
                            invsobj.ds_name = dr["ds_name"].ToString();
                            invsobj.afe_nation_code = dr["afe_nation_code"].ToString();
                            invsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            invsobj.afe_class = dr["afe_class"].ToString();
                            invsobj.afe_class_name = dr["pays_appl_form_field_value1"].ToString();
                            invsobj.afe_applicable_frequency = dr["afe_applicable_frequency"].ToString();
                            invsobj.afe_applicable_month = dr["afe_applicable_month"].ToString();
                            invsobj.afe_adult_rate = dr["afe_adult_rate"].ToString();
                            invsobj.afe_child_rate = dr["afe_child_rate"].ToString();
                            invsobj.afe_infant_rate = dr["afe_infant_rate"].ToString();
                            invsobj.afe_finalize_status = dr["afe_finalize_status"].Equals("Y") ? true : false;
                            try
                            {
                                invsobj.afe_reimbursement = dr["afe_reimbursement"].ToString();
                                invsobj.afe_airport = dr["afe_airport"].ToString();
                                invsobj.afe_travelling_date = db.UIDDMMYYYYformat(dr["afe_travelling_date"].ToString());
                                invsobj.afe_returing_date = db.UIDDMMYYYYformat(dr["afe_returing_date"].ToString());
                                invsobj.afe_created_by = dr["afe_created_by"].ToString();
                                invsobj.afe_creation_date = db.UIDDMMYYYYformat(dr["afe_creation_date"].ToString());
                                invsobj.gr_code = dr["gr_code"].ToString();
                                invsobj.afe_id = dr["afe_id"].ToString();
                                invsobj.afe_parent_id = dr["afe_parent_id"].ToString();
                                invsobj.afe_amount_ticket = dr["afe_amount_ticket"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }

                            airfare_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, airfare_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
        }


        [Route("getAirFareEmployee")]
        public HttpResponseMessage getAirFareEmployee(string afe_grade_code, string afe_dest_code, string afe_nation_code, string afe_class, string afe_em_number)
        {

            if (afe_grade_code == "undefined")
            { afe_grade_code = null; }
            if (afe_dest_code == "undefined")
            { afe_dest_code = null; }
            if (afe_nation_code == "undefined")
            { afe_nation_code = null; }
            if (afe_class == "undefined")
            { afe_class = null; }
            if (afe_em_number == "undefined")
            { afe_em_number = null; }

            List<PerAFEmp> airfare_list = new List<PerAFEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "J"),
                                new SqlParameter("@afe_grade_code",afe_grade_code),
                                new SqlParameter("@afe_dest_code",afe_dest_code),
                                new SqlParameter("@afe_nation_code",afe_nation_code),
                                new SqlParameter("@afe_class",afe_class),
                                  new SqlParameter("@afe_em_number",afe_em_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAFEmp invsobj = new PerAFEmp();
                            invsobj.afe_em_number = dr["afe_em_number"].ToString();
                            invsobj.afe_grade_code = dr["afe_grade_code"].ToString();
                            invsobj.gr_desc = dr["gr_desc"].ToString();
                            invsobj.afe_dest_code = dr["afe_dest_code"].ToString();
                            invsobj.ds_name = dr["ds_name"].ToString();
                            invsobj.afe_nation_code = dr["afe_nation_code"].ToString();
                            invsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            invsobj.afe_class = dr["afe_class"].ToString();
                            invsobj.afe_class_name = dr["pays_appl_form_field_value1"].ToString();
                            invsobj.afe_applicable_frequency = dr["afe_applicable_frequency"].ToString();
                            invsobj.afe_applicable_month = dr["afe_applicable_month"].ToString();
                            invsobj.afe_adult_rate = dr["afe_adult_rate"].ToString();
                            invsobj.afe_child_rate = dr["afe_child_rate"].ToString();
                            invsobj.afe_infant_rate = dr["afe_infant_rate"].ToString();
                            invsobj.afe_finalize_status = dr["afe_finalize_status"].Equals("Y") ? true : false;
                            try
                            {
                                invsobj.afe_reimbursement = dr["afe_reimbursement"].ToString();
                                invsobj.afe_airport = dr["afe_airport"].ToString();
                                invsobj.afe_travelling_date = db.UIDDMMYYYYformat(dr["afe_travelling_date"].ToString());
                                invsobj.afe_returing_date = db.UIDDMMYYYYformat(dr["afe_returing_date"].ToString());
                                invsobj.afe_created_by = dr["afe_created_by"].ToString();
                                invsobj.afe_creation_date = db.UIDDMMYYYYformat(dr["afe_creation_date"].ToString());
                                invsobj.gr_code = dr["gr_code"].ToString();
                                invsobj.afe_id = dr["afe_id"].ToString();
                                invsobj.afe_parent_id = dr["afe_parent_id"].ToString();
                                invsobj.afe_amount_ticket = dr["afe_amount_ticket"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            airfare_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, airfare_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
        }
        
        [Route("getDestinationAirFare")]
        public HttpResponseMessage getDestinationAirFare()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDestinationAirFare(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getDestinationAirFare"));

            List<PerAF> fare_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "A"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF obj = new PerAF();
                            obj.af_dest_code = dr["ds_code"].ToString();
                            obj.ds_name = dr["ds_name"].ToString();
                            fare_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fare_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fare_list);
        }

        [Route("getNations")]
        public HttpResponseMessage getNations()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getNations(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getNations"));

            List<PerAF> nation_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),    
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF obj = new PerAF();
                            obj.af_nation_code = dr["sims_country_code"].ToString();
                            obj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            nation_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, nation_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, nation_list);
        }

        [Route("getGrades")]
        public HttpResponseMessage getGrades()
        {

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "E"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.gr_code = dr["gr_code"].ToString();
                            persobj.gr_desc = dr["gr_desc"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, class_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("getClass")]
        public HttpResponseMessage getClass()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getClass(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getClass"));

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "B"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.af_class = dr["pays_appl_parameter"].ToString();
                            persobj.pays_appl_form_value1 = dr["pays_appl_form_field_value1"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, class_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("CUAirFareEmp")]
        public HttpResponseMessage CUDAirFareEmp(List<PerAFEmp> data)
        {

            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (PerAFEmp persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_air_fare_employee_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", persobj.opr),
                                    new SqlParameter("@afe_em_number",persobj.afe_em_number),
                                    new SqlParameter("@afe_grade_code", persobj.afe_grade_code),
                                    new SqlParameter("@afe_dest_code", persobj.afe_dest_code),
                                    new SqlParameter("@afe_nation_code", persobj.afe_nation_code),
                                    new SqlParameter("@afe_class", persobj.afe_class),
                                    new SqlParameter("@afe_adult_rate",persobj.afe_adult_rate != ""?persobj.afe_adult_rate:"0"),
                                    new SqlParameter("@afe_child_rate",persobj.afe_child_rate != ""?persobj.afe_child_rate:"0"),
                                    new SqlParameter("@afe_infant_rate",persobj.afe_infant_rate != ""?persobj.afe_infant_rate:"0"),
                                    new SqlParameter("@afe_finalize_status", persobj.afe_finalize_status.Equals(true)?"Y":"N"),
                                    
                                    new SqlParameter("@afe_airport",persobj.afe_airport),
                                    new SqlParameter("@afe_travelling_date",db.DBYYYYMMDDformat(persobj.afe_travelling_date)),
                                    new SqlParameter("@afe_returing_date",db.DBYYYYMMDDformat(persobj.afe_returing_date)),
                                    new SqlParameter("@afe_created_by",persobj.afe_created_by),
                                    new SqlParameter("@afe_applicable_frequency",persobj.afe_applicable_frequency),
                                    new SqlParameter("@afe_old_frequency",persobj.afe_old_frequency),
                                    new SqlParameter("@afe_applicable_month",persobj.afe_applicable_month),
                                    new SqlParameter("@afe_reimbursement",persobj.afe_reimbursement),
                                    new SqlParameter("@afe_id",persobj.afe_id)
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUAirFareEmpdetails")]
        public HttpResponseMessage CUAirFareEmpdetails(List<PerAFEmp> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (PerAFEmp persobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", persobj.opr),
                                    new SqlParameter("@afe_em_number",persobj.afe_em_number),
                                    new SqlParameter("@afe_grade_code", persobj.afe_grade_code),
                                    new SqlParameter("@afe_dest_code", persobj.afe_dest_code),
                                    new SqlParameter("@afe_nation_code", persobj.afe_nation_code),
                                    new SqlParameter("@afe_class", persobj.afe_class),
                                    new SqlParameter("@afe_adult_rate",persobj.afe_adult_rate != ""?persobj.afe_adult_rate:"0"),
                                    new SqlParameter("@afe_child_rate",persobj.afe_child_rate != ""?persobj.afe_child_rate:"0"),
                                    new SqlParameter("@afe_infant_rate",persobj.afe_infant_rate != ""?persobj.afe_infant_rate:"0"),
                                    new SqlParameter("@afe_finalize_status", persobj.afe_finalize_status.Equals(true)?"Y":"N"),
                                    new SqlParameter("@afe_airport",persobj.afe_airport),
                                    new SqlParameter("@afe_travelling_date",db.DBYYYYMMDDformat(persobj.afe_travelling_date)),
                                    new SqlParameter("@afe_returing_date",db.DBYYYYMMDDformat(persobj.afe_returing_date)),
                                    new SqlParameter("@afe_created_by",persobj.afe_created_by),
                                    new SqlParameter("@afe_applicable_frequency",persobj.afe_applicable_frequency),
                                    new SqlParameter("@afe_old_frequency",persobj.afe_old_frequency),
                                    new SqlParameter("@afe_applicable_month",persobj.afe_applicable_month),
                                    new SqlParameter("@afe_reimbursement",persobj.afe_reimbursement)
                                    
                                    //new SqlParameter("@afe_creation_date",persobj.afe_creation_date)
                            });
                            while (dr.Read())
                            {
                                string cnt = dr["airfare_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //[Route("CDAirFareEmp")]
        //public HttpResponseMessage CUDAirFareEmp(List<PerAFEmp> data)
        //{

        //    Message message = new Message();
        //    bool inserted = false;
        //    try
        //    {
        //        if (data != null)
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                db.Open();
        //                foreach (PerAFEmp persobj in data)
        //                {
        //                    int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_air_fare_employee_proc]",
        //                        new List<SqlParameter>() 
        //                     { 
        //                            new SqlParameter("@opr", persobj.opr),
        //                            new SqlParameter("@afe_em_number",persobj.afe_em_number),
        //                            new SqlParameter("@afe_grade_code", persobj.afe_grade_code),
        //                            new SqlParameter("@afe_dest_code", persobj.afe_dest_code),
        //                            new SqlParameter("@afe_nation_code", persobj.afe_nation_code),
        //                            new SqlParameter("@afe_class", persobj.afe_class),
        //                            new SqlParameter("@afe_finalize_status", persobj.afe_finalize_status.Equals(true)?"Y":"N"),
        //                    });
        //                    if (ins > 0)
        //                    {
        //                        inserted = true;
        //                    }
        //                    else
        //                    {
        //                        inserted = false;
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //        }

        //    }
        //    catch (Exception x)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}

        [Route("getairfaredetails")]
        public HttpResponseMessage getairfaredetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getClass(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getClass"));

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "K"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.gr_code = dr["gr_code"].ToString();
                            persobj.gr_desc = dr["class_nm"].ToString();
                            persobj.af_applicable_frequency = dr["af_applicable_frequency"].ToString();
                            persobj.af_adult_rate = dr["af_adult_rate"].ToString();
                            persobj.af_child_rate = dr["af_child_rate"].ToString();
                            persobj.af_infant_rate = dr["af_infant_rate"].ToString();
                            persobj.af_default_applicable_month = dr["af_default_applicable_month"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, class_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("getAirFareEmployeeDetails")]
        public HttpResponseMessage getAirFareEmployeeDetails(string company_code, string dept_code, string grade_code, string staff_type_code, string dg_code, string afe_em_number)
        {

            if (company_code == "undefined")
            { company_code = null; }
            if (dept_code == "undefined")
            { dept_code = null; }
            if (grade_code == "undefined")
            { grade_code = null; }
            if (staff_type_code == "undefined")
            { staff_type_code = null; }
            if (dg_code == "undefined")
            { dg_code = null; }
            if (afe_em_number == "undefined")
            { afe_em_number = null; }

            List<PerAFEmp> airfare_list = new List<PerAFEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "L"),
                                new SqlParameter("@company_code",company_code),
                                new SqlParameter("@dept_code",dept_code),
                                new SqlParameter("@grade_code",grade_code),
                                new SqlParameter("@staff_type_code",staff_type_code),
                                new SqlParameter("@dg_code",dg_code),
                                new SqlParameter("@afe_em_number",afe_em_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAFEmp invsobj = new PerAFEmp();
                            invsobj.afe_em_number = dr["afe_em_number"].ToString();
                            invsobj.afe_grade_code = dr["afe_grade_code"].ToString();
                            invsobj.gr_desc = dr["gr_desc"].ToString();
                            invsobj.afe_dest_code = dr["afe_dest_code"].ToString();
                            invsobj.ds_name = dr["ds_name"].ToString();
                            invsobj.afe_nation_code = dr["afe_nation_code"].ToString();
                            invsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            invsobj.afe_class = dr["afe_class"].ToString();
                            invsobj.afe_class_name = dr["pays_appl_form_field_value1"].ToString();
                            invsobj.afe_applicable_frequency = dr["afe_applicable_frequency"].ToString();
                            invsobj.afe_applicable_month = dr["afe_applicable_month"].ToString();
                            invsobj.afe_adult_rate = dr["afe_adult_rate"].ToString();
                            invsobj.afe_child_rate = dr["afe_child_rate"].ToString();
                            invsobj.afe_infant_rate = dr["afe_infant_rate"].ToString();
                            invsobj.afe_finalize_status = dr["afe_finalize_status"].Equals("Y") ? true : false;
                            try
                            {
                                invsobj.afe_reimbursement = dr["afe_reimbursement"].ToString();
                                invsobj.afe_airport = dr["afe_airport"].ToString();
                                invsobj.afe_travelling_date = db.UIDDMMYYYYformat(dr["afe_travelling_date"].ToString());
                                invsobj.afe_returing_date = db.UIDDMMYYYYformat(dr["afe_returing_date"].ToString());
                                invsobj.afe_created_by = dr["afe_created_by"].ToString();
                                invsobj.afe_creation_date = db.UIDDMMYYYYformat(dr["afe_creation_date"].ToString());
                                invsobj.gr_code = dr["gr_code"].ToString();
                                invsobj.afe_id = dr["afe_id"].ToString();
                                invsobj.afe_parent_id = dr["afe_parent_id"].ToString();
                                invsobj.afe_amount_ticket = dr["afe_amount_ticket"].ToString();
                                invsobj.afe_academic_year = dr["afe_academic_year"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            airfare_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
        }

        [Route("AifareAssignedEmployee")]
        public HttpResponseMessage AifareAssignedEmployee(string data)
        {

            Sims189 obj = new Sims189();

            List<Sims189> Emp_List = new List<Sims189>();

            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims189>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'M'),
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),
                            new SqlParameter("@teacher_name",obj.em_department),
                            new SqlParameter("@em_desg_code",obj.em_desg_code),
                            new SqlParameter("@codp_comp_code",obj.company_code)                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();

                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_department = dr["dept_name"].ToString();
                            simsobj.em_desg_code = dr["desg_name"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();

                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            simsobj.sims_student_enroll_number = dr["emp_child"].ToString();

                            simsobj.afe_grade_code = dr["afe_grade_code"].ToString();
                            simsobj.afe_airport = dr["afe_airport"].ToString();
                            simsobj.afe_applicable_frequency = dr["afe_applicable_frequency"].ToString();
                            simsobj.afe_travelling_date = db.UIDDMMYYYYformat(dr["afe_travelling_date"].ToString());
                            simsobj.afe_returing_date = db.UIDDMMYYYYformat(dr["afe_returing_date"].ToString());
                            simsobj.afe_reimbursement = dr["afe_reimbursement"].ToString();
                            simsobj.afe_nation_code = dr["afe_nation_code"].ToString();
                            simsobj.afe_dest_code = dr["afe_dest_code"].ToString();
                            simsobj.afe_finalize_status = dr["afe_finalize_status"].ToString().Equals("Y") ? true : false;                            
                            simsobj.afe_id = dr["afe_id"].ToString();

                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }


        [Route("CUAirFareEmpTransaction")]
        public HttpResponseMessage CUAirFareEmpTransaction(List<PerAFEmp> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (PerAFEmp persobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_employee_proc]",
                                new List<SqlParameter>()
                             {
                                    new SqlParameter("@opr", persobj.opr),
                                    new SqlParameter("@afe_em_number",persobj.afe_em_number),
                                    new SqlParameter("@afe_grade_code", persobj.afe_grade_code),
                                    new SqlParameter("@afe_dest_code", persobj.afe_dest_code),
                                    new SqlParameter("@afe_nation_code", persobj.afe_nation_code),
                                    new SqlParameter("@afe_class", persobj.afe_class),
                                    new SqlParameter("@afe_adult_rate",persobj.afe_adult_rate != ""?persobj.afe_adult_rate:"0"),
                                    new SqlParameter("@afe_child_rate",persobj.afe_child_rate != ""?persobj.afe_child_rate:"0"),
                                    new SqlParameter("@afe_infant_rate",persobj.afe_infant_rate != ""?persobj.afe_infant_rate:"0"),
                                    new SqlParameter("@afe_finalize_status", persobj.afe_finalize_status.Equals(true)?"Y":"N"),
                                    new SqlParameter("@afe_airport",persobj.afe_airport),
                                    new SqlParameter("@afe_travelling_date",db.DBYYYYMMDDformat(persobj.afe_travelling_date)),
                                    new SqlParameter("@afe_returing_date",db.DBYYYYMMDDformat(persobj.afe_returing_date)),
                                    new SqlParameter("@afe_created_by",persobj.afe_created_by),
                                    new SqlParameter("@afe_applicable_frequency",persobj.afe_applicable_frequency),
                                    new SqlParameter("@afe_old_frequency",persobj.afe_old_frequency),
                                    new SqlParameter("@afe_applicable_month",persobj.afe_applicable_month),
                                    new SqlParameter("@afe_reimbursement",persobj.afe_reimbursement),
                                    new SqlParameter("@afe_amount_ticket",persobj.afe_amount_ticket),
                                    new SqlParameter("@afe_id",persobj.afe_id),
                                    new SqlParameter("@afe_academic_year",persobj.afe_academic_year)
                                    
                                    //new SqlParameter("@afe_creation_date",persobj.afe_creation_date)
                            });

                            if(dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            
                            return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
    
}