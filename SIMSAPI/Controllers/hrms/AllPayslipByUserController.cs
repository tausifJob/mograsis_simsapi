﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.gradebookClass;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/AllPayslipByUser")]
     [BasicAuthentication]
    public class AllPayslipByUserController : ApiController
    {
        [Route("getfinanceyear")]
        public HttpResponseMessage getfinanceyear()
        {
            List<payslip_byuser> empDocDetail = new List<payslip_byuser>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.all_payslip_by_user",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payslip_byuser objone = new payslip_byuser();

                            objone.paysheet_year = dr["paysheet_year"].ToString();
                            objone.paysheet_year_desc = dr["paysheet_year_desc"].ToString();
                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("getpayslipList")]
        public HttpResponseMessage getpayslipList(string paysheet_year, string pe_number)
        {
            List<payslip_byuser> empDocDetail = new List<payslip_byuser>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.all_payslip_by_user",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@paysheet_year", paysheet_year),
                            new SqlParameter("@pe_number", pe_number),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payslip_byuser objone = new payslip_byuser();

                            objone.paysheet_year = dr["paysheet_year"].ToString();
                            objone.paysheet_year_desc = dr["paysheet_year_desc"].ToString();
                            objone.paysheet_month = dr["paysheet_month"].ToString();
                            objone.paysheet_month_name = dr["paysheet_month_name"].ToString();
                            objone.pe_number = dr["pe_number"].ToString();
                            objone.emp_name = dr["emp_name"].ToString();
                            objone.sd_amount = dr["sd_amount"].ToString();
                            objone.pe_year_month = dr["pe_year_month"].ToString();
                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("getpath")]
        public HttpResponseMessage getpath()
        {

            List<payslip_byuser> empDocDetail = new List<payslip_byuser>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.all_payslip_by_user",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payslip_byuser objone = new payslip_byuser();
                            objone.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }
    }
}