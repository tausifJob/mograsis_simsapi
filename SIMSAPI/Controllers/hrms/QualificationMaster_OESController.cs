﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.QualificationMaster_OESController
{
    [RoutePrefix("api/QualMasterOES")]
    
    [BasicAuthentication]
    public class QualificationMaster_OESController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("getAllQualification")]
        public HttpResponseMessage getAllQualification()
        {

            List<Per256> desg_list = new List<Per256>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_qualification_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per256 simsobj = new Per256();
                            simsobj.pays_qualification_code = dr["pays_qualification_code"].ToString();
                            simsobj.pays_qualification_desc = dr["pays_qualification_desc"].ToString();
                            simsobj.pays_qualification_desc_arabic = dr["pays_qualification_desc_arabic"].ToString();
                            simsobj.pays_qualificaion_display_oder = dr["pays_qualificaion_display_oder"].ToString();
                            simsobj.pays_qualificaiton_status = dr["pays_qualificaiton_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }



        [Route("CUDQualification")]
        public HttpResponseMessage CUDDesignation(List<Per256> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per256 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_qualification_proc]",
                           new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_qualification_code", simsobj.pays_qualification_code),
                                new SqlParameter("@pays_qualification_desc", simsobj.pays_qualification_desc),
                                new SqlParameter("@pays_qualification_desc_ar", simsobj.pays_qualification_desc_arabic),
                                new SqlParameter("@pays_qualificaion_display_oder", simsobj.pays_qualificaion_display_oder),
                                new SqlParameter("@pays_qualificaiton_status", simsobj.pays_qualificaiton_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
