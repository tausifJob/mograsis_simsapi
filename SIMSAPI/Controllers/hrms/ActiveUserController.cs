﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/ActiveUser")]
    public class ActiveUserController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Search_Employee_by_first_name")]
        public HttpResponseMessage Search_Employee_by_first_name(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_first_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Employee_by_first_name"));

            List<Pers308> list = new List<Pers308>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.em_company_code = dr["em_company_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.status = "Inactive";
                            //    obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Employee_by_last_name")]
        public HttpResponseMessage Search_Employee_by_last_name(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_last_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Employee_by_last_name"));

            List<Pers308> list = new List<Pers308>();

            if (last_name == null)
            {
                last_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.em_company_code = dr["em_company_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.status = "Inactive";
                            // obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Employee_by_login_id")]
        public HttpResponseMessage Search_Employee_by_login_id(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_login_id(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Employee_by_login_id"));

            List<Pers308> list = new List<Pers308>();

            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.em_company_code = dr["em_company_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.status = "Inactive";
                            //  obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_first_name")]
        public HttpResponseMessage Search_Student_by_first_name(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_first_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Student_by_first_name"));

            List<Pers308> list = new List<Pers308>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Inactive";
                            // obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_last_name")]
        public HttpResponseMessage Search_Student_by_last_name(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_last_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Student_by_last_name"));

            List<Pers308> list = new List<Pers308>();

            if (last_name == null)
            {
                last_name = "";
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Inactive";
                            //  obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_login_id")]
        public HttpResponseMessage Search_Student_by_login_id(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_login_id(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Student_by_login_id"));

            List<Pers308> list = new List<Pers308>();
            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Pers308 obj = new Pers308();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Inactive";
                            // obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_first_name")]
        public HttpResponseMessage Search_Parent_by_first_name(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_first_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Parent_by_first_name"));

            List<Pers308> list = new List<Pers308>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Inactive";
                            // obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_last_name")]
        public HttpResponseMessage Search_Parent_by_last_name(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_last_name(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Parent_by_last_name"));

            List<Pers308> list = new List<Pers308>();
            if (last_name == null)
            {
                last_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Inactive";
                            //  obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_login_id")]
        public HttpResponseMessage Search_Parent_by_login_id(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_login_id(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ActiveUser", "Search_Parent_by_login_id"));

            List<Pers308> list = new List<Pers308>();
            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("activate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers308 obj = new Pers308();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Inactive";
                            // obj.activation_date = DateTime.Now.ToShortDateString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Insert_Parent_activation_history")]
        public HttpResponseMessage Insert_Parent_activation_history(List<Pers308> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Parent_activation_history(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Parent", "Insert_Parent_activation_history"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers308 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("activate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'L'),
                                new SqlParameter("@login_code", persobj.login_id),
                                 new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.activation_date)),
                                 new SqlParameter("@sims_student_status_updation_user_code", persobj.sims_student_status_updation_user_code)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Student_activation_history")]
        public HttpResponseMessage Insert_Student_activation_history(List<Pers308> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Student_activation_history(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Parent", "Insert_Student_activation_history"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers308 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("activate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'Y'),
                                new SqlParameter("@login_code", persobj.login_id),
                                new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.activation_date)),
                                new SqlParameter("@sims_student_status_updation_user_code", persobj.sims_student_status_updation_user_code)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Employee_activation_history")]
        public HttpResponseMessage Insert_Employee_activation_history(List<Pers308> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Employee_activation_history(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Parent", "Insert_Employee_activation_history"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers308 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("activate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'X'),
                                new SqlParameter("@login_code", persobj.login_id),
                                new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.activation_date)),
                                new SqlParameter("@sims_student_status_updation_user_code", persobj.sims_student_status_updation_user_code)
                                //new SqlParameter("@eh_number", persobj.em_number)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}