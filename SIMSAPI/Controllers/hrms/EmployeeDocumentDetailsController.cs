﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/employeedoc")]
    public class EmployeeDocumentDetailsController : ApiController
    {
        [Route("getEmpDetails")]
        public HttpResponseMessage getEmpDetails()
        {
            List<Per610> empDocDetail = new List<Per610>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per610 objone = new Per610();

                            objone.co_desc = dr["co_desc"].ToString();
                            objone.co_company_code = dr["co_company_code"].ToString();

                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }
        [Route("getDeptDetails")]
        public HttpResponseMessage getDeptDetails()
        {
            List<Per610> empDocDetail = new List<Per610>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per610 objone1 = new Per610();

                            objone1.codp_dept_name = dr["codp_dept_name"].ToString();
                            objone1.codp_dept_no = dr["codp_dept_no"].ToString();

                            empDocDetail.Add(objone1);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }
        
        [Route("getDesgDetails")]
        public HttpResponseMessage getDesgDetails()
        {
            List<Per610> empDocDetail = new List<Per610>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per610 objone2 = new Per610();

                            objone2.dg_desc = dr["dg_desc"].ToString();
                            objone2.dg_code = dr["dg_code"].ToString();

                           
                            empDocDetail.Add(objone2);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("getDescDetails")]
        public HttpResponseMessage getDescDetails()
        {
            List<Per610> empDocDetail = new List<Per610>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "E")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per610 objone3 = new Per610();

                            objone3.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            objone3.pays_doc_code = dr["pays_doc_code"].ToString();
                            // objone1.codp_comp_code = dr["codp_comp_code"].ToString();
                            empDocDetail.Add(objone3);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        //#region Employee-Teacher Mapping

        [Route("getTableDetails")]
        public HttpResponseMessage getTableDetails(string dept, string desg)
        {
            List<Per610> empDocDetail = new List<Per610>();
            try
            {
                if (dept == "undefined" || dept == "")
                {
                    dept = null;
                }
                if (desg == "undefined" || desg == "")
                {
                    desg = null;
                }
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@em_desg_code", desg),
                            new SqlParameter("@em_dept_code", dept),


                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per610 objone3 = new Per610();
                            objone3.em_login_code = dr["em_login_code"].ToString();
                            objone3.em_first_name = dr["em_first_name"].ToString();
                            objone3.pays_doc_srno = dr["pays_doc_srno"].ToString();
                            objone3.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            objone3.pays_doc_path = dr["pays_doc_path"].ToString();
                            objone3.pays_doc_issue_date = db.UIDDMMYYYYformat(dr["pays_doc_issue_date"].ToString());
                            objone3.pays_doc_expiry_date = db.UIDDMMYYYYformat(dr["pays_doc_expiry_date"].ToString());
                           


                            empDocDetail.Add(objone3);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("CUDgetAllRecords")]
        public HttpResponseMessage CUDgetAllRecords(List<Per610> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per610 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                                    {
                                             new SqlParameter("@opr", simsobj.opr),
                                             new SqlParameter("@pays_doc_mod_code", simsobj.pays_doc_mod_code),
                                             new SqlParameter("@pays_doc_code", simsobj.pays_doc_code),
                                             new SqlParameter("@pays_doc_srno",simsobj.pays_doc_srno),
                                             new SqlParameter("@pays_doc_empl_id",simsobj.pays_doc_empl_id),
                                             new SqlParameter("@pays_doc_desc",simsobj.pays_doc_desc),
                                             new SqlParameter("@pays_doc_path",simsobj.pays_doc_path),
                                             new SqlParameter("@pays_doc_issue_date",db.DBYYYYMMDDformat(simsobj.pays_doc_issue_date)),
                                             new SqlParameter("@pays_doc_expiry_date",db.DBYYYYMMDDformat(simsobj.pays_doc_expiry_date)),
                                             new SqlParameter("@pays_doc_status",simsobj.pays_doc_status)
                                             
                                    });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDDeleteAllRecords")]
        public HttpResponseMessage CUDDeleteAllRecords(List<Per610> data)
        {
            bool deleted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per610 simsobj in data)
                    {
                        int del = db.ExecuteStoreProcedureforInsert("[dbo].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                                    {
                                          new SqlParameter("@opr", simsobj.opr),
                                           new SqlParameter("@pays_doc_srno",simsobj.pays_doc_srno),
                                           new SqlParameter("@pays_doc_empl_id",simsobj.pays_doc_empl_id),
                                               
                                    });

                        if (del > 0)
                        {
                            deleted = true;
                        }
                        else
                        {
                            deleted = false;
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, deleted);
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, deleted);
        }


       
    }
}