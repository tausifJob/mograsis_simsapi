﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmpDocumentMaster")]
    public class EmployeeDocumentMasterController : ApiController
    {
        //static string root = "http://localhost/SIMSAPI/Content/databaseConnectionString/UploadedFiles/";

        [Route("getEmpDocumentDetail")]
        public HttpResponseMessage getEmpDocumentDetail()
        {
            List<Sim020> empDocDetail = new List<Sim020>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_doc_master_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim020 objone = new Sim020();

                            objone.pays_doc_mod_code = dr["pays_doc_mod_code"].ToString();
                            objone.pays_doc_code = dr["pays_doc_code"].ToString();
                            objone.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            //objone.pays_doc_path = dr["pays_doc_path"].ToString();
                            objone.pays_doc_create_date = db.UIDDMMYYYYformat(dr["pays_doc_create_date"].ToString());
                            objone.pays_doc_created_by = dr["pays_doc_created_by"].ToString();
                            objone.pays_doc_status = dr["pays_doc_status"].ToString() == "A";
                            objone.pays_recruitment_status = dr["pays_recruitment_status"].ToString().Equals("A")?true:false;
                            objone.pays_available_on_short_form = dr["pays_available_on_short_form"].ToString().Equals("Y") ? true : false;
                            objone.pays_available_on_long_form = dr["pays_available_on_long_form"].ToString().Equals("Y") ? true : false;
                            objone.pays_emp_visible_status = dr["pays_emp_visible_status"].ToString().Equals("Y") ? true : false;
                            objone.pays_emp_upload_doc = dr["pays_emp_upload_doc"].ToString().Equals("Y") ? true : false;
                            //if (dr["pays_doc_status"].ToString() == "Y")
                            //    objone.pays_doc_status = true;
                            //else
                            //    objone.pays_doc_status = false;
                            empDocDetail.Add(objone);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, empDocDetail);
        }

        [Route("CUDgetAllRecords")]
        public HttpResponseMessage CUDgetAllRecordsNew(List<Sim020> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim020 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_doc_master_proc]",
                        new List<SqlParameter>()
                        {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@pays_doc_code", simsobj.pays_doc_code),
                             new SqlParameter("@pays_doc_desc", simsobj.pays_doc_desc),
                             new SqlParameter("@pays_doc_status", simsobj.pays_doc_status? "A" : "I"),
                             new SqlParameter("@pays_doc_created_by",simsobj.pays_doc_created_by),
                             new SqlParameter("@pays_recruitment_status",simsobj.pays_recruitment_status.Equals(true)?"A":"I"),
                             new SqlParameter("@pays_available_on_short_form",simsobj.pays_available_on_short_form.Equals(true)?"Y":"N"),
                             new SqlParameter("@pays_available_on_long_form",simsobj.pays_available_on_long_form.Equals(true)?"Y":"N"),
                             new SqlParameter("@pays_emp_visible_status",simsobj.pays_emp_visible_status.Equals(true)?"Y":"N"),
                             new SqlParameter("@pays_emp_upload_doc",simsobj.pays_emp_upload_doc.Equals(true)?"Y":"N")                                             
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}