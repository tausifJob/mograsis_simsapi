﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;

namespace SIMSAPI.Controllers.hrms
{
     [RoutePrefix("api/common/LeaveType")]

    public class LeaveTypeController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("Getleavetype")]
         public HttpResponseMessage Getleavetype()
         {
             List<Persleave_type> pay_list = new List<Persleave_type>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_type_proc]",
                         new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "B"),   
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Persleave_type lepay = new Persleave_type();
                             lepay.staff_code = dr["staff_code"].ToString();
                             lepay.staff_type = dr["staff_type"].ToString();
                             pay_list.Add(lepay);
                         }
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, pay_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, pay_list);
         }

         [Route("Get_pay_leavetype")]
         public HttpResponseMessage Get_pay_leavetype()
         {
             List<Persleave_type> pay_list = new List<Persleave_type>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_type_proc]",
                         new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),   
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Persleave_type lepay = new Persleave_type();
                             lepay.le_company_code = dr["comp_code"].ToString();
                             lepay.le_company_name = dr["le_company_code"].ToString();
                             lepay.staff_code = dr["staff_code"].ToString();
                             if (!string.IsNullOrEmpty(dr["staff_type"].ToString()))
                               lepay.staff_type = dr["staff_type"].ToString();
                             lepay.le_leave_code = dr["leave_code"].ToString();
                             lepay.le_leave_type = dr["lCode"].ToString();
                             lepay.le_formula_code = dr["le_formula_code"].ToString();
                             if (dr["le_accumulated_tag"].ToString().Equals("Y"))
                             {
                                 lepay.le_accumulated_tag = true;
                             }
                             else
                             {
                                 lepay.le_accumulated_tag = false;
                             }

                             //lepay.le_accumulated_tag = dr["le_accumulated_tag"].ToString();
                             lepay.le_max_days_allowed = decimal.Parse(dr["le_max_days_allowed"].ToString());

                            try
                            {
                                if (dr["led_lwp_consider"].ToString().Equals("Y"))
                                {
                                    lepay.led_lwp_consider = true;
                                }
                                if (dr["led_accural_status"].ToString().Equals("Y"))
                                {
                                    lepay.led_accural_status = true;
                                }

                                lepay.led_accural_type = dr["led_accural_type"].ToString();
                                lepay.led_accural_days = dr["led_accural_days"].ToString();
                                lepay.led_created_user = dr["led_created_user"].ToString();
                            }
                            catch (Exception ex) { }
                            
                             
                            pay_list.Add(lepay);
                         }
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, pay_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, pay_list);
         }

         [Route("Get_leave_codes")]
         public HttpResponseMessage Get_leave_codes()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_leave_codes(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "HRMS", "Get_leave_codes"));

             List<Persleave_type> leave_list = new List<Persleave_type>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_type_proc]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),                                
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Persleave_type objleave = new Persleave_type();
                             objleave.le_leave_code = dr["cl_code"].ToString();
                             objleave.cl_desc = dr["cl_desc"].ToString();
                             leave_list.Add(objleave);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, leave_list);
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, leave_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, leave_list);
         }

         [Route("CUDLeaveType")]
         public HttpResponseMessage CUDLeaveType(List<Persleave_type> data)
         {
             Message message = new Message();
             bool inserted = false;
             try
             {
                 if (data != null)
                 {
                     using (DBConnection db = new DBConnection())
                     {
                         db.Open();
                         foreach (Persleave_type pay in data)
                         {
                             SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_type_proc]",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", pay.opr),
                            new SqlParameter("@le_company_code", pay.le_company_code),
                            new SqlParameter("@le_leave_code", pay.le_leave_code),
                            new SqlParameter("@le_formula_code", pay.le_formula_code),
                            new SqlParameter("@le_accumulated_tag", pay.le_accumulated_tag?"Y":"N"),
                            new SqlParameter("@le_max_days_allowed", pay.le_max_days_allowed),
                            new SqlParameter("@le_staff_type", pay.staff_code),
                            new SqlParameter("@led_lwp_consider", pay.led_lwp_consider==true?"Y":"N"),
                            new SqlParameter("@led_accural_status", pay.led_accural_status==true?"Y":"N"),
                            new SqlParameter("@led_accural_type", pay.led_accural_type),
                            new SqlParameter("@led_accural_days", pay.led_accural_days),
                            new SqlParameter("@led_created_user", pay.led_created_user)
                         });
                             while (dr.Read())
                             {
                                 string cnt = dr["leave_cnt"].ToString();
                                 if (cnt == "1")
                                 {
                                     inserted = true;
                                 }
                                 else
                                 {
                                     inserted = false;
                                 }
                             }
                         }
                     }

                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, inserted);
                 }
             }
             catch (Exception x)
             {
                 return Request.CreateResponse(HttpStatusCode.OK, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }

         [Route("CUDeleteLeaveType")]
         public HttpResponseMessage CUDeleteLeaveType(List<Persleave_type> data)
         {
             Message message = new Message();
             bool inserted = false;
             try
             {
                 if (data != null)
                 {
                     using (DBConnection db = new DBConnection())
                     {
                         db.Open();
                         foreach (Persleave_type pay in data)
                         {
                             int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_leave_type_proc]",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", pay.opr),
                            new SqlParameter("@le_company_code", pay.le_company_code),
                            new SqlParameter("@le_leave_code", pay.le_leave_code),
                            new SqlParameter("@le_formula_code", pay.le_formula_code),
                            new SqlParameter("@le_accumulated_tag", pay.le_accumulated_tag?"Y":"N"),
                            new SqlParameter("@le_max_days_allowed", pay.le_max_days_allowed),
                            new SqlParameter("@le_staff_type", pay.staff_code)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                         }
                     }

                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, inserted);
                 }
             }
             catch (Exception x)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }

        [Route("Get_accumulate_leave_types")]
        public HttpResponseMessage Get_accumulate_leave_types()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_accumulate_leave_types(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_accumulate_leave_types"));
            List<Persleave_type> leave_list = new List<Persleave_type>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_type_proc]",
                    new List<SqlParameter>()
                    {
                       new SqlParameter("@opr", "C")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Persleave_type objleave = new Persleave_type();
                            objleave.led_accural_type = dr["pays_appl_parameter"].ToString();
                            objleave.led_accural_type_desc = dr["pays_appl_form_field_value1"].ToString();
                            leave_list.Add(objleave);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, leave_list);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, leave_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, leave_list);
        }



    }
}