﻿using log4net;
using SIMSAPI.Helper;

using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/ShiftMaster")]
    public class ShiftMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getShiftId")]
        public HttpResponseMessage getShiftId()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getShiftId(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getShiftId"));
            Pers092 obj = new Pers092();


            List<Pers046> nation_list = new List<Pers046>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_mast_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"), 
                            new SqlParameter("@sh_company_code", 1),
                            new SqlParameter("@sh_company_code_name", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_shift_desc", ""),
                            new SqlParameter("@comn_appl_code","Per092"),
                            new SqlParameter("@comn_mod_code","004")   
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            obj.sh_shift_id = dr["comn_number_sequence"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, obj.sh_shift_id);

            }
            return Request.CreateResponse(HttpStatusCode.OK, obj.sh_shift_id);
        }

        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getCompany"));

            List<HrmsClass> comp_list = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_mast_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),    
                            new SqlParameter("@sh_company_code", 1),
                            new SqlParameter("@sh_company_code_name", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_shift_desc", ""),
                            new SqlParameter("@comn_appl_code","Per092"),
                            new SqlParameter("@comn_mod_code","004") 
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.sh_company_code = dr["co_company_code"].ToString();
                            hrmsobj.sh_company_name = dr["co_desc"].ToString();
                            comp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, comp_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, comp_list);
        }

        [Route("getShiftMaster")]
        public HttpResponseMessage getShiftMaster()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDestinationAirFare(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getDestinationAirFare"));

            List<Pers092> shift_list = new List<Pers092>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_shift_mast_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"), 
                            new SqlParameter("@sh_company_code", 1),
                            new SqlParameter("@sh_company_code_name", ""),
                            new SqlParameter("@sh_shift_id", ""),
                            new SqlParameter("@sh_shift_desc", ""),
                            new SqlParameter("@comn_appl_code","Per092"),
                            new SqlParameter("@comn_mod_code","004")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers092 hrmsobj = new Pers092();
                            hrmsobj.sh_company_name = dr["company_name"].ToString();
                            hrmsobj.sh_company_code = dr["sh_company_code"].ToString();
                            hrmsobj.sh_shift_id = dr["sh_shift_id"].ToString();
                            hrmsobj.sh_shift_desc = dr["sh_shift_desc"].ToString();
                            hrmsobj.sh_shift1_in = dr["sh_shift1_in"].ToString();
                            hrmsobj.sh_shift1_out = dr["sh_shift1_out"].ToString();
                            hrmsobj.sh_shift2_in = dr["sh_shift2_in"].ToString();
                            hrmsobj.sh_shift2_out = dr["sh_shift2_out"].ToString();
                            if (!string.IsNullOrEmpty(dr["sh_shift1_in_greece_start"].ToString()))
                                hrmsobj.sh_shift1_in_greece_start = int.Parse(dr["sh_shift1_in_greece_start"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift1_in_greece_end"].ToString()))
                                hrmsobj.sh_shift1_in_greece_end = int.Parse(dr["sh_shift1_in_greece_end"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift1_out_greece_start"].ToString()))
                                hrmsobj.sh_shift1_out_greece_start = int.Parse(dr["sh_shift1_out_greece_start"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift1_out_greece_end"].ToString()))
                                hrmsobj.sh_shift1_out_greece_end = int.Parse(dr["sh_shift1_out_greece_end"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift2_in_greece_start"].ToString()))
                                hrmsobj.sh_shift2_in_greece_start = int.Parse(dr["sh_shift2_in_greece_start"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift2_in_greece_end"].ToString()))
                                hrmsobj.sh_shift2_in_greece_end = int.Parse(dr["sh_shift2_in_greece_end"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift2_out_greece_start"].ToString()))
                                hrmsobj.sh_shift2_out_greece_start = int.Parse(dr["sh_shift2_out_greece_start"].ToString());
                            if (!string.IsNullOrEmpty(dr["sh_shift2_out_greece_end"].ToString()))
                                hrmsobj.sh_shift2_out_greece_end = int.Parse(dr["sh_shift2_out_greece_end"].ToString());
                            shift_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shift_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, shift_list);
        }

        [Route("ShiftMasterINSERT")]
        public HttpResponseMessage ShiftMasterINSERT(List<Pers092> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ShiftMasterINSERT(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "ShiftMasterINSERT"));

            Message message = new Message();
            bool inserted = false;


            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();


                        foreach (Pers092 hrmsobj in data)
                        {
                            if (hrmsobj.sh_shift1_in == "")
                            {
                                hrmsobj.sh_shift1_in = null;
                            }
                            if (hrmsobj.sh_shift1_out == "")
                            {
                                hrmsobj.sh_shift1_out = null;
                            }
                            if (hrmsobj.sh_shift2_in == "")
                            {
                                hrmsobj.sh_shift2_in = null;
                            }
                            if (hrmsobj.sh_shift2_out == "")
                            {
                                hrmsobj.sh_shift2_out = null;
                            }

                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_shift_mast_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", hrmsobj.opr),
                                new SqlParameter("@sh_company_code",""),
                                new SqlParameter("@sh_company_code_name",hrmsobj.sh_company_code),
                                new SqlParameter("@sh_shift_id", hrmsobj.sh_shift_id),
                                new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_desc),
                                new SqlParameter("@sh_shift1_in", hrmsobj.sh_shift1_in),
                                new SqlParameter("@sh_shift1_out", hrmsobj.sh_shift1_out),
                                new SqlParameter("@sh_shift2_in", hrmsobj.sh_shift2_in),
                                new SqlParameter("@sh_shift2_out", hrmsobj.sh_shift2_out),
                                new SqlParameter("@sh_shift1_in_greece_start", hrmsobj.sh_shift1_in_greece_start),
                                new SqlParameter("@sh_shift1_in_greece_end", hrmsobj.sh_shift1_in_greece_end),
                                new SqlParameter("@sh_shift1_out_greece_start", hrmsobj.sh_shift1_out_greece_start),
                                new SqlParameter("@sh_shift1_out_greece_end", hrmsobj.sh_shift1_out_greece_end),
                                new SqlParameter("@sh_shift2_in_greece_start", hrmsobj.sh_shift2_in_greece_start),
                                new SqlParameter("@sh_shift2_in_greece_end", hrmsobj.sh_shift2_in_greece_end),
                                new SqlParameter("@sh_shift2_out_greece_start", hrmsobj.sh_shift2_out_greece_start),
                                new SqlParameter("@sh_shift2_out_greece_end", hrmsobj.sh_shift2_out_greece_end),
                                new SqlParameter("@comn_appl_code","Per092"),
                                new SqlParameter("@comn_mod_code","004")
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}