﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.DestinationController
{
    [RoutePrefix("api/common/Destination")]
    [BasicAuthentication]
    public class DestinationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllDestination")]
        public HttpResponseMessage getAllDestination()//int pageIndex, int PageSize
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDestination(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllDestination"));

            List<Per299> dest_list = new List<Per299>();
            //int total = 0, skip = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_destination_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per299 simsobj = new Per299();
                            simsobj.country_code = dr["ds_nation_code_code"].ToString();
                            simsobj.country_name = dr["ds_nation_code"].ToString();
                            simsobj.dest_code = dr["ds_code"].ToString();
                            simsobj.dest_name = dr["ds_name"].ToString();
                            dest_list.Add(simsobj);
                            //total = dest_list.Count;
                            //skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dest_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dest_list);//.Skip(skip).Take(PageSize).ToList()
        }

        [Route("getCountryName")]
        public HttpResponseMessage getCountryName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCountryName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCountryName"));

            List<Per299> mod_list = new List<Per299>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_destination_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per299 simsobj = new Per299();
                            simsobj.country_code = dr["sims_country_code"].ToString();
                            simsobj.country_name = dr["sims_country_name_en"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        //[Route("getAutoGenerateCode")]
        //public HttpResponseMessage getAutoGenerateCode()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateCode()PARAMETERS ::NA";
        //    Log.Debug(string.Format(debug, "ERP/Inventory/", "getAutoGenerateCode"));

        //    List<Per299> mod_list = new List<Per299>();

        //    Message message = new Message();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_destination_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", 'R'),

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Per299 simsobj = new Per299();
        //                    simsobj.dest_code = dr["Dest_code"].ToString();
        //                    mod_list.Add(simsobj);

        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //            }
        //            else
        //                return Request.CreateResponse(HttpStatusCode.OK, dr);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        message.strMessage = "No Records Found";
        //        message.messageType = MessageType.Error;
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //    }



        //    //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        //}

        [Route("CUDDestination")]
        public HttpResponseMessage CUDDestination(List<Per299> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDestination(),PARAMETERS :: OPR {2}";
            bool inserted = false;

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per299 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_destination_proc]",
                          new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@ds_nation_code", simsobj.country_code),
                                new SqlParameter("@ds_code", simsobj.dest_code),
                                 new SqlParameter("@ds_code_list", simsobj.dest_code),
                                new SqlParameter("@ds_name", simsobj.dest_name),
                                
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
