﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/contract")]
    public class ContractController:ApiController
    {
        #region Pays_contract(Pers060)

        //[Route("CUD_Contract")]
        //public HttpResponseMessage CUD_Contract(Pers060 contract)
        //{
        //    bool Updated = false;
        //    string prorata="";
        //    try
        //    {
        //        //if(contract.cn_prorata_flat_tag == true)
        //        //{
        //        //   prorata = "P";
        //        //}
        //        //else
        //        //{
        //        //    prorata = "F";
        //        //}
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_contract_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //        new SqlParameter("@opr", contract.opr),
        //        new SqlParameter("@cn_company_code", contract.dg_company_code),
        //        new SqlParameter("@cn_code", contract.cn_code.ToString()),
        //        new SqlParameter("@cn_desc", contract.cn_desc),
        //        new SqlParameter("@cn_duration", contract.cn_duration),
        //         new SqlParameter("@cn_prorata_flat_tag", prorata),
        //        new SqlParameter("@cn_min_work_days", contract.cn_min_work_days),
        //        new SqlParameter("@cn_first_cl_working_days", contract.cn_first_cl_working_days),
        //        new SqlParameter("@cn_first_cl_leave_days", contract.cn_first_cl_leave_days),
        //        new SqlParameter("@cn_cons_cl_working_days", contract.cn_cons_cl_working_days),
        //        new SqlParameter("@cn_cons_cl_leave_days", contract.cn_cons_cl_leave_days)
        //                 });
        //            if (dr.RecordsAffected > 0)
        //            {
        //                Updated = true;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
               
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, Updated);
        //}



        [Route("CUD_Contract")]
        public HttpResponseMessage CUD_Contract(List<Pers060> data)
        {
            bool insert = false;
           
            Message message = new Message();
            try
            {
              
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers060 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_contract_proc]",

                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@cn_company_code", simsobj.dg_company_code),
                          new SqlParameter("@cn_code", simsobj.cn_code.ToString()),
                          new SqlParameter("@cn_desc", simsobj.cn_desc),
                          new SqlParameter("@cn_duration", simsobj.cn_duration),
                           new SqlParameter("@cn_prorata_flat_tag", simsobj.cn_prorata_flat_tag?"P":"F"),
                          new SqlParameter("@cn_min_work_days", simsobj.cn_min_work_days),
                          new SqlParameter("@cn_first_cl_working_days", simsobj.cn_first_cl_working_days),
                          new SqlParameter("@cn_first_cl_leave_days", simsobj.cn_first_cl_leave_days),
                          new SqlParameter("@cn_cons_cl_working_days", simsobj.cn_cons_cl_working_days),
                          new SqlParameter("@cn_cons_cl_leave_days", simsobj.cn_cons_cl_leave_days)
                              
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



        [Route("Get_Contract")]
        public HttpResponseMessage Get_Contract()
        {
            List<Pers060> mod_list = new List<Pers060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_contract_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers060 hrmsObj = new Pers060();
                            hrmsObj.dg_company_code = dr["cn_company_code"].ToString();
                            hrmsObj.dg_company_name = dr["cn_company_name"].ToString();
                            hrmsObj.cn_code = Int32.Parse(dr["cn_code"].ToString());
                            hrmsObj.cn_desc = dr["cn_desc"].ToString();
                            hrmsObj.cn_duration = decimal.Parse(dr["cn_duration"].ToString());
                            hrmsObj.cn_first_cl_working_days = decimal.Parse(dr["cn_first_cl_working_days"].ToString());
                            hrmsObj.cn_first_cl_leave_days = decimal.Parse(dr["cn_first_cl_leave_days"].ToString());
                            hrmsObj.cn_cons_cl_working_days = decimal.Parse(dr["cn_cons_cl_working_days"].ToString());
                            hrmsObj.cn_cons_cl_leave_days = decimal.Parse(dr["cn_cons_cl_leave_days"].ToString());
                            hrmsObj.cn_prorata_flat_tag = (dr["cn_prorata_flat_tag"].Equals("P") ? true : false);
                            hrmsObj.cn_min_work_days = Convert.ToInt32(dr["cn_min_work_days"]);
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
              
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("Get_ContractDetails")]
        public HttpResponseMessage Get_ContractDetails()
        {
            List<Pers060> mod_list = new List<Pers060>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_contract_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers060 hrmsObj = new Pers060();
                            hrmsObj.dg_company_code = dr["cn_company_code"].ToString();
                            hrmsObj.dg_company_name = dr["cn_company_name"].ToString();
                            hrmsObj.cn_code = Int32.Parse(dr["cn_code"].ToString());
                            hrmsObj.cn_desc = dr["cn_desc"].ToString();
                            hrmsObj.cn_duration = decimal.Parse(dr["cn_duration"].ToString());                            
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        #endregion
    }
}