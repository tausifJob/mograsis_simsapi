﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/DefineCategoryDetails")]
    public class DefineCategoryController : ApiController
    {
        [Route("getDefineCategory")]
        public HttpResponseMessage getDefineCategory()
        {
            List<Per254> category_list = new List<Per254>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_category_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per254 obj = new Per254();

                            obj.ca_code = dr["ca_code"].ToString();
                            obj.ca_company_code = dr["ca_company_code"].ToString();
                            obj.ca_company_name = dr["ca_company_name"].ToString();

                            
                           // obj.comp_name = dr["comp_name"].ToString();
                            obj.ca_desc = dr["ca_desc"].ToString();
                            obj.ca_process_tag = dr["ca_process_tag"].ToString().Equals("Y") ? true : false;

                            category_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, category_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, category_list);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<Per254> mod_list = new List<Per254>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_category_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'W'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per254 simsobj = new Per254();
                            simsobj.ca_company_name = dr["co_company_code"].ToString();
                            simsobj.desg_company_code_name = dr["co_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("DefineCategoryCUD")]
        public HttpResponseMessage DefineCategoryCUD(List<Per254> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per254 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_category_proc]",
                        new List<SqlParameter>()
                        {

                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@ca_code",simsobj.ca_code),
                          new SqlParameter("@ca_company_code",simsobj.ca_company_name),
                          new SqlParameter("@ca_desc",simsobj.ca_desc),
                          new SqlParameter("@ca_process_tag",simsobj.ca_process_tag==true?"Y":"N")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}