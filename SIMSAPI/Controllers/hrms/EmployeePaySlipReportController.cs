﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/Payroll/EmployeePaySlipReport")]
    public class EmployeePaySlipReportController : ApiController
    {

        [Route("PubYear")]
        public HttpResponseMessage PubYear()
        {
            List<EPSR01> emp_list = new List<EPSR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeePaySlipReport_Proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "Y")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EPSR01 hrmsobj = new EPSR01();
                            hrmsobj.fin_year = dr["fins_appl_form_field_value1"].ToString();
                            hrmsobj.fin_year_desc = dr["fins_appl_form_field_value3"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("PubMonth")]
        public HttpResponseMessage PubMonth()
        {
            List<EPSR01> emp_list = new List<EPSR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeePaySlipReport_Proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EPSR01 hrmsobj = new EPSR01();
                            hrmsobj.sd_year_month_code = dr["pays_appl_parameter"].ToString();
                            hrmsobj.sd_year_month_name = dr["pays_appl_form_field_value1"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("EmployeeDetails")]
        public HttpResponseMessage EmployeeDetails(string year_month)
        {
            List<EPSR01> emp_list = new List<EPSR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeePaySlipReport_Proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@year_month", year_month)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EPSR01 hrmsobj = new EPSR01();
                            hrmsobj.emp_number = dr["sd_number"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.emp_name = dr["emp_name"].ToString();
                            hrmsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            hrmsobj.dg_desc = dr["dg_desc"].ToString();
                            hrmsobj.earn = dr["earn"].ToString();
                            hrmsobj.ded = dr["Ded"].ToString();
                            hrmsobj.net_amount = dr["net_amount"].ToString();
                           

                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

       
    }
}