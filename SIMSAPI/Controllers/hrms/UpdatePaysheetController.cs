﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/updatepaysheet")]
    public class UpdatePaysheetController:ApiController
    {
        #region Update Payroll

        [Route("GetMonth")]
        public HttpResponseMessage GetMonth()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'J')
               

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.sd_year_month_code = dr["pays_appl_parameter"].ToString();
                            hrmsObj.sd_year_month_name = dr["pays_appl_form_field_value1"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("GetCurrentFinYearFromFinsParameter")]
        public HttpResponseMessage GetCurrentFinYearFromFinsParameter()
        {
            string fin_year = "0";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "H")
                });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            fin_year = dr["fins_appl_form_field_value1"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, fin_year);

        }


       [Route("GetAllPayCodes")]
        public HttpResponseMessage GetAllPayCodes()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    while (dr.Read())
                    {
                        Pers078 hrmsObj = new Pers078();
                        hrmsObj.cp_desc = dr["cp_desc"].ToString();
                        hrmsObj.sd_pay_code = dr["cp_code"].ToString();
                        hrmsObj.pc_earn_dedn_tag = dr["pc_earn_dedn_tag"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

       [Route("GetSalaryDetailsByEmployee")]
       public HttpResponseMessage GetSalaryDetailsByEmployee(string year_month, string sd_number)
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                 using (DBConnection db = new DBConnection())
                {
                db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sd_number", sd_number),
                new SqlParameter("@sd_year_month", year_month)
            });
                     if(dr.HasRows)
                     {
                while (dr.Read())
                {
                    Pers078 hrmsObj = new Pers078();
                    hrmsObj.sd_pay_code = dr["sd_pay_code"].ToString();
                    hrmsObj.cp_desc = dr["cp_desc"].ToString();

                    hrmsObj.sd_number = dr["sd_number"].ToString();
                    hrmsObj.emp_name = dr["emp_name"].ToString();

                    hrmsObj.sd_company_code = dr["sd_company_code"].ToString();
                    hrmsObj.sd_year_month = dr["sd_year_month"].ToString();

                    hrmsObj.pc_earn_dedn_tag = dr["pc_earn_dedn_tag"].ToString();
                    hrmsObj.sd_remark = dr["sd_remark"].ToString();

                    if (string.IsNullOrEmpty(dr["sd_amount"].ToString()) == false)
                    {
                        if (dr["pc_earn_dedn_tag"].ToString() == "E")
                            hrmsObj.sd_amount = decimal.Parse(dr["sd_amount"].ToString());
                        else if (dr["pc_earn_dedn_tag"].ToString() == "D")
                            hrmsObj.sd_amount = (decimal.Parse(dr["sd_amount"].ToString()));
                    }
                    else
                        hrmsObj.sd_amount = 0;
                    hrmsObj.code = dr["sd_pay_code"].ToString();
                    mod_list.Add(hrmsObj);
                }
                     }
                }
               
            }
            catch (Exception c)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

       [Route("Get_Mapped_Employee")]
       public HttpResponseMessage Get_Mapped_Employee(string data)
       {
           sims173 simsobj = new sims173();
           if (data == "undefined" || data == "\"\"")
           {
               data = null;
           }
           else
           {
               simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<sims173>(data);
           }

           List<sims173> lstEmp = new List<sims173>();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[emp_teacher_mapping_proc]",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@company_code", simsobj.company_code),
                new SqlParameter("@dept_code", simsobj.dept_code),
                new SqlParameter("@desg_code", simsobj.desg_code),
                new SqlParameter("@grade_code", simsobj.grade_code)
                         });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           sims173 obj = new sims173();
                           obj.sims_emp_code = dr["em_login_code"].ToString();
                           obj.sims_emp_name = dr["Empname"].ToString();
                           obj.sims_teacher_type = dr["sims_teacher_type"].ToString();
                           if (string.IsNullOrEmpty(dr["sims_create_assignments"].ToString()) || dr["sims_create_assignments"].ToString().ToUpper() == "N")
                               obj.sims_emp_ca = false;
                           else
                               obj.sims_emp_ca = true;
                           if (string.IsNullOrEmpty(dr["sims_create_categories"].ToString()) || dr["sims_create_categories"].ToString().ToUpper() == "N")
                               obj.sims_emp_cc = false;
                           else
                               obj.sims_emp_cc = true;

                           if (string.IsNullOrEmpty(dr["sims_status"].ToString()) || dr["sims_status"].ToString().ToUpper() == "I")
                               obj.sims_emp_status = false;
                           else
                               obj.sims_emp_status = true;
                           lstEmp.Add(obj);
                       }
                   }
               }
               return Request.CreateResponse(HttpStatusCode.OK, lstEmp);
           }
           catch (Exception x)
           {

           }
           return Request.CreateResponse(HttpStatusCode.OK, lstEmp);
       }

       [Route("GetLoanDetailsofEmployees")]
       public HttpResponseMessage GetLoanDetailsofEmployees()
        {
            List<Pers134LoanRegister> mod_list = new List<Pers134LoanRegister>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A")
                });
                    while (dr.Read())
                    {
                        Pers134LoanRegister hrmsObj = new Pers134LoanRegister();
                        hrmsObj.EmployeeNameNo = dr["lo_number"].ToString();
                        hrmsObj.Company_code = dr["lo_company_code"].ToString();
                        hrmsObj.Loancode = dr["lo_code"].ToString();
                        hrmsObj.LoanDesc = dr["lc_desc"].ToString();
                        hrmsObj.LoanAmmount = dr["lo_amount"].ToString();
                        hrmsObj.Paid_amount = dr["lo_paid_amount"].ToString();
                        hrmsObj.Total_installments = dr["lo_no_of_installment"].ToString();
                        hrmsObj.Repaid_installments = dr["lo_installment_amount"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

       [Route("Update_Employee_Salary_Details")]
       public HttpResponseMessage Update_Employee_Salary_Details(Pers078 hrmsobj)
        {
            bool Updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'U'),
                new SqlParameter("@sd_number", hrmsobj.sd_number),
                new SqlParameter("@sd_company_code", hrmsobj.sd_company_code),
                new SqlParameter("@sd_year_month", hrmsobj.sd_year_month),
                new SqlParameter("@sd_pay_code_with_amount", hrmsobj.sd_pay_code),
                new SqlParameter("@lo_code_with_amount", hrmsobj.name)
                  });
                    if (dr.RecordsAffected > 0)
                    {
                        Updated = true;
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Updated);

        }

        [Route("Update_Employee_Salary_Details_New")]
        public HttpResponseMessage Update_Employee_Salary_Details_New(List<Pers078> data)
        {
            bool Updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers078 obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "V"),                                
                                new SqlParameter("@sd_number", obj.sd_number),
                                new SqlParameter("@sd_company_code", obj.sd_company_code),
                                new SqlParameter("@sd_year_month", obj.sd_year_month),
                                new SqlParameter("@sd_pay_code", obj.sd_pay_code),
                                new SqlParameter("@lo_code_with_amount", obj.name),
                                new SqlParameter("@sd_amount", obj.sd_amount),
                                new SqlParameter("@sd_remark", obj.sd_remark)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            Updated = true;
                        }
                        else
                        {
                            Updated = false;
                        }
                        dr.Close();
                    }
                }                
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Updated);
        }


        [Route("Employee_Genrated_salary_details")]
       public HttpResponseMessage Employee_Genrated_salary_details(Pers078 obj)
       {
           List<Pers078> hrmsobj = new List<Pers078>();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                    new List<SqlParameter>() 
                    {               
                       new SqlParameter("@opr", 'N'),
                       new SqlParameter("@pe_year_month", obj.sd_year_month),
                       new SqlParameter("@grade_list", obj.gr_code),
                       new SqlParameter("@em_dept_code", obj.dept_code),
                       new SqlParameter("@em_desg_code", obj.dg_code),
                       new SqlParameter("@em_login_code", obj.em_login_code),
                       new SqlParameter("@company_code", obj.Company_code)
                   });
                   while (dr.Read())
                   {
                       Pers078 obj1 = new Pers078();
                       obj1.em_login_code = dr["em_login_code"].ToString();
                       obj1.sd_number = dr["em_number"].ToString();
                       obj1.emp_name = dr["emp_name"].ToString();
                       obj1.gr_desc = dr["gr_desc"].ToString();
                       obj1.dept_desc = dr["codp_dept_name"].ToString();
                       obj1.dg_desc = dr["dg_desc"].ToString();
                       obj1.pe_status = dr["pe_status"].ToString();

                        hrmsobj.Add(obj1);
                   }
               }
           }
           catch (Exception e)
           {
           }
           return Request.CreateResponse(HttpStatusCode.OK, hrmsobj);

       }



        #endregion

       #region Delete Payroll

       [Route("GetEmployeesFromSalaryDetails")]
       public HttpResponseMessage GetEmployeesFromSalaryDetails(string year_month,string company_code,string dept_code,string desg_code,string grade_code,string login_code)
       {
           List<Pers078> mod_list = new List<Pers078>();
           try
           {
                if (company_code == "undefined" || company_code == "")
                    company_code = null;
                if (dept_code == "undefined" || dept_code == "")
                    dept_code = null;
                if (desg_code == "undefined" || desg_code == "")
                    desg_code = null;
                if (grade_code == "undefined" || grade_code == "")
                    grade_code = null;
                if (login_code == "undefined" || login_code == "")
                    login_code = null;
                
             using (DBConnection db = new DBConnection())
             {
               db.Open();
               SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
               new List<SqlParameter>() 
               { 
                   new SqlParameter("@opr", "E"),
                   new SqlParameter("@sd_year_month", year_month),
                   new SqlParameter("@company_code", company_code),
                   new SqlParameter("@dept_code", dept_code),
                   new SqlParameter("@desg_code", desg_code),
                   new SqlParameter("@grade_code", grade_code),
                   new SqlParameter("@login_code", login_code)
               });
               while (dr.Read())
               {
                   Pers078 hrmsObj = new Pers078();
                   hrmsObj.gr_desc = dr["gr_desc"].ToString();
                   hrmsObj.em_grade_code = dr["em_grade_code"].ToString();
                   hrmsObj.em_bank_cash_tag = dr["em_bank_cash_tag"].ToString();
                   hrmsObj.name = dr["EmpName"].ToString();
                   hrmsObj.code = dr["sd_number"].ToString();
                   mod_list.Add(hrmsObj);
               }
             }
           }
           catch (Exception e)
           {
           }
           return Request.CreateResponse(HttpStatusCode.OK, mod_list);
       }


       [Route("Payroll_Delete")]
       public HttpResponseMessage Payroll_Delete(List<Pers078> emp_list)
       {
           string deleted = "", year_month = "", emplist = "";
           foreach (var item in emp_list)
           {
               year_month = item.sd_year_month;
               emplist = emplist + item.sd_number + "/";
           }
           try
           {

               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[payroll_delete_proc]",
                       new List<SqlParameter>() 
                         { 
               new SqlParameter("@year_month", year_month),
               new SqlParameter("@emp_list", emplist)
                });
                   if (dr.RecordsAffected > 0)
                   {
                       deleted = "Payroll deleted for selected employee.";
                   }
                   else
                   {
                       deleted = "Payroll not deleted";
                   }
               }
           }
           catch (Exception c)
           {
               deleted = c.Message;
           }
           return Request.CreateResponse(HttpStatusCode.OK, deleted);

       }

        [Route("GetComapnyCurrencyDetails")]
        public HttpResponseMessage GetComapnyCurrencyDetails()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'W')                        
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.comp_code = dr["comp_code"].ToString();
                            hrmsObj.comp_short_name = dr["comp_short_name"].ToString();
                            hrmsObj.comp_curcy_code = dr["comp_curcy_code"].ToString();
                            hrmsObj.comp_curcy_dec = dr["comp_curcy_dec"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK,e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        #endregion

    }
}