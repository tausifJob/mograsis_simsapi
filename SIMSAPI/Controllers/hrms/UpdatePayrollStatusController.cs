﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/updatepayrollstatus")]
    public class UpdatePayrollStatusController : ApiController
    {
        #region Update payroll status

        [Route("GetYearMonthStatus")]
        public HttpResponseMessage GetYearMonthStatus()
        {
            List<Per326> mod_list = new List<Per326>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "T")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per326 hrmsObj = new Per326();
                            hrmsObj.us_year = dr["usyear"].ToString();
                            hrmsObj.us_month = dr["us_month"].ToString();
                            hrmsObj.us_year_month_code = dr["usyear_month"].ToString();
                            if (dr["pc_update_payroll_status"].ToString() == "Y")
                                hrmsObj.us_status = true;
                            else
                                hrmsObj.us_status = false;
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CheckPayrollStatus")]
        public HttpResponseMessage CheckPayrollStatus(string year_month)
        {
            bool chk = false;
            string msg = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'O'),
                
                new SqlParameter("@year_month",year_month)

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            msg = dr["usyear"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);

        }

        [Route("UpdatePayrollStatus")]
        public HttpResponseMessage UpdatePayrollStatus(string year_month, bool status)
        {
            bool chk = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'K'),
                new SqlParameter("@payroll_status", status.Equals(true)?"Y":"N"),
                new SqlParameter("@year_month",year_month)

              });
                    if (dr.RecordsAffected > 0)
                    {
                        chk = true;
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }





        #endregion
    }
}