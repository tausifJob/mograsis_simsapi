﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LeaveSalaryCriteria")]
    public class LeaveSalaryCriteriaController : ApiController
    {

        [Route("Get_All_pays_grade")]
        public HttpResponseMessage Get_All_pays_grade()
        {

            List<Pers400> list = new List<Pers400>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_salary_criteria_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers400 obj = new Pers400();
                            obj.gr_code = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetAll_Account_Names")]
        public HttpResponseMessage GetAll_Account_Names(string comp_cd)
        {


            List<Finn003> mod_list = new List<Finn003>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins].[fins_financial_periods_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@comp_code",comp_cd),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Finn003 finnobj = new Finn003();
                            finnobj.glac_acct_code = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString();
                            finnobj.glac_acct_name = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString() + "_" + dr["glma_acct_name"].ToString();
                            finnobj.comp_inter_comp_acct = dr["glma_acct_name"].ToString();
                            finnobj.comp_against_inter_comp_acct = dr["glma_acct_name"].ToString();
                            mod_list.Add(finnobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Get_paysLeaveCriteria")]
        public HttpResponseMessage Get_paysLeaveCriteria()
        {

            List<Pers400> list = new List<Pers400>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_salary_criteria_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers400 obj = new Pers400();
                            // obj.srno = Convert.ToInt32(dr["srno"].ToString());
                            obj.leave_criteria_code = dr["leave_criteria_code"].ToString();
                            obj.leave_criteria_name = dr["leave_criteria_name"].ToString();
                            obj.leave_criteria_min_days = Convert.ToInt32(dr["leave_criteria_min_days"].ToString().Trim());
                            obj.year = (obj.leave_criteria_min_days / 365);
                            obj.month = (((obj.leave_criteria_min_days) % 365) / 30);
                            obj.days = (((obj.leave_criteria_min_days) % 365) % 30);
                           
                            if (dr["leave_criteria_max_days"].ToString() != "-")
                            {
                                obj.leave_criteria_max_days = Convert.ToInt32(dr["leave_criteria_max_days"].ToString().Trim());
                            }
                            obj.leave_payable_days = dr["leave_payable_days"].ToString();
                            //obj.leave_working_days = dr["leave_working_days"].ToString();

                            obj.leave_pay_grade = dr["gr_code"].ToString();
                            obj.gr_desc = dr["gr_desc"].ToString();

                            obj.leave_acct_code = dr["accnt_code"].ToString();
                            if (dr["accnt_code"].ToString() !="_")
                            {
                                obj.leave_acct_name = dr["leave_acct_code"].ToString();
                            }
                             
                            obj.leave_bank_acct_code = dr["bank_accnt_code"].ToString();
                            if (dr["bank_accnt_code"].ToString() != "_")
                            {
                                obj.leave_bank_acct_name = dr["leave_bank_acct_code"].ToString();
                            }

                            obj.leave_cash_acct_code = dr["cash_accnt_code"].ToString();
                            if (dr["cash_accnt_code"].ToString() != "_")
                            {
                                obj.leave_cash_acct_name = dr["leave_cash_acct_code"].ToString();
                            }

                            obj.leave_airfare_acct_code = dr["airfare_acct_code"].ToString();
                            if (dr["airfare_acct_code"].ToString() != "_")
                            {
                                obj.leave_airfare_acct_name = dr["leave_airfare_acct_code"].ToString();
                            }

                            obj.leave_prov_acct_code = dr["provision_acct_code"].ToString();
                            if (dr["provision_acct_code"].ToString() != "_")
                            {
                                obj.leave_prov_acct_name = dr["leave_provision_acct_code"].ToString();
                            }

                            if (dr["leave_status"].ToString() == "1")
                            {
                                obj.leave_status = true;
                            }
                            else
                            {
                                obj.leave_status = false;
                            }
                            obj.leave_formula_code = dr["leave_formula_code"].ToString();
                            obj.leave_criteria_order = dr["leave_criteria_order"].ToString();
                           

                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                }
            }
            catch (Exception x)
            {
                
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CUDInsertpaysLeaveCriteria")]
        public HttpResponseMessage CUDInsertpaysLeaveCriteria(List<Pers400> data)
        {
      
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers400 hrmsobj in data)
                    {
                        if (hrmsobj.leave_pay_grade == "undefined")
                        {
                            hrmsobj.leave_pay_grade = null;
                        }
                        if (hrmsobj.leave_acct_code == "undefined")
                        {
                            hrmsobj.leave_acct_code = null;
                        }
                        if (hrmsobj.leave_bank_acct_code == "undefined")
                        {
                            hrmsobj.leave_bank_acct_code = null;
                        }
                        if (hrmsobj.leave_cash_acct_code == "undefined")
                        {
                            hrmsobj.leave_cash_acct_code = null;
                        }
                        if (hrmsobj.leave_airfare_acct_code == "undefined")
                        {
                            hrmsobj.leave_airfare_acct_code = null;
                        }
                        if (hrmsobj.leave_prov_acct_code == "undefined")
                        {
                            hrmsobj.leave_prov_acct_code = null;
                        }    
                        if (hrmsobj.leave_formula_code == "undefined")
                        {
                            hrmsobj.leave_formula_code = null;
                        }
            
                         SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_salary_criteria_proc]",        
                         new List<SqlParameter>()
                              {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@leave_criteria_code", hrmsobj.leave_criteria_code),
                                    new SqlParameter("@leave_criteria_name", hrmsobj.leave_criteria_name),
                                    new SqlParameter("@leave_criteria_min_days", hrmsobj.leave_criteria_min_days),
                                    new SqlParameter("@leave_pay_grade", hrmsobj.leave_pay_grade),
                                    new SqlParameter("@leave_acct_code", hrmsobj.leave_acct_code),
                                    new SqlParameter("@leave_bank_acct_code", hrmsobj.leave_bank_acct_code),
                                    new SqlParameter("@leave_cash_acct_code", hrmsobj.leave_cash_acct_code),
                                    new SqlParameter("@leave_airfare_acct_code", null),
                                    new SqlParameter("@leave_provision_acct_code", hrmsobj.leave_prov_acct_code),
                                    new SqlParameter("@leave_status", hrmsobj.leave_status?"1":"0"),
                                    new SqlParameter("@leave_formula_code", hrmsobj.leave_formula_code),
                                    new SqlParameter("@leave_criteria_max_days", hrmsobj.leave_criteria_max_days),
                                    //new SqlParameter("@leave_working_days", hrmsobj.leave_working_days),
                                    new SqlParameter("@leave_payable_days", hrmsobj.leave_payable_days)
                             });
                         if (dr.RecordsAffected > 0)
                         {
                             { 
                             }
                             inserted = true;
                             }

                          
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

     
        [Route("CUDDeleteLeaveCriteria")]
        public HttpResponseMessage CUDDeleteLeaveCriteria(List<Pers400> data)
        {

            string strMessage = "";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers400 hrmsobj in data)
                    {
                        if (hrmsobj.leave_pay_grade == "undefined")
                        {
                            hrmsobj.leave_pay_grade = null;
                        }
                        if (hrmsobj.leave_acct_code == "undefined")
                        {
                            hrmsobj.leave_acct_code = null;
                        }
                        if (hrmsobj.leave_bank_acct_code == "undefined")
                        {
                            hrmsobj.leave_bank_acct_code = null;
                        }
                        if (hrmsobj.leave_cash_acct_code == "undefined")
                        {
                            hrmsobj.leave_cash_acct_code = null;
                        }
                        if (hrmsobj.leave_airfare_acct_code == "undefined")
                        {
                            hrmsobj.leave_airfare_acct_code = null;
                        }
                        if (hrmsobj.leave_prov_acct_code == "undefined")
                        {
                            hrmsobj.leave_prov_acct_code = null;
                        }
                        if (hrmsobj.leave_formula_code == "undefined")
                        {
                            hrmsobj.leave_formula_code = null;
                        }
                 
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_salary_criteria_proc]",

                                 new List<SqlParameter>()
                              {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@leave_criteria_code", hrmsobj.leave_criteria_code),
                                    new SqlParameter("@leave_criteria_name", hrmsobj.leave_criteria_name),
                                    new SqlParameter("@leave_criteria_min_days", hrmsobj.leave_criteria_min_days),
                                    new SqlParameter("@leave_pay_grade", hrmsobj.leave_pay_grade),
                                    new SqlParameter("@leave_acct_code", hrmsobj.leave_acct_code),
                                    new SqlParameter("@leave_bank_acct_code", hrmsobj.leave_bank_acct_code),
                                    new SqlParameter("@leave_cash_acct_code", hrmsobj.leave_cash_acct_code),
                                    new SqlParameter("@leave_airfare_acct_code", hrmsobj.leave_airfare_acct_code),
                                    new SqlParameter("@leave_provision_acct_code", hrmsobj.leave_prov_acct_code),
                                    new SqlParameter("@leave_status", hrmsobj.leave_status?"1":"0"),
                                    new SqlParameter("@leave_formula_code", hrmsobj.leave_formula_code),
                                    new SqlParameter("@leave_payable_days", hrmsobj.leave_payable_days)
                             });

                        if (dr.Read())
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        else
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        dr.Close();

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, strMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strMessage);
        }

        [Route("CUDUpDelpaysLeaveCriteria")]
        public HttpResponseMessage CUDUpDelpaysLeaveCriteria(List<Pers400> data)
        {

            Message message = new Message();
            bool inserted = false;
            string cnt = "3";
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers400 hrmsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_leave_salary_criteria_proc]",
                                new List<SqlParameter>()
                             {
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@srno", hrmsobj.srno),
                                    new SqlParameter("@leave_criteria_code", hrmsobj.leave_criteria_code),
                                    new SqlParameter("@leave_criteria_name", hrmsobj.leave_criteria_name),
                                    new SqlParameter("@leave_criteria_min_days", hrmsobj.leave_criteria_min_days),
                                    new SqlParameter("@leave_acct_code", hrmsobj.leave_acct_code),
                                    new SqlParameter("@leave_pay_grade", hrmsobj.leave_pay_grade),
                                    new SqlParameter("@leave_bank_acct_code", hrmsobj.leave_bank_acct_code),
                                    new SqlParameter("@leave_cash_acct_code", hrmsobj.leave_cash_acct_code),
                                    new SqlParameter("@leave_airfare_acct_code", hrmsobj.leave_airfare_acct_code),
                                    new SqlParameter("@leave_provision_acct_code", hrmsobj.leave_prov_acct_code),
                                    new SqlParameter("@leave_payable_days", hrmsobj.leave_payable_days),
                                    new SqlParameter("@leave_status", hrmsobj.leave_status),
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}