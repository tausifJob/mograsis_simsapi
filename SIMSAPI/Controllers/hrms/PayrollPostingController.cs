﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/Payroll/PayrollPosting")]
    public class PayrollPostingController : ApiController
    {

        [Route("Year_Month")]
        public HttpResponseMessage Year_Month()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.year_month = dr["year_month"].ToString();
                            hrmsobj.year_month_desc = dr["year_month_name"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("PGrade")]
        public HttpResponseMessage PGrade()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.gr_code = dr["gr_code"].ToString();
                            hrmsobj.gr_desc = dr["gr_desc"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Department")]
        public HttpResponseMessage Department()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "D")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.dept_code = dr["codp_dept_no"].ToString();
                            hrmsobj.dept_desc = dr["codp_dept_name"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Designation")]
        public HttpResponseMessage Designation()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.dg_code = dr["dg_code"].ToString();
                            hrmsobj.dg_desc = dr["dg_desc"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("EmployeeDetails")]
        public HttpResponseMessage EmployeeDetails(Dictionary<string, string> sf)
        {
            object o = null;

            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var p in sf.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        pr1.Value = sf[p];
                        sp.Add(pr1);
                    }
                    SqlParameter pr0 = new SqlParameter();
                    pr0.ParameterName = "@OPR";
                    pr0.Value = "S";
                    sp.Add(pr0);

                    DataSet DS = db.ExecuteStoreProcedureDS("[pays].[pays_payroll_posting_proc]", sp);
                    o = DS;

                }
            }
            catch (Exception x)
            {
                o = x;
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);

        }

        [Route("Voucher_details")]
        public HttpResponseMessage Voucher_details(Pers078 obj)
        {
            Pers078 emp_list = new Pers078();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        emp_list.voucher1 = new List<V1>();
                        emp_list.voucher2 = new List<V2>();

                        while (dr.Read())
                        {
                            V1 hrmsobj = new V1();
                            hrmsobj.account_name1 = dr["acc_name"].ToString();
                            //hrmsobj.debit_amt1 = dr["debit_amt"].ToString();
                            if (dr["debit_amt"].ToString() != "")
                                hrmsobj.debit_amt1 = Math.Round(decimal.Parse(dr["debit_amt"].ToString()), 2).ToString();
                            else
                                hrmsobj.debit_amt1 = dr["debit_amt"].ToString();
                            if (dr["credit_amt"].ToString() != "")
                                hrmsobj.credit_amt1 = Math.Round(decimal.Parse(dr["credit_amt"].ToString()), 2).ToString();
                            else
                                hrmsobj.credit_amt1 = dr["credit_amt"].ToString();
                            //decimal.Parse((dr["credit_amt"].ToString()),2);
                            emp_list.voucher1.Add(hrmsobj);
                        }
                        dr.NextResult();
                        while (dr.Read())
                        {
                            V2 hrmsobj = new V2();
                            hrmsobj.account_name2 = dr["acc_name"].ToString();
                            if (dr["debit_amt"].ToString() != "")
                                hrmsobj.debit_amt2 = Math.Round(decimal.Parse(dr["debit_amt"].ToString()), 2).ToString();
                            else
                                hrmsobj.debit_amt2 = dr["debit_amt"].ToString();
                            if (dr["credit_amt"].ToString() != "")
                                hrmsobj.credit_amt2 = Math.Round(decimal.Parse(dr["credit_amt"].ToString()), 2).ToString();
                            else
                                hrmsobj.credit_amt2 = dr["credit_amt"].ToString();
                            emp_list.voucher2.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Post_details")]
        public HttpResponseMessage Post_details(Pers078 obj)
        {
            Pers078 emp_list = new Pers078();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "V"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@pl_finalize_date", db.DBYYYYMMDDformat(obj.posting_date)),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            emp_list.voucher_msg1 = dr["voucher_msg1"].ToString();
                        }
                        dr.NextResult();
                        while (dr.Read())
                        {
                            emp_list.voucher_msg2 = dr["voucher_msg2"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Gratuity_details")]
        public HttpResponseMessage Gratuity_details(Pers078 obj)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.criteria_name = dr["critera_name"].ToString();
                            hrmsobj.criteria_code = dr["grt_criteria_code"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.emp_name = dr["emp_name"].ToString();
                            hrmsobj.emp_number = dr["grt_emp_code"].ToString();
                            if (dr["grt_amount"].ToString() != "")
                                hrmsobj.grt_amount = Math.Round(decimal.Parse(dr["grt_amount"].ToString()), 2).ToString();
                            else
                                hrmsobj.grt_amount = dr["grt_amount"].ToString();
                            if (dr["grt_amount"].ToString() != "")
                                hrmsobj.grt_amount_old = Math.Round(decimal.Parse(dr["grt_amount"].ToString()), 2).ToString();
                            else
                                hrmsobj.grt_amount_old = dr["grt_amount"].ToString();
                            //hrmsobj.grt_amount = dr["grt_amount"].ToString();
                            //hrmsobj.grt_amount_old = dr["grt_amount"].ToString();

                            hrmsobj.account_no = dr["account_no"].ToString();
                            hrmsobj.account_name = dr["account_name"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("Gratuity_update")]
        public HttpResponseMessage Gratuity_update(List<Pers078> lstobj)
        {
            bool chk = false;
            try
            {
                foreach (var obj in lstobj)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_posting_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),
                                new SqlParameter("@grt_amount_old", obj.grt_amount_old),
                                new SqlParameter("@grt_amount", obj.grt_amount),
                                new SqlParameter("@grt_criteria_code", obj.criteria_code),
                                new SqlParameter("@emp_code", obj.emp_number),
                                new SqlParameter("@grt_updated_user", obj.update_user),
                                new SqlParameter("@year_month", obj.year_month)

                         });
                        while (dr.Read())
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, chk);
        }

        [Route("GratuityPosting")]
        public HttpResponseMessage GratuityPosting(Pers078 obj)
        {
            Pers078 emp_list = new Pers078();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_gratuity_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@pl_finalize_date", db.DBYYYYMMDDformat(obj.posting_date)),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            emp_list.voucher_msg1 = dr["voucher_msg1"].ToString();
                        }
                        dr.NextResult();

                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("AirFare_details")]
        public HttpResponseMessage AirFare_details(Pers078 obj)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.emp_name = dr["emp_name"].ToString();
                            hrmsobj.emp_number = dr["afepd_em_number"].ToString();
                            if (dr["afepd_unit_rate"].ToString() != "")
                                hrmsobj.grt_amount = Math.Round(decimal.Parse(dr["afepd_unit_rate"].ToString()), 2).ToString();
                            else
                                hrmsobj.grt_amount = dr["afepd_unit_rate"].ToString();
                            if (dr["afepd_unit_rate"].ToString() != "")
                                hrmsobj.grt_amount_old = Math.Round(decimal.Parse(dr["afepd_unit_rate"].ToString()), 2).ToString();
                            else
                                hrmsobj.grt_amount_old = dr["grt_amount"].ToString();
                            //hrmsobj.grt_amount = dr["afepd_unit_rate"].ToString();
                            //hrmsobj.grt_amount_old = dr["afepd_unit_rate"].ToString();
                            hrmsobj.account_no = dr["af_provision_acct"].ToString();
                            hrmsobj.account_name = dr["account_name"].ToString();
                            hrmsobj.criteria_code = dr["em_dest_code"].ToString();
                            hrmsobj.criteria_name = dr["dest_name"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("AirFare_update")]
        public HttpResponseMessage AirFare_update(List<Pers078> lstobj)
        {
            bool chk = false;
            try
            {
                foreach (var obj in lstobj)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_posting_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),
                                new SqlParameter("@grt_amount_old", obj.grt_amount_old),
                                new SqlParameter("@grt_amount", obj.grt_amount),
                                new SqlParameter("@emp_code", obj.emp_number),
                                new SqlParameter("@grt_updated_user", obj.update_user),
                                new SqlParameter("@year_month", obj.year_month)

                         });
                        while (dr.Read())
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, chk);
        }

        [Route("AirFarePosting")]
        public HttpResponseMessage AirFarePosting(Pers078 obj)
        {
            Pers078 emp_list = new Pers078();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_posting_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@year_month", obj.year_month),
                                new SqlParameter("@pl_finalize_date", db.DBYYYYMMDDformat(obj.posting_date)),
                                new SqlParameter("@fins_emp_payroll_list", obj.emp_list)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            emp_list.voucher_msg1 = dr["voucher_msg1"].ToString();
                        }
                        dr.NextResult();

                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }
    }
}