﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/SettlementType")]
    public class SettlementTypeController:ApiController
    {

        [Route("getComapany")]
        public HttpResponseMessage getComapany()
        {
            List<Pers090> lstModules = new List<Pers090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_settlement_type_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers090 sequence = new Pers090();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_short_name = dr["comp_short_name"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getFormulaCode")]
        public HttpResponseMessage getFormulaCode()
        {
            List<Pers090> lstModules = new List<Pers090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_settlement_type_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers090 sequence = new Pers090();
                            sequence.fm_code = dr["fm_code"].ToString();
                            sequence.fm_desc = dr["fm_desc"].ToString();
                          
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSettlementType")]
        public HttpResponseMessage getSettlementType()
        {
            List<Pers090> DefineSettlement = new List<Pers090>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_settlement_type_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers090 obj = new Pers090();

                            obj.se_company_code = dr["se_company_code"].ToString();
                            obj.se_code = dr["se_code"].ToString();
                            obj.se_formula_code = dr["se_formula_code"].ToString();
                            if (dr["se_leave_tag"].ToString() == "Y")
                                obj.se_leave_tag = true;
                            else
                                obj.se_leave_tag = false;

                            DefineSettlement.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, DefineSettlement);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, DefineSettlement);
        }

        [Route("SettlementTypeCUD")]
        public HttpResponseMessage SettlementTypeCUD(List<Pers090> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers090 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_settlement_type_proc]",
                          new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@se_company_code", simsobj.se_company_code),
                                 new SqlParameter("@se_code", simsobj.se_code),
                                 new SqlParameter("@se_formula_code", simsobj.se_formula_code),
                                 new SqlParameter("@se_leave_tag", simsobj.se_leave_tag==true?"Y":"N")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }
                    
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}