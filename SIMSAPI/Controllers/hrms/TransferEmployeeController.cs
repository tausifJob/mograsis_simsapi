﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Newtonsoft.Json;

namespace SIMSAPI.Controllers.modules.HRMS
{
    [RoutePrefix("api/TransferEmp")]
    [BasicAuthentication]
    public class TransferEmployeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSearchEmployee")]
        public HttpResponseMessage getSearchEmp(string em_company_code, string em_dept_code, string em_grade_code, string em_desg_code, string em_staff_type_code, string emp_name,string em_login_code)
        {

            if(em_company_code=="" || em_company_code=="undefined")
            {
                em_company_code = null; 
            }
            if (em_dept_code == "" || em_dept_code == "undefined")
            {
                em_dept_code = null;
            }
            if (em_grade_code == "" || em_grade_code == "undefined")
            {
                em_grade_code = null;
            }
            if (em_desg_code == "" || em_desg_code == "undefined")
            {
                em_desg_code = null;
            }
            if (em_staff_type_code == "" || em_staff_type_code == "undefined")
            {
                em_staff_type_code = null;
            }
            if (emp_name == "" || emp_name == "undefined")
            {
                emp_name = null;
            }
            if (em_login_code == "" || em_login_code == "undefined")
            {
                em_login_code = null;
            }

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'S'),
                           new SqlParameter("@em_company_code",em_company_code),
                           new SqlParameter("@em_dept_code",em_dept_code),
                           new SqlParameter("@em_grade_code",em_grade_code),
                           new SqlParameter("@em_desg_code",em_desg_code),
                           new SqlParameter("@em_staff_type_code",em_staff_type_code),
                           new SqlParameter("@emp_name",emp_name),
                           new SqlParameter("@em_login_code",em_login_code),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_login_code = dr["em_login_code"].ToString();
                            obj.em_number = dr["em_number"].ToString();
                            obj.em_Company_Code = dr["em_company_code"].ToString();
                            obj.em_Company_name = dr["company_name"].ToString();
                            obj.em_Dept_code = dr["em_dept_code"].ToString();
                            obj.em_Dept_name = dr["dept_name"].ToString();
                            obj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            obj.em_Grade_code = dr["em_grade_code"].ToString();
                            obj.em_Grade_name = dr["grade_name"].ToString();
                            obj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            obj.em_Desg_code = dr["em_desg_code"].ToString();
                            obj.em_desg_effect_from = db.UIDDMMYYYYformat(dr["em_desg_effect_from"].ToString());
                            obj.em_Desg_name = dr["designation_name"].ToString();
                            obj.em_Staff_type_code = dr["em_staff_type"].ToString();
                            obj.em_Staff_type_name = dr["em_staff_type_name"].ToString();
                            obj.em_staff_type_effect_from = db.UIDDMMYYYYformat(dr["em_staff_type_effect_from"].ToString());
                            obj.em_first_name = dr["em_first_name"].ToString();
                            obj.em_middle_name = dr["em_middle_name"].ToString();
                            obj.em_last_name = dr["em_last_name"].ToString();
                            obj.em_family_name = dr["em_family_name"].ToString();
                            obj.em_full_name = dr["em_full_name"].ToString();
                            obj.em_company_effect_from = db.UIDDMMYYYYformat(dr["em_company_effect_from"].ToString());
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);

            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
        }

        [Route("TransEmpUpdate")]
        public HttpResponseMessage TransEmpUpdate(List<TrnEmp> data)
        {

        

            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (TrnEmp simsobj in data)
                    {
                        if (simsobj.em_dept_effect_from == "undefined")
                        {
                            simsobj.em_dept_effect_from = null;
                        }
                        if (simsobj.em_grade_effect_from == "undefined")
                        {
                            simsobj.em_grade_effect_from = null;
                        }
                        if (simsobj.em_desg_effect_from == "undefined")
                        {
                            simsobj.em_desg_effect_from = null;
                        }
                        if (simsobj.em_staff_type_effect_from == "undefined")
                        {
                            simsobj.em_staff_type_effect_from = null;
                        }
                        if (simsobj.em_company_effect_from == "undefined")
                        {
                            simsobj.em_company_effect_from = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_transfer_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","U"),
                                new SqlParameter("@em_company_code",simsobj.em_Company_Code),
                                new SqlParameter("@em_login_code",simsobj.em_login_code),
                                new SqlParameter("@em_number",simsobj.em_number),
                                new SqlParameter("@em_dept_code",simsobj.em_Dept_code),
                                new SqlParameter("@em_dept_effect_from",db.DBYYYYMMDDformat(simsobj.em_dept_effect_from)),
                                new SqlParameter("@em_grade_code",simsobj.em_Grade_code),
                                new SqlParameter("@em_grade_effect_from", db.DBYYYYMMDDformat(simsobj.em_grade_effect_from)),
                                new SqlParameter("@em_desg_code",simsobj.em_Desg_code),
                                new SqlParameter("@em_desg_effect_from",db.DBYYYYMMDDformat(simsobj.em_desg_effect_from)),
                                new SqlParameter("@em_staff_type_code", simsobj.em_Staff_type_code),
                                new SqlParameter("@em_staff_type_effect_from", db.DBYYYYMMDDformat(simsobj.em_staff_type_effect_from)),
                                new SqlParameter("@em_company_effect_from", db.DBYYYYMMDDformat(simsobj.em_company_effect_from))
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
       
        [Route("getCompany")]
        public HttpResponseMessage getCompany()
        {

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'A'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_Company_Code = dr["co_company_code"].ToString();
                            obj.em_Company_name = dr["co_desc"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getDepartment")]
        public HttpResponseMessage getDepartment()
        {

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'B'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_Dept_code = dr["codp_dept_no"].ToString();
                            obj.em_Dept_name = dr["codp_dept_name"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getGrade")]
        public HttpResponseMessage getGrade()
        {

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'C'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_Grade_code = dr["gr_code"].ToString();
                            obj.em_Grade_name = dr["gr_desc"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getDesignation")]
        public HttpResponseMessage getDesignation()
        {

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'D'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_Desg_code = dr["dg_code"].ToString();
                            obj.em_Desg_name = dr["dg_desc"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getStaffType")]
        public HttpResponseMessage getStaffType()
        {

            List<TrnEmp> code_list = new List<TrnEmp>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_transfer_proc]",

                      new List<SqlParameter>()
                      {
                           new SqlParameter("@opr",'E'),
                         
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TrnEmp obj = new TrnEmp();
                            obj.em_Staff_type_code = dr["sims_appl_parameter"].ToString();
                            obj.em_Staff_type_name = dr["sims_appl_form_field_value1"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        
    }
}





















