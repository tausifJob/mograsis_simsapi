﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/OtherLoan")]
    public class OtherLoanController : ApiController
    {

        [Route("getComapany")]
        public HttpResponseMessage getComapany()
        {
            List<Pers061> lstModules = new List<Pers061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_grade_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers061 sequence = new Pers061();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_short_name = dr["comp_short_name"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getEmployee")]
        public HttpResponseMessage getEmployee()
        {
            List<Pers061> lstModules = new List<Pers061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_other_loan_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers061 sequence = new Pers061();
                            sequence.em_number = dr["em_number"].ToString();
                            sequence.em_name_ot = dr["em_name_ot"].ToString();
                            sequence.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            sequence.fullname = dr["fullname"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getLedger")]
        public HttpResponseMessage getLedger()
        {
            List<Pers061> lstModules = new List<Pers061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_other_loan_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "L"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers061 sequence = new Pers061();
                            sequence.sllc_ldgr_code = dr["sllc_ldgr_code"].ToString();
                            sequence.sllc_ldgr_name = dr["sllc_ldgr_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getDescription")]
        public HttpResponseMessage getDescription()
        {
            List<Pers061> lstModules = new List<Pers061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_other_loan_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Q"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers061 sequence = new Pers061();
                            sequence.lc_code = dr["lc_code"].ToString();
                            sequence.lc_desc = dr["lc_desc"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getOtherLoanDetail")]
        public HttpResponseMessage getOtherLoanDetail()
        {
            List<Pers061> OtherLoan = new List<Pers061>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_other_loan_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers061 obj = new Pers061();


                            obj.CompanyName = dr["CompanyName"].ToString();
                            obj.EMPNAME = dr["EMPNAME"].ToString();
                            obj.LoanName = dr["LoanName"].ToString();
                            obj.LedgerName = dr["LedgerName"].ToString();
                            obj.ol_account_number = dr["ol_account_number"].ToString();
                            OtherLoan.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, OtherLoan);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, OtherLoan);
        }

        [Route("OtherLoanCUD")]
        public HttpResponseMessage OtherLoanCUD(Pers061 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_other_loan_proc]",
                        new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@LoanNumber", simsobj.LoanNumber),
                                 new SqlParameter("@CompanyCode", simsobj.CompanyName),
                                 new SqlParameter("@LoanCode", simsobj.LoanName),
                                 new SqlParameter("@LedgerCode", simsobj.LedgerName),
                                 new SqlParameter("@AccountNo", simsobj.ol_account_number)

                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}