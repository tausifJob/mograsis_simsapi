﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS;
using System.Threading;
using System.Net.Mail;
using System.Text;
using System.IO;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/employeeOfferLetter")]

    public class EmployeeOfferLetterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getEmployeeApplicationDetails")]
        public HttpResponseMessage getEmployeeApplicationDetails(string pays_vacancy_id, string app_from_date,string app_to_date)
        {
            List<Pers233> pay_list = new List<Pers233>();

            try
            {

                if (pays_vacancy_id == "undefined" || pays_vacancy_id == "")
                    pays_vacancy_id = null;
                if (app_from_date == "undefined" || app_from_date == "")
                    app_from_date = null;
                if (app_to_date == "undefined" || app_to_date == "")
                    app_to_date = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@pays_vacancy_id", pays_vacancy_id),
                            new SqlParameter("@app_from_date", db.DBYYYYMMDDformat(app_from_date)),
                            new SqlParameter("@app_to_date", db.DBYYYYMMDDformat(app_to_date))
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers233 emOff = new Pers233();

                            emOff.em_sr_no = dr["em_sr_no"].ToString();
                            emOff.em_applicant_id = dr["em_applicant_id"].ToString();
                            emOff.em_vacancy_id = dr["em_vacancy_id"].ToString();
                            emOff.emp_name = dr["emp_name"].ToString();
                            emOff.em_desg_code = dr["em_desg_code"].ToString();
                            emOff.dg_desc = dr["dg_desc"].ToString();
                            emOff.em_date_of_birth =  db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            emOff.em_application_date = db.UIDDMMYYYYformat(dr["em_application_date"].ToString());
                            emOff.em_expected_date_of_join = db.UIDDMMYYYYformat(dr["em_expected_date_of_join"].ToString());
                            emOff.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            emOff.em_email = dr["em_email"].ToString();
                            emOff.em_dest_code = dr["em_dest_code"].ToString();
                            emOff.em_staff_type = dr["em_staff_type"].ToString();
                            emOff.em_grade_code = dr["em_grade_code"].ToString();
                            emOff.em_agreement = dr["em_agreement"].ToString();
                            emOff.em_agreement_start_date =  db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());
                            emOff.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());
                            emOff.em_visa_type = dr["em_visa_type"].ToString();
                            emOff.en_labour_card_no = dr["en_labour_card_no"].ToString();
                            emOff.offer_letter_doc = dr["offer_letter_doc"].ToString();
                            emOff.em_offer_letter_accepted_flag = dr["em_offer_letter_accepted_flag"].ToString();
                            emOff.em_offer_letter_emailsend_flag = dr["em_offer_letter_emailsend_flag"].ToString();
                            emOff.em_offer_letter_show_flag = dr["em_offer_letter_show_flag"].Equals("Y") ? true : false;
                          emOff.em_probation_period_days=dr["em_probation_period_days"].ToString();
                          emOff.em_probation_completion_date = db.UIDDMMYYYYformat(dr["em_probation_completion_date"].ToString());
                          emOff.em_probation_confirmation_date = db.UIDDMMYYYYformat(dr["em_probation_confirmation_date"].ToString());
                         
                            pay_list.Add(emOff);
                        }                        
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, pay_list);
        }
        
        [Route("getOfferLetterTemplate")]
        public HttpResponseMessage getOfferLetterTemplate()
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'A')                            
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_subject = dr["sims_msg_subject"].ToString();
                            simsobj.sims_msg_body = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, simsobj);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
        }


        [Route("SendOfferEmail")]
        public HttpResponseMessage SendOfferEmail(string filenames, List<Pers233> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool inserted = false;

            List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            //List<comn_email_attachments> data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(emailList[1].ToString());
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;
                    
                    string attachments = string.Empty;
                    //foreach (comn_email_attachments att in data)
                    //{
                    //    attachments = attachments + ',' + att.attFilename;
                    //}
                    //if (!string.IsNullOrEmpty(attachments))
                    //{
                    //    attachments = attachments.Substring(1);
                    //}
                    
                    foreach (Pers233 email in emailList)
                    {
                          SqlDataReader dr1 = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                          new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_recepient_id", email.emailsendto),
                            new SqlParameter("@sims_email_message", email.body),
                            new SqlParameter("@sims_email_subject", email.subject),                            
                            new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            //new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@em_applicant_id", email.em_applicant_id),
                            new SqlParameter("@dg_desc", email.dg_desc),                                                                                 
                            new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_cc_id", email.cc_id),
                            //new SqlParameter("@sims_email_schedule_date", System.DateTime.Now)                            
                        });

                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                            
                       dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getEmployeeApplicationDetailsForConfirm")]
        public HttpResponseMessage getEmployeeApplicationDetailsForConfirm(string desg_code, string app_from_date, string app_to_date)
        {
            List<Pers233> pay_list = new List<Pers233>();

            try
            {

                if (desg_code == "undefined" || desg_code == "")
                    desg_code = null;
                if (app_from_date == "undefined" || app_from_date == "")
                    app_from_date = null;
                if (app_to_date == "undefined" || app_to_date == "")
                    app_to_date = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@desg_code", desg_code),
                            new SqlParameter("@app_from_date", db.DBYYYYMMDDformat(app_from_date)),
                            new SqlParameter("@app_to_date", db.DBYYYYMMDDformat(app_to_date))
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers233 emOff = new Pers233();

                            emOff.em_sr_no = dr["em_sr_no"].ToString();
                            emOff.em_applicant_id = dr["em_applicant_id"].ToString();
                            emOff.em_vacancy_id = dr["em_vacancy_id"].ToString();

                            emOff.em_company_code = dr["em_company_code"].ToString();
                            emOff.em_dept_code = dr["em_dept_code"].ToString();
                            emOff.em_desg_code = dr["em_desg_code"].ToString();
                            emOff.em_nation_code = dr["em_nation_code"].ToString();
                            emOff.em_salutation = dr["em_salutation"].ToString();
                            emOff.em_first_name = dr["em_first_name"].ToString();
                            emOff.em_middle_name = dr["em_middle_name"].ToString();
                            emOff.em_last_name = dr["em_last_name"].ToString();
                            emOff.em_family_name = dr["em_family_name"].ToString();
                            emOff.em_name_ot = dr["em_name_ot"].ToString();
                            emOff.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            emOff.em_sex = dr["em_sex"].ToString();
                            emOff.em_marital_status = dr["em_marital_status"].ToString();
                            emOff.em_religion_code = dr["em_religion_code"].ToString();
                            emOff.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            emOff.em_appartment_number = dr["em_appartment_number"].ToString();
                            emOff.em_building_number = dr["em_building_number"].ToString();
                            emOff.em_street_number = dr["em_street_number"].ToString();
                            emOff.em_area_number = dr["em_area_number"].ToString();
                            emOff.em_summary_address = dr["em_summary_address"].ToString();
                            emOff.em_city = dr["em_city"].ToString();
                            emOff.em_state = dr["em_state"].ToString();
                            emOff.em_country_code = dr["em_country_code"].ToString();
                            emOff.em_phone = dr["em_phone"].ToString();
                            emOff.em_mobile = dr["em_mobile"].ToString();
                            emOff.em_email = dr["em_email"].ToString();                            
                            emOff.em_fax = dr["em_fax"].ToString();
                            emOff.em_po_box = dr["em_po_box"].ToString();
                            emOff.em_passport_number = dr["em_passport_number"].ToString();
                            emOff.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());
                            emOff.em_passport_expiry_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());
                            emOff.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            emOff.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            emOff.em_visa_number = dr["em_visa_number"].ToString();
                            emOff.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());
                            emOff.em_visa_expiry_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            emOff.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            emOff.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            emOff.em_visa_type = dr["em_visa_type"].ToString();
                            emOff.em_national_id = dr["em_national_id"].ToString();
                            emOff.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());
                            emOff.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());
                            emOff.em_pan_no = dr["em_pan_no"].ToString();
                            emOff.en_labour_card_no = dr["en_labour_card_no"].ToString();
                            emOff.em_img = dr["em_img"].ToString();
                            emOff.em_social_address = dr["em_social_address"].ToString();
                            emOff.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            emOff.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            emOff.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            emOff.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            emOff.em_expected_date_of_join = db.UIDDMMYYYYformat(dr["em_expected_date_of_join"].ToString());
                            emOff.em_joining_ref = dr["em_joining_ref"].ToString();
                            emOff.em_handicap_status = dr["em_handicap_status"].ToString();
                            emOff.em_blood_group_code = dr["em_blood_group_code"].ToString();
                            emOff.em_doc = dr["em_doc"].ToString();
                            emOff.em_application_status = dr["em_application_status"].ToString();
                            emOff.em_password = dr["em_password"].ToString();
                            emOff.em_application_date = db.UIDDMMYYYYformat(dr["em_application_date"].ToString());
                            emOff.em_user_id = dr["em_user_id"].ToString();
                            emOff.em_doc_path = dr["em_doc_path"].ToString();
                            emOff.em_dest_code = dr["em_dest_code"].ToString();
                            emOff.em_staff_type = dr["em_staff_type"].ToString();
                            emOff.em_grade_code = dr["em_grade_code"].ToString();
                            emOff.em_agreement = dr["em_agreement"].ToString();
                            emOff.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());
                            emOff.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());
                            emOff.offer_letter_doc = dr["offer_letter_doc"].ToString();
                            emOff.em_offer_letter_accepted_flag = dr["em_offer_letter_accepted_flag"].ToString();
                            emOff.em_offer_letter_show_flag = (dr["em_offer_letter_show_flag"].ToString()).Equals("Y") ? true : false;
                            emOff.em_offer_letter_emailsend_flag = dr["em_offer_letter_emailsend_flag"].ToString();
                            emOff.pays_doc_recruitment_status = dr["pays_doc_recruitment_status"].ToString();
                            emOff.dg_desc = dr["dg_desc"].ToString();
                            emOff.emp_name = dr["emp_name"].ToString();                            
                            emOff.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            emOff.em_offer_letter_send_date = db.UIDDMMYYYYformat(dr["em_offer_letter_send_date"].ToString());
                            emOff.em_probation_period_days = dr["em_probation_period_days"].ToString();
                            emOff.em_probation_completion_date = db.UIDDMMYYYYformat(dr["em_probation_completion_date"].ToString());
                            emOff.em_probation_confirmation_date = db.UIDDMMYYYYformat(dr["em_probation_confirmation_date"].ToString());

                            pay_list.Add(emOff);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, pay_list);
        }

        [Route("ConfirmEmployee")]
        public HttpResponseMessage ConfirmEmployee(List<Pers233> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ConfirmEmployee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "ConfirmEmployee"));

            //Pers046 persobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Pers046>(data);
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers233 hrmsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                            new List<SqlParameter>()
                            {
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@em_applicant_id", hrmsobj.em_applicant_id)                                    
                            });
                            if(dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }

                            dr.Close();
                        }
                    }
                }
               
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("OfferLetterUpdate")]
        public HttpResponseMessage OfferLetterUpdate(Pers233 data)
        {
            Pers233 simsobj = data;
            string empid = "";
            string imgpath = string.Empty;
            bool flag = false;
            Message message = new Message();

            try
            {             
                if (simsobj != null)
                {                    
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                            new List<SqlParameter>()
                            {
                                    new SqlParameter("@opr",simsobj.opr),
                                    new SqlParameter("@em_applicant_id",simsobj.em_applicant_id),                                    
                                    new SqlParameter("@em_dest_code", simsobj.em_dest_code),                                                                     
                                    new SqlParameter("@em_desg_code", simsobj.em_desg_code),                                    
                                    new SqlParameter("@em_grade_code", simsobj.em_grade_code),
                                    new SqlParameter("@em_staff_type", simsobj.em_staff_type),
                                    new SqlParameter("@em_visa_type", simsobj.em_visa_type),
                                    //new SqlParameter("@em_agreement",simsobj.em_agreement),
                                    new SqlParameter("@em_agreement_start_date", db.DBYYYYMMDDformat(simsobj.em_agreement_start_date)),
                                    new SqlParameter("@em_agreement_exp_date", db.DBYYYYMMDDformat(simsobj.em_agreement_exp_date)),                                    
                                    new SqlParameter("@em_labour_card_no",simsobj.en_labour_card_no),
                                    new SqlParameter("@offer_letter_doc",simsobj.offer_letter_doc),
                                    new SqlParameter("@em_offer_letter_show_flag",simsobj.em_offer_letter_show_flag.Equals(true)?"Y":"N"),
                                   new SqlParameter("@em_probation_period_days",simsobj.em_probation_period_days),
                                   new SqlParameter("@em_probation_completion_date",db.DBYYYYMMDDformat(simsobj.em_probation_completion_date)),
                                   new SqlParameter("@em_probation_confirmation_date",db.DBYYYYMMDDformat(simsobj.em_probation_confirmation_date))
                           });

                        if (dr.RecordsAffected > 0)
                        {
                            flag = true;                            
                        }
                        else
                        { flag = false; }
                    }                                       
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {                
               message.strMessage = "Error In Updating Employee Details" + x.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);

        }


        [Route("SendOfferLetterEmail")]
        public HttpResponseMessage SendOfferLetterEmail(List<Pers233> emailList)
        {
            Sims010_Auth simsobj = new Sims010_Auth();
            bool inserted = false;

            //List<comn_email_attachments> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(filenames);
            //List<comn_email_attachments> data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<comn_email_attachments>>(emailList[1].ToString());
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    Message msg = new Message();
                    string st = string.Empty;

                    string attachments = string.Empty;
                    //foreach (comn_email_attachments att in data)
                    //{
                    //    attachments = attachments + ',' + att.attFilename;
                    //}
                    //if (!string.IsNullOrEmpty(attachments))
                    //{
                    //    attachments = attachments.Substring(1);
                    //}

                    foreach (Pers233 email in emailList)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                        new List<SqlParameter>()
                      {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_recepient_id", email.emailsendto),
                            //new SqlParameter("@sims_email_message", email.body),
                            //new SqlParameter("@sims_email_subject", email.subject),
                            //new SqlParameter("@sims_sender_email_id",email.sender_emailid),
                            //new SqlParameter("@sims_email_date", System.DateTime.Now),
                            new SqlParameter("@em_applicant_id", email.em_applicant_id),
                            new SqlParameter("@dg_desc", email.dg_desc),
                            //new SqlParameter("@sims_email_attachment", attachments),
                            new SqlParameter("@sims_cc_id", email.cc_id),
                            new SqlParameter("@offer_letter_path", email.offer_letter_path)
                            //new SqlParameter("@sims_email_schedule_date", System.DateTime.Now)                            
                        });

                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getVacancy")]
        public HttpResponseMessage getVacancy()
        {
            List <Pers233> simslist = new List<Pers233>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_offer_letter_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'F')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers233 simsobj = new Pers233();
                            simsobj.pays_sr_no = dr["pays_sr_no"].ToString();
                            simsobj.pays_vacancy_id = dr["pays_vacancy_id"].ToString();
                            simsobj.pays_vacancy_desc = dr["pays_vacancy_desc"].ToString();
                            simsobj.pays_vacancy_roles = dr["pays_vacancy_roles"].ToString();
                            simslist.Add(simsobj);                            
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, simslist);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, simslist);                   
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


    }
}