﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.AttendanceClass;
using System.Globalization;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.ViewPayslipController
{
    [RoutePrefix("api/common/ViewPayslip")]
    [BasicAuthentication]
    public class ViewPayslipController : ApiController
    {
        //API For ViewPaySlip
        [Route("Getallperioddata")]
        public HttpResponseMessage Getallperioddata()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetCountryCountTotal(),PARAMETERS :: NO";

            List<Pe078V> dashboard_list = new List<Pe078V>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_parameter_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'J'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pe078V simsobj = new Pe078V();

                            simsobj.invs_appl_parameter = dr["invs_appl_parameter"].ToString();
                            simsobj.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();

                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }
        
        [Route("GetReportName")]
        public HttpResponseMessage GetReportName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetReportName(),PARAMETERS :: NO";

            List<Pe078V> dashboard_list = new List<Pe078V>();

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[get_fins_report_url]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'J'),
                           new SqlParameter("@sub_opr", "Payroll"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pe078V simsobj = new Pe078V();

                            simsobj.fins_appl_form_field_value1 = dr["fins_appl_form_field_value1"].ToString();

                            dashboard_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, dashboard_list);
        }
        
        [Route("getCurrentYear")]
        public HttpResponseMessage getCurrentYear()
        {
            List<Pe078V> mod_list = new List<Pe078V>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'G'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pe078V simsobj = new Pe078V();
                            simsobj.year1 = dr["fins_appl_form_field_value1"].ToString();

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }


        [Route("getCurrentYearwithMonth")]
        public HttpResponseMessage getCurrentYearwithMonth(string year)
        {
            List<Pe078V> mod_list = new List<Pe078V>();

            Message message = new Message();

            try
            {
                List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();

                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'G'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pe078V simsobj = new Pe078V();
                            simsobj.year1 = dr["fins_appl_form_field_value1"].ToString();

                            studentmonthlyattendance obj = new studentmonthlyattendance();
                            obj.sims_month_no = simsobj.year1;
                            obj.sims_month_name = string.Format("{0}-{1}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt16(obj.sims_month_no)), year);
                            
                            list.Add(obj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

        }

        [Route("Year_month")]
        public HttpResponseMessage Year_month(string year)
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M"),
                            new SqlParameter("@year1",year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.year_month = dr["year_month"].ToString();
                            simsobj.year_month_name = dr["year_month_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

    }
}