﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.DesignationController
{
    [RoutePrefix("api/common/Designation")]
    [BasicAuthentication]
    public class DesignationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getDesig")]
        public HttpResponseMessage getDesig()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllComnUserRole(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllComnUserRole"));

            List<Per234> desg_list = new List<Per234>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_designation_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per234 simsobj = new Per234();
                            simsobj.desg_company_code = dr["co_company_code"].ToString();
                            simsobj.desg_company_code_name = dr["co_desc"].ToString();
                            simsobj.desg_code = dr["dg_code"].ToString();
                            simsobj.desg_desc = dr["dg_desc"].ToString();
                            simsobj.desg_communication_status = dr["dg_communication_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanyName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCompanyName"));

            List<Per234> mod_list = new List<Per234>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_designation_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per234 simsobj = new Per234();
                            simsobj.desg_company_code = dr["co_company_code"].ToString();
                            simsobj.desg_company_code_name = dr["co_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        //[Route("getAutoGenerateCode")]
        //public HttpResponseMessage getAutoGenerateCode()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateCode()PARAMETERS ::NA";
        //    Log.Debug(string.Format(debug, "ERP/Inventory/", "getAutoGenerateCode"));

        //    List<Per234> mod_list = new List<Per234>();

        //    Message message = new Message();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_designation_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", 'R'),

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Per234 simsobj = new Per234();
        //                    simsobj.desg_code = dr["Desg_code"].ToString();
        //                    mod_list.Add(simsobj);

        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        //            }
        //            else
        //                return Request.CreateResponse(HttpStatusCode.OK, dr);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        message.strMessage = "No Records Found";
        //        message.messageType = MessageType.Error;
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //    }



        //    //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        //}

        [Route("CUDDesignation")]
        public HttpResponseMessage CUDDesignation(List<Per234> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDesignation(),PARAMETERS";
            bool inserted = false;


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per234 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_designation_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@dg_company_code", simsobj.desg_company_code),
                                  new SqlParameter("@dg_code", simsobj.desg_code),
                                new SqlParameter("@dg_code_list", simsobj.desg_code),
                                new SqlParameter("@dg_desc", simsobj.desg_desc),
                                new SqlParameter("@dg_communication_status", simsobj.desg_communication_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
