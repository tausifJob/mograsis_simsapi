﻿using log4net;
using SIMSAPI.Helper;

using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmployeeSettlement")]

    public class EmployeeFinalSettlementController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("GetPayrollGeneratedMonth")]
        public HttpResponseMessage GetPayrollGeneratedMonth(string em_number)
        {
            List<Pers306> emp_list = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_settlement_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'S'),
                        new SqlParameter("@em_number",em_number)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 invsobj = new Pers306();
                            invsobj.pe_year_month = dr["pe_year_month"].ToString();
                            invsobj.pe_number = dr["pe_number"].ToString();
                            invsobj.pe_status = dr["pe_status"].ToString();                            
                            emp_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK,x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("GetSalaryDetails")]
        public HttpResponseMessage GetSalaryDetails(string em_number)
        {
            List<Pers306> emp_list = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_settlement_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'A'),
                        new SqlParameter("@em_number",em_number)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 invsobj = new Pers306();
                            invsobj.sd_year_month = dr["sd_year_month"].ToString();
                            invsobj.sd_year = dr["sd_year"].ToString();
                            invsobj.earning_amount = dr["earning_amount"].ToString();
                            invsobj.deduction_amount = dr["deduction_amount"].ToString();
                            invsobj.net_amount = dr["net_amount"].ToString();
                            invsobj.sd_number = dr["sd_number"].ToString();
                            invsobj.sd_year_month_no = dr["sd_year_month_no"].ToString();
                            emp_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }


        [Route("GetGratuityByEmployee")]
        public HttpResponseMessage GetGratuityByEmployee(string em_number)
        {
            List<Pers306> emp_list = new List<Pers306>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_settlement_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'B'),
                        new SqlParameter("@em_number",em_number)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 invsobj = new Pers306();
                            invsobj.grt_amount = dr["grt_amount"].ToString();
                            invsobj.grt_applicable_days = dr["grt_applicable_days"].ToString();
                            invsobj.grt_working_days = dr["grt_working_days"].ToString();                            
                            emp_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }


        [Route("SubmitSettlement")]
        public HttpResponseMessage SubmitSettlement(Pers306 data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        //int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_gratuity_empl_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_settlement_proc]",

                        new List<SqlParameter>()
                          {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@reg_date", db.DBYYYYMMDDformat(data.reg_date)),                            
                            new SqlParameter("@em_number",data.em_number),
                            new SqlParameter("@em_company_code",data.com_code),
                            new SqlParameter("@grt_amount",data.grt_amount),
                            new SqlParameter("@leave_salary",data.leave_salary),
                            new SqlParameter("@air_fare",data.airfare),
                            new SqlParameter("@net_payable",data.netPayable),
                            new SqlParameter("@grt_applicable_days",data.grt_applicable_days),
                            new SqlParameter("@grt_working_days",data.grt_working_days),
                            new SqlParameter("@totalEarning",data.totalEarning),
                            new SqlParameter("@totalDeduction",data.totalDeduction)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            message.strMessage = "Employee Settlement Save Successfully";
                        }
                        else
                        {
                            inserted = false;
                            message.strMessage = "You can't update.\n Employee settlement already added";
                        }
                        dr.Close();
                    }
                }                
                else
                {
                    message.strMessage = "Parmeter error!! \n Please check some parameters are missing.";
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,message);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }            
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


    }
}