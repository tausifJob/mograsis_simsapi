﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LoanCodeDetails")]
    public class LoanCodeController:ApiController
    {

        [Route("getAllLoanCode")]
        public HttpResponseMessage getAllLoanCode()
        {
            List<Per131> loan_list = new List<Per131>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_loan_code",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per131 obj = new Per131();

                            obj.lc_company_code = dr["lc_company_code"].ToString();
                            obj.CompanyName = dr["CompanyName"].ToString();

                            obj.lc_code = dr["lc_code"].ToString();
                            obj.lc_desc = dr["lc_desc"].ToString();

                            obj.lc_ac_type = dr["lc_ac_type"].ToString();

                            obj.lc_ledger_code = dr["lc_ledger_code"].ToString();
                            obj.ledgerName = dr["ledgerName"].ToString();

                            obj.lc_debit_acno = dr["lc_debit_acno"].ToString();
                            obj.lc_debit_acno_nm = dr["lc_debit_acno_nm"].ToString();
                            loan_list.Add(obj);
                            
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loan_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loan_list);
        }

        [Route("LoanCodeCUD")]
        public HttpResponseMessage LoanCodeCUD(Per131 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LoanCodeCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("pays_loan_code",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@COMPANY_CODE",simsobj.lc_company_code),
                          new SqlParameter("@LOAN_CODE", simsobj.lc_code),
                          new SqlParameter("@LOAN_DESC", simsobj.lc_desc),
                          new SqlParameter("@LEDGER_CODE",simsobj.lc_ledger_code),
                          new SqlParameter("@DEBIT_ACC_NO", simsobj.lc_debit_acno),
                          new SqlParameter("@LOAN_ACC_TYPE", simsobj.lc_ac_type),
                        });
                    
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}