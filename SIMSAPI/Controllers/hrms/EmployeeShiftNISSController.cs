﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;



namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/EmployeeShiftNISS")]
    public class EmployeeShiftController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetDepartmentName")]
        public HttpResponseMessage GetDepartmentName(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDepartmentName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "GetDepartmentName"));

            List<Pers099> list = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),  
                            new SqlParameter("@gc_company_code", comp_code),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 hrmsobj = new Pers099();
                            hrmsobj.code = dr["codp_dept_no"].ToString();
                            hrmsobj.em_dept_name = dr["codp_dept_name"].ToString();
                            list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GetDesignationName")]
        public HttpResponseMessage GetDesignationName(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDesignationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "GetDesignationName"));

            List<Pers099> list = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),  
                            new SqlParameter("@gc_company_code", comp_code),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 simsobj = new Pers099();
                            simsobj.em_desg_code = dr["dg_code"].ToString();
                            simsobj.em_desg_desc = dr["dg_desc"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_gc_grade_code")]
        public HttpResponseMessage Get_gc_grade_code()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_gc_grade_code(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_gc_grade_code"));

            List<grade_change> gc_gradecode = new List<grade_change>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            grade_change objph = new grade_change();
                            objph.gc_grade_code = dr["gr_code"].ToString();
                            objph.gc_new_grade = dr["gr_desc"].ToString();
                            gc_gradecode.Add(objph);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, gc_gradecode);

            }
            return Request.CreateResponse(HttpStatusCode.OK, gc_gradecode);
        }

        [Route("Get_gc_emnumber")]
        public HttpResponseMessage Get_gc_emnumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_gc_emnumber(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_gc_emnumber"));

            List<grade_change> gcnumber = new List<grade_change>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            grade_change objph = new grade_change();
                            objph.gc_number = dr["em_number"].ToString();
                            objph.first_name = dr["em_first_name"].ToString();
                            gcnumber.Add(objph);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, gcnumber);

            }
            return Request.CreateResponse(HttpStatusCode.OK, gcnumber);
        }

        [Route("Get_gc_getgrade")]
        public HttpResponseMessage Get_gc_getgrade(string emp_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_gc_getgrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_gc_getgrade"));

            List<grade_change> ot_empid = new List<grade_change>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),  
                            new SqlParameter("@gc_number",emp_no),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            grade_change deptobj = new grade_change();
                            //deptobj.gc_effect_from = DateTime.Parse(dr["em_grade_effect_from"].ToString());
                            deptobj.gc_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            deptobj.gc_old_grade = dr["eg_grade_desc"].ToString();
                            ot_empid.Add(deptobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, ot_empid);

            }
            return Request.CreateResponse(HttpStatusCode.OK, ot_empid);
        }

        [Route("Getall_grade_change")]
        public HttpResponseMessage Getall_grade_change()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getall_grade_change(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Getall_grade_change"));

            List<grade_change> pay_list = new List<grade_change>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_grade_change]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            grade_change pay = new grade_change();
                            pay.gc_number = dr["gc_number"].ToString();
                            pay.gc_company_code = dr["gc_company_code"].ToString();
                            if (dr["gc_mgr_confirm_tag"].ToString().Equals("Y"))
                            {
                                pay.gc_mgr_confirm_tag = true;
                            }
                            else
                            {
                                pay.gc_mgr_confirm_tag = false;
                            }

                            pay.gc_old_grade = dr["gc_old_grade"].ToString();
                            pay.gc_new_grade = dr["gc_new_grade"].ToString();
                            //pay.gc_effect_from = DateTime.Parse(dr["gc_effect_from"].ToString());
                            pay.gc_effect_from = db.UIDDMMYYYYformat(dr["gc_effect_from"].ToString());
                            pay.gc_retro_effect_from = db.UIDDMMYYYYformat(dr["gc_retro_effect_from"].ToString());
                            pay_list.Add(pay);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, pay_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, pay_list);
        }

        [Route("CUDgrade_change")]
        public HttpResponseMessage CUDgrade_change(List<grade_change> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDgrade_change(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "grade", "grade_change"));

            Message message = new Message();
            bool inserted = false;

            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (grade_change hrmsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_grade_change]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", hrmsobj.opr),
                                new SqlParameter("@gc_number", hrmsobj.gc_number),
                                new SqlParameter("@gc_company_code", hrmsobj.gc_company_code),
                                new SqlParameter("@gc_mgr_confirm_tag", hrmsobj.gc_mgr_confirm_tag == true? "Y":"N"),
                                new SqlParameter("@gc_old_grade", hrmsobj.gc_old_grade),
                                new SqlParameter("@gc_new_grade", hrmsobj.gc_new_grade),
                                new SqlParameter("@gc_effect_from", db.DBYYYYMMDDformat(hrmsobj.gc_effect_from)),
                                new SqlParameter("@gc_retro_effect_from", db.DBYYYYMMDDformat(hrmsobj.gc_retro_effect_from))
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllEmployeeName")]
        public HttpResponseMessage GetAllEmployeeName(string comp_code, string dept_code, string desg_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllEmployeeName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "GetAllEmployeeName"));

            List<HrmsClass> list = new List<HrmsClass>();
            if (comp_code == "undefined")
            {
                comp_code = "";
            }
            if (dept_code == "undefined")
            {
                dept_code = "";
            }
            if (desg_code == "undefined")
            {
                desg_code = "";
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),  
                            new SqlParameter("@em_company_code", comp_code),  
                            new SqlParameter("@em_dept_code", dept_code),  
                            new SqlParameter("@em_desg_code", desg_code),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.em_number = dr["em_number"].ToString();
                            hrmsobj.sh_emp_name = dr["em_first_name"].ToString() + " " + dr["em_middle_name"].ToString() + " " + dr["em_last_name"].ToString();
                            hrmsobj.sh_template_id = dr["sh_template_id"].ToString();
                            hrmsobj.sh_template_name = dr["sh_template_name"].ToString();
                            hrmsobj.em_img = dr["em_img"].ToString();
                            hrmsobj.em_dept_code = dr["em_dept_code"].ToString();
                            if (dr["start_date"].ToString() == "")
                                hrmsobj.sh_start_date = null;
                            else
                            {
                                hrmsobj.sh_start_date = db.UIDDMMYYYYformat(dr["start_date"].ToString());

                                // hrmsobj.sh_start_date = Convert.ToDateTime(dr["start_date"].ToString());
                                //DateTime str = Convert.ToDateTime(dr["start_date"].ToString());
                                //int v = str.ToString().IndexOf(" ");
                                //hrmsobj.sh_start_date = Convert.ToDateTime(str.ToString().Substring(0, v));
                                //hrmsobj.sh_start_date = Convert.ToDateTime(hrmsobj.sh_start_date.ToString().Substring(0, v));
                            }
                            if (dr["sh_shift_status"].ToString() == "A")
                                hrmsobj.sh_shift_status = true;
                            else
                                hrmsobj.sh_shift_status = false;

                            try
                            {
                                hrmsobj.sh_start_fromdate = db.UIDDMMYYYYformat(dr["sh_attendence_from_date"].ToString());
                            }
                            catch (Exception ex)
                            { }
                            list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getCheckEmployee")]
        public HttpResponseMessage getCheckEmployee(string em_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCheckEmployee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getCheckEmployee"));
            bool flag = false;
            List<Pers099> list = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),  
                            new SqlParameter("@sh_emp_id", em_number),  
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            flag = true;
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, flag);

            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);
        }

        //[Route("CUDInsertEmployeeShift")]
        //public HttpResponseMessage CUDInsertEmployeeShift(List<HrmsClass> data)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertEmployeeShift(),PARAMETERS :: No";
        //    Log.Debug(string.Format(debug, "grade", "CUDInsertEmployeeShift"));
        //    string year = null, shift_id = null;
        //    DateTime? end_date = null, i = null;
        //    SqlDataReader dr;
        //    Message message = new Message();
        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (HrmsClass hrmsobj in data)
        //            {
        //                dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                   new List<SqlParameter>() 
        //                { 
        //                   new SqlParameter("@opr", "B"),  
        //                });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        year = dr["sims_financial_year"].ToString();
        //                        //end_date = dr["sims_financial_year_end_date"].ToString();
        //                        end_date = Convert.ToDateTime(dr["sims_financial_year_end_date"].ToString());
        //                    }
        //                }
        //                dr.Close();
        //                i = hrmsobj.sh_start_date;
        //                //while (DateTime.Parse(i) <= DateTime.Parse(end_date))
        //                while (i <= end_date)
        //                {
        //                    string j =i.Value.DayOfWeek.ToString();
        //                    shift_id = null;
        //                    switch (j)
        //                    {
        //                        case "Sunday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day1), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Monday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day2), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Tuesday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day3), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Wednesday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day4), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Thursday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day5), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Friday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day6), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Saturday":
        //                            dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day7), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                    }

        //                    int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_shift_proc]",
        //                       new List<SqlParameter>() 
        //                    {                             
        //                       new SqlParameter("@opr", 'E'),
        //                       new SqlParameter("@sh_year", year),
        //                       new SqlParameter("@sh_attendence_date", i),
        //                       new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
        //                       new SqlParameter("@IGNORED_SHIFT_LIST",hrmsobj.em_number),
        //                       //new SqlParameter("@sh_emp_id", hrmsobj.em_number),
        //                       new SqlParameter("@sh_shift_id", shift_id),
        //                       new SqlParameter("@sh_shift_status", "A"),
        //                       new SqlParameter("@em_company_code", hrmsobj.em_company_code),
        //                       new SqlParameter("@em_dept_code", hrmsobj.em_dept_code),
        //                       new SqlParameter("@em_desg_code", ""),
        //                    });
        //                    if (ins > 0)
        //                    {
        //                        inserted = true;
        //                        i = i.Value.AddDays(1);
        //                    }
        //                    else
        //                    {
        //                        inserted = false;
        //                    }

        //                }
        //            }
        //        }
        //        // 
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    catch (Exception x)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}

        [Route("CUDInsertEmployeeShift")]
        public HttpResponseMessage CUDInsertEmployeeShift(List<HrmsClass> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertEmployeeShift(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "grade", "CUDInsertEmployeeShift"));
            string year = null, shift_id = null;
            DateTime? end_date = null, i = null;
            SqlDataReader dr;
            Message message = new Message();
            bool inserted = false;
            string cnt="3";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (HrmsClass hrmsobj in data)
                    {
                        dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
                       new List<SqlParameter>() 
                        {                             
                            new SqlParameter("@opr", "E1"),
                            new SqlParameter("@sh_year", year),
                            new SqlParameter("@sh_attendence_date", db.DBYYYYMMDDformat(hrmsobj.sh_start_date)),
                            new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
                            new SqlParameter("@IGNORED_SHIFT_LIST",hrmsobj.em_number),
                            new SqlParameter("@sh_shift_id", shift_id),
                            new SqlParameter("@sh_shift_status", "A"),
                            new SqlParameter("@em_company_code", hrmsobj.em_company_code),
                            new SqlParameter("@em_dept_code", hrmsobj.em_dept_code),
                            new SqlParameter("@em_desg_code", null),
                            new SqlParameter("@sh_application_todate",db.DBYYYYMMDDformat(hrmsobj.sh_end_date))
                        });
                        while (dr.Read())
                        {
                            cnt = dr["empshift_cnt"].ToString();
                            if (cnt == "1")
                            {
                                cnt = "1";
                            }
                            if (cnt == "2")
                            {
                                cnt = "2";
                            }
                            if (cnt == "3")
                            {
                                cnt = "3";
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, cnt);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, cnt);
        }

        //[Route("CUDUpdateEmployeeShift")]
        //public HttpResponseMessage CUDUpdateEmployeeShift(List<HrmsClass> data)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateEmployeeShift(),PARAMETERS :: No";
        //    Log.Debug(string.Format(debug, "grade", "CUDUpdateEmployeeShift"));
        //    string year = null, shift_id = null;
        //    DateTime? end_date = null, i = null;
        //    SqlDataReader dr;
        //    int ins;
        //    Message message = new Message();
        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (HrmsClass hrmsobj in data)
        //            {
        //                dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
        //                   new List<SqlParameter>() 
        //                { 
        //                   new SqlParameter("@opr", "B"),  
        //                });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        year = dr["sims_financial_year"].ToString();
        //                        end_date =Convert.ToDateTime(dr["sims_financial_year_end_date"].ToString());
        //                    }
        //                }
        //                dr.Close();

        //                ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_employee_shift]",
        //                      new List<SqlParameter>() 
        //                    {                             
        //                       new SqlParameter("@opr", "D"),
        //                       new SqlParameter("@sh_year", ""),
        //                       new SqlParameter("@sh_attendence_date", hrmsobj.sh_start_date),
        //                       new SqlParameter("@sh_template_id", ""),
        //                       new SqlParameter("@sh_emp_id", hrmsobj.em_number),
        //                       new SqlParameter("@sh_shift_id", ""),
        //                       new SqlParameter("@sh_shift_status", ""),
        //                       new SqlParameter("@em_company_code", ""),
        //                       new SqlParameter("@em_dept_code", ""),
        //                       new SqlParameter("@em_desg_code", ""),
        //                    });

        //                i = hrmsobj.sh_start_date;
        //                while (i <= end_date)
        //                {

        //                    string j = i.Value.DayOfWeek.ToString();
        //                    shift_id = null;
        //                    switch (j)
        //                    {
        //                        case "Sunday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day1), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Monday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day2), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Tuesday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day3), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Wednesday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day4), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Thursday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day5), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Friday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day6), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Saturday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day7), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                    }

        //                    ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_employee_shift]",
        //                      new List<SqlParameter>() 
        //                    {                             
        //                       new SqlParameter("@opr", 'I'),
        //                       new SqlParameter("@sh_year", year),
        //                       new SqlParameter("@sh_attendence_date", i),
        //                       new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
        //                       new SqlParameter("@sh_emp_id", hrmsobj.em_number),
        //                       new SqlParameter("@sh_shift_id", shift_id),
        //                       new SqlParameter("@sh_shift_status", "A"),
        //                       new SqlParameter("@em_company_code", hrmsobj.em_company_code),
        //                       new SqlParameter("@em_dept_code", hrmsobj.em_dept_code),
        //                       new SqlParameter("@em_desg_code", ""),
        //                    });
        //                    if (ins > 0)
        //                    {
        //                        inserted = true;
        //                        i = i.Value.AddDays(1);
        //                    }
        //                    else
        //                    {
        //                        inserted = false;
        //                    }

        //                }
        //            }
        //        }
        //        // 
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    catch (Exception x)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}

        //[Route("CUDUpdateEmployeeShift")]
        //public HttpResponseMessage CUDUpdateEmployeeShift(List<HrmsClass> data)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDUpdateEmployeeShift(),PARAMETERS :: No";
        //    Log.Debug(string.Format(debug, "grade", "CUDUpdateEmployeeShift"));
        //    string year = null, shift_id = null, end_date = null, i = null;
        //    SqlDataReader dr;
        //    int ins;
        //    Message message = new Message();
        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (HrmsClass hrmsobj in data)
        //            {
        //                dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                   new List<SqlParameter>() 
        //                { 
        //                   new SqlParameter("@opr", "B"),  
        //                });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        year = dr["sims_financial_year"].ToString();
        //                        end_date = dr["sims_financial_year_end_date"].ToString();
        //                    }
        //                }
        //                dr.Close();

        //                ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_employee_shift]",
        //                      new List<SqlParameter>() 
        //                    {                             
        //                       new SqlParameter("@opr", "D"),
        //                       new SqlParameter("@sh_year", ""),
        //                       new SqlParameter("@sh_attendence_date", hrmsobj.sh_start_date),
        //                       new SqlParameter("@sh_template_id", ""),
        //                       new SqlParameter("@sh_emp_id", hrmsobj.em_number),
        //                       new SqlParameter("@sh_shift_id", ""),
        //                       new SqlParameter("@sh_shift_status", ""),
        //                       new SqlParameter("@em_company_code", ""),
        //                       new SqlParameter("@em_dept_code", ""),
        //                       new SqlParameter("@em_desg_code", ""),
        //                    });

        //                i = hrmsobj.sh_start_date;
        //                while (DateTime.Parse(i) <= DateTime.Parse(end_date))
        //                {

        //                    string j = DateTime.Parse(i).DayOfWeek.ToString();
        //                    shift_id = null;
        //                    switch (j)
        //                    {
        //                        case "Sunday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day1), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Monday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                            new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day2), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            dr.Close();
        //                            break;
        //                        case "Tuesday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day3), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            break;
        //                        case "Wednesday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day4), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            break;
        //                        case "Thursday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day5), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            break;
        //                        case "Friday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day6), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            break;
        //                        case "Saturday":
        //                            dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_shift]",
        //                           new List<SqlParameter>() 
        //                           { 
        //                               new SqlParameter("@opr", "C"), 
        //                               new SqlParameter("@sh_shift_desc",hrmsobj.sh_shift_day7), 
        //                           });
        //                            if (dr.HasRows)
        //                            {
        //                                while (dr.Read())
        //                                {
        //                                    shift_id = dr["sh_shift_id"].ToString();
        //                                }
        //                            }
        //                            break;
        //                    }

        //                    ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_employee_shift]",
        //                      new List<SqlParameter>() 
        //                    {                             
        //                       new SqlParameter("@opr","I"),
        //                       new SqlParameter("@sh_year", year),
        //                       new SqlParameter("@sh_attendence_date", i),
        //                       new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
        //                       new SqlParameter("@sh_emp_id", hrmsobj.em_number),
        //                       new SqlParameter("@sh_shift_id", shift_id),
        //                       new SqlParameter("@sh_shift_status", "A"),
        //                       new SqlParameter("@em_company_code", hrmsobj.em_company_code),
        //                       new SqlParameter("@em_dept_code", hrmsobj.em_dept_code),
        //                       new SqlParameter("@em_desg_code", ""),
        //                    });
        //                    if (ins > 0)
        //                    {
        //                        inserted = true;
        //                        i = DateTime.Parse(i).AddDays(1).ToString();
        //                    }
        //                    else
        //                    {
        //                        inserted = false;
        //                    }

        //                }
        //            }
        //        }
        //        // 
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    catch (Exception x)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}

        [Route("CUDInsertAll_EmployeeShift")]
        public HttpResponseMessage CUDInsertAll_EmployeeShift(List<HrmsClass> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertAll_EmployeeShift(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Employee", "EmployeeShift"));

            Message message = new Message();
            bool inserted = false;
            int i = 0, cnt = 0;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (HrmsClass hrmsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_shift_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", "U"),
                                new SqlParameter("@sh_year", ""),
                                new SqlParameter("@sh_attendence_date", db.DBYYYYMMDDformat(hrmsobj.sh_start_date)),
                                new SqlParameter("@sh_template_id", hrmsobj.sh_template_id),
                                new SqlParameter("@sh_emp_id", hrmsobj.em_number),
                                new SqlParameter("@sh_shift_id", ""),
                                new SqlParameter("@sh_shift_status", "A"),
                                new SqlParameter("@em_company_code", hrmsobj.em_company_code),
                                new SqlParameter("@em_dept_code", hrmsobj.em_dept_code),
                                new SqlParameter("@em_desg_code", "")
                             });
                            if (ins > 0)
                            {
                                hrmsobj.cnt = hrmsobj.cnt + 1;
                                cnt = hrmsobj.cnt;
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, cnt);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, cnt);
        }

        [Route("Get_EmpTemplateDetails")]
        public HttpResponseMessage Get_EmpTemplateDetails(string emp_id)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_EmpTemplateDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_EmpTemplateDetails"));

            List<HrmsClass> gcnumber = new List<HrmsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_shift_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@sh_emp_id", emp_id),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HrmsClass hrmsobj = new HrmsClass();
                            hrmsobj.em_login_code = dr["sh_emp_id"].ToString();
                            hrmsobj.sh_emp_name = dr["emp_name"].ToString();
                            hrmsobj.sh_template_id = dr["sh_template_id"].ToString();
                            hrmsobj.sh_template_name = dr["sh_template_name"].ToString();
                            hrmsobj.sh_from_date = db.UIDDMMYYYYformat(dr["from_date"].ToString());
                            hrmsobj.sh_to_date = db.UIDDMMYYYYformat(dr["to_date"].ToString());
                            gcnumber.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, gcnumber);

            }
            return Request.CreateResponse(HttpStatusCode.OK, gcnumber);
        }


    }
}