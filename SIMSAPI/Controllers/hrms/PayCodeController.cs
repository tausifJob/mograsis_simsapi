﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/PayCode")]
    public class PayCodeController : ApiController
    {
        [Route("getDisplayOrder")]
        public HttpResponseMessage getDisplayOrder()
        {
            List<Per058> pay_code = new List<Per058>();
            Per058 obj = new Per058();
            try
            {
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr= db.ExecuteStoreProcedure("[sims].[pays_pay_code_proc]",
                        new List<SqlParameter>()
                        {
                        new SqlParameter("@OPR","B")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            if (dr["MAX"].ToString() != "")
                                obj.cp_display_order = int.Parse(dr["MAX"].ToString());
                            else
                                obj.cp_display_order = 1;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, obj.cp_display_order);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, obj.cp_display_order);
            }
            
        }


        [Route("getPayCode")]
        public HttpResponseMessage getPayCode()
        {
            List<Per058> pay_code = new List<Per058>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per058 obj = new Per058();

                            obj.pc_code = dr["pc_code"].ToString();
                            obj.pc_company_code = dr["pc_company_code"].ToString();
                            obj.company_name = dr["company_name"].ToString();

                            obj.pc_earn_dedn_tag = dr["pc_earn_dedn_tag"].ToString();
                            obj.dedn_tag = dr["dedn_tag"].ToString();
                            obj.pc_citi_exp_tag = dr["pc_citi_exp_tag"].ToString();
                            obj.cp_desc = dr["cp_desc"].ToString();
                            obj.cp_change_desc = dr["cp_change_desc"].ToString();
                            obj.cp_display_order = int.Parse(dr["cp_display_order"].ToString());
                            obj.pc_company_grade_tag = dr["pc_company_grade_tag"].Equals("G") ? true : false;

                            pay_code.Add(obj);


                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, pay_code);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, pay_code);
        }

        [Route("PayCodeCUD")]
        public HttpResponseMessage PayCodeCUD(List<Per058> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "PayCodeCUD", simsobj));


            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per058 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_pay_code_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@pc_code",simsobj.pc_code),
                          new SqlParameter("@pc_company_code",simsobj.pc_company_code),
                          new SqlParameter("@pc_earn_dedn_tag", simsobj.pc_earn_dedn_tag),
                          new SqlParameter("@pc_citi_exp_tag",simsobj.pc_citi_exp_tag),
                          new SqlParameter("@cl_desc",simsobj.cp_desc),
                          new SqlParameter("@cl_change_desc", simsobj.cp_change_desc),
                          new SqlParameter("@cp_display_order",simsobj.cp_display_order),

                          new SqlParameter("@pc_company_grade_tag", simsobj.pc_company_grade_tag == true ? "G" : "C")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}