﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;


namespace SIMSAPI.Controllers.hrms
{
     [RoutePrefix("api/common/PaysGrade")]

    public class PaysGradeController : ApiController
    {
         private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [Route("getAutoGenerateSequncepers233")]
         public HttpResponseMessage getAutoGenerateSequncepers233()
         {
             List<Pers233> mod_list = new List<Pers233>();
             string next_comm_number = "";
             try
             {
                 
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_grade_proc]",
                         new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "A"),    
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             next_comm_number = dr["comn_number_sequence"].ToString();
                         }
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, next_comm_number);

             }
             return Request.CreateResponse(HttpStatusCode.OK, next_comm_number);
         }

         [Route("Getcategorycode")]
         public HttpResponseMessage Getcategorycode(string comp_code)//string comp_code
         {
             List<Pers233> mod_list = new List<Pers233>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_grade_proc]",
                         new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "B"),   
                              new SqlParameter("@gr_company_code",comp_code),   
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Pers233 hrmsobj = new Pers233();
                             hrmsobj.gr_category_code = dr["ca_code"].ToString();
                             hrmsobj.gr_category_desc = dr["ca_desc"].ToString();
                             mod_list.Add(hrmsobj);
                         }
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, mod_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, mod_list);
         }

         [Route("Get_paygrade")]
         public HttpResponseMessage Get_paygrade()
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_paygrade(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "HRMS", "Get_paygrade"));

             List<Pers233> pay_list = new List<Pers233>();

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_grade_proc]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Pers233 grpay = new Pers233();
                             grpay.gr_company_code = dr["comp_code"].ToString();
                             grpay.gr_company_desc = dr["gr_company_code"].ToString();
                             grpay.gr_code = dr["gr_code"].ToString();
                             grpay.gr_desc = dr["gr_desc"].ToString();
                             grpay.gr_category_code = dr["cat_code"].ToString();
                             grpay.gr_category_desc = dr["gr_category_code"].ToString();
                             grpay.gr_print_order = dr["gr_print_order"].ToString();
                             grpay.gr_min_basic =dr["gr_min_basic"].ToString();
                             grpay.gr_max_basic =dr["gr_max_basic"].ToString();
                             grpay.gr_amt = dr["gr_amt"].ToString();
                             pay_list.Add(grpay);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, pay_list);
                     }
                 }
             }
             catch (Exception x)
             {
                 Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, pay_list);

             }
             return Request.CreateResponse(HttpStatusCode.OK, pay_list);
         }

         [Route("CUDPaysGrade")]
         public HttpResponseMessage CUDPaysGrade(List<Pers233> data)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDAirFareDetails(),PARAMETERS :: NO";
             Log.Debug(string.Format(debug, "HRMS", "CUDAirFareDetails"));

             //Pers046 persobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Pers046>(data);
             Message message = new Message();
             bool inserted = false;
             try
             {
                 if (data != null)
                 {
                     using (DBConnection db = new DBConnection())
                     {
                         db.Open();
                         foreach (Pers233 hrmsobj in data)
                         {
                             int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_grade_proc]",
                                 new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", hrmsobj.opr),
                                    new SqlParameter("@gr_company_code", hrmsobj.gr_company_code),
                                    new SqlParameter("@gr_code", hrmsobj.gr_code),
                                    new SqlParameter("@gr_desc", hrmsobj.gr_desc),
                                    new SqlParameter("@gr_category_code", hrmsobj.gr_category_code),
                                    new SqlParameter("@gr_print_order", hrmsobj.gr_print_order),
                                    new SqlParameter("@gr_min_basic", hrmsobj.gr_min_basic),
                                    new SqlParameter("@gr_max_basic", hrmsobj.gr_max_basic),
                                    new SqlParameter("@gr_amt", hrmsobj.gr_amt),
                            });
                             if (ins > 0)
                             {
                                 inserted = true;
                             }
                             else
                             {
                                 inserted = false;
                             }
                         }
                     }
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, inserted);
                 }

             }
             catch (Exception x)
             {
                 return Request.CreateResponse(HttpStatusCode.OK, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }
      
    }
}