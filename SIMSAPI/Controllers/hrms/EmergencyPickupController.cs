﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using Telerik.Reporting.Processing;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/EmergencyPickup")]
    public class EmergencyPickupController : ApiController
    {

        [Route("EmergencyPickupDataImport")]
        public HttpResponseMessage EmergencyPickupDataImport(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_Emergency_pickup", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("EmergencyPickupDataDeletion")]
        public HttpResponseMessage EmergencyPickupDataDeletion(List<EmergencyPickup> data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (EmergencyPickup obj in data)
                    {
                        int inst = db.ExecuteStoreProcedureforInsert("[sims].[sims_Emergency_pickup]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@enroll_num",obj.enroll_num),
                            new SqlParameter("@cur_code",obj.cur_code)
                        });
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
                return Request.CreateResponse(s, false);
            }
            return Request.CreateResponse(s, true);
        }
            
       [Route("EmergencyPickupDataUpdation")]
        public HttpResponseMessage EmergencyPickupDataUpdation(List<EmergencyPickup> data)
        {
            Message message = new Message();
            bool insert = false;
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
             
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach(EmergencyPickup obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Emergency_pickup]", new List<SqlParameter>()
                         
                        {
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@enroll_num",obj.enroll_num),
                            new SqlParameter("@cur_code",obj.cur_code),
                            new SqlParameter("@pickup_name",obj.pickup_name),
                            new SqlParameter("@relation_id",obj.relation_id),
                            new SqlParameter("@contact_num",obj.mobile_num),
                            new SqlParameter("@img_path",obj.img_path)
                        });

                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
                return Request.CreateResponse(s, o);
            }
            return Request.CreateResponse(s, insert);
        }
    }
}