﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSRestAPI.Controllers.EmployeeReportController
{
    [RoutePrefix("api/EmployeeReport")]
    public class EmployeeReportController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetEmployeeReport")]
        public HttpResponseMessage GetEmployeeReport(string comp, string desg, string dept, string sstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCategorize(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (comp == "" || comp == "undefined")
                        comp = null;

                    if (dept == "" || dept == "undefined")
                        dept = null;

                    if (desg == "" || desg == "undefined")
                        desg = null;

                    if (sstatus == "" || sstatus == "undefined")
                        sstatus = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[employee_report_forall_school]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@em_company_code", comp),
                            new SqlParameter("@em_dept_code", dept),
                            new SqlParameter("@em_desg_code", desg),
                            new SqlParameter("@em_service_code", sstatus),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();
                            
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.em_number = dr["em_number"].ToString();
                            hrmsobj.em_company_name = dr["company_name"].ToString();
                            hrmsobj.em_company_code = dr["em_company_code"].ToString();
                            hrmsobj.em_qualification = dr["pays_qualification_desc"].ToString();
                            hrmsobj.em_Deptt_Code = dr["em_dept_code"].ToString();
                            hrmsobj.em_dept_name = dr["dept_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_dept_effect_from"].ToString()))
                            hrmsobj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            hrmsobj.em_desg_code = dr["em_desg_code"].ToString();
                            hrmsobj.em_desg_desc = dr["designation_name"].ToString();
                            hrmsobj.em_stat = dr["en_labour_card_issue_date"].ToString();
                            hrmsobj.em_expiry_date = db.UIDDMMYYYYformat(dr["en_labour_card_expiry_date"].ToString());
                            hrmsobj.em_Dest_Code = dr["em_dest_code"].ToString();
                            hrmsobj.em_dest_desc = dr["destination_name"].ToString();
                           

                            hrmsobj.nationality_name = dr["nationality_name"].ToString();
                            hrmsobj.em_Gradee_Code = dr["em_grade_code"].ToString();
                            hrmsobj.em_grade_name = dr["grade_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_grade_effect_from"].ToString()))
                            hrmsobj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            hrmsobj.em_Salutation_Code = dr["em_salutation"].ToString();
                            hrmsobj.em_salutation = dr["salutation"].ToString();
                            hrmsobj.em_first_name = dr["em_first_name"].ToString();
                            hrmsobj.em_middle_name = dr["em_middle_name"].ToString();
                            hrmsobj.em_last_name = dr["em_last_name"].ToString();
                            hrmsobj.em_full_name = dr["em_full_name"].ToString();
                            hrmsobj.em_family_name = dr["em_family_name"].ToString();
                           

                            hrmsobj.em_name_ot = dr["em_name_ot"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_date_of_birth"].ToString()))
                            hrmsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());

                            hrmsobj.em_Sex_Code = dr["em_sex"].ToString();
                            hrmsobj.em_sex = dr["Gender"].ToString();
                            hrmsobj.em_marital_status = dr["maritialStatus"].ToString();
                            hrmsobj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            hrmsobj.em_Religion_code = dr["em_religion_code"].ToString();
                            hrmsobj.em_religion_name = dr["religion_name"].ToString();
                            hrmsobj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            hrmsobj.em_ethnicity_name = dr["ethnicity_name"].ToString();
                            hrmsobj.em_apartment_number = dr["em_appartment_number"].ToString();
                            hrmsobj.em_building_number = dr["em_building_number"].ToString();
                            hrmsobj.em_street_number = dr["em_street_number"].ToString();
                            hrmsobj.em_area_number = dr["em_area_number"].ToString();
                            hrmsobj.em_summary_address = dr["em_summary_address"].ToString();
                            hrmsobj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            hrmsobj.em_city_code = dr["em_city"].ToString();
                            hrmsobj.em_city = dr["city_name"].ToString();
                            hrmsobj.em_state_code = dr["em_state"].ToString();
                            hrmsobj.em_state = dr["state_name"].ToString();
                            hrmsobj.em_Country_Code = dr["em_country_code"].ToString();
                            hrmsobj.em_country = dr["sims_country_name_en"].ToString();
                            hrmsobj.em_Nation_Code = dr["em_nation_code"].ToString();
                            hrmsobj.em_service_status_code = dr["ServiceStatus"].ToString();
                            hrmsobj.em_phone = dr["em_phone"].ToString();
                            hrmsobj.em_mobile = dr["em_mobile"].ToString();
                            hrmsobj.em_email = dr["em_email"].ToString();
                            hrmsobj.em_fax = dr["em_fax"].ToString();
                            hrmsobj.em_po_fax = dr["em_po_box"].ToString();
                            hrmsobj.em_img = dr["em_img"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_dependant_full"].ToString()))
                                hrmsobj.em_dependant_full = Convert.ToInt16(dr["em_dependant_full"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_half"].ToString()))
                                hrmsobj.em_dependant_half = Convert.ToInt16(dr["em_dependant_half"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_infant"].ToString()))
                                hrmsobj.em_dependant_infant = Convert.ToInt16(dr["em_dependant_infant"].ToString());
                            hrmsobj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            hrmsobj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            hrmsobj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            hrmsobj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_date_of_join"].ToString()))
                                hrmsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            hrmsobj.em_service_status = dr["em_service_status"].ToString();
                            hrmsobj.em_staff_type = dr["em_staff_type_name"].ToString();
                            hrmsobj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            if (dr["em_bank_cash_tag"].ToString() == "C")
                                hrmsobj.em_bank_cash_tag = true;
                            else
                                hrmsobj.em_bank_cash_tag = false;
                            hrmsobj.em_ledger_ac_no = dr["em_ledger_ac_no"].ToString();
                            hrmsobj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            hrmsobj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_gosi_start_date"].ToString()))
                                hrmsobj.em_gosi_start_date = db.UIDDMMYYYYformat(dr["em_gosi_start_date"].ToString());
                            hrmsobj.em_pan_no = dr["em_pan_no"].ToString();
                            hrmsobj.em_iban_no = dr["em_iban_no"].ToString();
                            hrmsobj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            hrmsobj.em_route_code = dr["em_route_code"].ToString();
                            hrmsobj.em_status = dr["em_status"].ToString();
                            hrmsobj.em_status_code = dr["em_status_code"].ToString();
                            if (dr["em_stop_salary_indicator"].ToString() == "Y")
                                hrmsobj.em_stop_salary_indicator = true;
                            else
                                hrmsobj.em_stop_salary_indicator = false;
                     
                            if (!string.IsNullOrEmpty(dr["em_leave_resume_date"].ToString()))
                                hrmsobj.em_leave_resume_date = db.UIDDMMYYYYformat(dr["em_leave_resume_date"].ToString());
                            if (dr["em_leave_tag"].ToString() == "Y")
                                hrmsobj.em_leave_tag = true;
                            else
                                hrmsobj.em_leave_tag = false;
                            if (dr["em_citi_exp_tag"].ToString() == "E")
                                hrmsobj.em_citi_exp_tag = true;
                            else
                                hrmsobj.em_citi_exp_tag = false;
                            hrmsobj.em_joining_ref = dr["em_joining_ref"].ToString();
                            hrmsobj.em_bank_code = dr["em_bank_code"].ToString();
                            hrmsobj.bank_name = dr["bank_name"].ToString();
                            hrmsobj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            hrmsobj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                                hrmsobj.em_left_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString()); //DateTime.Parse(dr["em_left_date"].ToString());
                            hrmsobj.em_left_reason = dr["em_left_reason"].ToString();
                            if (dr["em_habdicap_status"].ToString() == "I")
                                hrmsobj.em_handicap_status = true;
                            else
                                hrmsobj.em_handicap_status = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_start_date"].ToString()))
                                hrmsobj.em_leave_start_date = db.UIDDMMYYYYformat(dr["em_leave_start_date"].ToString()); //DateTime.Parse(dr["em_leave_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_leave_end_date"].ToString()))
                                hrmsobj.em_leave_end_date = db.UIDDMMYYYYformat(dr["em_leave_end_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_cl_resume_date"].ToString()))
                                hrmsobj.em_cl_resume_date = db.UIDDMMYYYYformat(dr["em_cl_resume_date"].ToString());
                            hrmsobj.em_leave_resume_ref = dr["em_leave_resume_ref"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_over_stay_days"].ToString()))
                                hrmsobj.em_over_stay_days = Convert.ToInt16(dr["em_over_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_under_stay_days"].ToString()))
                                hrmsobj.em_under_stay_days = Convert.ToInt16(dr["em_under_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_stop_salary_from"].ToString()))
                                hrmsobj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            hrmsobj.em_unpaid_leave = dr["em_unpaid_leave"].ToString();
                            if (dr["em_punching_status"].ToString() == "A")
                                hrmsobj.em_punching_status = true;
                            else
                                hrmsobj.em_punching_status = false;
                            hrmsobj.em_punching_id = dr["em_punching_id"].ToString();
                            hrmsobj.em_passport_no = dr["em_passport_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_issue_date"].ToString()))
                                hrmsobj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_remember_expiry_date"].ToString()))
                                hrmsobj.em_pssport_exp_rem_date = db.UIDDMMYYYYformat(dr["em_passport_remember_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_expiry_date"].ToString()))
                                hrmsobj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_return_date"].ToString()))
                                hrmsobj.em_passport_ret_date = db.UIDDMMYYYYformat(dr["em_passport_return_date"].ToString());

                            hrmsobj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            hrmsobj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            hrmsobj.em_visa_no = dr["em_visa_number"].ToString();
                            hrmsobj.em_visa_type = dr["em_visa_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_issue_date"].ToString()))
                                hrmsobj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_visa_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_visa_remember_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_rem_date = db.UIDDMMYYYYformat(dr["em_visa_remember_expiry_date"].ToString());

                            hrmsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            hrmsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            hrmsobj.em_national_id = dr["em_national_id"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_issue_date"].ToString()))
                                hrmsobj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_national_id_expiry_date"].ToString()))
                                hrmsobj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_modified_on"].ToString()))
                                hrmsobj.em_modified_on = db.UIDDMMYYYYformat(dr["em_modified_on"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_last_login"].ToString()))
                                hrmsobj.em_last_login = db.UIDDMMYYYYformat(dr["em_last_login"].ToString());
                            hrmsobj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            hrmsobj.em_secret_answer = dr["em_secret_answer"].ToString();
                            hrmsobj.em_img1 = dr["em"].ToString();

                            if (dr["em_agreement"].ToString() == "A")
                                hrmsobj.em_agreement = true;
                            else
                                hrmsobj.em_agreement = false;

                            if (!string.IsNullOrEmpty(dr["em_agreement_start_date"].ToString()))
                                hrmsobj.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_date"].ToString()))
                                hrmsobj.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_rem_date"].ToString()))
                                hrmsobj.em_agreemet_exp_rem_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_rem_date"].ToString());
                            try
                            {
                                hrmsobj.em_voter_id = dr["em_voter_id"].ToString();
                                hrmsobj.em_qualification_inst_name = dr["em_qualification_inst_name"].ToString();
                                //ministory details

                                hrmsobj.em_adec_approval_number = dr["em_adec_approval_number"].ToString();
                                if (!string.IsNullOrEmpty(dr["em_adec_approval_Date"].ToString()))
                                    hrmsobj.em_adec_approval_Date = db.UIDDMMYYYYformat(dr["em_adec_approval_Date"].ToString());

                                hrmsobj.adec_approved_qualification = dr["adec_approved_qualification"].ToString();
                                hrmsobj.em_adec_approved_level = dr["em_adec_approved_level"].ToString();
                                hrmsobj.adec_approved_designation = dr["adec_approved_designation"].ToString();
                                hrmsobj.adec_approved_subject = dr["adec_approved_subject"].ToString();
                                hrmsobj.em_house_name = dr["house_name"].ToString();

                            }
                            catch (Exception ex)
                            {
                            }
                            hrmsobj.em_pwd = dr["em_blood_group_name"].ToString();

                            try
                            {                              
                                 hrmsobj.en_labour_card_expiry_date = db.UIDDMMYYYYformat(dr["en_labour_card_expiry_date"].ToString());
                            }
                            catch (Exception ex)
                            {
                            }

                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetEmployeeReportDpsd")]
        public HttpResponseMessage GetEmployeeReportDpsd(string comp, string desg, string dept, string sstatus)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCategorize(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (comp == "" || comp == "undefined")
                        comp = null;

                    if (dept == "" || dept == "undefined")
                        dept = null;

                    if (desg == "" || desg == "undefined")
                        desg = null;

                    if (sstatus == "" || sstatus == "undefined")
                        sstatus = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[employee_report_forall_school_dpsd]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@em_company_code", comp),
                            new SqlParameter("@em_dept_code", dept),
                            new SqlParameter("@em_desg_code", desg),
                            new SqlParameter("@em_service_code", sstatus),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();

                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.em_number = dr["em_number"].ToString();
                            hrmsobj.em_company_name = dr["company_name"].ToString();
                            hrmsobj.em_company_code = dr["em_company_code"].ToString();
                            hrmsobj.em_qualification = dr["pays_qualification_desc"].ToString();
                            hrmsobj.em_Deptt_Code = dr["em_dept_code"].ToString();
                            hrmsobj.em_dept_name = dr["dept_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_dept_effect_from"].ToString()))
                            hrmsobj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            hrmsobj.em_desg_code = dr["em_desg_code"].ToString();
                            hrmsobj.em_desg_desc = dr["designation_name"].ToString();

                            hrmsobj.em_stat = dr["en_labour_card_issue_date"].ToString();
                            hrmsobj.em_expiry_date = db.UIDDMMYYYYformat(dr["en_labour_card_expiry_date"].ToString());


                            hrmsobj.em_Dest_Code = dr["em_dest_code"].ToString();
                            hrmsobj.em_dest_desc = dr["destination_name"].ToString();


                            hrmsobj.em_nation_name = dr["nationality_name"].ToString();
                            hrmsobj.em_Gradee_Code = dr["em_grade_code"].ToString();
                            hrmsobj.em_grade_name = dr["grade_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_grade_effect_from"].ToString()))
                            hrmsobj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            hrmsobj.em_Salutation_Code = dr["em_salutation"].ToString();
                            hrmsobj.em_salutation = dr["salutation"].ToString();
                            hrmsobj.em_first_name = dr["em_first_name"].ToString();
                            hrmsobj.em_middle_name = dr["em_middle_name"].ToString();
                            hrmsobj.em_last_name = dr["em_last_name"].ToString();
                            hrmsobj.em_full_name = dr["em_full_name"].ToString();
                            hrmsobj.em_family_name = dr["em_family_name"].ToString();
                            hrmsobj.em_name_ot = dr["em_name_ot"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_date_of_birth"].ToString()))
                            hrmsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());


                            hrmsobj.em_Sex_Code = dr["em_sex"].ToString();
                            hrmsobj.em_sex = dr["Gender"].ToString();
                            hrmsobj.em_marital_status = dr["maritialStatus"].ToString();
                            hrmsobj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            hrmsobj.em_Religion_code = dr["em_religion_code"].ToString();
                            hrmsobj.em_religion_name = dr["religion_name"].ToString();
                            hrmsobj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            hrmsobj.em_ethnicity_name = dr["ethnicity_name"].ToString();
                            hrmsobj.em_apartment_number = dr["em_appartment_number"].ToString();
                            hrmsobj.em_building_number = dr["em_building_number"].ToString();
                            hrmsobj.em_street_number = dr["em_street_number"].ToString();
                            hrmsobj.em_area_number = dr["em_area_number"].ToString();
                            hrmsobj.em_summary_address = dr["em_summary_address"].ToString();
                            hrmsobj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            hrmsobj.em_city_code = dr["em_city"].ToString();
                            hrmsobj.em_city = dr["city_name"].ToString();
                            hrmsobj.em_state_code = dr["em_state"].ToString();
                            hrmsobj.em_state = dr["state_name"].ToString();
                            hrmsobj.em_Country_Code = dr["em_country_code"].ToString();
                            hrmsobj.em_country = dr["sims_country_name_en"].ToString();
                            hrmsobj.em_Nation_Code = dr["em_nation_code"].ToString();
                            hrmsobj.em_service_status_code = dr["ServiceStatus"].ToString();
                            hrmsobj.em_phone = dr["em_phone"].ToString();
                            hrmsobj.em_mobile = dr["em_mobile"].ToString();
                            hrmsobj.em_email = dr["em_email"].ToString();
                            hrmsobj.em_fax = dr["em_fax"].ToString();
                            hrmsobj.em_po_fax = dr["em_po_box"].ToString();
                            hrmsobj.em_img = dr["em_img"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_dependant_full"].ToString()))
                                hrmsobj.em_dependant_full = Convert.ToInt16(dr["em_dependant_full"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_half"].ToString()))
                                hrmsobj.em_dependant_half = Convert.ToInt16(dr["em_dependant_half"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_infant"].ToString()))
                                hrmsobj.em_dependant_infant = Convert.ToInt16(dr["em_dependant_infant"].ToString());
                            hrmsobj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            hrmsobj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            hrmsobj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            hrmsobj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_date_of_join"].ToString()))
                                hrmsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            hrmsobj.em_service_status = dr["em_service_status"].ToString();
                            hrmsobj.em_staff_type = dr["em_staff_type_name"].ToString();
                            hrmsobj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            if (dr["em_bank_cash_tag"].ToString() == "C")
                                hrmsobj.em_bank_cash_tag = true;
                            else
                                hrmsobj.em_bank_cash_tag = false;
                            hrmsobj.em_ledger_ac_no = dr["em_ledger_ac_no"].ToString();
                            hrmsobj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            hrmsobj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_gosi_start_date"].ToString()))
                                hrmsobj.em_gosi_start_date = db.UIDDMMYYYYformat(dr["em_gosi_start_date"].ToString());
                            hrmsobj.em_pan_no = dr["em_pan_no"].ToString();
                            hrmsobj.em_iban_no = dr["em_iban_no"].ToString();
                            hrmsobj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            hrmsobj.em_route_code = dr["em_route_code"].ToString();
                            hrmsobj.em_status = dr["em_status"].ToString();
                            hrmsobj.em_status_code = dr["em_status_code"].ToString();
                            if (dr["em_stop_salary_indicator"].ToString() == "Y")
                                hrmsobj.em_stop_salary_indicator = true;
                            else
                                hrmsobj.em_stop_salary_indicator = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_resume_date"].ToString()))
                                hrmsobj.em_leave_resume_date = db.UIDDMMYYYYformat(dr["em_leave_resume_date"].ToString());
                            if (dr["em_leave_tag"].ToString() == "Y")
                                hrmsobj.em_leave_tag = true;
                            else
                                hrmsobj.em_leave_tag = false;
                            if (dr["em_citi_exp_tag"].ToString() == "E")
                                hrmsobj.em_citi_exp_tag = true;
                            else
                                hrmsobj.em_citi_exp_tag = false;
                            hrmsobj.em_joining_ref = dr["em_joining_ref"].ToString();
                            hrmsobj.em_bank_code = dr["em_bank_code"].ToString();
                            hrmsobj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            hrmsobj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                                hrmsobj.em_left_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString()); //DateTime.Parse(dr["em_left_date"].ToString());
                            hrmsobj.em_left_reason = dr["em_left_reason"].ToString();
                            if (dr["em_habdicap_status"].ToString() == "A")
                                hrmsobj.em_handicap_status = true;
                            else
                                hrmsobj.em_handicap_status = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_start_date"].ToString()))
                                hrmsobj.em_leave_start_date = db.UIDDMMYYYYformat(dr["em_leave_start_date"].ToString()); //DateTime.Parse(dr["em_leave_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_leave_end_date"].ToString()))
                                hrmsobj.em_leave_end_date = db.UIDDMMYYYYformat(dr["em_leave_end_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_cl_resume_date"].ToString()))
                                hrmsobj.em_cl_resume_date = db.UIDDMMYYYYformat(dr["em_cl_resume_date"].ToString());
                            hrmsobj.em_leave_resume_ref = dr["em_leave_resume_ref"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_over_stay_days"].ToString()))
                                hrmsobj.em_over_stay_days = Convert.ToInt16(dr["em_over_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_under_stay_days"].ToString()))
                                hrmsobj.em_under_stay_days = Convert.ToInt16(dr["em_under_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_stop_salary_from"].ToString()))
                                hrmsobj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            hrmsobj.em_unpaid_leave = dr["em_unpaid_leave"].ToString();
                            if (dr["em_punching_status"].ToString() == "A")
                                hrmsobj.em_punching_status = true;
                            else
                                hrmsobj.em_punching_status = false;
                            hrmsobj.em_punching_id = dr["em_punching_id"].ToString();
                            hrmsobj.em_passport_no = dr["em_passport_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_issue_date"].ToString()))
                                hrmsobj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_remember_expiry_date"].ToString()))
                                hrmsobj.em_pssport_exp_rem_date = db.UIDDMMYYYYformat(dr["em_passport_remember_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_expiry_date"].ToString()))
                                hrmsobj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_return_date"].ToString()))
                                hrmsobj.em_passport_ret_date = db.UIDDMMYYYYformat(dr["em_passport_return_date"].ToString());

                            hrmsobj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            hrmsobj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            hrmsobj.em_visa_no = dr["em_visa_number"].ToString();
                            hrmsobj.em_visa_type = dr["em_visa_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_issue_date"].ToString()))
                                hrmsobj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_visa_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_visa_remember_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_rem_date = db.UIDDMMYYYYformat(dr["em_visa_remember_expiry_date"].ToString());

                            hrmsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            hrmsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            hrmsobj.em_national_id = dr["em_national_id"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_issue_date"].ToString()))
                                hrmsobj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_national_id_expiry_date"].ToString()))
                                hrmsobj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_modified_on"].ToString()))
                                hrmsobj.em_modified_on = db.UIDDMMYYYYformat(dr["em_modified_on"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_last_login"].ToString()))
                                hrmsobj.em_last_login = db.UIDDMMYYYYformat(dr["em_last_login"].ToString());
                            hrmsobj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            hrmsobj.em_secret_answer = dr["em_secret_answer"].ToString();
                            hrmsobj.em_img1 = dr["em"].ToString();

                            if (dr["em_agreement"].ToString() == "A")
                                hrmsobj.em_agreement = true;
                            else
                                hrmsobj.em_agreement = false;

                            if (!string.IsNullOrEmpty(dr["em_agreement_start_date"].ToString()))
                                hrmsobj.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_date"].ToString()))
                                hrmsobj.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_rem_date"].ToString()))
                                hrmsobj.em_agreemet_exp_rem_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_rem_date"].ToString());

                            hrmsobj.em_pwd = dr["em_blood_group_name"].ToString();

                            hrmsobj.em_division_code = dr["em_division_code"].ToString();
                            hrmsobj.em_grade_code = dr["em_grade_code"].ToString();

                            //--Ministory
                            hrmsobj.em_adec_approval_number = dr["em_adec_approval_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_adec_approval_Date"].ToString()))
                                hrmsobj.em_adec_approval_Date = db.UIDDMMYYYYformat(dr["em_adec_approval_Date"].ToString());
                            hrmsobj.em_adec_approved_qualification = dr["em_adec_approved_qualification"].ToString();
                            hrmsobj.em_adec_approved_designation = dr["em_adec_approved_designation"].ToString();
                            hrmsobj.em_adec_approved_subject = dr["em_adec_approved_subject"].ToString();
                            hrmsobj.em_adec_approved_level = dr["em_adec_approved_level"].ToString();

                            //--Helth
                            hrmsobj.em_health_card_no = dr["em_health_card_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_health_card_effective_upto_date"].ToString()))
                                hrmsobj.em_health_card_effective_upto_date = db.UIDDMMYYYYformat(dr["em_health_card_effective_upto_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_health_card_effective_from_date"].ToString()))
                                hrmsobj.em_health_card_effective_from_date = db.UIDDMMYYYYformat(dr["em_health_card_effective_from_date"].ToString());
                            hrmsobj.em_heath_card_status = dr["em_heath_card_status"].ToString();

                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetEmployeeReportOes")]
        public HttpResponseMessage GetEmployeeReportOes(string comp, string desg, string dept, string sstatus,string empID)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCategorize(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (comp == "" || comp == "undefined")
                        comp = null;

                    if (dept == "" || dept == "undefined")
                        dept = null;

                    if (desg == "" || desg == "undefined")
                        desg = null;

                    if (empID == "" || empID == "undefined")
                        empID = null;
                    if (sstatus == "" || sstatus == "undefined")
                        sstatus = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[employee_report_forall_school]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@em_company_code", comp),
                            new SqlParameter("@em_dept_code", dept),
                            new SqlParameter("@em_desg_code", desg),
                            new SqlParameter("@em_service_code", sstatus),
                            new SqlParameter("@em_number", empID),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();

                            //hrmsobj.chk_em_sponser_name = dr["chk_em_sponser_name"].ToString();
                            //hrmsobj.chk_em_relation = dr["chk_em_relation"].ToString();
                            //hrmsobj.chk_em_occupation = dr["chk_em_occupation"].ToString();
                            //hrmsobj.chk_em_workplace = dr["chk_em_workplace"].ToString();
                            //hrmsobj.chk_em_office_phone = dr["chk_em_office_phone"].ToString();
                            //hrmsobj.chk_em_mobile1 = dr["chk_em_mobile1"].ToString();
                            //hrmsobj.chk_visa_num = dr["chk_visa_num"].ToString();
                            //hrmsobj.chk_visa_issue_date = db.UIDDMMYYYYformat(dr["chk_visa_issue_date"].ToString());
                            //hrmsobj.chk_visa_exp_date = db.UIDDMMYYYYformat(dr["chk_visa_exp_date"].ToString());
                            //hrmsobj.chk_em_attr1 = dr["chk_em_attr1"].ToString();
                            //hrmsobj.chk_em_attr2 = dr["chk_em_attr2"].ToString();
                            //hrmsobj.chk_em_attr3 = dr["chk_em_attr3"].ToString();
                            //hrmsobj.chk_em_attr4 = dr["chk_em_attr4"].ToString();
                            //hrmsobj.chk_em_attr5 = dr["chk_em_attr5"].ToString();
                            if (dr["chk_em_status1"].ToString() == "A")
                                hrmsobj.chk_em_status1 = true;
                            else
                                hrmsobj.chk_em_status1 = false;

                            // hrmsobj.chk_em_national_id1 = dr["chk_em_national_id1"].ToString();
                            //  hrmsobj.chk_em_accommodation = dr["chk_em_accommodation"].ToString();  accommodation 
                            //hrmsobj.chk_em_sponsership = dr["chk_em_sponsership"].ToString();
                            //hrmsobj.chk_em_visa_Type = dr["chk_em_visa_Type"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.em_number = dr["em_number"].ToString();
                            hrmsobj.em_company_name = dr["company_name"].ToString();
                            hrmsobj.em_company_code = dr["em_company_code"].ToString();
                            hrmsobj.em_Deptt_Code = dr["em_dept_code"].ToString();
                            hrmsobj.em_dept_name = dr["dept_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_dept_effect_from"].ToString()))
                            hrmsobj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            hrmsobj.em_desg_code = dr["em_desg_code"].ToString();
                            hrmsobj.em_desg_desc = dr["designation_name"].ToString();
                            hrmsobj.em_qualification= dr["pays_qualification_desc"].ToString();
                            hrmsobj.em_Qualification_Arabic = dr["pays_qualification_desc_arabic"].ToString();
                            hrmsobj.em_stat = db.UIDDMMYYYYformat(dr["en_labour_card_issue_date"].ToString());
                            hrmsobj.em_expiry_date = db.UIDDMMYYYYformat(dr["en_labour_card_expiry_date"].ToString());
                            hrmsobj.em_Dest_Code = dr["em_dest_code"].ToString();
                            hrmsobj.em_dest_desc = dr["destination_name"].ToString();
                            hrmsobj.em_nation_name = dr["nationality_name"].ToString();
                            hrmsobj.em_Gradee_Code = dr["em_grade_code"].ToString();
                            hrmsobj.em_grade_name = dr["grade_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_grade_effect_from"].ToString()))
                            hrmsobj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            hrmsobj.em_Salutation_Code = dr["em_salutation"].ToString();
                            hrmsobj.em_salutation = dr["salutation"].ToString();
                            hrmsobj.em_first_name = dr["em_first_name"].ToString();
                            hrmsobj.em_middle_name = dr["em_middle_name"].ToString();
                            hrmsobj.em_last_name = dr["em_last_name"].ToString();
                            hrmsobj.em_full_name = dr["em_full_name"].ToString();
                            hrmsobj.em_family_name = dr["em_family_name"].ToString();
                            hrmsobj.em_name_ot = dr["em_name_ot"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_date_of_birth"].ToString()))
                            hrmsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());
                            hrmsobj.em_Sex_Code = dr["em_sex"].ToString();
                            hrmsobj.em_sex = dr["Gender"].ToString();
                            hrmsobj.em_marital_status = dr["maritialStatus"].ToString();
                            hrmsobj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            hrmsobj.em_Religion_code = dr["em_religion_code"].ToString();
                            hrmsobj.em_religion_name = dr["religion_name"].ToString();
                            hrmsobj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            hrmsobj.em_ethnicity_name = dr["ethnicity_name"].ToString();
                            hrmsobj.em_apartment_number = dr["em_appartment_number"].ToString();
                            hrmsobj.em_building_number = dr["em_building_number"].ToString();
                            hrmsobj.em_street_number = dr["em_street_number"].ToString();
                            hrmsobj.em_area_number = dr["em_area_number"].ToString();
                            hrmsobj.em_summary_address = dr["em_summary_address"].ToString();
                            hrmsobj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            hrmsobj.em_city_code = dr["em_city"].ToString();
                            hrmsobj.em_city = dr["city_name"].ToString();
                            hrmsobj.em_state_code = dr["em_state"].ToString();
                            hrmsobj.em_state = dr["state_name"].ToString();
                            hrmsobj.em_Country_Code = dr["em_country_code"].ToString();
                            hrmsobj.em_country = dr["sims_country_name_en"].ToString();
                            hrmsobj.em_Nation_Code = dr["em_nation_code"].ToString();
                            hrmsobj.em_service_status_code = dr["ServiceStatus"].ToString();
                            hrmsobj.em_phone = dr["em_phone"].ToString();
                            hrmsobj.em_mobile = dr["em_mobile"].ToString();
                            hrmsobj.em_email = dr["em_email"].ToString();                            
                            hrmsobj.em_Emp_Person_Email = dr["em_personalemail"].ToString();
                            hrmsobj.em_fax = dr["em_fax"].ToString();
                            hrmsobj.em_po_fax = dr["em_po_box"].ToString();
                            hrmsobj.em_img = dr["em_img"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_dependant_full"].ToString()))
                                hrmsobj.em_dependant_full = Convert.ToInt16(dr["em_dependant_full"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_half"].ToString()))
                                hrmsobj.em_dependant_half = Convert.ToInt16(dr["em_dependant_half"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_infant"].ToString()))
                                hrmsobj.em_dependant_infant = Convert.ToInt16(dr["em_dependant_infant"].ToString());
                            hrmsobj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            hrmsobj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            hrmsobj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            hrmsobj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_date_of_join"].ToString()))
                                hrmsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                            hrmsobj.em_service_status = dr["em_service_status"].ToString();
                            hrmsobj.em_staff_type = dr["em_staff_type_name"].ToString();
                            hrmsobj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            if (dr["em_bank_cash_tag"].ToString() == "C")
                                hrmsobj.em_bank_cash_tag = true;
                            else
                                hrmsobj.em_bank_cash_tag = false;
                            hrmsobj.em_ledger_ac_no = dr["em_ledger_ac_no"].ToString();
                            hrmsobj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            hrmsobj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_gosi_start_date"].ToString()))
                                hrmsobj.em_gosi_start_date = db.UIDDMMYYYYformat(dr["em_gosi_start_date"].ToString());
                            hrmsobj.em_pan_no = dr["em_pan_no"].ToString();
                            hrmsobj.em_iban_no = dr["em_iban_no"].ToString();
                            hrmsobj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            hrmsobj.em_route_code = dr["em_route_code"].ToString();
                            hrmsobj.em_status = dr["em_status"].ToString();
                            hrmsobj.em_status_code = dr["em_status_code"].ToString();
                            if (dr["em_stop_salary_indicator"].ToString() == "Y")
                                hrmsobj.em_stop_salary_indicator = true;
                            else
                                hrmsobj.em_stop_salary_indicator = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_resume_date"].ToString()))
                                hrmsobj.em_leave_resume_date = db.UIDDMMYYYYformat(dr["em_leave_resume_date"].ToString());
                            if (dr["em_leave_tag"].ToString() == "Y")
                                hrmsobj.em_leave_tag = true;
                            else
                                hrmsobj.em_leave_tag = false;
                            if (dr["em_citi_exp_tag"].ToString() == "E")
                                hrmsobj.em_citi_exp_tag = true;
                            else
                                hrmsobj.em_citi_exp_tag = false;
                            hrmsobj.em_joining_ref = dr["em_joining_ref"].ToString();
                            hrmsobj.em_bank_code = dr["em_bank_code"].ToString();
                            hrmsobj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            hrmsobj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                                hrmsobj.em_left_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                            hrmsobj.em_left_reason = dr["em_left_reason"].ToString();
                            if (dr["em_habdicap_status"].ToString() == "A")
                                hrmsobj.em_handicap_status = true;
                            else
                                hrmsobj.em_handicap_status = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_start_date"].ToString()))
                                hrmsobj.em_leave_start_date = db.UIDDMMYYYYformat(dr["em_leave_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_leave_end_date"].ToString()))
                                hrmsobj.em_leave_end_date = db.UIDDMMYYYYformat(dr["em_leave_end_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_cl_resume_date"].ToString()))
                                hrmsobj.em_cl_resume_date = db.UIDDMMYYYYformat(dr["em_cl_resume_date"].ToString());
                            hrmsobj.em_leave_resume_ref = dr["em_leave_resume_ref"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_over_stay_days"].ToString()))
                                hrmsobj.em_over_stay_days = Convert.ToInt16(dr["em_over_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_under_stay_days"].ToString()))
                                hrmsobj.em_under_stay_days = Convert.ToInt16(dr["em_under_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_stop_salary_from"].ToString()))
                                hrmsobj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            hrmsobj.em_unpaid_leave = dr["em_unpaid_leave"].ToString();
                            if (dr["em_punching_status"].ToString() == "A")
                                hrmsobj.em_punching_status = true;
                            else
                                hrmsobj.em_punching_status = false;
                            hrmsobj.em_punching_id = dr["em_punching_id"].ToString();
                            hrmsobj.em_passport_no = dr["em_passport_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_issue_date"].ToString()))
                                hrmsobj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_remember_expiry_date"].ToString()))
                                hrmsobj.em_pssport_exp_rem_date = db.UIDDMMYYYYformat(dr["em_passport_remember_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_expiry_date"].ToString()))
                                hrmsobj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_return_date"].ToString()))
                                hrmsobj.em_passport_ret_date = db.UIDDMMYYYYformat(dr["em_passport_return_date"].ToString());

                            hrmsobj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            hrmsobj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            hrmsobj.em_visa_no = dr["em_visa_number"].ToString();
                            hrmsobj.em_visa_type = dr["em_visa_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_issue_date"].ToString()))
                                hrmsobj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_visa_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_visa_remember_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_rem_date = db.UIDDMMYYYYformat(dr["em_visa_remember_expiry_date"].ToString());

                            hrmsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            hrmsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            hrmsobj.em_national_id = dr["em_national_id"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_issue_date"].ToString()))
                                hrmsobj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_national_id_expiry_date"].ToString()))
                                hrmsobj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_modified_on"].ToString()))
                                hrmsobj.em_modified_on = db.UIDDMMYYYYformat(dr["em_modified_on"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_last_login"].ToString()))
                                hrmsobj.em_last_login = db.UIDDMMYYYYformat(dr["em_last_login"].ToString());
                            hrmsobj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            hrmsobj.em_secret_answer = dr["em_secret_answer"].ToString();
                            hrmsobj.em_img1 = dr["em"].ToString();

                            if (dr["em_agreement"].ToString() == "A")
                                hrmsobj.em_agreement = true;
                            else
                                hrmsobj.em_agreement = false;

                            if (!string.IsNullOrEmpty(dr["em_agreement_start_date"].ToString()))
                                hrmsobj.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_date"].ToString()))
                                hrmsobj.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_rem_date"].ToString()))
                                hrmsobj.em_agreemet_exp_rem_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_rem_date"].ToString());

                            hrmsobj.em_pwd = dr["em_blood_group_name"].ToString();
                            hrmsobj.em_accommodation_status = dr["em_accommodation_status"].ToString();
                        //Sponsor Details
                            hrmsobj.em_sponser_id = dr["Sponser_Id"].ToString();
                            hrmsobj.em_sponser_name = dr["Sponsor_Name_In_English"].ToString();
                            hrmsobj.em_sponser_name_ar = dr["Sponsor_Name_In_Arabic"].ToString();
                            hrmsobj.em_sponser_address_en = dr["Sponsor_Address_In_English"].ToString();
                            hrmsobj.em_sponser_address_ar = dr["Sponsor_Address_In_Arabic"].ToString();
                            hrmsobj.em_workplace = dr["Sponsor_Work_Place_In_English"].ToString();
                            hrmsobj.em_workplace_ar = dr["Sponsor_Work_Place_In_Arabic"].ToString();
                            hrmsobj.spem_mobile = dr["Sponsor_Mobile_No"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                            hrmsobj.em_date_of_Left = db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                            hrmsobj.em_Tot_exp = dr["total_experience"].ToString();
                            events.Add(hrmsobj);
                        }
                    }
                 
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
          
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetEmployeeReportAsd")]
        public HttpResponseMessage GetEmployeeReportAsd(string comp, string desg, string dept, string sstatus, string emp_id, string name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCategorize(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (comp == "" || comp == "undefined")
                        comp = null;

                    if (dept == "" || dept == "undefined")
                        dept = null;

                    if (desg == "" || desg == "undefined")
                        desg = null;

                    if (sstatus == "" || sstatus == "undefined")
                        sstatus = null;

                    if (name == "" || name == "undefined")
                        name = null;


                    if (emp_id == "" || emp_id == "undefined")
                        emp_id = null;

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[employee_report_forall_school_asd]",
                        new List<SqlParameter>() {

                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@em_company_code", comp),
                            new SqlParameter("@em_dept_code", dept),
                            new SqlParameter("@em_desg_code", desg),
                            new SqlParameter("@em_service_code", sstatus),
                            new SqlParameter("@em_id", emp_id),
                            new SqlParameter("@em_name", name),


                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();

                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.em_number = dr["em_number"].ToString();
                            hrmsobj.em_company_name = dr["company_name"].ToString();
                            hrmsobj.em_company_code = dr["em_company_code"].ToString();
                            hrmsobj.em_qualification = dr["pays_qualification_desc"].ToString();
                            hrmsobj.em_Deptt_Code = dr["em_dept_code"].ToString();
                            hrmsobj.em_dept_name = dr["dept_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_dept_effect_from"].ToString()))
                            hrmsobj.em_dept_effect_from = db.UIDDMMYYYYformat(dr["em_dept_effect_from"].ToString());
                            hrmsobj.em_desg_code = dr["em_desg_code"].ToString();
                            hrmsobj.em_desg_desc = dr["designation_name"].ToString();

                            hrmsobj.em_stat = db.UIDDMMYYYYformat(dr["en_labour_card_issue_date"].ToString());
                            hrmsobj.em_expiry_date = db.UIDDMMYYYYformat(dr["en_labour_card_expiry_date"].ToString());


                            hrmsobj.em_Dest_Code = dr["em_dest_code"].ToString();
                            hrmsobj.em_dest_desc = dr["destination_name"].ToString();


                            hrmsobj.em_nation_name = dr["nationality_name"].ToString();
                            hrmsobj.em_Gradee_Code = dr["em_grade_code"].ToString();
                            hrmsobj.em_grade_name = dr["grade_name"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_grade_effect_from"].ToString()))
                            hrmsobj.em_grade_effect_from = db.UIDDMMYYYYformat(dr["em_grade_effect_from"].ToString());
                            hrmsobj.em_Salutation_Code = dr["em_salutation"].ToString();
                            hrmsobj.em_salutation = dr["salutation"].ToString();
                            hrmsobj.em_first_name = dr["em_first_name"].ToString();
                            hrmsobj.em_middle_name = dr["em_middle_name"].ToString();
                            hrmsobj.em_last_name = dr["em_last_name"].ToString();
                            hrmsobj.em_full_name = dr["em_full_name"].ToString();
                            hrmsobj.em_family_name = dr["em_family_name"].ToString();
                            hrmsobj.em_name_ot = dr["em_name_ot"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_date_of_birth"].ToString()))
                            hrmsobj.em_date_of_birth = db.UIDDMMYYYYformat(dr["em_date_of_birth"].ToString());


                            hrmsobj.em_Sex_Code = dr["em_sex"].ToString();
                            hrmsobj.em_sex = dr["Gender"].ToString();
                            hrmsobj.em_marital_status = dr["maritialStatus"].ToString();
                            hrmsobj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            hrmsobj.em_Religion_code = dr["em_religion_code"].ToString();
                            hrmsobj.em_religion_name = dr["religion_name"].ToString();
                            hrmsobj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            hrmsobj.em_ethnicity_name = dr["ethnicity_name"].ToString();
                            hrmsobj.em_apartment_number = dr["em_appartment_number"].ToString();
                            hrmsobj.em_building_number = dr["em_building_number"].ToString();
                            hrmsobj.em_street_number = dr["em_street_number"].ToString();
                            hrmsobj.em_area_number = dr["em_area_number"].ToString();
                            hrmsobj.em_summary_address = dr["em_summary_address"].ToString();
                            hrmsobj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            hrmsobj.em_city_code = dr["em_city"].ToString();
                            hrmsobj.em_city = dr["city_name"].ToString();
                            hrmsobj.em_state_code = dr["em_state"].ToString();
                            hrmsobj.em_state = dr["state_name"].ToString();
                            hrmsobj.em_Country_Code = dr["em_country_code"].ToString();
                            hrmsobj.em_country = dr["sims_country_name_en"].ToString();
                            hrmsobj.em_Nation_Code = dr["em_nation_code"].ToString();
                            hrmsobj.em_service_status_code = dr["ServiceStatus"].ToString();
                            hrmsobj.em_phone = dr["em_phone"].ToString();
                            hrmsobj.em_mobile = dr["em_mobile"].ToString();
                            hrmsobj.em_email = dr["em_email"].ToString();
                       
                            hrmsobj.em_fax = dr["em_fax"].ToString();
                            hrmsobj.em_po_fax = dr["em_po_box"].ToString();
                            hrmsobj.em_img = dr["em_img"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_dependant_full"].ToString()))
                                hrmsobj.em_dependant_full = Convert.ToInt16(dr["em_dependant_full"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_half"].ToString()))
                                hrmsobj.em_dependant_half = Convert.ToInt16(dr["em_dependant_half"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_dependant_infant"].ToString()))
                                hrmsobj.em_dependant_infant = Convert.ToInt16(dr["em_dependant_infant"].ToString());
                            hrmsobj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            hrmsobj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            hrmsobj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            hrmsobj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();
                            hrmsobj.em_service_status = dr["em_service_status"].ToString();
                            hrmsobj.em_staff_type = dr["em_staff_type_name"].ToString();
                            hrmsobj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            if (dr["em_bank_cash_tag"].ToString() == "C")
                                hrmsobj.em_bank_cash_tag = true;
                            else
                                hrmsobj.em_bank_cash_tag = false;
                            hrmsobj.em_ledger_ac_no = dr["em_ledger_ac_no"].ToString();
                            hrmsobj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            hrmsobj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_gosi_start_date"].ToString()))
                                hrmsobj.em_gosi_start_date = db.UIDDMMYYYYformat(dr["em_gosi_start_date"].ToString());
                            hrmsobj.em_pan_no = dr["em_pan_no"].ToString();
                            hrmsobj.em_iban_no = dr["em_iban_no"].ToString();
                            hrmsobj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            hrmsobj.em_route_code = dr["em_route_code"].ToString();
                            hrmsobj.em_status = dr["em_status"].ToString();
                            hrmsobj.em_status_code = dr["em_status_code"].ToString();
                            if (dr["em_stop_salary_indicator"].ToString() == "Y")
                                hrmsobj.em_stop_salary_indicator = true;
                            else
                                hrmsobj.em_stop_salary_indicator = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_resume_date"].ToString()))
                                hrmsobj.em_leave_resume_date = db.UIDDMMYYYYformat(dr["em_leave_resume_date"].ToString());
                            if (dr["em_leave_tag"].ToString() == "Y")
                                hrmsobj.em_leave_tag = true;
                            else
                                hrmsobj.em_leave_tag = false;
                            if (dr["em_citi_exp_tag"].ToString() == "E")
                                hrmsobj.em_citi_exp_tag = true;
                            else
                                hrmsobj.em_citi_exp_tag = false;
                            hrmsobj.em_joining_ref = dr["em_joining_ref"].ToString();
                            hrmsobj.em_bank_code = dr["em_bank_code"].ToString();
                            hrmsobj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            hrmsobj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                                hrmsobj.em_left_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                            hrmsobj.em_left_reason = dr["em_left_reason"].ToString();
                            if (dr["em_habdicap_status"].ToString() == "A")
                                hrmsobj.em_handicap_status = true;
                            else
                                hrmsobj.em_handicap_status = false;

                            if (!string.IsNullOrEmpty(dr["em_leave_start_date"].ToString()))
                                hrmsobj.em_leave_start_date = db.UIDDMMYYYYformat(dr["em_leave_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_leave_end_date"].ToString()))
                                hrmsobj.em_leave_end_date = db.UIDDMMYYYYformat(dr["em_leave_end_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_cl_resume_date"].ToString()))
                                hrmsobj.em_cl_resume_date = db.UIDDMMYYYYformat(dr["em_cl_resume_date"].ToString());
                            hrmsobj.em_leave_resume_ref = dr["em_leave_resume_ref"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_over_stay_days"].ToString()))
                                hrmsobj.em_over_stay_days = Convert.ToInt16(dr["em_over_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_under_stay_days"].ToString()))
                                hrmsobj.em_under_stay_days = Convert.ToInt16(dr["em_under_stay_days"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_stop_salary_from"].ToString()))
                                hrmsobj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            hrmsobj.em_unpaid_leave = dr["em_unpaid_leave"].ToString();
                            if (dr["em_punching_status"].ToString() == "A")
                                hrmsobj.em_punching_status = true;
                            else
                                hrmsobj.em_punching_status = false;
                            hrmsobj.em_punching_id = dr["em_punching_id"].ToString();
                            hrmsobj.em_passport_no = dr["em_passport_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_issue_date"].ToString()))
                                hrmsobj.em_passport_issue_date = db.UIDDMMYYYYformat(dr["em_passport_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_remember_expiry_date"].ToString()))
                                hrmsobj.em_pssport_exp_rem_date = db.UIDDMMYYYYformat(dr["em_passport_remember_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_expiry_date"].ToString()))
                                hrmsobj.em_passport_exp_date = db.UIDDMMYYYYformat(dr["em_passport_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_passport_return_date"].ToString()))
                                hrmsobj.em_passport_ret_date = db.UIDDMMYYYYformat(dr["em_passport_return_date"].ToString());

                            hrmsobj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            hrmsobj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            hrmsobj.em_visa_no = dr["em_visa_number"].ToString();
                            hrmsobj.em_visa_type = dr["em_visa_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_issue_date"].ToString()))
                                hrmsobj.em_visa_issue_date = db.UIDDMMYYYYformat(dr["em_visa_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_visa_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_date = db.UIDDMMYYYYformat(dr["em_visa_expiry_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["em_visa_remember_expiry_date"].ToString()))
                                hrmsobj.em_visa_exp_rem_date = db.UIDDMMYYYYformat(dr["em_visa_remember_expiry_date"].ToString());

                            hrmsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            hrmsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            hrmsobj.em_national_id = dr["em_national_id"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_issue_date"].ToString()))
                                hrmsobj.em_national_id_issue_date = db.UIDDMMYYYYformat(dr["em_national_id_issue_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_national_id_expiry_date"].ToString()))
                                hrmsobj.em_national_id_expiry_date = db.UIDDMMYYYYformat(dr["em_national_id_expiry_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_modified_on"].ToString()))
                                hrmsobj.em_modified_on = db.UIDDMMYYYYformat(dr["em_modified_on"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_last_login"].ToString()))
                                hrmsobj.em_last_login = db.UIDDMMYYYYformat(dr["em_last_login"].ToString());
                            hrmsobj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            hrmsobj.em_secret_answer = dr["em_secret_answer"].ToString();
                            hrmsobj.em_img1 = dr["em"].ToString();

                            if (dr["em_agreement"].ToString() == "A")
                                hrmsobj.em_agreement = true;
                            else
                                hrmsobj.em_agreement = false;

                            if (!string.IsNullOrEmpty(dr["em_agreement_start_date"].ToString()))
                                hrmsobj.em_agreement_start_date = db.UIDDMMYYYYformat(dr["em_agreement_start_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_date"].ToString()))
                                hrmsobj.em_agreement_exp_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_date"].ToString());

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_rem_date"].ToString()))
                                hrmsobj.em_agreemet_exp_rem_date = db.UIDDMMYYYYformat(dr["em_agreement_exp_rem_date"].ToString());

                            hrmsobj.em_pwd = dr["em_blood_group_name"].ToString();

                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetCompanyNameForShift")]
        public HttpResponseMessage GetCompanyNameForShift()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() {
                new SqlParameter("tbl_name", "[pays].[pays_company]"),
                new SqlParameter("tbl_col_name1", "[co_company_code],[co_desc]")


                        }

                        );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();
                            hrmsobj.sh_company_code = dr["co_company_code"].ToString();
                            hrmsobj.sh_company_name = dr["co_desc"].ToString();
                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetDepartmentName")]
        public HttpResponseMessage GetDepartmentName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() {
                new SqlParameter("@opr", "D"),

                        }

                        );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();
                            hrmsobj.code = dr["codp_dept_no"].ToString();
                            hrmsobj.em_dept_name = dr["codp_dept_name"].ToString();
                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetDesignation")]
        public HttpResponseMessage GetDesignation(string com_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() {
                new SqlParameter("@opr", "DE"),
                new SqlParameter("@opr_mem", "DE"),
                new SqlParameter("@tbl_cond", com_code)

                        }

                        );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new simsobj = new Pers099_new();
                            simsobj.code = dr["dg_code"].ToString();
                            simsobj.em_desg_desc = dr["dg_desc"].ToString();


                            events.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [Route("GetServiceStatus")]
        public HttpResponseMessage GetServiceStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> mod_list = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[employee_report_forall_school]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "A"),
                        }

                        );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new simsobj = new Pers099_new();
                            simsobj.code = dr["sims_appl_parameter"].ToString();
                            simsobj.em_service_status = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}