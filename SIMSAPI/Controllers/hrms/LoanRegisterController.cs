﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/LoanCodeRegister")]
    public class LoanRegisterController:ApiController
    {

        [Route("getAllLoanRegister")]
        public HttpResponseMessage getAllLoanRegister()
        {
            List<Per134> loanRegister_list = new List<Per134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_register_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per134 obj = new Per134();

                            obj.lo_number = dr["lo_number"].ToString();
                            obj.EmployeeName = dr["employeeName"].ToString();

                            obj.lo_company_code = dr["lo_company_code"].ToString();
                            obj.CompanyName = dr["CompanyName"].ToString();

                            obj.lo_code = dr["lo_code"].ToString();
                            obj.LoanName = dr["LoanName"].ToString();
                            
                            obj.lo_sanction_date = db.UIDDMMYYYYformat(dr["lo_sanction_date"].ToString());
                            obj.lo_amount = dr["lo_amount"].ToString();
                            obj.lo_paid_amount = dr["lo_paid_amount"].ToString();
                            obj.lo_no_of_installment = dr["lo_no_of_installment"].ToString();
                            obj.lo_repaid_installment = dr["lo_repaid_installment"].ToString();
                            obj.lo_reference = dr["lo_reference"].ToString();
                            obj.lo_gaunt_no = dr["lo_gaunt_no"].ToString();
                            obj.lo_gaunt_name = dr["lo_gaunt_name"].ToString();
                            obj.lo_gaunt_comp_name = dr["lo_gaunt_comp_name"].ToString();
                            obj.lo_gaunt_desg = dr["lo_gaunt_desg"].ToString();
                            obj.lo_gaunt_address1 = dr["lo_gaunt_address1"].ToString();
                            obj.lo_gaunt_address2 = dr["lo_gaunt_address2"].ToString();
                            obj.lo_gaunt_address3 = dr["lo_gaunt_address3"].ToString();

                            obj.lo_mgr_confirm_tag = dr["lo_mgr_confirm_tag"].Equals("Y") ? true : false;
                            obj.lo_fully_rec_tag = dr["lo_fully_rec_tag"].Equals("Y") ? true : false;
                            obj.lo_monthly_rec_tag = dr["lo_monthly_rec_tag"].Equals("Y") ? true : false;

                            loanRegister_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loanRegister_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loanRegister_list);
        }



        [Route("LoanCodeRegisterCUD")]
        public HttpResponseMessage LoanCodeRegisterCUD(Per134 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LoanCodeRegisterCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_loan_register_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@Loan_number",simsobj.lo_number),
                          new SqlParameter("@Company_code",simsobj.lo_company_code),
                          new SqlParameter("@Loancode", simsobj.lo_code),
                          new SqlParameter("@Sanction_date", db.DBYYYYMMDDformat(simsobj.lo_sanction_date)),
                          new SqlParameter("@Amount",simsobj.lo_amount),
                          new SqlParameter("@Paid_amount", simsobj.lo_paid_amount),
                        
                          new SqlParameter("@Total_Installment", simsobj.lo_no_of_installment),
                          new SqlParameter("@Repaid_installment", simsobj.lo_repaid_installment),
                          
                          new SqlParameter("@Reference", simsobj.lo_reference),
                          new SqlParameter("@Gaunt_no", simsobj.lo_gaunt_no),

                          new SqlParameter("@Gaunt_name",simsobj.lo_gaunt_name),
                          new SqlParameter("@Gaunt_comp_name", simsobj.lo_gaunt_comp_name),
                          new SqlParameter("@Gaunt_desg", simsobj.lo_gaunt_desg),
                          new SqlParameter("@Gaunt_address1",simsobj.lo_gaunt_address1),
                          new SqlParameter("@Gaunt_address2", simsobj.lo_gaunt_address2),
                          new SqlParameter("@Gaunt_address3", simsobj.lo_gaunt_address3),

                          new SqlParameter("@Monthly_rec_tag",simsobj.lo_monthly_rec_tag==true?"Y":"N"),
                          new SqlParameter("@Fully_rec_tag",simsobj.lo_fully_rec_tag==true?"Y":"N"),
                          new SqlParameter("@Manager_confirm_tag",simsobj.lo_mgr_confirm_tag==true?"Y":"N"),
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}