﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.ReviewerController
{
    [RoutePrefix("api/Reviewer")]
    [BasicAuthentication]
    public class ReviewerController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllReviewer")]
        public HttpResponseMessage getAllReviewer()
        {
            List<Per301> desg_list = new List<Per301>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_sip_reviewer]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per301 simsobj = new Per301();
                            simsobj.sims_sip_reviewer_code = dr["sims_sip_reviewer_code"].ToString();
                            simsobj.sims_sip_reviewer_login_code = dr["sims_sip_reviewer_login_code"].ToString();
                            simsobj.emp_name = dr["Emp_name"].ToString();
                            simsobj.sims_sip_reviewer_order = dr["sims_sip_reviewer_order"].ToString();
                            simsobj.sims_sip_reviewer_status = dr["sims_sip_reviewer_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }


        [Route("CUDReviewer")]
        public HttpResponseMessage CUDReviewer(List<Per301> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per301 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_sip_reviewer]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_reviewer_code", simsobj.sims_sip_reviewer_code),
                                new SqlParameter("@sims_sip_reviewer_login_code", simsobj.sims_sip_reviewer_login_code),
                                new SqlParameter("@sims_sip_reviewer_order", simsobj.sims_sip_reviewer_order),
                                new SqlParameter("@sims_sip_reviewer_status", simsobj.sims_sip_reviewer_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
