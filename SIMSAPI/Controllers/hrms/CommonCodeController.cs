﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/CommonCode")]
    public class CommonCodeController : ApiController
    {

        [Route("getCommonCode")]
        public HttpResponseMessage getCommonCode()
        {
            List<Per244> Cl_code = new List<Per244>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per244 obj = new Per244();

                            obj.cl_code = dr["cl_code"].ToString();
                            obj.cl_desc = dr["cl_desc"].ToString();
                            obj.cl_change_desc = dr["cl_change_desc"].ToString();
                            obj.cl_carry_forward = dr["cl_carry_forward"].Equals("Y") ? true : false;
                            obj.cl_leave_document_code = dr["cl_leave_document_code"].ToString();
                            Cl_code.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Cl_code);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Cl_code);
        }

        [Route("CommondetailsCode")]
        public HttpResponseMessage CommondetailsCode(string code)
        {
            List<comn_details> Cl_code = new List<comn_details>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "V"),
                            new SqlParameter("@cld_code", code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_details obj = new comn_details();

                            obj.srno = dr["cld_sr_no"].ToString();
                            obj.applicable_from = dr["cld_applicable_from"].ToString();
                            obj.applicable_to = dr["cld_applicable_upto"].ToString();
                            obj.applicable_value = dr["cld_value"].ToString();

                            Cl_code.Add(obj);

                        }
                    }
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Cl_code);
        }

        [Route("InsertCommonCode")]
        public HttpResponseMessage InsertCommonCode(Per244 data)
        {
            bool chk = true;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                        new List<SqlParameter>() 
                    
                        {
                          new SqlParameter("@opr","I"),
                          new SqlParameter("@cl_code",data.cl_code),
                          new SqlParameter("@cl_desc", data.cl_desc),
                          new SqlParameter("@cl_change_desc",data.cl_change_desc),
                          new SqlParameter("@cl_carry_forward",data.cl_carry_forward == true ? "Y" : "N"),
                          new SqlParameter("@cl_leave_document_code",data.cl_leave_document_code)
                        });

                    while (dr.Read())
                    {
                        chk = InsertintoCommonCodeDetails(dr["cl_code"].ToString(), data.cmn_det);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, chk);
        }

        public bool InsertintoCommonCodeDetails(string data, List<comn_details> lst)
        {
            bool chk = false;
            try
            {
                foreach (var item in lst)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                            new List<SqlParameter>() 
                    
                        {
                          new SqlParameter("@opr","J"),
                          new SqlParameter("@cld_code",data),
                          new SqlParameter("@cld_applicable_form",item.applicable_from),
                          new SqlParameter("@cld_applicable_to", item.applicable_to),
                          new SqlParameter("@cld_value",item.applicable_value)
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
            }
            return chk;
        }

        [Route("UpdateCommonCode")]
        public HttpResponseMessage UpdateCommonCode(Per244 data)
        {
            bool chk = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                        new List<SqlParameter>() 
                    
                        {
                          new SqlParameter("@opr","U"),
                          new SqlParameter("@cl_code",data.cl_code),
                          new SqlParameter("@cl_desc", data.cl_desc),
                          new SqlParameter("@cl_change_desc",data.cl_change_desc),
                          new SqlParameter("@cl_carry_forward",data.cl_carry_forward == true ? "Y" : "N"),
                          new SqlParameter("@cl_leave_document_code",data.cl_leave_document_code)
                        });

                    while (dr.Read())
                    {
                        chk = InsertintoCommonCodeDetails(dr["cl_code"].ToString(), data.cmn_det);
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, chk);
        }

        [Route("Common_codeDelete")]
        public HttpResponseMessage Common_codeDelete(List<Per244> lst)
        {
            bool chk = false;
            try
            {
                foreach (var item in lst)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                            new List<SqlParameter>() 
                    
                        {
                          new SqlParameter("@opr","D"),
                          new SqlParameter("@cld_code",item.cl_code)
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            chk = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, chk);
        }

        [Route("getLeaveDocuments")]
        public HttpResponseMessage getLeaveDocuments()
        {

            List<Per256> desg_list = new List<Per256>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_common_code_proc]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", 'K')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per256 simsobj = new Per256();
                            simsobj.pays_leave_doc_code = dr["pays_leave_doc_code"].ToString();
                            simsobj.pays_leave_doc_name = dr["pays_leave_doc_name"].ToString();
                            simsobj.pays_leave_doc_desc = dr["pays_leave_doc_desc"].ToString();
                            simsobj.pays_leave_doc_is_mandatory = dr["pays_leave_doc_is_mandatory"].Equals("Y") ? true : false;
                            simsobj.pays_leave_doc_status = dr["pays_leave_doc_status"].Equals("A") ? true : false;
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

    }
}