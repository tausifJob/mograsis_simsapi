﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/UpdateHoldEmpStatus")]
    public class UpdateHoldEmpStatusController : ApiController
    {

        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<payrollOnholdStatus> mod_list = new List<payrollOnholdStatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "M")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payrollOnholdStatus hrmsObj = new payrollOnholdStatus();
                            hrmsObj.dep_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dep_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<payrollOnholdStatus> mod_list = new List<payrollOnholdStatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payrollOnholdStatus hrmsObj = new payrollOnholdStatus();
                            hrmsObj.dg_code = dr["dg_code"].ToString();
                            hrmsObj.dg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetYearMonth")]
        public HttpResponseMessage GetYearMonth()
        {
            List<payrollOnholdStatus> mod_list = new List<payrollOnholdStatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].Pays_mark_hold_for_employee",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'M'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payrollOnholdStatus simsobj = new payrollOnholdStatus();
                            simsobj.year_month = dr["pe_year_month"].ToString();
                            simsobj.year_month_name = dr["yearmonth_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("PaysheetDetails")]
        public HttpResponseMessage PaysheetDetails(string dept, string desg, string year_month, string empcode)
        {

            if (dept == "undefined")
                dept = null;
            if (desg == "undefined")
                desg = null;
            if (year_month == "undefined")
                year_month = null;
            if (empcode == "undefined")
                empcode = null;
            List<payrollOnholdStatus> mod_list = new List<payrollOnholdStatus>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].Pays_mark_hold_for_employee",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@depcode", dept),
                            new SqlParameter("@desgcode", desg),
                            new SqlParameter("@year_month", year_month),
                            new SqlParameter("@emp_code", empcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            payrollOnholdStatus simsobj = new payrollOnholdStatus();
                            simsobj.emp_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.year_month = dr["pe_year_month"].ToString();
                            simsobj.year_month_name = dr["yearmonth"].ToString();
                            simsobj.pe_remark = dr["pe_remark"].ToString();
                            if (dr["pe_onhold_flag"].ToString() == "Y")
                                simsobj.onhold_status = true;
                            else
                                simsobj.onhold_status = false;
                            mod_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("EmpUpdateOnHoldFlag")]
        public HttpResponseMessage EmpUpdateLeaveBal(List<payrollOnholdStatus> lst)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (payrollOnholdStatus obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].Pays_mark_hold_for_employee",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@emp_code", obj.emp_code),
                            new SqlParameter("@year_month", obj.year_month),
                            new SqlParameter("@onhold_status", obj.onhold_status==true?"Y":"N"),
                            new SqlParameter("@pe_remark", obj.pe_remark),
                      });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }


        [Route("publishPayroll")]
        public HttpResponseMessage publishPayroll(List<payrollOnholdStatus> lst)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (payrollOnholdStatus obj in lst)
                    {
                      SqlDataReader dr = db.ExecuteStoreProcedure("pays.Pays_mark_hold_for_employee",
                      new List<SqlParameter>()
                       {
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@emp_code", obj.emp_code),
                            new SqlParameter("@year_month", obj.year_month),                                                        
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("publishEmailPayroll")]
        public HttpResponseMessage publishEmailPayroll(List<payrollOnholdStatus> lst)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (payrollOnholdStatus obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("pays.Pays_mark_hold_for_employee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@emp_code", obj.emp_code),
                            new SqlParameter("@year_month", obj.year_month),
                          });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }


    }
}