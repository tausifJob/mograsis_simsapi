﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;



namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/GoalTargetEvaluation")]
    public class GoalTargetEvaluationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_Student")]
        public HttpResponseMessage Get_Student()//string comp_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Student(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_Student"));

            List<Pers099> list = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_sip_goal_target_evaluation]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),  
                         });
                    
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 hrmsobj = new Pers099();
                            hrmsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            hrmsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_Employee")]
        public HttpResponseMessage Get_Employee()//string comp_code
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Employee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "Get_Employee"));

            List<Pers099> list = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_sip_goal_target_evaluation]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),  
                         });
                    
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099 simsobj = new Pers099();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_first_name = dr["em_first_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAllGoalTargetEvaluation")]
        public HttpResponseMessage getAllGoalTargetEvaluation(string comp_code, string aca_year, string goal_name, string goal_target_name) //, string employee_code, string student_enroll
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGoalTargetEvaluation(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getAllGoalTargetEvaluation"));

            List<Per305> list = new List<Per305>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_sip_goal_target_evaluation]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),  
                            new SqlParameter("@sh_company_code", comp_code),  
                            new SqlParameter("@sims_sip_academic_year", aca_year),
                            new SqlParameter("@sims_sip_goal_code", goal_name),
                            new SqlParameter("@sims_sip_goal_target_code", goal_target_name),
                            //new SqlParameter("@em_login_code", employee_code),
                           // new SqlParameter("@sims_enroll_number", student_enroll),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_goal_target_kpi_code = dr["sims_sip_goal_target_kpi_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_desc = dr["sims_sip_goal_target_kpi_desc"].ToString();

                            simsobj.sims_sip_goal_target_kpi_measure_code = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();

                            //simsobj.sims_sip_achievement_code = dr["sims_sip_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_code = dr["sims_sip_goal_target_kpi_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_desc = dr["sims_sip_achievement_desc"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }



            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("CUDGoalTargetEvaluation")]
        public HttpResponseMessage CUDGoalTargetEvaluation(List<Per301> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per301 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_sip_goal_target_evaluation]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_academic_year", simsobj.sims_sip_academic_year),
                                new SqlParameter("@sims_sip_goal_code", simsobj.sims_sip_goal_code),
                                new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_sip_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_code", simsobj.sims_sip_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_code", simsobj.sims_sip_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_user_code", simsobj.sims_sip_goal_target_kpi_measure_user_code),
                                                     
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_reviewer1_achievement_code", simsobj.sims_sip_goal_target_kpi_measure_reviewer1_achievement_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_reviewer2_achievement_code", simsobj.sims_sip_goal_target_kpi_measure_reviewer2_achievement_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_reviewer3_achievement_code", simsobj.sims_sip_goal_target_kpi_measure_reviewer3_achievement_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_reviewer4_achievement_code", simsobj.sims_sip_goal_target_kpi_measure_reviewer4_achievement_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_reviewer5_achievement_code", simsobj.sims_sip_goal_target_kpi_measure_reviewer5_achievement_code),
                                //new SqlParameter("@sims_sip_goal_target_kpi_measure_user_self_rating", simsobj.sims_sip_goal_target_kpi_measure_user_self_rating),

                                //new SqlParameter("@strength", simsobj.sims_sip_goal_target_reviewer1_strength),
                                //new SqlParameter("@weakness", simsobj.sims_sip_goal_target_reviewer1_weakness),
                                //new SqlParameter("@opportunity", simsobj.sims_sip_goal_target_reviewer1_opportunity),
                                //new SqlParameter("@threat", simsobj.sims_sip_goal_target_reviewer1_threat),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}

