﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/InActiveUser")]
    public class InActiveUserController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Search_Employee_by_first_name309")]
        public HttpResponseMessage Search_Employee_by_first_name309(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_first_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Employee_by_first_name309"));

            List<Pers309> list = new List<Pers309>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Employee_by_last_name309")]
        public HttpResponseMessage Search_Employee_by_last_name309(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_last_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Employee_by_last_name309"));

            List<Pers309> list = new List<Pers309>();

            if (last_name == null)
            {
                last_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Employee_by_login_id309")]
        public HttpResponseMessage Search_Employee_by_login_id309(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Employee_by_login_id309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Employee_by_login_id309"));

            List<Pers309> list = new List<Pers309>();

            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["EMP_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_first_name309")]
        public HttpResponseMessage Search_Student_by_first_name309(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_first_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Student_by_first_name309"));

            List<Pers309> list = new List<Pers309>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_last_name309")]
        public HttpResponseMessage Search_Student_by_last_name309(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_last_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Student_by_last_name309"));

            List<Pers309> list = new List<Pers309>();

            if (last_name == null)
            {
                last_name = "";
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Student_by_login_id309")]
        public HttpResponseMessage Search_Student_by_login_id309(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Student_by_login_id309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Student_by_login_id309"));

            List<Pers309> list = new List<Pers309>();
            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["comn_user_name"].ToString();
                            obj.user_name = dr["Stud_name"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_first_name309")]
        public HttpResponseMessage Search_Parent_by_first_name309(string first_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_first_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Parent_by_first_name309"));

            List<Pers309> list = new List<Pers309>();

            if (first_name == null)
            {
                first_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'I'),
                            new SqlParameter("@First_name", first_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_last_name309")]
        public HttpResponseMessage Search_Parent_by_last_name309(string last_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_last_name309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Parent_by_last_name309"));

            List<Pers309> list = new List<Pers309>();
            if (last_name == null)
            {
                last_name = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@last_name", last_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Search_Parent_by_login_id309")]
        public HttpResponseMessage Search_Parent_by_login_id309(string login)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Search_Parent_by_login_id309(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "InActiveUser", "Search_Parent_by_login_id309"));

            List<Pers309> list = new List<Pers309>();
            if (login == null)
            {
                login = "";
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("deactivate_user_account",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                            new SqlParameter("@login_code", login)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers309 obj = new Pers309();
                            obj.login_id = dr["sims_parent_number"].ToString();
                            obj.user_name = dr["NAME"].ToString();
                            obj.status = "Active";
                            list.Add(obj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
        }

        [Route("Insert_Parent_activation_history309")]
        public HttpResponseMessage Insert_Parent_activation_history309(List<Pers309> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Parent_activation_history309(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Parent", "Insert_Parent_activation_history309"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers309 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("deactivate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'L'),
                                new SqlParameter("@login_code", persobj.login_id),
                                new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.deactivation_date)),
                                new SqlParameter("@comn_user_remark", persobj.remark)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Student_activation_history309")]
        public HttpResponseMessage Insert_Student_activation_history309(List<Pers309> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Student_activation_history309(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Student", "Insert_Student_activation_history309"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers309 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("deactivate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'Y'),
                                new SqlParameter("@login_code", persobj.login_id),
                                new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.deactivation_date)),
                                new SqlParameter("@sims_student_status_updation_user_code", persobj.sims_student_status_updation_user_code),
                                new SqlParameter("@comn_user_remark", persobj.remark)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Insert_Employee_activation_history309")]
        public HttpResponseMessage Insert_Employee_activation_history309(List<Pers309> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Insert_Employee_activation_history309(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Parent", "Insert_Employee_activation_history309"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Pers309 persobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("deactivate_user_account",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", 'X'),
                                new SqlParameter("@login_code", persobj.login_id),
                                new SqlParameter("@sims_student_status_updation_date", db.DBYYYYMMDDformat(persobj.deactivation_date)),
                                new SqlParameter("@sims_student_status_updation_user_code", persobj.sims_student_status_updation_user_code),
                                new SqlParameter("@comn_user_remark", persobj.remark)
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}