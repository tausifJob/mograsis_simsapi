﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/generatepayroll")]
    public class GeneratePayrollController : ApiController
    {


        #region Payroll Generation

        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "M")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.dep_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dep_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.dg_code = dr["dg_code"].ToString();
                            hrmsObj.dg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAllPaysGradeName")]
        public HttpResponseMessage GetAllPaysGradeName(string company_code)
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                    new List<SqlParameter>() 
                    { 
                        new SqlParameter("@opr", "F"),
                        new SqlParameter("@em_company_code", company_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.gr_name = dr["gr_desc"].ToString();
                            hrmsObj.gr_code = dr["gr_code"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetCurrentFinYearFromFinsParameter")]
        public HttpResponseMessage GetCurrentFinYearFromFinsParameter()
        {
            List<Pers078> lst = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "H")
                });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            Pers078 obj = new Pers078();
                            obj.fin_year = dr["fins_appl_form_field_value1"].ToString();
                            obj.fin_year_desc = dr["fins_appl_form_field_value3"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("Get_alert_messages")]
        public HttpResponseMessage Get_alert_messages()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'A')
                         });
                    while (dr.Read())
                    {
                        Pers078 hrmsObj = new Pers078();
                        hrmsObj.msg_code = dr["pays_appl_parameter"].ToString();
                        hrmsObj.msg_desc = dr["pays_appl_form_field_value2"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetCashBankTags")]
        public HttpResponseMessage GetCashBankTags()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'Z')
               

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.bct_name = dr["sims_appl_form_field_value1"].ToString();
                            hrmsObj.bct_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("Get_employee_service_status")]
        public HttpResponseMessage Get_employee_service_status()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'Y')
               

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.service_Status_desc = dr["sims_appl_form_field_value1"].ToString();
                            hrmsObj.service_Status_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetMonth")]
        public HttpResponseMessage GetMonth()
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'J')
               

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.sd_year_month_code = dr["pays_appl_parameter"].ToString();
                            hrmsObj.sd_year_month_name = dr["pays_appl_form_field_value1"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("EmployeeNamesByGrade")]
        public HttpResponseMessage EmployeeNamesByGrade(Pers078 obj)
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'C'),
                   new SqlParameter("@year_month", obj.sd_year_month),
                   new SqlParameter("@grade_list", obj.gr_code),
                   new SqlParameter("@cash_tag", obj.em_bank_cash_tag),
                   new SqlParameter("@service_status_list", obj.service_Status_code),
                   new SqlParameter("@dep_code", obj.dep_code),
                   new SqlParameter("@dg_code", obj.dg_code),
                

              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.emp_name = dr["EmpName"].ToString();
                            hrmsObj.emp_number = dr["em_number"].ToString();
                            hrmsObj.emp_login_code = dr["em_login_code"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("chk_payroll_genrated_for_current_month")]
        public HttpResponseMessage chk_payroll_genrated_for_current_month(List<Pers078> emp_list)
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                string emp = "", ym = "";
                foreach (Pers078 item in emp_list)
                {
                    emp = emp + item.emp_number + "/";
                    ym = item.sd_year_month;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
              
                new SqlParameter("@opr", 'Q'),
               new SqlParameter("@year_month", ym),
                new SqlParameter("@emp_list", emp)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsObj = new Pers078();
                            hrmsObj.emp_number = dr["pe_number"].ToString();
                            hrmsObj.emp_login_code = dr["em_login_code"].ToString();
                            hrmsObj.emp_name = dr["emp_name"].ToString();

                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("chk_payroll_genrated_for_last_month")]
        public HttpResponseMessage chk_payroll_genrated_for_last_month(List<Pers078> emp_list)
        {
            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                string emp = "", ym = "";
                foreach (Pers078 item in emp_list)
                {
                    emp = emp + item.emp_number + "/";
                    ym = item.sd_year_month;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'R'),
                new SqlParameter("@year_month", ym),
                new SqlParameter("@emp_list", emp)
               });

                    while (dr.Read())
                    {
                        Pers078 hrmsObj = new Pers078();
                        hrmsObj.emp_lst1 = dr["item1"].ToString();
                        mod_list.Add(hrmsObj);
                    }

                    dr.NextResult();
                    while (dr.Read())
                    {
                        Pers078 hrmsObj = new Pers078();
                        hrmsObj.emp_lst2 = dr["item2"].ToString();
                        mod_list.Add(hrmsObj);
                    }

                }

            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("update_att_and_ot_status")]
        public HttpResponseMessage update_att_and_ot_status(string parameter, bool status)
        {
            bool chk = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_payroll_empl_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'B'),
                new SqlParameter("@parameter", parameter),
                    new SqlParameter("@status",status.Equals(true)?"Y":"N")

              });
                    if (dr.RecordsAffected > 0)
                    {
                        chk = true;
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, chk);

        }

        [Route("Generate_Payroll")]
        public HttpResponseMessage Generate_Payroll(List<Pers078> emp_list)
        {
            string year_month = ""; string emp = ""; string grade_list = ""; string em_bank_cash_tag = ""; string empldate = "";
            string finlize_date = "";
            string finlize_status = "";
            string include_air_fare = "";
            string include_leave_salary = "";

            List<Pers078> mod_list = new List<Pers078>();
            try
            {
                foreach (Pers078 item in emp_list)
                {
                    emp = emp + item.emp_number + "/";
                    year_month = item.sd_year_month;
                    grade_list = item.gr_code;
                    em_bank_cash_tag = item.bct_code;
                    empldate = item.publishdata;
                    include_air_fare = item.include_air_fare.Equals(true) ? "Y" : "N";
                    include_leave_salary = item.include_leave_salary.Equals(true) ? "Y" : "N";

                    if (item.finalize_date == "")
                        finlize_date = null;
                    else
                        finlize_date = item.finalize_date;
                    if (item.finalize_status)
                        finlize_status = "1";
                    else
                        finlize_status = "0";

                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[payroll_generation_proc]",
                    new List<SqlParameter>() 
                    {                  
                         new SqlParameter("@year_month", year_month),
                         new SqlParameter("@visible_empl_from", db.DBYYYYMMDDformat(empldate)),
                         new SqlParameter("@emp_list", emp),
                         new SqlParameter("@grade_list", grade_list),
                         new SqlParameter("@em_bank_cash_tag", em_bank_cash_tag),
                         new SqlParameter("@pl_finalize_date",  db.DBYYYYMMDDformat(finlize_date)),
                         new SqlParameter("@pl_finalize_status", finlize_status),
                         new SqlParameter("@include_air_fare", include_air_fare),
                         new SqlParameter("@include_leave_salary", include_leave_salary)
                    });
                    while (dr.Read())
                    {
                        Pers078 hrmsObj = new Pers078();
                        try
                        {
                            hrmsObj.emp_login_code = dr["em_login_code"].ToString();
                        }
                        catch (Exception ex)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
                        }
                        hrmsObj.emp_number = dr["sd_number"].ToString();
                        hrmsObj.emp_name = dr["ename"].ToString();
                        hrmsObj.earn = dr["earn"].ToString();
                        hrmsObj.ded = dr["Ded"].ToString();
                        hrmsObj.total = (Decimal.Parse(dr["earn"].ToString()) - Decimal.Parse(dr["Ded"].ToString())).ToString();

                        hrmsObj.total_employees = 0;
                        hrmsObj.payroll_generated = 0;
                        hrmsObj.active_employees = 0;
                        hrmsObj.employees_on_vacation = 0;
                        hrmsObj.currently_generated = 0;
                        mod_list.Add(hrmsObj);
                    }

                    Pers078 Obj = new Pers078();

                    #region Total Employees
                    dr.NextResult();
                    if (dr.Read())
                    {
                        Obj.total_employees = int.Parse(dr["total_employees"].ToString());
                    }
                    #endregion

                    #region Active Employees
                    dr.NextResult();
                    if (dr.Read())
                    {
                        Obj.active_employees = int.Parse(dr["active_employees"].ToString());
                    }
                    #endregion

                    #region Employees On Vacation
                    dr.NextResult();
                    if (dr.Read())
                    {
                        Obj.employees_on_vacation = int.Parse(dr["employees_on_vacation"].ToString());
                    }
                    #endregion

                    #region Payroll Generated For
                    dr.NextResult();
                    if (dr.Read())
                    {
                        Obj.payroll_generated = int.Parse(dr["payroll_generated"].ToString());
                    }
                    #endregion
                    #region Currently Payroll Generated For
                    dr.NextResult();
                    if (dr.Read())
                    {
                        Obj.currently_generated = int.Parse(dr["currently_generated"].ToString());
                    }
                    #endregion

                    mod_list.Add(Obj);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        #endregion
    }
}