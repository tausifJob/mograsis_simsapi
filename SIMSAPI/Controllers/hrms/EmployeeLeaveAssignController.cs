﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;


namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmployeeLeave")]
    public class EmployeeLeaveAssignController : ApiController
    {

        #region Pers260

        [Route("getdept_design")]
        public HttpResponseMessage getdept_design(string company_code, string dept_code)
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@opr_mem", ""),
                            new SqlParameter("@tbl_cond", company_code),
                             new SqlParameter("@tbl_cond1", dept_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 hrmsObj = new Pers260();
                            hrmsObj.dg_desc = dr["dg_desc"].ToString();
                            hrmsObj.dg_code = dr["em_desg_code"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getGrade")]
        public HttpResponseMessage getGrade(string company_code)
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@comp_code", company_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 hrmsObj = new Pers260();
                            hrmsObj.gr_code = dr["gr_code"].ToString();
                            hrmsObj.gr_desc = dr["gr_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getGender")]
        public HttpResponseMessage getGender()
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                            new SqlParameter("@opr_mem", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 hrmsObj = new Pers260();
                            hrmsObj.sims_appl_parameter_gender = dr["sims_appl_parameter"].ToString();
                            hrmsObj.sims_appl_form_field_value1_gender = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getOrderbynames")]
        public HttpResponseMessage getOrderbynames()
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 hrmsObj = new Pers260();
                            hrmsObj.orderby_col = dr["pays_appl_parameter"].ToString();
                            hrmsObj.orderby_name = dr["pays_appl_form_field_value1"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAllLeaveType")]
        public HttpResponseMessage GetAllLeaveType()
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 simsobj = new Pers260();
                            simsobj.emp_leave_code = dr["cl_code"].ToString();
                            simsobj.emp_leave_name = dr["cl_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetEmployeeLeaveAssign")]
        public HttpResponseMessage GetEmployeeLeaveAssign(string simsobj)
        {
            Pers260 data = new Pers260();
            if (simsobj == "undefined" || simsobj == "\"\"")
            {
                simsobj = null;
            }
            else
            {
                data = Newtonsoft.Json.JsonConvert.DeserializeObject<Pers260>(simsobj);
                if (data.emp_code == "")
                {
                    data.emp_code = null;
                }
            }
            string str = "";
            List<Pers260> hrms_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@comp_code", data.co_company_code),
                new SqlParameter("@dept_code", data.codp_dept_no),
                new SqlParameter("@desi_code", data.dg_code),
                new SqlParameter("@gender_code", data.emp_gender_code),
                new SqlParameter("@leave_codeList", ""),
                new SqlParameter("@emp_code",data.emp_code),
                new SqlParameter("@emp_name",data.emp_name),
                new SqlParameter("@em_staff_type",data.staff_code),
                new SqlParameter("@orderby",data.orderby_col),
                new SqlParameter("@gr_code",data.gr_code),
                new SqlParameter("@doj_date",db.DBYYYYMMDDformat(data.em_date_of_join)),
                 new SqlParameter("@religion_code",data.sims_religion_code),
                });
                    while (dr.Read())
                    {
                        Pers260 hrmsobj = new Pers260();
                        Leave leav = new Leave();
                        str = dr["em_login_code"].ToString();
                        var v = (from p in hrms_list where p.emp_code == str select p);

                        if (v.Count() == 0)
                        {
                            hrmsobj.leavelist = new List<Leave>();
                            leav.emp_leave_code = dr["cl_code"].ToString();
                            leav.emp_leave_name = dr["cl_desc"].ToString();
                            leav.emp_leave_status = dr["Status"].Equals("T") ? true : false;
                            leav.ischange_new = leav.emp_leave_status;
                            hrmsobj.employee_name = dr["EmployeeName"].ToString();
                            hrmsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["dt"].ToString());
                            hrmsobj.emp_code = dr["em_login_code"].ToString();
                            hrmsobj.leave_code = dr["cl_code"].ToString();
                            hrmsobj.emp_leave_name = dr["cl_desc"].ToString();
                            //hrmsobj.emp_leave_status = dr["Status"].Equals("T") ? true : false;
                            hrms_list.Add(hrmsobj);

                            hrmsobj.leavelist.Add(leav);
                        }
                        else
                        {

                            leav.emp_leave_code = dr["cl_code"].ToString();
                            leav.emp_leave_name = dr["cl_desc"].ToString();
                            leav.emp_leave_status = dr["Status"].Equals("T") ? true : false;
                            leav.ischange_new = leav.emp_leave_status;
                            v.ElementAt(0).leavelist.Add(leav);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, hrms_list);
        }

        [Route("InsertEmployeeLeaveAssign")]
        public HttpResponseMessage InsertEmployeeLeaveAssign(List<Pers260> lst)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int i = 1;
                    foreach (Pers260 obj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@comp_code", obj.co_company_code),
                                new SqlParameter("@dept_code", obj.codp_dept_no),
                                new SqlParameter("@desi_code", obj.dg_code),
                                new SqlParameter("@gender_code", obj.sims_appl_parameter),
                                new SqlParameter("@leave_codeList", obj.leave_name),//@delOpr
                                new SqlParameter("@emp_code", obj.emp_code),
                                new SqlParameter("@leave_delete_list", obj.leave_delete_list),
                                new SqlParameter("@delOpr", i),
                                new SqlParameter("@user_name", obj.user_name)
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("getReligion")]
        public HttpResponseMessage getReligion()
        {
            List<Pers260> mod_list = new List<Pers260>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_EmployeeLeaveAssign_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers260 hrmsObj = new Pers260();
                            hrmsObj.sims_religion_code = dr["sims_religion_code"].ToString();
                            hrmsObj.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        #endregion
    }
}