﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/OvertimeHour")]
    public class OvertimeHourController:ApiController
    {
        [Route("Company")]
        public HttpResponseMessage Company()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.comp_code = dr["co_company_code"].ToString();
                            simsobj.comp_name = dr["co_desc"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
              //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Department")]
        public HttpResponseMessage Department()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","D"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Designation")]
        public HttpResponseMessage Designation()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","E"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.desg_code = dr["dg_code"].ToString();
                            simsobj.desg_name = dr["dg_desc"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Grade")]
        public HttpResponseMessage Grade()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.grade_code = dr["gr_code"].ToString();
                            simsobj.grade_name = dr["gr_desc"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("Year_month")]
        public HttpResponseMessage Year_month()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.year_month = dr["year_month"].ToString();
                            simsobj.year_month_name = dr["year_month_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("GetYear_month")]
        public HttpResponseMessage GetYear_month(string financial_year)
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                     new List<SqlParameter>()
                     {
                        new SqlParameter("@opr","N"),
                        new SqlParameter("@financial_year",financial_year)
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ot_hour simsobj = new ot_hour();
                            simsobj.year_month = dr["year_month"].ToString();
                            simsobj.year_month_name = dr["year_month_name"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("OTtype")]
        public HttpResponseMessage OTtype()
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.ot_type = dr["pays_appl_parameter"].ToString();
                            simsobj.ot_name = dr["pays_appl_form_field_value1"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("EmployeeOT")]
        public HttpResponseMessage EmployeeOT(ot_hour obj)
        {

            List<ot_hour> employee_list = new List<ot_hour>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@emp_id",obj.emp_id),
                            new SqlParameter("@comp_code",obj.comp_code),
                            new SqlParameter("@dept_code",obj.dept_code),
                            new SqlParameter("@desg_code",obj.desg_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                            new SqlParameter("@year_month",obj.year_month),
                            new SqlParameter("@noh_ot_type",obj.ot_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ot_hour simsobj = new ot_hour();
                            simsobj.login_code = dr["em_login_code"].ToString();
                            simsobj.emp_id = dr["em_number"].ToString();
                            simsobj.emp_name = dr["em_first_name"].ToString();
                            simsobj.ot_type = dr["pays_appl_parameter"].ToString();
                            simsobj.ot_name = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.ot_hou = dr["noh_ot_hou"].ToString();

                            employee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, employee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, employee_list);
        }

        [Route("UpdateOT")]
        public HttpResponseMessage UpdateOT(List<ot_hour> obj)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (ot_hour data in obj)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_ot_hour_proc]",
                            new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "I"),
                new SqlParameter("@comp_code", data.comp_code),
                new SqlParameter("@emp_id", data.emp_id),
                new SqlParameter("@ot_hour", data.ot_hou),
                new SqlParameter("@year_month", data.year_month),
                new SqlParameter("@noh_ot_type", data.ot_type)
               
                 });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}