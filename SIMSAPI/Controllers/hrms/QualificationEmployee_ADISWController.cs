﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.QualificationEmployeeController
{
    [RoutePrefix("api/Qualification")]
    [BasicAuthentication]
    public class QualificationEmployee_ADISWController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllQualification")]
        public HttpResponseMessage getAllQualification(string data)
        {
            Per258 data1 = new Per258();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Per258>(data);
            }
            List<Per258> desg_list = new List<Per258>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@qcode",data1.emp_qual_qual_code),
                            new SqlParameter("@co_code",data1.emp_qual_company_code),
                            new SqlParameter("@de_code",data1.emp_qual_dept_code),
                            new SqlParameter("@emp_name_id",data1.emp_name_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_company_name = dr["co_desc"].ToString();
                            simsobj.emp_qual_company_code = dr["em_company_code"].ToString();
                            simsobj.emp_qual_dept_code = dr["em_dept_code"].ToString();
                            simsobj.emp_qual_em_code = dr["pays_qualification_emp_id"].ToString();
                            simsobj.emp_qual_qual_code = dr["pays_qualification_code"].ToString();

                            simsobj.emp_qual_institute = dr["pays_qualification_institude_name"].ToString();
                            simsobj.emp_qual_type_code = dr["pays_qualification_type"].ToString();
                            simsobj.emp_qual_type_name = dr["qualification_type"].ToString();

                            simsobj.em_qual_from_year = dr["pays_qualification_year_start_year"].ToString();
                            simsobj.emp_qual_percentage = dr["pays_qualification_percentage"].ToString();

                            simsobj.emp_qual_remark = dr["pays_qualification_remark"].ToString();
                            simsobj.em_qual_year = dr["pays_qualification_year"].ToString();
                            // simsobj.em_qual_from_to_year = dr["from_to_year"].ToString();
                            simsobj.em_qual_status = dr["pays_qualification_emp_status"].Equals("A") ? true : false;
                            simsobj.emp_qual_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.emp_qual_em_name = dr["ename"].ToString();
                            simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();
                            desg_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getEmployeeName")]
        public HttpResponseMessage getEmployeeName(string em_company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeeName()PARAMETERS ::data{1}";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getEmployeeName", em_company_code));

            List<Per258> mod_list = new List<Per258>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            //new SqlParameter("@em_dept_code",em_dept_code),
                            new SqlParameter("@em_company_code",em_company_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_em_code = dr["em_login_code"].ToString();
                            simsobj.emp_qual_em_name = dr["EmpName"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAllEmployeeName")]
        public HttpResponseMessage getEmployeeName()
        {

            List<Per258> mod_list = new List<Per258>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_em_code = dr["em_login_code"].ToString();
                            simsobj.emp_qual_em_name = dr["em_first_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getQualificationByEmployee")]
        public HttpResponseMessage getQualificationByEmployee(string emp_code)
        {
            List<Per258> mod_list = new List<Per258>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                            new SqlParameter("@pays_qualification_emp_id",emp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_em_code = dr["pays_qualification_emp_id"].ToString();
                            simsobj.emp_qual_qual_code = dr["pays_qualification_code"].ToString();
                            simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();
                            //simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();
                            //simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();
                            //simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();
                            simsobj.emp_qual_remark = dr["pays_qualification_remark"].ToString();
                            simsobj.em_qual_from_year = dr["pays_qualification_year_start_year"].ToString();
                            simsobj.emp_qual_institute = dr["pays_qualification_institude_name"].ToString();
                            simsobj.emp_qual_type_code = dr["pays_qualification_type"].ToString();
                            simsobj.emp_qual_percentage = dr["pays_qualification_percentage"].ToString();
                            simsobj.em_qual_to_year = dr["pays_qualification_year"].ToString();
                            simsobj.em_qual_status = dr["pays_qualification_emp_status"].Equals("A") ? true : false;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("getQualificationByApplicant")]
        public HttpResponseMessage getQualificationByApplicant(string applicant_id)
        {
            List<Per258> mod_list = new List<Per258>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@pays_qualification_applicant_id",applicant_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_em_code = dr["pays_qualification_emp_id"].ToString();
                            simsobj.emp_qual_qual_code = dr["pays_qualification_code"].ToString();
                            simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString();                            
                            simsobj.emp_qual_remark = dr["pays_qualification_remark"].ToString();
                            simsobj.em_qual_from_year = dr["pays_qualification_year_start_year"].ToString();
                            simsobj.emp_qual_institute = dr["pays_qualification_institude_name"].ToString();
                            simsobj.emp_qual_type_code = dr["pays_qualification_type"].ToString();
                            simsobj.emp_qual_percentage = dr["pays_qualification_percentage"].ToString();
                            simsobj.em_qual_to_year = dr["pays_qualification_year"].ToString();
                            simsobj.em_qual_status = dr["pays_qualification_emp_status"].Equals("A") ? true : false;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDQualification")]
        public HttpResponseMessage CUDQualification(List<Per258> simsobj1)
        {
            Message message = new Message();
            foreach (Per258 simsobj in simsobj1)
            {
                try
                {
                    if (simsobj != null)
                    {

                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                                new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter ("@pays_qualification_applicant_id",simsobj.emp_qual_em_code),
                                new SqlParameter("@pays_qualification_emp_id", simsobj.emp_qual_em_code),
                                new SqlParameter("@pays_qualification_code", simsobj.emp_qual_qual_code),
                                new SqlParameter("@dg_code_list", simsobj.emp_qual_qual_code),
                                new SqlParameter("@pays_qualification_institute",simsobj.emp_qual_institute),
                                new SqlParameter("@pays_qualification_type",simsobj.emp_qual_type_code),
                                new SqlParameter("@pays_qualification_percentage",simsobj.emp_qual_percentage),
                                new SqlParameter("@pays_qualification_from_year", simsobj.em_qual_from_year),
                                new SqlParameter("@pays_qualification_year",simsobj.em_qual_year),
                                new SqlParameter("@pays_qualification_remark", simsobj.emp_qual_remark),
                                new SqlParameter("@pays_qualification_emp_status", simsobj.em_qual_status.Equals(true)?"A":"I"),
                         });
                            if (dr.RecordsAffected > 0)
                            {

                                if (simsobj.opr.Equals("I"))
                                    message.strMessage = "Qualification Employee Added Sucessfully!!";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }
                        }
                    }

                    else
                    {
                        message.strMessage = "Error In Parsing Information!!";
                        message.messageType = MessageType.Error;
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                    }
                }
                catch (Exception x)
                {
                    /*if (simsobj.opr.Equals("U"))
                        message.strMessage = "Error In Updating Qualification Employee!!";
                    else if (simsobj.opr.Equals("I"))
                        message.strMessage = "Error In Adding Qualification Employee!!";
                    else if (simsobj.opr.Equals("D"))
                        message.strMessage = "Error In Deleting Qualification Employee!!";*/
                    return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UpadateQualification")]
        public HttpResponseMessage UpadateQualification(List<Per258> simsobj2)
        {

            Message message = new Message();
            foreach (Per258 simsobj in simsobj2)
            {
                try
                {

                    if (simsobj != null)
                    {

                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                                new List<SqlParameter>()
                         { 
                                //new SqlParameter("@opr", simsobj.opr),
                                //new SqlParameter("@pays_qualification_emp_id", simsobj.emp_qual_em_code),
                                //new SqlParameter("@pays_qualification_code", simsobj.emp_qual_qual_code),
                                //new SqlParameter("@dg_code_list", simsobj.emp_qual_qual_code),
                                //new SqlParameter("@pays_qualification_year", simsobj.em_qual_year),
                                //new SqlParameter("@pays_qualification_remark", simsobj.emp_qual_remark),
                                //new SqlParameter("@pays_qualification_emp_status", simsobj.em_qual_status .Equals(true)?"A":"I"),
                                 new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter ("@pays_qualification_applicant_id",simsobj.emp_qual_em_code),
                                new SqlParameter("@pays_qualification_emp_id", simsobj.emp_qual_em_code),
                                new SqlParameter("@pays_qualification_code", simsobj.emp_qual_qual_code),
                                new SqlParameter("@dg_code_list", simsobj.emp_qual_qual_code),
                                new SqlParameter("@pays_qualification_institute",simsobj.emp_qual_institute),
                                new SqlParameter("@pays_qualification_type",simsobj.emp_qual_type_code),
                                new SqlParameter("@pays_qualification_percentage",simsobj.emp_qual_percentage),
                                new SqlParameter("@pays_qualification_from_year", simsobj.em_qual_from_year),
                                new SqlParameter("@pays_qualification_year",simsobj.em_qual_year),
                                new SqlParameter("@pays_qualification_remark", simsobj.emp_qual_remark),
                                new SqlParameter("@pays_qualification_emp_status", simsobj.em_qual_status.Equals(true)?"A":"I"),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                if (simsobj.opr.Equals("U"))
                                {
                                    message.strMessage = "Qualification Employee Updated Sucessfully!!";
                                }
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }
                        }
                    }

                    else
                    {
                        message.strMessage = "Error In Parsing Information!!";
                        message.messageType = MessageType.Error;
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                    }

                }

                catch (Exception x)
                {
                    if (simsobj.opr.Equals("U"))
                        message.strMessage = "Error In Updating Qualification Employee!!";
                    else if (simsobj.opr.Equals("I"))
                        message.strMessage = "Error In Adding Qualification Employee!!";
                    else if (simsobj.opr.Equals("D"))
                        message.strMessage = "Error In Deleting Qualification Employee!!";
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("DelQualification")]
        public HttpResponseMessage DelQualification(List<Per258> simsobj3)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per258 simsobj in simsobj3)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.[pays_qualification_empl_proc]",
                           new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_qualification_emp_id", simsobj.emp_qual_em_code),

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Ischeck")]
        public HttpResponseMessage Ischeck(string emp_code)
        {
            List<Per258> mod_list = new List<Per258>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_qualification_empl_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                             new SqlParameter("@pays_qualification_emp_id", emp_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_em_code = dr["pays_qualification_emp_id"].ToString();
                            simsobj.emp_qual_qual_code = dr["pays_qualification_code"].ToString();
                            simsobj.emp_qual_remark = dr["pays_qualification_remark"].ToString();
                            simsobj.em_qual_year = dr["pays_qualification_year"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getQualificationName")]
        public HttpResponseMessage getQualificationName()
        {
            List<Per258> mod_list = new List<Per258>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'H'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_qual_code = dr["pays_qualification_code"].ToString();
                            simsobj.emp_qual_qual_name = dr["pays_qualification_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getQualificationType")]
        public HttpResponseMessage getQualificationType()
        {
            List<Per258> mod_list = new List<Per258>();
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_qualification_empl_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per258 simsobj = new Per258();
                            simsobj.emp_qual_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.emp_qual_type_name = dr["sims_appl_form_field_value1"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("CQualification")]
        public HttpResponseMessage CQualification(List<Per258> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per258 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_qualification_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_qualification_code", simsobj.pays_qualification_code),
                                new SqlParameter("@pays_qualification_desc", simsobj.pays_qualification_desc),
                                new SqlParameter("@pays_qualification_desc_ar", simsobj.pays_qualification_desc_arabic),                                
                                new SqlParameter("@pays_qualificaion_display_oder", simsobj.pays_qualificaion_display_oder),
                                new SqlParameter("@pays_qualificaiton_status", simsobj.pays_qualificaiton_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDQualificationApplicant")]
        public HttpResponseMessage CUDQualificationApplicant(List<Per258> simsobj1)
        {
            Message message = new Message();
            foreach (Per258 simsobj in simsobj1)
            {
                try
                {
                    if (simsobj != null)
                    {

                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();


                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.[pays_qualification_empl_proc]",
                            new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_qualification_applicant_id",simsobj.pays_qualification_applicant_id),
                                new SqlParameter("@pays_qualification_emp_id", simsobj.emp_qual_em_code),
                                new SqlParameter("@pays_qualification_code", simsobj.emp_qual_qual_code),
                                new SqlParameter("@dg_code_list", simsobj.emp_qual_qual_code),
                                new SqlParameter("@pays_qualification_institute",simsobj.emp_qual_institute),
                                new SqlParameter("@pays_qualification_type",simsobj.emp_qual_type_code),
                                new SqlParameter("@pays_qualification_percentage",simsobj.emp_qual_percentage),
                                new SqlParameter("@pays_qualification_from_year", simsobj.em_qual_from_year),
                                new SqlParameter("@pays_qualification_year",simsobj.em_qual_year),
                                new SqlParameter("@pays_qualification_remark", simsobj.emp_qual_remark),
                                new SqlParameter("@pays_qualification_emp_status", simsobj.em_qual_status.Equals(true)?"A":"I"),
                         });
                            if (dr.RecordsAffected > 0)
                            {

                                if (simsobj.opr.Equals("I"))
                                    message.strMessage = "Qualification Employee Added Sucessfully!!";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }
                        }
                    }

                    else
                    {
                        message.strMessage = "Error In Parsing Information!!";
                        message.messageType = MessageType.Error;
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                    }
                }
                catch (Exception x)
                {                   
                    return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

    }
}

