﻿using log4net;
using SIMSAPI.Helper;

using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmpReRegistration")]

    public class EmpReRegistrationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getEmployeDetails")]
        public HttpResponseMessage getEmployeDetails(string login_code)
        {            
            List<Pers306> emp_list = new List<Pers306>();           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.pays_emp_re_registration_proc",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'S'),
                        new SqlParameter("@em_login_code",login_code)                                
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers306 invsobj = new Pers306();
                                invsobj.em_number = dr["em_number"].ToString();
                                invsobj.em_login_code = dr["em_login_code"].ToString();                                
                                invsobj.em_full_name = dr["em_full_name"].ToString();                                
                                invsobj.em_date_of_join = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                                invsobj.dg_desc = dr["designation_name"].ToString();                                
                                invsobj.com_name = dr["company_name"].ToString();                                
                                invsobj.dep_name = dr["dept_name"].ToString();                                                                    
                                invsobj.reg_date = db.UIDDMMYYYYformat(dr["em_left_date"].ToString());
                                invsobj.reg_resion = dr["em_left_reason"].ToString();
                                if (dr["em_status"].ToString() == "Y")
                                    invsobj.em_status = true;
                                else
                                    invsobj.em_status = false;
                                
                            emp_list.Add(invsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {                
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }
        
        [Route("ReregisterEmployee")]
        public HttpResponseMessage ReregisterEmployee(Pers306 persobj)
        {
            
            // Pers306 persobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Pers306>(data);
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("pays.pays_emp_re_registration_proc",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@em_login_code", persobj.em_login_code),
                            new SqlParameter("@em_number", persobj.em_number),
                            new SqlParameter("@em_activation_date",db.DBYYYYMMDDformat(persobj.activation_date)),
                            new SqlParameter("@em_remark",persobj.remark),
                        });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("SearchResignedEmployee")]
        public HttpResponseMessage SearchResignedEmployee(string data)
        {

            Sims189 obj = new Sims189();

            List<Sims189> Emp_List = new List<Sims189>();

            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims189>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.pays_emp_re_registration_proc",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'E'),                            
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),
                            new SqlParameter("@teacher_name",obj.em_department),
                            new SqlParameter("@em_desg_code",obj.em_desg_code),
                            new SqlParameter("@codp_comp_code",obj.company_code),
                            new SqlParameter("@resignedEmpFlag",obj.resignedEmpFlag),                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();

                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_department = dr["dept_name"].ToString();
                            simsobj.em_desg_code = dr["desg_name"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();

                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            simsobj.sims_student_enroll_number = dr["emp_child"].ToString();

                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }

    }
}