﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using Telerik.Reporting.Processing;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.HRMS
{
    [RoutePrefix("api/EmployeePayslip")]
    public class EmailEmployeePayslipController : ApiController
    {

        [Route("getpaysyear")]
        public HttpResponseMessage getpaysyear()
        {
            List<employeepayslip> year_lst = new List<employeepayslip>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_email_payslip_proc]", 
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S"),
                            new SqlParameter("@comp_code", "1")
                         }

                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            employeepayslip obj = new employeepayslip();

                            obj.year = dr["year1"].ToString();

                            year_lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, year_lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, year_lst);
        }

        [Route("getpaysmonth")]
        public HttpResponseMessage getpaysmonth()
        {
            List<employeepayslip> month_lst = new List<employeepayslip>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_email_payslip_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "B"),
                            new SqlParameter("@comp_code", "1")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            employeepayslip obj = new employeepayslip();

                            obj.monthname = dr["monthname1"].ToString();
                            obj.month = dr["mnth"].ToString();

                            month_lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, month_lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, month_lst);
        }

        [Route("getpaysemployee")]
        public HttpResponseMessage getpaysemployee(string year,string month)
        {
            List<employeepayslip> emp_lst = new List<employeepayslip>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_email_payslip_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "A"),
                            new SqlParameter("@comp_code", "1"),
                            new SqlParameter("@year", year),
                            new SqlParameter("@month", month)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            employeepayslip obj = new employeepayslip();

                            obj.empid = dr["empid"].ToString();
                            obj.empname = dr["Empname"].ToString();
                            obj.empemailid = dr["Empemail"].ToString();
                            obj.empmobile = dr["Empmobile"].ToString();

                            emp_lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, emp_lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, emp_lst);
        }

        [Route("getsend_payslipby_email")]
        public HttpResponseMessage getsend_payslipby_email(string year, string month, string empemailid)
        {
            string ret = "1";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_email_payslip_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@Opr", "E"),
                            new SqlParameter("@year", year),
                            new SqlParameter("@month", month),
                            new SqlParameter("@empid_str", empemailid)
                         }
                         );
                    if (dr.HasRows)
                    {
                        ret = "2";
                    }
                }
            }
            catch (Exception dee)
            { }

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }

        [Route("sendpayslipemail")]
        public HttpResponseMessage sendpayslipemail(string year, string month, string empemailid)
        {
            string ret = "1";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_email_payslip_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "E"),
                            new SqlParameter("@year", year),
                            new SqlParameter("@month", month),
                            new SqlParameter("@empid_str", empemailid)
                         }
                         );
                    if (dr.HasRows)
                    {
                        ret = "2";
                    }
                }
            }
            catch (Exception dee)
            { }

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }
        
        //[Route("sendpayslipemail")]
        //public HttpResponseMessage sendpayslipemail(string year, string month, string empid,string empemailid)
        //{
        //    string ret = "1";
        //    try
        //    {
        //        //SimsReports.Payroll rfile = new SimsReports.Payroll.PERR94();
        //        SimsReports.Payroll.PERR94 rfile = new SimsReports.Payroll.PERR94();
        //        SaveReport(rfile, year, month, empid, empemailid);

        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, ret);
        //}

        //private void SaveReport(Telerik.Reporting.Report report, string year, string month, string empid,string empemailid)
        //{
        //    try
        //    {
        //        ReportProcessor reportProcessor = new ReportProcessor();

        //        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        //        instanceReportSource.ReportDocument = report;
        //        report.ReportParameters["paysheet_year"].Value = year;
        //        report.ReportParameters["paysheet_month"].Value = month;
        //        report.ReportParameters["search"].Value = empid;
        //        string fileName = HttpContext.Current.Server.MapPath("..\\..\\Docs\\Attachments\\" + year + "_" + month + "_" + empid + ".tiff");
        //        string fileName1 = year + "_" + month + "_" + empid + ".tiff";

        //        RenderingResult result = reportProcessor.RenderReport("IMAGE", instanceReportSource, null);

        //        using (FileStream fs = new FileStream(fileName, FileMode.Create))
        //        {
        //            fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        //            fs.Close();
        //            fs.Dispose();
        //        }

        //        report.Dispose();
        //        reportProcessor = null;
        //        result = null;

        //        string EmailSubject = "Invoice Details";
        //        //string Email_Body = "Dear Parent,<p>Please find attached the copy of Invoice for academic year " + academic_year + ".</p><br><p>Regards,<br>Star International School</p>";
        //        string Email_Body = "<style type='text/css'>p { margin-top: 0px;margin-bottom: 12px;line-height: 1.15; } body { font-family: 'Segoe UI';font-style: Normal;font-weight: normal;font-size: 12px; } " +
        //        ".Normal { telerik-style-type: paragraph;telerik-style-name: Normal;border-collapse: collapse; } .TableNormal { telerik-style-type: table;telerik-style-name: TableNormal;border-collapse: collapse; }" +
        //        "</style><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>Dear Parents, </span></p><p class='Normal'>" +
        //        "<span style='font-family: 'Calibri';font-style: Normal;font-weight: bold;font-size: 12px;text-decoration: underline;'>SCHOOL FEES:</span>" +
        //        "<span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>" +
        //        "The School fees for your child has to be paid before 1st September 2016. If you would like to make the payment now, the Accountant is available in the School from 8.30am to 1pm from Sunday to Thursday" +
        //        "(till 30th June 2016 and from 15th August onwards). The misc fees are included in the First term fees. Please see your child&rsquo;s invoice(attached) for the breakup. " +
        //        "</span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>The sibling discount is applicable ONLY if Term I is fully paid and two post dated cheques are submitted for Term II and III on or before" +
        //        "1st Sep 2016.</span><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>*Term I includes the misc fees</span></p><p class='Normal'>" +
        //        "<span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>1) Due dates for payment of fees: First term 1st September 2016, Second term 15th December 2016; Third term 15th&nbsp; March 2017. Cheques should be made in favor of " +
        //        "</span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>Star International School." +
        //       "</span><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>2) T</span>" +
        //        "<span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>Two Post-dated cheques for the 2nd and 3rd installments MUST be paid to the school along with the cash/cheque for the 1st term</span>" +
        //        "<span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>. If you would like to make cash payments for Term II and III, the cheque will be kept as security deposits which the parents can collect it before the cheque due date. " +
        //        "These cheques will be deposited on the due date.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;text-decoration: underline;'>" +
        //        "Optional Fees: </span></p><p class='Normal'><span style='font-family: 'Calibri';'>Fine for returned Cheque-AED 100</span></p><p class='Normal'>" +
        //        "<span style='font-family: 'Calibri';'>Fine for bounced cheque-AED 250</span></p><p class='Normal'><span style='font-family: 'Calibri';'>Postponing the Cheque for 1 week(5 working days) will be 100Dhs and any subsequent working days after that will be charged @25Dhs per day. (Subject to the Principal&rsquo;s approval)</span></p><p class='Normal'> </p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;'>Please see the School Policies regarding the Discount, Late Admission and Tuition Refund. It will be effective from 1st September 2016.</span> <span style='font-family: 'Calibri';font-size: 10.6666666666667px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>DISCOUNT POLICY</span><span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>Sibling Discount </span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>If more than one child takes admission from a family of the same parents, then the second child gets 10% discount, third child onwards will get 20%. Discount will be applicable for the lowest fees with following conditions and will be implemented strictly starting September 2016.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>&nbsp; </span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>1.&nbsp; &nbsp;  </span><span style='font-family: 'Calibri';font-weight: bold;font-size: 10.6666666666667px;text-decoration: underline;'>The sibling discount is applicable only if Term I is fully paid and two PDC is submitted on or before 1st Sep 2016.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'> 2.&nbsp; &nbsp; &nbsp;  The discount is to be reflected on the last installment payment. If any of the payments are late, sibling discount will be revoked.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'> 3.&nbsp; &nbsp; &nbsp;  If the cheque is dishonored due to insufficient fund, the discount will be revoked.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>4.&nbsp; &nbsp; &nbsp;  The discount will not be eligible for late admission students and it will be applicable only from the beginning of ensuing academic year.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>5.&nbsp; &nbsp; &nbsp;  The student will not be entitled to avail more than one discount.</span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>6.&nbsp; &nbsp; &nbsp;  We will NOT be offering any special discount to any organization (like UOD, Emirates etc.,)</span></p><p class='Normal'><span style='font-family: 'Calibri';font-weight: bold;font-size: 12px;'>TUITION REFUND POLICY</span><span style='font-family: 'Calibri';font-size: 12px;'></span></p><p class='Normal'><span style='font-family: 'Calibri';font-size: 10.6666666666667px;'>As per KHDA fee framework.</span></p><p class='Normal'><span>&nbsp;</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Best Regards</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Aleemunnisa</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>STAR INTERNATIONAL SCHOOL-ALTWAR 2</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>P.O. Box 51008 - Dubai, UAE </span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Tel: 04-263 8999 Fax: 04-263 8998</span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Email: admin.altwar@starintl.ae&nbsp; </span></p><p class='Normal'><span style='font-family: 'Times New Roman';font-style: Italic;font-weight: bold;font-size: 14.6666666666667px;color: #002060;'>Website: http://starintlschoolaltwar.com</span></p>";
                
        //        //List<comn_email_attachments> lstatt = new List<comn_email_attachments>();
        //        //comn_email_attachments att = new comn_email_attachments();
        //        //att.attFilename = fileName1;
        //        //lstatt.Add(att);
                
        //        //ScheduleMails(email, lstatt);
        //    }
        //    catch (Exception de)
        //    { }
        //}
    }
}