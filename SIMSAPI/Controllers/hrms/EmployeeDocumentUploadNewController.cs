﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;

using System.IO;
using System.Web.Configuration;
using System.Web;

namespace SIMSAPI.Controllers.hrms
{
     [RoutePrefix("api/EmpDocumentupload")]
    public class EmployeeDocumentUploadNewController : ApiController
    {
        [Route("getcompany")]
        public HttpResponseMessage getcompany()
        {
            List<Per312> lst = new List<Per312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_upd", "A")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per312 obj = new Per312();

                            obj.company_name = dr["co_desc"].ToString();
                            obj.company_code = dr["co_company_code"].ToString();

                            lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getDepartmentNames")]
        public HttpResponseMessage getDepartmentNames(string company_code)
        {


            List<Per312> goaltarget_list = new List<Per312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                             new SqlParameter("@opr_upd",'D'),
                                new SqlParameter("@company_code",company_code),
                           
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per312 invsobj = new Per312();
                            invsobj.dept_name = dr["dep_name"].ToString();
                            invsobj.dept_code = dr["dep_code"].ToString();
                            goaltarget_list.Add(invsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {


                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getDesignation")]
        public HttpResponseMessage getDesignation(string company_code)
        {
            List<Per312> lst = new List<Per312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_upd", "B"),
                            new SqlParameter("@company_code",company_code),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per312 obj = new Per312();

                            obj.designation_name = dr["dg_desc"].ToString();
                            obj.designation_code = dr["dg_code"].ToString();

                            lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("getDocumentName")]
        public HttpResponseMessage getDocumentName()
        {
            List<Per312> lst = new List<Per312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_upd", "C"),
                           

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per312 obj = new Per312();

                            obj.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            obj.pays_doc_code = dr["pays_doc_code"].ToString();

                            lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("DocumentdetailData")]
        public HttpResponseMessage DocumentdetailData(Per312 data)
        {
            List<Per312> lst = new List<Per312>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_upd", "E"),
                            new SqlParameter("@show", data.show),
                            new SqlParameter("@company_code", data.company_code),
                            new SqlParameter("@dept_code", data.dept_code),
                            new SqlParameter("@designation_code", data.designation_code),
                            new SqlParameter("@pays_doc_code", data.pays_doc_code),
                            new SqlParameter("@pays_doc_empl", data.pays_doc_empl_id),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per312 obj = new Per312();

                            obj.pays_doc_empl_id = dr["em_login_code"].ToString();
                            obj.pays_doc_empl_name = dr["employeename"].ToString();

                            obj.pays_doc_desc1 = dr["docdesc"].ToString();
                            obj.pays_doc_mod_code = dr["pays_doc_mod_code1"].ToString();

                            obj.pays_doc_code = dr["pays_doc_code1"].ToString();
                            obj.pays_doc_srno = dr["pays_doc_srno"].ToString();

                            obj.pays_doc_desc = dr["pays_doc_desc"].ToString();
                            obj.pays_doc_path = dr["pays_doc_path"].ToString();

                            // if (!string.IsNullOrEmpty(dr["effective_from"].ToString()))
                            //  obj.pa_effective_from = DateTime.Parse(dr["effective_from"].ToString()).Year + "-" + DateTime.Parse(dr["effective_from"].ToString()).Month + "-" + DateTime.Parse(dr["effective_from"].ToString()).Day;

                            if (string.IsNullOrEmpty(dr["pays_doc_issue_date"].ToString()) == false)
                                obj.pays_doc_issue_date = db.UIDDMMYYYYformat(dr["pays_doc_issue_date"].ToString()); //obj.pays_doc_issue_date = DateTime.Parse(dr["pays_doc_issue_date"].ToString()).Year + "-" + DateTime.Parse(dr["pays_doc_issue_date"].ToString()).Month + "-" + DateTime.Parse(dr["pays_doc_issue_date"].ToString()).Day;
                            else
                                obj.pays_doc_issue_date = dr["pays_doc_issue_date"].ToString();
                            if (string.IsNullOrEmpty(dr["pays_doc_expiry_date"].ToString()) == false)
                                obj.pays_doc_expiry_date = db.UIDDMMYYYYformat(dr["pays_doc_expiry_date"].ToString());  //DateTime.Parse(dr["pays_doc_expiry_date"].ToString()).Year + "-" + DateTime.Parse(dr["pays_doc_expiry_date"].ToString()).Month + "-" + DateTime.Parse(dr["pays_doc_expiry_date"].ToString()).Day;
                            else
                                obj.pays_doc_expiry_date = db.UIDDMMYYYYformat(dr["pays_doc_expiry_date"].ToString());
                            if (dr["pays_doc_status"].ToString() == "A")
                                obj.pays_doc_status = true;
                            else
                                obj.pays_doc_status = false;

                            obj.pays_available_on_short_form = dr["pays_available_on_short_form"].ToString();
                            obj.pays_available_on_long_form = dr["pays_available_on_long_form"].ToString();
                            obj.pays_emp_visible_status = dr["pays_emp_visible_status"].ToString();
                            obj.pays_emp_upload_doc = dr["pays_emp_upload_doc"].ToString();
                            
                            lst.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("InsertUpdateDocumentDetail")]
        public HttpResponseMessage InsertUpdateDocumentDetail(List<Per312> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var perobj in data)
                    {
                        if (perobj.pays_doc_issue_date == "")
                            perobj.pays_doc_issue_date = null;
                        if (perobj.pays_doc_expiry_date == "")
                            perobj.pays_doc_expiry_date = null;

                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                            {

                                 new SqlParameter("@opr", perobj.opr),
                                
                                new SqlParameter("@pays_doc_code", perobj.pays_doc_code),
                                new SqlParameter("@pays_doc_srno", perobj.pays_doc_srno),
                                new SqlParameter("@pays_doc_mod_code", perobj.pays_doc_mod_code),
                                new SqlParameter("@pays_doc_path", perobj.pays_doc_path),
                                new SqlParameter("@pays_doc_desc", perobj.pays_doc_desc),
                                new SqlParameter("@pays_doc_empl_id", perobj.pays_doc_empl_id),
                                new SqlParameter("@pays_doc_issue_date", db.DBYYYYMMDDformat(perobj.pays_doc_issue_date)),
                                new SqlParameter("@pays_doc_expiry_date", db.DBYYYYMMDDformat(perobj.pays_doc_expiry_date)),
                                new SqlParameter("@comn_appl_code", "Per312"),
                                new SqlParameter("@comn_sequence_code", "DSR"),
                                new SqlParameter("@pays_doc_status", perobj.pays_doc_status ? "A" : "I"),
                               
                            });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                    }



                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        // string type = Path.GetFileName(hpf.ContentType);
                        file = filename;
                        hpf.SaveAs((root + filename));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }


        [Route("DocumentDelete")]
        public HttpResponseMessage DocumentDelete(Per312 obj)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_doc_detail_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr","F"),
                            new SqlParameter("@pays_doc_code", obj.pays_doc_code),
                            new SqlParameter("@pays_doc_srno", obj.pays_doc_srno),
                            new SqlParameter("@pays_doc_empl_id", obj.pays_doc_empl_id)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    
                }

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch(Exception x) {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            
        }


    }
}