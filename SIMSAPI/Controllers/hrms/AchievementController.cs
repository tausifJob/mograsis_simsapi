﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.AchievementController
{
    [RoutePrefix("api/Achievement")]
    [BasicAuthentication]
    public class AchievementController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllAchievement")]
        public HttpResponseMessage getAllComnUserRole()
        {
            List<Ucw240> achieve_list = new List<Ucw240>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw240 simsobj = new Ucw240();
                            simsobj.sims_sip_achievement_type = dr["sims_sip_achievement_type"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_sip_achievement_desc = dr["sims_sip_achievement_desc"].ToString();
                            simsobj.sims_sip_achievement_point = dr["sims_sip_achievement_point"].ToString();
                            simsobj.sims_sip_achievement_code = dr["sims_sip_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_status = dr["sims_sip_achievement_status"].Equals("A") ? true : false;
                            achieve_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, achieve_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, achieve_list);
        }

        [Route("getAchievementType")]
        public HttpResponseMessage getAchievementType()
        {

            List<Ucw240> achieve_list = new List<Ucw240>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Ucw240 simsobj = new Ucw240();
                            simsobj.sims_sip_achievement_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString(); ;
                            achieve_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, achieve_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, achieve_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDAchievement")]
        public HttpResponseMessage CUDAchievement(List<Ucw240> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Ucw240 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_achievement_proc]",
                            new List<SqlParameter>() 
                         { 
		 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_achievement_type", simsobj.sims_sip_achievement_type),
                                new SqlParameter("@sims_sip_achievement_code", simsobj.sims_sip_achievement_code),
                                new SqlParameter("@sims_sip_achievement_desc", simsobj.sims_sip_achievement_desc),
                                new SqlParameter("@sims_sip_achievement_point", simsobj.sims_sip_achievement_point),
                                new SqlParameter("@sims_sip_achievement_status",simsobj.sims_sip_achievement_status.Equals(true)?"A":"I"),
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
