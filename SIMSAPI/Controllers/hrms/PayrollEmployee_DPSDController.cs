﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/PayrollEmployeeDPSD")]
    [BasicAuthentication]
    public class PayrollEmployee_DPSDController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("Get_dept_Code")]
        public HttpResponseMessage Get_dept_Code(string company_code)
        {
            List<Pers099> gc_company = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[fins_departments]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),
                           new SqlParameter("@codp_comp_code", company_code)
                         });
                    while (dr.Read())
                    {
                        Pers099 objph = new Pers099();
                        objph.code = dr["codp_dept_no"].ToString();
                        objph.em_dept_name = dr["codp_dept_name"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        [Route("Get_designation_Code")]
        public HttpResponseMessage Get_designation_Code(string company_code)
        {
            List<Pers099> gc_company = new List<Pers099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_designation]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),
                           new SqlParameter("@dg_company_code", company_code)
                         });
                    while (dr.Read())
                    {
                        Pers099 objph = new Pers099();
                        objph.em_desg_code = dr["dg_code"].ToString();
                        objph.em_desg_desc = dr["dg_desc"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }


        [Route("GetPayrollEmployee")]
        public HttpResponseMessage GetPayrollEmployee(string com_code, string dept_code, string deg_code, string empid,string gr_code)
        {

            List<Pers072> list = new List<Pers072>();
            string str = null, year = null, month = null, strMonthName = null;

            try
            {
                DateTime d;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_payroll_empl_DPSD_proc]",
                        new List<SqlParameter>()
                         {                  
                   
                    new SqlParameter("@opr", "S"),
                    new SqlParameter("@pe_number", ""),
                    new SqlParameter("@pe_year_month", 0),
                    new SqlParameter("@em_company_code", com_code),
                    new SqlParameter("@em_dept_code", dept_code),
                    new SqlParameter("@em_desg_code", deg_code),                    
                    new SqlParameter("@em_login_code", empid),
                    new SqlParameter("@em_grade_code", gr_code),

                    });
                    while (dr.Read())
                    {
                        Pers072 obj = new Pers072();
                        obj.em_number = dr["em_number"].ToString();
                        obj.pd_pay_code = dr["em_login_code"].ToString();
                        obj.em_company_code = dr["em_company_code"].ToString();
                        obj.sh_emp_name = dr["em_name"].ToString();
                        obj.em_desg_code = dr["em_desg_code"].ToString();
                        obj.em_sponser_name = dr["em_sponser_name"].ToString();
                        //str = dr["pe_year_month"].ToString();
                        obj.em_desg_desc = dr["dg_desc"].ToString();
                        //if (!string.IsNullOrEmpty(str))
                        //{
                        //    year = str.Substring(0, 4);
                        //    month = str.Substring(4, 2);
                        //    strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt16(month));
                        //}
                        //obj.pe_year_month = strMonthName + " " + year;
                        if (DateTime.TryParse(dr["em_date_of_join"].ToString(), out d))
                        {   //obj.pe_doj = d.ToShortDateString(); 
                            obj.pe_doj = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                        }
                        //obj.pe_doj = Convert.ToDateTime(dr["em_date_of_join"].ToString()).ToShortDateString();
                        obj.pe_doj = db.UIDDMMYYYYformat(dr["em_date_of_join"].ToString());
                        if (DateTime.TryParse(dr["pa_effective_from"].ToString(), out d))
                        {
                            //obj.pa_effective_from = d.ToShortDateString();
                            obj.pa_effective_from = db.UIDDMMYYYYformat(dr["pa_effective_from"].ToString());

                        }
                        else
                        {
                            obj.pa_effective_from = null;
                        }
                        list.Add(obj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }


        [Route("GetSalaryGeneratedForSelectedMonthOrNot")]
        public HttpResponseMessage GetSalaryGeneratedForSelectedMonthOrNot(string sd_number, string start, string end)
        {
            List<Pers072> list = new List<Pers072>();
            if (start == "undefined" || start == "\"\"")
            {
                start = null;
            }
            if (end == "undefined" || end == "\"\"")
            {
                end = null;
            }
            try
            {
                //string[] a = sd_year_month.Split('/');
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_payroll_empl_DPSD_proc]",
                        new List<SqlParameter>()
                         {
                             
                new SqlParameter("@opr", "V"),                
                new SqlParameter("@effective_from_date", db.DBYYYYMMDDformat(start)),
                new SqlParameter("@effective_upto", db.DBYYYYMMDDformat(end)),
                new SqlParameter("@pe_number", sd_number)
              
               });
                    while (dr.Read())
                    {
                        Pers072 obj = new Pers072();

                        obj.pd_pay_code = dr["cp_code"].ToString();
                        //obj.pe_insert = dr["pe_insert"].Equals("A") ? true : false;
                        obj.cp_desc = dr["cp_desc"].ToString();
                       // obj.pc_earn_dedn_tag = dr["pc_ern_dedn"].ToString();
                        obj.pc_earn_dedn_tag = dr["pc_ern_dedn"].ToString();
                        if (!string.IsNullOrEmpty(dr["effective_from"].ToString()))
                        {
                            obj.pa_effective_from = db.UIDDMMYYYYformat(dr["effective_from"].ToString());
                        }
                         //obj.pa_effective_from = DateTime.Parse(dr["effective_from"].ToString()).Year + "-" + DateTime.Parse(dr["effective_from"].ToString()).Month + "-" +                                DateTime.Parse(dr["effective_from"].ToString()).Day;
                            

                        if (!string.IsNullOrEmpty(dr["effective_upto"].ToString()))
                        {
                            obj.code = db.UIDDMMYYYYformat(dr["effective_upto"].ToString());
                        }
                            //obj.code = DateTime.Parse(dr["effective_upto"].ToString()).Year + "-" + DateTime.Parse(dr["effective_upto"].ToString()).Month + "-" + DateTime.Parse                              (dr["effective_upto"].ToString()).Day;
                            

                        if (dr["pc_ern_dedn"].ToString() == "E")
                            obj.pc_earn_dedn_tag_name = "Earn";
                        else if (dr["pc_ern_dedn"].ToString() == "D")
                            obj.pc_earn_dedn_tag_name = "Deduction";

                        if (!string.IsNullOrEmpty(dr["pa_amount"].ToString()))
                            obj.pd_amount = Decimal.Parse(dr["pa_amount"].ToString());
                        else
                            obj.pd_amount = 0;
                        obj.pe_insert = true;

                        if (obj.pc_earn_dedn_tag == "D")
                        {
                            obj.pd_amount = obj.pd_amount;
                        }
                        obj.pa_remark = dr["pa_remark"].ToString();

                        list.Add(obj);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }

        [Route("RemaingPaycode")]
        public HttpResponseMessage RemaingPaycode(string sd_number, string start, string end)
        {
            List<Pers072> list = new List<Pers072>();
            if (start == "undefined" || start == "\"\"")
            {
                start = null;
            }
            if (end == "undefined" || end == "\"\"")
            {
                end = null;
            }
            try
            {
                //string[] a = sd_year_month.Split('/');
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_payroll_empl_DPSD_proc]",
                        new List<SqlParameter>()
                         {
                             
                new SqlParameter("@opr", "P"),                
                new SqlParameter("@effective_from_date", db.DBYYYYMMDDformat(start)),
                new SqlParameter("@effective_upto",  db.DBYYYYMMDDformat(end)),
                new SqlParameter("@pe_number", sd_number)
              
               });
                    while (dr.Read())
                    {
                        Pers072 obj = new Pers072();

                        obj.pd_pay_code = dr["cp_code"].ToString();
                        obj.cp_desc = dr["cp_desc"].ToString();
                        obj.pc_earn_dedn_tag = dr["pc_ern_dedn"].ToString();
                        if (dr["pc_ern_dedn"].ToString() == "E")
                            obj.pc_earn_dedn_tag_name = "Earn";
                        else if (dr["pc_ern_dedn"].ToString() == "D")
                            obj.pc_earn_dedn_tag_name = "Deduction";

                        if (!string.IsNullOrEmpty(dr["pa_amount"].ToString()))
                            obj.pd_amount = Decimal.Parse(dr["pa_amount"].ToString());
                        else
                            obj.pd_amount = 0;
                        obj.pe_insert = true;

                        if (obj.pc_earn_dedn_tag == "D")
                        {
                            obj.pd_amount = obj.pd_amount;
                        }

                        list.Add(obj);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }

        [Route("HistoryPaycode")]
        public HttpResponseMessage HistoryPaycode(string sd_number)
        {
            List<Pers072> list = new List<Pers072>();

            try
            {
                //string[] a = sd_year_month.Split('/');
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_payroll_empl_DPSD_proc]",
                        new List<SqlParameter>()
                         {
                             
                            new SqlParameter("@opr", "L"),                
                            new SqlParameter("@emp_number", sd_number)
              
                           });      
                    while (dr.Read())
                    {
                        Pers072 obj = new Pers072();

                        obj.paycode_code = dr["pa_pay_code"].ToString();
                        obj.paycode_name = dr["cp_desc"].ToString();
                        obj.pa_effective_from = db.UIDDMMYYYYformat(dr["pa_effective_from"].ToString());
                        obj.pa_effective_upto = db.UIDDMMYYYYformat(dr["pa_effective_upto"].ToString());
                        obj.inc_fixed_amout = dr["pa_amount"].ToString();
                        obj.pc_earn_dedn_tag_name = dr["earnded"].ToString();
                        obj.pa_remark = dr["pa_remark"].ToString();
                        try
                        {
                            obj.pa_status= dr["pa_status"].ToString();
                        }
                        catch(Exception e) {}


                        list.Add(obj);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }

        [Route("InsertUpdatePaysPayable")]
        public HttpResponseMessage InsertUpdatePaysPayable(List<Pers072> obj)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers072 data in obj)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays_payroll_empl_DPSD_proc]",
                          new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@pe_number", data.em_number),
                            new SqlParameter("@em_company_code", data.em_company_code),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(data.pa_effective_from)),  //DateTime.Parse(data.pa_effective_from).Year + "-" + DateTime.Parse(data.pa_effective_from).Month + "-" + DateTime.Parse(data.pa_effective_from).Day),
                            new SqlParameter("@upto_date", db.DBYYYYMMDDformat(data.code)),
                            new SqlParameter("@pa_pay_code", data.pd_pay_code),
                            new SqlParameter("@pa_amount", data.pd_amount),
                            new SqlParameter("@pa_remark", data.pa_remark)

                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
          //  return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("PayCodeStatusUpdate")]
        public HttpResponseMessage PayCodeStatusUpdate(string pa_number, string pa_pay_code, string pa_effective_from, string pa_effective_upto,string pa_status)
        {
            List<Pers072> list = new List<Pers072>();
            if (pa_effective_from == "undefined" || pa_effective_from == "\"\"")
            {
                pa_effective_from = null;
            }
            if (pa_effective_upto == "undefined" || pa_effective_upto == "\"\"")
            {
                pa_effective_upto = null;
            }
            bool updated = false;
            try
            {
                //string[] a = sd_year_month.Split('/');
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_payroll_empl_DPSD_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "W"),
                        new SqlParameter("@pe_number", pa_number),
                        new SqlParameter("@pa_pay_code", pa_pay_code),
                        new SqlParameter("@effective_from_date", db.DBYYYYMMDDformat(pa_effective_from)),
                        new SqlParameter("@effective_upto",  db.DBYYYYMMDDformat(pa_effective_upto)),
                        new SqlParameter("@pa_status", pa_status)
                    });
                    
                    if(dr.RecordsAffected > 0)
                    {
                        updated = true;
                    }
                    else
                    {
                        updated = false;
                    }
                    dr.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, updated);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }


    }
}