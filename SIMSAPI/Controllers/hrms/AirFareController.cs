﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Controllers.hrms;



namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/AirFare")]
    public class AirFareController : ApiController
    {


        [Route("nations")]
        public HttpResponseMessage nations()
        {

            List<PerAF> nation_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),    
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF obj = new PerAF();
                            obj.nation_code = dr["sims_country_code"].ToString();
                            obj.nation_name = dr["sims_country_name_en"].ToString();
                            nation_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, nation_list);
        }

        [Route("destination")]
        public HttpResponseMessage destination(string nationcode)
        {

            List<PerAF> fare_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "A"),    
                                 new SqlParameter("@af_nation_code", nationcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF obj = new PerAF();
                            obj.ds_code = dr["ds_code"].ToString();
                            obj.ds_name = dr["ds_name"].ToString();
                            fare_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, fare_list);
        }

        [Route("grade")]
        public HttpResponseMessage grade()
        {

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "E"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.gr_code = dr["gr_code"].ToString();
                            persobj.gr_desc = dr["gr_desc"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("Classget")]
        public HttpResponseMessage Classget()
        {

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "B"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.af_class = dr["pays_appl_parameter"].ToString();
                            persobj.af_class_name = dr["pays_appl_form_field_value1"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("applicableMonth")]
        public HttpResponseMessage applicableMonth()
        {

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "F"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.pays_month_code = dr["pays_appl_parameter"].ToString();
                            persobj.pays_month_name = dr["pays_appl_form_field_value1"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("accountName")]
        public HttpResponseMessage accountName()
        {

            List<PerAF> class_list = new List<PerAF>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "H"),    
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF persobj = new PerAF();
                            persobj.af_gl_acct_no = dr["glma_acct_code"].ToString();
                            persobj.af_gl_acct_name = dr["glma_acct_name"].ToString();
                            class_list.Add(persobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, class_list);
        }

        [Route("AirFareDetails")]
        public HttpResponseMessage AirFareDetails()
        {
            List<PerAF> airfare_list = new List<PerAF>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PerAF invsobj = new PerAF();
                            invsobj.gr_code = dr["af_grade_code"].ToString();
                            invsobj.gr_desc = dr["gr_desc"].ToString();
                            invsobj.ds_code = dr["af_dest_code"].ToString();
                            invsobj.ds_name = dr["ds_name"].ToString();
                            invsobj.nation_code = dr["af_nation_code"].ToString();
                            invsobj.nation_name = dr["sims_country_name_en"].ToString();
                            invsobj.af_class = dr["af_class"].ToString();
                            invsobj.af_class_name = dr["pays_appl_form_field_value1"].ToString();
                            invsobj.af_adult_rate = dr["af_adult_rate"].ToString();
                            invsobj.pays_month_name = dr["month_name"].ToString();
                            invsobj.af_applicable_frequency = dr["af_applicable_frequency"].ToString();
                            invsobj.update_after = dr["af_upgrade_after"].ToString();
                            invsobj.update_freq = dr["af_upgrade_frequency"].ToString();
                            invsobj.af_gl_acct_name = dr["glma_acct_name"].ToString();
                            invsobj.af_gl_acct_no = dr["af_gl_acct_no"].ToString();
                            invsobj.af_default_applicable_month = dr["af_default_applicable_month"].ToString();
                            invsobj.af_payroll_flag = dr["af_payroll_flag"].Equals("Y") ? true : false;
                            invsobj.upgrade_flag = dr["af_upgrade_flag"].Equals("Y") ? true : false;
                            invsobj.af_pay_code = dr["af_pay_code"].ToString();                            
                            airfare_list.Add(invsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, airfare_list);
        }

        [Route("insertAirFare")]
        public HttpResponseMessage insertAirFare(PerAF data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr",data.opr),
                                    new SqlParameter("@af_grade_code", data.gr_code),
                                    new SqlParameter("@af_dest_code", data.ds_code),
                                    new SqlParameter("@af_nation_code", data.nation_code),
                                    new SqlParameter("@af_class", data.af_class),
                                    new SqlParameter("@af_applicable_frequency", data.af_applicable_frequency),
                                    new SqlParameter("@af_adult_rate",data.af_adult_rate != ""?data.af_adult_rate:"0"),
                                    new SqlParameter("@af_child_rate","0"),
                                    new SqlParameter("@af_infant_rate","0"),
                                    new SqlParameter("@af_applicable_month", data.af_applicable_month),
                                    new SqlParameter("@upgrade_flag",data.upgrade_flag.Equals(true)?"Y":"N" ),
                                    new SqlParameter("@upgrade_after", data.update_after==""?"0":data.update_after),
                                    new SqlParameter("@updated_frequency", data.update_freq==""?"0": data.update_freq),
                                    new SqlParameter("@af_payroll_flag", data.af_payroll_flag.Equals(true)?"Y":"N"),
                                    new SqlParameter("@af_gl_acct_no", data.af_gl_acct_no),
                                    new SqlParameter("@af_pay_code", data.af_pay_code)
                                    
                            });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("AirFaredelete")]
        public HttpResponseMessage AirFaredelete(PerAF data)
        {


            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_air_fare_proc]",
                                new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr", 'D'),
                                    new SqlParameter("@af_grade_code", data.gr_code),
                                    new SqlParameter("@af_dest_code", data.ds_code),
                                    new SqlParameter("@af_nation_code", data.nation_code)

                            });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }


}