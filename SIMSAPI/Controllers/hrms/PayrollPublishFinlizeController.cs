﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/Payroll/PayrollPubFin")]
    public class PayrollPublishFinlizeController : ApiController
    {

        [Route("PubYear")]
        public HttpResponseMessage PubYear()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Y")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.fin_year = dr["fins_appl_form_field_value1"].ToString();
                            hrmsobj.fin_year_desc = dr["fins_appl_form_field_value3"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("PubMonth")]
        public HttpResponseMessage PubMonth()
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.sd_year_month_code = dr["pays_appl_parameter"].ToString();
                            hrmsobj.sd_year_month_name = dr["pays_appl_form_field_value1"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("EmployeeDetails")]
        public HttpResponseMessage EmployeeDetails(string year_month)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@year_month", year_month)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.emp_number = dr["sd_number"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.emp_name = dr["emp_name"].ToString();
                            hrmsobj.earn = dr["earn"].ToString();
                            hrmsobj.ded = dr["Ded"].ToString();
                            hrmsobj.pe_bank_cash_tag = dr["pe_bank_cash_tag"].ToString();
                            hrmsobj.net_amount = dr["net_amount"].ToString();
                            hrmsobj.pe_onhold_flag = dr["pe_onhold_flag"].ToString();
                            if (dr["pe_Status"].ToString() == "U")
                                hrmsobj.holdstatus = false;
                            if (dr["pe_Status"].ToString() == "H")
                                hrmsobj.holdstatus = true;

                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("PubFinDetails")]
        public HttpResponseMessage PubFinDetails(string year_month)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@year_month", year_month)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.publishdata = db.UIDDMMYYYYformat(dr["visible_empl_from"].ToString());
                            hrmsobj.finalize_date = db.UIDDMMYYYYformat(dr["pl_finalize_Date"].ToString());
                            if (dr["pl_finalize_status"].ToString() == "1")
                                hrmsobj.finalize_status = true;
                            else
                                hrmsobj.finalize_status = false;
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("FinDetails")]
        public HttpResponseMessage FinDetails(string year_month,string emp_id)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "F"),
                                new SqlParameter("@year_month", year_month),
                                new SqlParameter("@pe_number", emp_id)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.sd_pay_code = dr["sd_pay_code"].ToString();
                            hrmsobj.cp_desc = dr["cp_desc"].ToString();
                            hrmsobj.amount = dr["amount"].ToString();
                            hrmsobj.pd_debit_acno = dr["pd_debit_acno"].ToString();
                            hrmsobj.pd_credit_acno = dr["pd_credit_acno"].ToString();
                            hrmsobj.pc_earn_dedn_tag = dr["pc_earn_dedn_tag"].ToString();
                            hrmsobj.earn_ded_desc = dr["earn_ded_desc"].ToString();
                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }

        [Route("UpdatePubFinStatus")]
        public HttpResponseMessage UpdatePubFinStatus( List<Pers078> data)
        {
            bool inserted = false;
            try
            {
                
                foreach (var d in data)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                            new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr",'I'),
                                    new SqlParameter("@finalize_status", d.holdstatus==true?"H":"U"),
                                    new SqlParameter("@year_month", d.year_month),
                                    new SqlParameter("@emp_number", d.emp_number)
                            });
                        if (dr.RecordsAffected>0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }


            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateFin")]
        public HttpResponseMessage UpdateFin(string year_month, string finalize_date,string emp_id,string user_name)
        {
            bool inserted = false;
            try
            {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                            new List<SqlParameter>() 
                             { 
                                    new SqlParameter("@opr",'U'),
                                    new SqlParameter("@year_month", year_month),
                                    new SqlParameter("@finalize_date", db.DBYYYYMMDDformat(finalize_date)),
                                    new SqlParameter("@pe_number", emp_id),
                                    new SqlParameter("@user_name", user_name)
                            });
                        if(dr.RecordsAffected >0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                }


            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateFinProcess")]
        public HttpResponseMessage UpdateFinProcess(string year_month, string finalize_date, string emp_id,string user_name)
        {
            bool inserted = false;
            try
            {                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'P'),
                                new SqlParameter("@year_month", year_month),
                                new SqlParameter("@process_date", db.DBYYYYMMDDformat(finalize_date)),
                                new SqlParameter("@pe_number", emp_id),
                                new SqlParameter("@user_name", user_name)
                        });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr.Close();
                }

            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("EmployeeDetailsForFinalize")]
        public HttpResponseMessage EmployeeDetailsForFinalize(string year_month)
        {
            List<Pers078> emp_list = new List<Pers078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_publish_finalize_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "A"),
                        new SqlParameter("@year_month", year_month)

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers078 hrmsobj = new Pers078();
                            hrmsobj.emp_number = dr["sd_number"].ToString();
                            hrmsobj.em_login_code = dr["em_login_code"].ToString();
                            hrmsobj.emp_name = dr["emp_name"].ToString();
                            hrmsobj.earn = dr["earn"].ToString();
                            hrmsobj.ded = dr["Ded"].ToString();
                            hrmsobj.pe_bank_cash_tag = dr["pe_bank_cash_tag"].ToString();
                            hrmsobj.net_amount = dr["net_amount"].ToString();
                            hrmsobj.pe_onhold_flag = dr["pe_onhold_flag"].ToString();
                            if (dr["pe_Status"].ToString() == "U")
                                hrmsobj.holdstatus = false;
                            if (dr["pe_Status"].ToString() == "H")
                                hrmsobj.holdstatus = true;

                            emp_list.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, emp_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, emp_list);
        }


    }
}