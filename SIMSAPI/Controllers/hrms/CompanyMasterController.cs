﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/companyhrms")]
    public class CompanyMasterController : ApiController
    {

        #region Pays_Company (Pers059)
        [Route("GetAllCompany_Name")]
        public HttpResponseMessage GetAllCompany_Name()
        {
            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")
                });
                    while (dr.Read())
                    {
                        Pers059 hrmsobj = new Pers059();
                        hrmsobj.company_code = dr["comp_code"].ToString();
                        hrmsobj.ca_company_code_name = dr["comp_name"].ToString();
                        hrmsobj.company_short_name = dr["comp_short_name"].ToString();
                        hrmsobj.excg_curcy_code = dr["comp_curcy_code"].ToString();
                        mod_list.Add(hrmsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetCurrency_Name")]
        public HttpResponseMessage GetCurrency_Name(string code)
        {

            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Z"),
                new SqlParameter("@excg_curcy_code", code)
                         });
                    while (dr1.Read())
                    {
                        Pers059 hrmsobj = new Pers059();
                        hrmsobj.comp_currency_name = dr1["excg_curcy_desc"].ToString();
                        hrmsobj.excg_curcy_code = dr1["excg_curcy_code"].ToString();
                        mod_list.Add(hrmsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAllFormula_Description")]
        public HttpResponseMessage GetAllFormula_Description()
        {
            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "F")
                         });
                    while (dr.Read())
                    {
                        Pers059 hrmsobj = new Pers059();
                        hrmsobj.comp_not_formula = dr["fm_desc"].ToString();
                        hrmsobj.comp_hot_formula = dr["fm_desc"].ToString();
                        hrmsobj.comp_shot_formula = dr["fm_desc"].ToString();
                        hrmsobj.comp_sot_formula = dr["fm_desc"].ToString();
                        hrmsobj.se_formula_desc = dr["fm_desc"].ToString();
                        hrmsobj.comp_not_formula_code = dr["fm_code"].ToString();
                        hrmsobj.comp_hot_formula_code = dr["fm_code"].ToString();
                        hrmsobj.comp_shot_formula_code = dr["fm_code"].ToString();
                        hrmsobj.comp_sot_formula_code = dr["fm_code"].ToString();
                        hrmsobj.se_formula_desc_code = dr["fm_code"].ToString();
                        mod_list.Add(hrmsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAllsettlement_code")]
        public HttpResponseMessage GetAllsettlement_code()
        {
            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "Q")
                         });
                    while (dr.Read())
                    {
                        Pers059 hrmsobj = new Pers059();
                        hrmsobj.comp_settlement_code = dr["se_code"].ToString();

                        mod_list.Add(hrmsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAll_Week_Day")]
        public HttpResponseMessage GetAll_Week_Day()
        {
            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "W")
                });

                    while (dr.Read())
                    {
                        Pers059 hrmsobj = new Pers059();
                        hrmsobj.comp_weekly_off_day1_name = dr["sims_appl_form_field_value1"].ToString();
                        hrmsobj.comp_weekly_off_day1_code = dr["sims_appl_parameter"].ToString();
                        hrmsobj.comp_weekly_off_day2_name = dr["sims_appl_form_field_value1"].ToString();
                        hrmsobj.comp_weekly_off_day2_code = dr["sims_appl_parameter"].ToString();
                        mod_list.Add(hrmsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetAll_Company")]
        public HttpResponseMessage GetAll_Company()
        {
            List<Pers059> mod_list = new List<Pers059>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'S'),
                new SqlParameter("@sims_appl_code", "Sim064"),
                new SqlParameter("@sims_appl_form_field", "Day Code")
                         });
                    while (dr.Read())
                    {
                        Pers059 hrmsObj = new Pers059();
                        hrmsObj.company_code = dr["co_company_code"].ToString();
                        hrmsObj.excg_curcy_code = dr["co_curr_code"].ToString();
                        hrmsObj.comp_currency_name = dr["excg_curcy_desc"].ToString();
                        hrmsObj.company_desc = dr["co_desc"].ToString();
                        if (dr["co_currency_tag"].ToString().Equals("S"))
                        {
                            hrmsObj.comp_currency_tag_S = true;
                            hrmsObj.comp_currency_tag = "Single";
                        }
                        if (dr["co_currency_tag"].ToString().Equals("M"))
                        {
                            hrmsObj.comp_currency_tag_M = true;
                            hrmsObj.comp_currency_tag = "Multiple";
                        }
                        hrmsObj.comp_normal_hrs = dr["co_normal_hrs"].ToString();
                        hrmsObj.comp_weekly_off_day1_name = dr["co_weekly_off_day1_desc"].ToString();
                        hrmsObj.comp_cut_off_day = dr["co_cut_off_day"].ToString();
                        hrmsObj.company_short_name = dr["co_short_name"].ToString();
                        hrmsObj.comp_special_hrs = dr["co_special_hrs"].ToString();
                        hrmsObj.comp_normal_ot_hrs = dr["co_normal_ot_hrs"].ToString();
                        hrmsObj.comp_not_rate = dr["co_not_rate"].ToString();
                        hrmsObj.comp_not_formula_desc = dr["co_not_formula_desc"].ToString();
                        hrmsObj.comp_hot_rate = dr["co_hot_rate"].ToString();
                        hrmsObj.comp_hot_formula_desc = dr["co_hot_formula_desc"].ToString();
                        hrmsObj.comp_shot_rate = dr["co_shot_rate"].ToString();
                        hrmsObj.comp_shot_formula_desc = dr["co_shot_formula_desc"].ToString();
                        hrmsObj.comp_sot_rate = dr["co_sot_rate"].ToString();
                        hrmsObj.comp_sot_formula_desc = dr["co_sot_formula_desc"].ToString();
                        hrmsObj.comp_weekly_off_day2_name = dr["co_weekly_off_day2_desc"].ToString();
                        hrmsObj.comp_effect_from = db.UIDDMMYYYYformat(dr["co_effect_from"].ToString());
                        hrmsObj.comp_effect_to = db.UIDDMMYYYYformat(dr["co_effect_to"].ToString());
                        hrmsObj.comp_settlement_code = dr["co_settlement_code"].ToString();
                        hrmsObj.comp_travel_rate = dr["co_travel_rate"].ToString();
                        hrmsObj.comp_not_formula = dr["co_not_formula"].ToString();
                        hrmsObj.comp_hot_formula = dr["co_hot_formula"].ToString();
                        hrmsObj.comp_sot_formula = dr["co_sot_formula"].ToString();
                        hrmsObj.comp_shot_formula = dr["co_shot_formula"].ToString();
                        hrmsObj.comp_weekly_off_day1_code = dr["co_weekly_off_day1"].ToString();
                        hrmsObj.comp_weekly_off_day2_code = dr["co_weekly_off_day2"].ToString();
                        mod_list.Add(hrmsObj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("Insert_Company")]
        public HttpResponseMessage Insert_Company(List<Pers059> data)
        {
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers059 hrmsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_company_master_proc]",
                            new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", hrmsobj.opr),
                new SqlParameter("@co_company_code", hrmsobj.company_code),
                new SqlParameter("@excg_curcy_desc", hrmsobj.excg_curcy_code),
                new SqlParameter("@co_desc", hrmsobj.company_desc),
                new SqlParameter("@co_currency_tag", hrmsobj.comp_currency_tag_S==true?hrmsobj.comp_currency_tag_S==true?"S":"M":hrmsobj.comp_currency_tag_M==true?"M":"S"),
                new SqlParameter("@co_normal_hrs",decimal.Parse(hrmsobj.comp_normal_hrs)),
                new SqlParameter("@co_weekly_off_day1", hrmsobj.comp_weekly_off_day1_code),
                new SqlParameter("@co_cut_off_day", decimal.Parse(hrmsobj.comp_cut_off_day)),
                new SqlParameter("@co_short_name", hrmsobj.company_short_name),
                new SqlParameter("@co_special_hrs", decimal.Parse(hrmsobj.comp_special_hrs)),
                new SqlParameter("@co_normal_ot_hrs", decimal.Parse(hrmsobj.comp_normal_ot_hrs)),
                new SqlParameter("@co_not_rate", decimal.Parse(hrmsobj.comp_not_rate)),
                new SqlParameter("@co_not_formula", hrmsobj.comp_not_formula),
                new SqlParameter("@co_hot_rate", decimal.Parse(hrmsobj.comp_hot_rate)),
                new SqlParameter("@co_hot_formula", hrmsobj.comp_hot_formula),
                new SqlParameter("@co_shot_rate", decimal.Parse(hrmsobj.comp_shot_rate)),
                new SqlParameter("@co_shot_formula", hrmsobj.comp_shot_formula),
                new SqlParameter("@co_sot_rate", decimal.Parse(hrmsobj.comp_sot_rate)),
                new SqlParameter("@co_sot_formula", hrmsobj.comp_sot_formula),
                new SqlParameter("@co_weekly_off_day2", hrmsobj.comp_weekly_off_day2_code),
                new SqlParameter("@co_effect_from", db.DBYYYYMMDDformat(hrmsobj.comp_effect_from.ToString())),
                new SqlParameter("@co_effect_to", db.DBYYYYMMDDformat(hrmsobj.comp_effect_to.ToString())),
                new SqlParameter("@co_settlement_code", hrmsobj.comp_settlement_code),
                new SqlParameter("@co_travel_rate", decimal.Parse(hrmsobj.comp_travel_rate)),
                new SqlParameter("@sims_appl_code", "Sim064"),
                new SqlParameter("@sims_appl_form_field", "Day Code")
                 });
                        if (dr.RecordsAffected > 0)
                        {
                            Inserted = true;
                        }
                        dr.Close();
                    }


                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);

        }

        //[Route("Update_Company")]
        //public HttpResponseMessage Update_Company(Pers059 hrmsobj)
        //{
        //    bool Updated = false;
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("pays_company_master");
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = con;
        //        new SqlParameter("@opr", 'U');
        //        new SqlParameter("@co_company_name", hrmsobj.company_code);
        //        new SqlParameter("@excg_curcy_desc", hrmsobj.comp_currency_name);
        //        new SqlParameter("@co_desc", hrmsobj.company_desc);
        //        if (hrmsobj.comp_currency_tag_S)
        //            new SqlParameter("@co_currency_tag", "S");
        //        if (hrmsobj.comp_currency_tag_M)
        //            new SqlParameter("@co_currency_tag", "M");

        //        new SqlParameter("@co_normal_hrs", hrmsobj.comp_normal_hrs);
        //        new SqlParameter("@co_weekly_off_day1_name", hrmsobj.comp_weekly_off_day1_name);
        //        new SqlParameter("@co_cut_off_day", hrmsobj.comp_cut_off_day);
        //        new SqlParameter("@co_short_name", hrmsobj.company_short_name);
        //        new SqlParameter("@co_special_hrs", hrmsobj.comp_special_hrs);
        //        new SqlParameter("@co_normal_ot_hrs", hrmsobj.comp_normal_ot_hrs);
        //        new SqlParameter("@co_not_rate", hrmsobj.comp_not_rate);
        //        new SqlParameter("@co_not_formula_desc", hrmsobj.comp_not_formula);
        //        new SqlParameter("@co_hot_rate", hrmsobj.comp_hot_rate);
        //        new SqlParameter("@co_hot_formula_desc", hrmsobj.comp_hot_formula);
        //        new SqlParameter("@co_shot_rate", hrmsobj.comp_shot_rate);
        //        new SqlParameter("@co_shot_formula_desc", hrmsobj.comp_shot_formula);
        //        new SqlParameter("@co_sot_rate", hrmsobj.comp_sot_rate);
        //        new SqlParameter("@co_sot_formula_desc", hrmsobj.comp_sot_formula);
        //        new SqlParameter("@co_weekly_off_day2_name", hrmsobj.comp_weekly_off_day2_name);
        //        new SqlParameter("@co_effect_from", DateTime.Parse(hrmsobj.comp_effect_from.ToString()));
        //        new SqlParameter("@co_effect_to", DateTime.Parse(hrmsobj.comp_effect_to.ToString()));
        //        new SqlParameter("@co_settlement_code", hrmsobj.comp_settlement_code);
        //        new SqlParameter("@co_travel_rate", hrmsobj.comp_travel_rate);
        //        new SqlParameter("@sims_appl_code", commonStaticClass.pays059_appl_code);
        //        new SqlParameter("@sims_appl_form_field", commonStaticClass.pays059_appl_form_field);
        //        int upd = cmd.ExecuteNonQuery();
        //        if (upd > 0)
        //        {
        //            Updated = true;
        //        }
        //        con.Close();
        //        return Updated;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return Updated;
        //    }
        //}

        //[Route("Delete_Company")]
        //public HttpResponseMessage Delete_Company(string company_code)
        //{
        //    bool Deleted = false;
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("pays_company_master");
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = con;
        //        new SqlParameter("@opr", 'D');
        //        new SqlParameter("@co_company_name", company_code);
        //        int del = cmd.ExecuteNonQuery();
        //        if (del > 0)
        //        {
        //            Deleted = true;
        //        }
        //        con.Close();
        //        return Deleted;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return Deleted;
        //    }
        //}
        #endregion

    }
}