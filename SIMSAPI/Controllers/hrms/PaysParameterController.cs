﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.PaysParameterController
{
    [RoutePrefix("api/common/PaysParameter")]
    [BasicAuthentication]
    public class PaysParameterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllPaysParameter")]
        public HttpResponseMessage getAllPaysParameter()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPaysParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPaysParameter"));

            List<Per235> paypara_list = new List<Per235>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per235 simsobj = new Per235();
                            //simsobj.pays_comp_code = dr["compcode"].ToString();
                            simsobj.pays_comp_name = dr["pays_comp_name"].ToString();
                            simsobj.pays_comp_code = dr["pays_comp_code"].ToString();
                            simsobj.pays_appl_code = dr["pays_appl_code"].ToString();
                            simsobj.pays_appl_name_en = dr["pays_appl_name"].ToString();
                            simsobj.pays_appl_form_field = dr["pays_appl_form_field"].ToString();
                            simsobj.pays_appl_parameter = dr["pays_appl_parameter"].ToString();
                            simsobj.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.pays_appl_form_field_value2 = dr["pays_appl_form_field_value2"].ToString();
                            simsobj.pays_appl_form_field_value3 = dr["pays_appl_form_field_value3"].ToString();
                            simsobj.pays_appl_form_field_value4 = dr["pays_appl_form_field_value4"].ToString();
                            //simsobj.desg_communication_status = dr["dg_communication_status"].Equals("A") ? true : false;
                            paypara_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, paypara_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, paypara_list);
        }


        [Route("getAllPaysParameter")]
        public HttpResponseMessage getAllPaysParameter(string comn_appl_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPaysParameter(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPaysParameter"));

            List<Per235> paypara_list = new List<Per235>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@comnapplication",comn_appl_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per235 simsobj = new Per235();
                            //simsobj.pays_comp_code = dr["compcode"].ToString();
                            simsobj.pays_comp_name = dr["pays_comp_name"].ToString();
                            simsobj.pays_comp_code = dr["pays_comp_code"].ToString();
                            simsobj.pays_appl_code = dr["pays_appl_code"].ToString();
                            simsobj.pays_appl_name_en = dr["pays_appl_name"].ToString();
                            simsobj.pays_appl_form_field = dr["pays_appl_form_field"].ToString();
                            simsobj.pays_appl_parameter = dr["pays_appl_parameter"].ToString();
                            simsobj.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.pays_appl_form_field_value2 = dr["pays_appl_form_field_value2"].ToString();
                            simsobj.pays_appl_form_field_value3 = dr["pays_appl_form_field_value3"].ToString();
                            simsobj.pays_appl_form_field_value4 = dr["pays_appl_form_field_value4"].ToString();
                            //simsobj.desg_communication_status = dr["dg_communication_status"].Equals("A") ? true : false;
                            paypara_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, paypara_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, paypara_list);
        }



        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanyName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getCompanyName"));

            List<Per235> mod_list = new List<Per235>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_designation_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per235 simsobj = new Per235();
                            simsobj.pays_comp_code = dr["co_company_code"].ToString();
                            simsobj.pays_comp_name = dr["co_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }



        [Route("getApplicationName")]
        public HttpResponseMessage getApplicationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getApplicationName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getApplicationName"));

            List<Per235> fin_list = new List<Per235>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per235 simsobj = new Per235();
                            simsobj.pays_application_code = dr["comn_appl_code"].ToString();
                            simsobj.pays_application_name = dr["comn_appl_name_en"].ToString();
                            fin_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, fin_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDPaysParameter")]
        public HttpResponseMessage CUDPaysParameter(List<Per235> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDPaysParameter(),PARAMETERS";
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per235 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_parameter_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@pays_comp_code", simsobj.pays_comp_code),
                                new SqlParameter("@pays_appl_code", simsobj.pays_appl_code),
                                new SqlParameter("@pays_appl_form_field", simsobj.pays_appl_form_field),
                                new SqlParameter("@pays_appl_parameter", simsobj.pays_appl_parameter),
                                new SqlParameter("@pays_appl_form_field_value1", simsobj.pays_appl_form_field_value1),
                                new SqlParameter("@pays_appl_form_field_value2", simsobj.pays_appl_form_field_value2),
                                new SqlParameter("@pays_appl_form_field_value3", simsobj.pays_appl_form_field_value3),
                                new SqlParameter("@pays_appl_form_field_value4", simsobj.pays_appl_form_field_value4),
                                new SqlParameter("@old_form",simsobj.old_form),	    
                                new SqlParameter("@old_parameter",simsobj.old_parameter),    
                                new SqlParameter("@old_value1",simsobj.old_value1),
                                    
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}
