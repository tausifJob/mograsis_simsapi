﻿using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmployeeLeavePolicyController")]
    [BasicAuthentication]
    public class EmployeeLeavePolicyController : ApiController
    {
        [Route("IUDLeavePolicy")]
        public HttpResponseMessage IUDLeavePolicy(List<leavePolicy> data)
        {
            bool inserted = false;
            string operend = string.Empty;
            string leave_policy_id = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (leavePolicy simsobj in data)
                    {
                        if (simsobj.Opr==null)
                        {
                            operend = "D";
                        }
                        else
                        {
                            operend = simsobj.Opr;
                        }
                        //int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_employee_leave_policy_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_leave_policy_proc]",
                         new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", operend),
                                 new SqlParameter("@leave_policy_id", simsobj.leave_policy_id ),
                                 new SqlParameter("@staff_type_code", simsobj.staff_type_code ),
                                 new SqlParameter("@leave_code", simsobj.leave_code),
                                 //new SqlParameter("@combination_of_leave", simsobj.combination_of_leave),
                                 new SqlParameter("@monthly_allowed", simsobj.monthly_allowed ),
                                 new SqlParameter("@termly_allowed", simsobj.termly_allowed),
                                 new SqlParameter("@yearly_allowed", simsobj.yearly_allowed ),
                                 new SqlParameter("@max_leave_per_Application", simsobj.max_leave_per_Application),
                                 new SqlParameter("@is_prefix_holiday_allowed", simsobj.is_prefix_holiday_allowed.Equals("True")?"Y":"N" ),
                                 new SqlParameter("@post_holiday_allowed", simsobj.post_holiday_allowed.Equals("True")?"Y":"N"  ),
                                 new SqlParameter("@prefix_weekend_allowed", simsobj.prefix_weekend_allowed.Equals("True")?"Y":"N" ),
                                 new SqlParameter("@post_weekend_allowed", simsobj.post_weekend_allowed.Equals("True")?"Y":"N" ),
                                 new SqlParameter("@combination_of_holiday_and_weekend_allowed", simsobj.combination_of_holiday_and_weekend_allowed.Equals("True")?"Y":"N" ),
                                 new SqlParameter("@pays_empl_leave_policy_effective_from",db.DBYYYYMMDDformat(simsobj.pays_empl_leave_policy_effective_from)),
                                 new SqlParameter("@pays_empl_leave_policy_status", simsobj.pays_empl_leave_policy_status.Equals("True")?"Y":"N" ),
                                 new SqlParameter("@pays_empl_leave_policy_created_by", simsobj.pays_empl_leave_policy_created_by)

                         });
                        if (dr.HasRows)
                        {
                            try {
                                while (dr.Read())
                                {
                                    leave_policy_id = dr["leave_policy_id"].ToString();
                                }
                            }
                            catch (Exception) { }                                                        
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, leave_policy_id);
        }

        [Route("IUDLeavePolicyDetails")]
        public HttpResponseMessage IUDLeavePolicyDetails(string leave_policy_id,List<leavePolicy> data)
        {
            bool inserted = false;
            string operend = string.Empty;            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (leavePolicy simsobj in data)
                    {   
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_leave_policy_proc]",
                         new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr","E"),
                                 new SqlParameter("@empl_leave_policy_id",int.Parse(leave_policy_id)),                                 
                                 new SqlParameter("@leave_code", simsobj.leave_code)                            
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;                           
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getallDetails")]
        public HttpResponseMessage getallDetails()
        {
            List<leavePolicy> listOfEmployeeLeavePolicy = new List<leavePolicy>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_employee_leave_policy_proc]",
                        new List<SqlParameter>()
                        {
                              new SqlParameter("@opr", "S"),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            leavePolicy obj_leavePolicy = new leavePolicy();
                            obj_leavePolicy.staff_type_code = dr["pays_empl_leave_policy_staff_type_code"].ToString();
                            obj_leavePolicy.staff_type_dec = dr["staff_type_dec"].ToString();
                            obj_leavePolicy.lCode = dr["lCode"].ToString();
                            obj_leavePolicy.leave_code = dr["pays_empl_leave_policy_leave_code"].ToString();
                            obj_leavePolicy.leave_policy_id = dr["pays_empl_leave_policy_id"].ToString();
                            obj_leavePolicy.combination_of_leave = dr["combination_of_leave"].ToString();
                            obj_leavePolicy.combination_leave_Desc = dr["combination_leave_Desc"].ToString();
                            obj_leavePolicy.monthly_allowed = dr["pays_empl_leave_policy_monthly_allowed_leave_day"].ToString();
                            obj_leavePolicy.termly_allowed = dr["pays_empl_leave_policy_termly_allowed_leave_days"].ToString();
                            obj_leavePolicy.yearly_allowed = dr["pays_empl_leave_policy_yearly_allowed_leave_days"].ToString();
                            obj_leavePolicy.max_leave_per_Application = dr["pays_empl_leave_policy_max_leave_per_application"].ToString();
                            obj_leavePolicy.is_prefix_holiday_allowed = dr["pays_empl_leave_policy_is_prefix_holiday_allowed"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.post_holiday_allowed = dr["pays_empl_leave_policy_post_holiday_allowed"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.prefix_weekend_allowed = dr["pays_empl_leave_policy_prefix_weekend_allowed"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.post_weekend_allowed = dr["pays_empl_leave_policy_post_weekend_allowed"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.combination_of_holiday_and_weekend_allowed = dr["pays_empl_leave_policy_holiday_weekend_allowed"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.pays_empl_leave_policy_effective_from = db.UIDDMMYYYYformat(dr["pays_empl_leave_policy_effective_from"].ToString());
                            obj_leavePolicy.pays_empl_leave_policy_status = dr["pays_empl_leave_policy_status"].ToString().Equals("Y") ? "Yes" : "No";
                            obj_leavePolicy.pays_empl_leave_policy_created_by = dr["pays_empl_leave_policy_created_by"].ToString();
                            listOfEmployeeLeavePolicy.Add(obj_leavePolicy);
                        }
                    }
                }

            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, listOfEmployeeLeavePolicy);
            }
            return Request.CreateResponse(HttpStatusCode.OK, listOfEmployeeLeavePolicy);
        }
    }
}