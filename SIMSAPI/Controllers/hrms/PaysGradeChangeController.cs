﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/PaysGradeChange")]
    [BasicAuthentication]
    public class PaysGradeChangeController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("Get_gc_company_Code")]
        public HttpResponseMessage Get_gc_company_Code()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_company_master]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.gc_company_code = dr["co_company_code"].ToString();
                        objph.gc_company_desc = dr["co_desc"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        [Route("Get_gc_Code")]
        public HttpResponseMessage Get_gc_Code()
        {
            List<Pers109> gc_company = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_grade]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    while (dr.Read())
                    {
                        Pers109 objph = new Pers109();
                        objph.gc_new_grade = dr["gr_code"].ToString();
                        objph.gc_new_grade_desc = dr["gr_desc"].ToString();
                        objph.gc_old_grade = dr["gr_code"].ToString();
                        objph.gc_old_grade_desc = dr["gr_desc"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }


        [Route("Getall_grade_change")]
        public HttpResponseMessage Getall_grade_change()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Getall_grade_change(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Getall_grade_change"));

            List<Pers109> pay_list = new List<Pers109>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays_grade_change]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    while (dr.Read())
                    {
                        Pers109 pay = new Pers109();
                        pay.gc_number = dr["gc_number"].ToString();
                        pay.gc_emp_name = dr["gc_emp_name"].ToString();
                        pay.gc_company_code = dr["gc_company_code"].ToString();
                        pay.gc_company_desc = dr["gc_company_desc"].ToString();
                        pay.gc_mgr_confirm_tag = dr["gc_mgr_confirm_tag"].Equals("Y") ? true : false;
                        pay.gc_old_grade = dr["gc_old_grade"].ToString();
                        pay.gc_old_grade_desc = dr["gc_old_grade_desc"].ToString();
                        pay.gc_new_grade = dr["gc_new_grade"].ToString();
                        pay.gc_new_grade_desc = dr["gc_new_grade_desc"].ToString();
                        //pay.gc_effect_from = DateTime.Parse(dr["gc_effect_from"].ToString()).ToShortDateString();
                        pay.gc_effect_from = db.UIDDMMYYYYformat(dr["gc_effect_from"].ToString());
                        //pay.gc_retro_effect_from = DateTime.Parse(dr["gc_retro_effect_from"].ToString()).ToShortDateString();
                        pay.gc_retro_effect_from = db.UIDDMMYYYYformat(dr["gc_retro_effect_from"].ToString());

                        pay_list.Add(pay);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, pay_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, pay_list);
            }
        }

        [Route("CUDPaysGradeChange")]
        public HttpResponseMessage CUDLibraryStatus(List<Pers109> data)
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers109 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[pays_grade_change]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@gc_number", obj.gc_number),
                                new SqlParameter("@gc_company_code", obj.gc_company_code),
                                new SqlParameter("@gc_mgr_confirm_tag", obj.gc_mgr_confirm_tag==true?"Y":"N"),
                                new SqlParameter("@gc_old_grade", obj.gc_old_grade),
                                new SqlParameter("@gc_new_grade", obj.gc_new_grade),
                                new SqlParameter("@gc_effect_from",db.DBYYYYMMDDformat(obj.gc_effect_from)),
                                new SqlParameter("@gc_retro_effect_from",db.DBYYYYMMDDformat(obj.gc_retro_effect_from))
                     });
                        if (ins > 0)
                        {
                            if (obj.opr.Equals("U"))
                                message.strMessage = "Record Updated Sucessfully!!";
                            else if (obj.opr.Equals("I"))
                                message.strMessage = "Record Added Sucessfully!!";
                            else if (obj.opr.Equals("D"))
                                message.strMessage = "Record Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {
                            // message.strMessage = "";
                            message.strMessage = "Grade is already present";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }

            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
    }
}