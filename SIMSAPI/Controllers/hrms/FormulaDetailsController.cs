﻿using log4net;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.messageClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/FormulaDetails")]
    [BasicAuthentication]
    public class FormulaDetailsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// In this function get compan code and bind company name
        /// </summary>
        /// <returns></returns>
        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<FormulaClass> mod_list = new List<FormulaClass>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'K'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FormulaClass simsobj = new FormulaClass();
                            simsobj.company_code = dr["co_company_code"].ToString();
                            simsobj.company_name = dr["co_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        /// <summary>
        /// In this function geting pay code description
        /// </summary>
        /// <returns></returns>
        [Route("GetPayCode")]
        public HttpResponseMessage GetPayCode()
        {
            List<FormulaClass> gc_company = new List<FormulaClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),

                         });
                    while (dr.Read())
                    {
                        FormulaClass objph = new FormulaClass();
                        objph.pd_pay_code = dr["cp_code"].ToString();
                        objph.pd_pay_code_nm = dr["cp_desc"].ToString();
                        objph.paysEarnDed = dr["pc_earn_dedn_tag"].ToString();
                        gc_company.Add(objph);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, gc_company);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, gc_company);
            }
        }

        /// <summary>
        /// In this Function get all details from 2 table Formula and Formula_details
        /// </summary>
        /// <param name="fo_company_code">Pass Comany code</param>
        /// <returns></returns>
        [Route("getAllFormulaDetails")]
        public HttpResponseMessage getAllFormulaDetails()
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFormulaDetails(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "HRMS", "getAllFormulaDetails"));
            List<FormulaClass> list = new List<FormulaClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_Formula_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "H"),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            #region To get All Details
                            FormulaClass obj_FormulaClass = new FormulaClass();
                            obj_FormulaClass.company_name = dr["company_name"].ToString();
                            obj_FormulaClass.company_code = dr["fo_company_code"].ToString();
                            obj_FormulaClass.pd_pay_code = dr["fo_pay_code"].ToString();
                            obj_FormulaClass.pay_code_description = dr["pay_code_description"].ToString();
                            obj_FormulaClass.formula_name = dr["formul_Description"].ToString();
                            //obj_FormulaClass.serial_number = dr["fo_serial_no"].ToString();
                            obj_FormulaClass.percantage_number = dr["fo_percentage"].ToString();
                            obj_FormulaClass.month_number = dr["fo_month"].ToString();
                            obj_FormulaClass.operatore = dr["fo_operator"].ToString();
                            list.Add(obj_FormulaClass);
                            #endregion
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        /// <summary>
        /// In this function get all details as per given data by user
        /// </summary>
        /// <param name="formulaName">search name</param>
        /// <returns></returns>
        [Route("getAllFormulaDetails")]
        public HttpResponseMessage getAllFormulaDetails(string formulaName)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFormulaDetails(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "HRMS", "getAllFormulaDetails"));
            List<FormulaClass> list = new List<FormulaClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_Formula_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@fm_desc",formulaName),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            #region To get All Details
                            FormulaClass obj_FormulaClass = new FormulaClass();
                            obj_FormulaClass.company_name = dr["company_name"].ToString();
                            obj_FormulaClass.company_code = dr["fo_company_code"].ToString();
                            obj_FormulaClass.pd_pay_code = dr["fo_pay_code"].ToString();
                            obj_FormulaClass.pay_code_description = dr["pay_code_description"].ToString();
                            obj_FormulaClass.formula_name = dr["formul_Description"].ToString();
                            //obj_FormulaClass.serial_number = dr["fo_serial_no"].ToString();
                            obj_FormulaClass.percantage_number = dr["fo_percentage"].ToString();
                            obj_FormulaClass.month_number = dr["fo_month"].ToString();
                            obj_FormulaClass.operatore = dr["fo_operator"].ToString();
                            list.Add(obj_FormulaClass);
                            #endregion
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllFormulaDetailsSearch")]
        public HttpResponseMessage getAllFormulaDetailsSearch(string formulaName)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFormulaDetails(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "HRMS", "getAllFormulaDetails"));
            List<FormulaClass> list = new List<FormulaClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_Formula_Details_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@fm_desc",formulaName),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            #region To get All Details
                            FormulaClass obj_FormulaClass = new FormulaClass();
                            obj_FormulaClass.company_name = dr["company_name"].ToString();
                            obj_FormulaClass.company_code = dr["fo_company_code"].ToString();
                            obj_FormulaClass.pd_pay_code = dr["fo_pay_code"].ToString();
                            obj_FormulaClass.pay_code_description = dr["pay_code_description"].ToString();
                            obj_FormulaClass.formula_name = dr["formul_Description"].ToString();
                            //obj_FormulaClass.serial_number = dr["fo_serial_no"].ToString();
                            obj_FormulaClass.percantage_number = dr["fo_percentage"].ToString();
                            obj_FormulaClass.month_number = dr["fo_month"].ToString();
                            obj_FormulaClass.operatore = dr["fo_operator"].ToString();
                            list.Add(obj_FormulaClass);
                            #endregion
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        /// <summary>
        /// in this function adding into data base
        /// </summary>
        /// <param name="data">list of user data</param>
        /// <returns></returns>
        [Route("insertFormulaDetailsData")]
        public HttpResponseMessage insertFormulaDetailsData(List<FormulaClass> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (FormulaClass simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_Formula_Details_Proc]",
                         new List<SqlParameter>()
                         {

                             #region Adding data into database
                             new SqlParameter("@opr", "I"),
                             new SqlParameter("@fo_company_code",simsobj.company_code),
                             new SqlParameter("@fm_desc",simsobj.formulaname),
                             new SqlParameter("@fo_pay_code",simsobj.pdpaycode),
                             new SqlParameter("@fo_percentage",simsobj.percantagenumber),
                             new SqlParameter("@fo_month",simsobj.monthnumber),
                             new SqlParameter("@fo_operator",simsobj.operatornumber),
                             #endregion
                         });
                        #region Sending Response
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                        #endregion
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        /// <summary>
        /// in this function update the user data
        /// </summary>
        /// <param name="data">user data</param>
        /// <returns></returns>
        [Route("updateFormulaDetailsData")]
        public HttpResponseMessage updateFormulaDetailsData(List<FormulaClass> data)
        {

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (FormulaClass simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_Formula_Details_Proc]",
                         new List<SqlParameter>()
                         {

                             #region Adding data into database
                             new SqlParameter("@opr", "U"),
                             new SqlParameter("@fo_company_code",simsobj.company_code),
                             new SqlParameter("@fm_desc",simsobj.formulaname),
                             new SqlParameter("@fo_pay_code",simsobj.pdpaycode),
                             new SqlParameter("@fo_percentage",simsobj.percantagenumber),
                             new SqlParameter("@fo_month",simsobj.monthnumber),
                             new SqlParameter("@fo_operator",simsobj.operatornumber),
                             #endregion
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        /// <summary>
        /// in this function we are deleting record as per user data
        /// </summary>
        /// <param name="obj">user selection data</param>
        /// <returns></returns>
        [Route("FormulaDetailsDeleting")]
        public HttpResponseMessage FormulaDetailsDeleting(List<FormulaClass> obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : FormulaDetailsDeleting(),PARAMETERS ::";
            Log.Debug(string.Format(debug, "HRMS", "FormulaDetailsDeleting"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (FormulaClass simsobj in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_Formula_Details_Proc]",
                            new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@fo_cp_desc", simsobj.formulaname),
                            new SqlParameter("@fo_company_code",simsobj.company_code),
                             });


                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }

                    }


                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }
    }
}