﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/PaycodewiseEmployee")]
    public class PayrollEmployee_1Controller : ApiController
    {

       

        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 hrmsObj = new Pers072();
                            hrmsObj.dept_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 hrmsObj = new Pers072();
                            hrmsObj.desg_code = dr["dg_code"].ToString();
                            hrmsObj.desg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetGrade")]
        public HttpResponseMessage GetGrade()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.gr_code = dr["gr_code"].ToString();
                            simsobj.gr_name = dr["gr_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetPayCode")]
        public HttpResponseMessage GetPayCode()
        {
            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'D'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.paycode_code = dr["pc_code"].ToString();
                            simsobj.paycode_name = dr["cp_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("EmpDetails")]
        public HttpResponseMessage EmpDetails(Pers072 varlst)
        {

            List<Pers072> mod_list = new List<Pers072>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@pa_pay_code", varlst.paycode_code),
                            new SqlParameter("@date", db.DBYYYYMMDDformat(varlst.pa_effective_from)),
                            new SqlParameter("@em_dept_code", varlst.dept_code),
                            new SqlParameter("@em_desg_code", varlst.desg_code),
                            new SqlParameter("@em_grade_code", varlst.gr_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers072 simsobj = new Pers072();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.emp_name = dr["em_name"].ToString();
                            simsobj.paycode_code = dr["pa_pay_code"].ToString();
                            simsobj.paycode_name = dr["paycode_name"].ToString();
                            simsobj.pay_amount = dr["pa_amount"].ToString();
                            simsobj.pd_remark = dr["pa_remark"].ToString();
                            if (!string.IsNullOrEmpty(dr["pa_effective_from"].ToString()))
                            {
                                //simsobj.pa_effective_from = DateTime.Parse(dr["pa_effective_from"].ToString()).ToShortDateString();
                                //simsobj.pa_effective_from = dr["pa_effective_from"].ToString();
                                simsobj.pa_effective_from = db.UIDDMMYYYYformat(dr["pa_effective_from"].ToString());
                            }
                                
                            if (!string.IsNullOrEmpty(dr["pa_effective_upto"].ToString()))
                            {
                                //simsobj.pa_effective_upto = DateTime.Parse(dr["pa_effective_upto"].ToString()).ToShortDateString();
                                //simsobj.pa_effective_upto = dr["pa_effective_upto"].ToString();
                                simsobj.pa_effective_upto = db.UIDDMMYYYYformat(dr["pa_effective_upto"].ToString());
                            }
                                
                            mod_list.Add(simsobj);

                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("UpdatePaysPayable")]
        public HttpResponseMessage UpdatePaysPayable(List<Pers072> obj)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Pers072 data in obj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_payroll_empl_paycode_wise_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@pe_number", data.em_number),
                                new SqlParameter("@from_date",db.DBYYYYMMDDformat(data.pa_effective_from)), 
                                new SqlParameter("@upto_date", db.DBYYYYMMDDformat(data.pa_effective_upto)), 
                                //DateTime.Parse(data.pa_effective_from).Year + "-" + DateTime.Parse(data.pa_effective_from).Month + "-" + DateTime.Parse(data.pa_effective_from).Day),
                                new SqlParameter("@pa_pay_code", data.pd_pay_code),
                                new SqlParameter("@pa_amount", data.pd_amount),
                                new SqlParameter("@pa_remark", data.pd_remark),
                                new SqlParameter("@ph_new_amount", data.ph_new_amount),
                                new SqlParameter("@ph_old_amount", data.ph_old_amount),
                                new SqlParameter("@ph_user_name", data.ph_user_name)

                        });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        dr.Close();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}