﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using System.Web;
using System.IO;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/EmployeeLetterReq")]
    public class EmployeeLetterRequestController: ApiController
    {
        [Route("Get_EmpDoc")]
        public HttpResponseMessage Get_EmpDoc()
        {
            List<Sims104> sim_list = new List<Sims104>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims104 simsobj = new Sims104();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            sim_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, sim_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sim_list);
        }

        [Route("Get_Purpose")]
        public HttpResponseMessage Get_Purpose()
        {
            List<Sims104> sim_list = new List<Sims104>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'B'),                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims104 simsobj = new Sims104();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sim_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, sim_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sim_list);
        }

        [Route("Get_ReqDoc")]
        public HttpResponseMessage Get_ReqDoc(string user)
        {
            List<Sims105> sim_list = new List<Sims105>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),    
                                new SqlParameter("@sims_certificate_requested_by", user),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_requested_by = dr["requested_by"].ToString();
                            simsobj.sims_certificate_reason = dr["sims_certificate_reason"].ToString();
                            simsobj.sims_certificate_dept_code = dr["sims_certificate_dept_code"].ToString();
                            simsobj.sims_certificate_dept_name = dr["sims_certificate_dept_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_request_date"].ToString()))
                                simsobj.sims_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_certificate_request_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_certificate_expected_date"].ToString()))
                                simsobj.sims_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_certificate_expected_date"].ToString());                           
                            simsobj.sims_certificate_approved_by = dr["approved_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_approve_date"].ToString()))
                                simsobj.sims_certificate_approve_date = db.UIDDMMYYYYformat(dr["sims_certificate_approve_date"].ToString());                           
                            simsobj.sims_certificate_issued_by = dr["issued_by"].ToString();
                            simsobj.sims_certificate_approved_status = dr["sims_certificate_approved_status"].ToString(); 
                            simsobj.sims_certificate_request_status = dr["sims_certificate_request_status"].ToString();
                            simsobj.print_count = dr["print_count"].ToString();
                            simsobj.sims_certificate_doc_path = dr["sims_certificate_doc_path"].ToString();
                            simsobj.sims_certificate_PurposeName = dr["sims_certificate_PurposeName"].ToString();
                            simsobj.sims_certificate_other_purpose_reason = dr["sims_certificate_other_purpose_reason"].ToString();
                            if (!string.IsNullOrEmpty(simsobj.sims_certificate_other_purpose_reason))
                            {
                                simsobj.sims_certificate_PurposeName = simsobj.sims_certificate_PurposeName + " (" + simsobj.sims_certificate_other_purpose_reason + ")";
                            }
                            sim_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, sim_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sim_list);
        }

        [Route("Get_ReqDocApprove")]
        public HttpResponseMessage Get_ReqDocApprove(string date_from, string date_to,string sims_certificate_request_status,string sims_certificate_approved_status,string userName)
        {
            List<Sims105> sim_list = new List<Sims105>();
            if (date_from == "undefined" || date_from == "\"\"")
            {
                date_from = null;
            }
            if (date_to == "undefined" || date_to == "\"\"")
            {
                date_to = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'M'),    
                                new SqlParameter("@sims_certificate_request_date", date_from),
                                new SqlParameter("@sims_certificate_request_date1", date_to),
                                new SqlParameter("@sims_certificate_request_status",sims_certificate_request_status),
                                new SqlParameter("@sims_certificate_approved_status", sims_certificate_approved_status),
                                  new SqlParameter("@sims_certificate_requested_by", userName),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_requested_by = dr["sims_certificate_requested_by"].ToString();
                            simsobj.sims_certificate_requested_by_name = dr["requested_by"].ToString();
                            simsobj.sims_certificate_reason = dr["sims_certificate_reason"].ToString();
                            simsobj.sims_certificate_dept_code = dr["sims_certificate_dept_code"].ToString();
                            simsobj.sims_certificate_dept_name = dr["sims_certificate_dept_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_request_date"].ToString()))
                                simsobj.sims_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_certificate_request_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_certificate_expected_date"].ToString()))
                                simsobj.sims_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_certificate_expected_date"].ToString());
                            simsobj.sims_certificate_approved_by = dr["approved_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_approve_date"].ToString()))
                                simsobj.sims_certificate_approve_date = db.UIDDMMYYYYformat(dr["sims_certificate_approve_date"].ToString());
                            simsobj.sims_certificate_issued_by = dr["issued_by"].ToString();
                            simsobj.sims_certificate_approved_status = dr["sims_certificate_approved_status"].ToString();
                            simsobj.sims_certificate_request_status = dr["sims_certificate_request_status"].ToString();
                            simsobj.print_count = dr["print_count"].ToString();
                            simsobj.sims_certificate_doc_path = dr["sims_certificate_doc_path"].ToString();
                            sim_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, sim_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sim_list);
        }



        [Route("Get_ReqDocIssue")]
        public HttpResponseMessage Get_ReqDocIssue(string date_from, string date_to)
        {
            List<Sims105> sim_list = new List<Sims105>();
            if (date_from == "undefined" || date_from == "\"\"")
            {
                date_from = null;
            }
            if (date_to == "undefined" || date_to == "\"\"")
            {
                date_to = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'L'),    
                                new SqlParameter("@sims_certificate_request_date", date_from),
                                new SqlParameter("@sims_certificate_request_date1", date_to)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims105 simsobj = new Sims105();
                            simsobj.sims_certificate_request_number = dr["sims_certificate_request_number"].ToString();
                            simsobj.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            simsobj.sims_certificate_name = dr["sims_certificate_name"].ToString();
                            simsobj.sims_certificate_requested_by = dr["sims_certificate_requested_by"].ToString();
                            simsobj.sims_certificate_requested_by_name = dr["requested_by"].ToString();
                            simsobj.sims_certificate_reason = dr["sims_certificate_reason"].ToString();
                            simsobj.sims_certificate_dept_code = dr["sims_certificate_dept_code"].ToString();
                            simsobj.sims_certificate_dept_name = dr["sims_certificate_dept_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_request_date"].ToString()))
                                simsobj.sims_certificate_request_date = db.UIDDMMYYYYformat(dr["sims_certificate_request_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_certificate_expected_date"].ToString()))
                                simsobj.sims_certificate_expected_date = db.UIDDMMYYYYformat(dr["sims_certificate_expected_date"].ToString());
                            simsobj.sims_certificate_approved_by = dr["approved_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_certificate_approve_date"].ToString()))
                                simsobj.sims_certificate_approve_date = db.UIDDMMYYYYformat(dr["sims_certificate_approve_date"].ToString());
                            simsobj.sims_certificate_issued_by = dr["issued_by"].ToString();
                            simsobj.sims_certificate_approved_status = dr["sims_certificate_approved_status"].ToString();
                            simsobj.sims_certificate_request_status = dr["sims_certificate_request_status"].ToString();
                            simsobj.print_count = dr["print_count"].ToString();
                            simsobj.sims_certificate_doc_path = dr["sims_certificate_doc_path"].ToString();
                            try
                            {
                                simsobj.report_loc = dr["report_loc"].ToString();
                                simsobj.sims_certificate_PurposeName = dr["sims_certificate_PurposeName"].ToString();

                            }
                            catch(Exception rx) { }
                            sim_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, sim_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sim_list);
        }

        [Route("CUDEmployeeReqLetter")]
        public HttpResponseMessage CUDEmployeeReqLetter(List<Sims105> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims105 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_certificate_request_number", simsobj.sims_certificate_request_number),
                                new SqlParameter("@sims_certificate_request_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_request_date)),   
                                new SqlParameter("@sims_certificate_requested_by", simsobj.sims_certificate_requested_by),
                                new SqlParameter("@sims_certificate_number", simsobj.sims_certificate_number),
                                new SqlParameter("@sims_certificate_reason", simsobj.sims_certificate_reason),
                                new SqlParameter("@sims_certificate_expected_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_expected_date)),
                                new SqlParameter("@sims_certificate_dept_code", simsobj.sims_certificate_dept_code),
                                new SqlParameter("@sims_certificate_approved_by", simsobj.sims_certificate_approved_by),
                                new SqlParameter("@sims_certificate_approve_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_approve_date)),
                                new SqlParameter("@sims_certificate_issued_by", simsobj.sims_certificate_issued_by),
                                new SqlParameter("@sims_certificate_approved_status", simsobj.sims_certificate_approved_status),
                                new SqlParameter("@sims_certificate_request_status", simsobj.sims_certificate_request_status),
                                 new SqlParameter("@sims_certificate_Purpose", simsobj.sims_appl_parameter),
                                 new SqlParameter("@sims_certificate_other_purpose_reason", simsobj.sims_certificate_other_purpose_reason),
                     });
                        if (ins > 0)
                        {                          
                            insert = true;
                        }

                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           
        }

        [Route("CUDEmployeeIssueLetter")]
        public HttpResponseMessage CUDEmployeeIssueLetter(List<Sims105> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims105 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_employee_letter_request_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_certificate_request_number", simsobj.sims_certificate_request_number),
                                new SqlParameter("@sims_certificate_request_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_request_date)),   
                                new SqlParameter("@sims_certificate_requested_by", simsobj.sims_certificate_requested_by),
                                new SqlParameter("@sims_certificate_number", simsobj.sims_certificate_number),
                                new SqlParameter("@sims_certificate_reason", simsobj.sims_certificate_reason),
                                new SqlParameter("@sims_certificate_expected_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_expected_date)),
                                new SqlParameter("@sims_certificate_dept_code", simsobj.sims_certificate_dept_code),
                                new SqlParameter("@sims_certificate_approved_by", simsobj.sims_certificate_approved_by),
                                new SqlParameter("@sims_certificate_approve_date", db.DBYYYYMMDDformat(simsobj.sims_certificate_approve_date)),
                                new SqlParameter("@sims_certificate_issued_by", simsobj.sims_certificate_issued_by),
                                new SqlParameter("@sims_certificate_approved_status", simsobj.sims_certificate_approved_status),
                                new SqlParameter("@sims_certificate_request_status", simsobj.sims_certificate_request_status),
                                new SqlParameter("@sims_certificate_doc_path", simsobj.sims_certificate_doc_path),
                                new SqlParameter("@sims_certificate_print_count", simsobj.sims_certificate_print_count),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Images/Requestdoc/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

    }
}