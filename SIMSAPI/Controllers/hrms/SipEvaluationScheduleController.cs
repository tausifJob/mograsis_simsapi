﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.SipEvaluationScheduleController
{
    [RoutePrefix("api/SipEvaluation")]
    [BasicAuthentication]
    public class SipEvaluationScheduleController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllSipEvalSchedule")]
        public HttpResponseMessage getAllSipEvalSchedule()
        {
            List<Per302> desg_list = new List<Per302>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),      
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();

                            simsobj.sims_kpi_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_kpi_goal_target_code = dr["sims_sip_goal_target_code"].ToString();
                            simsobj.sims_sip_user_code = dr["sims_sip_goal_target_user_code"].ToString();
                            simsobj.sims_sip_user_name = dr["user_name"].ToString();
                            simsobj.sims_sip_goal_target_schedule1 = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_schedule1"].ToString());
                            simsobj.sims_sip_goal_target_reviewer1_code = dr["sims_sip_goal_target_reviewer1_code"].ToString();
                            simsobj.emp_name1 = dr["emp_name1"].ToString();

                            simsobj.sims_sip_goal_target_schedule2 = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_schedule2"].ToString());
                            simsobj.sims_sip_goal_target_reviewer2_code = dr["sims_sip_goal_target_reviewer2_code"].ToString();
                            simsobj.emp_name2 = dr["emp_name2"].ToString();

                            simsobj.sims_sip_goal_target_schedule3 = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_schedule3"].ToString());
                            simsobj.sims_sip_goal_target_reviewer3_code = dr["sims_sip_goal_target_reviewer3_code"].ToString();
                            simsobj.emp_name3 = dr["emp_name3"].ToString();

                            simsobj.sims_sip_goal_target_schedule4 = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_schedule4"].ToString());
                            simsobj.sims_sip_goal_target_reviewer4_code = dr["sims_sip_goal_target_reviewer4_code"].ToString();
                            simsobj.emp_name4 = dr["emp_name4"].ToString();

                            simsobj.sims_sip_goal_target_schedule5 = db.UIDDMMYYYYformat(dr["sims_sip_goal_target_schedule5"].ToString());
                            simsobj.sims_sip_goal_target_reviewer5_code = dr["sims_sip_goal_target_reviewer5_code"].ToString();
                            simsobj.emp_name5 = dr["emp_name5"].ToString();

                            desg_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, desg_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, desg_list);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.sip_company_code = dr["comp_code"].ToString();
                            simsobj.sip_company_code_name = dr["comp_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDesigantion")]
        public HttpResponseMessage getDesigantion()
        {
            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.desg_code = dr["dg_code"].ToString();
                            simsobj.desg_name = dr["dg_desc"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDepartment")]
        public HttpResponseMessage getDepartment()
        {
            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getReviewer")]
        public HttpResponseMessage getReviewer()
        {
            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'G'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.sims_sip_reviewer_code = dr["sims_sip_reviewer_code"].ToString();
                            simsobj.sims_sip_reviewer_name = dr["Employee_Name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getUserInfo")]
        public HttpResponseMessage getUserInfo(string academic_year, string grade_code, string section_code)
        {

            if (section_code == "undefined")
                section_code = null;

            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sims_sip_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_code",section_code)

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_sip_user_code = dr["sims_student_login_id"].ToString();
                            simsobj.sims_user_name = dr["Student_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getStaffInfo")]
        public HttpResponseMessage getStaffInfo(string company_code, string dept_code, string desg_code)
        {

            if (desg_code == "undefined")
                desg_code = null;

            if (dept_code == "undefined")
                dept_code = null;

            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@em_company_code",company_code),
                            new SqlParameter("@em_dept_code",dept_code),
                            new SqlParameter("@em_desg_code",desg_code)

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.sims_sip_user_code = dr["em_login_code"].ToString();
                            simsobj.sims_user_name = dr["em_first_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getUserInfoByUserCode")]
        public HttpResponseMessage getUserInfoByUserCode(string user_code)
        {
            List<Per302> mod_list = new List<Per302>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_user_evaluation_schedule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@sims_sip_goal_target_user_code",user_code)

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per302 simsobj = new Per302();
                            simsobj.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.sims_sip_user_code = dr["sims_student_login_id"].ToString();
                            simsobj.sims_user_name = dr["Student_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDSipEvaluationSchedule")]
        public HttpResponseMessage CUDStudentHealth(List<Per302> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per302 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_user_evaluation_schedule_proc]",
                            new List<SqlParameter>() 
                         { 

                               new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@sims_sip_academic_year",simsobj.sims_goal_kpi_academic_year),
                               new SqlParameter("@sims_sip_goal_code",simsobj.sims_kpi_goal_code),
                               new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_kpi_goal_target_code),
                               new SqlParameter("@sims_sip_goal_target_user_code", simsobj.sims_sip_user_code),
                               new SqlParameter("@old_sims_sip_goal_target_user_code",simsobj.old_sims_sip_user_code),
                               new SqlParameter("@sims_sip_goal_target_schedule1",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_schedule1)),
                               new SqlParameter("@sims_sip_goal_target_reviewer1_code",simsobj.sims_sip_goal_target_reviewer1_code),
                               new SqlParameter("@sims_sip_goal_target_schedule2",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_schedule2)),
                               new SqlParameter("@sims_sip_goal_target_reviewer2_code",simsobj.sims_sip_goal_target_reviewer2_code),
                               new SqlParameter("@sims_sip_goal_target_schedule3",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_schedule3)),
                               new SqlParameter("@sims_sip_goal_target_reviewer3_code",simsobj.sims_sip_goal_target_reviewer3_code),
                               new SqlParameter("@sims_sip_goal_target_schedule4",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_schedule4)), 
                               new SqlParameter("@sims_sip_goal_target_reviewer4_code",simsobj.sims_sip_goal_target_reviewer4_code),
                               new SqlParameter("@sims_sip_goal_target_schedule5",db.DBYYYYMMDDformat(simsobj.sims_sip_goal_target_schedule5)),
                               new SqlParameter("@sims_sip_goal_target_reviewer5_code", simsobj.sims_sip_goal_target_reviewer5_code),
                         
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}