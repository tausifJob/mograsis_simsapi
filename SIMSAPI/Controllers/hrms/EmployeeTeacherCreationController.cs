﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/employeeteachermapping")]
    public class EmployeeTeacherCreationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        #region Employee-Teacher Mapping

        [Route("Get_Mapped_Employee1")]
        public HttpResponseMessage Get_Mapped_Employee1(string data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Mapped_Employee(),PARAMETERS :: DATA";
            Log.Debug(string.Format(debug, "HRMS", "Get_Mapped_Employee", data));
            sims173 simsobj =new sims173();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<sims173>(data);
            }
           
            List<sims173> lstEmp = new List<sims173>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[emp_teacher_mapping_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@company_code", simsobj.company_code),
                new SqlParameter("@dept_code", simsobj.dept_code),
                new SqlParameter("@desg_code", simsobj.desg_code),
                new SqlParameter("@grade_code", simsobj.grade_code),
               new SqlParameter("@em_login_code", simsobj.em_login_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims173 obj = new sims173();
                            obj.sims_emp_code = dr["em_login_code"].ToString();
                            obj.sims_emp_name = dr["Empname"].ToString();
                            obj.sims_teacher_type = dr["sims_teacher_type"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_create_assignments"].ToString()) || dr["sims_create_assignments"].ToString().ToUpper() == "N")
                                obj.sims_emp_ca = false;
                            else
                                obj.sims_emp_ca = true;
                            if (string.IsNullOrEmpty(dr["sims_create_categories"].ToString()) || dr["sims_create_categories"].ToString().ToUpper() == "N")
                                obj.sims_emp_cc = false;
                            else
                                obj.sims_emp_cc = true;

                            if (string.IsNullOrEmpty(dr["sims_status"].ToString()) || dr["sims_status"].ToString().ToUpper() == "I")
                                obj.sims_emp_status = false;
                            else
                                obj.sims_emp_status = true;
                            lstEmp.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lstEmp);
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);
        }


        [Route("CUDpdate_Mapped_Teacher_Employee")]
        public HttpResponseMessage CUDpdate_Mapped_Teacher_Employee(string opr,List<sims173> lstEmp)
        {
            Message message=new Message();

                try
                {
                    if (lstEmp.Count > 0)
                    {
                        for (int i = 0; i < lstEmp.Count; i++)
                        {
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();

                                sims173 tobj = lstEmp[i];
                                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[emp_teacher_mapping_proc]",
                                    new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", opr),
                        new SqlParameter("@company_code", ""),
                        new SqlParameter("@sims_appl_code", "Per099"),
                        new SqlParameter("@sims_appl_form_field", "Teacher Type"),
                        new SqlParameter("@sims_teacher_name", tobj.sims_emp_name),
                        new SqlParameter("@sims_employee_code", tobj.sims_emp_code),
                        new SqlParameter("@sims_teacher_type", tobj.sims_teacher_type),
                        new SqlParameter("@sims_create_assignments",tobj.sims_emp_ca.Equals(true)?"Y":"N"),
                        new SqlParameter("@sims_create_categories",tobj.sims_emp_cc.Equals(true)?"Y":"N"),
                        new SqlParameter("@sims_status",tobj.sims_emp_status.Equals(true)?"A":"I")
                    });
                                if (dr.RecordsAffected > 0)
                                {
                                    if (opr.Equals("U"))
                                        message.strMessage = "Teacher Created Sucessfully";
                                }
                                else
                                {
                                    if (opr.Equals("U"))
                                        message.strMessage = "Teacher Not Created";
                                   
                                }

                            }
                        }

                    }

                    else
                    {
                        message.strMessage = "Error In Parsing Information";
                        message.messageType = MessageType.Error;
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                    }
            }
            catch (Exception x)
            {
                //if (opr.Equals("U"))
                   // message.strMessage = "Error In Updating Information";
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }



        #endregion


    }
}