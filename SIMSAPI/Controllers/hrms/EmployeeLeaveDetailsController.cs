﻿using SIMSAPI.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/common/EmployeeLeaveDetails")]
    public class EmployeeLeaveDetailsController : ApiController
    {
        [Route("GetStatusforLeave")]
        public HttpResponseMessage GetStatusforLeave()
        {
            List<Pers318_workflow> lst = new List<Pers318_workflow>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@Opr", "M")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers318_workflow obj = new Pers318_workflow();
                            obj.sims_workflow_status_code = dr["pays_appl_parameter"].ToString();
                            obj.sims_workflow_status = dr["pays_appl_form_field_value1"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lst);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetEMPLeaveDetails")]
        public HttpResponseMessage GetEMPLeaveDetails(string comp_id, string emp_id, string status_code)
        {
            List<Pers300_le> lstEmp_leave = new List<Pers300_le>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_leave_details_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@Opr", "T"),
                              new SqlParameter("@lt_company_code", comp_id),
                              new SqlParameter("@lt_number", emp_id),
                              new SqlParameter("@lt_mgrctag", status_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers300_le hrmsobj = new Pers300_le();
                            hrmsobj.em_login_code = dr["EMP"].ToString();
                            hrmsobj.EmpName = dr["EmployeeName"].ToString();
                            hrmsobj.comp_code = dr["lt_company_code"].ToString();
                            hrmsobj.comp_name = dr["CompanyName"].ToString();
                            hrmsobj.leave_type = dr["LeaveDescription"].ToString();
                            hrmsobj.lt_start_date = db.UIDDMMYYYYformat(dr["lt_start_date"].ToString());
                            hrmsobj.lt_end_date = db.UIDDMMYYYYformat(dr["lt_end_date"].ToString());
                            hrmsobj.lt_days = dr["lt_days"].ToString();
                            hrmsobj.lt_hours = dr["lt_hours"].ToString();
                            hrmsobj.lt_modified_on = db.UIDDMMYYYYformat(dr["lt_modified_on"].ToString());
                            hrmsobj.lt_remarks = dr["lt_remarks"].ToString();
                            hrmsobj.leavestatus = dr["LeaveStatus"].ToString();
                            hrmsobj.lt_mgr_confirm_tag = dr["lt_mgr_confirm_tag"].ToString();
                            hrmsobj.requestdate = db.UIDDMMYYYYformat(dr["lt_request_date"].ToString());
                            hrmsobj.updated = false;
                            lstEmp_leave.Add(hrmsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp_leave);
        }

        [Route("CUDUpdate_pay_empleavedoc")]
        public HttpResponseMessage CUDUpdate_pay_empleavedoc(Pers300_le obj)
        {
            bool inserted = false;
            string transacid = string.Empty;
            try
            {
                if (obj != null)
                {
                    string date = DateTime.Now.ToString();
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_leave_details_proc]",
                            new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr",'R'),
                                new SqlParameter("@lt_number", obj.em_login_code),
                                new SqlParameter("@lt_company_code", obj.comp_code),
                                new SqlParameter("@lt_start_date", db.DBYYYYMMDDformat(obj.lt_start_date)),
                                new SqlParameter("@lt_end_date", db.DBYYYYMMDDformat(obj.lt_end_date)),
                                new SqlParameter("@lt_mgrctag", "E"),
                                new SqlParameter("@lt_modified_on",DateTime.Now.Year.ToString()+'-'+DateTime.Now.Month.ToString()+'-'+DateTime.Now.Day.ToString()),
                                new SqlParameter("@lt_remarks", obj.lt_remarks)
                        });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}