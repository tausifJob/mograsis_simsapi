﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.GoalTargetKPIMeasureAchievmentController
{
    [RoutePrefix("api/GoalTargetKPIMeasureAchievement")]
    [BasicAuthentication]
    public class GoalTargetKPIMeasureAchievmentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllGoalTargetKPIMeasureAchievement")]
        public HttpResponseMessage getAllGoalTarget()
        {
            List<Per305> goaltarget_list = new List<Per305>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_academic_year = dr["sims_sip_academic_year"].ToString();
                            simsobj.sims_sip_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_sip_goal_desc = dr["sims_sip_goal_desc"].ToString();
                            simsobj.sims_sip_goal_target_code = dr["sims_sip_goal_target_code"].ToString();
                            simsobj.sims_sip_goal_target_desc = dr["sims_sip_goal_target_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_code = dr["sims_sip_goal_target_kpi_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_desc = dr["sims_sip_goal_target_kpi_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_code = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_code_m = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc_m = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_code = dr["sims_sip_goal_target_kpi_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_desc = dr["sims_sip_achievement_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_category_code = dr["sims_sip_goal_target_kpi_achievement_category_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_category_desc = dr["sims_sip_goal_target_kpi_achievement_category_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_status = dr["sims_sip_goal_target_kpi_achievement_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<Per305> lstModules = new List<Per305>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 sequence = new Per305();
                            sequence.sims_goal_kpi_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_goal_kpi_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getGoalName")]
        public HttpResponseMessage getGoalName()
        {
            List<Per305> goal_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_kpi_goal_code = dr["sims_sip_goal_code"].ToString();
                            simsobj.sims_kpi_goal_desc = dr["sims_sip_goal_desc"].ToString();
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getGoalTargetName")]
        public HttpResponseMessage getGoalTargetName(string sims_sip_goal_code)
        {

            List<Per305> goalTarget_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_sip_goal_code",sims_sip_goal_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_kpi_goal_target_code = dr["sims_sip_goal_target_code"].ToString();
                            simsobj.sims_kpi_goal_target_desc = dr["sims_sip_goal_target_desc"].ToString(); ;
                            goalTarget_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getGoalTargetKPIMeasure")]
        public HttpResponseMessage getGoalTargetKPIMeasure(string sims_sip_goal_code, string sims_sip_goal_target_code)
        {

            List<Per305> goalTarget_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_sip_goal_code",sims_sip_goal_code),
                            new SqlParameter("@sims_sip_goal_target_code",sims_sip_goal_target_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_goal_target_kpi_code = dr["sims_sip_goal_target_kpi_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_desc = dr["sims_sip_goal_target_kpi_desc"].ToString();
                            goalTarget_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getGoalTargetKPIMeasureDisplay")]
        public HttpResponseMessage getGoalTargetKPIMeasureDisplay(string sims_sip_academic_year, string sims_sip_goal_code, string sims_sip_goal_target_code, string sims_sip_goal_target_kpi_code)
        {

            List<Per305> goalTarget_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@sims_sip_academic_year",sims_sip_academic_year),
                            new SqlParameter("@sims_sip_goal_code",sims_sip_goal_code),
                            new SqlParameter("@sims_sip_goal_target_code",sims_sip_goal_target_code),
                            new SqlParameter("@sims_sip_goal_target_kpi_code",sims_sip_goal_target_kpi_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_goal_target_kpi_measure_code = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_code = dr["sims_sip_goal_target_kpi_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_desc = dr["sims_sip_achievement_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_category_code = dr["sims_sip_goal_target_kpi_achievement_category_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_category_desc = dr["sims_sip_goal_target_kpi_achievement_category_desc"].ToString();
                            simsobj.sims_sip_goal_target_kpi_achievement_status = dr["sims_sip_goal_target_kpi_achievement_status"].Equals("A") ? true : false;
                            goalTarget_list.Add(simsobj);


                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goalTarget_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getMeasure")]
        public HttpResponseMessage getMeasure()
        {

            List<Per305> measure_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_goal_target_kpi_measure_code_m = dr["sims_sip_goal_target_kpi_measure_code"].ToString();
                            simsobj.sims_sip_goal_target_kpi_measure_desc_m = dr["sims_sip_goal_target_kpi_measure_desc"].ToString();
                            measure_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAchievement")]
        public HttpResponseMessage getAchievement()
        {

            List<Per305> measure_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_sip_achievement_code = dr["sims_sip_achievement_code"].ToString();
                            simsobj.sims_sip_achievement_desc = dr["sims_sip_achievement_desc"].ToString();
                            measure_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getAchievementCategory")]
        public HttpResponseMessage getAchievementCategory()
        {
            List<Per305> measure_list = new List<Per305>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per305 simsobj = new Per305();
                            simsobj.sims_achievement_category_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_achievement_category_desc = dr["sims_appl_form_field_value1"].ToString();
                            measure_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, measure_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDGoalTargetKPIMeasureAchievement")]
        public HttpResponseMessage CUDGoalTargetKPIMeasureAchievement(List<Per305> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per305 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_goal_target_kpi_measure_achievement_proc]",
                            new List<SqlParameter>() 
                         { 
		 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_sip_academic_year", simsobj.sims_goal_kpi_academic_year),
                                new SqlParameter("@sims_sip_goal_code", simsobj.sims_kpi_goal_code),
                                new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_kpi_goal_target_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_code",simsobj.sims_sip_goal_target_kpi_code),
                               // new SqlParameter("@sims_sip_goal_target_kpi_measure_code",simsobj.sims_sip_goal_target_kpi_measure_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_measure_code",simsobj.sims_sip_goal_target_kpi_measure_code_m),
                                new SqlParameter("@old_sims_sip_goal_target_kpi_measure_code",simsobj.old_sims_sip_goal_target_kpi_measure_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_achievement_code",simsobj.sims_sip_achievement_code),
                                new SqlParameter("@old_sims_sip_goal_target_kpi_achievement_code",simsobj.old_sims_sip_goal_target_kpi_achievement_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_achievement_category_code",simsobj.sims_achievement_category_code),
                                new SqlParameter("@sims_sip_goal_target_kpi_achievement_status",simsobj.sims_sip_goal_target_kpi_achievement_status.Equals(true)?"A":"I"),
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}




