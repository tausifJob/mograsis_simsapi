﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FEE;
namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/Fee/CancelFeeReceipt")]
    [BasicAuthentication]
    public class CFRController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCFRYear")]
        public HttpResponseMessage getCFRYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCFRYear()";
            Log.Debug(string.Format(debug, "FEES", "getCFRYear"));

            List<CFR> acayear_list = new List<CFR>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cancel_fee_receipt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CFR simsobj = new CFR();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_status = dr["sims_academic_year_status"].ToString();

                            acayear_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }

        [Route("getCFRReceipts")]
        public HttpResponseMessage getCFRReceipts(string academic_year, string user_name, string enroll_list, string start_date, string end_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCFRYear()";
            Log.Debug(string.Format(debug, "FEES", "getCFRReceipts"));
            user_name = string.IsNullOrEmpty(user_name) ? null : user_name;
            enroll_list = string.IsNullOrEmpty(enroll_list) ? null : enroll_list;
            start_date = string.IsNullOrEmpty(start_date) ? null : start_date;
            end_date = string.IsNullOrEmpty(end_date) ? null : end_date;

            List<CFR> acayear_list = new List<CFR>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cancel_fee_receipt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@sims_enroll_number", enroll_list),
                                new SqlParameter("@start_date", db.DBYYYYMMDDformat(start_date)),
                                new SqlParameter("@end_date", db.DBYYYYMMDDformat(end_date)),
                                new SqlParameter("@doc_user", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CFR simsobj = new CFR();
                            simsobj.sims_receipt_no = dr["doc_no"].ToString();
                            simsobj.sims_receipt_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.sims_receipt_amount = dr["doc_total_amount"].ToString();
                            simsobj.sims_receipt_dt_code = dr["dt_code"].ToString();
                            simsobj.sims_receipt_status = dr["doc_status"].ToString();
                            simsobj.sims_receipt_status_desc = dr["Status_Desc"].ToString();
                            simsobj.sims_enrollment_nos = dr["enroll_number"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_student_name = dr["Name"].ToString();
                            simsobj.sims_receipt_genrated_by = dr["CreateUser"].ToString();
                            simsobj.IsSiblingReceipt = dr["IsSiblingReceipt"].ToString();
                            simsobj.sims_receipt_narration = string.Empty;
                            acayear_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }

        [Route("CFRCancelReceipts")]
        public HttpResponseMessage CFRCancelReceipts(string cancel_doc_date,string user_name, List<CFR> cancel_receipts)
        {
            bool res = true;
            try
            {
                for (int i = 0; i < cancel_receipts.Count; i++)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> param = new List<SqlParameter>();
                        if (cancel_receipts[i].IsSiblingReceipt.ToUpper() == "Y")
                            param.Add(new SqlParameter("@opr", "C"));
                        else
                            param.Add(new SqlParameter("@opr", 'U'));
                        param.Add(new SqlParameter("@sims_doc_no", cancel_receipts[i].sims_receipt_no));
                        param.Add(new SqlParameter("@sims_enroll_number", cancel_receipts[i].sims_enrollment_nos));
                        param.Add(new SqlParameter("@cur_code", cancel_receipts[i].sims_cur_code));
                        param.Add(new SqlParameter("@academic_year", cancel_receipts[i].sims_academic_year));
                        param.Add(new SqlParameter("@grade_code", cancel_receipts[i].sims_grade_code));
                        param.Add(new SqlParameter("@section_code", cancel_receipts[i].sims_section_code));
                        param.Add(new SqlParameter("@cancel_doc_date", db.DBYYYYMMDDformat(cancel_doc_date)));
                        param.Add(new SqlParameter("@doc_narration", cancel_receipts[i].sims_receipt_narration));
                        param.Add(new SqlParameter("@doc_user", user_name));
                        int ins = db.ExecuteStoreProcedureforInsert("sims_cancel_fee_receipt", param);
                        if (ins > 0)
                        {
                            res = true;
                        }
                        else
                        {
                            res = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }
    }
}