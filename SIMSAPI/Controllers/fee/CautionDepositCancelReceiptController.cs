﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/CancelCautionFeeReceipt")]
    public class CautionDepositCancelReceiptController : ApiController
    {


        [Route("getAllRecordsCancelCautionFeeReceipt")]
        public HttpResponseMessage getAllRecordsCancelCautionFeeReceipt(string from_date, string to_date, string search)
        {
             
            List<Sim615> com_list = new List<Sim615>();

            try
            {

                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cancel_caution_deposit_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_date = dr["doc_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["doc_total_amount"].ToString();
                            simsobj.Doc_status = dr["doc_status"].ToString();
                            simsobj.sims_enroll_number = dr["enroll_number"].ToString();
                            simsobj.sims_fee_number = dr["dd_fee_number"].ToString();
                            simsobj.section_name = dr["class"].ToString();
                            com_list.Add(simsobj);
                        }

                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("CUDCancelCautionFeeReceipt")]
        public HttpResponseMessage CUDCancelCautionFeeReceipt(List<Sim615> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim615 data1 in data)
                    {
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_cancel_caution_deposit_fee_receipt_proc]",
                            new List<SqlParameter>()
                             {
                            new SqlParameter("@opr","I"),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(data1.to_date)),
                            new SqlParameter("@doc_no",data1.doc_no),
                            new SqlParameter("@doc_academic_year",data1.doc_academic_year),
                            new SqlParameter("@sims_enroll_number",data1.sims_enroll_number),
                            new SqlParameter("@creation_user",data1.cashier_name),
                            new SqlParameter("@sims_fee_number",data1.sims_fee_number),
                            new SqlParameter("@doc_final_amount",data1.grand_total)

                             });
                        if (dr>0)
                        {
                             inserted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}