﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FEE;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/concessioncel")]
    public class ConcessionCancelController:ApiController
    {

        [Route("GetCancelAcademic_year")]
        public HttpResponseMessage GetCancelAcademic_year()
        {
            List<cancel_receipt> acayear = new List<cancel_receipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_cancel_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cancel_receipt ob = new cancel_receipt();
                            ob.academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            ob.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            acayear.Add(ob);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, acayear);

        }

        [Route("Concession_ReceiptList")]
        public HttpResponseMessage Concession_ReceiptList(cancel_receipt o)
        {
            List<cancel_receipt> lstReceipts = new List<cancel_receipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_cancel_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_enroll_number", o.enrollment_no),
                new SqlParameter("@doc_user", o.doc_user),
                new SqlParameter("@start_date", db.DBYYYYMMDDformat(o.start_date)),
                new SqlParameter("@end_date", db.DBYYYYMMDDformat(o.end_date)),
                new SqlParameter("@academic_year", o.sims_academic_year_description),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cancel_receipt obj = new cancel_receipt();
                            obj.isEnabled = false;
                            obj.doc_no = dr["doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_date"].ToString()) == false)
                                //obj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                                obj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            else
                                obj.doc_date = string.Empty;
                            obj.doc_amount = dr["doc_total_amount"].ToString();
                            obj.doc_status = dr["doc_status"].ToString();
                            obj.doc_code = dr["dt_code"].ToString();
                            //if (obj.doc_status.ToUpper() == commonStaticClass.sims043_Feedoc_Type_2)
                            //    obj.isEnabled = false;
                            //else
                            //    obj.isEnabled = true;
                            obj.enrollment_no = dr["enroll_number"].ToString();
                            obj.studentName = dr["Name"].ToString();
                            obj.cur_code = dr["sims_cur_code"].ToString();
                            obj.academic_year = dr["sims_academic_year"].ToString();
                            obj.grade_code = dr["sims_grade_code"].ToString();
                            obj.section_code = dr["sims_section_code"].ToString();
                            obj.doc_generated_by = dr["CreateUser"].ToString();
                            obj.IsSiblingReceipt = dr["IsSiblingReceipt"].ToString().Equals("Y") ? true : false;
                            obj.sims_concession_description = dr["sims_concession_description"].ToString();
                            lstReceipts.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstReceipts);

        }

        [Route("ReceiptList")]
        public HttpResponseMessage ReceiptList(cancel_receipt o)
        {
            List<cancel_receipt> lstReceipts = new List<cancel_receipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_cancel_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_enroll_number", o.enrollment_no),
                new SqlParameter("@doc_user", o.doc_user),
                new SqlParameter("@start_date", db.DBYYYYMMDDformat(o.start_date)),
                new SqlParameter("@end_date", db.DBYYYYMMDDformat(o.end_date)),
                new SqlParameter("@academic_year", o.sims_academic_year_description),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cancel_receipt obj = new cancel_receipt();
                            obj.doc_no = dr["doc_no"].ToString();
                            if (string.IsNullOrEmpty(dr["doc_date"].ToString()) == false)
                                //obj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                                obj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            else
                                obj.doc_date = string.Empty;
                            obj.doc_amount = dr["doc_total_amount"].ToString();
                            obj.doc_status = dr["doc_status"].ToString();
                            obj.doc_code = dr["dt_code"].ToString();
                            //if (obj.doc_status.ToUpper() == commonStaticClass.sims043_Feedoc_Type_2)
                            //    obj.isEnabled = false;
                            //else
                            //    obj.isEnabled = true;
                            obj.enrollment_no = dr["enroll_number"].ToString();
                            obj.studentName = dr["Name"].ToString();
                            obj.cur_code = dr["sims_cur_code"].ToString();
                            obj.academic_year = dr["sims_academic_year"].ToString();
                            obj.grade_code = dr["sims_grade_code"].ToString();
                            obj.section_code = dr["sims_section_code"].ToString();
                            obj.doc_generated_by = dr["CreateUser"].ToString();
                            obj.IsSiblingReceipt = dr["IsSiblingReceipt"].ToString().Equals("Y") ? true : false;
                            lstReceipts.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstReceipts);

        }

        [Route("Cancel_Concession_FeeReceipt")]
        public HttpResponseMessage Cancel_Concession_FeeReceipt(List<cancel_receipt> obj)
        {
            bool inserted = false;
            List<cancel_receipt> lstEmp = new List<cancel_receipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (cancel_receipt simsobj in obj)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_cancel_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "U"),
                new SqlParameter("@sims_doc_no", simsobj.doc_no),
                new SqlParameter("@sims_enroll_number", simsobj.enrollment_no),
                new SqlParameter("@cur_code", simsobj.cur_code),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@grade_code", simsobj.grade_code),
                new SqlParameter("@section_code", simsobj.section_code),
                new SqlParameter("@cancel_doc_date", db.DBYYYYMMDDformat(simsobj.doc_date)),
                         });
                    if (dr.RecordsAffected>0)
                    {
                        inserted = true;
                    }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CancelFeeReceipt")]
        public HttpResponseMessage CancelFeeReceipt(List<cancel_receipt> objlst)
        {
            bool inserted = false;
            List<cancel_receipt> lstEmp = new List<cancel_receipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (cancel_receipt simsobj in objlst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_cancel_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr",simsobj.IsSiblingReceipt==true? "C":"U"),
                new SqlParameter("@sims_doc_no", simsobj.doc_no),
                new SqlParameter("@sims_enroll_number", simsobj.enrollment_no),
                new SqlParameter("@cur_code", simsobj.cur_code),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@grade_code", simsobj.grade_code),
                new SqlParameter("@section_code", simsobj.section_code),
                new SqlParameter("@cancel_doc_date", db.DBYYYYMMDDformat(simsobj.doc_date)),
                         });
                        int r = dr.RecordsAffected;
                        if (r > 0 && simsobj.IsSiblingReceipt == false)
                        {
                            inserted = true;
                            dr.Close();
                            db.Open();
                            dr = db.ExecuteStoreProcedure("dbo.sims_cancel_fee_receipt",
                       new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","I"),
                new SqlParameter("@sims_doc_no", simsobj.doc_no),
                new SqlParameter("@sims_enroll_number", simsobj.enrollment_no),
                new SqlParameter("@cur_code", simsobj.cur_code),
                new SqlParameter("@academic_year", simsobj.academic_year),
                new SqlParameter("@grade_code", simsobj.grade_code),
                new SqlParameter("@section_code", simsobj.section_code),
                new SqlParameter("@cancel_doc_date", db.DBYYYYMMDDformat(simsobj.doc_date)),
                         });
                            if(dr.RecordsAffected>0)
                            {
                                inserted = true;
                            }
                            else
                                inserted = false;

                        dr.Close();

                        }
                        else if (r>0)
                             inserted = true;
                            else
                                inserted = false;
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}